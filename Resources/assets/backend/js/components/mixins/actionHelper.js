export default {
  computed: {
    routeParameters: function () {
      return !_.isNil(this.configuration) && !_.isUndefined(this.configuration.parameters) ? this.configuration.parameters : null;
    },
    apiReference: function () {
      if (!_.isNil(this.configuration) && !_.isNil(this.configuration.type)) {
        return this.getApiReferenceFromConfig(this.configuration.type);
      }
      console.error('Could not find the api reference for "' + this.configuration.type + '" action.');
      return null;
    },
    actionParameters: function () {
      let action = null;
      // Case New
      if (this.isNew && this.currentNewEdit) {
        action = this.currentNewEdit?.apis?.new;
      } else if (!_.isNil(this.configuration) && !_.isNil(this.configuration.type) && !_.isNil(this.configuration.apis)) {
        action = _.get(this.configuration.apis, this.configuration.type) ?? null;
      }
      return !_.isNil(action) ? {...this.buildPathParametersForAction(action), ...this.getApiStaticParametersForAction(action)} : null;
    },
    parent: function () {
      let parent = _.find(this.actionParameters, (parameterValue, parameterName) => {
        let parameterBase = parameterName.split('.')[0];
        return parameterBase === 'parent';
      });
      return parent !== undefined ? parent : null;
    },
    backRoute: function () {
      if (this.configuration?.backAction) {
        return this.buildActionRoute(this.configuration.backAction);
      }
      return null;
    }
  },
  methods: {
    buildParametersFromData: function (definition, data) {
      let parameters = {};
      if(definition !== undefined && data !== null) {
        _.forEach(definition, (parameterDefinition) => {
          let parameterName = parameterDefinition.name;
          let parameterValue = this.getParameterValue(parameterDefinition, data);
          let parameterBase = this.getBaseParameter(parameterName);
          if (parameterValue !== null) {
            parameters[parameterName] = parameterValue;
            parameters['exists[' + parameterBase + ']'] = true;
          } else {
            parameters['exists[' + parameterBase + ']'] = false;
          }
        });
      }
      return parameters;
    },
    buildPathParametersFromData: function (definition, data) {
      let parameters = {}
      if(definition !== undefined && data !== null) {
        _.forEach(definition, (contentValue, pathParameterName) => {

          let parameterValue = _.get(data, contentValue, null);
          if (parameterValue !== null) {
            parameters[pathParameterName] = parameterValue;
          }
        });
      }
      return parameters;
    }
    ,
    getParameterValue: function (definition, data) {
      switch(definition.type) {
        case 'static':
          return definition.value;
        case 'dynamic':
          let base = this.getBaseParameter(definition.value);
          return _.get(data, base, null);
      }
      return null;
    },
    buildActionRoute: function (action, row = null) {
      let params = this.buildPathParametersFromData(action.pathParameters, row);
      return {
        name: action.name,
        params
      };
    },
    getBaseParameter: function (parameter) {
      return parameter.split('.')[0];
    },
    getApiReferenceFromConfig: function (action) {
      if (!_.isNil(this.configuration) && !_.isNil(this.configuration.apis) && _.has(this.configuration.apis, action)) {
        return  _.get(this.configuration.apis, action).reference.replace('.', '_');
      }
      return null;
    },
    getApiStaticParametersForAction: function (action) {
      return  action?.staticParameters ?? null;
    },
    buildPathParametersForAction: function (action) {
      let renameParameterRules = action?.renamePathParameters ?? null;
      // Replace route param key if there is a rule in config
      return _.mapKeys(this.$route.params, (value, key) => {
        return _.has(renameParameterRules, key) ? _.get(renameParameterRules, key) : key;
      })
    },
    changeActionName: function (fromActionWithSection, toAction) {
      const [fromAction, section] = fromActionWithSection.split('#');
      return `${toAction}#${section}`;
    }
  }
}
