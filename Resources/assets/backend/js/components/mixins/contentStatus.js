import {mapState, mapGetters} from "vuex";

export default {
  computed: {
    ...mapState('languages', [
      'currentLocale'
    ]),
    ...mapGetters('edits', [
      'getCurrentContentHash'
    ]),
    versionHashes: function () {
      return this.content && this.content.versionHashes !== undefined
        ? this.content.versionHashes
        : null;
    },
    draftHashes: function () {
      return this.content && this.content.draftHashes !== undefined
        ? this.content.draftHashes
        : null;
    },
    versionHash: function () {
      return this.content && this.content.versionHashes !== undefined && this.content.versionHashes[this.currentLocale] !== undefined
        ? this.content.versionHashes[this.currentLocale]
        : null;
    },
    draftHash: function () {
      return this.content && this.content.draftHashes !== undefined && this.content.draftHashes[this.currentLocale] !== undefined
        ? this.content.draftHashes[this.currentLocale]
        : null;
    },
    lastPublicationDate: function () {
      return this.versionHash && this.versionHash.date !== undefined
        ? this.versionHash.date.date
        : null;
    },
    lastSaveDate: function () {
      return this.draftHash && this.draftHash.date !== undefined
        ? this.draftHash.date.date
        : null;
    },
    changesHaveBeenMade: function () {
      return this.versionHash !== null
        && this.currentHash !== null
        && !this.isFixture(this.draftHash.hash)
        && this.versionHash.hash !== this.currentHash
        ;
    },
    currentHash: function () {
      return this.getCurrentContentHash
    },
    noDraftForLocale: function () {
      return this.draftHash === null || this.draftHash.hash === null;
    }
  },
  methods: {
    isPublicationPending: function(locale) {
      if(!this.isPublished(locale)) {
        return false;
      } else if (this.getLocaleVersionHash(locale) && this.getLocaleDraftHash(locale)) {
        return this.getLocaleVersionHash(locale).hash !== this.getLocaleDraftHash(locale).hash;
      } else {
        return false;
      }
    },
    isPublished: function (locale) {
      let localeVersionHash = this.getLocaleVersionHash(locale);
      return localeVersionHash !== null && localeVersionHash.hash !== null;
    },
    getLocaleVersionHash: function (locale) {
      return this.versionHashes && this.versionHashes[locale] !== undefined ? this.versionHashes[locale] : null;
    },
    getLocaleDraftHash: function (locale) {
      return this.draftHashes && this.draftHashes[locale] !== undefined ? this.draftHashes[locale] : null;
    },
    getLanguageStatusClass: function (locale = null) {
      if(!locale) {
        locale = Object.values(this.languages)[0];
      }

      if (this.isPublicationPending(locale)) {
        return 'pending';
      } else if(this.isPublished(locale)) {
        return 'active';
      } else {
        return '';
      }
    },
    isFixture: function (hash) {
      return hash.includes('fixture');
    }
  }
}
