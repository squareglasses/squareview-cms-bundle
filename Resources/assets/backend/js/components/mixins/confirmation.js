import {mapActions, mapState} from "vuex";

export default {
  computed: {
    ...mapState('general', [
      'confirmation'
    ])
  },
  methods: {
    ...mapActions('general', [
      'openConfirmation',
      'setConfirmationMessage',
      'setConfirmationTitle',
      'closeConfirmation',
      'clearConfirmation',
      'setConfirmationOkCallback',
      'setConfirmationCancelCallback',
      'setConfirmationOkBtn',
      'setConfirmationCancelBtn',
    ]),
    confirm: function (title, message, okCallback, cancelCallback = null, okBtn = "Confirmer", cancelBtn = "Annuler") {
      this.setConfirmationMessage(message);
      this.setConfirmationTitle(title);
      this.setConfirmationOkCallback(okCallback);
      this.setConfirmationCancelCallback(cancelCallback);
      this.setConfirmationOkBtn(okBtn);
      this.setConfirmationCancelBtn(cancelBtn);
      this.openConfirmation();
    },
    closeAndClearConfirmation: function () {
      this.clearConfirmation();
      this.closeConfirmation();
    },
    runCallbackOk: function () {
      if(!this.confirmation.callback.ok) {
        console.error('No OK callback function defined for confirmation');
      } else if(typeof(this.confirmation.callback.ok) !== 'function') {
        console.error('OK callback is not a function', typeof(this.confirmation.callback.ok));
      } else {
        this.confirmation.callback.ok();
        this.closeAndClearConfirmation();
      }
    },
    runCallbackCancel: function () {
      if(!this.confirmation.callback.cancel || typeof(this.confirmation.callback.cancel) !== 'function') {
        this.closeAndClearConfirmation();
      } else {
        this.confirmation.callback.cancel();
        this.closeAndClearConfirmation();
      }
    }
  }
}
