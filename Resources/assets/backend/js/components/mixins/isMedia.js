export default {
  data: function () {
    return {
      imgPattern: process.env.FRONTEND_URL+'medias/{format}/{id}/{filename}',
    }
  },
  methods: {
    getMediaUrl: function (format, id, filename) {
      return this.imgPattern
        .replace('{format}', format)
        .replace('{id}', id)
        .replace('{filename}', filename);
    }
  },
  filters: {
    formatBytes: function (bytes, decimals = 2) {
      if (bytes === 0) return '0 Bytes';
      if (bytes === undefined || bytes === null) return '';

      const k = 1000; //utilisation SI ou kilo = 1000 et non 1024
      const dm = decimals < 0 ? 0 : decimals;
      const sizes = ['octets', 'Ko', 'Mo', 'Go', 'To'];
      let i = Math.floor(Math.log(bytes) / Math.log(k));
      let result = parseFloat((bytes / Math.pow(k, i)).toFixed(dm));
      return new Intl.NumberFormat('fr-FR').format(result) + ' ' + sizes[i];
    },
    translateProvider: function (provider) {
      switch(provider) {
        case 'image':
          return 'Image';
        case 'file':
          return 'Fichier';
        case 'pdf':
          return 'Fichier PDF';
        case 'video':
          return 'Vidéo';
        default:
          return provider.capitalize();
      }
    }
  }
}
