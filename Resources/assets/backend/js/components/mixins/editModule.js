import _ from "lodash";
import {unobserve} from "sv/js/util/unobserver";

export default {
  props: ['module', 'guide'],
  data: function () {
    return {
      parameters: {}
    };
  },
  watch: {
    module: {
      handler: 'updateModuleParameters',
      deep: true
    },
    guide: {
      handler: 'updateModuleParameters',
      immediate: true
    }
  },
  methods: {
    updateModuleParameters: function () {
      let moduleParameters = {};
      _.each(this.guide.parameters, (guideParameter) => {
        let parameter = this.getParameterForGuide(guideParameter);
        if(parameter === null || parameter === undefined) {
          parameter = {
            identifier: guideParameter.name,
            value: null,
            id: null
          };
        }
        moduleParameters[guideParameter.name] = parameter;
      });
      this.parameters = moduleParameters;
    },
    getParameterKey: function (name) {
      return _.findKey(this.module.parameters, {identifier: name});
    },
    getParameterForGuide: function (guide) {
      if(guide === undefined || guide === null) {
        return null;
      }

      switch(guide.formType) {
        case 'media':
          return _.find(this.module.medias, {identifier: guide.name});
        default:
          return _.find(this.module.parameters, {identifier: guide.name});
      }
    },
    getValueForGuide: function (guide) {
      let parameter = this.parameters[guide.name];
      switch(guide.formType) {
        case 'media':
          return parameter;
        case 'autocomplete':
          return parameter !== null && _.has(parameter, 'resource') ? parameter.resource : null;
        default:
          return parameter !== null && _.has(parameter, 'value') ? parameter.value : null;
      }
    },
    getParameterGuide: function (parameter) {
      let identifier = parameter.identifier;
      return _.find(this.guide.parameters, {name: identifier});
    },
    updateModule: function(parameter, value, validateOnly = false) {
      let updatedParameterKey = this.getParameterKey(parameter);
      let updatedParameter = unobserve(this.parameters[parameter]);
      let updatedModule = unobserve(this.module);

      if(!validateOnly) {
        if (_.isObject(value)) {
          updatedParameter = _.merge(updatedParameter, value);
        } else {
          updatedParameter.value = value;
        }
      }

      if(updatedParameterKey === undefined) {
        updatedModule.parameters.push(updatedParameter);
      } else {
        updatedModule.parameters[updatedParameterKey] = updatedParameter;
      }

      this.$emit('update', updatedModule, parameter, validateOnly);
    },
    buildParameterOptions: function (parameter) {
      let guide = this.getParameterGuide(parameter);

      //default options
      let selectedOptions = [
        'name',
        'label',
        'defaultValue',
        'required',
        'description'
      ];

      //specific options
      switch (guide.formType) {
        case 'select':
          selectedOptions.push('choices');
          break;
      }

      return _.pick(guide, selectedOptions);
    }
  }
}
