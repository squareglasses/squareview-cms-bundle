//This mixin must be used either with editHelper or listHelper depending on the context

import {generateObjectHash, getDeep, mergeConfigurations, setDeep} from "../../util/objectHandler";
import {mapActions, mapGetters, mapMutations, mapState} from "vuex";
import _ from "lodash";
import {validationMixin} from 'vuelidate';
import {required, requiredIf} from 'vuelidate/lib/validators';
import errorLog from "sv/js/components/mixins/errorLog";
import {unobserve} from "sv/js/util/unobserver";
import featureHelper from "sv/js/components/mixins/featureHelper";
import actionHelper from "sv/js/components/mixins/actionHelper";

const queueDebounceDelay = 1000;

//Validators:
const isAnArray = (value) => _.isArray(value);
const mustBeOneOf = (acceptedValues) => (value) => {
  return _.includes(acceptedValues, value);
};

export default {
  mixins: [validationMixin, errorLog, featureHelper, actionHelper],
  computed: {
    ...mapState('languages', [
      'currentLocale'
    ]),
    ...mapState('edits', [
      'currentNewEdit',
      'currentEdit',
      'isNew',
      'hasErrors'
    ]),
    ...mapGetters('edits', [
      'isAutoSaving',
      'isCurrentContentCreated',
      'getCurrentContent',
      'getCurrentEditName',
      'hasQueuedUpdates'
    ])
  },
  data: function () {
    return {
      updatePayload: {
        edit: null,
        value: null,
        apiName: null,
        path: [],
        id: null,
        parameter: null,
        queueStrategy: null,
        updateStrategy: null
      }
    };
  },
  validations: {
    updatePayload: {
      edit: {
        required
      },
      value: {
        required
      },
      apiName: {
        required
      },
      type: {
        required
      },
      path: {
        isAnArray
      },
      id: {
        required: requiredIf(function () {
          return this.updatePayload.type !== 'properties'
            && this.updatePayload.type !== 'modules_order'
            && this.updatePayload.type !== 'collection';
        })
      },
      parameter: {
        required: requiredIf(function () {
          return this.updatePayload.type === 'modules';
        })
      },
      zone: {
        required: requiredIf(function () {
          return this.updatePayload.type === 'modules_order' || this.updatePayload.type === 'collection';
        })
      },
      queueStrategy: {
        required,
        mustBeOneOf: mustBeOneOf(['skip', 'merge', 'merge_parameters', 'replace'])
      },
      updateStrategy: {
        required,
        mustBeOneOf: mustBeOneOf(['skip', 'merge', 'replace'])
      }
    }
  },
  methods: {
    ...mapGetters('edits', [
      'alreadyInUpdateQueue',
      'getUpdateQueuedByStatus',
      'getApiReferenceByActionType'
    ]),
    ...mapActions('edits', [
      'isSaving',
      'showHasErrors',
      'updateDraftHashes',
      'forceDraftHashUpdate',
      'replaceNewContentWithCreatedContent',
      'clearViolations',
      'removeViolation'
    ]),
    ...mapMutations('edits', {
      'addToUpdateQueue': 'addToUpdateQueue',
      'removeFromUpdateQueue': 'removeFromUpdateQueue',
      'markIsNew': 'isNew',
      'markHasErrors': 'hasErrors',
      'setLastErrorType': 'lastErrorType'
    }),
    ...mapGetters('general', [
      'getModuleGuide',
      'getGuide'
    ]),
    queueUpdate: function (updateFunction, payload, successCallback = null, failureCallback = null, forceSave = false, validateOnly = false) {
      let updatePayload = this.normalizeUpdatePayload(payload);
      this.$v.updatePayload.$touch();//Validate update payload

      if(updatePayload.type === 'properties') {
        this.removeViolation(updatePayload.path);
      }

      if (this.$v.updatePayload.$invalid) {
        let error = 'Payload sent to queueUpdate is not valid. Check console for errors.';
        console.error('Validation errors', this.$v.updatePayload, updatePayload);
        this.logError(error);
      } else {
        let hash = this.createUpdateHash(updateFunction, updatePayload);
        let alreadyQueued = this.alreadyInUpdateQueue()(hash);

        if (alreadyQueued) {
          //Case modules: only update new parameter, keep already queued other parameters for same module:
          if (updatePayload.queueStrategy === 'merge_parameters') {
            let newParameterFound = false;
            let newParameter = _.find(unobserve(updatePayload.value.parameters), (parameter) => {
              return parameter.identifier === updatePayload.parameter;
            });
            _.mergeWith(
              updatePayload.value.parameters,
              alreadyQueued.updatePayload.value.parameters,
              (objValue, srcValue, key, object, source) => {
                if (key === 'value') {
                  if (object.identifier !== updatePayload.parameter) {
                    return srcValue; //Keep alreadyQueued/source value for all parameter identifiers != than current updated one
                  } else { //We are on the currently updated parameter, we keep the new parameter value
                    newParameterFound = true;
                    return newParameter.value; //We don't use objValue as, in case of newly added parameters, this value can be undefined
                  }
                }
                return undefined;
              }
            );
            if(!newParameterFound) { //If the new parameter was not found (case newly added parameter), we add it manually
              updatePayload.value.parameters.push(newParameter);
            }
            //Case properties
          } else if (updatePayload.queueStrategy === 'merge') {
            _.mergeWith(
              updatePayload.value,
              alreadyQueued.updatePayload.value,
              (objValue, srcValue) => {
                if (_.isPlainObject(objValue) && _.isPlainObject(srcValue)) { //Case property is an object
                  return _.merge(unobserve(srcValue), unobserve(objValue));
                } else {
                  if (objValue === undefined) {
                    return srcValue;
                  } else {
                    return objValue;
                  }
                }
              }
            );
          } //else = replace, don't do anything, keep new values only

          //If was debouncing (waiting for) a save, remove timeout to start a new one further along
          if(alreadyQueued.status === 'debouncing') {
            clearTimeout(alreadyQueued.updateTimeout);
          }
        }

        //Add auto and default data when new
        if(this.isNew) {
          updatePayload.value = _.merge(
            updatePayload.value,
            this.buildDefaultNewValues(updatePayload.value)
          );
        }

        let updateTimeout = null;
        let status = 'stalled';
        if(this.validateUpdate(updatePayload)) {
          if(this.hasErrors) {
            this.markHasErrors(false);
          }

          if(!validateOnly) {
            if (updatePayload.queueStrategy === 'skip' || forceSave) {
              //Handle non-autosaving (case routes OR no-autosaving and clicked on Save -> see revalidateAllStalled)
              this.doUpdate(updateFunction, updatePayload, successCallback, failureCallback);
              if (forceSave) {
                this.removeFromUpdateQueue({hash});
              }
            } else {
              //isPublishable = case versionable, with draft
              //isAutoSaving = ??? TODO: remember why this was used this way and rename it or document it
              if (this.isAutoSaving && this.isPublishable) {
                status = 'debouncing';
                updateTimeout = setTimeout(() => {
                  this.removeFromUpdateQueue({hash});
                  this.doUpdate(updateFunction, updatePayload, successCallback, failureCallback);
                }, queueDebounceDelay);
              } else {
                //console.log('Not autoSaving and not publishable (or isPublishable not available), change update strategy to "skip" or update code');
              }
            }
          }
        } else if (!this.isNew) {
          this.markHasErrors(true);
          this.setLastErrorType('invalid');
        }

        //Add changes to update queue
        if (updatePayload.queueStrategy !== 'skip' && !validateOnly && !forceSave) {
          this.addToUpdateQueue({
            hash,
            data: {
              status,
              updateFunction,
              updatePayload,
              updateTimeout,
              //successCallback ? save successCallback if needed after revalidation
            }
          });
        }
      }
    },
    createUpdateHash: function (updateFunction, updatePayload) {
      let updateIdentification = {
        'edit': updatePayload.edit,
        'function': updateFunction.name,
        'apiName': updatePayload.apiName,
        'id': updatePayload.id ?? null,
      };

      return generateObjectHash(updateIdentification);
    },
    normalizeUpdatePayload: function (payload) {
      if(this.isNew) {
        this.updatePayload.apiReference = this.getApiReferenceByActionType()('new');
      } else {
        this.updatePayload.apiReference = payload.apiReference ? payload.apiReference : this.getApiReferenceByActionType()('edit');
      }

      this.updatePayload.edit = this.getCurrentEditName;
      this.updatePayload.apiName = payload.apiName ?? 'properties';
      this.updatePayload.type = payload.type ?? this.updatePayload.apiName;
      this.updatePayload.path = payload.path ?? [];
      this.updatePayload.id = payload.id ?? null;
      this.updatePayload.parameter = payload.parameter ?? null;
      this.updatePayload.locale = payload.locale ?? 'fr';
      this.updatePayload.zone = payload.zone ?? null;
      this.updatePayload.query = payload.query ?? {};

      //Normalize path, case nodes have . in them
      this.updatePayload.path = _.reduce(this.updatePayload.path, (normalized, node) => {
        if(!_.isString(node)){
          normalized.push(node);
        } else {
          let nodes = node.split('.');
          _.each(nodes, (node) => {
            normalized.push(node);
          });
        }
        return normalized;
      }, []);

      //Normalize value
      if(_.isPlainObject(payload.value) && payload.value !== null) {
        this.updatePayload.value = payload.value;
      } else {
        this.updatePayload.value = setDeep({}, this.updatePayload.path, payload.value, true);
      }

      //Detect strategies
      switch(this.updatePayload.type) {
        case 'routes':
          this.updatePayload.queueStrategy = payload.queueStrategy ?? 'skip';
          this.updatePayload.updateStrategy = payload.updateStrategy ?? 'merge';
          break;
        case 'modules':
          if(payload.parameter === undefined || payload.parameter === null) {
            this.logError('Queue update payload of apiName "modules" must define a "parameter" property');
          }
          this.updatePayload.queueStrategy = payload.queueStrategy ?? 'merge_parameters';
          this.updatePayload.updateStrategy = payload.updateStrategy ?? 'replace';
          break;
        case 'properties':
          this.updatePayload.queueStrategy = payload.queueStrategy ?? 'merge';
          this.updatePayload.updateStrategy = payload.updateStrategy ?? 'merge';
          break;
        case 'modules_order':
          this.updatePayload.queueStrategy = payload.queueStrategy ?? 'replace';
          this.updatePayload.updateStrategy = payload.updateStrategy ?? 'skip';
          break;
        case 'collection':
          this.updatePayload.queueStrategy = payload.queueStrategy ?? 'replace';
          this.updatePayload.updateStrategy = payload.updateStrategy ?? 'merge';
          break;
        case 'metatags':
          this.updatePayload.queueStrategy = payload.queueStrategy ?? 'replace';
          this.updatePayload.updateStrategy = payload.updateStrategy ?? 'merge';
          break;
        default:
          this.logError(this.updatePayload.type + " update strategies not defined");
          break;
      }

      return unobserve(this.updatePayload);
    },
    doUpdate: function (updateFunction, payload, successCallback, failureCallback) {
      this.isSaving(true);
      this.clearViolations();
      updateFunction(payload).then((response) => {
        //POST UPDATE ACTIONS
        if(this.isNew) {
          this.markIsNew(false);
          if(this.currentNewEdit.editAfterCreation) {
            this.replaceNewContentWithCreatedContent({content:response.data, locale: this.currentLocale});
            this.$router.replace({
              name: this.currentEdit.name, params: {
                'id': this.getCurrentContent.id,
                'slug': this.getCurrentContent.slug,
                'parent': this.getCurrentContent.parent?.slug,
              }
            });
          } else {
            if(this.currentNewEdit.backAction) {
              this.$router.push(this.buildActionRoute(this.currentNewEdit.backAction));
            } else {
              const listAction = this.changeActionName(this.currentNewEdit.name, 'list');
              this.$router.push(this.buildActionRoute({
                name: listAction,
                params: this.actionParameters ?? {}
              }));
            }
          }
        } else if (this.isPublishable && _.has(response.data, this.currentLocale)) {
          let localeDraftHash = response.data[this.currentLocale];
          this.updateDraftHashes({version: localeDraftHash, locale: this.currentLocale});
        }

        this.isSaving(false);
        if(successCallback && _.isFunction(successCallback)) {
          successCallback(response);
        }
      }).catch((error) => {
        this.logError(error);
        this.showHasErrors({duration: 5000, type: 'api', error: error.response.data});
        this.isSaving(false);

        if(failureCallback && _.isFunction(failureCallback)) {
          failureCallback(error);
        }
      });
    },
    validateUpdate: function (updatePayload) {
      switch(updatePayload.type) {
        case 'routes':
          return this.validateRoute(updatePayload);
        case 'modules':
          return this.validateModule(updatePayload);
        case 'properties':
          return this.validateProperties(updatePayload);
        case 'modules_order':
          return this.isCurrentContentCreated;
        case 'collection':
          return this.validateCollection(updatePayload);
        case 'metatags':
          return true; //no validation
        default:
          this.logError(this.updatePayload.apiName + " validation not defined");
          break;
      }
    },
    validateRoute: function (updatePayload) {
      if (!this.isCurrentContentCreated) {
        return false;
      }
      //make sure path is filled
      let isValid =  updatePayload.value.path !== undefined
        && updatePayload.value.path !== null
        && updatePayload.value.path !== "";

      if (!isValid) {
        this.logError('Route path is not filled');
      }

      return isValid;
    },
    validateModule: function (updatePayload) {
      if (!this.isCurrentContentCreated) {
        return false;
      }
      //make sure each required parameters are filled
      let isValid = true;
      let moduleGuide = this.getModuleGuide()(updatePayload.value.guideIdentifier);
      moduleGuide.parameters.forEach((guide) => {
        let parameter = _.find(updatePayload.value.parameters, (value) => {
          return guide.name === value.identifier;
        });
        if (guide.required !== undefined
          && guide.required === true
          && (guide.formType !== 'media' && guide.formType !== 'autocomplete' && (
            parameter === undefined
            || parameter.value === null
            || parameter.value === undefined
            || parameter.value === ""
            || _.isNaN(parameter.value)
            ) || ( guide.formType === 'media'
              && _.every(updatePayload.value.medias, media => media.identifier !== guide.name)
            ) || ( guide.formType === 'wysiwyg'
              && parameter.value === "<p></p>"
            )
          )
        ) {
          this.logError("Module '"+ moduleGuide.name +"' parameter '"+ guide.name +"' is not valid");
          isValid = false;
        }
      });

      return isValid;
    },
    validateProperties: function (updatePayload) {
      //merge properties to current content
      let currentContent = unobserve(this.getCurrentContent)
      let contentToValidate = setDeep(
        currentContent,
        [],
        updatePayload.value,
        false,
        true
      );

      let requiredProperties = this.getGuideRequiredProperties(currentContent.guide);

      if(this.isNew && this.currentNewEdit && !_.isNil(this.currentNewEdit.generateSlugFrom)) {
        requiredProperties.push(this.currentNewEdit.generateSlugFrom);
      }

      let isValid = true;
      requiredProperties.forEach((property) => {
        let propertyValue = _.get(contentToValidate, property);
        if (propertyValue === undefined
          || propertyValue === null
          || propertyValue === ""
          || _.isNaN(propertyValue)
          || propertyValue === "<p></p>"
        ) {
          this.logError('Property "'+ property +'" is not valid, its value is "'+propertyValue+'"');
          isValid = false;
        }
      });

      //make sure generateSlugFrom property is filled in case it's not marked as required
      return isValid;
    },
    validateCollection: function (updatePayload) {
      if (!this.isCurrentContentCreated) {
        return false;
      }
      let currentContent = unobserve(this.getCurrentContent);
      let collectionElementToValidate = setDeep(
        currentContent,
        updatePayload.path,
        updatePayload.value,
        false,
        false
      );

      //make sure each required field is filled
      let requiredFields = this.getCollectionRequiredFields(currentContent.guide, updatePayload.zone.name);
      let isValid = true;
      requiredFields.forEach((field) => {
        let fieldValue = _.get(collectionElementToValidate, field);
        if (fieldValue === undefined
          || fieldValue === null
          || fieldValue === ""
          || _.isNaN(fieldValue)
        ) {
          this.logError('Collection "'+ updatePayload.zone.name +'" field "'+ field +'" is not valid, its value is "'+ fieldValue +'"');
          isValid = false;
        }
      });

      return isValid;
    },
    revalidateAllStalled: function (forceSave = false) {
      let stalledUpdates = this.getUpdateQueuedByStatus()('stalled');

      if (stalledUpdates.length > 0 || forceSave) {
        _.forEach(stalledUpdates, (update, hash) => {
          this.removeFromUpdateQueue({hash});
          this.queueUpdate(update.updateFunction, update.updatePayload, null, () => {
            this.restallFailed(stalledUpdates);
          }, forceSave);
        });
      } else {
        this.isSaving(true);
        setTimeout(() => {
          this.isSaving(false);
        }, 200);
      }
    },
    restallFailed: function (stalledUpdates) {
      _.forEach(stalledUpdates, (update, hash) => {
        this.addToUpdateQueue({hash, data: update});
      });
    },
    buildDefaultNewValues: function (filledValues) {
      let generateSlugFrom = this.currentNewEdit.generateSlugFrom;
      let generateSlugFromValue = filledValues[generateSlugFrom] ?? null;
      let newValues = {
        'defaultName': generateSlugFromValue
      };
      let newParameters = this.actionParameters; //Add action parameters to body in case needed (they are also in the query)

      _.each(newParameters, (parameterValue, parameterName) => {
        newValues[parameterName] = parameterValue;
      });
      return newValues;
    },
    getGuideConfiguration: function (guideName) {
      let sectionConfiguration = _.omit(this.currentEdit, 'content');
      let guideConfiguration = this.getGuide()(guideName);
      let obj = unobserve(sectionConfiguration);
      let src = guideConfiguration !== null && guideConfiguration !== undefined
        ? unobserve(guideConfiguration.actions.find((action) => action.type === sectionConfiguration.type))
        : {};
      return _.mergeWith(obj, src, mergeConfigurations);
    },
    getGuideRequiredProperties: function (guideName) {
      let configuration = this.getGuideConfiguration(guideName);
      let requiredFields = [];
      if (configuration) {
        let zones = configuration.zones;
        let propertyZones = zones.filter((zone) => {
          return zone.type === 'properties';
        });

        propertyZones.forEach((zone) => {
          let fields = zone.fields;
          fields.forEach((field) => {
            if (field.required !== undefined && field.required === true) {
              requiredFields.push(field.identifier);
            }
          });
        });

        if (configuration.mainTitleRequired) {
          requiredFields.push(configuration.mainTitleProperty);
        }
      }

      return requiredFields;
    },
    getCollectionRequiredFields: function (guideName, collectionZoneName) {
      let configuration = this.getGuideConfiguration(guideName);
      let requiredFields = [];
      if (configuration) {
        let zones = configuration.zones;
        let collectionZone = zones.find((zone) => {
          return zone.name === collectionZoneName;
        });

        collectionZone.fields.forEach((field) => {
          if (field.required !== undefined && field.required === true) {
            requiredFields.push(field.identifier);
          }
        });
      }

      return requiredFields;
    }
  }
}
