import _ from "lodash";
import actionHelper from "sv/js/components/mixins/actionHelper";
import * as DOMPurify from "dompurify";
import {jsonStringToObject} from "sv/js/util/objectHandler";

export default {
  mixins: [actionHelper],
  data: function () {
    return {
      newValue: null
    };
  },
  props: {
    'field': {
      required: true
    },
    'value': {
      default: null
    },
    'options': {
      default: null
    },
    'api': {
      default: null
    },
    'parameters': {
      default: null
    },
    'forModule': {
      type: Boolean,
      default: false
    },
    'module': {
      default: null
    },
    'locale': {
      default: null
    }
  },
  computed: {
    label: function () {
      let label = this.options ? this.options.label || null : null;
      let required = this.options ? this.options.required || null : null;
      return label ? label + (required ? " *" : "") : null;
    },
    inputValue: function () {
      return this.newValue !== null ? this.newValue : (this.value ?? null);
    },
    hasError: function () {
      return this.isRequired
        && (this.inputValue === undefined
          || this.inputValue === null
          || this.inputValue === ''
          || _.isNaN(this.inputValue)
          || (this.inputValue.id !== undefined && this.inputValue.id === null) //case media
          || (this.inputValue === "<p></p>")
        );
    },
    isRequired: function () {
      return (this.field !== null && this.field !== undefined && this.field.required !== undefined && this.field.required === true)
        || (this.options !== null && this.options !== undefined && this.options.required !== undefined && this.options.required === true);
    },
    isReadOnly: function () {
      return (this.field !== null && this.field !== undefined && this.field.readonly !== undefined && this.field.readonly === true)
        || (this.options !== null && this.options !== undefined && this.options.readonly !== undefined && this.options.readonly === true);
    },
    resultFormat: function () {
      if (this.options.resultFormat !== undefined) {
        return this.options.resultFormat;
      } else if(_.has(this.options, 'options') && this.options.options.resultFormat !== undefined) {
        return this.options.options.resultFormat; //Case modules
      } else {
        return null;
      }
    }
  },
  methods: {
    update: function(eventOrValue, validateOnly = false) {
      let value = eventOrValue && eventOrValue.target !== undefined ? eventOrValue.target.value : eventOrValue;
      if(typeof value == "boolean" || value !== this.value || validateOnly) { //Only if changed or validateOnly
        this.newValue = value;
        this.$emit('update', this.field.identifier, this.sanitize(value), validateOnly);
      }
    },
    formatResult: function (result) {
      if(result && this.resultFormat) {
        let formatter = new Function('result', 'return '+this.resultFormat);
        try {
          return formatter(result);
        } catch (error) {
          console.error(error);
          return null;
        }
      } else if(result && _.has(result, 'name')) {
        return result.name;
      } else {
        return null;
      }
    },
    sanitize: function (value) {
      if(value === null) {
        return null;
      }
      if(_.isArray(value) || _.isNumber(value) || _.isBoolean(value)) {
        return value;
      }
      //Optionally deactivate sanitization through field option "sanitize: false"
      //This could allow XSS attacks -- with limited risk due to backend security -- but might be necessary for certain needs.
      if(this.options?.sanitize !== undefined && this.options.sanitize === false) {
        return value;
      }
      //Options for the sanitizer can be passed through "sanitize_options" which requires a valid JSON string of options
      if(this.options?.sanitize_options !== undefined) {
        const sanitizeOptions = jsonStringToObject(this.options.sanitize_options);
        if(sanitizeOptions) { //will be null if jsonString is not valid (with console error)
          return DOMPurify.sanitize(value, sanitizeOptions);
        }
      }
      return DOMPurify.sanitize(value);
    }
  },
  watch: {
    value: function (value) {
      if(this.newValue === value) {
        this.newValue = value;
      }
    }
  }
}
