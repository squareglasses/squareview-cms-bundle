import {mapGetters} from "vuex";
import {mergeConfigurations} from "sv/js/util/objectHandler";
import {unobserve} from "sv/js/util/unobserver";
import _ from "lodash";

export default {
  data: function () {
    return {
      guideConfiguration: null
    };
  },
  computed: {
    actionType: function () {
      return this.sectionConfiguration.type;
    },
    configuration: function () { //Merged configuration with section and guide (if guide defined), equivalent to section if guide not defined
      if(this.guideConfiguration === null || !_.has(this.guideConfiguration, 'actions') || this.guideConfiguration.actions === null) {
        return this.sectionConfiguration;
      }

      let guideConfigurationForActionType = this.guideConfiguration.actions.find((action) => action.type === this.actionType);
      if (guideConfigurationForActionType === undefined) {
        return this.sectionConfiguration;
      }

      let obj = unobserve(this.sectionConfiguration);
      let src = unobserve(guideConfigurationForActionType);
      return _.mergeWith(obj, src, mergeConfigurations);
    }
  },
  methods: {
    ...mapGetters('general', [
      'getGuide'
    ]),
  },
  watch: {
    guide: {
      handler: function (guide) {
        this.guideConfiguration = guide ? this.getGuide()(guide, this.sectionConfiguration.section) : null;
      },
      immediate: true
    }
  }
}
