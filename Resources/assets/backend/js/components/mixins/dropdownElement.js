import { mixin as clickaway } from "vue-clickaway";

export default {
  props: ['show'],
  mixins: [clickaway],
  data: function () {
    return {
      canClose: false
    }
  },
  methods: {
    close: function () {
      if(this.canClose) {
        this.$emit('close');
      }
    },
  },
  watch: {
    show: function (isShowing) {
      if (isShowing) {
        setTimeout(() => {
          this.canClose = true;
        }, 100);
      } else {
        this.canClose = false;
      }
    }
  }
}
