import TextAreaField from "../show/fields/TextAreaField";
import TextField from "../show/fields/TextField";
import PercentageField from "../show/fields/PercentageField";
import FloatField from "../show/fields/FloatField";
import DateField from "../show/fields/DateField";
import TimeField from "../show/fields/TimeField";
import DateTimeField from "../show/fields/DateTimeField";
import ToggleField from "../show/fields/ToggleField";
import SelectField from "../show/fields/SelectField";
import {snakeToPascal} from "../../util/textTransformer";

export default {
  methods: {
    showFieldComponent: function(fieldType) {
      if(fieldType === undefined || fieldType === null || fieldType === "") {
        console.error('fieldType is empty, cannot get fieldComponent in fieldHelper', fieldType);
        return null;
      } else {
        switch (fieldType) {
          case 'textarea':
          case 'paragraph':
          case 'wysiwyg':
          case 'tinymce':
          case 'rich':
            return TextAreaField;
          case 'text':
            return TextField;
          case 'percentage':
            return PercentageField;
          case 'float':
            return FloatField;
          case 'date':
            return DateField;
          case 'time':
            return TimeField;
          case 'datetime':
            return DateTimeField;
          case 'toggle':
          case 'boolean':
          case 'bool':
            return ToggleField;
          case 'select':
          case 'choices':
            return SelectField;
          default:
            let CustomField = snakeToPascal(fieldType) + "Field";
            if (this.$root.importedComponents.includes(CustomField)) {
              return CustomField;
            }
            console.error("fieldType "+fieldType+" is not implemented in fieldHelper");
            return null;
        }
      }
    }
  }
}
