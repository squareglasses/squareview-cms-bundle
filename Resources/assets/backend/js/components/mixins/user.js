export default {
  computed: {
    user: function () {
      return {
        username: USER.username,
        email: USER.email,
        roles: USER.roles.split(';')
      }
    },
    isSuperAdmin: function () {
      return this.hasRole('ROLE_SUPER_ADMIN');
    }
  },
  methods: {
    hasRole: function (role) {
      return this.user.roles.includes(role);
    }
  }
}
