import {mapState} from "vuex";

export default {
  computed: {
    ...mapState('languages', [
      'languages'
    ]),
    ...mapState('edits', {
      'edit': 'currentEdit',
      'newEdit': 'currentNewEdit'
    }),
    ...mapState('general', {
      'generalConfiguration': 'configuration'
    }),
    features: function () {
      if (this.actionType === 'edit' && this.edit !== undefined && this.edit !== null && this.edit.features !== undefined) {
        return this.edit.features;
      } else if (this.actionType === 'edit' && this.newEdit !== undefined && this.newEdit !== null && this.newEdit.features !== undefined) {
        return this.newEdit.features;
      } else if (this.actionType === 'list' && this.list !== undefined && this.list !== null && this.list.features !== undefined) {
        return this.list.features;
      } else if (this.configuration && this.configuration.features !== undefined) {
        return this.configuration.features;
      } else {
        return undefined;
      }
    },
    isMultilingual: function () {
      return this.hasFeature('translatable') && Object.keys(this.languages).length > 0 && this.generalConfiguration.multilingual;
    },
    isPublishable: function () {
      return this.hasFeature('publishable');
    },
    isRoutable: function () {
      return this.hasFeature('routable');
    },
    isPositionable: function () {
      return this.hasFeature('positionable');
    },
    isMetaTaggable: function () {
      return this.hasFeature('metataggable');
    },
  },
  methods: {
    hasFeature: function (feature) {
      return this.features !== undefined ? this.features.includes(feature) : false;
    }
  }
}
