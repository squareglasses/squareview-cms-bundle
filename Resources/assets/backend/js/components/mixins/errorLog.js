import {mapActions, mapState} from "vuex";
import moment from "moment";

export default {
  computed: {
    ...mapState('general', [
      'errorLog'
    ]),
    hasErrorsInLog: function () {
      return this.countErrors > 0;
    },
    countErrors: function () {
      return Object.keys(this.errorLog).length;
    }
  },
  methods: {
    ...mapActions('general', [
      'logError'
    ])
  },
  filters: {
    timestamp: function (timestamp) {
      return moment(+timestamp).format('HH:mm:ss');
    }
  }
}
