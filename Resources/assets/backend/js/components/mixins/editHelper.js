import {mapState} from "vuex";
import guideHelper from "sv/js/components/mixins/guideHelper";
import featureHelper from "sv/js/components/mixins/featureHelper";

export default {
  mixins: [guideHelper, featureHelper],
  computed: {
    ...mapState('languages', [
      'languages',
      'currentLocale'
    ]),
    ...mapState('edits', {
      'edit': 'currentEdit',
      'newEdit': 'currentNewEdit'
    }),
    sectionConfiguration: function () {
      return _.omit(this.edit, 'content');
    },
    content: function() {
      return this.edit && this.edit.content ? this.edit.content : null;
    },
    guide: function () {
      return this.content && this.content.guide ? this.content.guide : null;
    },
    zones: function () {
      return this.configuration.zones;
    }
  }
}
