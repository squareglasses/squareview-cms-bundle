import {mapState} from "vuex";
import guideHelper from "sv/js/components/mixins/guideHelper";
import featureHelper from "sv/js/components/mixins/featureHelper";

export default {
  mixins: [guideHelper, featureHelper],
  computed: {
    ...mapState('languages', [
      'languages',
      'currentLocale'
    ]),
    ...mapState('shows', {
      'show': 'currentShow',
    }),
    sectionConfiguration: function () {
      return _.omit(this.show, 'content');
    },
    content: function() {
      return this.show && this.show.content ? this.show.content : null;
    },
    guide: function () {
      return this.content && this.content.guide ? this.content.guide : null;
    },
    zones: function () {
      return this.configuration.zones;
    }
  }
}
