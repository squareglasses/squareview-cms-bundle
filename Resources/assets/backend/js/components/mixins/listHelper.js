import {mapState} from "vuex";
import guideHelper from "sv/js/components/mixins/guideHelper";
import featureHelper from "sv/js/components/mixins/featureHelper";

export default {
  mixins: [guideHelper, featureHelper],
  computed: {
    ...mapState('languages', [
      'languages',
      'currentLocale'
    ]),
    ...mapState('lists', {
      'list': 'currentList',
      'pagination': 'currentPagination',
      'filters': 'filters'
    }),
    sectionConfiguration: function () {
      return _.omit(this.list, 'content');
    },
    count: function () {
      return this.pagination ? this.pagination.records : 0;
    },
    isFiltered: function () {
      return this.filters !== null && Object.keys(this.filters).length > 0;
    }
  },
  methods: {
    getApiReferenceFromConfig: function (action) {
      if (!_.isNil(this.configuration) && !_.isNil(this.configuration.apis) && _.has(this.configuration.apis, action)) {
        return  _.get(this.configuration.apis, action).reference.replace('.', '_');
      }
      return null;
    },
  }
}
