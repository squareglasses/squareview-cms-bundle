import MediaField from "../edit/fields/MediaField";
import TextAreaField from "../edit/fields/TextAreaField";
import TextField from "../edit/fields/TextField";
import PercentageField from "../edit/fields/PercentageField";
import SelectField from "../edit/fields/SelectField";
import EntityField from "../edit/fields/EntityField";
import MultiSelectField from "../edit/fields/MultiSelectField";
import IntegerField from "../edit/fields/IntegerField";
import FloatField from "../edit/fields/FloatField";
import DateField from "../edit/fields/DateField";
import TimeField from "../edit/fields/TimeField";
import DateTimeField from "../edit/fields/DateTimeField";
import WysiwygField from "../edit/fields/WysiwygField";
import AutocompleteField from "../edit/fields/AutocompleteField";
import ToggleField from "../edit/fields/ToggleField";
import {snakeToPascal} from "../../util/textTransformer";

export default {
  methods: {
    fieldComponent: function(fieldType) {
      if(fieldType === undefined || fieldType === null || fieldType === "") {
        console.error('fieldType is empty, cannot get fieldComponent in fieldHelper', fieldType);
        return null;
      } else {
        switch (fieldType) {
          case 'media':
            return MediaField;
          case 'textarea':
          case 'paragraph':
            return TextAreaField;
          case 'text':
            return TextField;
          case 'percentage':
            return PercentageField;
          case 'entity':
            return EntityField;
          case 'select':
          case 'choice':
            return SelectField;
          case 'multi_select':
            return MultiSelectField;
          case 'integer':
            return IntegerField;
          case 'float':
            return FloatField;
          case 'date':
            return DateField;
          case 'time':
            return TimeField;
          case 'datetime':
            return DateTimeField;
          case 'wysiwyg':
          case 'tinymce':
          case 'rich':
            return WysiwygField;
          case 'autocomplete':
            return AutocompleteField;
          case 'toggle':
          case 'boolean':
          case 'bool':
            return ToggleField;
          default:
            let CustomField = snakeToPascal(fieldType) + "Field";
            if (this.$root.importedComponents.includes(CustomField)) {
              return CustomField;
            }
            console.error("fieldType "+fieldType+" is not implemented in fieldHelper");
            return null;
        }
      }
    }
  }
}
