import _ from "lodash";
import actionHelper from "sv/js/components/mixins/actionHelper";

export default {
  mixins: [actionHelper],
  props: {
    'field': {
      required: true
    },
    'value': {
      default: null
    },
    'label': {
      default: null
    },
    'description': {
      default: null
    },
    'options': {
      default: null
    },
    'api': {
      default: null
    },
    'parameters': {
      default: null
    },
    'locale': {
      default: null
    },
    'showLabel': {
      default: true
    }
  },
  computed: {
    resultFormat: function () {
      if (this.options.resultFormat !== undefined) {
        return this.options.resultFormat;
      } else if(_.has(this.options, 'options') && this.options.options.resultFormat !== undefined) {
        return this.options.options.resultFormat; //Case modules
      } else {
        return null;
      }
    }
  },
  methods: {
    formatResult: function (result) {
      if(result && this.resultFormat) {
        let formatter = new Function('result', 'return '+this.resultFormat);
        try {
          return formatter(result);
        } catch (error) {
          console.error(error);
          return null;
        }
      } else if(result && _.has(result, 'name')) {
        return result.name;
      } else {
        return null;
      }
    }
  }
}
