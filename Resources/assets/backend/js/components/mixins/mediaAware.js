import MediaManager from "../media/MediaManager";
import {mapActions, mapState} from "vuex";

export default {
  components: {MediaManager},
  computed: {
    ...mapState('media', {
      'showMediaManager': 'showManager'
    })
  },
  methods: {
    ...mapActions('media', {
      'toggleMediaManager': 'toggleManager',
      'setMediaManagerTarget': 'setTarget'
    })
  }
}
