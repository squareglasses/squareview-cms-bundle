import VueRouter from "vue-router";
import api from "./api";
import Home from './components/home/Home';
import List from './components/list/List';
import Edit from './components/edit/Edit';
import Show from './components/show/Show';
import Translations from './components/translations/Translations';
import EditLayoutGuideTranslations from "sv/js/components/translations/EditLayoutGuideTranslations";
import _ from "lodash";

let staticRoutes = [{
  path: '/',
  component: Home,
  name: 'home'
},{
  path: '/translations',
  component: Translations,
  name: 'translations'
},{
  path: '/translations/:guide',
  component: EditLayoutGuideTranslations,
  name: 'editLayoutGuideTranslations'
}];

let actionComponentMatching = {
  'list': List,
  'edit': Edit,
  'new': Edit,
  'show': Show
};

function getDynamicRoutes() {
  return api.get('routing');
}

function buildDynamicRoutes(response) {
  let routing = response.data;
  return routing
    //Remove unauthorized routes
    .filter((route) => {
      if(!route.restrictions) {
        return true;
      }
      const roles = USER.roles.split(';');
      const authorizedRoles = _.intersection(roles, route.restrictions);
      return authorizedRoles.length > 0;//At least one common role
    })
    //Prioritize routes
    .sort((routeA, routeB) => {
      //Get number of arguments of the route (number of "{" caracters)
      let routeAArgumentCount = (routeA.path.match(/\{([^\/]*)\}/g)||[]).length;
      let routeBArgumentCount = (routeB.path.match(/\{([^\/]*)\}/g)||[]).length;

      if (routeA.type !== 'new' && routeB.type !== 'new') {
        //Make sure edits are last in priority (to not confuse with news or lists)
        if(routeA.type === 'edit' && routeB.type !== 'edit') {
          return 1; //1 means B comes first
        } else if (routeA.type !== 'edit' && routeB.type === 'edit') {
          return -1; //-1 means A comes first
        }

        //Else, if both edits or both not edits, prioritize routes with less arguments
        if(routeAArgumentCount !== routeBArgumentCount) {
          return routeAArgumentCount - routeBArgumentCount; //The more there are arguments, the lower priority it should have
        } else { //Else if both have same number of arguments, prioritize shortest paths
          return routeA.path.length - routeB.path.length; //The longer the path, the lower the priority
        }
      } else if(routeA.type === 'new' && routeB.type !== 'new') {
          return -1; //1 means A comes first
      } else if (routeA.type !== 'new' && routeB.type === 'new') {
        return 1; //-1 means B comes first
      }
    })
    .map((route) => {
      return {
        path: formatApiPathForVue(route.path),
        component: getRouteComponent(route),
        name: route.name,
        meta: {
          section: route.section
        }
      }
    });
}

function getRouteComponent(route) {
  return actionComponentMatching[route.type] || null;
}

function registerDynamicRoutes(router, routes) {
  _.forEach(routes, route => router.addRoute(route));
}

export const formatApiPathForVue = function (path) {
  return path
    .replace(/\{([^\/]*)\}/g, ':$1');
};

export const formatVuePathForApi = function (path) {
  return path
    .replace(/:([^\/]*)/g, '\{$1\}');
};

export const getMatchedPath = function (route) {
  if(route.matched.length > 0) {
    return route.matched[0].path;
  } else {
    return route.path;
  }
};

export const router = new VueRouter({
  routes: staticRoutes
});

export const dynamicRouter = {
  getDynamicRoutes,
  buildDynamicRoutes,
  registerDynamicRoutes
};

export default {
  router,
  dynamicRouter,
  formatApiPathForVue,
  formatVuePathForApi,
  getMatchedPath
};
