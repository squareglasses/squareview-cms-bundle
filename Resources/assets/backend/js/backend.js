/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import 'vue-multiselect/dist/vue-multiselect.min.css';
import 'sv/scss/backend.scss';
import 'tippy.js/dist/tippy.css';

import 'babel-polyfill';

// Vue application
import Vue from 'vue';

// Custom imports for project
// Automatic registration of components, based on:
// https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';
const requireComponent = require.context('~/js/custom', true, /[a-zA-Z]\w+\.(vue|js)$/);
let importedComponents = [];
requireComponent.keys().forEach(fileName => {
  const componentConfig = requireComponent(fileName);
  const componentName = upperFirst(camelCase(fileName.split('/').pop().replace(/\.\w+$/, '')));
  Vue.component(componentName, componentConfig.default || componentConfig);
  importedComponents.push(componentName);
});

import VueRouter from "vue-router";
import Clipboard from 'v-clipboard'
import Backend from './components/Backend.vue';
import {dynamicRouter, router} from './routing';
import store from './store';

Vue.use(VueRouter);
Vue.use(Clipboard);

new Vue({
  el: '#backend',
  router,
  store,
  created: function () {
    dynamicRouter.getDynamicRoutes().then((response) => {
      let routes = dynamicRouter.buildDynamicRoutes(response);
      dynamicRouter.registerDynamicRoutes(this.$router, routes);
    });
  },
  computed: {
    importedComponents: function () {
      return importedComponents;
    }
  },
  render: h => h(Backend)
});
