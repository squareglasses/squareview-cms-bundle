import {isString} from "lodash";

export const snakeToCamel = function (text) {
  return isString(text) ? text.replace( /([-_]\w)/g, g => g[ 1 ].toUpperCase() ) : null;
};

export const snakeToPascal = function (text) {
    let camelCase = snakeToCamel(text);
    return camelCase ? camelCase[ 0 ].toUpperCase() + camelCase.substr( 1 ) : null;
};

export default {
  snakeToCamel,
  snakeToPascal
};
