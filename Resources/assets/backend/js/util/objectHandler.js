import stringify from 'json-stringify-deterministic';
import sha512 from 'crypto-js/sha512';
import sortKeysRecursive from 'sort-keys-recursive';
import _ from "lodash";

/**
 * Dynamically sets a deeply nested value in an object.
 * Optionally "bores" a path to it if its undefined.
 * @function
 * @param {!object} obj  - The object which contains the value you want to change/set.
 * @param {!array} path  - The array representation of path to the value you want to change/set.
 * @param {!mixed} value - The value you want to set it to.
 * @param {boolean} create - If true, will set value of non-existing path as well.
 * @param {boolean} merge - If true, merge value instead of replacing (case setting objects)
 */
export const setDeep = (obj, path, value, create = false, merge = false) => {
  if(path.length === 0) {
    if(merge) {
      obj = _.merge(obj, value);
    } else {
      obj = value;
    }
  } else {
    let lastLevelNb = path.length - 1;
    path.reduce((carry, currentLevel, levelNb) => {
      if (create && typeof carry[currentLevel] === "undefined" && levelNb !== lastLevelNb) {
        carry[currentLevel] = {};
        return carry[currentLevel];
      }

      if (levelNb === lastLevelNb) {
        if (merge) {
          carry[currentLevel] = _.merge(carry[currentLevel], value);
        } else {
          carry[currentLevel] = value;
        }

        return value;
      }
      return carry[currentLevel];
    }, obj);
  }

  return obj;
};

/**
 * Dynamically gets a deeply nested value in an object.
 * @function
 * @param {!object} obj  - The object which contains the value you want to change/set.
 * @param {!array} path  - The array representation of path to the value you want to change/set.
 */
export const getDeep = (obj, path) => {
  return path.reduce((carry, currentLevel) => {
    if(carry === null || typeof carry[currentLevel] === "undefined") {
      return null;//not found
    }
    return carry[currentLevel];
  }, obj);
};

export const generateObjectHash = (obj) => {
  let stringified = stringify(sortKeysRecursive(obj));
  let hash = sha512(stringified);
  return hash.toString();
};

//Function to merge "section" configuration with "guide" configuration
//Used with lodash's mergeWith function
export const mergeConfigurations = (sectionConfig, guideConfig) => {
  if(sectionConfig === null && guideConfig !== null) {
    return guideConfig; //Add guide configurations if they don't exist in section
  } else if(sectionConfig !== null && (guideConfig === undefined || guideConfig === null)) {
    return sectionConfig; //If guide config is undefined, keep sectionConfig as is
  } else if (_.isArray(sectionConfig)) { //Case section config is an array, merge them
    let mergedConfig = [];
    //Go over each section config
    _.each(sectionConfig, (arraySectionConfig) => {
      let arrayGuideValue = guideConfig.find((arrayGuideElement) => {
        //Possible matches are: exactly same, same name, same identifier.
        return (arrayGuideElement === arraySectionConfig)
          || (
            _.has(arrayGuideElement, 'name')
            && _.has(arraySectionConfig, 'name')
            && arrayGuideElement.name === arraySectionConfig.name
          )
          || (
            _.has(arrayGuideElement, 'identifier')
            && _.has(arraySectionConfig, 'identifier')
            && arrayGuideElement.identifier === arraySectionConfig.identifier
          );
      });
      if(arrayGuideValue !== undefined && arrayGuideValue !== null) { //Guide has defined it, merge section with guide
        mergedConfig.push(_.mergeWith(arraySectionConfig, arrayGuideValue, mergeConfigurations));
      } else if (arrayGuideValue !== undefined && arrayGuideValue === null) { //Guide has defined it, but with ~, use sectionConfig
        mergedConfig.push(arraySectionConfig); //@TODO: guide never gives null, always some default values...
      }
    });
    //Go over guide configs
    _.each(guideConfig, (arrayGuideConfig) => {
      //If already merged before, don't add again
      let alreadyExists = _.some(mergedConfig, (mergeElement) => {
        //Possible matches are: exactly same, same name, same identifier.
        return (arrayGuideConfig === mergeElement)
          || (
            _.has(arrayGuideConfig, 'name')
            && _.has(mergeElement, 'name')
            && arrayGuideConfig.name === mergeElement.name
          )
          || (
            _.has(arrayGuideConfig, 'identifier')
            && _.has(mergeElement, 'identifier')
            && arrayGuideConfig.identifier === mergeElement.identifier
          );
      });
      //Case guide has a value the section didn't have, add it
      if(!alreadyExists) {
        mergedConfig.push(arrayGuideConfig);
      }
    });
    return _.orderBy(mergedConfig, ['position'], ['asc']);
  } else { //If both are defined and none are arrays, use guide configuration
    return guideConfig;
  }
};

export const countLeaves = (obj) => {
  if (!_.isObject(obj) && !_.isArray(obj)) {
    return 1;
  }

  return Object.values(obj).map(countLeaves).reduce((a, b) => a+b, 0);
}

export const jsonStringToObject = (value) => {
  if (!_.isError(_.attempt(JSON.parse, value))) {
    return JSON.parse(value);
  } else {
    console.error('Given value is not a JSON string');
    return null;
  }
}

export default {
  setDeep,
  getDeep,
  generateObjectHash,
  countLeaves,
  jsonStringToObject
};
