import pdf from "../../images/placeholder/pdf.png";
import svg from "../../images/placeholder/svg.png";
import doc from "../../images/placeholder/doc.png";
import docx from "../../images/placeholder/docx.png";
import xls from "../../images/placeholder/xls.png";
import xlsx from "../../images/placeholder/xlsx.png";
import mp4 from "../../images/placeholder/mp4.png";
import mov from "../../images/placeholder/mov.png";
import avi from "../../images/placeholder/avi.png";
import webm from "../../images/placeholder/webm.png";

const placeholders = {
  pdf,
  svg,
  doc,
  docx,
  xls,
  xlsx,
  mp4,
  mov,
  avi,
  webm
};

export default placeholders;
