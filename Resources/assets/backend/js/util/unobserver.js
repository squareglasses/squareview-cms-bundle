export const unobserve = function (object) {
  return object !== null ? JSON.parse(JSON.stringify(object)) : null;
}
