const ready = (callback) => {
  if (document.readyState !== "loading") callback();
  else document.addEventListener("DOMContentLoaded", callback);
}

const handleInputs = () => {
  let inputs = getInputs();
  inputs.forEach(input => checkInitiallyNotEmpty(input));
  inputs.forEach(input => handleInputFocusBlurEvents(input));
}

const getInputs = () => {
  return document.querySelectorAll('input#username, input#password, input#resetting_form_plainPassword_first, input#resetting_form_plainPassword_second');
}

const getSubmit = () => {
  return document.querySelector('button#_submit');
}

const toggleInputStatus = (input, status) => {
  if(input.classList.contains('empty') && status === 'not-empty') {
    input.classList.remove('empty');
    input.classList.add('not-empty');
  } else if (input.classList.contains('not-empty') && status === 'empty') {
    input.classList.remove('not-empty');
    input.classList.add('empty');
  }
}

const checkInitiallyNotEmpty = (input) => {
  if(input.value.length > 0 && input.classList.contains('empty')) {
    toggleInputStatus(input, 'not-empty');
  }
}

const handleInputFocusBlurEvents = (input) => {
  input.addEventListener("focus", () => {
    toggleInputStatus(input, 'not-empty');
  });
  input.addEventListener("blur", () => {
    if(input.value.length === 0) {
      toggleInputStatus(input, 'empty');
    }
  });
}

const handleSubmitButton = () => {
  let inputs = getInputs();
  let submit = getSubmit();
  checkSubmitReady(inputs, submit);
  handleSubmitReadyEvents(inputs, submit);
}

const toggleSubmitStatus = (submit, ready) => {
  if(ready) {
    submit.classList.remove('disabled-login');
  } else {
    submit.classList.add('disabled-login');
  }
}

const checkSubmitReady = (inputs, submit) => {
  let ready = true;
  inputs.forEach((input) => {
    if(input.value.length === 0) {
      ready = false;
    }
  });
  toggleSubmitStatus(submit, ready);
}

const handleSubmitReadyEvents = (inputs, submit) => {
  inputs.forEach((input) => {
    input.addEventListener("input", () => {
      checkSubmitReady(inputs, submit)
    });
  });
}

const handleErrors = () => {
  let alerted = document.querySelector('.input-group.alert');
  let callout = document.querySelector('.callout.alert');
  if(alerted) {
    let inputs = getInputs();
    inputs.forEach((input) => {
      input.addEventListener("input", () => {
        alerted.classList.remove('alert');
        callout.remove();
      });
    });
  }
}

ready(() => {
  handleInputs();
  handleSubmitButton();
  handleErrors();
});
