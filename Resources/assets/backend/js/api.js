import axios from 'axios';
import Routing from 'fos-router';

//Make sure symfony knows request is ajax
axios.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';

axios.interceptors.response.use( (response) => {
  // Return a successful response back to the calling service
  return response;
}, (error) => {
  // Return any error which is not due to authentication back to the calling service
  if (error.response.status !== 401) {
    return new Promise((resolve, reject) => {
      reject(error);
    });
  }

  //If status IS 401
  window.location.reload(); //Reload page to redirect to login
});

export const get = (route, routeParams = {}, query = {}) => axios.get(
  Routing.generate(route, routeParams),
  {params: query}
);
export const post = (route, routeParams = {}, query = {}, data = {}) => axios.post(
  Routing.generate(route, routeParams),
  data,
  {params: query}
);
export const put = (route, routeParams = {}, query = {}, data = {}) => axios.put(
  Routing.generate(route, routeParams),
  data,
  {params: query}
);
export const del = (route, routeParams = {}, query = {}) => axios.delete(
  Routing.generate(route, routeParams),
  {params: query}
);

export const squareview = (method, apiReference, query = null, data = null) => {
  switch (method) {
    case 'GET':
      return get('squareview', {'apiReference': apiReference}, query);
    case 'POST':
      return post('squareview', {'apiReference': apiReference}, query, data);
    case 'PUT':
      return put('squareview', {'apiReference': apiReference}, query, data);
    case 'DELETE':
      return del('squareview', {'apiReference': apiReference}, query);
    default:
      console.error('Method ' + method + ' has not been implemented in squareview api yet');
      break;
  }
};

export default {get, post, put, del, squareview};
