import api from '../../api';
import _ from 'lodash';
import Vue from 'vue';

const INIT_CONFIRMATION = {
  show: false,
  title: null,
  message: null,
  callback: {
    ok: null,
    cancel: null
  },
  btn: {
    ok: null,
    cancel: null
  }
};

const state = {
  isLoading: false,
  configuration: null,
  guides: [],
  moduleGuides: [],
  confirmation: INIT_CONFIRMATION,
  errorLog: {},
  actions: []
};

// getters
const getters = {
  getGuide: (state) => (name, section) => {
    return _.find(state.guides, {name, section});
  },
  getModuleGuide: (state) => (name) => {
    return _.find(state.moduleGuides, {name});
  },
  getMetaTags: (state) => {
    return state.configuration ? state.configuration.metaTags : null;
  },
  getLayoutTranslations: (state) => {
    return state.configuration ? state.configuration.layoutTranslations : null;
  },
  getAction: (state) => (name) => {
    return _.find(state.actions, function(action) { return action.name === name });
  },
};

// actions
const actions = {
  fetch({commit, dispatch}) {
    commit('isLoading', true);
    api.get('general').then((response) => {
      commit('configuration', response.data);
      commit('isLoading', false);
    }).catch((error) => dispatch('logError', error.response.data.error || 'Error fetching general configuration'));
    api.get('guides').then((response) => commit('guides', response.data)).catch((error) => dispatch('logError', error.response.data.error || 'Error fetching guides configuration'));
    api.get('module_guides').then((response) => commit('moduleGuides', response.data)).catch((error) => dispatch('logError', error.response.data.error || 'Error fetching module_guides configuration'));
  },
  openConfirmation({commit}) {
    commit('updateConfirmation', {
      path: ['show'],
      value: true
    });
  },
  closeConfirmation({commit}) {
    commit('updateConfirmation', {
      path: ['show'],
      value: false
    });
  },
  setConfirmationMessage({commit}, payload) {
    commit('updateConfirmation', {
      path: ['message'],
      value: payload
    });
  },
  setConfirmationTitle({commit}, payload) {
    commit('updateConfirmation', {
      path: ['title'],
      value: payload
    });
  },
  setConfirmationOkCallback({commit}, payload) {
    commit('updateConfirmation', {
      path: ['callback', 'ok'],
      value: payload
    });
  },
  setConfirmationCancelCallback({commit}, payload) {
    commit('updateConfirmation', {
      path: ['callback', 'cancel'],
      value: payload
    });
  },
  setConfirmationOkBtn({commit}, payload) {
    commit('updateConfirmation', {
      path: ['btn', 'ok'],
      value: payload
    });
  },
  setConfirmationCancelBtn({commit}, payload) {
    commit('updateConfirmation', {
      path: ['btn', 'cancel'],
      value: payload
    });
  },
  clearConfirmation({commit}) {
    commit('confirmation', INIT_CONFIRMATION);
  },
  logError({state, commit}, payload) {
    let errorMessage;
    if(_.isString(payload)) {
      errorMessage = payload;
    } else {
      console.log({error: payload});
      errorMessage = payload.message;
      let error = payload;
      if(_.has(error, 'error')) {
        error = payload.error;
      }
      errorMessage += " [";
      if(_.has(error, 'response')) {
        if(_.has(error, 'response.data')) {
          if (_.has(error, 'response.data.detail')) {
            errorMessage += error.response.data.detail;
          } else if (_.has(error, 'response.data.message')) {
            errorMessage += error.response.data.message;
          } else if (_.has(error, 'response.data.error')) {
            errorMessage += error.response.data.error;
          } else if (_.isString(error.response.data)) {
            errorMessage += error.response.data;
          }
        } else if (_.isString(error.response)) {
          errorMessage += error.response;
        }
      } else {
        errorMessage += error;
      }
      errorMessage += "]";
    }

    if(!_.some(state.errorLog, (oldLog) => {
      return oldLog === errorMessage;
    })) {
      commit('errorLog', errorMessage);
    }
  }
};

// mutations
const mutations = {
  configuration(state, payload) {
    state.configuration = payload;
  },
  guides(state, payload) {
    state.guides = payload;
  },
  moduleGuides(state, payload) {
    state.moduleGuides = payload;
  },
  confirmation(state, payload) {
    state.confirmation = payload;
  },
  updateConfirmation(state, {path, value}) {
    state.confirmation = _.set(state.confirmation, path, value);
  },
  errorLog(state, payload) {
    Vue.set(state.errorLog, Date.now(), payload);
  },
  isLoading(state, payload) {
    state.isLoading = payload;
  },
  actions(state, payload) {
    state.actions = payload;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
