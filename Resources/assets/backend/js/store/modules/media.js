import api from '../../api';
import Vue from 'vue';
import _ from "lodash";
import {setDeep} from "sv/js/util/objectHandler";
import {unobserve} from "sv/js/util/unobserver";

const LIST_ITEMS_THUMBNAILS = 40;
const LIST_ITEMS_VERTICAL = 7;

const orderMedias = function (medias) {
  return _.orderBy(
    medias,
    [
      (media) => media.uploading || false,
      'updatedAt'
    ],
    [
      'desc',
      'desc'
    ]
  )
}

const state = {
  showManager: false,
  listLoading: false,
  listView: 'thumbnails',
  currentListPage: 1,
  medias: [],
  mediaToLoadQueue: [],
  mediaLoadingQueue: [],
  filters: {
    resource: null,
    search: ""
  },
  selected: null,
  target: null
};

// getters
const getters = {
  mediaById: (state) => (id) => state.medias.find((media) => media.id === id),
  mediasPerPage: (state) => state.listView === 'thumbnails' ? LIST_ITEMS_THUMBNAILS : LIST_ITEMS_VERTICAL,
  filteredMedias: (state, getters) => {
    let searchString = state.filters.search.toLowerCase();
    let provider = _.has(state.target, 'provider') ? state.target.provider : null;
    let filteredMedias = state.medias;
    if(searchString !== "") {
      filteredMedias = filteredMedias.filter((media) => {
        if(media.providerReference !== undefined && media.providerReference.toLowerCase().includes(searchString)){
          return true;
        } else if(media.resourceHasMedia !== undefined && media.resourceHasMedia.length) {
          return media.resourceHasMedia.find((resource) => {
            return resource.name.toLowerCase().includes(searchString)
              || resource.alt.toLowerCase().includes(searchString);
          }) !== undefined;
        }
        return false;
      });
    }
    if(provider) {
      filteredMedias = filteredMedias.filter((media) => media.providerName === provider);
    }
    return filteredMedias;
  },
  paginatedMedias: (state, getters) => {
    return _.chunk(getters.filteredMedias, getters.mediasPerPage)[state.currentListPage - 1];
  },
  pagination: (state, getters) => {
    let filteredMedias = getters.filteredMedias;
    if(filteredMedias === undefined || filteredMedias === null) {
      filteredMedias = state.medias;
    }
    return filteredMedias.length > 0 ? {
      count: Math.ceil(filteredMedias.length / getters.mediasPerPage),
      limit: getters.mediasPerPage,
      page: state.currentListPage,
      records: filteredMedias.length
    } : null;
  }
};

// actions
const actions = {
  fetch: ({commit}) => {
    commit('listLoading', true);
    api.squareview(
      'GET',
      'medias_read'
    ).then((response) => {
      commit('medias', response.data);
      commit('listLoading', false);
    });
  },
  queueMediaToLoad: ({commit}, payload) => {
    let priority = payload.context === 'manager-select';
    delete payload.context;
    commit('queueMedia', {payload, priority});
    commit('updateMedia', {
      id: payload.id,
      path: ['loading', payload.format],
      value: true
    });
  },
  loadMedia: ({commit, getters}, payload) => {
    commit('loadingMedia', payload);
    let media = getters.mediaById(payload.id);

    return new Promise(resolve => {
      if(media) {
        const image = new Image();
        if (payload.path) {
          image.onload = () => {
            resolve(payload);
          };
          image.onerror = () => {
            resolve(payload);
          }
          image.src = payload.path;
        } else {
          commit('updateMedia', {
            id: payload.id,
            path: ['loading', payload.format],
            value: false
          });
          resolve(payload);
        }
      } else {
        console.error('Trying to load unexisting media');
        resolve(payload);
      }
    });
  },
  mediaLoaded: ({commit}, payload) => {
    commit('unqueueMedia', payload);
    commit('updateMedia', {
      id: payload.id,
      path: ['src', payload.format],
      value: payload.path
    });
    commit('updateMedia', {
      id: payload.id,
      path: ['loading', payload.format],
      value: false
    });
  },
  mediaLoadedWithError: ({commit}, payload) => {
    commit('updateMedia', {
      id: payload.id,
      path: ['src', payload.format],
      value: null
    });
    commit('updateMedia', {
      id: payload.id,
      path: ['loading', payload.format],
      value: false
    });
  },
  select: ({commit, state}, payload) => {
    if(state.selected !== null && state.selected.id === payload.id) {
      payload = null; //unselect if currently selected
    }
    commit('selected', payload);
  },
  setTarget: ({commit}, payload) => {
    commit('target', payload);
  },
  toggleManager: ({commit}, payload) => {
    commit('showManager', payload);
    commit('target', null);
  },
  uploadMedia: ({commit, state, dispatch}, payload) => {
    if(state.currentListPage !== 1) {
      commit('currentListPage', 1);
    }

    let now = new Date();
    let reader = new FileReader();
    let file = payload.file;
    reader.onload = function(e) {
      let uploadingMedia = {
        providerReference: file.name,
        providerName: payload.provider,
        loading: {
          media_manager_list_thumbnail: true
        },
        src : {
          media_manager_list_thumbnail: e.target.result
        },
        contentType: file.type,
        createdAt: now.toISOString(),
        updatedAt: now.toISOString(),
        id: 'upload' + now.getTime(),
        uploading: true
      };
      commit('medias', [uploadingMedia]);
    }
    reader.readAsDataURL(file);

    let formData = new FormData();
    formData.append('file', file);

    return api.squareview(
      'POST',
      'medias_create',
      {provider: payload.provider},
      formData
    ).then((response) => {
      commit('updateMedia', {
        id: 'upload' + now.getTime(),
        path: ['id'],
        value: response.data.id
      });
      commit('medias', [response.data]);
    }).catch((error) => {
      dispatch('general/logError', {message: "Error uploading file '"+file.name+"'", error}, { root: true });
      commit('updateMedia', {
        id: 'upload' + now.getTime(),
        path: ['error'],
        value: true
      });
    });
  },
  deleteMedia: ({commit, state, dispatch}) => {
    return api.squareview(
      'DELETE',
      'medias_delete',
      {
        id: state.selected.id
      }
    ).then((response) => {
      commit('removeMedia', { id: state.selected.id });
      commit('selected', null);
      return response
    }).catch((error) => {
      dispatch('general/logError', {message: "Error deleting media '"+state.selected.id+"'", error}, { root: true });
      return error.response;
    });
  }
};

// mutations
const mutations = {
  showManager: (state, payload) => {
    state.showManager = payload;
  },
  listLoading: (state, payload) => {
    state.listLoading = payload;
  },
  queueMedia: (state, {payload, priority}) => {
    let mediaIndex = state.mediaToLoadQueue.findIndex((queuedMedia) => {
      return queuedMedia.id === payload.id && queuedMedia.format === payload.format;
    });
    if(mediaIndex === -1) { //If already in queue, don't add again
      if(priority) {
        state.mediaToLoadQueue.unshift(payload);
      } else {
        state.mediaToLoadQueue.push(payload);
      }
    }
  },
  loadingMedia: (state, payload) => { //Switch queues
    let mediaIndex = state.mediaToLoadQueue.findIndex((queuedMedia) => {
      return queuedMedia.id === payload.id && queuedMedia.format === payload.format;
    });
    state.mediaToLoadQueue.splice(mediaIndex, 1);
    state.mediaLoadingQueue.push(payload);
  },
  unqueueMedia: (state, payload) => {
    let mediaIndex = state.mediaLoadingQueue.findIndex((queuedMedia) => {
      return queuedMedia.id === payload.id && queuedMedia.format === payload.format;
    });
    state.mediaLoadingQueue.splice(mediaIndex, 1);
  },
  updateMedia: (state, payload) => {
    let mediaKey = _.findIndex(state.medias, (media) => {
      return media.id === payload.id;
    });
    payload.path.unshift(mediaKey);
    let newMedias = setDeep(unobserve(state.medias), payload.path, payload.value, true);
    Vue.set(state, 'medias', orderMedias(newMedias));

    if(state.selected && state.selected.id === payload.id) {
      state.selected = newMedias[mediaKey];
    }
  },
  removeMedia: (state, payload) => {
    const newMedias = state.medias.filter((media) => {
      return media.id !== payload.id;
    });
    state.medias = newMedias;
  },
  medias: (state, payload) => {
    let updatedMedias = _.unionBy(payload, state.medias,(media) => {
      return media.id !== undefined ? media.id : media.providerReference;
    });
    state.medias = orderMedias(updatedMedias);
  },
  selected: (state, payload) => {
    state.selected = payload;
  },
  target: (state, payload) => {
    state.target = payload;
  },
  currentListPage(state, payload) {
    state.mediaToLoadQueue = []; //reset queue when changing pages
    state.currentListPage = payload;
  },
  listView(state, payload) {
    state.listView = payload;
    state.currentListPage = 1;
  },
  search(state, payload) {
    state.filters.search = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
