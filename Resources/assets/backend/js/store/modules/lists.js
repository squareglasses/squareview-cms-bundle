import api from '../../api';
import Vue from 'vue';
import _ from "lodash";
import {unobserve} from "sv/js/util/unobserver";
import {countLeaves} from "sv/js/util/objectHandler";

const LIST_ITEMS_PER_PAGE = {
  normal: 20,
  compact: 50
};

const state = {
  listsLoaded: false,
  lists: [],
  isLoading: false,
  currentList: null,
  currentPagination: null,
  currentPage: 1,
  filters: null,
  filterMode: null,
  orderBy: null
};

// getters
const getters = {
  getListConfigurationForPath: (state) => (path) => {
    return path ? state.lists.find((list) => {
      return list.path === path
    }) : null;
  },
  getListByActionName: (state) => (name) => {
    let list = state.lists.find((list) => list.name === name);
    return list !== undefined ? list : null;
  },
  isListContentLoadedByActionName: (state, getters) => (name) => {
    let list = getters.getListByActionName(name);
    return list.loaded;
  }
};

// actions
const actions = {
  fetchLists({commit}) {
    commit('isLoading', true);
    api.get('sections').then((response) => {
      let lists = [];
      let actionsConfig = [];

      for (let section of response.data) {
        let features = section.features;
        let actions = section.actions;
        for (let action of actions) {
          actionsConfig.push(action);

          if (action.type === 'list') {
            action.features = features;
            // Add delete api if section has delete action
            const hasDeleteAction = actions.find((action) => action.type === 'delete');
            if(hasDeleteAction) {
              action.apis['delete'] = hasDeleteAction.apis.delete;
            }
            lists.push(action);
          }
        }
      }
      commit('general/actions', actionsConfig, { root: true });
      commit('lists', lists);
    });
  },
  loadListContent({commit, getters, state}, payload) {
    commit('isLoading', _.has(payload, 'loader') ? payload.loader : true);
    let query = {
      'locale': payload.locale,
      'serialization_groups': ['get', 'count', 'parents']
    };

    //Handle pagination
    if(payload.paginated === false) {
      query['pagination'] = false;
    } else {
      query['pagination'] = true;
      query['page'] = payload.page ?? state.currentPage;
      query['items'] = LIST_ITEMS_PER_PAGE[payload.style];
    }

    if (payload.parameters !== undefined) {
      let hasParent = false;
      _.each(payload.parameters, (parameterValue, parameterName) => {
          if (parameterName.split('.')[0] === 'parent' && !_.isNil(parameterValue)) hasParent = true;
          query[parameterName] = parameterValue;
      });
      query['exists[parent]'] = hasParent;
    }

    if(payload.order !== undefined && payload.order !== null) {
      query['order['+payload.order.name+']'] = payload.order.direction || 'asc';
    }

    if(state.filters) {
      _.each(state.filters, (filterValue, filterName) => {
        query[filterName] = filterValue;
      });
      if(countLeaves(state.filters) > 1) {
        query['mode'] = state.filterMode;
      }
    }

    return api.squareview(
      'GET',
      payload.apiReference,
      query
    ).then((response) => {
      let list = getters.getListByActionName(payload.actionName);
      commit('listContent', {'list': list, 'content': response.data});

      let pagination =
        _.mapValues(
          _.mapKeys(
            _.pickBy(response.headers, (value, header) => {
              return header.includes('pagination');
            }),
          (value, header) => {
            return header.replace('pagination-', '');
          }),
        (value) => {
          return parseInt(value);
        });
      commit('currentPagination', pagination);
    });
  },
  switchCurrentList({commit, getters}, actionName) {
    let list = getters.getListByActionName(actionName);
    if (list && list.loaded) {
      commit('currentList', list);
      commit('currentPage', 1);
    }
  },
  deleteElement({state}, payload) {
    return api.squareview(
      'DELETE',
      payload.apiReference,
      {
        id: payload.id
      }
    );
  },
  removeElementFromCurrentList({state, commit}, payload) {
    let currentListContent = unobserve(state.currentList.content);

    _.remove(currentListContent, (element) => {
      return element.id === payload;
    });

    commit('listContent', {list: state.currentList, content: currentListContent});
  },
  repositionList({commit, getters, state}, payload) {
    return api.squareview(
      'PUT',
      payload.apiReference,
      null,
      payload.ids
    );
  },
  resetFilters({commit}) {
    commit('filters', null);
  },
  resetOrderBy({commit}) {
    commit('orderBy', null)
  }
};

// mutations
const mutations = {
  lists(state, payload) {
    for (let index in payload) {
      payload[index].content = null;
      payload[index].loaded = false;
    }
    state.lists = payload;
    state.listsLoaded = true;
  },
  listContent(state, payload) {
    let lists = unobserve(state.lists);
    let list = _.find(lists, (list) => {
      return list.name === payload.list.name;
    });

    list.loaded = true;
    list.content = payload.content;

    Vue.set(state, 'lists', lists);

    state.currentList = list;
    state.isLoading = false;
  },
  currentList(state, payload) {
    state.currentList = payload;
  },
  isLoading(state, payload) {
    state.isLoading = payload;
  },
  currentPagination(state, payload) {
    state.currentPagination = payload;
  },
  currentPage(state, payload) {
    state.currentPage = payload;
  },
  filters(state, payload) {
    state.filters = payload;
  },
  filterMode(state, payload) {
    state.filterMode = payload;
  },
  orderBy(state, payload) {
    state.orderBy = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
