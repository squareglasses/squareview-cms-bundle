import api from '../../api';
import _ from "lodash";
import Vue from "vue";
import {unobserve} from "sv/js/util/unobserver";

const state = {
  isLoading: false,
  isSaving: false,
  isPublishing: false,
  hasErrors: false,
  layoutGuideTranslations: {},
  layoutGuideLastPublicationDates: {}
};

// actions
const actions = {
  fetchCatalog: ({commit}, {discriminator, zone, locale}) => {
    if(discriminator !== null && zone !== null && locale !== null) {
      commit('isLoading', true);
      return api.squareview(
        'GET',
        'translations_list',
        {
          type: 'layout_guide',
          discriminator,
          zone,
          locale
        }
      ).then((response) => {
        let translations = response.data.translations;
        let lastPublishedAt = response.data.lastPublishedAt;

        commit('layoutGuideTranslations', {
          data: translations,
          path: [discriminator, zone, locale]
        });

        commit('layoutGuideLastPublicationDates', {
          data: lastPublishedAt,
          path: [discriminator, locale]
        });

        commit('isLoading', false);
      });
    } else {
      return null;
    }
  },
  updateTranslation: ({commit, dispatch}, {label, message, key, type, discriminator, zone, locale, resourceId = null}) => {
    switch(type) {
      case 'layout':
        commit('isSaving', true);
        return api.squareview(
          'PUT',
          'translations_update',
          {
            type: 'layout_guide',
            discriminator,
            zone,
            locale
          },
          {
            key,
            message
          }
        ).then(() => {
          commit('layoutGuideTranslations', {
            data: {
              label: label,
              message: message
            },
            path: [discriminator, zone, locale, key]
          });
          commit('isSaving', false);
        }).catch((error) => {
          commit('isSaving', false);
          dispatch('markError');
          throw error;
        });
      case 'resource':
        return api.squareview(
          'PUT',
          'translations_update',
          {
            type: 'resource_guide',
            discriminator,
            zone,
            locale,
            resourceId
          },
          {
            key,
            message
          }
        );
      default:
        console.error('Translation type '+type+' not implemented in translations store');
        return null;
    }
  },
  publishTranslations: ({commit, dispatch}, {type, discriminator, locale}) => {
    switch(type) {
      case 'layout':
        commit('isPublishing', true);
        return api.squareview(
          'POST',
          'translations_create',
          {
            type: 'layout_guide',
            discriminator,
            locale
          }
        ).then((response) => {
          commit('layoutGuideLastPublicationDates', {
            data: response.data.publication_date,
            path: [discriminator, locale]
          });
          commit('isPublishing', false);
        }).catch((error) => {
          commit('isPublishing', false);
          dispatch('markError');
          throw error;
        });
      case 'resource':
        console.error('Translations of type resource_guide are published with the resource content in main Edit component');
        return null;
      default:
        console.error('Translation type '+type+' not implemented in translations store');
        return null;
    }
  },
  markError: ({commit}) => {
    commit('hasErrors', true);
    setTimeout(() => {
      commit('hasErrors', false);
    }, 5000);
  }
};

// mutations
const mutations = {
  layoutGuideTranslations: (state, {data, path}) => {
    let current = unobserve(state.layoutGuideTranslations);
    _.set(current, path, data);
    Vue.set(state, 'layoutGuideTranslations', current);
  },
  layoutGuideLastPublicationDates: (state, {data, path}) => {
    let current = unobserve(state.layoutGuideLastPublicationDates);
    _.set(current, path, data);
    Vue.set(state, 'layoutGuideLastPublicationDates', current);
  },
  isLoading: (state, payload) => {
    state.isLoading = payload;
  },
  isSaving: (state, payload) => {
    state.isSaving = payload;
  },
  isPublishing: (state, payload) => {
    state.isPublishing = payload;
  },
  hasErrors: (state, payload) => {
    state.hasErrors = payload;
  }
};

export default {
  namespaced: true,
  state,
  actions,
  mutations
};
