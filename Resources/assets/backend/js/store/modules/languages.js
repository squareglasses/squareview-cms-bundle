import api from '../../api';

const state = {
  languages: {},
  currentLocale: null
};

// getters
const getters = {
  languagesLoaded: (state) => {
    return Object.keys(state.languages).length > 0;
  },
  isLanguageEnabled: (state) => (locale) => {
    let language = state.languages[locale];
    return language !== undefined ? language.enabled : false;
  },
  getMainLocale: (state) => {
    for(let locale in state.languages){
      let language = state.languages[locale];
      if(language.main){
        return locale;
      }
    }
    return null;
  }
};

// actions
const actions = {
  fetch({commit, getters}) {
    return api.get('languages').then((response) => {
      commit('languages', response.data);
      commit('currentLocale', getters.getMainLocale);
    });
  },
  switchLocale({commit}, payload) {
    commit('currentLocale', payload);
  }
};

// mutations
const mutations = {
  languages(state, payload) {
    let languages = {};
    for(let index in payload.languages) {
      let language = payload.languages[index];
      let id = language.id;
      delete language.id;
      languages[id] = language;
    }
    state.languages = languages;
  },
  currentLocale(state, payload) {
    state.currentLocale = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
