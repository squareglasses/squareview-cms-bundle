import api from '../../api';
import _ from 'lodash';
import Vue from "vue";

const state = {
  choices: {}
};

// getters
const getters = {
  isLoadingApi: (state) => (apiName) => {
    let choices = state.choices[apiName];
    return choices !== undefined && _.has(choices, 'loading') ? choices.loading : false;
  },
  isLoadedApi: (state) => (apiName) => {
    let choices = state.choices[apiName];
    return choices !== undefined && choices.loading === false && choices.results !== undefined || false;
  },
  getChoicesForApi: (state) => (apiName) => {
    let choices = state.choices[apiName];
    return choices !== undefined && _.has(choices, 'results') ? choices.results : null;
  }
};

// actions
const actions = {
  fetchChoices({commit, getters}, {editName, apiName, params}) {
    if(!getters.isLoadingApi(apiName) && !getters.isLoadedApi(apiName)) {
      commit('isLoading', apiName);
      api.squareview('GET', editName, apiName, params).then((response) => {
        if(response.status === 200 && _.isArray(response.data)) {
          commit('choices', {apiName, results: response.data});
        } else {
          console.error('field choices for api '+apiName+' returned a response that was not in the correct format. Awaiting status 200 and array.', response);
        }
      });
    }
  }
};

// mutations
const mutations = {
  choices(state, {apiName, results}) {
    Vue.set(state.choices, apiName, {
      results,
      loading: false
    });
  },
  isLoading(state, apiName) {
    Vue.set(state.choices, apiName, {
      loading: true
    });
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
