import api from '../../api';

const state = {
  items: [],
  isLoading: false
};

// getters
const getters = {
  getItem: (state) => (id) => {
    let item = state.items[id];
    return item || null;
  }
};

// actions
const actions = {
  fetch({commit}) {
    commit('isLoading', true);
    api.get('navigation').then((response) => {
      commit('items', response.data);
      commit('isLoading', false);
    });
  }
};

// mutations
const mutations = {
  items(state, data) {
    state.items = data;
  },
  isLoading(state, payload) {
    state.isLoading = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
