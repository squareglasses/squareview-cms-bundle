import api from "sv/js/api";
import {unobserve} from "sv/js/util/unobserver";
import _ from "lodash";

export default {
  createMetaTag({state, getters}, payload) {
    let content = getters.getCurrentContent;
    return api.squareview(
      'POST',
      'metatags_create',
      {
        locale: payload.locale,
        discriminator: content.discriminator,
        id: content.id
      },
      payload.data
    );
  },
  deleteMetaTag({state, getters}, payload) {
    return api.squareview(
      'DELETE',
      'metatags_delete',
      {
        id: payload
      }
    );
  },
  addMetaTagToContent({commit, getters}, payload) {
    let content = getters.getCurrentContent;
    let metaTags = unobserve(content.metatags);
    metaTags.push(payload);
    commit('updateCurrentContent', {
      path: ['metatags'],
      content: content,
      value: metaTags,
      updateStrategy: 'replace'
    });
  },
  removeMetaTagFromContent({commit, getters}, payload) {
    let content = getters.getCurrentContent;
    let metaTags = unobserve(content.metatags);
    _.remove(metaTags, (metaTags) => {
      return metaTags.id === payload;
    });
    commit('updateCurrentContent', {
      path: ['metatags'],
      content: content,
      value: metaTags,
      updateStrategy: 'replace'
    });
  }
}
