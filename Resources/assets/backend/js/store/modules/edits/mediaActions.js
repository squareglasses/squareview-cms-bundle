import {unobserve} from "sv/js/util/unobserver";
import api from "sv/js/api";

export default {
  updateMedia({state, commit, getters, dispatch}, payload) {
    let content = unobserve(getters.getCurrentContent);
    if (content !== undefined) {
      switch (payload.type) {
        case 'resource':
          let resourceMediaData = getResourceMediaData(content, payload);
          if(resourceMediaData) {
            let {resourceHasMedias, resourceHasMediaIndex} = resourceMediaData;
            if (resourceHasMediaIndex !== -1) { //Changing resource media
              let resourceHasMedia = resourceHasMedias[resourceHasMediaIndex];
              commit('updateCurrentContent', {
                content,
                type: 'medias',
                path: [resourceHasMediaIndex, 'selecting'],
                updateStrategy: 'replace',
                value: true
              });
              return api.squareview(
                'PUT',
                'resource_has_medias_update',
                {
                  id: resourceHasMedia.resourceHasMediaId
                },
                {
                  media: {
                    id: payload.media.id
                  },
                  name: payload.name,
                  alt: payload.alt
                }
              ).then((response) => {
                commit('updateCurrentContent', {
                  content,
                  type: 'medias',
                  path: [resourceHasMediaIndex],
                  updateStrategy: 'replace',
                  value: response.data
                });
                dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
              });
            } else { //Defining a resource media that was empty
              resourceHasMedias.push({
                identifier: payload.identifier,
                selecting: true
              });
              commit('updateCurrentContent', {
                content,
                type: 'medias',
                path: [],
                updateStrategy: 'replace',
                value: resourceHasMedias
              });
              resourceHasMediaIndex = resourceHasMedias.findIndex((rhm) => {
                return rhm.identifier === payload.identifier;
              });

              return api.squareview(
                'POST',
                'resource_has_medias_create',
                {
                  discriminator: content.discriminator,
                  resourceIdentifier: content.id
                },
                {
                  identifier: payload.identifier,
                  media: {
                    id: payload.media.id
                  },
                  name: payload.name,
                  alt: payload.alt
                }
              ).then((response) => {
                commit('updateCurrentContent', {
                  content,
                  type: 'medias',
                  path: [resourceHasMediaIndex],
                  updateStrategy: 'replace',
                  value: response.data
                });
                dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
              });
            }
          } else {
            console.error('No medias found in content when trying to update a media');
          }
          break;
        case 'module':
          let moduleMediaData = getModuleMediaData(content, payload);
          if(moduleMediaData) {
            let {medias, mediaIndex, moduleIndex} = moduleMediaData;
            if(mediaIndex !== -1) {
              medias[mediaIndex].selecting = true;
              commit('updateCurrentContent', {
                content,
                type: 'modules',
                path: [moduleIndex, 'medias'],
                updateStrategy: 'replace',
                value: unobserve(medias)
              });
              return api.squareview(
                'PUT',
                'resource_has_medias_update',
                {
                  id: medias[mediaIndex].resourceHasMediaId
                },
                {
                  media: {
                    id: payload.media.id
                  },
                  name: payload.name,
                  alt: payload.alt
                }
              ).then((response) => {
                medias[mediaIndex] = response.data;
                commit('updateCurrentContent', {
                  content,
                  type: 'modules',
                  path: [moduleIndex, 'medias'],
                  updateStrategy: 'replace',
                  value: unobserve(medias)
                });
                dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
              });
            } else {
              medias.push({
                identifier: payload.identifier,
                selecting: true
              });
              commit('updateCurrentContent', {
                content,
                type: 'modules',
                path: [moduleIndex, 'medias'],
                updateStrategy: 'replace',
                value: unobserve(medias)
              });
              mediaIndex = medias.findIndex((media) => {
                return payload.identifier === media.identifier;
              });

              return api.squareview(
                'POST',
                'resource_has_medias_create',
                {
                  discriminator: 'module',
                  resourceIdentifier: payload.moduleId
                },
                {
                  identifier: payload.identifier,
                  media: {
                    id: payload.media.id
                  },
                  name: payload.name,
                  alt: payload.alt
                }
              ).then((response) => {
                medias[mediaIndex] = response.data;
                commit('updateCurrentContent', {
                  content,
                  type: 'modules',
                  path: [moduleIndex, 'medias'],
                  updateStrategy: 'replace',
                  value: unobserve(medias)
                });
                dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
              });
            }
          } else {
            console.error('No modules found in content when trying to update a media');
          }
          break;
        case 'gallery':
          let galleryMediaData = getGalleryMediaData(content, payload);
          if(galleryMediaData) {
            let {galleryId, galleryIndex, medias, mediaIndex} = galleryMediaData;
            if(galleryIndex === -1) {
              commit('updateCurrentContent', {
                content,
                type: 'galleries',
                path: [0],
                updateStrategy: 'replace',
                value: {
                  identifier: payload.identifier,
                  creating: true
                }
              });
              return api.squareview(
                'POST',
                'gallery_create',
                {
                  discriminator: content.discriminator,
                  resourceIdentifier: content.id
                },
                {
                  identifier: payload.identifier,
                  name: _.capitalize(payload.identifier)
                }
              ).then((response) => {
                let galleries = unobserve(content.galleries) || [];
                galleries[0] = response.data;
                commit('updateCurrentContent', {
                  content,
                  type: 'galleries',
                  path: [],
                  updateStrategy: 'replace',
                  value: galleries
                });
                dispatch('updateMedia', payload);
              });
            } else if(medias[mediaIndex] !== undefined) {
              medias[mediaIndex].selecting = true;
              commit('updateCurrentContent', {
                content,
                type: 'galleries',
                path: [galleryIndex, 'medias'],
                updateStrategy: 'replace',
                value: unobserve(medias)
              });
              return api.squareview(
                'PUT',
                'gallery_has_medias_update',
                {
                  id: medias[mediaIndex].galleryHasMediaId
                },
                {
                  media: {
                    id: payload.media.id
                  },
                  name: payload.name,
                  alt: payload.alt,
                  position: payload.position
                }
              ).then((response) => {
                medias[mediaIndex] = response.data;
                commit('updateCurrentContent', {
                  content,
                  type: 'galleries',
                  path: [galleryIndex, 'medias'],
                  updateStrategy: 'replace',
                  value: unobserve(medias)
                });
                dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
              });
            } else {
              medias.push({
                selecting: true
              });
              commit('updateCurrentContent', {
                content,
                type: 'galleries',
                path: [galleryIndex, 'medias'],
                updateStrategy: 'replace',
                value: unobserve(medias)
              });

              return api.squareview(
                'POST',
                'gallery_has_medias_create',
                {
                  galleryId
                },
                {
                  identifier: payload.identifier,
                  media: {
                    id: payload.media.id
                  },
                  name: payload.name,
                  alt: payload.alt
                }
              ).then((response) => {
                medias[mediaIndex] = response.data;
                commit('updateCurrentContent', {
                  content,
                  type: 'galleries',
                  path: [galleryIndex, 'medias'],
                  updateStrategy: 'replace',
                  value: unobserve(medias)
                });
                dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
              });
            }
          } else {
            console.error('No gallery found in content when trying to update a media');
          }
          break;
        default:
          console.error('Type ' + payload.type + ' for updateMedia not implemented');
          break;
      }
    } else {
      console.error('No content found when trying to update a media');
    }
  },
  removeMedia({state, commit, getters, dispatch}, payload) {
    let content = getters.getCurrentContent;
    if(content !== undefined) {
      switch(payload.type) {
        case 'resource':
          let resourceMediaData = getResourceMediaData(content, payload);
          if(resourceMediaData) {
            let {resourceHasMedias, resourceHasMediaIndex} = resourceMediaData;
            if(resourceHasMediaIndex !== -1) {
              resourceHasMedias[resourceHasMediaIndex].deleting = true;
              commit('updateCurrentContent', {
                content,
                type: 'medias',
                path: [],
                updateStrategy: 'replace',
                value: unobserve(resourceHasMedias)
              });
              return api.squareview(
                'DELETE',
                'resource_has_medias_delete',
                {
                  id: resourceHasMedias[resourceHasMediaIndex].resourceHasMediaId
                }
              ).then((response) => {
                if(response.status === 204) {
                  resourceHasMedias.splice(resourceHasMediaIndex, 1);
                  commit('updateCurrentContent', {
                    content,
                    type: 'medias',
                    path: [],
                    updateStrategy: 'replace',
                    value: unobserve(resourceHasMedias)
                  });
                  dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
                }
              });
            }
          } else {
            console.error('No medias found in content when trying to update a media');
          }
          break;
        case 'module':
          let moduleMediaData = getModuleMediaData(content, payload);
          if(moduleMediaData) {
            let {medias, mediaIndex, moduleIndex} = moduleMediaData;
            if(mediaIndex !== -1) {
              medias[mediaIndex].deleting = true;
              commit('updateCurrentContent', {
                content,
                type: 'modules',
                path: [moduleIndex, 'medias'],
                updateStrategy: 'replace',
                value: unobserve(medias)
              });
              return api.squareview(
                'DELETE',
                'resource_has_medias_delete',
                {
                  id: medias[mediaIndex].resourceHasMediaId
                }
              ).then((response) => {
                if(response.status === 204) {
                  medias.splice(mediaIndex, 1);
                  commit('updateCurrentContent', {
                    content,
                    type: 'modules',
                    path: [moduleIndex, 'medias'],
                    updateStrategy: 'replace',
                    value: unobserve(medias)
                  });
                  dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
                }
              });
            }
          } else {
            console.error('No modules found in content when trying to remove a media');
          }
          break;
        case 'gallery':
          let galleryMediaData = getGalleryMediaData(content, payload);
          if(galleryMediaData) {
            console.log({galleryMediaData});
            let {galleryId, galleryIndex, medias, mediaIndex} = galleryMediaData;
            if(galleryIndex === -1) {
              console.error('Gallery does not exist, this should not happen, investigation needed.');
            } else if(mediaIndex !== -1) {
              medias[mediaIndex].deleting = true;
              commit('updateCurrentContent', {
                content,
                type: 'galleries',
                path: [galleryIndex, 'medias'],
                updateStrategy: 'replace',
                value: unobserve(medias)
              });
              return api.squareview(
                'DELETE',
                'gallery_has_medias_delete',
                {
                  id: medias[mediaIndex].galleryHasMediaId
                }
              ).then((response) => {
                if(response.status === 204) {
                  medias.splice(mediaIndex, 1);
                  commit('updateCurrentContent', {
                    content,
                    type: 'galleries',
                    path: [galleryIndex, 'medias'],
                    updateStrategy: 'replace',
                    value: unobserve(medias)
                  });
                  dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
                }
              });
            }
          } else {
            console.error('No modules found in content when trying to remove a media');
          }
          break;
        default:
          console.error('Type '+payload.type+' for removeMedia not implemented');
          break;
      }
    } else {
      console.error('No content found when trying to remove a media');
    }
  }
}

const getResourceMediaData = (content, payload) => {
  let resourceHasMedias = content.medias;
  if (resourceHasMedias !== undefined) {
    let resourceHasMediaIndex = resourceHasMedias.findIndex((rhm) => {
      return rhm.identifier === payload.identifier;
    });
    return {resourceHasMedias, resourceHasMediaIndex};
  } else {
    console.error('No medias found in content');
    return null;
  }
}

const getModuleMediaData = (content, payload) => {
  let modules = content.modules;
  if (modules !== undefined) {
    let moduleIndex = modules.findIndex(module => module.id === payload.moduleId);
    let module = modules[moduleIndex];
    let medias = module.medias;
    let mediaIndex = medias.findIndex(media => media.identifier === payload.identifier);
    return {medias, mediaIndex, moduleIndex};
  } else {
    console.error('No modules found in content');
    return null;
  }
};

const getGalleryMediaData = (content, payload) => {
  let galleries = content.galleries;
  if (galleries !== undefined) {
    let galleryIndex = galleries.findIndex(gallery => gallery.identifier === payload.identifier);
    if(galleryIndex === -1) {
      return {medias: null, mediaIndex: null, galleryIndex}; //gallery does not exist
    }
    let gallery = galleries[galleryIndex];
    let galleryId = gallery.id;
    let medias = gallery.medias;
    let mediaIndex = payload.position;
    return {galleryId, galleryIndex, medias, mediaIndex};
  } else {
    console.error('No galleries found in content');
    return null;
  }
};
