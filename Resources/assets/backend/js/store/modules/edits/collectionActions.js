import api from "sv/js/api";
import {unobserve} from "sv/js/util/unobserver";
import _ from "lodash";

export default {
  createCollectionElement({state}, payload) {
    return api.squareview(
      'POST',
      payload.apiReference,
      {
        locale: payload.locale
      },
      payload.data
    );
  },
  addCollectionElementToContent({commit, getters}, payload) {
    let content = getters.getCurrentContent;
    let collection = unobserve(content[payload.collection]);
    collection.push(payload.element);
    commit('updateCurrentContent', {
      path: [payload.collection],
      content: content,
      value: collection,
      updateStrategy: 'replace'
    });
  },
  updateCollectionElement({state, commit, getters, dispatch}, payload) {
    let content = getters.getCurrentContent;
    //Mark as deleting
    let collectionName = payload.path.shift();
    let elementIndex = payload.path.shift();
    //Update through API
    api.squareview(
      'PUT',
      payload.apiReference,
      {id: payload.id},
      payload.value
    ).then((response) => {
      //Update in store
      commit('updateCurrentContent', {
        path: [collectionName, elementIndex],
        type: 'collection',
        content: content,
        value: response.data,
        updateStrategy: 'replace'
      });
      //Update hashes
      dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
    });
  },
  deleteCollectionElement({state, commit, getters, dispatch}, payload) {
    let content = getters.getCurrentContent;
    //Mark as deleting
    let collectionName = payload.path.shift();
    let elementIndex = payload.path.shift();
    let collection = unobserve(content[collectionName]);
    collection[elementIndex].deleting = true;
    commit('updateCurrentContent', {
      path: [collectionName],
      type: 'collection',
      content: content,
      value: collection,
      updateStrategy: 'replace'
    });
    //Delete through API
    api.squareview(
      'DELETE',
      payload.apiReference,
      {id: payload.id}
    ).then(() => {
      //Remove from store
      collection = unobserve(content[collectionName]); //get again in case other updates were made
      _.remove(collection, (element) => {
        return element.id === payload.id;
      });
      commit('updateCurrentContent', {
        path: [collectionName],
        type: 'collection',
        content: content,
        value: collection,
        updateStrategy: 'replace'
      });
      //Update hashes
      dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
    });
  },
  orderCollection({state, commit, getters, dispatch}, payload) {
    let collectionName = payload.path.pop();
    let content = getters.getCurrentContent;
    let collection = unobserve(content[collectionName]);
    let otherElements = unobserve(collection);

    //Reorder in store
    let reorderedCollection = [];
    _.forEach(payload.value, (elementId, position) => {
      let elementIndex = _.findIndex(collection, (e) => {
        return e.id === elementId;
      });
      if (elementIndex !== -1) {
        let reorderedElement = collection[elementIndex];
        reorderedElement.position = position + 1; //start at 1, not 0
        reorderedCollection.push(reorderedElement);
        _.remove(otherElements, (e) => {
          return e.id === elementId;
        });
      } else {
        console.error('Could not find collection with id', elementId);
      }
    });

    reorderedCollection = [...reorderedCollection, ...otherElements];

    commit('updateCurrentContent', {
      path: [collectionName],
      type: 'collection',
      content: content,
      value: reorderedCollection,
      updateStrategy: 'replace'
    });

    let value = {};
    value[collectionName] = payload.value;
    api.squareview(
      'PUT',
      payload.apiReference,
      {locale: payload.locale},
      value
    ).then(() => {
      //Update hashes
      dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
    });
  },
}
