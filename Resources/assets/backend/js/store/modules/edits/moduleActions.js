import api from "sv/js/api";
import {unobserve} from "sv/js/util/unobserver";
import _ from "lodash";

export default {
  createModule({state, getters}, payload) {
    let content = getters.getCurrentContent;
    return api.squareview(
      'POST',
      'modules_create',
      {
        locale: payload.locale,
        discriminator: content.discriminator,
        id: content.id
      },
      payload.data
    );
  },
  deleteModule({state, getters}, payload) {
    return api.squareview(
      'DELETE',
      'modules_delete',
      {
        id: payload
      }
    );
  },
  addModuleToContent({commit, getters}, payload) {
    let content = getters.getCurrentContent;
    let modules = unobserve(content.modules);
    modules.push(payload);
    commit('updateCurrentContent', {
      path: ['modules'],
      content: content,
      value: modules,
      updateStrategy: 'replace'
    });
  },
  removeModuleFromContent({commit, dispatch, getters}, payload) {
    let content = getters.getCurrentContent;
    let modules = unobserve(content.modules);
    _.remove(modules, (module) => {
      return module.id === payload.moduleId;
    });
    commit('updateCurrentContent', {
      path: ['modules'],
      content: content,
      value: modules,
      updateStrategy: 'replace'
    });
    //Update hashes
    dispatch('forceDraftHashUpdate', {locale: payload.locale, id: content.id});
  },
  orderModules({state, commit, getters}, payload) {
    let content = getters.getCurrentContent;
    let modules = unobserve(content.modules);
    let otherModules = unobserve(modules);

    //Reorder in store
    let reorderedModules = [];
    _.forEach(payload.value.modules, (moduleId, position) => {
      let moduleIndex = _.findIndex(modules, (m) => {
        return m.id === moduleId;
      });
      if (moduleIndex !== -1) {
        let reorderedModule = modules[moduleIndex];
        reorderedModule.position = position + 1; //start at 1, not 0
        reorderedModules.push(reorderedModule);
        _.remove(otherModules, (m) => {
          return m.id === moduleId;
        });
      } else {
        console.error('Could not find module with id', moduleId);
      }
    });

    reorderedModules = [...reorderedModules, ...otherModules];

    commit('updateCurrentContent', {
      path: ['modules'],
      content: content,
      value: reorderedModules,
      updateStrategy: 'replace'
    });

    payload.value.hash = getters.getCurrentContentHash;

    return api.squareview(
      'PUT',
      'modules_order',
      {
        discriminator: content.discriminator,
        resourceIdentifier: content.id,
        zone: payload.zone,
        locale: payload.locale
      },
      payload.value
    );
  },
  addModuleNewParameterIds({commit, getters}, payload) {
    let content = unobserve(getters.getCurrentContent);
    let modules = content.modules;
    if (modules !== undefined) {
      let moduleIndex = modules.findIndex(module => module.id === payload.moduleId);
      let module = modules[moduleIndex];
      let parameters = unobserve(module.parameters);
      _.each(payload.newParameters, (parameterId, parameterIdentifier) => {
        let parameterKey = _.findKey(parameters, (parameter) => {
          return parameter.identifier === parameterIdentifier;
        });
        parameters[parameterKey].id = parameterId;
      });
      commit('updateCurrentContent', {
        content,
        type: 'modules',
        path: [moduleIndex, 'parameters'],
        updateStrategy: 'replace',
        value: parameters
      });
    }
  },
}
