import api from '../../api';
import Vue from 'vue';
import objectHandler from "sv/js/util/objectHandler";
import _ from 'lodash';
import {unobserve} from "sv/js/util/unobserver";
import mediaActions from "sv/js/store/modules/edits/mediaActions";
import moduleActions from "sv/js/store/modules/edits/moduleActions";
import collectionActions from "sv/js/store/modules/edits/collectionActions";
import metaTagActions from "./edits/metaTagActions";

const state = {
  isLoading: false,
  editsLoaded: false,
  edits: [],
  newEdits: [],
  currentEdit: null,
  currentNewEdit: null,
  currentViolations: {},
  updateQueue: {},
  isNew: false,
  saving: false,
  publishing: false,
  hasErrors: false,
  lastErrorType: null,
};

// getters
const getters = {
  getEditConfigurationForPath: (state) => (path) => {
    let edit = state.edits.find((edit) => edit.path === path);
    if(!edit) {
      let newEdit = state.newEdits.find((newEdit) => newEdit.path === path);
      if(newEdit) {
        edit = state.edits.find((edit) => edit.section === newEdit.section);
      }
    }
    return edit;
  },
  getEditByActionName: (state) => (name) => {
    let edit = state.edits.find((edit) => edit.name === name);
    return edit !== undefined ? edit : null;
  },
  isEditContentLoadedByActionName: (state, getters) => (name) => {
    let edit = getters.getEditByActionName(name);
    return edit ? edit.loaded : false;
  },
  getCurrentContent: (state) => {
    return state.currentEdit && state.currentEdit.content !== undefined ? state.currentEdit.content : null;
  },
  getCurrentEditName: (state) => {
    return state.currentEdit !== null ? state.currentEdit.name : null;
  },
  getCurrentContentRepresentation: (state, getters) => {
    let currentContent = unobserve(getters.getCurrentContent);
    return _.omit(currentContent, [
      'enabled',
      'draftHashes',
      'versionHashes',
      'updatedAt',
      'createdAt',
      'routes'
    ]);
  },
  getCurrentContentHash: (state, getters) => {
    let currentContentRepresentation = getters.getCurrentContentRepresentation;
    return objectHandler.generateObjectHash(currentContentRepresentation);
  },
  isCurrentContentCreated: (state, getters) => {
    let currentContent = getters.getCurrentContent;
    return currentContent !== null && currentContent.id !== undefined && currentContent.id !== null;
  },
  alreadyInUpdateQueue: (state) => (hash) => {
    return state.updateQueue[hash] !== undefined ? state.updateQueue[hash] : false;
  },
  hasQueuedUpdates: (state) => {
    return !_.isEmpty(state.updateQueue);
  },
  getUpdateQueuedByStatus: (state, getters) => (status) => {
    return _.pickBy(state.updateQueue,(queued) => {
      return queued.updatePayload.edit === getters.getCurrentEditName && queued.status === status;
    });
  },
  isAutoSaving: (state, getters) => {
    let currentContent = getters.getCurrentContent;
    return currentContent !== null && currentContent !== undefined && currentContent.id !== undefined && currentContent.id !== null;
  },
  getApiReferenceByActionType: (state) => (action) => {
    if(action === 'new') {
      return !_.isNil(state.currentNewEdit) && !_.isNil(state.currentNewEdit.apis) && !_.isNil(state.currentNewEdit.apis.new) ? state.currentNewEdit.apis.new.reference.replace('.', '_') : null;
    } else if(action === 'edit') {
        return !_.isNil(state.currentEdit) && !_.isNil(state.currentEdit.apis) && !_.isNil(state.currentEdit.apis.edit) ? state.currentEdit.apis.edit.reference.replace('.', '_') : null;
    }  else if(action === 'show') {
      return !_.isNil(state.currentEdit) && !_.isNil(state.currentEdit.apis) && !_.isNil(state.currentEdit.apis.show) ? state.currentEdit.apis.show.reference.replace('.', '_') : null;
    }
    console.error('No apiReference found for action type "' + action + '".');
  },
  getFieldViolation: (state) => (field) => {
    return state.currentViolations[field] ?? null;
  }
};

// actions
const actions = {
  ...moduleActions,
  ...mediaActions,
  ...collectionActions,
  ...metaTagActions,
  fetch({commit}) {
    commit('isLoading', true);
    api.get('sections').then((response) => {
      let edits = [];
      let newEdits = [];

      for (let section of response.data) {
        let features = section.features;
        let actions = section.actions;
        for (let action of actions) {
          action.features = features;
          let actionType = action.type.split('#')[0];
          if (actionType === 'edit') {
            // Add delete api if section has delete action
            const hasDeleteAction = actions.find((action) => action.type === 'delete');
            if(hasDeleteAction) {
              action.apis['delete'] = hasDeleteAction.apis.delete;
            }
            edits.push(action);
          } else if (actionType === 'new') {
            newEdits.push(action);
          }
        }
      }

      commit('edits', edits);
      commit('newEdits', newEdits);
    });
  },
  checkAndMarkIfNew({commit}, payload) {
    let newEdit = state.newEdits.find((newEdit) => newEdit.path === payload);
    let hasNew = !_.isNil(newEdit);
    commit('isNew', hasNew);
    if (hasNew) {
      commit('currentNewEdit', newEdit);
    }
  },
  loadNewEditContent({commit, getters}, payload) {
    let edit = getters.getEditByActionName(payload.actionName);
    commit('newEditContent', {edit, content: payload.content});
  },
  loadEditContent({dispatch, commit, getters}, payload) {
    commit('isLoading', true);
    return api.squareview(
      'GET',
      payload.apiReference,
      payload.query
    ).then((response) => {
      let edit = getters.getEditByActionName(payload.actionName);
      commit('currentEdit', edit);
      dispatch('loadNewForEdit', edit);
      commit('editContent', {'edit': edit, 'content': response.data, 'node': payload.apiName});
    });
  },
  switchCurrentEdit({dispatch, commit, getters}, edit) {
    if (edit && edit.loaded) {
      commit('currentEdit', edit);
      dispatch('loadNewForEdit', edit);
    }
  },
  loadNewForEdit({commit, state}, payload) {
    let newEdit = state.newEdits.find((newEdit) => newEdit.section === payload.section);
    if (newEdit) {
      commit('currentNewEdit', newEdit);
    }
  },
  updateCurrentContent({commit, state, getters}, payload) {
    payload.content = getters.getCurrentContent;
    payload.id = payload.id ?? payload.content.id;
    if (!state.isNew) {
      commit('updateCurrentContent', payload);
    }

    let hash = getters.getCurrentContentHash;

    let updatedValue = unobserve(payload.value);
    if (payload.mergeStrategy !== 'skip') {
      updatedValue.hash = hash;
    }

    payload.query.locale = payload.locale;
    if (payload.id) {
      payload.query.id = payload.id;
    }

    return state.isNew ?
      api.squareview(
        'POST',
        payload.apiReference,
        payload.query,
        updatedValue
      ) :
      api.squareview(
        'PUT',
        payload.apiReference,
        payload.query,
        updatedValue
      );
  },
  publishCurrentContent({getters}, payload) {
    let content = getters.getCurrentContent;
    return api.put(
      'publish',
      {
        locale: payload.locale,
        discriminator: content.discriminator,
        id: content.id
      }
    );
  },
  updateVersionHashes({commit, getters}, payload) {
    let content = getters.getCurrentContent;
    commit('updateCurrentContent', {
      path: ['versionHashes', payload.locale],
      content: content,
      value: payload.version,
      updateStrategy: 'replace'
    });
  },
  updateDraftHashes({commit, getters}, payload) {
    let content = getters.getCurrentContent;
    commit('updateCurrentContent', {
      path: ['draftHashes', payload.locale],
      content: content,
      value: payload.version,
      updateStrategy: 'replace'
    });
  },
  deleteContent({state}, payload) {
    return api.squareview(
      'DELETE',
      payload.apiReference,
      {
        id: payload.id
      }
    );
  },
  replaceNewContentWithCreatedContent({commit, dispatch}, payload) {
    commit('replaceContent', payload.content);
    dispatch('forceDraftHashUpdate', {locale: payload.locale, id: payload.content.id});
  },
  handleUpdatedResourceGuideTranslation({commit, dispatch, getters}, payload) {
    let content = getters.getCurrentContent;
    commit('updateCurrentContent', {
      path: ['translations', payload.zone, payload.key, 'message'],
      content,
      value: payload.message,
      updateStrategy: 'replace'
    });

    dispatch('forceDraftHashUpdate', {locale: payload.locale, id: payload.resourceId});
  },
  forceDraftHashUpdate({dispatch, state, getters}, payload) {
    let hash = getters.getCurrentContentHash;
    dispatch('isSaving', true);
    api.squareview(
      'PUT',
      getters.getApiReferenceByActionType('edit'),
      payload,
      {hash}
    ).then((response) => {
      if(_.has(response.data, payload.locale)) {
        dispatch('updateDraftHashes', {
          version: response.data[payload.locale],
          locale: payload.locale
        });
      }
      dispatch('isSaving', false);
    });
  },
  isSaving({commit}, payload) {
    commit('saving', payload);
  },
  isPublishing({commit}, payload) {
    commit('publishing', payload);
  },
  showHasErrors({commit, dispatch}, {duration = null, type = null, error = null}) {
    commit('hasErrors', true);
    commit('lastErrorType', type);
    if(error && error.violations !== undefined) {
      commit('currentViolations', error.violations);
    } else {
      dispatch('clearViolations');
    }

    if(duration) {
      setTimeout(() => {
        commit('hasErrors', false);
      }, duration);
    }
  },
  clearViolations({commit}) {
    commit('currentViolations', {});
  },
  removeViolation({commit, getters}, identifier) {
    if(getters.getFieldViolation(identifier)) {
      commit('currentViolations', _.omit(state.currentViolations, identifier));
    }
  }
};

// mutations
const mutations = {
  edits(state, payload) {
    for (let index in payload) {
      payload[index].content = null;
      payload[index].loaded = false;
    }
    state.edits = payload;
    state.editsLoaded = true;
  },
  newEdits(state, payload) {
    state.newEdits = payload;
  },
  isNew(state, payload) {
    state.isNew = payload;
  },
  editContent(state, payload) {
    //let index = state.edits.indexOf(payload.edit);
    let edit = payload.edit;

    if(!edit.content){
      edit.content = {};
    }

    if(payload.node !== 'properties'){
      Vue.set(edit.content, payload.node, payload.content);
    } else {
      edit.content = Object.assign({}, edit.content, payload.content);
    }

    edit.loaded = true;

    state.isLoading = false;
  },
  newEditContent (state, payload) {
    let edit = payload.edit;
    edit.loaded = true;
    edit.content = payload.content;
    state.currentEdit = edit;
    state.isLoading = false;
  },
  currentEdit(state, payload) {
    state.currentEdit = payload;
  },
  currentNewEdit(state, payload) {
    state.currentNewEdit = payload;
  },
  updateCurrentContent(state, payload) {
    let newContent = null;
    let path = payload.path ?? [];
    if (payload.type && payload.type !== 'properties' && payload.type !== 'collection') { //Add api name to path
      path.unshift(payload.type);
    }
    if(payload.updateStrategy === 'merge'){
      path.pop(); //Remove last part of path
      newContent = objectHandler.setDeep(payload.content, path, payload.value, false, true);
    } else if(payload.updateStrategy === 'replace'){
      newContent = objectHandler.setDeep(payload.content, path, payload.value);
    }
    let newEdit = Object.assign({}, state.currentEdit, {content: newContent});

    Vue.set(state, 'currentEdit', newEdit);
  },
  replaceContent(state, payload) {
    let newEdit = Object.assign({}, state.currentEdit, {content: payload});
    Vue.set(state, 'currentEdit', newEdit);
  },
  saving(state, payload) {
    state.saving = payload;
  },
  publishing(state, payload) {
    state.publishing = payload;
  },
  addToUpdateQueue(state, payload) {
    Vue.set(state.updateQueue, payload.hash, payload.data);
  },
  removeFromUpdateQueue(state, payload) {
    Vue.delete(state.updateQueue, payload.hash);
  },
  emptyUpdateQueue(state) {
    state.updateQueue = {};
  },
  hasErrors(state, payload) {
    state.hasErrors = payload;
  },
  lastErrorType(state, payload) {
    state.lastErrorType = payload;
  },
  currentViolations(state, payload) {
    state.currentViolations = payload;
  },
  isLoading(state, payload) {
    state.isLoading = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
