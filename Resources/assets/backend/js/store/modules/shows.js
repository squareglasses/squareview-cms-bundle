import api from '../../api';
import Vue from 'vue';
import objectHandler from "sv/js/util/objectHandler";
import _ from 'lodash';
import {unobserve} from "sv/js/util/unobserver";

const state = {
  isLoading: false,
  showsLoaded: false,
  shows: [],
  currentShow: null
};

// getters
const getters = {
  getShowConfigurationForPath: (state) => (path) => {
    let show = state.shows.find((show) => show.path === path);
    return show;
  },
  getShowByActionName: (state) => (name) => {
    let show = state.shows.find((show) => show.name === name);
    return show !== undefined ? show : null;
  },
  isShowContentLoadedByActionName: (state, getters) => (name) => {
    let show = getters.getShowByActionName(name);
    return show ? show.loaded : false;
  },
  getCurrentContent: (state) => {
    return state.currentShow && state.currentShow.content !== undefined ? state.currentShow.content : null;
  },
  getCurrentShowName: (state) => {
    return state.currentShow !== null ? state.currentShow.name : null;
  },
  getApiReferenceByActionType: (state) => (action) => {
    return !_.isNil(state.currentShow) && !_.isNil(state.currentShow.apis) && !_.isNil(state.currentShow.apis.show) ? state.currentShow.apis.show.reference.replace('.', '_') : null;
  },
};

// actions
const actions = {
  fetch({commit}) {
    commit('isLoading', true);
    api.get('sections').then((response) => {
      let shows = [];

      for (let section of response.data) {
        let features = section.features;
        let actions = section.actions;
        for (let action of actions) {
          action.features = features;
          let actionType = action.type.split('#')[0];
          shows.push(action);
        }
      }

      commit('shows', shows);
    });
  },
  loadShowContent({dispatch, commit, getters}, payload) {
    commit('isLoading', true);
    return api.squareview(
      'GET',
      payload.apiReference,
      payload.query
    ).then((response) => {
      let show = getters.getShowByActionName(payload.actionName);
      commit('currentShow', show);
      commit('showContent', {'show': show, 'content': response.data, 'node': payload.apiName});
    });
  },
  switchCurrentShow({dispatch, commit, getters}, show) {
    if (show && show.loaded) {
      commit('currentShow', show);
      dispatch('loadNewForShow', show);
    }
  }
};

// mutations
const mutations = {
  shows(state, payload) {
    for (let index in payload) {
      payload[index].content = null;
      payload[index].loaded = false;
    }
    state.shows = payload;
    state.showsLoaded = true;
  },
  showContent(state, payload) {
    //let index = state.shows.indexOf(payload.show);
    let show = payload.show;

    if(!show.content){
      show.content = {};
    }

    if(payload.node !== 'properties'){
      Vue.set(show.content, payload.node, payload.content);
    } else {
      show.content = Object.assign({}, show.content, payload.content);
    }

    show.loaded = true;

    state.isLoading = false;
  },
  currentShow(state, payload) {
    state.currentShow = payload;
  },
  isLoading(state, payload) {
    state.isLoading = payload;
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
};
