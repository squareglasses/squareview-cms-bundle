import Vue from 'vue';
import Vuex from 'vuex';

import navigation from './modules/navigation';
import languages from './modules/languages';
import lists from './modules/lists';
import edits from './modules/edits';
import shows from './modules/shows';
import general from './modules/general';
import media from './modules/media';
import fields from './modules/fields';
import translations from "./modules/translations";

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    navigation,
    languages,
    lists,
    edits,
    shows,
    general,
    media,
    fields,
    translations
  },
  strict: debug
});
