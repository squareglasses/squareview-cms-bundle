<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Exception;

use Throwable;

/**
 * Class HttpClientResourceNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HttpClientResourceNotFoundException extends HttpClientException
{
    /**
     * @param string|null     $message
     * @param Throwable|null $previous
     * @param int             $code
     * @param array           $headers
     */
    public function __construct(string $message = null, Throwable $previous = null, int $code = 404, array $headers = [])
    {
        parent::__construct($message, $previous, $code, $headers);
    }
}
