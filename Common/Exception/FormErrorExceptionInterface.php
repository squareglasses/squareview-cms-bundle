<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Exception;

/**
 * Interface FormErrorExceptionInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface FormErrorExceptionInterface
{
    /**
     * @return array
     */
    public function getViolations(): array;

    /**
     * @return string
     */
    public function getMessage(): string;
}
