<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Form\Util;

use SG\CmsBundle\Common\Exception\FormErrorExceptionInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;

/**
 * Class FormErrorParser
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FormErrorParser
{
    /**
     * @param FormInterface               $form
     * @param FormErrorExceptionInterface $exception
     *
     * @return FormInterface
     */
    public static function parseFormErrors(FormInterface $form, FormErrorExceptionInterface $exception): FormInterface
    {
        if (count($exception->getViolations()) > 0) { //This is a assert field error
            // @TODO : Make it recursive, only 2 levels managed now
            foreach ($exception->getViolations() as $propertyPath => $message) {
                $error = new FormError($message);
                if (str_contains($propertyPath, ".")) {
                    $properties = explode('.', $propertyPath);
                    if ($form->has($properties[0])) {
                        $form->get($properties[0])->get($properties[1])->addError($error);
                    } else {
                        $form->addError($error);
                    }
                } elseif ($form->has($propertyPath)) {
                    $form->get($propertyPath)->addError($error);
                } else {
                    $form->addError($error);
                }
            }
        } else { // This is an unknown field error
            $form->addError(new FormError($exception->getMessage()));
        }
        return $form;
    }
}
