<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

/**
 * Interface ResourceGuideInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ResourceGuideInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self;

    /**
     * @return string|null
     */
    public function getController(): ?string;

    /**
     * @param string $controller
     *
     * @return $this
     */
    public function setController(string $controller): self;

    /**
     * @return string|null
     */
    public function getAction(): ?string;

    /**
     * @param string $action
     *
     * @return $this
     */
    public function setAction(string $action): self;

    /**
     * @return string
     */
    public function getView(): string;

    /**
     * @param string $view
     *
     * @return $this
     */
    public function setView(string $view): self;

    /**
     * @return string|null
     */
    public function getTranslationDomain(): ?string;

    /**
     * @param string $translationDomain
     *
     * @return $this
     */
    public function setTranslationDomain(string $translationDomain): self;

    /**
     * @return bool
     */
    public function isSitemap(): bool;

    /**
     * @param bool $sitemap
     *
     * @return $this
     */
    public function setSitemap(bool $sitemap): self;

    /**
     * @return string|null
     */
    public function getSitemapFrequency(): ?string;

    /**
     * @param string|null $sitemapFrequency
     *
     * @return $this
     */
    public function setSitemapFrequency(?string $sitemapFrequency): self;

    /**
     * @return float|null
     */
    public function getSitemapPriority(): ?float;

    /**
     * @param float|null $sitemapPriority
     *
     * @return $this
     */
    public function setSitemapPriority(?float $sitemapPriority): self;

    /**
     * @return array
     */
    public function getZones(): array;

    /**
     * @param array $zones
     *
     * @return $this
     */
    public function setZones(array $zones): self;

    /**
     * @param string $zoneName
     *
     * @return ZoneInterface
     */
    public function getZone(string $zoneName): ZoneInterface;
}
