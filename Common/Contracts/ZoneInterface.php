<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

use SG\CmsBundle\Frontend\Contracts\ModuleInterface;

/**
 * Interface ZoneInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ZoneInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @return string|null
     */
    public function getType(): ?string;

    /**
     * @param string $type
     */
    public function setType(string $type): void;

    /**
     * @return int|null
     */
    public function getPosition(): ?int;

    /**
     * @param int $position
     */
    public function setPosition(int $position): void;

    /**
     * @return array
     */
    public function getModules(): array;

    /**
     * @param ModuleInterface $module
     */
    public function addModule(ModuleInterface $module): void;
}
