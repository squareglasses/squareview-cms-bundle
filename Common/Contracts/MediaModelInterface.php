<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

/**
 * Interface MediaModelInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface MediaModelInterface
{
    /**
     * @return string|null
     */
    public function getIdentifier(): ?string;

    /**
     * @return string
     */
    public function getFileName(): string;
}
