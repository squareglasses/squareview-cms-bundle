<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Entity\AbstractContent;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait EnableableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait EnableableTrait
{
    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @Groups({"get", "put"})
     */
    protected bool $enabled = false;

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return EnableableTrait|AbstractContent
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }
}
