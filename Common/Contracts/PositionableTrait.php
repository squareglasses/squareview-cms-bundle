<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Entity\AbstractContent;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait PositionableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait PositionableTrait
{
    /**
     * @ORM\Column(name="position", type="integer", nullable=true)
     * @Groups({"get", "post", "put", "publish", "parents"})
     */
    protected ?int $position = 0;

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return PositionableTrait|AbstractContent
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }
}
