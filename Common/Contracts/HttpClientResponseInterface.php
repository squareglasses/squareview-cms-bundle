<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

/**
 * Interface HttpClientResponseInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface HttpClientResponseInterface
{
    /**
     * @return int
     */
    public function getStatusCode(): int;

    /**
     * @param int $statusCode
     *
     * @return $this
     */
    public function setStatusCode(int $statusCode): self;

    /**
     * @return array
     */
    public function getInfo(): array;

    /**
     * @param array $info
     *
     * @return $this
     */
    public function setInfo(array $info): self;

    /**
     * @return array
     */
    public function getHeaders(): array;

    /**
     * @param array $headers
     *
     * @return $this
     */
    public function setHeaders(array $headers): self;

    /**
     * @return string|null
     */
    public function getContent(): ?string;

    /**
     * @return array|null
     */
    public function toArray(): ?array;

    /**
     * @param string|null $content
     *
     * @return $this
     */
    public function setContent(?string $content): self;

    /**
     * @return bool
     */
    public function isCached(): bool;

    /**
     * @param bool $cached
     *
     * @return $this
     */
    public function setCached(bool $cached): self;

    /**
     * @return array
     */
    public function getTags(): array;

    /**
     * @param array $tags
     */
    public function setTags(array $tags): void;
}
