<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Frontend\Model\Gallery;

/**
 * Trait HasGalleriesTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait HasGalleriesTrait
{
    /**
     * @return Collection<MediaModelInterface>
     */
    public function getGalleries(): Collection
    {
        return $this->galleries;
    }

    /**
     * @param Gallery $gallery
     */
    public function addGallery(Gallery $gallery): void
    {
        if (!$this->galleries->contains($gallery)) {
            $this->galleries[] = $gallery;
        }
    }

    /**
     * @param Gallery $gallery
     */
    public function removeGallery(Gallery $gallery): void
    {
        if ($this->galleries->contains($gallery)) {
            $this->galleries->removeElement($gallery);
        }
    }

    /**
     * @param string $identifier
     *
     * @return Gallery|null
     */
    public function getGalleryByIdentifier(string $identifier): ?Gallery
    {
        foreach ($this->galleries as $gallery) {
            if ($gallery->getIdentifier() === $identifier) {
                return $gallery;
            }
        }
        return null;
    }
}
