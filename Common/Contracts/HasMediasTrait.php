<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Common\Media\Model\Media;

/**
 * Trait HasMediasTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait HasMediasTrait
{
    /**
     * @return Collection<MediaModelInterface>
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    /**
     * @param Media $media
     */
    public function addMedia(Media $media): void
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
        }
    }

    /**
     * @param Media $media
     */
    public function removeMedia(Media $media): void
    {
        if ($this->medias->contains($media)) {
            $this->medias->removeElement($media);
        }
    }

    /**
     * @param string $identifier
     *
     * @return MediaModelInterface|null
     */
    public function getMediaByIdentifier(string $identifier): ?MediaModelInterface
    {
        foreach ($this->medias as $media) {
            if ($media->getIdentifier() === $identifier) {
                return $media;
            }
        }
        return null;
    }
}
