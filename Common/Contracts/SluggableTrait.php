<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Contracts;

/**
 * Trait SluggableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait SluggableTrait
{
    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     *
     * @return self
     */
    public function setSlug(?string $slug = null): self
    {
        $this->slug = $slug;

        return $this;
    }
}
