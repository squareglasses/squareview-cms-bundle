<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Bag;

/**
 * Trait ApiResourceBagTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait ApiResourceBagTrait
{
    /**
     * @return array<string>
     */
    public function getClasses(): array
    {
        return $this->bundleConfiguration['api']['classes'];
    }

    /**
     * @return string
     */
    public function getContentClass(): string
    {
        return $this->bundleConfiguration['api']['classes']['content'];
    }

    /**
     * @return string
     */
    public function getContentVersionClass(): string
    {
        return $this->bundleConfiguration['api']['classes']['content_version'];
    }

    /**
     * @return string
     */
    public function getWebsiteClass(): string
    {
        return $this->bundleConfiguration['api']['classes']['website'];
    }

    /**
     * @return string
     */
    public function getContactClass(): string
    {
        return $this->bundleConfiguration['api']['classes']['contact'];
    }


    /**
     * @return string
     */
    public function getLanguageClass(): string
    {
        return $this->bundleConfiguration['api']['classes']['language'];
    }

    /**
     * @param string $discriminator
     *
     * @return string|null
     */
    public function getResourceClassByDiscriminator(string $discriminator): ?string
    {
        return array_key_exists($discriminator, $this->bundleConfiguration['api']['discriminators']) ?
            $this->bundleConfiguration['api']['discriminators'][$discriminator]['class'] :
            null;
    }
}
