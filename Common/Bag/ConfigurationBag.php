<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Bag;

/**
 * Class ConfigurationBag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ConfigurationBag
{
    use FrontendResourceBagTrait;
    use ApiResourceBagTrait;

    /**
     * @param array $bundleConfiguration
     */
    public function __construct(private readonly array $bundleConfiguration = [])
    {
    }

    /**
     * @return string|null
     */
    public function getWebsiteIdentifier(): ?string
    {
        return $this->bundleConfiguration['website_identifier'];
    }

    /**
     * @return string|null
     */
    public function getWebsiteName(): ?string
    {
        return $this->bundleConfiguration['website_name'];
    }

    /**
     * @return string
     */
    public function getDefaultLocale(): string
    {
        return $this->bundleConfiguration['locale'];
    }

    /**
     * @return array
     */
    public function getAllowedLocales(): array
    {
        return $this->bundleConfiguration['allowed_locales'];
    }

    /**
     * @return bool
     */
    public function isMultilingual(): bool
    {
        return $this->bundleConfiguration['multilingual'];
    }
}
