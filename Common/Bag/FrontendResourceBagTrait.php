<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Bag;

/**
 * Trait FrontendResourceBagTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait FrontendResourceBagTrait
{
    /**
     * @return string
     */
    public function getSecurityUserClass(): string
    {
        return $this->bundleConfiguration['frontend']['security']['user_class'];
    }

    /**
     * @return string
     */
    public function getSecurityMailerFromAddress(): string
    {
        return $this->bundleConfiguration['frontend']['security']['mailer']['from'] ?: $this->bundleConfiguration['mailer']['from'];
    }

    /**
     * @return int
     */
    public function getSecurityResettingRetryTtl(): int
    {
        return $this->bundleConfiguration['frontend']['security']['resetting']['retryTtl'];
    }

    /**
     * @return string
     */
    public function getRegistrationFormClass(): string
    {
        return $this->bundleConfiguration['frontend']['security']['registration']['form'];
    }

    /**
     * @return bool
     */
    public function getRegistrationUseUsername(): bool
    {
        return $this->bundleConfiguration['frontend']['security']['registration']['use_username'];
    }

    /**
     * @return string
     */
    public function getProfileRoute(): string
    {
        return $this->bundleConfiguration['frontend']['security']['profile']['route'];
    }

    /**
     * @return string
     */
    public function getProfileEditRoute(): string
    {
        return $this->bundleConfiguration['frontend']['security']['profile']['edit']['route'];
    }

    /**
     * @return string
     */
    public function getProfileEditFormClass(): string
    {
        return $this->bundleConfiguration['frontend']['security']['profile']['edit']['form'];
    }

    /**
     * @return string
     */
    public function getProfilePersonalInfosFormClass(): string
    {
        return $this->bundleConfiguration['frontend']['security']['profile']['edit']['personal_infos_form'];
    }

    /**
     * @return string
     */
    public function getProfileChangePasswordFormClass(): string
    {
        return $this->bundleConfiguration['frontend']['security']['profile']['change_password']['form'];
    }

    /**
     * @return string
     */
    public function getContactFromAddress(): string
    {
        return $this->bundleConfiguration['frontend']['contact']['from_address'];
    }

    /**
     * @return array
     */
    public function getContactToAddresses(): array
    {
        return $this->bundleConfiguration['frontend']['contact']['to_addresses'];
    }

    /**
     * @return array
     */
    public function getContactCcAddresses(): array
    {
        return $this->bundleConfiguration['frontend']['contact']['cc_addresses'];
    }

    /**
     * @return array
     */
    public function getContactBccAddresses(): array
    {
        return $this->bundleConfiguration['frontend']['contact']['bcc_addresses'];
    }

    /**
     * @return string
     */
    public function getGalleriesDefaultView(): string
    {
        return $this->bundleConfiguration['medias']['galleries_default_view'];
    }

    /**
     * @param string $galleryIdentifier
     *
     * @return array|null
     */
    public function getResourceGalleryConfiguration(string $galleryIdentifier): ?array
    {
        return $this->bundleConfiguration['medias']['galleries'][$galleryIdentifier] ?? null;
    }

    /**
     * @param string $parameterName
     *
     * @return mixed|null
     */
    public function getPaginationConfiguration(string $parameterName): mixed
    {
        return $this->bundleConfiguration['frontend']['pagination'][$parameterName] ?? null;
    }

    /**
     * @return mixed
     */
    public function getBreadcrumbsConfiguration(): mixed
    {
        return $this->bundleConfiguration['frontend']['breadcrumbs'] ?? null;
    }

    /**
     * @return mixed
     */
    public function getLocaleGuessingOrder(): array
    {
        return $this->bundleConfiguration['frontend']['locale_matcher']["guessing_order"] ?? [];
    }
}
