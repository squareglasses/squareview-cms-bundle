<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Mailer\Transport;

use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mailer\SentMessage;
use Symfony\Component\Mailer\Transport\Smtp\SmtpTransport;
use Symfony\Component\Mailer\Transport\Smtp\Stream\AbstractStream;
use Symfony\Component\Mailer\Transport\SendmailTransport as BaseTransport;
use Symfony\Component\Mailer\Transport\Smtp\Stream\ProcessStream;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Class SendmailTransport
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SendmailTransport extends BaseTransport
{
    protected ProcessStream $stream;
    protected SmtpTransport $transport;

    /**
     * If using -t mode you are strongly advised to include -oi or -i in the flags.
     * For example: /usr/sbin/sendmail -oi -t
     * -f<sender> flag will be appended automatically if one is not present.
     *
     * The recommended mode is "-bs" since it is interactive and failure notifications are hence possible.
     *
     * @param string                        $command
     * @param EventDispatcherInterface|null $dispatcher
     * @param LoggerInterface|null          $logger
     */
    public function __construct(private readonly string $command = '/usr/sbin/sendmail -bs', private readonly ?EventDispatcherInterface $dispatcher = null, private readonly ?LoggerInterface $logger = null)
    {
        parent::__construct($command, $dispatcher, $logger);

        if ((null !== $command) && !str_contains($command, ' -bs') && !str_contains($command, ' -t')) {
            throw new InvalidArgumentException(sprintf('Unsupported sendmail command flags "%s"; must be one of "-bs" or "-t" but can include additional flags.', $command));
        }

        $this->stream = new ProcessStream();
        if (str_contains($this->command, ' -bs')) {
            $this->stream->setCommand($this->command);
            $this->transport = new SmtpTransport($this->stream, $dispatcher, $logger);
        }
    }

    /**
     * @param SentMessage $message
     *
     * @return void
     */
    protected function doSend(SentMessage $message): void
    {
        $originalMessage = $message->getOriginalMessage();
        $envelope = $message->getEnvelope();

        foreach ($envelope->getRecipients() as $key => $recipient) {
            if ($key === 0) {
                $originalMessage->to($recipient->getAddress());
            } else {
                $originalMessage->addto($recipient->getAddress());
            }
        }

        $message = new SentMessage($originalMessage, $envelope);

        $this->getLogger()->debug(sprintf('Email transport "%s" starting', __CLASS__));

        $command = $this->command;
        if (!str_contains($command, ' -f')) {
            $command .= ' -f'.escapeshellarg($message->getEnvelope()->getSender()->getEncodedAddress());
        }

        $chunks = AbstractStream::replace("\r\n", "\n", $message->toIterable());

        if (!str_contains($command, ' -i') && !str_contains($command, ' -oi')) {
            $chunks = AbstractStream::replace("\n.", "\n..", $chunks);
        }

        $this->stream->setCommand($command);
        $this->stream->initialize();
        foreach ($chunks as $chunk) {
            $this->stream->write($chunk);
        }
        $this->stream->flush();
        $this->stream->terminate();

        $this->getLogger()->debug(sprintf('Email transport "%s" stopped', __CLASS__));
    }
}
