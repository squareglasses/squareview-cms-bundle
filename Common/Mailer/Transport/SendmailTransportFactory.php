<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Mailer\Transport;

use Symfony\Component\Mailer\Exception\TransportException;
use Symfony\Component\Mailer\Exception\UnsupportedSchemeException;
use Symfony\Component\Mailer\Transport\AbstractTransportFactory;
use Symfony\Component\Mailer\Transport\Dsn;
use Symfony\Component\Mailer\Transport\Smtp\SmtpTransport;
use Symfony\Component\Mailer\Transport\Smtp\Stream\SocketStream;
use Symfony\Component\Mailer\Transport\TransportInterface;
use function in_array;
use const DIRECTORY_SEPARATOR;

/**
 * Class SendmailTransportFactory
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class SendmailTransportFactory extends AbstractTransportFactory
{
    /**
     * @param Dsn $dsn
     *
     * @return TransportInterface
     */
    public function create(Dsn $dsn): TransportInterface
    {
        if (!in_array($dsn->getScheme(), $this->getSupportedSchemes(), true)) {
            throw new UnsupportedSchemeException($dsn, 'native+sendmail', $this->getSupportedSchemes());
        }

        if ($sendMailPath = ini_get('sendmail_path')) {
            return new SendmailTransport($sendMailPath, $this->dispatcher, $this->logger);
        }

        if ('\\' !== DIRECTORY_SEPARATOR) {
            throw new TransportException('sendmail_path is not configured in php.ini.');
        }

        // Only for windows hosts; at this point non-windows
        // host have already thrown an exception or returned a transport
        $host = ini_get('SMTP');
        $port = (int) ini_get('smtp_port');

        if (!$host || !$port) {
            throw new TransportException('smtp or smtp_port is not configured in php.ini.');
        }

        $socketStream = new SocketStream();
        $socketStream->setHost($host);
        $socketStream->setPort($port);
        if (465 !== $port) {
            $socketStream->disableTls();
        }

        return new SmtpTransport($socketStream, $this->dispatcher, $this->logger);
    }

    /**
     * @return string[]
     */
    protected function getSupportedSchemes(): array
    {
        return ['native+sendmail'];
    }
}
