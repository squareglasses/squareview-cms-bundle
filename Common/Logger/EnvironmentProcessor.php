<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Logger;

use Monolog\LogRecord;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class EnvironmentProcessor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class EnvironmentProcessor
{
    /**
     * @param RequestStack $requestStack
     */
    public function __construct(private readonly RequestStack $requestStack)
    {
    }

    /**
     * this method is called for each log record; optimize it to not hurt performance
     *
     * @param LogRecord|array $record
     *
     * @return LogRecord|array
     */
    public function __invoke(LogRecord|array $record): LogRecord|array
    {
        $request = $this->requestStack->getMainRequest();

        if ($request === null) {
            return $record;
        }

        $requested = $request->getSchemeAndHttpHost().$request->getRequestUri();

        $record['extra']['method'] = $request->getMethod();
        $record['extra']['requested'] = $requested;
        $record['extra']['origin'] = $request->headers->get('client_origin');

        return $record;
    }
}
