<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\HttpClient;

use Exception;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class FrontendApiClient
 * @package SG\CmsBundle\Common\HttpClient
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
class FrontendApiClient
{
    private HttpClientInterface $clientClient;
    /**
     */
    public function __construct(HttpClientInterface $clientClient)
    {
        $this->clientClient = $clientClient;
    }

    /**
     * @param array $options
     * @param string $method
     * @return string[]
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function generateHeaders(
        array $options,
        string $method
    ): array {
        $defaultHeaders =  [
            'Accept' => 'application/json',
            'Authorization' => 'Bearer '.$this->getApiAuthorization()
        ];
        if ($method === 'POST') {
            $defaultHeaders['Content-Type'] = 'application/json';
        }
        return array_key_exists('headers', $options) ?
            array_merge_recursive($defaultHeaders, $options['headers'])
            : $defaultHeaders;
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getApiAuthorization(): string
    {
        try {
            $response = $this->clientClient->request("POST", "api/authentication_token", [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'username' => $_ENV['FRONTEND_API_USER'],
                    "password" => $_ENV['FRONTEND_API_PASSWORD']
                ], JSON_THROW_ON_ERROR)
            ]);
            $result = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
            return $result['token'];
        } catch (Exception $e) {
            return 'unsecure';
        }
    }

    /**
     * @param string $method
     * @param string $endpoint
     * @param array $options
     * @return ResponseInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function request(
        string $method,
        string $endpoint,
        array $options = []
    ): ResponseInterface
    {
        $headers = array_key_exists('headers', $options) ? $options['headers'] : [];
        $options['headers'] = $this->generateHeaders($headers, $method);
        return $this->clientClient->request($method, $endpoint, $options);
    }
}
