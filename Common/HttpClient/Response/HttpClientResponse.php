<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\HttpClient\Response;

use JsonException;
use SG\CmsBundle\Common\Contracts\HttpClientResponseInterface;

/**
 * Class HttpClientResponse
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HttpClientResponse implements HttpClientResponseInterface
{
    private int $statusCode;
    private array $info = [];
    private array $headers = [];
    private array $tags = [];
    private ?string $content = null;
    private bool $cached = false;

    /**
     * @return int
     */
    public function getStatusCode(): int
    {
        return $this->statusCode;
    }

    /**
     * @param int $statusCode
     *
     * @return $this
     */
    public function setStatusCode(int $statusCode): self
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @return array
     */
    public function getInfo(): array
    {
        return $this->info;
    }

    /**
     * @param array $info
     *
     * @return $this
     */
    public function setInfo(array $info): self
    {
        $this->info = $info;

        return $this;
    }

    /**
     * @return array
     */
    public function getHeaders(): array
    {
        return $this->headers;
    }

    /**
     * @param array $headers
     *
     * @return $this
     */
    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @return array|null
     */
    public function toArray(): ?array
    {
        try {
            return json_decode($this->content, true, 512, JSON_THROW_ON_ERROR);
        } catch (JsonException) {
            return null;
        }
    }

    /**
     * @param string|null $content
     *
     * @return $this
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCached(): bool
    {
        return $this->cached;
    }

    /**
     * @param bool $cached
     *
     * @return $this
     */
    public function setCached(bool $cached): self
    {
        $this->cached = $cached;

        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }
}
