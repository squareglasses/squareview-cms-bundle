<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\HttpClient\Response;

use Exception;
use JsonException;
use SG\CmsBundle\Common\Contracts\HttpClientResponseInterface;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;

/**
 * Class ResponseParser
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResponseParser
{
    /**
     * @param HttpClientResponseInterface $response
     *
     * @return array
     * @throws JsonException
     * @throws Exception
     */
    public function parse(HttpClientResponseInterface $response): array
    {
        $items = [];
        if ($response->getStatusCode() !== 200) {
            throw $this->getException($response);
        }
        $responseArray = $response->toArray();
        if (array_key_exists('response', $responseArray)) { // Single result
            if ($responseArray['response'] === null) {
                throw $this->getException($response);
            }
            $items[] = $this->parseItem($responseArray['response']);
        } else { // array of results
            foreach ($responseArray as $member) {
                $items[] = $this->parseItem($member);
            }
        }
        return $items;
    }

    /**
     * @param HttpClientResponseInterface $response
     *
     * @return ResponseItem
     * @throws JsonException
     * @throws Exception
     */
    public function parseOne(HttpClientResponseInterface $response): ResponseItem
    {
        if ($response->getStatusCode() !== 200) {
            throw $this->getException($response);
        }
        return $this->parseItem($response->toArray());
    }

    /**
     * @param array $data
     *
     * @return ResponseItem
     */
    private function parseItem(array $data): ResponseItem
    {
        $item = new ResponseItem();
        foreach ($data as $key => $value) {
            $item->set($key, $value);
        }
        return $item;
    }

    /**
     * @param HttpClientResponseInterface $response
     *
     * @return Exception
     * @throws JsonException
     */
    private function getException(HttpClientResponseInterface $response): Exception
    {
        switch ($response->getStatusCode()) {
            case 404:
                $e = json_decode($response->getContent(false), true, 512, JSON_THROW_ON_ERROR);
                $descriptionProperty = 'hydra:description';
                if (isset($e->$descriptionProperty)) {
                    // @TODO Try to add trace to the exception
                    //http://www.inanzzz.com/index.php/post/xj81/handling-and-logging-exceptions-in-symfony-api-applications
                    $exception = new ResourceNotFoundException($e->$descriptionProperty);
                } else {
                    $exception = new ResourceNotFoundException($e->detail);
                }

            break;
            case 200:
                $exception = new ResourceNotFoundException("Resource not found.");
                break;
            default:
                $exception = new Exception("unknown exception", $response->getStatusCode());
                break;
        }
        return $exception;
    }
}
