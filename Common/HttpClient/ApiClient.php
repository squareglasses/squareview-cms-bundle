<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\HttpClient;

use JsonException;
use Psr\Cache\CacheException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Common\Cache\CacheKeyGenerator;
use SG\CmsBundle\Common\Contracts\HttpClientResponseInterface;
use SG\CmsBundle\Common\Exception\HttpClientException;
use SG\CmsBundle\Common\Exception\HttpClientResourceNotFoundException;
use SG\CmsBundle\Common\HttpClient\Response\HttpClientResponse;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\Cache\ItemInterface;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;
use Throwable;

/**
 * Class ApiClient
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiClient
{
    public const METHOD_GET     = 'GET';
    public const METHOD_POST    = 'POST';
    public const METHOD_PUT     = 'PUT';
    public const METHOD_DELETE  = 'DELETE';

    protected HttpClientInterface $client;
    protected ?TagAwareCacheInterface $apiCache = null;
    protected LoggerInterface $cacheLogger;

    /**
     * ApiClient constructor.
     * @param HttpClientInterface $apiClient
     * @param SerializerInterface $serializer
     * @param LoggerInterface $cacheTagsLogger
     * @param string|null $apiKey
     * @param string|null $applicationMode
     */
    public function __construct(
        protected  HttpClientInterface $apiClient,
        protected SerializerInterface $serializer,
        protected LoggerInterface $cacheTagsLogger,
        protected Security $security,
        protected ?string $apiKey,
        protected ?string $applicationMode = 'api'
    ) {
        $this->client           = $apiClient;
        $this->cacheLogger      = $cacheTagsLogger;
    }

    /**
     * @param string             $endpoint
     * @param string             $method
     * @param array              $options
     * @param bool               $apiUserRequest
     * @param UserInterface|null $user
     * @param bool               $useCachedResult
     * @param array              $tags
     * @param int|null           $expires
     *
     * @return HttpClientResponseInterface
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|HttpExceptionInterface
     */
    public function request(
        string $endpoint,
        string $method = self::METHOD_GET,
        array $options = [],
        bool $apiUserRequest = false,
        ?UserInterface $user = null,
        bool $useCachedResult = true,
        array $tags = [],
        ?int $expires = null
    ): HttpClientResponseInterface {
        if ($this->cachedRequestAuthorized($method, $useCachedResult)) {
            $serializedResponse = $this->apiCache->get(
                CacheKeyGenerator::generateKey($endpoint),
                function (ItemInterface $item) use ($endpoint, $options, $method, $apiUserRequest, $user, $tags, $expires) {
                    return $this->getSerializedCachedResponse(
                        $item,
                        $endpoint,
                        $method,
                        $options,
                        $apiUserRequest,
                        $user,
                        $tags,
                        $expires
                    );
                }
            );
        } else {
            $serializedResponse = $this->getSerializedResponse(
                $method,
                $endpoint,
                $options,
                $apiUserRequest,
                $user
            );
        }
        return $this->serializer->deserialize($serializedResponse, HttpClientResponse::class, 'json');
    }

    /**
     * Execute a request with api bearer token authentication headers
     *
     * @param string   $endpoint
     * @param string   $method
     * @param array    $options
     * @param bool     $useCachedResult
     * @param array    $tags
     * @param int|null $expires
     *
     * @return HttpClientResponseInterface
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|HttpExceptionInterface
     */
    public function apiRequest(
        string $endpoint,
        string $method = self::METHOD_GET,
        array $options = [],
        bool $useCachedResult = true,
        array $tags = [],
        ?int $expires = null
    ): HttpClientResponseInterface {
        return $this->request(
            $endpoint,
            $method,
            $options,
            true,
            null,
            $useCachedResult,
            $tags,
            $expires
        );
    }

    /**
     * Execute a request with user bearer token authentication headers
     *
     * @param UserInterface $user
     * @param string        $endpoint
     * @param string        $method
     * @param array         $options
     * @param bool          $useCachedResult
     * @param array         $tags
     * @param int|null      $expires
     *
     * @return HttpClientResponseInterface
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|HttpExceptionInterface
     */
    public function userRequest(
        UserInterface $user,
        string $endpoint,
        string $method = self::METHOD_GET,
        array $options = [],
        bool $useCachedResult = true,
        array $tags = [],
        ?int $expires = null
    ): HttpClientResponseInterface {
        if ($this->security->isGranted("IS_IMPERSONATOR")) {
            $endpoint.= str_contains($endpoint,"?") ? "&iuId=".$user->getId() : "?iuId=".$user->getId();
        }
        return $this->request(
            $endpoint,
            $method,
            $options,
            false,
            $user,
            $useCachedResult,
            $tags,
            $expires
        );
    }

    /**
     * Execute a http client request and return its json serialized representation
     *
     * @param string             $method
     * @param string             $endpoint
     * @param array              $options
     * @param bool               $apiUserRequest
     * @param UserInterface|null $user
     *
     * @return string
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getSerializedResponse(
        string $method,
        string $endpoint,
        array $options = [],
        bool $apiUserRequest = false,
        ?UserInterface $user = null
    ): string {
        try {
            $clientResponse = $this->client->request(
                $method,
                $endpoint,
                $this->parseOptions($options, $method, $apiUserRequest, $user)
            );

            if ($clientResponse->getStatusCode() < 200 || $clientResponse->getStatusCode() >= 300) {
                try {
                    $error = json_decode(
                        $clientResponse->getContent(false),
                        true,
                        512,
                        JSON_THROW_ON_ERROR
                    );
                } catch (JsonException $e) {
                    throw new JsonException('API Response JSON Syntax Error: '.$clientResponse->getContent(false));
                }
                if (array_key_exists("violations", $error)) {
                    throw new InvalidFormInputException(
                        $error['title'],
                        $error['violations'],
                        $clientResponse->getStatusCode()
                    );
                }
            }

            return $this->serializer->serialize(
                self::parseResponse($clientResponse),
                'json'
            );
        } catch (ClientExceptionInterface|RedirectionExceptionInterface
            |ServerExceptionInterface|TransportExceptionInterface $e) {
                throw self::createException(
                    $e->getCode() !== 0 ? $e->getCode() : 500,
                    $e->getMessage(),
                    $e,
                    isset($clientResponse) ? $clientResponse->getContent(false) : null
                );
            }
    }

    /**
     * Execute a http client request, apply cache options and return its json serialized representation
     *
     * @param ItemInterface      $item
     * @param string             $endpoint
     * @param string             $method
     * @param array              $options
     * @param bool               $apiUserRequest
     * @param UserInterface|null $user
     * @param array              $tags
     * @param int|null           $expires
     *
     * @return string
     * @throws CacheException
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     */
    protected function getSerializedCachedResponse(
        ItemInterface $item,
        string $endpoint,
        string $method = self::METHOD_GET,
        array $options = [],
        bool $apiUserRequest = false,
        ?UserInterface $user = null,
        array $tags = [],
        ?int $expires = null
    ): string {
        try {
            $cachedResponse = self::parseResponse($this->client->request(
                $method,
                $endpoint,
                $this->parseOptions($options, $method, $apiUserRequest, $user)
            ));

            // Add tags
            $tags = array_merge($tags, $cachedResponse->getTags());
            foreach ($tags as $tag) {
                if ($tag !== "") {
                    $item->tag($tag);
                }
            }
            $this->cacheLogger->info('getSerializedCachedResponse: tags='.implode(',', $tags));
            // Expiration
            if (null !== $expires) {
                $item->expiresAfter($expires);
            }
            return $this->serializer->serialize(
                $cachedResponse,
                'json'
            );
        } catch (ClientExceptionInterface|RedirectionExceptionInterface
        |ServerExceptionInterface|TransportExceptionInterface $e) {
            throw self::createException($e->getCode() !== 0 ? $e->getCode() : 500, $e->getMessage(), $e);
        }
    }

    /**
     * @param int $code
     * @param string|null $message
     * @param Throwable|null $previous
     * @param string|null $response
     * @param array $content
     * @return HttpExceptionInterface
     */
    public static function createException(
        int $code,
        ?string $message = null,
        Throwable $previous = null,
        ?string $response = null,
    ): HttpExceptionInterface {
        if ($code === Response::HTTP_NOT_FOUND) {
            $exception = new HttpClientResourceNotFoundException($message, $previous, $code);
        } elseif ($code === Response::HTTP_CONFLICT) {
            $exception = new ConflictHttpException($response, $previous, $code);
        } else {
            if ($response) {
                try {
                    $content = json_decode($response, true, 512, JSON_THROW_ON_ERROR);
                    if (array_key_exists("message", $content)) {
                        $message = $content['message'];
                    } else if (array_key_exists("detail", $content)) {
                        $message = $content['detail'];
                    } else if (array_key_exists("error", $content)) {
                        $message = $content['error'];
                    } else {
                        $message = 'Unknown message';
                    }
                    if (array_key_exists("code", $content)) {
                        $code = $content['code'];
                    } else if (array_key_exists("status", $content)) {
                        $code = $content['status'];
                    }
                } catch (JsonException $e) {
                    $message .= " (Api Response: " . $response . " : ".$e->getMessage().")";
                }
            }
            $exception = new HttpClientException($message, $previous, $code);
        }
        return $exception;
    }

    /**
     * @param ResponseInterface $response
     *
     * @return HttpClientResponseInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public static function parseResponse(ResponseInterface $response): HttpClientResponseInterface
    {
        $clientResponse = (new HttpClientResponse())
            ->setStatusCode($response->getStatusCode())
            ->setHeaders($response->getHeaders())
            ->setInfo($response->getInfo())
            ->setContent($response->getContent())
        ;

        $headers = $response->getHeaders();
        if (array_key_exists('cache-tags', $headers)) {
            $clientResponse->setTags(explode(',', $headers['cache-tags'][0]));
        }
        return $clientResponse;
    }

    /**
     * @param string $method
     * @param bool   $useCachedResult
     *
     * @return bool
     */
    public function cachedRequestAuthorized(string $method, bool $useCachedResult): bool
    {
        return $method === self::METHOD_GET && $useCachedResult && null !== $this->apiCache;
    }

    /**
     * @param array              $options
     * @param string             $method
     * @param bool               $apiUserRequest
     * @param UserInterface|null $user
     *
     * @return array
     */
    protected function parseOptions(
        array $options,
        string $method,
        bool $apiUserRequest = false,
        ?UserInterface $user = null
    ): array {
        $defaultOptions = [];
        $defaultOptions['headers'] = $this->generateHeaders($options, $method, $apiUserRequest, $user);
        return array_merge_recursive($defaultOptions, $options);
    }

    /**
     * @param array              $options
     * @param string             $method
     * @param bool               $apiUserRequest
     * @param UserInterface|null $user
     *
     * @return string[]
     */
    protected function generateHeaders(
        array $options,
        string $method,
        bool $apiUserRequest = false,
        ?UserInterface $user = null
    ): array {
        $defaultHeaders =  [
            'Accept' => 'application/json'
        ];
        if ($apiUserRequest) {
            $defaultHeaders['X-AUTH-TOKEN'] = $this->apiKey;
        }
        if ($method === self::METHOD_POST) {
            $defaultHeaders['Content-Type'] = 'application/json';
        }
        if (null !== $user) {
            $defaultHeaders['Authorization'] = 'Bearer '.$user->getToken();
        }
        $defaultHeaders['Client-Origin'] = $this->applicationMode;
        return array_key_exists('headers', $options) ?
            array_merge_recursive($defaultHeaders, $options['headers'])
            : $defaultHeaders;
    }

    /**
     * @param TagAwareCacheInterface $apiCache
     *
     * @return void
     */
    public function setCacheService(TagAwareCacheInterface $apiCache): void
    {
        $this->apiCache     = $apiCache;
    }
}
