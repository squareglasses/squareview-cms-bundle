<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Cache;

/**
 * Class CacheKeyGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CacheKeyGenerator
{
    /**
     * @param string $value
     *
     * @return string
     */
    public static function generateKey(string $value): string
    {
        return sha1($value);
    }
}
