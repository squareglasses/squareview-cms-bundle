<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Cache;

use Exception;
use JsonException;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\SkipCacheInvalidationInterface;
use SG\CmsBundle\Common\HttpClient\FrontendApiClient;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class CacheManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CacheManager
{
    public const CACHE_INVALIDATE_ENDPOINT = 'api/cache/invalidate';
    public const ROUTER_INVALIDATE_ENDPOINT = 'api/cache/invalidate-router';
    private HttpClientInterface $httpClient;

    /**
     * @param TagExtractor $tagExtractor
     * @param FrontendApiClient $client
     */
    public function __construct(private readonly TagExtractor $tagExtractor, private FrontendApiClient $client)
    {
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function invalidateRouterCache(): void
    {
        $this->client->request(
            'POST',
            self::ROUTER_INVALIDATE_ENDPOINT
        );
    }

    /**
     * @param Cacheable $cacheable
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function invalidateCacheForResource(Cacheable $cacheable): void
    {
        if (!$cacheable instanceof SkipCacheInvalidationInterface) {
            $this->invalidateCacheTags($this->tagExtractor->generateTags($cacheable));
        }
    }

    /**
     * @param array $tags
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function invalidateCacheTags(array $tags): void
    {
        $options = [
            'body' => json_encode([
                'tags' => $tags, JSON_THROW_ON_ERROR
            ], JSON_THROW_ON_ERROR)
        ];
        $this->client->request(
            'POST',
            self::CACHE_INVALIDATE_ENDPOINT,
            $options
        );
    }

    /**
     * @param Cacheable $cacheable
     *
     * @return array
     * @throws Exception
     */
    public function getResourceTags(Cacheable $cacheable): array
    {
        return $this->tagExtractor->generateTags($cacheable);
    }
}
