<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Cache;

use Behat\Transliterator\Transliterator;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionClass;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;

/**
 * Class TagExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TagExtractor
{
    /**
     * @param array $resourceCustomTags
     * @param array $discriminatorCustomTags
     */
    public function __construct(private array $resourceCustomTags = [], private array $discriminatorCustomTags = [])
    {
        foreach ($resourceCustomTags as $key => $customTags) {
            $newKey = str_replace("_", "-", $key);
            $this->resourceCustomTags[$newKey] = $customTags;
        }
        foreach ($discriminatorCustomTags as $key => $customTags) {
            $newKey = str_replace("_", "-", $key);
            $this->discriminatorCustomTags[$newKey] = $customTags;
        }
    }

    /**
     * @param Cacheable $resource
     * @param bool      $deletion
     *
     * @return array
     */
    public function generateTags(Cacheable $resource, bool $deletion = false): array
    {
        $tags = array_merge(
            $this->getResourceCustomTags($resource),
            $this->getDiscriminatorCustomTags($resource)
        );

        if (method_exists($resource, 'getDiscriminator')
            && null !== $resource->getDiscriminator() && !in_array($resource->getDiscriminator(), $tags, true)
        ) {
            $tags[] = $resource->getDiscriminator();
        }

        //Used to invalidate guide layout translation publications
        if ($resource instanceof Versionable) {
            try {
                $version = $resource->getDraftVersion();
                if ($version instanceof Guidable) {
                    $tags[] = $version->getGuide();
                }
            } catch (Exception) {
            }
        } elseif ($resource instanceof Guidable) {
            $tags[] = $resource->getGuide();
        }

        $idTag = self::stringify(get_class($resource).'-'.$resource->getId());
        if (!in_array($idTag, $tags, true)) {
            $tags[] = $idTag;
        }

        if ($resource instanceof CmsResourceInterface) {
            if (null !== $resource->getDiscriminator() && !in_array($resource->getDiscriminator(), $tags, true)) {
                $tags[] = $resource->getDiscriminator();
            }
            if (null !== $resource->getResourceIdentifier()) {
                $tags[] = self::stringify($resource->getResourceClass().'-'.$resource->getResourceIdentifier());
            }
            if (null !== $resource->getSlug()) {
                $tags[] = self::stringify($resource->getSlug());
            }
            if (method_exists($resource, 'getChildren')) {
                foreach ($resource->getChildren() as $child) {
                    if (method_exists($child, 'getDiscriminator')
                        && !in_array($child->getDiscriminator(), $tags, true)) {
                        $tags[] = self::stringify($child->getDiscriminator());
                    }
                }
            }
        }

        return $tags;
    }

    /**
     * @param string $resourceClass
     * @param array  $results
     * @param array  $cacheTags
     *
     * @return array
     */
    #[ArrayShape(['cache-tags' => "string"])] public static function getCacheHeaders(string $resourceClass, array $results = [], array $cacheTags = []): array
    {
        $tags = $cacheTags;
        try {
            $refClass = new ReflectionClass($resourceClass);
            if ($refClass->hasMethod("getDiscriminator")) {
                $resource = $refClass->newInstance();
                if (null !== $resource->getDiscriminator()) {
                    $tags[] = $resource->getDiscriminator();
                }
            }

            foreach ($results as $result) {
                if (method_exists($result, 'getDiscriminator')
                    && !in_array($result->getDiscriminator(), $tags, true)) {
                    $tags[] = $result->getDiscriminator();
                }
                $idTag = self::stringify($resourceClass.'-'.$result->getId());
                if (!in_array($idTag, $tags, true)) {
                    $tags[] = $idTag;
                }
            }
        } catch (Exception) {
        }
        return [
            'cache-tags' => implode(',', $tags)
        ];
    }

    /**
     * @param Cacheable $resource
     *
     * @return array
     */
    private function getResourceCustomTags(Cacheable $resource): array
    {
        $tags = [];
        if (array_key_exists($resource->getSlug(), $this->resourceCustomTags)) {
            $tags = array_merge(explode(',', $this->resourceCustomTags[$resource->getSlug()]), $tags);
        }
        return $tags;
    }

    /**
     * @param Cacheable $resource
     *
     * @return array
     */
    private function getDiscriminatorCustomTags(Cacheable $resource): array
    {
        $tags = [];
        if (method_exists($resource, 'getDiscriminator')
            && array_key_exists($resource->getDiscriminator(), $this->discriminatorCustomTags)) {
            $tags = array_merge(explode(',', $this->discriminatorCustomTags[$resource->getDiscriminator()]), $tags);
        }
        return $tags;
    }

    /**
     * @param string $tag
     *
     * @return string
     */
    public static function stringify(string $tag): string
    {
        return Transliterator::urlize($tag);
    }

    /**
     * @param array $originalTags
     * @param array $tagsToMerge
     *
     * @return array
     */
    public function mergeTags(array $originalTags, array $tagsToMerge): array
    {
        foreach ($tagsToMerge as $tag) {
            if (!in_array($tag, $originalTags, true)) {
                $originalTags[] = $tag;
            }
        }
        return $originalTags;
    }
}
