<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Twig\Extension;

use RuntimeException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Common\Media\Twig\TokenParser\GalleryTokenParser;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Common\Contracts\GalleryInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TokenParser\TokenParserInterface;

/**
 * Class GalleryTwigExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GalleryTwigExtension extends AbstractExtension
{
    private array $cachedTemplates = array();

    /**
     * @param Environment         $environment
     * @param ConfigurationBag    $configurationBag
     * @param SerializerInterface $serializer
     */
    public function __construct(
        private readonly Environment         $environment,
        private readonly ConfigurationBag    $configurationBag,
        private readonly SerializerInterface $serializer
    ) {
    }

    /**
     * @return array|TokenParserInterface[]
     */
    public function getTokenParsers(): array
    {
        return array(
            new GalleryTokenParser()
        );
    }

    /**
     * @param HasGalleriesInterface $resource
     * @param string                $identifier
     * @param array                 $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function gallery(HasGalleriesInterface $resource, string $identifier, array $options = array()): string
    {
        $gallery = $resource->getGalleryByIdentifier($identifier);
        if (!$gallery) {
            return '';
        }

        return $this->renderGallery($gallery, $options);
    }


    /**
     * @param GalleryInterface $gallery
     * @param array            $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderGallery(GalleryInterface $gallery, array $options = array()): string
    {
        $galleryConfig = $this->configurationBag->getResourceGalleryConfiguration($gallery->getIdentifier());
        if (!$galleryConfig) {
            throw new RuntimeException("No configuration found for gallery ".$gallery->getIdentifier().". Please check your medias.yaml configuration.");
        }
        $view = array_key_exists("view", $galleryConfig) && null !== $galleryConfig['view'] ?
            $galleryConfig['view'] :
            $this->configurationBag->getGalleriesDefaultView();
        if (array_key_exists("template", $options)) {
            $view = $options['template'];
        }
        $medias = [];
        foreach ($gallery->getMedias() as $media) {
            $resourceBag = new ResourceBag($this->serializer);
            $resourceBag->setMedias([$media]);
            $medias[] = $resourceBag;
        }

        return $this->render($view, array(
            'medias' => $medias,
            'identifier' => $gallery->getIdentifier(),
            'thumbnail_format' => array_key_exists("thumbnail_format", $galleryConfig) ?
                $galleryConfig['thumbnail_format'] : $galleryConfig['fullsize_format'],
            'fullsize_format' => $galleryConfig['fullsize_format'],
            'options' => $options,
        ));
    }

    /**
     * @param string $template
     * @param array  $parameters
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $template, array $parameters = array()): string
    {
        if (!isset($this->cachedTemplates[$template])) {
            $this->cachedTemplates[$template] = $this->environment->load($template);
        }
        return $this->cachedTemplates[$template]->render($parameters);
    }
}
