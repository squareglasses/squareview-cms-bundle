<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Twig\Extension;

use SG\CmsBundle\Common\Contracts\HasMediasInterface;
use SG\CmsBundle\Common\Media\Exception\FormatNotFoundException;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Exception\MediaNotFoundException;
use SG\CmsBundle\Common\Media\Provider\Pool;
use SG\CmsBundle\Common\Media\Twig\TokenParser\AltTokenParser;
use SG\CmsBundle\Common\Media\Twig\TokenParser\MediaTokenParser;
use SG\CmsBundle\Common\Media\Twig\TokenParser\MediaDownloadPathTokenParser;
use SG\CmsBundle\Common\Media\Twig\TokenParser\MediaPathTokenParser;
use SG\CmsBundle\Common\Media\Twig\TokenParser\ThumbnailDownloadPathTokenParser;
use SG\CmsBundle\Common\Media\Twig\TokenParser\ThumbnailPathTokenParser;
use SG\CmsBundle\Common\Media\Twig\TokenParser\ThumbnailTokenParser;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use Symfony\Component\Routing\RouterInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TokenParser\TokenParserInterface;
use Twig\TwigFunction;

/**
 * Class MediaTwigExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaTwigExtension extends AbstractExtension
{
    private array $cachedTemplates = array();

    /**
     * @param Environment $environment
     * @param Pool $pool
     * @param RouterInterface $router
     */
    public function __construct(
        private readonly Environment $environment,
        private readonly Pool $pool,
        private readonly RouterInterface $router
    )
    {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'get_video_thumbnail_path',
                [$this, 'getVideoThumbnailPath'],
                []
            ),
        ];
    }

    /**
     * @return array
     */
    public function getTokenParsers(): array
    {
        return array(
            new AltTokenParser(),
            new MediaTokenParser(),
            new MediaPathTokenParser(),
            new MediaDownloadPathTokenParser(),
            new ThumbnailTokenParser(),
            new ThumbnailPathTokenParser(),
            new ThumbnailDownloadPathTokenParser(),
        );
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     * @param array              $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function media(HasMediasInterface $resource, string $identifier, array $options = array()): string
    {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }
        return $this->renderMedia($media, 'reference', false, $options);
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     * @param string|null        $defaultValue
     *
     * @return string|null
     */
    public function alt(HasMediasInterface $resource, string $identifier, ?string $defaultValue = null): ?string
    {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }
        return $media->getAlt() ?: $defaultValue;
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     *
     * @return string
     */
    public function mediaPath(HasMediasInterface $resource, string $identifier): string
    {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }
        return $this->path($media, 'reference');
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     *
     * @return string
     */
    public function mediaDownloadPath(HasMediasInterface $resource, string $identifier): string
    {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }

        return $this->router->generate("media_download", array(
            'id' => $media->getId(),
            'format' => 'reference'
        ));
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     * @param string|null        $format
     * @param bool               $webpSupported
     * @param array              $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function thumbnail(
        HasMediasInterface $resource,
        string $identifier,
        string $format = null,
        bool $webpSupported = true,
        array $options = array()
    ): string {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }

        $format = $format ?? $this->getProviderFormat($media);
        if (!$format) {
            throw new FormatNotFoundException($media);
        }

        return $this->renderMedia($media, $format, $webpSupported, $options);
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     * @param string|null        $format
     * @param bool               $webpSupported
     *
     * @return string|null
     */
    public function thumbnailPath(
        HasMediasInterface $resource,
        string $identifier,
        string $format = null,
        bool $webpSupported = true
    ): ?string {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }

        $format = $format ?? $this->getProviderFormat($media);
        if (!$format) {
            throw new FormatNotFoundException($media);
        }
        $provider = $this->pool->getProvider($media->getProviderName());
        if (null === $provider) {
            return "";
        }
        try {
            $mediasOptions = $provider->getHelperProperties($media, $format, $webpSupported);
            return $mediasOptions['src'];
        } catch (MediaNotFoundException) {
            return "";
        }
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     * @param string|null        $format
     *
     * @return string
     */
    public function thumbnailDownloadPath(
        HasMediasInterface $resource,
        string $identifier,
        string $format = null
    ): string {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }

        $format = $format ?? $this->getProviderFormat($media);
        if (!$format) {
            throw new FormatNotFoundException($media);
        }

        return $this->router->generate("media_download", array(
            'id' => $media->getId(),
            'format' => $format
        ));
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     * @param bool                $webpSupported
     * @param array               $options
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderMedia(MediaModelInterface $media, string $format, bool $webpSupported = true, array $options = array()): string
    {
        $provider = $this->pool->getProvider($media->getProviderName());
        if (null === $provider) {
            return "";
        }
        try {
            $mediasOptions = $provider->getHelperProperties($media, $format, $webpSupported, $options);
            $options = array_merge($options, $mediasOptions);

            return $this->render($provider->getViewTemplate(), array(
                'media' => $media,
                'format' => $format,
                'options' => $options,
            ));
        } catch (MediaNotFoundException) {
            return "";
        }
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     *
     * @return string
     */
    public function path(MediaModelInterface $media, string $format): string
    {
        return $this->router->generate("media_display", array(
            'id' => $media->getId(),
            'format' => $format,
            'filename' => $media->getFileName()
        ));
    }

    /**
     * @param string $template
     * @param array  $parameters
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function render(string $template, array $parameters = array()): string
    {
        if (!isset($this->cachedTemplates[$template])) {
            $this->cachedTemplates[$template] = $this->environment->load($template);
        }

        return $this->cachedTemplates[$template]->render($parameters);
    }

    /**
     * Gets the default format configure for this resource identifier (resource discriminator + media identifier)
     * in the configuration for this provider.
     *
     * @param MediaModelInterface $media
     *
     * @return string|null
     */
    private function getProviderFormat(MediaModelInterface $media): ?string
    {
        return $this->pool->getProvider($media->getProviderName())->getFormatByResourceMediaIdentifier($media->getIdentifier());
    }

    /**
     * @param HasMediasInterface $resource
     * @param string $identifier
     * @return string
     */
    public function getVideoThumbnailPath(
        HasMediasInterface $resource,
        string $identifier
    ): string
    {
        $media = $resource->getMediaByIdentifier($identifier);
        if (!$media) {
            return '';
        }

        if (!array_key_exists("thumbnail", $media->getProviderMetadata())) {
            return '';
        }

        return "video_thumbnails/".$media->getProviderMetadata()["thumbnail"];
    }
}
