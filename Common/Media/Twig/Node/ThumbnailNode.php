<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Twig\Node;

use Twig\Compiler;
use Twig\Node\Expression\AbstractExpression;
use Twig\Node\Node;

/**
 * Class ThumbnailNode
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ThumbnailNode extends Node
{
    protected string $extensionName;

    /**
     * @param string             $name
     * @param AbstractExpression $resource
     * @param AbstractExpression $identifier
     * @param AbstractExpression $format
     * @param AbstractExpression $webpSupported
     * @param AbstractExpression $attributes
     * @param int                $line
     * @param string|null        $tag
     */
    public function __construct(
        string $name,
        AbstractExpression $resource,
        AbstractExpression $identifier,
        AbstractExpression $format,
        AbstractExpression $webpSupported,
        AbstractExpression $attributes,
        int $line,
        string $tag = null
    ) {
        $this->extensionName = $name;
        parent::__construct(array(
            'resource' => $resource,
            'identifier' => $identifier,
            'format' => $format,
            'webpSupported' => $webpSupported,
            'attributes' => $attributes
        ), array(), $line, $tag);
    }

    /**
     * @param Compiler $compiler
     *
     * @return void
     */
    public function compile(Compiler $compiler): void
    {
        $compiler
            ->addDebugInfo($this)
            ->write(sprintf("echo \$this->env->getExtension('%s')->thumbnail(", $this->extensionName))
            ->subcompile($this->getNode('resource'))
            ->raw(', ')
            ->subcompile($this->getNode('identifier'))
            ->raw(', ')
            ->subcompile($this->getNode('format'))
            ->raw(', ')
            ->subcompile($this->getNode('webpSupported'))
            ->raw(', ')
            ->subcompile($this->getNode('attributes'))
            ->raw(");\n")
        ;
    }
}
