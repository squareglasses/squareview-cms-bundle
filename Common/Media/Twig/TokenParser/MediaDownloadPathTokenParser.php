<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Twig\TokenParser;

use SG\CmsBundle\Common\Media\Twig\Extension\MediaTwigExtension;
use SG\CmsBundle\Common\Media\Twig\Node\MediaDownloadPathNode;
use Twig\Error\SyntaxError;
use Twig\Node\Node;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

/**
 * Class MediaDownloadPathTokenParser
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaDownloadPathTokenParser extends AbstractTokenParser
{
    /**
     * @param Token $token
     *
     * @return Node
     * @throws SyntaxError
     */
    public function parse(Token $token): Node
    {
        $parser = $this->parser;
        $stream = $parser->getStream();

        $resource = $parser->getExpressionParser()->parseExpression();

        $stream->next();
        $identifier = $parser->getExpressionParser()->parseExpression();

        $stream->expect(Token::BLOCK_END_TYPE);

        return new MediaDownloadPathNode(
            MediaTwigExtension::class,
            $resource,
            $identifier,
            $token->getLine(),
            $this->getTag()
        );
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return 'download_path';
    }
}
