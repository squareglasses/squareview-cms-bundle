<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Twig\TokenParser;

use SG\CmsBundle\Common\Media\Twig\Extension\MediaTwigExtension;
use SG\CmsBundle\Common\Media\Twig\Node\ThumbnailPathNode;
use Twig\Error\SyntaxError;
use Twig\Node\Expression\ConstantExpression;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

/**
 * Class ThumbnailPathTokenParser
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ThumbnailPathTokenParser extends AbstractTokenParser
{
    /**
     * @param Token $token
     *
     * @return ThumbnailPathNode
     * @throws SyntaxError
     */
    public function parse(Token $token): ThumbnailPathNode
    {
        $parser = $this->parser;
        $stream = $parser->getStream();

        $resource = $parser->getExpressionParser()->parseExpression();

        $stream->next();
        $identifier = $parser->getExpressionParser()->parseExpression();

        if ($parser->getStream()->test(Token::PUNCTUATION_TYPE)) {
            $parser->getStream()->next();

            $format = $parser->getExpressionParser()->parseExpression();
        } else {
            $format = new ConstantExpression(null, $token->getLine());
        }

        if ($parser->getStream()->test(Token::PUNCTUATION_TYPE)) {
            $parser->getStream()->next();

            $webpSupported = $parser->getExpressionParser()->parseExpression();
        } else {
            $webpSupported = new ConstantExpression(true, $token->getLine());
        }

        $stream->expect(Token::BLOCK_END_TYPE);

        return new ThumbnailPathNode(
            MediaTwigExtension::class,
            $resource,
            $identifier,
            $format,
            $webpSupported,
            $token->getLine(),
            $this->getTag()
        );
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return 'thumbnail_path';
    }
}
