<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Twig\TokenParser;

use SG\CmsBundle\Common\Media\Twig\Extension\MediaTwigExtension;
use SG\CmsBundle\Common\Media\Twig\Node\AltNode;
use Twig\Error\SyntaxError;
use Twig\Node\Expression\ConstantExpression;
use Twig\Node\Node;
use Twig\Token;
use Twig\TokenParser\AbstractTokenParser;

/**
 * Class AltTokenParser
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AltTokenParser extends AbstractTokenParser
{
    /**
     * @param Token $token
     *
     * @return Node
     * @throws SyntaxError
     */
    public function parse(Token $token): Node
    {
        $parser = $this->parser;
        $stream = $parser->getStream();

        $resource = $parser->getExpressionParser()->parseExpression();

        $stream->next();
        $identifier = $parser->getExpressionParser()->parseExpression();

        if ($parser->getStream()->test(Token::PUNCTUATION_TYPE)) {
            $parser->getStream()->next();

            $defaultValue = $parser->getExpressionParser()->parseExpression();
        } else {
            $defaultValue = new ConstantExpression(null, $token->getLine());
        }

        $stream->expect(Token::BLOCK_END_TYPE);

        return new AltNode(
            MediaTwigExtension::class,
            $resource,
            $identifier,
            $defaultValue,
            $token->getLine(),
            $this->getTag()
        );
    }

    /**
     * @return string
     */
    public function getTag(): string
    {
        return 'alt';
    }
}
