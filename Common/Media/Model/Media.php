<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Model;

use SG\CmsBundle\Common\Contracts\MediaModelInterface;

/**
 * Class Media
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Media implements MediaModelInterface
{
    protected ?int $id = null;
    protected ?string $identifier = null;
    /** @var string|null $name */
    protected ?string $name = null;
    protected ?string $providerName = null;
    protected ?string $providerReference = null;
    protected ?array $providerMetadata = null;
    protected ?string $contentType = null;
    protected ?int $contentSize = null;
    protected ?int $width = null;
    protected ?int $height = null;
    protected ?float $length = null;
    protected ?string $author = null;
    protected ?string $alt = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return $this
     */
    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name ?: $this->providerReference;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProviderName(): ?string
    {
        return $this->providerName;
    }

    /**
     * @param string|null $providerName
     *
     * @return $this
     */
    public function setProviderName(?string $providerName): self
    {
        $this->providerName = $providerName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProviderReference(): ?string
    {
        return $this->providerReference;
    }

    /**
     * @param string|null $providerReference
     *
     * @return $this
     */
    public function setProviderReference(?string $providerReference): self
    {
        $this->providerReference = $providerReference;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getProviderMetadata(): ?array
    {
        return $this->providerMetadata;
    }

    /**
     * @param array|null $providerMetadata
     * @return $this
     */
    public function setProviderMetadata(?array $providerMetadata): self
    {
        $this->providerMetadata = $providerMetadata;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    /**
     * @param string|null $contentType
     *
     * @return $this
     */
    public function setContentType(?string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getContentSize(): ?int
    {
        return $this->contentSize;
    }

    /**
     * @param int|null $size
     *
     * @return $this
     */
    public function setContentSize(?int $size = null): self
    {
        $this->contentSize = $size;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     *
     * @return $this
     */
    public function setWidth(?int $width = null): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     *
     * @return $this
     */
    public function setHeight(?int $height = null): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     *
     * @return $this
     */
    public function setLength(?float $length = null): self
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     *
     * @return $this
     */
    public function setAuthor(string $author = null): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string|null $alt
     *
     * @return $this
     */
    public function setAlt(?string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * @return string
     */
    public function getFileName(): string
    {
        return $this->getName() ?? $this->getProviderReference();
    }
}
