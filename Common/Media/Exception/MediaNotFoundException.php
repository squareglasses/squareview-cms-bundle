<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Class MediaNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaNotFoundException extends HttpException
{
    /**
     * @param int          $mediaId
     * @param Throwable|null $previous
     * @param int             $code
     * @param array           $headers
     */
    public function __construct(int $mediaId, Throwable $previous = null, int $code = 0, array $headers = [])
    {
        $message = "Media not found for id '".$mediaId."'.";
        parent::__construct(404, $message, $previous, $headers, $code);
    }
}
