<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Exception;

use RuntimeException;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use Throwable;

/**
 * Class FormatNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FormatNotFoundException extends RuntimeException
{
    /**
     * @param MediaModelInterface $media
     * @param                     $code
     * @param Throwable|null     $previous
     */
    public function __construct(MediaModelInterface $media, $code = 0, Throwable $previous = null)
    {
        $message = "No default format configured for media '".$media->getIdentifier()."' and provider "
        ."'".$media->getProviderName()."', and format argument of twig extension is not provided.";
        parent::__construct($message, $code, $previous);
    }
}
