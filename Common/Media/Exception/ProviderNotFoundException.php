<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Exception;

use Throwable;

/**
 * Class ProviderNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ProviderNotFoundException extends \RuntimeException
{
    /**
     * @param string          $providerName
     * @param                 $code
     * @param Throwable|null $previous
     */
    public function __construct(
        string $providerName,
        $code = 0,
        Throwable $previous = null
    ) {
        $message = "No provider found for name '".$providerName."'.";
        parent::__construct($message, $code, $previous);
    }
}
