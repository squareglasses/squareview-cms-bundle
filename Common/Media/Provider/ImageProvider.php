<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use League\Flysystem\FilesystemException;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Imagine\Service\FilterService;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Api\Entity\Media as MediaEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ImageProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ImageProvider extends FileProvider
{
    /**
     * @param FilterService       $liipFilter
     * @param HttpClientInterface $apiClient
     * @param string              $apiUrl
     */
    public function __construct(private readonly FilterService $liipFilter, HttpClientInterface $apiClient, string $apiUrl)
    {
        parent::__construct($apiClient, $apiUrl);
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     * @param bool                $webpSupported
     * @param array               $options
     *
     * @return array
     */
    public function getHelperProperties(MediaModelInterface $media, string $format, bool $webpSupported = true, array $options = array()): array
    {
        $src = $this->generatePublicUrl($media, $format, $webpSupported);
        return array_merge(array(
            'alt' => $media->getAlt() ?? $media->getName(),
            'title' => $media->getAlt() ?? $media->getName(),
            'src' => $src
        ), $options);
    }

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $useWebp
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws FilesystemException
     */
    public function getMediaResponse(Media $media, string $format, bool $useWebp = true): Response
    {
        $response = new Response();

        //Cache-control des medias:
        //cf. https://symfony.com/blog/new-in-symfony-4-1-session-improvements#allow-to-cache-requests-that-use-sessions
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setMaxAge(31536000);//1 year -> use query versions to invalidate (Cache-Control)

        $response->headers->set('Content-type', $media->getContentType());

        if ($format === 'reference') {
            $apiResponse = $this->apiClient->request('GET', 'medias/reference/'.$media->getId(), []);
            $data = $apiResponse->getContent();

            if ($media->getContentSize()) {
                $response->headers->set('Content-length', (string)$media->getContentSize());
            }

            // Send headers before outputting anything
            $response->sendHeaders();
            $response->setContent($data);
        } else {
            $path = $this->generatePrivateUrl($media, $format, $useWebp);
            $file = $this->getFilesystem()->read($path);
            $response->headers->set('Content-length', (string)$this->getFilesystem()->fileSize($path));

            //Case is webp, send back webp mime type
            if($this->getFilesystem()->mimeType($path) !== $media->getContentType()) {
                $response->headers->set('Content-type', $this->getFilesystem()->mimeType($path));
            }

            // Send headers before outputting anything
            $response->sendHeaders();
            $response->setContent($file);
        }

        return $response;
    }

    /**
     * @param Media  $media
     * @param string $format
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws FilesystemException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getDownloadResponse(Media $media, string $format): Response
    {
        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', $media->getContentType());
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $media->getName() . '";');

        if ($format === 'reference') {
            $apiResponse = $this->apiClient->request('GET', 'medias/reference/'.$media->getId(), []);
            $data = $apiResponse->getContent();
            if ($media->getContentSize()) {
                $response->headers->set('Content-length', (string)$media->getContentSize());
            }

            // Send headers before outputting anything
            $response->sendHeaders();
            $response->setContent($data);
        } else {
            $path = $this->generatePrivateUrl($media, $format);
            $file = $this->getFilesystem()->read($path);
            $response->headers->set('Content-length', (string)$this->getFilesystem()->fileSize($path));

            // Send headers before outputting anything
            $response->sendHeaders();
            $response->setContent($file);
        }

        return $response;
    }

    /**
     * @param MediaEntity $media
     * @param string      $filename
     *
     * @return MediaEntity
     * @throws FilesystemException
     */
    public function setMetadata(MediaEntity $media, string $filename): MediaEntity
    {
        if ($this->getFilesystem()->has($filename)) {
            $media->setContentType($this->getFilesystem()->mimeType($filename));
            if ($media->getContentType() === "image/svg") {
                $media->setContentType("image/svg+xml");
            }
            $media->setContentSize($this->getFilesystem()->fileSize($filename));
            $metadatas = [
                "type" => $this->getFilesystem()->mimeType($filename),
                "path" => $filename,
                "size" => $this->getFilesystem()->fileSize($filename)
            ];
            $media->setProviderMetadata($metadatas);

            if ($media->getContentType() !== "image/svg+xml") {
                $image = imagecreatefromstring($this->getFilesystem()->read($filename));
                if ($image !== false) { //case formats not supported by imagecreatefromstring
                    $media->setWidth(imagesx($image));
                    $media->setHeight(imagesy($image));
                }
            }
        }
        return $media;
    }

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $webpSupported
     *
     * @return string
     */
    public function generatePrivateUrl(Media $media, string $format, bool $webpSupported = true): string
    {
        $publicUrl = $this->liipFilter->getUrlOfFilteredImage($media, $format, null, $webpSupported); // Generate cache file if necessary
        return substr($publicUrl, strpos($publicUrl, "/cache/")); //get relative url starting with /cache/
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     * @param bool                $webpSupported
     *
     * @return string
     */
    public function generatePublicUrl(MediaModelInterface $media, string $format, bool $webpSupported = true): string
    {
        if ($format === "reference" || preg_match("/.svg/", $media->getName())) {
            return $this->apiUrl."/medias/reference/".$media->getId()."/".$media->getName();
        }
        return $this->liipFilter->getUrlOfFilteredImage($media, $format, null, $webpSupported);
    }

    /**
     * @return string
     */
    public function getViewTemplate(): string
    {
        return "@SGCms/media/provider/image.html.twig";
    }
}
