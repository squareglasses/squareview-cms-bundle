<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use RuntimeException;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Api\Entity\Media as MediaEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class VimeoProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class VimeoProvider extends BaseProvider
{
    /**
     * @param HttpClientInterface $httpClient
     */
    public function __construct(private readonly HttpClientInterface $httpClient)
    {
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     * @param bool                $webpSupported
     * @param array               $options
     *
     * @return array
     */
    #[ArrayShape(['src' => "string", 'id' => "mixed", 'frameborder' => "int|mixed", 'width' => "int|mixed|null", 'height' => "int|mixed|null"])]
    public function getHelperProperties(MediaModelInterface $media, string $format, bool $webpSupported = true, array $options = array()): array
    {
        // documentation : http://vimeo.com/api/docs/moogaloop
        $defaults = array(
            // (optional) Flash Player version of app. Defaults to 9 .NEW!
            // 10 - New Moogaloop. 9 - Old Moogaloop without the newest features.
            'fp_version' => 10,

            // (optional) Enable fullscreen capability. Defaults to true.
            'fullscreen' => true,

            // (optional) Show the byline on the video. Defaults to true.
            'title' => true,

            // (optional) Show the title on the video. Defaults to true.
            'byline' => 0,

            // (optional) Show the user's portrait on the video. Defaults to true.
            'portrait' => true,

            // (optional) Specify the color of the video controls.
            'color' => null,

            // (optional) Set to 1 to disable HD.
            'hd_off' => 0,

            // Set to 1 to enable the Javascript API.
            'js_api' => null,

            // (optional) JS function called when the player loads. Defaults to vimeo_player_loaded.
            'js_onLoad' => 0,

            // Unique id that is passed into all player events as the ending parameter.
            'js_swf_id' => uniqid('vimeo_player_', true),
        );

        $player_parameters = array_merge($defaults, $options['player_parameters'] ?? array());

        return array(
            'src' => http_build_query($player_parameters),
            'id' => $player_parameters['js_swf_id'],
            'frameborder' => $options['frameborder'] ?? 0,
            'width' => $options['width'] ?? $media->getWidth(),
            'height' => $options['height'] ?? $media->getHeight()
        );
    }

    /**
     * @param MediaEntity $media
     * @param string      $filename
     *
     * @return MediaEntity
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function setMetadata(MediaEntity $media, string $filename): MediaEntity
    {
        try {
            $response = $this->httpClient->request("GET", sprintf('https://vimeo.com/api/oembed.json?url=https://vimeo.com/%s', $filename));
            $metadata = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (RuntimeException|JsonException) {
            return $media;
        }

        // store provider information
        $media->setProviderMetadata($metadata);
        $media->setHeight($metadata['height']);
        $media->setWidth($metadata['width']);
        $media->setLength($metadata['duration']);
        $media->setAuthor($metadata['author_name']);
        $media->setContentType('video/x-flv');

        return $media;
    }

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $useWebp
     *
     * @return Response
     */
    public function getMediaResponse(Media $media, string $format, bool $useWebp = true): Response
    {
        throw new RuntimeException("getMediaResponse not supported by vimeoProvider");
    }

    /**
     * @return string
     */
    public function getViewTemplate(): string
    {
        return "@SGCms/media/provider/vimeo.html.twig";
    }
}
