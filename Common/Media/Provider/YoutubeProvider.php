<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use RuntimeException;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Api\Entity\Media as MediaEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class YoutubeProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class YoutubeProvider extends BaseProvider
{
    /**
     * @param HttpClientInterface $httpClient
     */
    public function __construct(private readonly HttpClientInterface $httpClient)
    {
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     * @param bool                $webpSupported
     * @param array               $options
     *
     * @return array
     */
    #[ArrayShape(['html5' => "bool", 'player_url_parameters' => "string", 'player_parameters' => "[]|array"])]
    public function getHelperProperties(MediaModelInterface $media, string $format, bool $webpSupported = true, array $options = array()): array
    {
        // documentation : http://code.google.com/apis/youtube/player_parameters.html

        $default_player_url_parameters = array(

            //Values: 0 or 1. Default is 1. Sets whether the player should load related
            // videos once playback of the initial video starts. Related videos are
            // displayed in the "genie menu" when the menu button is pressed. The player
            // search functionality will be disabled if rel is set to 0.
            'rel' => 0,

            // Values: 0 or 1. Default is 0. Sets whether the initial video will autoplay
            // when the player loads.
            'autoplay' => 0,

            // Values: 0 or 1. Default is 0. In the case of a single video player, a setting of 1
            // will cause the player to play the initial video again and again. In the case of a
            // playlist player (or custom player), the player will play the entire playlist and
            // then start again at the first video.
            'loop' => 0,

            // Values: 0 or 1. Default is 0. Setting this to 1 will enable the Javascript API.
            // For more information on the Javascript API and how to use it, see the JavaScript
            // API documentation.
            'enablejsapi' => 0,

            // Value can be any alphanumeric string. This setting is used in conjunction with the
            // JavaScript API. See the JavaScript API documentation for details.
            'playerapiid' => null,

            // Values: 0 or 1. Default is 0. Setting to 1 will disable the player keyboard controls.
            // Keyboard controls are as follows:
            //      Spacebar: Play / Pause
            //      Arrow Left: Jump back 10% in the current video
            //      Arrow Right: Jump ahead 10% in the current video
            //      Arrow Up: Volume up
            //      Arrow Down: Volume Down
            'disablekb' => 0,

            // Values: 0 or 1. Default is 0. Setting to 1 enables the "Enhanced Genie Menu". This
            // behavior causes the genie menu (if present) to appear when the user's mouse enters
            // the video display area, as opposed to only appearing when the menu button is pressed.
            'egm' => 0,

            // Values: 0 or 1. Default is 0. Setting to 1 enables a border around the entire video
            // player. The border's primary color can be set via the color1 parameter, and a
            // secondary color can be set by the color2 parameter.
            'border' => 0,

            // Values: Any RGB value in hexadecimal format. color1 is the primary border color, and
            // color2 is the video control bar background color and secondary border color.
            'color1' => null,
            'color2' => null,

            // Values: 0 or 1. Default is 0. Setting to 1 enables the fullscreen button. This has no
            // effect on the Chromeless Player. Note that you must include some extra arguments to
            // your embed code for this to work.
            'fs' => 1,

            // Values: A positive integer. This parameter causes the player to begin playing the video
            // at the given number of seconds from the start of the video. Note that similar to the
            // seekTo function, the player will look for the closest keyframe to the time you specify.
            // This means sometimes the play head may seek to just before the requested time, usually
            // no more than ~2 seconds
            'start' => 0,

            // Values: 0 or 1. Default is 0. Setting to 1 enables HD playback by default. This has no
            // effect on the Chromeless Player. This also has no effect if an HD version of the video
            // is not available. If you enable this option, keep in mind that users with a slower
            // connection may have an sub-optimal experience unless they turn off HD. You should ensure
            // your player is large enough to display the video in its native resolution.
            'hd' => 1,

            // Values: 0 or 1. Default is 1. Setting to 0 disables the search box from displaying when
            // the video is minimized. Note that if the rel parameter is set to 0 then the search box
            // will also be disabled, regardless of the value of showsearch.
            'showsearch' => 0,

            // Values: 0 or 1. Default is 1. Setting to 0 causes the player to not display information
            // like the video title and rating before the video starts playing.
            'showinfo' => 0,

            // Values: 1 or 3. Default is 1. Setting to 1 will cause video annotations to be shown by
            // default, whereas setting to 3 will cause video annotation to not be shown by default.
            'iv_load_policy' => 1,

            // Values: 1. Default is based on user preference. Setting to 1 will cause closed captions
            // to be shown by default, even if the user has turned captions off.
            'cc_load_policy' => 1,

            // Values: 'window' or 'opaque' or 'transparent'.
            // When wmode=window, the Flash movie is not rendered in the page.
            // When wmode=opaque, the Flash movie is rendered as part of the page.
            // When wmode=transparent, the Flash movie is rendered as part of the page.
            'wmode' => 'window',

        );

        $default_player_parameters = array(

            // Values: 0 or 1. Default is 0. Setting to 1 enables a border around the entire video
            // player. The border's primary color can be set via the color1 parameter, and a
            // secondary color can be set by the color2 parameter.
            'border' => $default_player_url_parameters['border'],

            // Values: 'allowfullscreen' or empty. Default is 'allowfullscreen'. Setting to empty value disables
            //  the fullscreen button.
            'allowFullScreen' => $default_player_url_parameters['fs'] === '1',

            // The allowScriptAccess parameter in the code is needed to allow the player SWF to call
            // functions on the containing HTML page, since the player is hosted on a different domain
            // from the HTML page.
            'allowScriptAccess' => $options['allowScriptAccess'] ?? 'always',

            // Values: 'window' or 'opaque' or 'transparent'.
            // When wmode=window, the Flash movie is not rendered in the page.
            // When wmode=opaque, the Flash movie is rendered as part of the page.
            // When wmode=transparent, the Flash movie is rendered as part of the page.
            'wmode' => $default_player_url_parameters['wmode'],

        );

        $player_url_parameters = array_merge($default_player_url_parameters, $options['player_url_parameters'] ?? array());

        $player_parameters = array_merge($default_player_parameters, $options['player_parameters'] ?? array(), array(
            'width' => $options['width'] ?? $media->getWidth(),
            'height' => $options['height'] ?? $media->getHeight(),
        ));

        return array(
            'html5' => true,
            'player_url_parameters' => http_build_query($player_url_parameters),
            'player_parameters' => $player_parameters,
        );
    }

    /**
     * @param MediaEntity $media
     * @param string      $filename
     *
     * @return MediaEntity
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function setMetadata(MediaEntity $media, string $filename): MediaEntity
    {
        try {
            $response = $this->httpClient->request("GET", sprintf('https://www.youtube.com/oembed?url=https://www.youtube.com/watch?v=%s&format=json', $filename));
            $metadata = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (RuntimeException) {
            return $media;
        }

        // store provider information
        $media->setProviderMetadata($metadata);
        $media->setAuthor($metadata['author_name']);
        $media->setHeight($metadata['height']);
        $media->setWidth($metadata['width']);
        $media->setContentType('video/x-flv');

        return $media;
    }

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $useWebp
     *
     * @return Response
     */
    public function getMediaResponse(Media $media, string $format, bool $useWebp = true): Response
    {
        throw new RuntimeException("getMediaResponse not supported by vimeoProvider");
    }

    /**
     * @return string
     */
    public function getViewTemplate(): string
    {
        return "@SGCms/media/provider/youtube.html.twig";
    }
}
