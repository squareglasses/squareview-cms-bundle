<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use League\Flysystem\Filesystem;

/**
 * Class BaseProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class BaseProvider implements ProviderInterface
{
    protected array $mediaIdentifiers = array();
    protected string $name;
    protected Filesystem $filesystem;

    /**
     * @param string $identifier
     * @param array $mediaSettings
     */
    public function addMediaIdentifier(string $identifier, array $mediaSettings = []): void
    {
        $this->mediaIdentifiers[$identifier] = $mediaSettings;
    }

    /**
     * @param string $identifier
     *
     * @return string|null
     */
    public function getFormatByResourceMediaIdentifier(string $identifier): ?string
    {
        return array_key_exists($identifier, $this->mediaIdentifiers) ? $this->mediaIdentifiers[$identifier]['format'] : null;
    }

    /**
     * @param Filesystem $filesystem
     *
     * @return $this
     */
    public function setFilesystem(Filesystem $filesystem): self
    {
        $this->filesystem = $filesystem;

        return $this;
    }

    /**
     * @return Filesystem
     */
    public function getFilesystem(): Filesystem
    {
        return $this->filesystem;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
