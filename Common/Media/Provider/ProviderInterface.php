<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Api\Entity\Media as MediaEntity;
use Symfony\Component\HttpFoundation\Response;

/**
 * Interface ProviderInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ProviderInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $webpSupported
     * @param array  $options
     *
     * @return array
     */
    public function getHelperProperties(MediaModelInterface $media, string $format, bool $webpSupported = true, array $options = array()): array;

    /**
     * @param MediaEntity $media
     * @param string      $filename
     *
     * @return MediaEntity
     */
    public function setMetadata(MediaEntity $media, string $filename): MediaEntity;

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $useWebp
     *
     * @return Response
     */
    public function getMediaResponse(Media $media, string $format, bool $useWebp = true): Response;
}
