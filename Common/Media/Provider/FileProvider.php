<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use JetBrains\PhpStorm\ArrayShape;
use League\Flysystem\FilesystemException;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Api\Entity\Media as MediaEntity;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\EventListener\AbstractSessionListener;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class FileProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FileProvider extends BaseProvider
{
    /**
     * @param HttpClientInterface $apiClient
     * @param string              $apiUrl
     */
    public function __construct(protected HttpClientInterface $apiClient, protected string $apiUrl)
    {
    }

    /**
     * @param Media  $media
     * @param string $format
     *
     * @return string
     */
    public function generatePublicUrl(Media $media, string $format): string
    {
        return $this->apiUrl."media/reference/".$media->getId()."/".$media->getName();
    }

    /**
     * @param MediaEntity $media
     *
     * @return array
     * @throws FilesystemException
     */
    #[ArrayShape(['stream' => "resource", 'mimetype' => "mixed", 'size' => "mixed"])]
    public function getReferenceMedia(MediaEntity $media): array
    {
        return [
            'stream' => $this->getFilesystem()->readStream($media->getProviderReference()),
            'mimetype' => $this->getFilesystem()->mimeType($media->getProviderReference()),
            'size' => $this->getFilesystem()->fileSize($media->getProviderReference())
        ];
    }

    /**
     * @param Media  $media
     * @param string $format
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getDownloadResponse(Media $media, string $format): Response
    {
        $response = new Response();
        $response->headers->set('Cache-Control', 'private');
        $response->headers->set('Content-type', $media->getContentType());
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $media->getName() . '";');

        $apiResponse = $this->apiClient->request('GET', 'medias/reference/'.$media->getId(), []);
        $data = $apiResponse->getContent();
        if ($media->getContentSize()) {
            $response->headers->set('Content-length', (string)$media->getContentSize());
        }

        // Send headers before outputting anything
        $response->sendHeaders();
        $response->setContent($data);

        return $response;
    }

    /**
     * @param Media  $media
     * @param string $format
     * @param bool   $useWebp
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getMediaResponse(Media $media, string $format, bool $useWebp = true): Response
    {
        $response = new Response();

        //Cache-control des medias:
        //cf. https://symfony.com/blog/new-in-symfony-4-1-session-improvements#allow-to-cache-requests-that-use-sessions
        $response->headers->set(AbstractSessionListener::NO_AUTO_CACHE_CONTROL_HEADER, 'true');
        $response->setMaxAge(31536000);//1 year -> use query versions to invalidate (Cache-Control)

        $response->headers->set('Content-type', $media->getContentType());

        $apiResponse = $this->apiClient->request('GET', 'medias/reference/'.$media->getId(), []);
        $data = $apiResponse->getContent();

        if ($media->getContentSize()) {
            $response->headers->set('Content-length', (string)$media->getContentSize());
        }

        // Send headers before outputting anything
        $response->sendHeaders();
        $response->setContent($data);

        return $response;
    }

    /**
     * @param MediaEntity $media
     * @param string      $filename
     *
     * @return MediaEntity
     * @throws FilesystemException
     */
    public function setMetadata(MediaEntity $media, string $filename): MediaEntity
    {
        if ($this->getFilesystem()->has($filename)) {
            $media->setContentType($this->getFilesystem()->mimeType($filename));
            $media->setContentSize($this->getFilesystem()->fileSize($filename));

            $metadatas = [
                "type" => $this->getFilesystem()->mimeType($filename),
                "path" => $filename,
                "size" => $this->getFilesystem()->fileSize($filename)
            ];
            $media->setProviderMetadata($metadatas);
        }
        return $media;
    }

    /**
     * @param MediaModelInterface $media
     * @param string              $format
     * @param bool                $webpSupported
     * @param array               $options
     *
     * @return array
     */
    public function getHelperProperties(MediaModelInterface $media, string $format, bool $webpSupported = true, array $options = array()): array
    {
        return array();
    }
}
