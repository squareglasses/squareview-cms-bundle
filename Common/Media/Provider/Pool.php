<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Provider;

use SG\CmsBundle\Common\Media\Exception\ProviderNotFoundException;

/**
 * Class Pool
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Pool
{
    private array $providers = [];

    /**
     * @param string            $name
     * @param ProviderInterface $provider
     *
     * @return $this
     */
    public function addProvider(string $name, ProviderInterface $provider): self
    {
        $this->providers[$name] = $provider;

        return $this;
    }

    /**
     * @return array
     */
    public function getProviders(): array
    {
        return $this->providers;
    }

    /**
     * @param string $name
     *
     * @return ProviderInterface|null
     */
    public function getProvider(string $name): ?ProviderInterface
    {
        if (!isset($this->providers[$name])) {
            throw new ProviderNotFoundException($name);
        }

        return $this->providers[$name];
    }
}
