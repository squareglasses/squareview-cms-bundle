<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Controller;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\HttpClient\ApiClient;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Common\Media\Exception\MediaNotFoundException;
use SG\CmsBundle\Common\Media\Provider\Pool;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class DownloadAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/download/media/{format}/{id}", methods={"GET"}, name="media_download", options={"expose": true})
 */
class DownloadAction
{
    /**
     * @param ApiClient           $apiClient
     * @param Pool                $pool
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly ApiClient $apiClient, private readonly Pool $pool, private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param Request $request
     * @param string  $id
     * @param string  $format
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(Request $request, string $id, string $format): Response
    {
        $media = $this->getMedia($id);
        return $this->pool->getProvider($media->getProviderName())->getDownloadResponse($media, $format);
    }

    /**
     * @param string $id
     *
     * @return Media
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws InvalidArgumentException
     * @throws HttpExceptionInterface
     * @throws JsonException
     */
    public function getMedia(string $id): Media
    {
        try {
            $response = $this->apiClient->apiRequest('/medias/'.$id);
            $serializedMedia = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception) {
            throw new MediaNotFoundException($id);
        }
        return $this->serializer->deserialize(json_encode($serializedMedia, JSON_THROW_ON_ERROR), Media::class, 'json');
    }
}
