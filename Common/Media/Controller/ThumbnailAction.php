<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Controller;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\HttpClient\ApiClient;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Common\Media\Exception\MediaNotFoundException;
use SG\CmsBundle\Common\Media\Provider\Pool;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ThumbnailAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/media/cache/{format}/{filename}", methods={"GET"}, requirements={"filename": ".+"}, name="thumbnail_display", options={"expose": true})
 */
class ThumbnailAction
{
    /**
     * @param ApiClient           $apiClient
     * @param Pool                $pool
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly ApiClient $apiClient, private readonly Pool $pool, private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param Request $request
     * @param string  $format
     * @param string  $filename
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws JsonException
     */
    public function __invoke(Request $request, string $format, string $filename): Response
    {
        $filename = str_replace(".webp", "", $filename);
        $media = $request->get('id', false) ? $this->getMedia($request->get('id')) : $this->getMedia($filename);
        return $this->pool->getProvider($media->getProviderName())->getMediaResponse($media, $format, $request->get('webp', true));
    }

    /**
     * @param string $filename
     *
     * @return mixed
     * @throws InvalidArgumentException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|JsonException
     */
    public function getMedia(string $filename): mixed
    {
        try {
            $response = $this->apiClient->apiRequest('/medias/'.$filename);
            $serializedMedia = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            throw new MediaNotFoundException($filename, $e);
        }
        return $this->serializer->deserialize(json_encode($serializedMedia, JSON_THROW_ON_ERROR), Media::class, 'json');
    }
}
