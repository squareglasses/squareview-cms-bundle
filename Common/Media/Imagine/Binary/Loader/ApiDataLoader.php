<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Imagine\Binary\Loader;

use Exception;
use finfo;
use Liip\ImagineBundle\Binary\Loader\LoaderInterface;
use Liip\ImagineBundle\Model\Binary;
use SG\CmsBundle\Common\Media\Exception\MediaNotFoundException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ApiDataLoader
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiDataLoader implements LoaderInterface
{
    /**
     * @param HttpClientInterface $apiClient
     */
    public function __construct(private HttpClientInterface $apiClient)
    {
    }

    /**
     * @param $path
     *
     * @return Binary
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function find($path): Binary
    {
        if (str_contains((string)$path, "video_thumbnails")) {
            $response = $this->apiClient->request('GET', 'video-thumbnails/'.str_replace("video_thumbnails/", '', (string)$path));
            if (404 === $response->getStatusCode()) {
                throw new MediaNotFoundException($path);
            }
            $buffer = $response->getContent();
        } else {
            try {
                $response = $this->apiClient->request('GET', 'medias/reference/'.$path);
                if (404 === $response->getStatusCode()) {
                    throw new MediaNotFoundException($path);
                }
                $buffer = $response->getContent();
            } catch (Exception $e) {
                throw new MediaNotFoundException($path, $e);
            }
        }

        // return binary instance with data
        return new Binary($buffer, $this->getMimeType($buffer), $this->getExtension($buffer));
    }

    /**
     * @param HttpClientInterface $apiClient
     *
     * @return void
     */
    public function setApiClient(HttpClientInterface $apiClient): void
    {
        $this->apiClient = $apiClient;
    }

    /**
     * @param string $buffer
     *
     * @return string
     */
    private function getMimeType(string $buffer): string
    {
        return (new finfo(FILEINFO_MIME_TYPE))->buffer($buffer);
    }

    /**
     * @param string $buffer
     *
     * @return string
     */
    private function getExtension(string $buffer): string
    {
        $finfo = new finfo(FILEINFO_EXTENSION);
        $exts = $finfo->buffer($buffer);
        if ($exts !== "???" && preg_match('//', $exts)) {
            $ext = explode('/', $exts)[0];
        } else {
            $ext = $exts;
        }
        return $ext;
    }
}
