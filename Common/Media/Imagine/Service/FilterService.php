<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Media\Imagine\Service;

use Liip\ImagineBundle\Binary\BinaryInterface;
use Liip\ImagineBundle\Exception\Imagine\Filter\NonExistingFilterException;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Liip\ImagineBundle\Imagine\Data\DataManager;
use Liip\ImagineBundle\Imagine\Filter\FilterManager;
use Liip\ImagineBundle\Service\FilterPathContainer;
use Liip\ImagineBundle\Service\FilterService as BaseFilterService;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use SG\CmsBundle\Common\Media\Model\Media;

/**
 * Class FilterService
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FilterService extends BaseFilterService
{
    /**
     * @param DataManager $dataManager
     * @param FilterManager $filterManager
     * @param CacheManager $cacheManager
     * @param bool $webpGenerate
     * @param array $webpOptions
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        private DataManager            $dataManager,
        private readonly FilterManager $filterManager,
        private readonly CacheManager  $cacheManager,
        private readonly bool          $webpGenerate,
        private readonly array         $webpOptions,
        private ?LoggerInterface       $logger = null
    ) {
        parent::__construct($dataManager, $filterManager, $cacheManager, $webpGenerate, $webpOptions, $logger);
        $this->logger           = $logger ?: new NullLogger();
    }

    /**
     * @param mixed $path
     * @param       $filter
     * @param       $resolver
     * @param bool  $webpSupported
     *
     * @return string
     */
    public function getUrlOfFilteredImage(mixed $path, $filter, $resolver = null, bool $webpSupported = false): string
    {
        $media = $path;
        $options = [];
        if (preg_match("/.gif/", $media->getProviderReference())) {
            $options['format'] = 'gif';
        }
        $basePathContainer = new FilterPathContainer($media->getProviderReference(), '', $options);

        return urldecode($this->getUrlOfFilteredImageByContainer($media, $basePathContainer, $filter, $resolver, $webpSupported));
    }

    /**
     * @param Media               $media
     * @param FilterPathContainer $basePathContainer
     * @param string              $filter
     * @param string|null         $resolver
     * @param bool                $webpSupported
     *
     * @return string
     */
    private function getUrlOfFilteredImageByContainer(
        Media $media,
        FilterPathContainer $basePathContainer,
        string $filter,
        ?string $resolver = null,
        bool $webpSupported = false
    ): string {
        $filterPathContainers = [$basePathContainer];

        if ($this->webpGenerate) {
            $webpPathContainer = $basePathContainer->createWebp($this->webpOptions);
            $filterPathContainers[] = $webpPathContainer;
        }

        foreach ($filterPathContainers as $filterPathContainer) {
            if (!$this->cacheManager->isStored($filterPathContainer->getTarget(), $filter, $resolver)) {
                $this->cacheManager->store(
                    $this->createFilteredBinary($media, $filterPathContainer, $filter),
                    $filterPathContainer->getTarget(),
                    $filter,
                    $resolver
                );
            }
        }

        if ($webpSupported && isset($webpPathContainer)) {
            return $this->cacheManager->resolve($webpPathContainer->getTarget(), $filter, $resolver);
        }

        return $this->cacheManager->resolve($basePathContainer->getTarget(), $filter, $resolver);
    }

    /**
     * @param Media               $media
     * @param FilterPathContainer $filterPathContainer
     * @param string              $filter
     *
     * @return BinaryInterface
     */
    private function createFilteredBinary(
        Media $media,
        FilterPathContainer $filterPathContainer,
        string $filter
    ): BinaryInterface {
        $binary = $this->dataManager->find($filter, $media->getId());

        try {
            return $this->filterManager->applyFilter($binary, $filter, $filterPathContainer->getOptions());
        } catch (NonExistingFilterException $e) {
            $this->logger->debug(sprintf(
                'Could not locate filter "%s" for path "%s". Message was "%s"',
                $filter,
                $filterPathContainer->getSource(),
                $e->getMessage()
            ));

            throw $e;
        }
    }

    /**
     * @param DataManager $dataManager
     *
     * @return void
     */
    public function setDataManager(DataManager $dataManager): void
    {
        $this->dataManager = $dataManager;
    }
}
