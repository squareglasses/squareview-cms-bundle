<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Command;

use Exception;
use JsonException;
use SG\CmsBundle\Common\HttpClient\FrontendApiClient;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\Yaml\Yaml;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class SyncFrontendConfigCommand
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SyncFrontendConfigCommand extends Command
{
    public const CONFIGURATION_ENDPOINT = 'api/cms/configuration';

    /**
     * @param string $projectDir
     * @param FrontendApiClient $client
     */
    public function __construct(private readonly string $projectDir, protected FrontendApiClient $client)
    {
        parent::__construct('sg:config:sync-frontend');
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Sync the cms configuration with frontend\'s one.')
            ->setHelp('This command sync some parameters of cms configuration with parameters of frontend configuration');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Synchronize cms configuration');

        // Getting API configuration array
        $response = $this->client->request('GET', self::CONFIGURATION_ENDPOINT, [
            'timeout' => 20,
        ]);

        try {
            $result = $response->getContent();
        } catch (Exception $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
        $cmsYamlFile = $this->projectDir.'/config/squareview/cms.yaml';
        $clientConfiguration = json_decode($result, true, 512, JSON_THROW_ON_ERROR);
        $apiConfiguration = Yaml::parseFile($cmsYamlFile);
        $apiConfiguration['sg_cms']['website_name'] = $clientConfiguration['website_name'];
        $apiConfiguration['sg_cms']['website_identifier'] = $clientConfiguration['website_identifier'];
        $apiConfiguration['sg_cms']['translations']['project'] = $clientConfiguration['website_identifier'];
        if (isset($clientConfiguration['guides'])) {
            $apiConfiguration['sg_cms']['guides'] = $clientConfiguration['guides'];
        }
        if (isset($clientConfiguration['modules'])) {
            $apiConfiguration['sg_cms']['modules'] = $clientConfiguration['modules'];
        }
        if (isset($clientConfiguration['medias'])) {
            $apiConfiguration['sg_cms']['medias'] = $clientConfiguration['medias'];
        }
        $apiConfigurationYaml = Yaml::dump($apiConfiguration, 10);
        file_put_contents($cmsYamlFile, $apiConfigurationYaml);

        $io->success("Cms configuration for guides and modules is now synced with frontend configuration.");

        return Command::SUCCESS;
    }
}
