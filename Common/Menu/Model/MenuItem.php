<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Menu\Model;

use SG\CmsBundle\Common\Contracts\MenuItemInterface;

/**
 * Class MenuItem
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MenuItem implements MenuItemInterface
{
    protected ?string $name = null;
    protected ?string $slug = null;
    protected ?string $anchor = null;
    protected ?string $target = null;
    protected ?string $queryString = null;
    protected array $ulAttributes = [];
    protected array $liAttributes = [];
    protected array $linkAttributes = [];
    protected array $labelAttributes = [];
    protected ?string $uri = null;
    protected ?string $value = null;
    protected ?string $route = null;
    protected ?MenuItem $parent = null;
    protected bool $isCurrent = false;
    protected string $kind;
    protected ?string $cssClasses = null;

    /** @var MenuItem[] $children  */
    protected array $children = [];

    /**
     * @return MenuItem[]
     */
    public function getChildren(): array
    {
        return $this->children;
    }

    /**
     * @param array $children
     */
    public function setChildren(array $children): void
    {
        $this->children = $children;
    }

    /**
     * @param MenuItem $item
     */
    public function addChild(MenuItem $item): void
    {
        $this->children[] = $item;
    }

    /**
     * @return MenuItem|null
     */
    public function getParent(): ?MenuItem
    {
        return $this->parent;
    }

    /**
     * @param MenuItem|null $parent
     */
    public function setParent(?MenuItem $parent): void
    {
        $this->parent = $parent;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     */
    public function setSlug(?string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function getAnchor(): ?string
    {
        return $this->anchor;
    }

    /**
     * @param string|null $anchor
     */
    public function setAnchor(?string $anchor): void
    {
        $this->anchor = $anchor;
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @param string|null $target
     */
    public function setTarget(?string $target): void
    {
        $this->target = $target;
    }

    /**
     * @return string|null
     */
    public function getQueryString(): ?string
    {
        return $this->queryString;
    }

    /**
     * @param string|null $queryString
     */
    public function setQueryString(?string $queryString): void
    {
        $this->queryString = $queryString;
    }

    /**
     * @return array
     */
    public function getUlAttributes(): array
    {
        return $this->ulAttributes;
    }

    /**
     * @param array $ulAttributes
     */
    public function setUlAttributes(array $ulAttributes): void
    {
        $this->ulAttributes = $ulAttributes;
    }

    /**
     * @return array
     */
    public function getLiAttributes(): array
    {
        return $this->liAttributes;
    }

    /**
     * @param array $liAttributes
     */
    public function setLiAttributes(array $liAttributes): void
    {
        $this->liAttributes = $liAttributes;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addLiAttribute(string $name, string $value): self
    {
        if (isset($this->liAttributes[$name])) {
            $this->liAttributes[$name] .= " " . $value;
        } else {
            $this->liAttributes[$name] = $value;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getLinkAttributes(): array
    {
        return $this->linkAttributes;
    }

    /**
     * @param array $linkAttributes
     */
    public function setLinkAttributes(array $linkAttributes): void
    {
        $this->linkAttributes = $linkAttributes;
    }

    /**
     * @param string $name
     * @param string $value
     * @return $this
     */
    public function addLinkAttribute(string $name, string $value): self
    {
        if (isset($this->linkAttributes[$name])) {
            $this->linkAttributes[$name] .= " " . $value;
        } else {
            $this->linkAttributes[$name] = $value;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getLabelAttributes(): array
    {
        return $this->labelAttributes;
    }

    /**
     * @param array $labelAttributes
     */
    public function setLabelAttributes(array $labelAttributes): void
    {
        $this->labelAttributes = $labelAttributes;
    }

    /**
     * @return string|null
     */
    public function getUri(): ?string
    {
        return $this->uri;
    }

    /**
     * @param string|null $uri
     */
    public function setUri(?string $uri): void
    {
        $this->uri = $uri;
    }

    /**
     * @return string|null
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string|null $route
     */
    public function setRoute(?string $route): void
    {
        $this->route = $route;
    }

    /**
     * @return bool
     */
    public function isCurrent(): bool
    {
        return $this->isCurrent;
    }

    /**
     * @param bool $isCurrent
     */
    public function setIsCurrent(bool $isCurrent): void
    {
        $this->isCurrent = $isCurrent;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     */
    public function setKind(string $kind): void
    {
        $this->kind = $kind;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     */
    public function setValue(?string $value): void
    {
        $this->value = $value;
    }

    /**
     * @return string|null
     */
    public function getCssClasses(): ?string
    {
        return $this->cssClasses;
    }

    /**
     * @param string|null $cssClasses
     * @return void
     */
    public function setCssClasses(?string $cssClasses): void
    {
        $this->cssClasses = $cssClasses;
    }
}
