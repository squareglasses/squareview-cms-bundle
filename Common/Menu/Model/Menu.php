<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\Menu\Model;

/**
 * Class Menu
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Menu
{
    private string $name;
    private string $slug;
    protected array $attributes = [];
    protected ?string $cssClasses = null;

    /** @var MenuItem[]  */
    private array $items = [];

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return MenuItem[]
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param MenuItem[] $items
     */
    public function setItems(array $items): void
    {
        $this->items = $items;
    }

    /**
     * @param MenuItem $item
     */
    public function addItem(MenuItem $item): void
    {
        $this->items[] = $item;
    }

    /**
     * @return array
     */
    public function getAttributes(): array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     */
    public function setAttributes(array $attributes): void
    {
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug(string $slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return string|null
     */
    public function getCssClasses(): ?string
    {
        return $this->cssClasses;
    }

    /**
     * @param string|null $cssClasses
     *
     * @return Menu
     */
    public function setCssClasses(?string $cssClasses): Menu
    {
        $this->cssClasses = $cssClasses;
        return $this;
    }
}
