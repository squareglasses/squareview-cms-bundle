<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class TranslationConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationConfiguration
{
    /**
     * @return mixed
     */
    public function getConfig(): mixed
    {
        $treeBuilder = new TreeBuilder('translations');
        // @TODO: Add constraints/defaults values on nodes
        return $treeBuilder->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('http_client_service')->defaultNull()->end()
                ->scalarNode('project')->defaultNull()->end()
                ->scalarNode('api_key')->defaultNull()->end()
                ->scalarNode('environment')->defaultNull()->end()
                ->scalarNode('api_user')->defaultNull()->end()
                ->scalarNode('api_password')->defaultNull()->end()
            ->end()
        ;
    }
}
