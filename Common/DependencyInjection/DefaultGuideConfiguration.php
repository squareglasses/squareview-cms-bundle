<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class DefaultGuideConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DefaultGuideConfiguration
{
    public function getConfig(): ArrayNodeDefinition
    {
        $treeBuilder = new TreeBuilder('default_guide');
        return $treeBuilder->getRootNode()
            ->info('The default guide used in the application.')
            ->addDefaultsIfNotSet()
            ->children()
                ->scalarNode('controller')
                    ->defaultValue("SG\CmsBundle\Frontend\Controller\CmsController")
                    ->info('Controller called by the guide.')
                ->end()
                ->scalarNode('action')
                    ->defaultValue("default")
                    ->info('Action called by the guide.')
                ->end()
                ->scalarNode('view')->end()
                ->scalarNode('translation_domain')->end()
                ->booleanNode('sitemap')->defaultTrue()->end()
                ->scalarNode('sitemap_frequency')
                    ->defaultValue("monthly")
                    ->info('If the resources using this guide is displayed in the sitemap, specify the frequency value.')
                ->end()
                ->floatNode('sitemap_priority')
                    ->defaultValue(0.5)
                    ->info('If the resources using this guide is displayed in the sitemap, specify the priority value.')
                ->end()
            ->end();
    }
}
