<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection\Compiler;

use SG\CmsBundle\Common\Media\Provider\Pool;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Class MediaProviderCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaProviderCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(Pool::class)) {
            return;
        }
        $pool = $container->getDefinition(Pool::class);

        if ($container->hasParameter('squareview.medias')) {
            $settings = $container->getParameter('squareview.medias');

            foreach ($settings['providers'] as $providerName => $providerSettings) {
                $providerDefinition = $container->getDefinition($providerSettings['service']);
                $providerDefinition->addMethodCall('setName', [$providerName]);

                // Adding managed media identifiers to the provider
                foreach ($settings['identifiers'] as $identifier => $mediaSettings) {
                    if ($mediaSettings['provider'] === $providerName) {
                        $providerDefinition->addMethodCall('addMediaIdentifier', [$identifier, $mediaSettings]);
                    }
                }

                // Setting filesystem to the provider
                $filesystemDefinition = $container->getDefinition($providerSettings['filesystem']);
                $providerDefinition->addMethodCall('setFilesystem', [$filesystemDefinition]);

                // Adding provider to the pool
                $pool->addMethodCall("addProvider", [$providerName, $providerDefinition]);
            }
        }
    }
}
