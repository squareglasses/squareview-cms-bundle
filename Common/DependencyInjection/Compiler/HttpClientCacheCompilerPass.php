<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection\Compiler;

use SG\CmsBundle\Common\HttpClient\ApiClient as CommonApiClient;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Class HttpClientCacheCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HttpClientCacheCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
        if ($container->hasDefinition('api.cache')) {
            $cacheService = $container->getDefinition('api.cache');
            if ($providerDefinition = $container->getDefinition(CommonApiClient::class)) {
                $providerDefinition->addMethodCall('setCacheService', [$cacheService]);
            }
            if ($providerDefinition = $container->getDefinition(ApiClient::class)) {
                $providerDefinition->addMethodCall('setCacheService', [$cacheService]);
            }
        }
    }
}
