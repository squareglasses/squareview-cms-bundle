<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class GuideConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GuideConfiguration
{
    /**
     * @return mixed
     */
    public function getConfig(): mixed
    {
        $treeBuilder = new TreeBuilder('guides');
        return $treeBuilder->getRootNode()
            ->info('The list of available guides in the application.')
            ->useAttributeAsKey('name')
            ->prototype('array')
                ->children()
                    ->booleanNode('layout')
                        ->defaultFalse()
                        ->info('If the guide will be used by multiple resources (ex. blog articles) or not (ex. specific page).')
                    ->end()
                    ->scalarNode('controller')
                        ->defaultValue("SG\CmsBundle\Frontend\Controller\CmsController")
                        ->info('Controller called by the guide.')
                    ->end()
                    ->scalarNode('action')
                        ->defaultNull()
                        ->info('Action called by the guide.')
                    ->end()
                    ->scalarNode('view')
                        ->defaultValue("default.html.twig")
                        ->info('The twig view rendered by the guide.')
                    ->end()
                    ->scalarNode('translation_domain')
                        ->info('The identifier for the translation\'s domain.')
                    ->end()
                    ->booleanNode('sitemap')
                        ->defaultTrue()
                        ->info('If the resources using this guide should be included in the automatically generated sitemap.')
                    ->end()
                    ->scalarNode('sitemap_frequency')
                        ->defaultValue("monthly")
                        ->info('If the resources using this guide is displayed in the sitemap, specify the frequency value.')
                    ->end()
                    ->floatNode('sitemap_priority')
                        ->defaultValue(0.5)
                        ->info('If the resources using this guide is displayed in the sitemap, specify the priority value.')
                    ->end()
                    ->arrayNode('zones')
                        ->useAttributeAsKey('name')
                        ->prototype('array')
                            ->addDefaultsIfNotSet()
                            ->canBeDisabled()
                            ->children()
                                ->enumNode('type')
                                    ->values(['modules', 'translations'])
                                    ->info('The type of zone.')
                                ->end()
                                ->booleanNode('layout')->defaultFalse()->info('True if the zone is for layout translations')->end()
                                ->integerNode('position')
                                    ->defaultValue(1)
                                    ->info('The position of the zone.')
                                ->end()
                                ->scalarNode('description')
                                    ->info('A description of the zone to be used in the admin interface.')
                                ->end()
                                ->arrayNode('keys')
                                    ->useAttributeAsKey('name')
                                    ->prototype('enum')
                                        ->values(['text', 'textarea', 'wysiwyg'])
                                        ->info('The form field type for the translation key.')
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
