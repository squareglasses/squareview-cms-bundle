<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class ModuleConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleConfiguration
{
    /**
     * @return mixed
     */
    public function getConfig(): mixed
    {
        $treeBuilder = new TreeBuilder('modules');
        return $treeBuilder->getRootNode()
            ->info('Modules are used for page construction.')
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('guides')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->addDefaultsIfNotSet()
                        ->children()
                            ->scalarNode('label')
                                ->info('A name for the module in the backend')
                            ->end()
                            ->scalarNode('description')
                                ->info('A description for the module in the backend')
                            ->end()
                            ->scalarNode('icon')
                                ->defaultValue('glasses')
                                ->info('A material UI name to be used in the backend')
                            ->end()
                            ->scalarNode('renderer')
                                ->defaultValue('SG\CmsBundle\Frontend\Renderer\ModuleRenderer')
                                ->info('Renderer used for module generation.')
                            ->end()
                            ->scalarNode('view')
                                ->defaultNull()
                                ->info('View used for module rendering.')
                            ->end()
                            ->scalarNode('bo_grid_columns')
                                ->defaultNull()
                                ->info('CSS grid to define number and size of columns in BO')
                            ->end()
                            ->scalarNode('pattern')
                                ->defaultNull()
                                ->info('By default, used for rendering the module parameters in the view.')
                            ->end()
                            ->arrayNode('parameters')
                                ->useAttributeAsKey('name')
                                ->prototype('array')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('label')->defaultValue('')->info('Label for backend.')->end()
                                        ->scalarNode('description')->defaultValue('')->info('Description for backend.')->end()
                                        ->scalarNode('form_type')->defaultValue('text')->info('Indication for the backend as to what type of form to use.')->end()
                                        ->scalarNode('bo_grid_column')->defaultNull()->info('CSS grid to define the size of the field in BO')->end()
                                        ->arrayNode('choices')
                                            ->info('List of static choices if form_type is choice or select')
                                            ->arrayPrototype()
                                                ->children()
                                                    ->scalarNode('label')->end()
                                                    ->scalarNode('value')->end()
                                                ->end()
                                            ->end()
                                        ->end()
                                        ->scalarNode('api')->defaultNull()->info('An api name for some fields that require an api')->end()
                                        ->variableNode('options')
                                            //TODO define fixed configuration when it will be settled
                                        ->end()
                                        ->scalarNode('default_value')->defaultNull()->info('A default value for the parameter')->end()
                                        ->booleanNode('translatable')->defaultFalse()->info('If the value is translatable.')->end()
                                        ->booleanNode('required')->defaultFalse()->info('If the parameter must be filled')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }
}
