<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class MediaConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaConfiguration
{
    /**
     * @return mixed
     */
    public function getConfig(): mixed
    {
        $treeBuilder = new TreeBuilder('medias');

        // @TODO: Add constraints/defaults values on nodes
        return $treeBuilder->getRootNode()
            ->info('Medias and galleries configuration.')
            ->children()
               ->scalarNode('galleries_default_view')->defaultValue('partials/gallery.html.twig')->end()
                ->append($this->addProvidersNode())
                ->append($this->addIdentifiersNode())
                ->append($this->addGalleriesNode())
            ->end()
        ;
    }

    /**
     * @return mixed
     */
    private function addIdentifiersNode(): mixed
    {
        $treeBuilder = new TreeBuilder('identifiers');

        return $treeBuilder->getRootNode()
            ->useAttributeAsKey('id')
            ->prototype('array')
                ->children()
                    ->scalarNode('description')->isRequired()->end()
                    ->scalarNode('provider')->isRequired()->end()
                    ->scalarNode('format')->defaultNull()->end()
                    ->scalarNode('dimensions')->defaultNull()->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return mixed
     */
    private function addProvidersNode(): mixed
    {
        $treeBuilder = new TreeBuilder('providers');

        return $treeBuilder->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
            ->arrayNode('image')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('SG\\CmsBundle\\Common\\Media\\Provider\\ImageProvider')->end()
            ->scalarNode('filesystem')->defaultValue('local.storage')->end()
            ->end()
            ->end()
            ->arrayNode('file')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('SG\\CmsBundle\\Common\\Media\\Provider\\FileProvider')->end()
            ->scalarNode('filesystem')->defaultValue('local.storage')->end()
            ->end()
            ->end()
            ->arrayNode('video')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('SG\\CmsBundle\\Common\\Media\\Provider\\VideoProvider')->end()
            ->scalarNode('filesystem')->defaultValue('local.storage')->end()
            ->end()
            ->end()
            ->arrayNode('vimeo')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('SG\\CmsBundle\\Common\\Media\\Provider\\VimeoProvider')->end()
            ->scalarNode('filesystem')->defaultValue('local.storage')->end()
            ->end()
            ->end()
            ->arrayNode('youtube')
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('service')->defaultValue('SG\\CmsBundle\\Common\\Media\\Provider\\YoutubeProvider')->end()
            ->scalarNode('filesystem')->defaultValue('local.storage')->end()
            ->end()
            ->end()
            ->end();
    }

    /**
     * @return mixed
     */
    private function addGalleriesNode(): mixed
    {
        $treeBuilder = new TreeBuilder('galleries');

        return $treeBuilder->getRootNode()
            ->useAttributeAsKey('id')
            ->prototype('array')
                ->children()
                    ->scalarNode('view')->defaultNull()->end()
                    ->scalarNode('thumbnail_format')->defaultNull()->end()
                    ->scalarNode('fullsize_format')->isRequired()->end()
                ->end()
            ->end()
        ;
    }
}
