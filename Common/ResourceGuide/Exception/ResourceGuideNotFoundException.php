<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\ResourceGuide\Exception;

use Exception;
use Throwable;

/**
 * Class ResourceGuideNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceGuideNotFoundException extends Exception
{
    /**
     * @param string          $guideName
     * @param                 $code
     * @param Throwable|null $previous
     */
    public function __construct(string $guideName, $code = 0, Throwable $previous = null)
    {
        $message = "No guide found for identifier '".$guideName."'. Please check the sg_cms.guides "
            ."(located in config/packages/sg_cms.yaml) configuration of"
            ." your client application.";
        parent::__construct($message, $code, $previous);
    }
}
