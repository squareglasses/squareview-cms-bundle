<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\ResourceGuide\Model;

use RuntimeException;
use SG\CmsBundle\Common\Contracts\ResourceGuideInterface;
use SG\CmsBundle\Common\Contracts\ZoneInterface;

/**
 * Class ResourceGuide
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class ResourceGuide implements ResourceGuideInterface
{
    private ?string $name = null;
    private ?string $controller = null;
    private ?string $action = null;
    private ?string $view = null;
    private ?string $translationDomain = null;
    private bool $sitemap = true;
    private ?string $sitemapFrequency = null;
    private ?float $sitemapPriority = null;

    /**
     * @var Zone[]
     */
    private array $zones;

    public function __construct()
    {
        $this->zones = [];
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getController(): ?string
    {
        return $this->controller;
    }

    /**
     * @param string $controller
     *
     * @return $this
     */
    public function setController(string $controller): self
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string|null $action
     *
     * @return $this
     */
    public function setAction(?string $action = null): self
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return string
     */
    public function getView(): string
    {
        if (!$this->getName()) {
            return '';
        }
        return $this->view ?: strtolower($this->getName()).'.html.twig';
    }

    /**
     * @param string $view
     *
     * @return $this
     */
    public function setView(string $view): self
    {
        $this->view = $view;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTranslationDomain(): ?string
    {
        return $this->translationDomain;
    }

    /**
     * @param string $translationDomain
     *
     * @return $this
     */
    public function setTranslationDomain(string $translationDomain): self
    {
        $this->translationDomain = $translationDomain;

        return $this;
    }

    /**
     * @return bool
     */
    public function isSitemap(): bool
    {
        return $this->sitemap;
    }

    /**
     * @param bool $sitemap
     *
     * @return $this
     */
    public function setSitemap(bool $sitemap): self
    {
        $this->sitemap = $sitemap;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSitemapFrequency(): ?string
    {
        return $this->sitemapFrequency;
    }

    /**
     * @param string|null $sitemapFrequency
     *
     * @return $this
     */
    public function setSitemapFrequency(?string $sitemapFrequency): self
    {
        $this->sitemapFrequency = $sitemapFrequency;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getSitemapPriority(): ?float
    {
        return $this->sitemapPriority;
    }

    /**
     * @param float|null $sitemapPriority
     *
     * @return $this
     */
    public function setSitemapPriority(?float $sitemapPriority): self
    {
        $this->sitemapPriority = $sitemapPriority;

        return $this;
    }

    /**
     * @return Zone[]
     */
    public function getZones(): array
    {
        return $this->zones;
    }

    /**
     * @param Zone[] $zones
     *
     * @return $this
     */
    public function setZones(array $zones): self
    {
        $this->zones = $zones;

        return $this;
    }

    /**
     * @param Zone $zone
     */
    public function addZone(Zone $zone): void
    {
        $this->zones[] = $zone;
    }

    /**
     * @param string $zoneName
     *
     * @return ZoneInterface
     */
    public function getZone(string $zoneName): ZoneInterface
    {
        foreach ($this->zones as $zone) {
            if ($zone->getName() === $zoneName) {
                return $zone;
            }
        }
        throw new RuntimeException("No zone found for name ".$zoneName);
    }
}
