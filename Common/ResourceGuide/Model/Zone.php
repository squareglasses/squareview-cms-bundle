<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\ResourceGuide\Model;

use RuntimeException;
use SG\CmsBundle\Frontend\Contracts\ModuleInterface;
use SG\CmsBundle\Common\Contracts\ZoneInterface;

/**
 * Class Zone
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class Zone implements ZoneInterface
{
    public const TYPE_MODULES = "modules";
    public const TYPE_TRANSLATIONS = "translations";

    private ?string $name = null;
    private ?string $type = null;
    private bool $layout = false;
    private ?int $position = null;

    /** @var array<ModuleInterface> $modules */
    private array $modules = [];

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return void
     * @throws RuntimeException
     */
    public function setType(string $type): void
    {
        if (!in_array($type, [self::TYPE_MODULES, self::TYPE_TRANSLATIONS])) {
            throw new RuntimeException("Zone type '" . $type . "' is not valid.
            Choices for zone type are '" . self::TYPE_MODULES . "' or '" . self::TYPE_TRANSLATIONS . "'.");
        }
        $this->type = $type;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return array<ModuleInterface>
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * @param ModuleInterface $module
     */
    public function addModule(ModuleInterface $module): void
    {
        $this->modules[] = $module;
    }

    /**
     * @return bool
     */
    public function isLayout(): bool
    {
        return $this->layout;
    }

    /**
     * @param bool $layout
     */
    public function setLayout(bool $layout): void
    {
        $this->layout = $layout;
    }
}
