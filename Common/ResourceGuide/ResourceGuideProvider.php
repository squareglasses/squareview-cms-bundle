<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Common\ResourceGuide;

use JsonException;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ResourceGuideProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceGuideProvider
{
    /** @var array<string> $availableGuides */
    private array $availableGuides = [];

    /** @var array<string> $defaultGuideConfig */
    private array $defaultGuideConfig;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param array<string> $guides
     */
    public function setAvailableGuides(array $guides): void
    {
        $this->availableGuides = $guides;
    }

    public function getAvailableGuides(): array
    {
        return $this->availableGuides;
    }

    /**
     * @param array<string> $defaultGuideConfig
     */
    public function setDefaultGuideConfiguration(array $defaultGuideConfig): void
    {
        $this->defaultGuideConfig = $defaultGuideConfig;
    }

    /**
     * @param string|null $guideName
     *
     * @return ResourceGuide|null
     * @throws ResourceGuideNotFoundException|JsonException
     */
    public function getGuide(?string $guideName = null): ?ResourceGuide
    {
        if ($guideName === null || $guideName === "default") {
            if (!array_key_exists("default", $this->availableGuides)) {
                return $this->getDefaultGuide();
            }
            $guideName = 'default';
        }

        if (!array_key_exists($guideName, $this->availableGuides)) {
            throw new ResourceGuideNotFoundException($guideName);
        }

        $guideData = array_merge($this->availableGuides[$guideName], array('name' => $guideName));
        $i = 0;

        foreach ($guideData['zones'] as $zoneName => $zone) {
            $guideData['zones'][$zoneName]['name'] = $zoneName;
            $guideData['zones'][$i] = $guideData['zones'][$zoneName]; // Fix array keys for serializer
            unset($guideData['zones'][$zoneName]);
            $i++;
        }

        return $this->serializer->deserialize(json_encode($guideData, JSON_THROW_ON_ERROR), ResourceGuide::class, 'json');
    }

    /**
     * @return ResourceGuide
     * @throws JsonException
     */
    private function getDefaultGuide(): ResourceGuide
    {
        $guideData = array_merge($this->defaultGuideConfig, array('name' => 'default'));

        return $this->serializer->deserialize(json_encode($guideData, JSON_THROW_ON_ERROR), ResourceGuide::class, 'json');
    }
}
