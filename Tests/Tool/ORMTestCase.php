<?php

namespace SG\CmsBundle\Tests\Tool;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\EventManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver as ORMAnnotationDriver;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;
use Metadata\MetadataFactory;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Doctrine\EventSubscriber\LanguageEnableableSubscriber;
use SG\CmsBundle\Api\Mapping\Driver\AnnotationDriver;
use SG\CmsBundle\Tests\Api\Fixture\Language;

/**
 * Class ORMTestCase
 * @package SG\CmsBundle\Tests\Tool
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class ORMTestCase extends TestCase
{
    private $em;
    private $evm;
    private $listener;

    public function getEntityManager()
    {
        if (!$this->em) {
            $this->em = $this->createEntityManager();
        }

        return $this->em;
    }

    public function createEntityManager()
    {
        $conn = array(
            'driver' => 'pdo_sqlite',
            'memory' => true,
        );

        AnnotationRegistry::registerFile(__DIR__ . '/../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php');

        $reader = new AnnotationReader();
        $reader = new CachedReader($reader, new ArrayCache());

        $config = Setup::createConfiguration(true);
        $config->setMetadataDriverImpl(new ORMAnnotationDriver($reader, array(__DIR__ . '/../Fixture')));

        $em = EntityManager::create($conn, $config, $this->getEventManager());

        $schemaTool = new SchemaTool($em);
        $schemaTool->createSchema(array_map(function ($class) use ($em) {
            return $em->getClassMetadata($class);
        }, $this->getFixtureClasses()));

        return $em;
    }

    public function getEventManager()
    {
        if (!$this->evm) {
            $this->evm = $this->createEventManager();
        }

        return $this->evm;
    }

    public function createEventManager()
    {
        $evm = new EventManager();
        $evm->addEventSubscriber($this->getTranslatableListener());

        return $evm;
    }

    public function getTranslatableListener(string $languageClass = null)
    {
        if (!$this->listener) {
            $this->listener = $this->createTranslatableListener($languageClass);
        }

        return $this->listener;
    }

    public function createTranslatableListener(string $languageClass = null)
    {
        return new LanguageEnableableSubscriber(new MetadataFactory(new AnnotationDriver(new AnnotationReader())), $languageClass ?: Language::class);
    }

    public function getFixtureClasses()
    {
        return array();
    }
}
