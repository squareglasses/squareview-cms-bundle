<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Frontend\Provider;

use JMS\Serializer\SerializerBuilder;
use SG\CmsBundle\Frontend\Exception\ModuleGuideConfigurationException;
use SG\CmsBundle\Frontend\Exception\ModuleGuideNotFoundException;
use SG\CmsBundle\Frontend\Model\ModuleGuide;
use SG\CmsBundle\Frontend\Provider\ModuleGuideProvider;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Frontend\Renderer\ModuleRendererInterface;
use SG\CmsBundle\Frontend\Renderer\ModuleRenderer;

/**
 * Class ModuleGuideProviderTest
 * @package SG\CmsBundle\Tests\Frontend\Provider
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleGuideProviderTest extends TestCase
{
    private ?ModuleGuideProvider $provider = null;

    public function setUp(): void
    {
        $serializer = SerializerBuilder::create()->build();
        $this->provider = new ModuleGuideProvider($serializer);
    }

    public function tearDown(): void
    {
        $this->provider = null;
    }

    public function testSetAvailableGuides()
    {
        // @TODO : A corriger, mauvais guides
        $guides = [
            "title_h1" => [
                "pattern" => '<h1>%value</h1>',
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ],
            "title_paragraph" => [
                "pattern" => '<h2>%title</h2><p>%paragraph</p>',
                "parameters" => [
                    "title" => [
                        "label" => '',
                        "description" => ''
                    ],
                    "paragraph" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ]
        ];

        $this->provider->setAvailableGuides($guides);
        $this->assertInstanceOf(ModuleGuide::class, $this->provider->getGuide("title_h1"));
    }

    public function testNoGuidesException()
    {
        $this->expectException(\Exception::class);
        $this->provider->getGuide("page");
    }

    public function testModuleGuideNotFoundException()
    {
        $guides = [
            "title_h1" => [
                "pattern" => '<h1>%value</h1>',
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ]
        ];

        $this->provider->setAvailableGuides($guides);

        $this->expectException(ModuleGuideNotFoundException::class);
        $this->provider->getGuide("page");
    }

    public function testModuleGuideConfigurationException()
    {
        $guides = [
            "title_h1" => [
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer"
            ]
        ];

        $this->provider->setAvailableGuides($guides);

        $this->expectException(ModuleGuideConfigurationException::class);
        $this->provider->getGuide("title_h1");
    }

    public function testGetRenderer()
    {
        $renderer = $this->createMock(ModuleRenderer::class);
        $this->provider->addRenderer(ModuleRenderer::class, $renderer);
        $guides = [
            "title_h1" => [
                "pattern" => '<h1>%value</h1>',
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ]
        ];
        $this->provider->setAvailableGuides($guides);
        $guide = $this->provider->getGuide("title_h1");
        $this->assertInstanceOf(ModuleRendererInterface::class, $this->provider->getRenderer($guide));
    }
}
