<?php

namespace SG\CmsBundle\Tests\Frontend\Model;

use SG\CmsBundle\Frontend\Exception\MetaTagValidationException;
use SG\CmsBundle\Frontend\Model\MetaTag;
use PHPUnit\Framework\TestCase;

/**
 * Class MetaTagTest
 * @package SG\SeoBundle\Tests\Model
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MetaTagTest extends TestCase
{
    private $metatag;

    public function setUp(): void
    {
        $this->metatag = new MetaTag();
    }

    public function tearDown(): void
    {
        $this->metatag = null;
    }

    public function testNameType()
    {
        $this->metatag->setName("title");
        $this->metatag->setType("name");
        $this->metatag->setContent("content");

        $this->assertEquals("<meta name=\"title\" content=\"content\" />", $this->metatag->getHtml());
    }

    public function testTitleType()
    {
        $this->metatag->setName("title");
        $this->metatag->setType("title");
        $this->metatag->setContent("content");

        $this->assertEquals("<title>content</title>", $this->metatag->getHtml());
    }

    public function testDefaultOrEmptyType()
    {
        $this->metatag->setName("title");
        $this->metatag->setType("");
        $this->metatag->setContent("content");

        $this->assertEquals("", $this->metatag->getHtml());

        $this->metatag->setType("badType");
        $this->assertEquals("", $this->metatag->getHtml());
    }

    public function testValidateDeserializationInput()
    {
        $correctInput = [
            "name" => "name",
            "type" => "type",
            "content" => "content"
        ];
        $this->assertTrue(MetaTag::validateDeserializationInput($correctInput));

        $badInput = [
            "name" => "name",
            "content" => "content"
        ];

        $this->expectException(MetaTagValidationException::class);
        $this->assertTrue(MetaTag::validateDeserializationInput($badInput));
    }
}
