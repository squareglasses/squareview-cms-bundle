<?php

namespace SG\CmsBundle\Tests\Frontend\Module\Model;

use SG\CmsBundle\Frontend\Model\ModuleGuide;
use PHPUnit\Framework\TestCase;

class ModuleGuideTest extends TestCase
{
    private $guide;

    public function setUp(): void
    {
        $this->guide = new ModuleGuide();
    }

    public function tearDown(): void
    {
        $this->guide = null;
    }

    public function testGetSetView()
    {
        $this->guide->setView('test.html.twig');
        $this->assertEquals('test.html.twig', $this->guide->getView());
    }

    public function testGetSetPattern()
    {
        $this->guide->setPattern('pattern');
        $this->assertEquals('pattern', $this->guide->getPattern());
    }

    public function testGetSetName()
    {
        $this->guide->setName('name');
        $this->assertEquals('name', $this->guide->getName());
    }

    public function testGetSetRenderer()
    {
        $this->guide->setRenderer('renderer');
        $this->assertEquals('renderer', $this->guide->getRenderer());
    }
}
