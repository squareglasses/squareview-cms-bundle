<?php

namespace SG\CmsBundle\Tests\Frontend\Module\Model;

use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Frontend\Model\ModuleGuide;
use SG\CmsBundle\Frontend\Model\Module;
use SG\CmsBundle\Frontend\Model\ModuleParameter;

class ModuleTest extends TestCase
{
    private $module;

    public function setUp(): void
    {
        $this->module = new Module();
    }

    public function tearDown(): void
    {
        $this->module = null;
    }

    public function testGetSetParameters()
    {
        $expected = ['param1', 'param2'];
        $this->module->setParameters($expected);
        $this->assertEquals($expected, $this->module->getParameters());
    }

    public function testGetSetLocale()
    {
        $this->module->setLocale('fr');
        $this->assertEquals('fr', $this->module->getLocale());
    }

    public function testGetSetGuideIdentifier()
    {
        $this->module->setGuideIdentifier('value');
        $this->assertEquals('value', $this->module->getGuideIdentifier());
    }

    public function testGetSetPosition()
    {
        $this->module->setPosition(1);
        $this->assertEquals(1, $this->module->getPosition());
    }

    public function testGetSetZone()
    {
        $this->module->setZone("zone");
        $this->assertEquals("zone", $this->module->getZone());
    }

    public function testGetSetGuide()
    {
        $guide = new ModuleGuide();
        $this->module->setGuide($guide);
        $this->assertEquals($guide, $this->module->getGuide());
    }

    public function testGetView()
    {
        $guide = new ModuleGuide();
        $guide->setView('view.html.twig');
        $this->module->setGuide($guide);
        $this->assertEquals('view.html.twig', $this->module->getView());
    }

    public function testGetViewException()
    {
        try {
            $this->module->getView();
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Exception::class, $e);
            $this->assertEquals('ResourceGuide not configured', $e->getMessage());
        }
    }

    public function testGetPattern()
    {
        $guide = new ModuleGuide();
        $guide->setPattern('pattern');
        $this->module->setGuide($guide);
        $this->assertEquals('pattern', $this->module->getPattern());
    }

    public function testGetPatternException()
    {
        try {
            $this->module->getPattern();
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Exception::class, $e);
            $this->assertEquals('ResourceGuide not configured', $e->getMessage());
        }
    }

    public function testGetViewParameters()
    {
        $moduleParameter1 = new ModuleParameter();
        $moduleParameter1->setIdentifier("title");
        $moduleParameter1->setValue("value title");

        $moduleParameter2 = new ModuleParameter();
        $moduleParameter2->setIdentifier("text");
        $moduleParameter2->setValue("value text");

        $this->module->setParameters([0 => $moduleParameter1, 1 => $moduleParameter2]);

        $viewParameters = $this->module->getViewParameters();
        $this->assertEquals('value title', $viewParameters['title']);
    }

    /*public function testSerialization()
    {
        $arrayValues = [
            '@id' => '/modules/4',
            '@type' => 'Module',
            'id' => 4,
            'resourceIdentifier' => '58d4365d-3303-11ea-b506-0242ac150003',
            'resourceClass' => 'App\Entity\Content',
            'guideIdentifier' => 'title_paragraph',
            'locale' => 'fr',
            'parameters' => [
                0 => [
                    'module' => '/modules/4',
                    'value' => 'titre',
                    'identifier' => 'title'
                ],
                1 => [
                    'module' => '/modules/4',
                    'value' => 'texte',
                    'identifier' => 'text'
                ]
            ]
        ];

        $serializer = new Serializer();
    }*/
}
