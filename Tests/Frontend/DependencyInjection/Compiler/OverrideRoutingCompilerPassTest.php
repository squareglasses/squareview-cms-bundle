<?php

namespace SG\CmsBundle\Tests\Frontend\DependencyInjection\Compiler;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\ModuleGuideProviderCompilerPass;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\OverrideRoutingCompilerPass;
use SG\CmsBundle\Frontend\Provider\ModuleGuideProvider;
use SG\CmsBundle\Frontend\Renderer\ModuleRenderer;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Frontend\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Class ModuleGuideProviderCompilerPassTest
 * @package SG\CmsBundle\Tests\Frontend\DependencyInjection\Compiler
 * @author Florent Chaboud <florent@squareglasses.com>
 */

class OverrideRoutingCompilerPassTest extends AbstractCompilerPassTestCase
{
    protected function registerCompilerPass(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new OverrideRoutingCompilerPass());
    }

    public function testProcess()
    {
        $routerDefinition = new Definition();
        $this->setDefinition(Router::class, $routerDefinition);

        $this->setDefinition("router", $routerDefinition);

        $this->compile();

        $this->assertContainerBuilderHasAlias("router", Router::class);
    }

    public function testProcessAlias()
    {
        $routerDefinition = new Definition();
        $this->setDefinition(Router::class, $routerDefinition);

        $this->container->setAlias("router", Router::class);

        $this->compile();

        $this->assertContainerBuilderHasAlias("router", Router::class);
    }

    public function testException()
    {
        $routerDefinition = new Definition();
        $this->setDefinition(Router::class, $routerDefinition);

        $this->expectException(ServiceNotFoundException::class);

        $this->compile();
    }
}
