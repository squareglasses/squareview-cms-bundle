<?php

namespace SG\CmsBundle\Tests\Frontend\DependencyInjection\Compiler;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractCompilerPassTestCase;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\ModuleGuideProviderCompilerPass;
use SG\CmsBundle\Frontend\Provider\ModuleGuideProvider;
use SG\CmsBundle\Frontend\Renderer\ModuleRenderer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

/**
 * Class ModuleGuideProviderCompilerPassTest
 * @package SG\CmsBundle\Tests\Frontend\DependencyInjection\Compiler
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleGuideProviderCompilerPassTest extends AbstractCompilerPassTestCase
{
    protected function registerCompilerPass(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new ModuleGuideProviderCompilerPass());
    }

    public function testProcess()
    {
        $config = [
            "title_h1" => [
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ],
            "title_paragraph" => [
                "parameters" => [
                    "title" => [
                        "label" => '',
                        "description" => ''
                    ],
                    "paragraph" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ]
        ];
        $this->setParameter("squareview.modules.guides", $config);

        $moduleGuideProviderDefinition = new Definition();
        $this->setDefinition(ModuleGuideProvider::class, $moduleGuideProviderDefinition);

        $rendererDefinition = new Definition();
        $this->setDefinition(ModuleRenderer::class, $rendererDefinition);

        $this->compile();

        $this->assertContainerBuilderHasService(ModuleGuideProvider::class);
        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            ModuleGuideProvider::class,
            'setAvailableGuides',
            [$config]
        );

        $this->assertContainerBuilderHasServiceDefinitionWithMethodCall(
            ModuleGuideProvider::class,
            'addRenderer',
            [ModuleRenderer::class, $rendererDefinition]
        );
    }
}
