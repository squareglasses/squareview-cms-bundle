<?php

namespace SG\CmsBundle\Tests\Frontend\Fixture;

use SG\CmsBundle\Frontend\Bag\AbstractPublicationBag as BaseBag;

/**
 * Class AbstractPublicationBag
 * @package SG\CmsBundle\Tests\Api\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PublicationBag extends BaseBag
{
}
