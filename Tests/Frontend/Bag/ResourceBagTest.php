<?php

namespace SG\CmsBundle\Tests\Frontend\Bag;

use JMS\Serializer\SerializerBuilder;
use SG\CmsBundle\Frontend\Model\MetaTag;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use PHPUnit\Framework\TestCase;
use SG\Contracts\Cms\Model\ModuleInterface;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;

/**
 * Class ResourceBagTest
 * @package SG\CmsBundle\Tests\Frontend\Bag
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceBagTest extends TestCase
{
    private ?ResourceBag $resourceBag = null;

    public function setUp(): void
    {
        $serializer = SerializerBuilder::create()->build();
        $this->resourceBag = new ResourceBag($serializer);
    }

    public function tearDown(): void
    {
        $this->resourceBag = null;
    }

    public function testSetGuide()
    {
        $guide = new ResourceGuide();
        $this->resourceBag->setGuide($guide);
        $this->assertEquals($guide, $this->resourceBag->getGuide());
        $this->assertInstanceOf(ResourceGuide::class, $this->resourceBag->getGuide());
    }

    public function testSetResource()
    {
        $resource = [
            'title' => 'string'
        ];
        $this->resourceBag->setResource($resource);
        $this->assertEquals($resource, $this->resourceBag->getResource());
        $this->assertIsArray($this->resourceBag->getResource());
    }

    public function testSetRawDatas()
    {
        $rawDatas = [
            "resource" => [
                "title" => "title 1",
                "id" => "6586e04f-5978-11ea-a27e-0242ac150002",
                "discriminator" => "page",
                "name" => "Page 1",
                "slug" => "page-1",
                "guide" => "page1",
            ],
            "modules" => [
                0 => [
                    "id" => 37,
                    "resourceIdentifier" => "6586e04f-5978-11ea-a27e-0242ac150002",
                    "resourceClass" => "App\Entity\Content",
                    "guideIdentifier" => "title_paragraph",
                    "parameters" => [
                        0 => [
                            "module" => "/modules/37",
                            "value" => "titre zone 2",
                            "identifier" => "title",
                        ]
                    ],
                    "position" => 1,
                    "locale" => "fr",
                    "zone" => "zone_modules2",
                ],
                1 => [
                    "id" => 38,
                    "resourceIdentifier" => "6586e04f-5978-11ea-a27e-0242ac150002",
                    "resourceClass" => "App\Entity\Content",
                    "guideIdentifier" => "title_h1",
                    "parameters" => [
                        0 => [
                            "module" => "/modules/38",
                            "value" => "titre",
                            "identifier" => "value",
                        ]
                    ],
                    "position" => 2,
                    "locale" => "fr",
                    "zone" => "zone_modules1",
                ]
            ],
            "meta_tags" => [
                0 => [
                    "id" => "6588978e-5978-11ea-a27e-0242ac150002",
                    "resourceIdentifier" => "6586e04f-5978-11ea-a27e-0242ac150002",
                    "resourceClass" => "App\Entity\Content",
                    "name" => "title",
                    "content" => "metatag titre 1",
                    "type" => "name",
                    "locale" => "fr",
                ],
                1 => [
                    "id" => "6588978e-5978-11ea-a27e-0242ac150003",
                    "resourceIdentifier" => "6586e04f-5978-11ea-a27e-0242ac150003",
                    "resourceClass" => "App\Entity\Content",
                    "name" => "description",
                    "content" => "metatag titre 3",
                    "type" => "name",
                    "locale" => "fr",
                ]
            ],
            "medias" => [
                0 => [
                    "id" => "658a87e1-5978-11ea-a27e-0242ac150002",
                    "identifier" => "media_id1",
                    "name" => "test.jpg",
                    "providerReference" => "test.jpg",
                    "providerName" => "image",
                    "providerMetadata" => [
                        "type" => "file",
                        "path" => "test.jpg",
                        "size" => 29323
                    ],
                    "width" => 500,
                    "height" => 375,
                    "length" => null,
                    "author" => null,
                    "alt" => null,
                ]
            ]
        ];

        $this->resourceBag->setRawDatas($rawDatas);
        $this->assertIsArray($this->resourceBag->getResource());
        $this->assertIsArray($this->resourceBag->getMetaTags());
        $this->assertInstanceOf(MetaTag::class, $this->resourceBag->getMetaTags()[0]);
        $this->assertIsArray($this->resourceBag->getMedias());
        $this->assertInstanceOf(MediaModelInterface::class, $this->resourceBag->getMedias()[0]);
        $this->assertIsArray($this->resourceBag->getModules());
        $this->assertInstanceOf(ModuleInterface::class, $this->resourceBag->getModules()[0]);
    }

    public function testGetMediaByIdentifier()
    {
        $rawDatas = [
            "resource" => [],
            "modules" => [],
            "meta_tags" => [],
            "medias" => [
                0 => [
                    "id" => "658a87e1-5978-11ea-a27e-0242ac150002",
                    "identifier" => "media_id1",
                    "name" => "test.jpg",
                    "providerReference" => "test.jpg",
                    "providerName" => "image",
                    "providerMetadata" => [
                        "type" => "file",
                        "path" => "test.jpg",
                        "size" => 29323
                    ],
                    "width" => 500,
                    "height" => 375,
                    "length" => null,
                    "author" => null,
                    "alt" => null,
                ]
            ]
        ];

        $this->resourceBag->setRawDatas($rawDatas);
        $this->assertNull($this->resourceBag->getMediaByIdentifier('unknown_identifier'));
        $this->assertInstanceOf(MediaModelInterface::class, $this->resourceBag->getMediaByIdentifier('media_id1'));
    }

    public function testMagicGettersAndSetters()
    {
        $rawDatas = [
            "resource" => [
                "title" => "title 1",
                "id" => "6586e04f-5978-11ea-a27e-0242ac150002",
                "discriminator" => "page",
                "name" => "Page 1",
                "slug" => "page-1",
                "guide" => "page1",
            ],
            "modules" => [],
            "meta_tags" => [],
            "medias" => []
        ];

        $this->resourceBag->setRawDatas($rawDatas);
        $this->assertEquals("title 1", $this->resourceBag->title);
        $this->assertNull($this->resourceBag->plop);
        $this->assertFalse($this->resourceBag->__isset("plop"));
    }
}
