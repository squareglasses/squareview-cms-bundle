<?php

namespace SG\CmsBundle\Tests\Frontend\Bag;

use JMS\Serializer\SerializerBuilder;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Tests\Frontend\Fixture\PublicationBag;
use SG\CmsBundle\Frontend\Model\MetaTag;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;

class WebsiteBagTest extends TestCase
{
    public function testSetRawDatas()
    {
        $rawDatas = [
            "resource" => [
                "title" => "title 1",
                "id" => "6586e04f-5978-11ea-a27e-0242ac150002",
                "discriminator" => "page",
                "name" => "Page 1",
                "slug" => "page-1",
                "guide" => "page1",
            ],
            "meta_tags" => [
                0 => [
                    "id" => "6588978e-5978-11ea-a27e-0242ac150002",
                    "resourceIdentifier" => "6586e04f-5978-11ea-a27e-0242ac150002",
                    "resourceClass" => "App\Entity\Content",
                    "name" => "title",
                    "content" => "metatag titre 1",
                    "type" => "name",
                    "locale" => "fr",
                ],
                1 => [
                    "id" => "6588978e-5978-11ea-a27e-0242ac150003",
                    "resourceIdentifier" => "6586e04f-5978-11ea-a27e-0242ac150003",
                    "resourceClass" => "App\Entity\Content",
                    "name" => "description",
                    "content" => "metatag titre 3",
                    "type" => "name",
                    "locale" => "fr",
                ]
            ],
            "medias" => [
                0 => [
                    "id" => "658a87e1-5978-11ea-a27e-0242ac150002",
                    "identifier" => "media_id1",
                    "name" => "test.jpg",
                    "providerReference" => "test.jpg",
                    "providerName" => "image",
                    "providerMetadata" => [
                        "type" => "file",
                        "path" => "test.jpg",
                        "size" => 29323
                    ],
                    "width" => 500,
                    "height" => 375,
                    "length" => null,
                    "author" => null,
                    "alt" => null,
                ]
            ]
        ];

        $serializer = SerializerBuilder::create()->build();
        $resourceBag = new PublicationBag($serializer);
        $resourceBag->setRawDatas($rawDatas);
        $this->assertIsArray($resourceBag->getResource());
        $this->assertIsArray($resourceBag->getMetaTags());
        $this->assertInstanceOf(MetaTag::class, $resourceBag->getMetaTags()[0]);
        $this->assertIsArray($resourceBag->getMedias());
        $this->assertInstanceOf(MediaModelInterface::class, $resourceBag->getMedias()[0]);
    }
}
