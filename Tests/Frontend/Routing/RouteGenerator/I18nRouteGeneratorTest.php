<?php

namespace SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator;

use SG\CmsBundle\Frontend\Routing\Exception\MissingLocaleException;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\I18nRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector\PostfixInflector;
use Symfony\Component\Routing\Route;
use PHPUnit\Framework\TestCase;

/**
 * Class I18nRouteGeneratorTest
 * @package SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class I18nRouteGeneratorTest extends TestCase
{
    public function testGenerateRoute()
    {
        $routeGenerator = new I18nRouteGenerator(new PostfixInflector());

        $defaults = ['_controller' => 'SG\\CmsBundle\Frontend\Controller\CmsRouteController::page'];
        $methods = ['GET', 'POST'];
        $requirements = ['path' => '.+'];
        $expected = new Route('/{_locale}/path', $defaults, $requirements, [], '', [], $methods, '');
        $route = $routeGenerator->generateRoute('route_name', 'path', 'fr');

        $this->assertEquals($expected, $route['route']);
        $this->assertEquals('route_name.fr', $route['name']);

        try {
            $routeGenerator->generateRoute('route_name', 'path');
        } catch (\Exception $e) {
            $this->assertInstanceOf(MissingLocaleException::class, $e);
        }
    }
}
