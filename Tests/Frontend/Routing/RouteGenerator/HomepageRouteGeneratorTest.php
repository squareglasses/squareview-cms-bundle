<?php

namespace SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator;

use SG\CmsBundle\Frontend\Routing\Exception\MissingLocaleException;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\HomepageRouteGenerator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Route;

/**
 * Class HomepageRouteGeneratorTest
 * @package SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HomepageRouteGeneratorTest extends TestCase
{
    /**
     * @var HomepageRouteGenerator
     */
    private $generator;


    public function setUp(): void
    {
        $this->generator = new HomepageRouteGenerator();
    }

    public function testSetMultilingual()
    {
        $this->generator->setMultilingual(true);
        $this->assertEquals(true, $this->generator->getMultilingual());
    }

    public function testSetDefaultLocale()
    {
        $this->generator->setDefaultLocale('fr');
        $this->assertEquals('fr', $this->generator->getDefaultLocale());
    }

    public function testGenerateRoute()
    {
        $this->generator->setMultilingual(true);
        $this->generator->setDefaultLocale('fr');
        $defaults = [
            '_controller' => 'SG\\CmsBundle\Frontend\Controller\CmsHomepageController::index',
            '_locale' => "fr"
        ];
        $methods = ['GET', 'POST'];
        $requirements = [];
        $expected = new Route('/{_locale}', $defaults, $requirements, [], '', [], $methods, '');
        $this->assertEquals($expected, $this->generator->generateRoute());

        $this->generator->setMultilingual(false);
        $defaults = [
            '_controller' => 'SG\\CmsBundle\Frontend\Controller\CmsHomepageController::index'
        ];
        $expected = new Route('/', $defaults, $requirements, [], '', [], $methods, '');
        $this->assertEquals($expected, $this->generator->generateRoute());
    }

    public function testException()
    {
        $this->generator->setMultilingual(true);
        try {
            $this->generator->generateRoute('route_name', 'path');
        } catch (\Exception $e) {
            $this->assertInstanceOf(MissingLocaleException::class, $e);
        }
    }
}
