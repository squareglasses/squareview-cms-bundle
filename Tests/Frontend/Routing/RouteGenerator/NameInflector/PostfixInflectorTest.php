<?php

namespace SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator\NameInflector;

use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector\PostfixInflector;

/**
 * Class PostfixInflectorTest
 * @package SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator\NameInflector
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PostfixInflectorTest extends TestCase
{
    public function testInflect()
    {
        $inflector = new PostfixInflector();
        $this->assertEquals('page.fr', $inflector->inflect('page', 'fr'));
    }
}
