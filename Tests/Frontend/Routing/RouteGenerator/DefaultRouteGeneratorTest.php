<?php

namespace SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator;

use SG\CmsBundle\Frontend\Routing\RouteGenerator\DefaultRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector\PostfixInflector;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Route;

/**
 * Class DefaultRouteGeneratorTest
 * @package SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DefaultRouteGeneratorTest extends TestCase
{
    public function testGenerateRoute()
    {
        $routeGenerator = new DefaultRouteGenerator(new PostfixInflector());

        $defaults = ['_controller' => 'SG\\CmsBundle\Frontend\Controller\CmsRouteController::page'];
        $methods = ['GET', 'POST'];
        $requirements = ['path' => '.+'];
        $expected = new Route('path', $defaults, $requirements, [], '', [], $methods, '');

        $route = $routeGenerator->generateRoute('route_name', 'path', null);

        $this->assertEquals($expected, $route['route']);
        $this->assertEquals('route_name', $route['name']);
    }
}
