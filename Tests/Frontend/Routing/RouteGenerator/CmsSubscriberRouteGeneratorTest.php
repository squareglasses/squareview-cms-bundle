<?php

namespace SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator;

use SG\CmsBundle\Frontend\Routing\RouteGenerator\CmsSubscriberRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\HomepageRouteGenerator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Route;

/**
 * Class CmsSubscriberRouteGeneratorTest
 * @package SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsSubscriberRouteGeneratorTest extends TestCase
{
    /**
     * @var HomepageRouteGenerator
     */
    private $generator;

    public function setUp(): void
    {
        $this->generator = new CmsSubscriberRouteGenerator();
    }

    public function testSetMultilingual()
    {
        $this->generator->setMultilingual(true);
        $this->assertEquals(true, $this->generator->getMultilingual());
    }

    public function testGenerateRoute()
    {
        $this->generator->setMultilingual(true);
        $defaults = ['_controller' => 'SG\\CmsBundle\Frontend\Controller\CmsRouteController::page'];
        $methods = ['GET', 'POST'];
        $requirements = ['path' => '.+'];
        $expected = new Route('/{_locale}/{path}', $defaults, $requirements, [], '', [], $methods, '');

        $this->assertEquals($expected, $this->generator->generateRoute());

        $this->generator->setMultilingual(false);
        $expected = new Route('/{path}', $defaults, $requirements, [], '', [], $methods, '');
        $this->assertEquals($expected, $this->generator->generateRoute());
    }
}
