<?php

namespace SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator;

use SG\CmsBundle\Frontend\Routing\RouteGenerator\LanguageSwitchRouteGenerator;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Routing\Route;

/**
 * Class LanguageSwitchRouteGeneratorTest
 * @package SG\CmsBundle\Tests\Frontend\Routing\RouteGenerator
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageSwitchRouteGeneratorTest extends TestCase
{
    /** @var LanguageSwitchRouteGenerator $generator */
    private $generator;


    public function setUp(): void
    {
        $this->generator = new LanguageSwitchRouteGenerator();
    }

    public function testSetMultilingual()
    {
        $this->generator->setMultilingual(true);
        $this->assertEquals(true, $this->generator->getMultilingual());
    }

    public function testGenerateRoute()
    {
        $this->generator->setMultilingual(false);
        $this->assertNull($this->generator->generateRoute());

        $this->generator->setMultilingual(true);
        $defaults = ['_controller' => 'SG\\CmsBundle\Frontend\Controller\LanguageSwitchController::switchLanguage'];
        $path = '/switch-language/{target}';
        $methods = ['GET'];
        $requirements = [];
        $expected = new Route($path, $defaults, $requirements, [], '', [], $methods, '');
        $this->assertEquals($expected, $this->generator->generateRoute());
    }
}
