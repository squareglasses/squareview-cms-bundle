<?php

namespace SG\CmsBundle\Tests\Frontend\Referer;

use SG\CmsBundle\Frontend\Referer\Referer;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

class RefererTest extends TestCase
{
    public function testResolve()
    {
        $request = Request::create('homepage', 'GET', ['page' => 1]);
        $request->attributes = new ParameterBag([
            '_route' => 'homepage',
            '_controller' => 'SG\CmsBundle\Controller\CmsHomepageController::index',
            '_locale' => 'fr',
            '_route_params' => [
                'paramName' => 'test'
            ]

        ]);
        //var_dump($request);die;
        $referer = new Referer($request);
        $this->assertEquals('http://localhost/homepage?page=1', $referer->getUri());
        $this->assertEquals('homepage', $referer->getRoute());
        $this->assertEquals('test', $referer->getRouteParameter('paramName'));
        $this->assertNull($referer->getRouteParameter('wrongParamName'));
        $this->assertIsArray($referer->getRouteParameters());
        $this->assertEquals(['page' => 1], $referer->getQueryParameters());
    }

//    public function testGetRouteParameter()
//    {
//
//    }
//
//    public function testGetQueryParameters()
//    {
//
//    }
//
//    public function testGetRouteParameters()
//    {
//
//    }
}
