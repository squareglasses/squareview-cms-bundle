<?php

namespace SG\CmsBundle\Tests\Frontend\Referer\EventListener;

use SG\CmsBundle\Frontend\Referer\EventListener\RefererListener;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Frontend\Referer\Referer;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Storage\MockArraySessionStorage;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\EventListener\ResponseListener;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\Session\Session;

class RefererListenerTest extends TestCase
{
    private $dispatcher;
    private $kernel;
    private $listener;

    protected function setUp(): void
    {
        $this->dispatcher = new EventDispatcher();
        $listener = new ResponseListener('UTF-8');
        $this->dispatcher->addListener(KernelEvents::RESPONSE, [$listener, 'onKernelResponse']);
        $this->kernel = $this->getMockBuilder('Symfony\Component\HttpKernel\HttpKernelInterface')->getMock();
        $this->listener = new RefererListener(['switch_language', '_wdt', '_profiler_home', '_profiler_search',
            '_profiler_search_bar', '_profiler_phpinfo', '_profiler_search_results', '_profiler_open_file',
            '_profiler', '_profiler_router', '_profiler_exception', '_profiler_exception_css',
            'api_entrypoint', '_errors'], ['/api/']);
    }
    protected function tearDown(): void
    {
        $this->dispatcher = null;
        $this->kernel = null;
        $this->listener = null;
    }

    public function testHandledRoute()
    {
        $response = new Response('foo');
        $request = $this->getRequest();
        $request->attributes = new ParameterBag([
            '_route' => 'homepage',
            '_controller' => 'SG\CmsBundle\Controller\CmsHomepageController::index',
            '_locale' => 'fr',
            '_route_params' => [
                'paramName' => 'test'
            ]
        ]);
        $event = new ResponseEvent($this->kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);
        $this->listener->onKernelResponse($event);

        $this->assertTrue($request->getSession()->has('sg.referer'));
        $this->assertInstanceOf(Referer::class, $request->getSession()->get('sg.referer'));
    }

    public function testExcludedRoute()
    {
        $response = new Response('foo');
        $request = $this->getRequest();
        $request->attributes = new ParameterBag([
            '_route' => 'switch_language',
            '_controller' => 'SG\CmsBundle\Controller\CmsHomepageController::index',
            '_locale' => 'fr',
            '_route_params' => [
                'paramName' => 'test'
            ]
        ]);
        $event = new ResponseEvent($this->kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);
        $this->listener->onKernelResponse($event);

        $this->assertFalse($request->getSession()->has('sg.referer'));
    }

    public function testExcludedPattern()
    {
        $response = new Response('foo');
        $request = Request::create('api/test', 'GET', ['page' => 1]);
        $session = new Session(new MockArraySessionStorage());
        $request->setSession($session);
        $request->attributes = new ParameterBag([
            '_route' => 'api_test',
            '_controller' => 'SG\CmsBundle\Controller\CmsHomepageController::index',
            '_locale' => 'fr'
        ]);
        $event = new ResponseEvent($this->kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);
        $this->listener->onKernelResponse($event);

        $this->assertFalse($request->getSession()->has('sg.referer'));
    }

    public function testEmptyRouteRequest()
    {
        $request = $this->getRequest();
        $response = new Response('foo');
        $event = new ResponseEvent($this->kernel, $request, HttpKernelInterface::MASTER_REQUEST, $response);

        $this->listener->onKernelResponse($event);
        $this->assertFalse($request->getSession()->has('sg.referer'));
    }

    public function testSubRequest()
    {
        $request = $this->getRequest();
        $response = new Response('foo');
        $event = new ResponseEvent($this->kernel, $request, HttpKernelInterface::SUB_REQUEST, $response);

        $this->listener->onKernelResponse($event);
        $this->assertFalse($request->getSession()->has('sg.referer'));
    }


    private function getRequest(): Request
    {
        $request = Request::create('homepage', 'GET', ['page' => 1]);
        $session = new Session(new MockArraySessionStorage());
        $request->setSession($session);
        return $request;
    }
}
