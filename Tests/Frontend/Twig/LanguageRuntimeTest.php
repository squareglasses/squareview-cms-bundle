<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Frontend\Twig;

use SG\CmsBundle\Common\HttpClient\Response\ResponseItem;
use SG\CmsBundle\Common\HttpClient\Response\ResponseParser;
use SG\CmsBundle\Frontend\Twig\LanguageRuntime;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Environment;

class LanguageRuntimeTest extends KernelTestCase
{
    private ?LanguageRuntime $extension = null;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->extension = $kernel->getContainer()->get(LanguageRuntime::class);
        $responseParser = $this->createMock(ResponseParser::class);
        $responseParser->method("parse")->willReturn([
            (new ResponseItem())
                ->set('id', 'en')
                ->set('name', 'Anglais')
                ->set('position', null)
                ->set('enabled', true),
            (new ResponseItem())
                ->set('id', 'fr')
                ->set('name', 'Français')
                ->set('position', null)
                ->set('enabled', true)
        ]);
        $this->extension->setApiClient($this->createMock(HttpClientInterface::class));
        $this->extension->setResponseParser($responseParser);
    }

    public function tearDown(): void
    {
        $this->extension = null;
    }

    public function testRenderLanguageSwitch()
    {
        $expected = "<ul>
        <li><a href=\"/switch-language/en\" rel=\"nofollow\">Anglais</a></li>
        <li><a href=\"/switch-language/fr\" rel=\"nofollow\">Français</a></li>
    </ul>
";
        $this->assertEquals($expected, $this->extension->renderLanguageSwitch());
    }
}
