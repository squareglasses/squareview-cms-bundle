<?php

namespace SG\CmsBundle\Tests\Frontend\Twig;

use JMS\Serializer\SerializerBuilder;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Model\MetaTag;
use SG\CmsBundle\Frontend\Twig\SeoExtension;
use PHPUnit\Framework\TestCase;
use Twig\TwigFunction;

class SeoExtensionTest extends TestCase
{
    private ?SeoExtension $extension = null;

    public function setUp(): void
    {
        $serializer = SerializerBuilder::create()->build();
        $websiteBag = new WebsiteBag($serializer);
        $this->extension = new SeoExtension($serializer, $websiteBag);
    }

    public function tearDown(): void
    {
        $this->extension = null;
    }

    public function testRenderMetasTags()
    {
        $tag1 = new MetaTag();
        $tag1->setType("name");
        $tag1->setName("title");
        $tag1->setContent("metatag titre 1");

        $tag2 = new MetaTag();
        $tag2->setType("name");
        $tag2->setName("description");
        $tag2->setContent("metatag description 1");

        $arrayTags = [$tag1, $tag2];

        // Testing default behavior
        $expected = "<meta name=\"title\" content=\"metatag titre 1\" />
        <meta name=\"description\" content=\"metatag description 1\" />
        ";
        $this->assertEquals($expected, $this->extension->renderMetasTags($arrayTags));

        // Testing override behavior with new tag
        $expected = "<meta name=\"title\" content=\"metatag titre 1\" />
        <meta name=\"description\" content=\"metatag description 1\" />
        <meta name=\"keywords\" content=\"keywords 1\" />
        ";
        $this->assertEquals($expected, $this->extension->renderMetasTags($arrayTags, [[
            "type" => "name",
            "name" => "keywords",
            "content" => "keywords 1"
        ]]));

        // Testing override behavior with existing tag
        $expected = "<meta name=\"title\" content=\"titre 1\" />
        <meta name=\"description\" content=\"metatag description 1\" />
        ";
        $this->assertEquals($expected, $this->extension->renderMetasTags($arrayTags, [[
            "type" => "name",
            "name" => "title",
            "content" => "titre 1"
        ]]));
    }

    public function testFallbackMetasTags()
    {
        $tag1 = new MetaTag();
        $tag1->setType("name");
        $tag1->setName("title");
        $tag1->setContent("metatag titre 1");

        $arrayTags = [$tag1];

        $serializer = SerializerBuilder::create()->build();
        $websiteBag = new WebsiteBag($serializer);
        $websiteTag1 = new MetaTag();
        $websiteTag1->setType("name");
        $websiteTag1->setName("title");
        $websiteTag1->setContent("metatag titre website");
        $websiteTag2 = new MetaTag();
        $websiteTag2->setType("name");
        $websiteTag2->setName("description");
        $websiteTag2->setContent("metatag description website");
        $websiteBag->setMetaTags([$websiteTag1, $websiteTag2]);

        $this->extension = new SeoExtension($serializer, $websiteBag);

        // Testing default behavior
        $expected = "<meta name=\"title\" content=\"metatag titre 1\" />
        <meta name=\"description\" content=\"metatag description website\" />
        ";
        $this->assertEquals($expected, $this->extension->renderMetasTags($arrayTags));

        // Testing override behavior with new tag
        $expected = "<meta name=\"title\" content=\"metatag titre 1\" />
        <meta name=\"keywords\" content=\"keywords 1\" />
        <meta name=\"description\" content=\"metatag description website\" />
        ";
        $this->assertEquals($expected, $this->extension->renderMetasTags($arrayTags, [[
            "type" => "name",
            "name" => "keywords",
            "content" => "keywords 1"
        ]]));
    }

    public function testGetFunctions()
    {
        $functions = [
            new TwigFunction(
                'render_meta_tags',
                [$this->extension, 'renderMetasTags'],
                ['is_safe' => ['html']]
            )
        ];
        $this->assertEquals($functions, $this->extension->getFunctions());
    }
}
