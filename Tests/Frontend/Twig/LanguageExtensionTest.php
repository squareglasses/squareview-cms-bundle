<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Frontend\Twig;

use SG\CmsBundle\Frontend\Twig\LanguageExtension;
use SG\CmsBundle\Frontend\Twig\LanguageRuntime;
use PHPUnit\Framework\TestCase;
use Twig\TwigFunction;

/**
 * Class LanguageExtensionTest
 * @package SG\CmsBundle\Tests\Frontend\Twig
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageExtensionTest extends TestCase
{
    public function testGetFunctions()
    {
        $extension = new LanguageExtension();
        $functions = [
            new TwigFunction(
                'language_switch',
                [LanguageRuntime::class, 'renderLanguageSwitch'],
                ['is_safe' => ['html']]
            )
        ];
        $this->assertEquals($functions, $extension->getFunctions());
    }
}
