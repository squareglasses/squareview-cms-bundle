<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Frontend\Twig;

use JMS\Serializer\SerializerBuilder;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use SG\CmsBundle\Common\ResourceGuide\Model\Zone;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Model\Module;
use SG\CmsBundle\Frontend\Model\ModuleGuide;
use SG\CmsBundle\Frontend\Model\ModuleParameter;
use SG\CmsBundle\Frontend\Provider\ModuleGuideProvider;
use SG\CmsBundle\Frontend\Renderer\ModuleRendererInterface;
use SG\CmsBundle\Frontend\Twig\ModuleExtension;
use SG\CmsBundle\Frontend\Renderer\ModuleRenderer;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Twig\TwigFunction;

/**
 * Class ModuleExtensionTest
 * @package SG\CmsBundle\Tests\Frontend\Twig
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleExtensionTest extends KernelTestCase
{
    private ?ModuleExtension $extension = null;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->extension = new ModuleExtension($this->getModuleGuideProvider(
            $kernel->getContainer()->get(ModuleRenderer::class)
        ));
    }

    public function tearDown(): void
    {
        $this->extension = null;
    }

    public function testGetFunctions()
    {
        $functions = [
            new TwigFunction(
                'render_modules',
                [$this->extension, 'renderModules'],
                ['is_safe' => ['html']]
            )
        ];
        $this->assertEquals($functions, $this->extension->getFunctions());
    }

    public function testRenderAllZOnesModules()
    {
        $cmsBag = new ResourceBag(SerializerBuilder::create()->build());
        $cmsBag->setGuide($this->getResourceGuide());

        $module1 = new Module();
        $module1->setGuideIdentifier("title_h1");
        $module1->setGuide($this->getModuleGuide());
        $module1->setPosition(1);
        $module1->setZone("zone1");
        $module1->setLocale('fr');
        $moduleParameter = new ModuleParameter();
        $moduleParameter->setIdentifier('value')->setValue("Titre");
        $module1->setParameters([$moduleParameter]);

        $module2 = new Module();
        $module2->setGuideIdentifier("title_paragraph");
        $module2->setGuide($this->getModuleGuide());
        $module2->setPosition(2);
        $module2->setZone("zone2");
        $module2->setLocale('fr');
        $moduleParameter1 = (new ModuleParameter())->setIdentifier('title')->setValue("Titre2");
        $moduleParameter2 = (new ModuleParameter())->setIdentifier('paragraph')->setValue("texte");
        $module2->setParameters([$moduleParameter1,$moduleParameter2]);

        $cmsBag->setModules([$module1, $module2]);

        $expected = "<h1>Titre</h1>
<h2>Titre2</h2><p>texte</p>
";
        $this->assertEquals($expected, $this->extension->renderModules($cmsBag));
    }

    public function testRenderOneZoneModules()
    {
        $cmsBag = new ResourceBag(SerializerBuilder::create()->build());
        $cmsBag->setGuide($this->getResourceGuide());

        $module1 = new Module();
        $module1->setGuideIdentifier("title_h1");
        $module1->setGuide($this->getModuleGuide());
        $module1->setPosition(1);
        $module1->setZone("zone1");
        $module1->setLocale('fr');
        $moduleParameter = new ModuleParameter();
        $moduleParameter->setIdentifier('value')->setValue("Titre");
        $module1->setParameters([$moduleParameter]);

        $module2 = new Module();
        $module2->setGuideIdentifier("title_paragraph");
        $module2->setGuide($this->getModuleGuide());
        $module2->setPosition(2);
        $module2->setZone("zone2");
        $module2->setLocale('fr');
        $moduleParameter1 = (new ModuleParameter())->setIdentifier('title')->setValue("Titre2");
        $moduleParameter2 = (new ModuleParameter())->setIdentifier('paragraph')->setValue("texte");
        $module2->setParameters([$moduleParameter1,$moduleParameter2]);

        $cmsBag->setModules([$module1, $module2]);

        $expected = "<h2>Titre2</h2><p>texte</p>
";
        $this->assertEquals($expected, $this->extension->renderModules($cmsBag, 'zone2'));
    }

    public function testRenderOrderModules()
    {
        $cmsBag = new ResourceBag(SerializerBuilder::create()->build());
        $cmsBag->setGuide($this->getResourceGuide());

        $module1 = new Module();
        $module1->setGuideIdentifier("title_h1");
        $module1->setGuide($this->getModuleGuide());
        $module1->setPosition(2);
        $module1->setZone("zone1");
        $module1->setLocale('fr');
        $moduleParameter = new ModuleParameter();
        $moduleParameter->setIdentifier('value')->setValue("Titre 1");
        $module1->setParameters([$moduleParameter]);

        $module2 = new Module();
        $module2->setGuideIdentifier("title_h1");
        $module2->setGuide($this->getModuleGuide());
        $module2->setPosition(1);
        $module2->setZone("zone1");
        $module2->setLocale('fr');
        $moduleParameter = (new ModuleParameter())->setIdentifier('value')->setValue("Titre 2");
        $module2->setParameters([$moduleParameter]);

        $cmsBag->setModules([$module1, $module2]);

        $expected = "<h1>Titre 2</h1>
<h1>Titre 1</h1>
";
        $this->assertEquals($expected, $this->extension->renderModules($cmsBag));
    }

    public function testRenderViewModule()
    {
        $cmsBag = new ResourceBag(SerializerBuilder::create()->build());
        $cmsBag->setGuide($this->getResourceGuide());

        $module1 = new Module();
        $module1->setGuideIdentifier("paragraph");
        $module1->setGuide($this->getModuleGuide());
        $module1->setPosition(1);
        $module1->setZone("zone1");
        $module1->setLocale('fr');
        $moduleParameter = new ModuleParameter();
        $moduleParameter->setIdentifier('value')->setValue("texte");
        $module1->setParameters([$moduleParameter]);

        $cmsBag->setModules([$module1]);

        $expected = "<p>texte</p>
";
        $this->assertEquals($expected, $this->extension->renderModules($cmsBag));
    }

    private function getModuleGuideProvider(ModuleRendererInterface $renderer): ModuleGuideProvider
    {
        $guideProvider = new ModuleGuideProvider(SerializerBuilder::create()->build());
        $guideProvider->addRenderer(ModuleRenderer::class, $renderer);
        $moduleGuides = [
            "title_h1" => [
                "pattern" => '<h1>%value</h1>',
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ],
            "title_paragraph" => [
                "pattern" => '<h2>%title</h2><p>%paragraph</p>',
                "parameters" => [
                    "title" => [
                        "label" => '',
                        "description" => ''
                    ],
                    "paragraph" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",
                "view" => null
            ],
            "paragraph" => [
                "pattern" => null,
                "view" => "modules/paragraph.html.twig",
                "parameters" => [
                    "value" => [
                        "label" => '',
                        "description" => ''
                    ]
                ],
                "enabled" => true,
                "renderer" => "SG\CmsBundle\Frontend\Renderer\ModuleRenderer",

            ]
        ];
        $guideProvider->setAvailableGuides($moduleGuides);

        return $guideProvider;
    }

    private function getModuleGuide(): ModuleGuide
    {
        $guide = new ModuleGuide();
        $guide->setName("title_h1");
        $guide->setView("module.html.twig");
        return $guide;
    }

    private function getResourceGuide(): ResourceGuide
    {
        $zone1 = new Zone();
        $zone1->setName("zone1");
        $zone1->setPosition(1);
        $zone1->setType("modules");

        $zone2 = new Zone();
        $zone2->setName("zone2");
        $zone2->setPosition(2);
        $zone2->setType("modules");

        $resourceGuide = new ResourceGuide();
        $resourceGuide->setName("page");
        $resourceGuide->setAction("action");
        $resourceGuide->setController("controller");
        $resourceGuide->setSitemap(true);
        $resourceGuide->setView("view.html.twig");
        $resourceGuide->setZones([$zone1, $zone2]);
        return $resourceGuide;
    }
}
