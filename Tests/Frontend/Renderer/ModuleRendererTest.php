<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Frontend\Renderer;

use SG\CmsBundle\Frontend\Model\Module;
use SG\CmsBundle\Frontend\Model\ModuleGuide;
use SG\CmsBundle\Frontend\Model\ModuleParameter;
use SG\CmsBundle\Frontend\Renderer\ModuleRenderer;
use SG\CmsBundle\Frontend\Renderer\ModuleRendererInterface;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Twig\Environment;

class ModuleRendererTest extends KernelTestCase
{
    private ?ModuleRenderer $renderer = null;

    public function setUp(): void
    {
        $kernel = self::bootKernel();
        $this->renderer = $kernel->getContainer()->get(ModuleRenderer::class);
    }

    public function tearDown(): void
    {
        $this->renderer = null;
    }

    public function testSetModule()
    {
        $module = new Module();
        $this->assertInstanceOf(ModuleRendererInterface::class, $this->renderer->setModule($module));
    }

    public function testRender()
    {
        $moduleGuide = new ModuleGuide();
        $moduleGuide->setPattern("<h1>%title</h1>");
        $moduleParameter = new ModuleParameter();
        $moduleParameter->setIdentifier("title");
        $moduleParameter->setValue("value");
        $module = new Module();
        $module->setParameters([$moduleParameter]);
        $module->setGuide($moduleGuide);
        $this->renderer->setModule($module);

        $expected = "<h1>value</h1>\n";
        $this->assertEquals($expected, $this->renderer->render());
    }

    public function testExecute()
    {
        $this->assertInstanceOf(ModuleRendererInterface::class, $this->renderer->execute());
    }
}
