<?php

require_once __DIR__ . '/../vendor/autoload.php';
require dirname(__DIR__)."/Tests/Functional/src/Kernel.php";

use Symfony\Component\Dotenv\Dotenv;
use Tests\Functional\Kernel;

if (!class_exists(Dotenv::class)) {
    throw new RuntimeException('Please run "composer require symfony/dotenv" to load the ".env" files configuring the application.');
} else {
    // load all the .env files
    (new Dotenv(false))->loadEnv(dirname(__DIR__).'/Tests/Functional/.env');
}

$_SERVER += $_ENV;
$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = ($_SERVER['APP_ENV'] ?? $_ENV['APP_ENV'] ?? null) ?: 'dev';
$_SERVER['APP_DEBUG'] = $_SERVER['APP_DEBUG'] ?? $_ENV['APP_DEBUG'] ?? 'prod' !== $_SERVER['APP_ENV'];
$_SERVER['APP_DEBUG'] = $_ENV['APP_DEBUG'] = (int) $_SERVER['APP_DEBUG'] || filter_var($_SERVER['APP_DEBUG'], FILTER_VALIDATE_BOOLEAN) ? '1' : '0';


function bootstrap(): void
{
    $kernel = new Kernel('test', false);
    $kernel->boot();

    $application = new \Symfony\Bundle\FrameworkBundle\Console\Application($kernel);
    $application->setAutoExit(false);

//    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
//        'command' => 'doctrine:database:drop',
//        '--if-exists' => '1',
//        '--force' => '1',
//    ]));
//
//    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
//        'command' => 'doctrine:database:create',
//    ]));
//
//    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
//        'command' => 'doctrine:schema:create',
//        '--quiet' => '1',
//        '--no-debug' => '1'
//    ]));
//
//    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
//        'command' => 'doctrine:fixtures:load',
//    ]));

//    $application->run(new \Symfony\Component\Console\Input\ArrayInput([
//        'command' => 'doctrine:query:sql',
//        'sql' => 'CREATE TABLE test (test VARCHAR(10))',
//    ]));

    $kernel->shutdown();
}

bootstrap();
