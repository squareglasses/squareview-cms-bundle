<?php

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\AbstractWebsite;
use SG\CmsBundle\Tests\Api\Fixture\Website;
use PHPUnit\Framework\TestCase;

class AbstractWebsiteTest extends TestCase
{
    private ?Website $entity = null;

    public function setUp(): void
    {
        $this->entity = new Website();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetName()
    {
        $this->entity->setName("string");
        $this->assertEquals("string", $this->entity->getName());
    }

    public function testGetResourceClass()
    {
        $this->assertEquals(Website::class, $this->entity->getResourceClass());
    }

    public function testGetDiscriminator()
    {
        $this->assertEquals("website", $this->entity->getDiscriminator());
    }

    public function testGetId()
    {
        $this->entity->setSlug("string");
        $this->entity->setId("id");
        $this->assertEquals("id", $this->entity->getId());
        $this->assertEquals("string", $this->entity->getResourceIdentifier());
    }
}
