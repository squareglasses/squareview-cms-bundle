<?php

namespace SG\CmsBundle\Api\Tests\Entity;

use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Tests\Api\Fixture\Language;

/**
 * Class LanguageTest
 * @package SG\CmsBundle\Api\Tests\Entity
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageTest extends TestCase
{
    private $entity;

    public function setUp(): void
    {
        $this->entity = new Language();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetPosition()
    {
        $this->entity->setPosition(1);
        $this->assertEquals(1, $this->entity->getPosition());
    }

    public function testGetId()
    {
        $this->entity->setId(1);
        $this->assertEquals(1, $this->entity->getId());
    }

    public function testGetName()
    {
        $this->entity->setName("string");
        $this->assertEquals("string", $this->entity->getName());
    }

    public function testGetEnabled()
    {
        $this->entity->setEnabled(true);
        $this->assertEquals(true, $this->entity->getEnabled());
    }
}
