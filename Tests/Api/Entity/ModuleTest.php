<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use SG\CmsBundle\Api\Entity\Module;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Entity\ModuleParameter;
use SG\CmsBundle\Tests\Api\Fixture\Modulable;

class ModuleTest extends TestCase
{
    private ?Module $entity = null;

    public function setUp(): void
    {
        $this->entity = new Module();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetResourceIdentifier()
    {
        $this->entity->setResourceIdentifier("string");
        $this->assertEquals("string", $this->entity->getResourceIdentifier());
    }

    public function testGetResourceClass()
    {
        $this->entity->setResourceClass("string");
        $this->assertEquals("string", $this->entity->getResourceClass());
    }

    public function testGetLocale()
    {
        $this->entity->setLocale("string");
        $this->assertEquals("string", $this->entity->getLocale());
    }

    public function testGetZone()
    {
        $this->entity->setZone("string");
        $this->assertEquals("string", $this->entity->getZone());
    }

    public function testGetId()
    {
        $this->entity->setId(1);
        $this->assertEquals(1, $this->entity->getId());
    }

    public function testSetResource()
    {
        $modulable = new Modulable();
        $this->entity->setResource($modulable);
        $this->assertEquals("resource_class", $this->entity->getResourceClass());
        $this->assertEquals("resource_identifier", $this->entity->getResourceIdentifier());
    }


    public function testGetParameters()
    {
        $moduleParameter = new ModuleParameter();
        $parameters = new ArrayCollection();
        $parameters[] = $moduleParameter;
        $this->entity->addParameter($moduleParameter);
        $this->assertEquals($parameters, $this->entity->getParameters());

        $this->entity->removeParameter($moduleParameter);
        $this->assertCount(0, $this->entity->getParameters());
    }

    public function testGetPosition()
    {
        $this->entity->setPosition(1);
        $this->assertEquals(1, $this->entity->getPosition());
    }

    public function testGetGuideIdentifier()
    {
        $this->entity->setGuideIdentifier("string");
        $this->assertEquals("string", $this->entity->getGuideIdentifier());
    }
}
