<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\Route;
use PHPUnit\Framework\TestCase;

class RouteTest extends TestCase
{
    private ?Route $entity = null;

    public function setUp(): void
    {
        $this->entity = new Route();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetId()
    {
        $this->entity->setId(1);
        $this->assertEquals(1, $this->entity->getId());
    }

    public function testGetPath()
    {
        $this->entity->setPath("path");
        $this->assertEquals("path", $this->entity->getPath());
    }

    public function testGetResourceIdentifier()
    {
        $this->entity->setResourceIdentifier("string");
        $this->assertEquals("string", $this->entity->getResourceIdentifier());
    }

    public function testGetLocale()
    {
        $this->entity->setLocale("fr");
        $this->assertEquals("fr", $this->entity->getLocale());
    }

    public function testGetResourceClass()
    {
        $this->entity->setResourceClass("string");
        $this->assertEquals("string", $this->entity->getResourceClass());
    }

    public function testGetName()
    {
        $this->entity->setName("string");
        $this->assertEquals("string", $this->entity->getName());
    }
}
