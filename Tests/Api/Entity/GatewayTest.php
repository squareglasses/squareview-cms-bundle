<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\Gateway;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Entity\Route;

class GatewayTest extends TestCase
{
    private ?Gateway $entity = null;

    public function setUp(): void
    {
        $route = new Route();
        $route->setPath('path');
        $route->setLocale('fr');
        $route->setResourceIdentifier("identifier");
        $this->entity = new Gateway($route);
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetPublicationIdentifier()
    {
        $this->assertEquals('fr/identifier', $this->entity->getPublicationIdentifier());
    }

    public function testGetResponse()
    {
        $response = ['content'];
        $this->entity->setResponse($response);
        $this->assertEquals($response, $this->entity->getResponse());
    }

    public function testGetRoute()
    {
        $this->assertInstanceOf(Route::class, $this->entity->getRoute());
    }

    public function testGetId()
    {
        $this->assertInstanceOf(Route::class, $this->entity->getId());
    }
}
