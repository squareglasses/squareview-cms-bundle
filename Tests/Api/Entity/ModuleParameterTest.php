<?php

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\ModuleParameter;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Entity\Module;

class ModuleParameterTest extends TestCase
{
    private ?ModuleParameter $entity = null;

    public function setUp(): void
    {
        $this->entity = new ModuleParameter();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetIdentifier()
    {
        $this->entity->setIdentifier("string");
        $this->assertEquals("string", $this->entity->getIdentifier());
    }

    public function testGetModule()
    {
        $module = new Module();
        $module->setLocale('fr');
        $this->entity->setModule($module);
        $this->assertEquals($module, $this->entity->getModule());
    }

    public function testGetValue()
    {
        $this->entity->setValue("string");
        $this->assertEquals("string", $this->entity->getValue());
    }
}
