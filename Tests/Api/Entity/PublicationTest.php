<?php

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\TranslationsPublication;
use PHPUnit\Framework\TestCase;

class PublicationTest extends TestCase
{
    private ?TranslationsPublication $entity = null;

    public function setUp(): void
    {
        $this->entity = new TranslationsPublication();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetLocale()
    {
        $this->entity->setLocale("string");
        $this->assertEquals("string", $this->entity->getLocale());
    }

    public function testGetResourceId()
    {
        $this->entity->setResourceId("string");
        $this->assertEquals("string", $this->entity->getResourceId());
    }

    public function testGetId()
    {
        $this->entity->setId("string");
        $this->assertEquals("string", $this->entity->getId());
    }

    public function testGetResponse()
    {
        $response = ['response'];
        $this->entity->setResponse($response);
        $this->assertEquals($response, $this->entity->getResponse());
    }

    public function testGetResourceClass()
    {
        $this->entity->setResourceClass("string");
        $this->assertEquals("string", $this->entity->getResourceClass());
    }
}
