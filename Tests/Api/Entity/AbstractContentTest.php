<?php

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\AbstractContent;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Tests\Api\Fixture\Content;

class AbstractContentTest extends TestCase
{
    private ?Content $entity = null;

    public function setUp(): void
    {
        $this->entity = new Content();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetResourceIdentifier()
    {
        $this->entity->setId("string");
        $this->assertEquals("string", $this->entity->getId());
        $this->assertEquals("string", $this->entity->getResourceIdentifier());
    }

    public function testGetResourceClass()
    {
        $this->assertEquals(Content::class, $this->entity->getResourceClass());
    }

    public function testGetName()
    {
        $this->entity->setName("string");
        $this->assertEquals("string", $this->entity->getName());
    }

    public function testGetDiscriminator()
    {
        $this->entity->setDiscriminator("string");
        $this->assertEquals("string", $this->entity->getDiscriminator());
    }
}
