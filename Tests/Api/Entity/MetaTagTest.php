<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Api\Entity;

use SG\CmsBundle\Api\Entity\MetaTag;
use PHPUnit\Framework\TestCase;

class MetaTagTest extends TestCase
{
    private ?MetaTag $entity = null;

    public function setUp(): void
    {
        $this->entity = new MetaTag();
    }

    public function tearDown(): void
    {
        $this->entity = null;
    }

    public function testGetName()
    {
        $this->entity->setName("string");
        $this->assertEquals("string", $this->entity->getName());
    }

    public function testGetLocale()
    {
        $this->entity->setLocale("string");
        $this->assertEquals("string", $this->entity->getLocale());
    }

    public function testGetResourceClass()
    {
        $this->entity->setResourceClass("string");
        $this->assertEquals("string", $this->entity->getResourceClass());
    }

    public function testGetId()
    {
        $this->entity->setId("string");
        $this->assertEquals("string", $this->entity->getId());
    }

    public function testGetType()
    {
        $this->entity->setType("string");
        $this->assertEquals("string", $this->entity->getType());
    }

    public function testGetResourceIdentifier()
    {
        $this->entity->setResourceIdentifier("string");
        $this->assertEquals("string", $this->entity->getResourceIdentifier());
    }

    public function testGetContent()
    {
        $this->entity->setContent("string");
        $this->assertEquals("string", $this->entity->getContent());
    }
}
