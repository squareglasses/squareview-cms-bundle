<?php

namespace SG\CmsBundle\Tests\Api\Mapping\Metadata;

use SG\CmsBundle\Tests\Api\Fixture\Translatable;
use SG\CmsBundle\Tests\Tool\ORMTestCase;

/**
 * Class ClassMetadataFactoryTest
 * @package SG\CmsBundle\Tests\Api\Mapping\Metadata
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ClassMetadataFactoryTest extends ORMTestCase
{
    public function testTranslatableMetadata()
    {
        $classMetadata = $this->getTranslatableListener()
            ->getMetadataFactory()
            ->getMetadataForClass(Translatable::class);

        $this->assertEquals(Translatable::class, $classMetadata->name);
    }
}
