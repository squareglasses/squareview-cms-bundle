<?php

namespace SG\CmsBundle\Tests\Api\Mapping\Metadata;

use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Mapping\TranslatableMetadata;
use SG\CmsBundle\Tests\Api\Fixture\Language;

/**
 * Class ClassMetadataTest
 * @package SG\CmsBundle\Tests\Api\Mapping\Metadata
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ClassMetadataTest extends TestCase
{
    public function testTranslatableSerialization()
    {
        $meta = new TranslatableMetadata(Language::class);
        $meta->name = 'SG\\CmsBundle\\Tests\Api\\Fixture\\Language';

        $string = serialize($meta);
        $copy = unserialize($string);

        $this->assertEquals($meta, $copy);
    }
}
