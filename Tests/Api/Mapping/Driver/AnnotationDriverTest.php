<?php

namespace SG\CmsBundle\Tests\Api\Mapping\Driver;

use Doctrine\Common\Annotations\AnnotationReader;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Mapping\Driver\AnnotationDriver;
use SG\CmsBundle\Tests\Api\Fixture\Language;
use SG\CmsBundle\Tests\Api\Fixture\Translatable;

/**
 * Class AnnotationDriverTest
 * @package SG\CmsBundle\Tests\Api\Mapping\Driver
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AnnotationDriverTest extends TestCase
{
    protected function getDriver()
    {
        return new AnnotationDriver(new AnnotationReader());
    }

    public function testLoadTranslatableMetadata()
    {
        $metadata = $this->getDriver()->loadMetadataForClass(new \ReflectionClass(Translatable::class));

        $this->assertEquals($metadata->name, Translatable::class);
    }

    public function testNullLoadTranslatableMetadata()
    {
        $metadata = $this->getDriver()->loadMetadataForClass(new \ReflectionClass(Language::class));

        $this->assertNull($metadata);
    }
}
