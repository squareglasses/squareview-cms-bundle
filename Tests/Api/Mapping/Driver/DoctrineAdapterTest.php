<?php

namespace SG\CmsBundle\Tests\Api\Mapping\Driver;

use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver as ORMAnnotationDriver;
use Doctrine\Persistence\Mapping\Driver\PHPDriver;
use Doctrine\Persistence\Mapping\Driver\SymfonyFileLocator;
use Metadata\Driver\DriverChain;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Api\Mapping\Driver\AnnotationDriver;
use SG\CmsBundle\Api\Mapping\Driver\DoctrineAdapter;

/**
 * Class DoctrineAdapterTest
 * @package SG\CmsBundle\Tests\Api\Mapping\Driver
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DoctrineAdapterTest extends TestCase
{
    public function testFromMetadataDriver()
    {
        $reader = new AnnotationReader();

        $omDriver = new MappingDriverChain();
        $omDriver->addDriver(new ORMAnnotationDriver($reader), 'SG\\TranslatableBundle\\Tests\\Fixture\\Translatable');

        $expected = new DriverChain(array(
            new AnnotationDriver($reader)
        ));

        $driver = DoctrineAdapter::fromMetadataDriver($omDriver);
        $this->assertEquals($expected, $driver);
    }

    public function testException()
    {
        $omDriver = new MappingDriverChain();
        $locator = new SymfonyFileLocator(array(), '.php');
        $omDriver->addDriver(new PHPDriver($locator), 'SG\\TranslatableBundle\\Tests\\Fixture\\Translatable');

        $this->expectException(\InvalidArgumentException::class);
        $driver = DoctrineAdapter::fromMetadataDriver($omDriver);
    }
}
