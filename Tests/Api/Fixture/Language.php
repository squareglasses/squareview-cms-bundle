<?php

namespace SG\CmsBundle\Tests\Api\Fixture;

use SG\CmsBundle\Api\Entity\Language as BaseLanguage;
use SG\CmsBundle\Api\Contracts\LanguageInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class Language
 * @package SG\TranslatableBundle\Tests\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 * @ORM\Entity
 */
class Language extends BaseLanguage implements LanguageInterface
{
}
