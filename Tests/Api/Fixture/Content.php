<?php

namespace SG\CmsBundle\Tests\Api\Fixture;

use SG\CmsBundle\Api\Entity\AbstractContent;

/**
 * Class Language
 * @package SG\CmsBundle\Tests\Api\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Content extends AbstractContent
{
}
