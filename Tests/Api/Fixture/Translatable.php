<?php

namespace SG\CmsBundle\Tests\Api\Fixture;

use Doctrine\ORM\Mapping as ORM;
use SG\Contracts\Cms\Translatable as TranslatableInterface;
use SG\CmsBundle\Api\Contracts\LanguageEnableableTrait;

/**
 * Class Translatable
 * @package SG\TranslatableBundle\Tests\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 * @ORM\Entity
 */
class Translatable implements TranslatableInterface
{
    use LanguageEnableableTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\Column(name="id", type="integer")
     */
    private $id;
}
