<?php

namespace SG\CmsBundle\Tests\Api\Fixture;

/**
 * Class Modulable
 * @package SG\CmsBundle\Tests\Api\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Modulable implements \SG\Contracts\Cms\Modulable
{
    public function getResourceIdentifier(): string
    {
        return 'resource_identifier';
    }

    public function getResourceClass(): string
    {
        return 'resource_class';
    }
}
