<?php

namespace SG\CmsBundle\Tests\Api\Fixture;

use SG\CmsBundle\Api\Entity\AbstractWebsite;

/**
 * Class Website
 * @package SG\CmsBundle\Tests\Api\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Website extends AbstractWebsite
{
}
