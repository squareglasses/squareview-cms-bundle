<?php

namespace SG\CmsBundle\Tests\Api\Fixture;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class MetaTaggable
 * @package SG\CmsBundle\Tests\Frontend\Fixture
 * @author Florent Chaboud <florent@squareglasses.com>
 * @ORM\Entity
 */
class MetaTaggable implements \SG\Contracts\Cms\MetaTaggable
{
    /**
     * @var string $id The unique guuid of the tag
     *
     * @ORM\Id()
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     *
     */
    protected $id;

    /**
     * @var string $resourceIdentifier Identify the associated resource with a unique id.
     *   It should be the guuid of the resource.
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $resourceIdentifier;

    /**
     * @var string $resourceClass Identify the associated resource with its class name.
     *
     * @ORM\Column(type="string", length=255)
     */
    protected $resourceClass;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return string
     */
    public function setId(string $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceIdentifier(): string
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param string $resourceIdentifier
     * @return $this
     */
    public function setResourceIdentifier(string $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): string
    {
        return $this->resourceClass;
    }

    /**
     * @param string $resourceClass
     * @return $this
     */
    public function setResourceClass(string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }
}
