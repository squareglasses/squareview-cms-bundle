<?php

namespace SG\CmsBundle\Tests\Api\Manager;

use PHPUnit\Framework\TestCase;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\Common\Annotations\AnnotationRegistry;
use Doctrine\Common\Annotations\CachedReader;
use Doctrine\Common\Cache\ArrayCache;
use Doctrine\Common\EventManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\Setup;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Tests\Api\Fixture\MetaTaggable;

/**
 * Class MetaTagManagerTest
 * @package SG\CmsBundle\Tests\Api\Manager
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MetaTagManagerTest extends TestCase
{
    private $em;
    private $evm;

    private $manager;

    public function setUp(): void
    {
        $this->manager = new MetaTagManager($this->getEntityManager());
    }

    public function tearDown(): void
    {
        $this->manager = null;
    }

    public function testAddMetaTagToResource()
    {
        $resource = new MetaTaggable();
        $resource->setResourceIdentifier("identifier");
        $resource->setResourceClass("class");
        $this->em->persist($resource);
        $this->em->flush();
        $this->em->clear();

        $resource = $this->manager->addMetaTagToResource($resource, "title", "title content", "fr", "name");
        $entity = $this->em->getRepository(MetaTag::class)->findOneByName("title");

        $this->assertNotNull($entity);
    }

    public function testFindByResource()
    {
        $resource = new MetaTaggable();
        $resource->setResourceIdentifier("identifier");
        $resource->setResourceClass("class");
        $this->em->persist($resource);
        $this->em->flush();
        $this->em->clear();

        $resource = $this->manager->addMetaTagToResource($resource, "title", "title content", "fr", "name");
        $entity = $this->em->getRepository(MetaTag::class)->findOneByName("title");

        $this->assertNotNull($entity);

        $foundResource = $this->manager->findByResource($resource);
        $this->assertNotNull($foundResource);
        $this->assertContains($entity, $foundResource);

        $foundResource = $this->manager->findByResource($resource, 'fr', true);
        $this->assertNotNull($foundResource);
        $this->assertIsArray($foundResource);
    }

    private function getEntityManager()
    {
        if (!$this->em) {
            $this->em = $this->createEntityManager();
        }

        return $this->em;
    }

    public function createEntityManager()
    {
        $conn = array(
            'driver' => 'pdo_sqlite',
            'memory' => true,
        );

        AnnotationRegistry::registerFile(__DIR__ . '/../../../vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/DoctrineAnnotations.php');

        $reader = new AnnotationReader();
        $reader = new CachedReader($reader, new ArrayCache());

        $config = Setup::createConfiguration(true);
        $config->setMetadataDriverImpl(new AnnotationDriver($reader, array(__DIR__ . '/../Fixture')));

        $em = EntityManager::create($conn, $config, $this->getEventManager());

        $schemaTool = new SchemaTool($em);
        $schemaTool->createSchema(array_map(function ($class) use ($em) {
            return $em->getClassMetadata($class);
        }, $this->getFixtureClasses()));

        return $em;
    }

    public function getEventManager()
    {
        if (!$this->evm) {
            $this->evm = $this->createEventManager();
        }

        return $this->evm;
    }

    public function createEventManager()
    {
        $evm = new EventManager();

        return $evm;
    }

    public function getFixtureClasses()
    {
        $fixtures = array(
            MetaTag::class,
            MetaTaggable::class
        );

        return $fixtures;
    }
}
