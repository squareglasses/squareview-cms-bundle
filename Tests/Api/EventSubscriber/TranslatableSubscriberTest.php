<?php

namespace SG\CmsBundle\Tests\Api\EventSubscriber;

use SG\CmsBundle\Tests\Api\Fixture\AbstractLanguage;
use SG\CmsBundle\Tests\Api\Fixture\Language;
use SG\CmsBundle\Tests\Api\Fixture\Translatable;
use SG\CmsBundle\Tests\Tool\ORMTestCase;

/**
 * Class TranslatableSubscriberTest
 * @package SG\CmsBundle\Tests\Api\EventSubscriber
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslatableSubscriberTest extends ORMTestCase
{
    public function getFixtureClasses()
    {
        $fixtures = array(
            Translatable::class,
            Language::class,
            AbstractLanguage::class
        );

        return $fixtures;
    }

    public function getEntities()
    {
        return array(
            array(Translatable::class),
        );
    }

    /**
     * @dataProvider getEntities
     */
    public function testPostLoad($translatableClass)
    {
        // setup
        $em = $this->getEntityManager();

        $french  = new Language();
        $french->setId('fr');
        $french->setName('French');
        $em->persist($french);
        $em->flush();

        $entity = new $translatableClass();
        $entity->addLanguage($french);

        $em->persist($entity);
        $em->flush();
        $em->clear();
        // end setup

        $entity = $em->find($translatableClass, 1);

        $this->assertNotNull($entity);
        $this->assertCount(1, $entity->getLanguages());
    }

    public function testPostLoadException()
    {
        $this->getTranslatableListener("Language");
        $this->expectException(\Exception::class);
        $this->getEntityManager();
    }

    public function testCachedMetadatas()
    {
        $listener = $this->getTranslatableListener();
        $listener->getClassMetadata(Translatable::class);
        $listener->getClassMetadata(Translatable::class);
        $this->assertTrue($listener->isCached(Translatable::class));
    }
}
