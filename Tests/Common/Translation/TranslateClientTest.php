<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Common\Translation;

use SG\CmsBundle\Common\Translation\Client\EnvironmentClient;
use SG\CmsBundle\Common\Translation\Client\ProjectClient;
use SG\CmsBundle\Common\Translation\Serializer\CatalogNormalizer;
use SG\CmsBundle\Common\Translation\TranslateClient;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class TranslateClientTest extends TestCase
{
    private $serializer;
    /*private ?TranslateClient $client = null;

    public function setUp(): void
    {
        $httpClient = new MockHttpClient($response, 'https://translate.squareglasses.com/');

        $encoders = [new JsonEncoder()];
        $normalizers = [new CatalogNormalizer(new ObjectNormalizer())];
        $this->serializer = new Serializer($normalizers, $encoders);
        $this->client = new SquareTranslateClient($this->serializer, $httpClient, $project, $apiKey, $environment);

        $this->client = new TranslateClient([
            "translations_fallback_locale" => "fr",
            "locale" => "fr",
            "classes" => [
                "content" => "App\Entity\Content",
                "language" => "SG\CmsBundle\Api\Entity\Language",
                "website" => "App\Entity\Website"
            ],
            "repositories" => [
                "website" => "App\Repository\WebsiteRepository"
            ]
        ]);
    }

    public function tearDown(): void
    {
        $this->client = null;
    }*/

    public function testSetEnvironmentIdentifier()
    {
        $client = $this->buildDefaultClient();
        $client->setEnvironmentIdentifier("string");
        $this->assertEquals("string", $client->getEnvironmentIdentifier());
    }

    public function testSetApiKey()
    {
        $client = $this->buildDefaultClient();
        $client->setApiKey("string");
        $this->assertEquals("string", $client->getApiKey());
    }

    public function testSetProjectIdentifier()
    {
        $client = $this->buildDefaultClient();
        $client->setProjectIdentifier("string");
        $this->assertEquals("string", $client->getProjectIdentifier());
    }

    public function testProject()
    {
        $response = new MockResponse(
            'response',
            ['http_code' => 200]
        );
        $client = $this->buildClient($response, "cmsbundle_tests", "apikey", "test");
        $project = $client->project();
        $this->assertInstanceOf(ProjectClient::class, $project);
    }

    /*public function testEnvironment()
    {
        $response = new MockResponse(
            'response',
            ['http_code' => 200]
        );
        $client = $this->buildClient($response, "cmsbundle_tests", "apikey", "test");
        $project = $client->project()->get("cmsbundle_tests");
        $environment = $client->environment($project);
        $this->assertInstanceOf(EnvironmentClient::class, $environment);
    }*/

    private function buildClient($response, $project, $apiKey, $environment): TranslateClient
    {
        $httpClient = new MockHttpClient($response, 'https://translate.squareglasses.com/');

        $encoders = [new JsonEncoder()];
        $normalizers = [new CatalogNormalizer(new ObjectNormalizer())];
        $this->serializer = new Serializer($normalizers, $encoders);
        $client = new TranslateClient($this->serializer, $httpClient);
        $client->setProjectIdentifier($project);
        $client->setEnvironmentIdentifier($environment);
        $client->setApiKey($apiKey);

        return $client;
    }


    private function buildDefaultClient(): TranslateClient
    {
        $response = new MockResponse(
            'response',
            ['http_code' => 200]
        );
        $httpClient = new MockHttpClient($response, 'https://translate.squareglasses.com/');
        $encoders = [new JsonEncoder()];
        $normalizers = [new CatalogNormalizer(new ObjectNormalizer())];
        $serializer = new Serializer($normalizers, $encoders);
        return new TranslateClient($serializer, $httpClient);
    }



    /*





    public function testCatalog()
    {

    }
*/
}
