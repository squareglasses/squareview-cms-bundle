<?php

namespace SG\CmsBundle\Tests\Common\Translation\Client;

use SG\CmsBundle\Common\Translation\Client\LocaleClient;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\Translation\Model\Locale;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class LocaleClientTest extends BaseClientTest
{
    public function testGet()
    {
        $responses = [new MockResponse(
            '{"id":3,"domain":"homepage_accueil","zone":"properties","translationKeys":[]}',
            ['http_code' => 200]
        )];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new LocaleClient($serializer, $httpClient, 'api_key', $this->getProject());
        $locale = $client->get('catalog', 'zone');
        $this->assertInstanceOf(Locale::class, $locale);
    }

    private function getProject(): Project
    {
        $project = new Project("project", "api_key");
        $project->setSlug("project");
        return $project;
    }
}
