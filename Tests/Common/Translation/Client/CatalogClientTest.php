<?php

namespace SG\CmsBundle\Tests\Common\Translation\Client;

use SG\CmsBundle\Common\Translation\Client\CatalogClient;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\Translation\Exception\ResourceNotFoundException;
use SG\CmsBundle\Common\Translation\Exception\UnknownException;
use SG\CmsBundle\Common\Translation\Model\Catalog;
use SG\CmsBundle\Common\Translation\Model\Environment;
use SG\CmsBundle\Common\Translation\Model\Project;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class CatalogClientTest extends BaseClientTest
{
    public function testGet()
    {
        $responses = [new MockResponse(
            '{"id":3,"domain":"homepage_accueil","zone":"properties","translationKeys":[]}',
            ['http_code' => 200]
        )];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new CatalogClient($serializer, $httpClient, 'api_key', $this->getProject(), $this->getEnvironment());
        $catalog = $client->get('catalog', 'zone');
        $this->assertInstanceOf(Catalog::class, $catalog);
    }

    public function testPost()
    {
        $responses = [new MockResponse(
            '{"@context":"\/api\/contexts\/Catalog","@id":"\/api\/catalogs\/5","@type":"Catalog","id":5,"domain":"homepage_accueil","zone":"properties","translationKeys":[]}',
            ['http_code' => 200]
        )];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new CatalogClient($serializer, $httpClient, 'api_key', $this->getProject(), $this->getEnvironment());
        $catalog = $client->post('domain', 'zone');
        $this->assertInstanceOf(Catalog::class, $catalog);
    }

    public function testGetOrCreate()
    {
        $responses = [
            new MockResponse("[]"),
            new MockResponse(
                '{"@context":"\/api\/contexts\/Catalog","@id":"\/api\/catalogs\/5","@type":"Catalog","id":5,"domain":"homepage_accueil","zone":"properties","translationKeys":[]}'
            )
        ];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new CatalogClient($serializer, $httpClient, 'api_key', $this->getProject(), $this->getEnvironment());
        $catalog = $client->getOrCreate('domain', 'zone');
        $this->assertInstanceOf(Catalog::class, $catalog);
    }

    public function testResourceNotFoundException()
    {
        $responses = [new MockResponse('', ['http_code' => 404])];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new CatalogClient($serializer, $httpClient, 'api_key', $this->getProject(), $this->getEnvironment());
        $this->expectException(ResourceNotFoundException::class);
        $client->get('domain', 'zone');
    }

    public function testUnknownException()
    {
        $responses = [new MockResponse('', ['http_code' => 400])];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new CatalogClient($serializer, $httpClient, 'api_key', $this->getProject(), $this->getEnvironment());
        $this->expectException(UnknownException::class);
        $client->get('domain', 'zone');
    }

    /*

    public function testTranslate()
    {

    }

    public function testGetTranslations()
    {

    }

    public function testGetTranslationValue()
    {

    }*/

    private function getProject(): Project
    {
        $project = new Project("project", "api_key");
        $project->setSlug("project");
        return $project;
    }

    private function getEnvironment(): Environment
    {
        $environment = new Environment();
        $environment->setName('test');
        return $environment;
    }
}
