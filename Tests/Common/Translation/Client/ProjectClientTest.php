<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Common\Translation\Client;

use SG\CmsBundle\Common\Translation\Client\ProjectClient;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\Translation\Exception\ResourceNotFoundException;
use SG\CmsBundle\Common\Translation\Exception\UnknownException;
use SG\CmsBundle\Common\Translation\Model\Project;
use SG\CmsBundle\Common\Translation\Serializer\CatalogNormalizer;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class ProjectClientTest extends BaseClientTest
{
    public function testGet()
    {
        $responses = [
            new MockResponse(
                '{
                "id":"2f8b5fa1-6859-11ea-af9d-0242ac120003",
                "name":"Eyesight",
                "slug":"eyesight",
                "apiKey":"testapikey",
                "environments":[
                    "\/api\/environments\/17",
                    "\/api\/environments\/18"
                ],
                "locales":[
                    "\/api\/locales\/en",
                    "\/api\/locales\/fr"
                ]
            }',
                ['http_code' => 200]
            ),
            new MockResponse('{"id":"en","name":"English","project":["\/api\/projects\/2f8b5fa1-6859-11ea-af9d-0242ac120003"]}'),
            new MockResponse('{"id":"fr","name":"Fran\u00e7ais","project":["\/api\/projects\/2f8b5fa1-6859-11ea-af9d-0242ac120003"]}')
        ];
        $serializer = $this->getSerializer();
        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new ProjectClient($serializer, $httpClient, 'api_key');
        $project = $client->get('project_identifier');
        $this->assertInstanceOf(Project::class, $project);
    }

    public function testResourceNotFoundException()
    {
        $responses = [new MockResponse('', ['http_code' => 404])];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new ProjectClient($serializer, $httpClient, 'api_key');
        $this->expectException(ResourceNotFoundException::class);
        $client->get('project_identifier');
    }

    public function testUnknownException()
    {
        $responses = [new MockResponse('', ['http_code' => 400])];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new ProjectClient($serializer, $httpClient, 'api_key');
        $this->expectException(UnknownException::class);
        $client->get('project_identifier');
    }
}
