<?php

declare(strict_types=1);

namespace SG\CmsBundle\Tests\Common\Translation\Client;

use SG\CmsBundle\Common\Translation\Exception\ResourceNotFoundException;
use SG\CmsBundle\Common\Translation\Client\EnvironmentClient;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\Translation\Exception\UnknownException;
use SG\CmsBundle\Common\Translation\Model\Environment;
use SG\CmsBundle\Common\Translation\Model\Project;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;

class EnvironmentClientTest extends BaseClientTest
{
    public function testGet()
    {
        $responses = [
            new MockResponse(
                '{"id":5,"name":"dev","version":"2020-03-13T17:42:41+01:00","catalogs":["\/api\/catalogs\/3"]}',
                ['http_code' => 200]
            )
        ];
        $serializer = $this->getSerializer();
        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new EnvironmentClient($serializer, $httpClient, 'api_key', $this->getProject());
        $environment = $client->get('environment_id');
        $this->assertInstanceOf(Environment::class, $environment);
    }

    public function testResourceNotFoundException()
    {
        $responses = [new MockResponse('', ['http_code' => 404])];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new EnvironmentClient($serializer, $httpClient, 'api_key', $this->getProject());
        $this->expectException(ResourceNotFoundException::class);
        $client->get('environment_id');
    }

    public function testUnknownException()
    {
        $responses = [new MockResponse('', ['http_code' => 400])];
        $serializer = $this->getSerializer();

        $httpClient = new MockHttpClient($responses, 'https://translate.squareglasses.com/');

        $client = new EnvironmentClient($serializer, $httpClient, 'api_key', $this->getProject());
        $this->expectException(UnknownException::class);
        $client->get('environment_id');
    }

    private function getProject(): Project
    {
        $project = new Project("project", "api_key");
        $project->setSlug("project");
        return $project;
    }
}
