<?php

namespace SG\CmsBundle\Tests\Common\HttpClient\Response;

use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use SG\CmsBundle\Common\HttpClient\Response\ResponseItem;
use SG\CmsBundle\Common\HttpClient\Response\ResponseParser;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Frontend\Exception\NonUniqueResultException;
use Symfony\Component\HttpClient\Response\CurlResponse;
use Symfony\Contracts\HttpClient\ResponseInterface;

class ResponseParserTest extends TestCase
{
    private ?ResponseParser $parser = null;

    public function setUp(): void
    {
        $this->parser = new ResponseParser();
    }

    public function tearDown(): void
    {
        $this->parser = null;
    }

    public function testException()
    {
        $this->expectException(\Exception::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(201);
        $this->parser->parse($response);
    }

    public function testResourceNotFoundException()
    {
        $this->expectException(ResourceNotFoundException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(404);
        $response->method("getContent")->willReturn(json_encode(array('detail'=>"message")));
        $this->parser->parse($response);
    }

    public function testResultResourceNotFoundException()
    {
        $this->expectException(ResourceNotFoundException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "response" => null
        ]);
        $this->parser->parse($response);
    }

    public function testJsonLDResourceNotFoundException()
    {
        $this->expectException(ResourceNotFoundException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(404);
        $response->method("getContent")->willReturn(json_encode(array('hydra:description'=>"message")));
        $this->parser->parse($response);
    }

    public function testParseJson()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            0 => [
                "id" => "1"
            ],
            1 => [
                "id" => "2"
            ]
        ]);
        $result = $this->parser->parse($response);
        $this->assertCount(2, $result);
        $this->assertEquals("1", $result[0]->id);
        $this->assertEquals("2", $result[1]->id);
    }

    public function testParseJsonWithResponse()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "response" => [
                "id" => "1"
            ]
        ]);
        $result = $this->parser->parse($response);
        $this->assertCount(1, $result);
        $this->assertEquals("1", $result[0]->id);
    }

    public function testParseJsonLDCollection()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "@type" => "hydra:Collection",
            "hydra:member" => [
                0 => [
                    "id" => "1"
                ],
                1 => [
                    "id" => "2"
                ]
            ]
        ]);
        $result = $this->parser->parse($response);
        $this->assertCount(2, $result);
        $this->assertEquals("1", $result[0]->id);
        $this->assertEquals("2", $result[1]->id);
    }

    public function testParseJsonLDSingle()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "id" => "1"
        ]);
        $result = $this->parser->parse($response);
        $this->assertCount(1, $result);
        $this->assertEquals("1", $result[0]->id);
    }

    public function testParseOneJson()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            0 => [
                "id" => "1"
            ]
        ]);
        $result = $this->parser->parseOne($response);
        $this->assertInstanceOf(ResponseItem::class, $result);
        $this->assertEquals("1", $result->id);
    }

    public function testParseOneJsonWithResponse()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "response" => [
                "id" => "1"
            ]
        ]);
        $result = $this->parser->parseOne($response);
        $this->assertInstanceOf(ResponseItem::class, $result);
        $this->assertEquals("1", $result->id);
    }

    public function testparseOneJsonWithResponseResourceNotFoundException()
    {
        $this->expectException(ResourceNotFoundException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "response" => null
        ]);
        $result = $this->parser->parseOne($response);
    }

    public function testparseOneJsonNonUniqueResultException()
    {
        $this->expectException(NonUniqueResultException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("getInfo")->willReturn(["url" => "url"]);
        $response->method("toArray")->willReturn([
            0 => [
                "id" => "1"
            ],
            1 => [
                "id" => "1"
            ]
        ]);
        $result = $this->parser->parseOne($response);
    }

    public function testParseOneForJsonLD()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "id" => "1"
        ]);
        $result = $this->parser->parseOne($response);
        $this->assertInstanceOf(ResponseItem::class, $result);
        $this->assertEquals("1", $result->id);
    }

    public function testParseOneJsonLDNonUniqueResultException()
    {
        $this->expectException(NonUniqueResultException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("getInfo")->willReturn(["url" => "url"]);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "@type" => "hydra:Collection",
            "hydra:member" => [
                0 => [
                    "id" => "1"
                ],
                1 => [
                    "id" => "2"
                ]
            ]
        ]);
        $result = $this->parser->parseOne($response);
    }

    public function testParseOneException()
    {
        $this->expectException(\Exception::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(201);
        $this->parser->parseOne($response);
    }

    public function testParseOneJsonLdException()
    {
        $this->expectException(\Exception::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(201);
        $this->parser->parseOneJsonLd($response);
    }

    public function testParseOneJsonLD()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "id" => "1"
        ]);
        $result = $this->parser->parseOneJsonLd($response);
        $this->assertInstanceOf(ResponseItem::class, $result);
        $this->assertEquals("1", $result->id);
    }

    public function testParseOneJsonLDCollection()
    {
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "@type" => "hydra:Collection",
            "hydra:member" => [
                0 => [
                    "id" => "1"
                ]
            ]
        ]);
        $result = $this->parser->parseOneJsonLd($response);
        $this->assertInstanceOf(ResponseItem::class, $result);
        $this->assertEquals("1", $result->id);
    }

    public function testParseOneJsonLDCollectionNonUniqueException()
    {
        $this->expectException(NonUniqueResultException::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getStatusCode")->willReturn(200);
        $response->method("getInfo")->willReturn(["url" => "url"]);
        $response->method("toArray")->willReturn([
            "@context" => "context",
            "@type" => "hydra:Collection",
            "hydra:member" => [
                0 => [
                    "id" => "1"
                ],
                1 => [
                    "id" => "2"
                ]
            ]
        ]);
        $result = $this->parser->parseOneJsonLd($response);
    }
}
