<?php

namespace SG\CmsBundle\Tests\Common\HttpClient\Response;

use SG\CmsBundle\Common\HttpClient\Response\ResponseItem;
use PHPUnit\Framework\TestCase;

class ResponseItemTest extends TestCase
{
    private ?ResponseItem $item = null;

    public function setUp(): void
    {
        $this->item = new ResponseItem();
    }

    public function tearDown(): void
    {
        $this->item = null;
    }

    public function test__get()
    {
        $this->item->setAttributes([
           "name" => "value",
        ]);
        $this->assertEquals("value", $this->item->get("name"));
        $this->assertEquals("value", $this->item->name);
        $this->assertEquals(["name"=>"value"], $this->item->getAttributes());
    }

    public function testGetResponse()
    {
        $this->item->set("response", ["response"]);
        $this->assertEquals(["response"], $this->item->getResponse());
    }

    public function testSet()
    {
        $this->item->set("name", "value");
        $this->assertEquals("value", $this->item->get("name"));
    }

    public function test__set()
    {
        $this->item->name = "value";
        $this->assertEquals("value", $this->item->get("name"));
    }

    public function testGetException()
    {
        $this->item->setAttributes([
            "name" => "value",
        ]);
        $this->expectException(\Exception::class);
        $this->item->get("plop");
    }

    /*public function testGetAttributes()
    {

    }

    public function testGetResponse()
    {

    }

    public function testGet()
    {

    }*/
}
