<?php

namespace SG\CmsBundle\Tests\Api\Command;

use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\Command\SyncFrontendConfigCommand;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Console\Tester\CommandTester;
use Symfony\Component\HttpClient\Exception\ClientException;
use Symfony\Component\HttpClient\Exception\RedirectionException;
use Symfony\Component\HttpClient\Exception\ServerException;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Response\CurlResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class SyncFrontendConfigCommandTest extends KernelTestCase
{
    private ?SyncFrontendConfigCommand $command = null;

    public function setUp(): void
    {
        $kernel = static::createKernel();
        $application = new Application($kernel);
        $this->command = new SyncFrontendConfigCommand(__DIR__ . "/../Fixture");
    }

    public function tearDown(): void
    {
        $this->command = null;
    }

    public function testExecute()
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getContent")->willReturn($this->getCmsConfiguration());

        $httpClient->method("request")->willReturn($response);
        $this->command->setClient($httpClient);
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Synchronize cms configuration', $output);
        $this->assertStringContainsString('[OK] Cms configuration for guides and modules is now synced with frontend configuration.', $output);
    }

    public function testClientCreation()
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getContent")->willReturn($this->getCmsConfiguration());

        $httpClient->method("request")->willReturn($response);
        // $command->setClient($httpClient);
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Synchronize cms configuration', $output);
        $this->assertStringContainsString('[OK] Cms configuration for guides and modules is now synced with frontend configuration.', $output);
    }

    public function testNoClient()
    {
        $previousValue = $_ENV['FRONTEND_URL'];
        $_ENV['FRONTEND_URL'] = "";
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Frontend is not configured. Sync skipped.', $output);
        $_ENV['FRONTEND_URL'] = $previousValue;
    }

    public function testTransportException()
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getContent")->willThrowException(new TransportException("Exception message"));
        $httpClient->method("request")->willReturn($response);
        $this->command->setClient($httpClient);
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Exception message', $output);
    }

    public function testException()
    {
        $httpClient = $this->createMock(HttpClientInterface::class);
        $response = $this->createMock(ResponseInterface::class);
        $response->method("getContent")->willThrowException(new \Exception("Exception message"));
        $httpClient->method("request")->willReturn($response);
        $this->command->setClient($httpClient);
        $commandTester = new CommandTester($this->command);
        $commandTester->execute([]);

        // the output of the command in the console
        $output = $commandTester->getDisplay();
        $this->assertStringContainsString('Exception message', $output);
    }

    private function getCmsConfiguration(): string
    {
        return '{
            "mode":"frontend",
            "website_identifier":"eyesight",
            "multilingual":true,
            "guides":{
                "homepage":{
                    "controller":"TestController",
                    "action":"testGuide",
                    "multiple":false,
                    "view":"default.html.twig",
                    "sitemap":true,
                    "zones":[]
                },
                "page1":{
                    "view":"page1.html.twig",
                    "zones":{
                        "zone_modules1":{
                            "type":"modules",
                            "position":1,
                            "enabled":true
                        },
                        "zone_modules2":{
                            "type":"modules",
                            "position":2,
                            "enabled":true
                        }
                    },
                    "multiple":false,
                    "controller":"CmsController",
                    "action":"default",
                    "sitemap":true
                }
            },
            "modules":{
                "guides":{
                    "title_h1":{
                        "pattern":"pattern",
                        "parameters":{
                            "value":{
                                "label":"",
                                "description":""
                            }
                        },
                        "enabled":true,
                        "renderer":"ModuleRenderer",
                        "view":null
                    }
                }
            },
            "translations_fallback":true,
            "translations_fallback_locale":"fr",
            "locale":"fr"
        }';
    }
}
