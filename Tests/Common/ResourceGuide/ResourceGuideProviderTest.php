<?php

namespace SG\CmsBundle\Tests\Common\ResourceGuide;

use JMS\Serializer\SerializerBuilder;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use PHPUnit\Framework\TestCase;

class ResourceGuideProviderTest extends TestCase
{
    private ?ResourceGuideProvider $provider = null;

    public function setUp(): void
    {
        $this->provider = new ResourceGuideProvider(SerializerBuilder::create()->build());
        $this->provider->setDefaultGuideConfiguration($this->getDefaultGuidesConfiguration());
    }

    public function tearDown(): void
    {
        $this->provider = null;
    }

    public function testGetGuide()
    {
        $guides = $this->getGuidesConfiguration();
        $this->provider->setAvailableGuides($guides);
        $guide = $this->provider->getGuide("page1");
        $this->assertInstanceOf(ResourceGuide::class, $guide);
        $this->assertEquals("page1", $guide->getName());
    }

    public function testGetDefaultGuide()
    {
        $guides = $this->getGuidesConfiguration();
        $guide = $this->provider->getGuide();
        $this->assertInstanceOf(ResourceGuide::class, $guide);
        $this->assertEquals("default", $guide->getName());
    }

    public function testResourceGuideNotFoundException()
    {
        $guides = $this->getGuidesConfiguration();
        $this->expectException(ResourceGuideNotFoundException::class);
        $guide = $this->provider->getGuide("invalid");
    }

    private function getGuidesConfiguration(): array
    {
        return [
            "homepage" => [
                "controller" => "App\Controller\TestController",
                "action" => "testGuide",
                "multiple" => false,
                "view" => "default.html.twig",
                "sitemap" => true,
                "zones" => []
             ],
            "page1" => [
                "view" => "page1.html.twig",
                "zones" => [
                    "zone_modules1" => [
                        "name" => "zone_modules1",
                        "type" => "modules",
                        "position" => 1,
                        "enabled" => true,
                    ],
                    "zone_modules2"  => [
                        "name" => "zone_modules2",
                        "type" => "modules",
                        "position" => 2,
                        "enabled" => true,
                    ]
                ],
                "multiple" => false,
                "controller" => "SG\CmsBundle\Frontend\Controller\CmsController",
                "action" => "default",
                "sitemap" => true,
            ],
            "page2" => [
                "controller" => "App\Controller\TestController",
                "action" => "testGuide",
                "view" => "page1.html.twig",
                "multiple" => false,
                "sitemap" => true,
                "zones" => [  ],
            ]
        ];
    }

    private function getDefaultGuidesConfiguration(): array
    {
        return [
            "default_guide" => [
                "controller" => "SG\CmsBundle\Frontend\Controller\CmsController",
                "action" => "default",
                "sitemap" => true
            ]
        ];
    }
    /*public function testSetDefaultGuideConfiguration()
    {

    }

    public function testSetAvailableGuides()
    {

    }

    public function testGetGuide()
    {

    }*/
}
