<?php

namespace SG\CmsBundle\Tests\Common\ResourceGuide\Model;

use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\ResourceGuide\Model\Zone;

class ResourceGuideTest extends TestCase
{
    private $guide;

    public function setUp(): void
    {
        $this->guide = new ResourceGuide();
    }

    public function tearDown(): void
    {
        $this->guide = null;
    }

    public function testIsSitemap()
    {
        $this->guide->setSitemap(true);
        $this->assertTrue($this->guide->isSitemap());
    }

    public function testGetView()
    {
        $this->guide->setName('name');
        $this->assertEquals('name.html.twig', $this->guide->getView());
        $this->guide->setView('test.html.twig');
        $this->assertEquals('test.html.twig', $this->guide->getView());
    }

    public function testGetAction()
    {
        $this->guide->setAction('index');
        $this->assertEquals('index', $this->guide->getAction());
    }

    public function testGetName()
    {
        $this->guide->setName('guideName');
        $this->assertEquals('guideName', $this->guide->getName());
    }

    public function testGetController()
    {
        $this->guide->setController('controller');
        $this->assertEquals('controller', $this->guide->getController());
    }

    public function testGetTranslationDomain()
    {
        $this->guide->setTranslationDomain('domain');
        $this->assertEquals('domain', $this->guide->getTranslationDomain());
    }

    public function testGetSetZones()
    {
        $zone1 = new Zone();
        $zone1->setName("zone1");
        $zone1->setPosition(1);
        $zone1->setType("modules");

        $zone2 = new Zone();
        $zone2->setName("zone2");
        $zone2->setPosition(2);
        $zone2->setType("modules");

        $this->guide->setZones([$zone1, $zone2]);
        $this->assertEquals(2, count($this->guide->getZones()));
        $this->assertEquals($zone1, $this->guide->getZone('zone1'));
    }

    public function testGetZoneException()
    {
        try {
            $zone1 = new Zone();
            $zone1->setName("zone1");
            $zone1->setPosition(1);
            $zone1->setType("modules");

            $this->guide->setZones([$zone1]);
            $this->guide->getZone('zone2');
        } catch (\Exception $e) {
            $this->assertInstanceOf(\Exception::class, $e);
            $this->assertEquals('No zone found for name zone2', $e->getMessage());
        }
    }
}
