<?php

namespace SG\CmsBundle\Tests\Common\ResourceGuide\Model;

use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use PHPUnit\Framework\TestCase;
use SG\CmsBundle\Common\ResourceGuide\Model\Zone;
use SG\CmsBundle\Frontend\Model\Module;

class ZoneTest extends TestCase
{
    private $zone;

    public function setUp(): void
    {
        $this->zone = new Zone();
    }

    public function tearDown(): void
    {
        $this->zone = null;
    }

    public function testGetSetName()
    {
        $this->zone->setName("zone");
        $this->assertEquals('zone', $this->zone->getName());
    }

    public function testGetSetType()
    {
        $this->zone->setType('type');
        $this->assertEquals('type', $this->zone->getType());
    }

    public function testGetSetPosition()
    {
        $this->zone->setPosition(1);
        $this->assertEquals(1, $this->zone->getPosition());
    }

    public function testGetSetModules()
    {
        $module = new Module();
        $this->zone->addModule($module);
        $this->assertCount(1, $this->zone->getModules());
        $modules = [$module];
        $this->assertEquals($modules, $this->zone->getModules());
    }
}
