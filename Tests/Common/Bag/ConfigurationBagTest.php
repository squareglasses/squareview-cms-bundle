<?php

namespace SG\CmsBundle\Tests\Common\Bag;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use PHPUnit\Framework\TestCase;

class ConfigurationBagTest extends TestCase
{
    private ?ConfigurationBag $bag = null;

    public function setUp(): void
    {
        $this->bag = new ConfigurationBag([
            "translations_fallback_locale" => "fr",
            "locale" => "fr",
            "classes" => [
                "content" => "App\Entity\Content",
                "language" => "SG\CmsBundle\Api\Entity\Language",
                "website" => "App\Entity\Website"
            ],
            "repositories" => [
                "website" => "App\Repository\WebsiteRepository"
            ]
        ]);
    }

    public function tearDown(): void
    {
        $this->bag = null;
    }

    public function testGetLanguageClass()
    {
        $this->assertEquals("SG\CmsBundle\Api\Entity\Language", $this->bag->getLanguageClass());
    }

    public function testGetDefaultLocale()
    {
        $this->assertEquals("fr", $this->bag->getDefaultLocale());
    }

    public function testGetClasses()
    {
        $expected = [
            "content" => "App\Entity\Content",
            "language" => "SG\CmsBundle\Api\Entity\Language",
            "website" => "App\Entity\Website"
        ];
        $this->assertEquals($expected, $this->bag->getClasses());
    }

    public function testGetContentClass()
    {
        $this->assertEquals("App\Entity\Content", $this->bag->getContentClass());
    }

    public function testGetWebsiteClass()
    {
        $this->assertEquals("App\Entity\Website", $this->bag->getWebsiteClass());
    }
}
