<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\DependencyInjection;

use Closure;
use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\DependencyInjection\EnvVarProcessorInterface;

/**
 * Class NullableStringEnvVarProcessor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class NullableStringEnvVarProcessor implements EnvVarProcessorInterface
{
    /**
     * @param string   $prefix
     * @param string   $name
     * @param Closure $getEnv
     *
     * @return mixed
     */
    public function getEnv(string $prefix, string $name, Closure $getEnv): mixed
    {
        $env = $getEnv($name);

        return $env !== "" ? $env : null;
    }

    /**
     * @return string[]
     */
    #[ArrayShape(['nullable_string' => "string"])]
    public static function getProvidedTypes(): array
    {
        return [
            'nullable_string' => 'string',
        ];
    }
}
