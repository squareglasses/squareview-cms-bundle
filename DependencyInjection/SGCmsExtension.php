<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\DependencyInjection;

use Exception;
use SG\CmsBundle\Frontend\Routing\CmsLoader;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\HomepageRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\I18nRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\LanguageSwitchRouteGenerator;
use SG\CmsBundle\Frontend\Routing\Router;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class SGCmsExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SGCmsExtension extends Extension
{
    /**
     * @param array            $configs
     * @param ContainerBuilder $container
     *
     * @return void
     * @throws Exception
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $configuration = new Configuration();

        $config = $this->processConfiguration($configuration, $configs);

        $this->bindParameters($container, 'squareview', $config);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('doctrine_extensions.yaml');
        $loader->load('services.yaml');
        if ($container->getParameter("squareview.mode") === "api") {
            $loader->load('services_api.yaml');
        } elseif ($container->getParameter("squareview.mode") === "frontend") {
            $loader->load('services_frontend.yaml');
            $this->configureRouting($container);
        } elseif ($container->getParameter("squareview.mode") === "backend") {
            $loader->load('services_backend.yaml');
        }
    }

    /**
     * Configure the router services with correct dependencies
     *
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function configureRouting(ContainerBuilder $container): void
    {
        // Configure routes loader
        if ($container->hasDefinition(CmsLoader::class) && $container->getParameter('squareview.multilingual') === true) {
            $definition = $container->getDefinition(CmsLoader::class);
            $I18nRouteGeneratorDefinition = $container->getDefinition(I18nRouteGenerator::class);
            $definition->replaceArgument('$routeGenerator', $I18nRouteGeneratorDefinition);
        }

        // Configure homepage route generator
        if ($container->hasDefinition(HomepageRouteGenerator::class)) {
            $definition = $container->getDefinition(HomepageRouteGenerator::class);
            $definition->addMethodCall('setMultilingual', [$container->getParameter('squareview.multilingual')]);
            $definition->addMethodCall('setDefaultLocale', [$container->getParameter('squareview.locale')]);
        }

        // Configure language switch route generator
        if ($container->hasDefinition(LanguageSwitchRouteGenerator::class)) {
            $definition = $container->getDefinition(LanguageSwitchRouteGenerator::class);
            $definition->addMethodCall('setMultilingual', [$container->getParameter('squareview.multilingual')]);
        }

        // Configure router
        if ($container->hasDefinition(Router::class)) {
            $definition = $container->getDefinition(Router::class);
            $definition->addMethodCall('setDefaultLocale', [$container->getParameter('squareview.locale')]);
        }
    }

    /**
     * Binds the params from config
     *
     * @param ContainerBuilder $container
     * @param string           $name
     * @param mixed            $config
     *
     * @return void
     */
    public function bindParameters(ContainerBuilder $container, string $name, mixed $config): void
    {
        if (is_array($config) && empty($config[0])) {
            $container->setParameter($name, $config);
            foreach ($config as $key => $value) {
                $container->setParameter($name . '.' . $key, $value);
                $this->bindParameters($container, $name . '.' . $key, $value);
            }
        } else {
            $container->setParameter($name, $config);
        }
    }
}
