<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\DependencyInjection;

use SG\CmsBundle\Backend\DependencyInjection\Configuration as BackendConfiguration;
use SG\CmsBundle\Common\DependencyInjection\DefaultGuideConfiguration;
use SG\CmsBundle\Common\DependencyInjection\GuideConfiguration;
use SG\CmsBundle\Common\DependencyInjection\MediaConfiguration;
use SG\CmsBundle\Common\DependencyInjection\ModuleConfiguration;
use SG\CmsBundle\Common\DependencyInjection\TranslationConfiguration;
use SG\CmsBundle\Frontend\DependencyInjection\Configuration as FrontendConfiguration;
use SG\CmsBundle\Api\DependencyInjection\Configuration as ApiConfiguration;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('sg_cms');

        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->enumNode('mode')
                    ->values(['api', 'frontend', 'backend'])
                    ->cannotBeEmpty()
                    ->isRequired()
                ->end()
                ->scalarNode('website_name')->defaultNull()->cannotBeEmpty()->end()
                ->scalarNode('website_identifier')->defaultNull()->cannotBeEmpty()->end()
                ->scalarNode('homepage_identifier')->defaultValue("accueil")->cannotBeEmpty()->end()
                ->booleanNode('multilingual')->defaultTrue()->end()
                ->booleanNode('translations_fallback')->defaultTrue()->end()
                ->scalarNode('translations_fallback_locale')->defaultValue("fr")->cannotBeEmpty()->end()
                ->scalarNode('locale')->defaultValue("fr")->cannotBeEmpty()->end()
                ->variableNode('allowed_locales')->end() // #TODO: make clean configuration

                ->append($this->addMailerNode()) // mailer

            ->end()
        ;

        $translationConfiguration = new TranslationConfiguration();
        $rootNode->append($translationConfiguration->getConfig());

        $backendConfiguration = new BackendConfiguration();
        $rootNode->append($backendConfiguration->getConfig());

        $frontendConfiguration = new FrontendConfiguration();
        $rootNode->append($frontendConfiguration->getConfig());

        $apiConfiguration = new ApiConfiguration();
        $rootNode->append($apiConfiguration->getConfig());

        $guideConfiguration = new GuideConfiguration();
        $rootNode->append($guideConfiguration->getConfig());

        $defaultGuideConfiguration = new DefaultGuideConfiguration();
        $rootNode->append($defaultGuideConfiguration->getConfig());

        $moduleConfiguration = new ModuleConfiguration();
        $rootNode->append($moduleConfiguration->getConfig());

        $mediaConfiguration = new MediaConfiguration();
        $rootNode->append($mediaConfiguration->getConfig());

        return $treeBuilder;
    }

    /**
     * @return mixed
     */
    private function addMailerNode(): mixed
    {
        $treeBuilder = new TreeBuilder('mailer');

        // @TODO: Add constraints/defaults values on nodes
        return $treeBuilder->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
            ->scalarNode('from')->defaultValue("Square Glasses <contact@squareglasses.com>")->end()
//                ->scalarNode('project')->defaultNull()->end()
//                ->scalarNode('api_key')->defaultNull()->end()
//                ->scalarNode('environment')->defaultNull()->end()
            ->end();
    }

    /**
     * Add configuration for app default guide section.
     *
     * @return mixed
     */
    private function addModulesNode(): mixed
    {
        $treeBuilder = new TreeBuilder('modules');

        return $treeBuilder->getRootNode()
            ->info('Modules are used for page construction.')
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('guides')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->addDefaultsIfNotSet()
                        ->canBeDisabled()
                        ->children()
                            ->scalarNode('renderer')
                                ->defaultValue('SG\CmsBundle\Frontend\Renderer\ModuleRenderer')
                                ->info('Renderer used for module generation.')
                            ->end()
                            ->scalarNode('view')
                                ->defaultNull()
                                ->info('View used for module rendering.')
                            ->end()
                            ->scalarNode('pattern')
                                ->defaultNull()
                                ->info('By default, used for rendering the module parameters in the view.')
                            ->end()
                            ->arrayNode('parameters')
                                ->useAttributeAsKey('name')
                                ->prototype('array')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('label')->defaultValue('')->end()
                                        ->scalarNode('description')->defaultValue('')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end();
    }

    /**
     * @return mixed
     */
    private function addRefererNode(): mixed
    {
        $treeBuilder = new TreeBuilder('referer');

        return $treeBuilder->getRootNode()
            ->addDefaultsIfNotSet()
            ->children()
                ->arrayNode('excluded_routes')
                    ->prototype('scalar')->end()
                    ->defaultValue(['switch_language', '_wdt', '_profiler_home', '_profiler_search',
                        '_profiler_search_bar', '_profiler_phpinfo', '_profiler_search_results', '_profiler_open_file',
                        '_profiler', '_profiler_router', '_profiler_exception', '_profiler_exception_css',
                        'api_entrypoint', '_errors'])
                ->end()

                ->arrayNode('excluded_patterns')
                    ->prototype('scalar')->end()
                    ->defaultValue(['/api/'])
                ->end()
            ->end();
    }
}
