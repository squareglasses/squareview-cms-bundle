<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\DependencyInjection\Compiler;

use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ResourceGuideProviderCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceGuideProviderCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(ResourceGuideProvider::class)) {
            return;
        }

        $guideProviderDefinition = $container->getDefinition(ResourceGuideProvider::class);
        $guidesConfiguration = $container->getParameter('squareview.guides');
        if ($guidesConfiguration) {
            // Adding available guides from current configuration
            $guideProviderDefinition->addMethodCall('setAvailableGuides', [$guidesConfiguration]);
        }
        $defaultGuideConfiguration = $container->getParameter('squareview.default_guide');
        if ($defaultGuideConfiguration) {
            $guideProviderDefinition->addMethodCall('setDefaultGuideConfiguration', [$defaultGuideConfiguration]);
        }
    }
}
