<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Contracts;

/**
 * Interface ModuleInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ModuleInterface
{
    /**
     * @return array
     */
    public function getParameters(): array;

    /**
     * @param array $parameters
     *
     * @return void
     */
    public function setParameters(array $parameters): void;

    /**
     * @return string|null
     */
    public function getLocale(): ?string;

    /**
     * @param string|null $locale
     *
     * @return void
     */
    public function setLocale(?string $locale): void;

    /**
     * @return string|null
     */
    public function getZone(): ?string;

    /**
     * @param string|null $zone
     *
     * @return void
     */
    public function setZone(?string $zone = null): void;

    /**
     * @return int|null
     */
    public function getPosition(): ?int;

    /**
     * @param int|null $position
     *
     * @return void
     */
    public function setPosition(?int $position): void;

    /**
     * @return string|null
     */
    public function getGuideIdentifier(): ?string;

    /**
     * @param string|null $guideIdentifier
     *
     * @return void
     */
    public function setGuideIdentifier(?string $guideIdentifier): void;

    /**
     * @return ModuleGuideInterface|null
     */
    public function getGuide(): ?ModuleGuideInterface;

    /**
     * @param ModuleGuideInterface $guide
     */
    public function setGuide(ModuleGuideInterface $guide): void;

    /**
     * @return string|null
     */
    public function getView(): ?string;

    /**
     * @return string|null
     */
    public function getPattern(): ?string;

    /**
     * @return array
     */
    public function getViewParameters(): array;
}
