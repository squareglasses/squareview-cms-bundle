<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Contracts;

/**
 * Interface ModuleRendererInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ModuleRendererInterface
{
    /**
     * @param ModuleInterface $module
     *
     * @return ModuleRendererInterface
     */
    public function setModule(ModuleInterface $module): ModuleRendererInterface;

    /**
     * @return ModuleRendererInterface
     */
    public function execute(): ModuleRendererInterface;

    /**
     * @return string
     */
    public function render(): string;
}
