<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Contracts;

/**
 * Interface ModuleGuideInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ModuleGuideInterface
{
    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void;

    /**
     * @return string|null
     */
    public function getRenderer(): ?string;

    /**
     * @param string $renderer
     *
     * @return void
     */
    public function setRenderer(string $renderer): void;

    /**
     * @return string|null
     */
    public function getView(): ?string;

    /**
     * @param string|null $view
     *
     * @return void
     */
    public function setView(?string $view = null): void;

    /**
     * @return string|null
     */
    public function getPattern(): ?string;

    /**
     * @param string|null $pattern
     *
     * @return void
     */
    public function setPattern(?string $pattern = null): void;
}
