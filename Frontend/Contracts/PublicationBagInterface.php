<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Contracts;

/**
 * Interface PublicationBagInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface PublicationBagInterface
{
    /**
     * @return array|null
     */
    public function getResource(): ?array;

    /**
     * @param array $resource
     *
     * @return PublicationBagInterface
     */
    public function setResource(array $resource): PublicationBagInterface;

    /**
     * @return array|null
     */
    public function getMetaTags(): ?array;

    /**
     * @param array $metaTags
     *
     * @return PublicationBagInterface
     */
    public function setMetaTags(array $metaTags): PublicationBagInterface;

    /**
     * @param array $serializedResources
     *
     * @return void
     */
    public function setRawDatas(array $serializedResources): void;

    /**
     * @return array
     */
    public function getMedias(): array;

    /**
     * @param array $medias
     *
     * @return PublicationBagInterface
     */
    public function setMedias(array $medias): PublicationBagInterface;

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __isset(string $name);

    /**
     * @param string $name
     *
     * @return mixed
     */
    public function __get(string $name);
}
