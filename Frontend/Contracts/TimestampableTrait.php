<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Contracts;

use DateTimeInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait TimestampableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait TimestampableTrait
{
    /**
     * @Groups({"get"})
     */
    protected ?DateTimeInterface $createdAt = null;

    /**
     * @Groups({"get"})
     */
    protected ?DateTimeInterface $updatedAt = null;

    /**
     * @return DateTimeInterface|null
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    /**
     * @param DateTimeInterface $datetime
     *
     * @return void
     */
    public function setCreatedAt(DateTimeInterface $datetime): void
    {
        $this->createdAt = $datetime;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface $datetime
     *
     * @return void
     */
    public function setUpdatedAt(DateTimeInterface $datetime): void
    {
        $this->updatedAt = $datetime;
    }
}
