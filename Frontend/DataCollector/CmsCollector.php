<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DataCollector;

use SG\CmsBundle\Frontend\Bag\ResourceBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\DataCollector\DataCollector;
use Throwable;

/**
 * Class CmsCollector
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsCollector extends DataCollector
{
    /**
     * @param ResourceBag $cmsBag
     */
    public function __construct(private readonly ResourceBag $cmsBag)
    {
    }

    /**
     * @param Request         $request
     * @param Response        $response
     * @param Throwable|null $exception
     *
     * @return void
     */
    public function collect(Request $request, Response $response, Throwable $exception = null): void
    {
        $this->data = [
            'resource' => $this->cmsBag->getResource(),
            'guide' => $this->cmsBag->getGuide(),
            'modules' => $this->cmsBag->getModules(),
            'meta_tags' => $this->cmsBag->getMetaTags()
        ];
    }

    /**
     * @return void
     */
    public function reset(): void
    {
        $this->data = [];
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return 'squareview.cms_collector';
    }

    /**
     * @return mixed
     */
    public function getResource(): mixed
    {
        return $this->data['resource'];
    }

    /**
     * @return mixed
     */
    public function getGuide(): mixed
    {
        return $this->data['guide'];
    }

    /**
     * @return mixed
     */
    public function getModules(): mixed
    {
        return $this->data['modules'];
    }

    /**
     * @return mixed
     */
    public function getMetaTags(): mixed
    {
        return $this->data['meta_tags'];
    }
}
