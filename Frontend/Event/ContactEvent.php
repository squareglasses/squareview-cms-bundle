<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Event;

use SG\CmsBundle\Common\Contracts\ContactInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ContactEvent
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ContactEvent extends Event
{
    /**
     * @param ContactInterface $contact
     * @param Request          $request
     * @param Response|null    $response
     */
    public function __construct(protected ContactInterface $contact, protected Request $request, protected ?Response $response = null)
    {
    }

    /**
     * @return ContactInterface
     */
    public function getContact(): ContactInterface
    {
        return $this->contact;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @param Request $request
     */
    public function setRequest(Request $request): void
    {
        $this->request = $request;
    }

    /**
     * @return Response|null
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     */
    public function setResponse(?Response $response): void
    {
        $this->response = $response;
    }
}
