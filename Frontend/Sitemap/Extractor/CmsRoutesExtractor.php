<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Extractor;

use App\Model\Content;
use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Model\Route;
use SG\CmsBundle\Frontend\Sitemap\Model\Alternate;
use SG\CmsBundle\Frontend\Sitemap\Model\Url;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class CmsRoutesExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsRoutesExtractor extends AbstractExtractor implements ExtractorInterface
{
    /**
     * @param array                $entries
     * @param OutputInterface|null $output
     *
     * @return array
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function extract(array $entries = [], ?OutputInterface $output = null): array
    {
        $section = $progress = null;
        if (null !== $output) {
            $this->startTimer();
            $section = $output->section();
            $section->writeln("Generate cms entries...");
        }
        $routes = $this->apiClient->getCollection("routes", [], Route::class, null, false);
        if (null !== $output) {
            $sectionProgress = $output->section();
            $progress = new ProgressBar($sectionProgress);
            $progress->start(count($routes));
        }
        foreach ($routes as $route) {
            if ($route->getPath() !== "homepage") {
                if (null === $route->getOptions() || !array_key_exists("sitemap", $route->getOptions()) ||
                    (array_key_exists("sitemap", $route->getOptions()) && $route->getOptions()['sitemap'] === true)
                ) {
                    try {
                        $resource = $this->apiClient->getItem("contents/".$route->getResourceIdentifier()."?".http_build_query(["override_groups" => ["get"]]), [], Content::class);
                        if ($resource && $resource->getEnabled()) {
                            $guide = $this->getResourceGuide($resource);
                            if ($guide && $guide->isSitemap()) {
                                if ($this->multilingual) {
                                    $location = $this->router->generate(
                                        $route->getName(),
                                        ['_locale' => $route->getLocale()],
                                        UrlGeneratorInterface::ABSOLUTE_URL
                                    );
                                } else {
                                    $location = $this->router->generate(
                                        $route->getName(),
                                        [],
                                        UrlGeneratorInterface::ABSOLUTE_URL
                                    );
                                }
                                $url = (new Url())
                                    ->setLocation($location)
                                    ->setChangeFrequency($this->getChangeFrequency($guide))
                                    ->setLastModification($resource->getUpdatedAt()->format('c'))
                                    ->setPriority($this->getPriority($guide))
                                    ->setResource($resource);
                                if ($this->multilingual) {
                                    $url->setAlternates($this->getAlternates($route, $routes));
                                }
                                $entries[$route->getName().'.'.$route->getLocale()] = $url;
                            }
                        }
                    } catch (Exception $e) {
                        $output?->write($e->getMessage());die;
                    }
                }
            }

            if (null !== $output && null !== $progress) {
                $progress->advance();
            }
        }

        if (null !== $output) {
            $progress->finish();
        }
        $section?->overwrite("Generate cms entries... OK (" . $this->getTimerValue() . "s)");
        return $entries;
    }

    /**
     * @param Route $route
     * @param array $routes
     *
     * @return array
     */
    private function getAlternates(Route $route, array $routes): array
    {
        $alternates = [];
        foreach ($routes as $otherRoute) {
            if ($route->getName() === $otherRoute->getName() && $route->getLocale() !== $otherRoute->getLocale()) {
                $alternates[] = (new Alternate())
                    ->setLocation($this->router->generate(
                        $otherRoute->getName(),
                        ['_locale' => $otherRoute->getLocale()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ))
                    ->setLanguage(
                        $otherRoute->getLocale()
                    );
            }
        }
        return $alternates;
    }
}
