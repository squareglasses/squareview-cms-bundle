<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Extractor;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Sitemap\Model\Alternate;
use SG\CmsBundle\Frontend\Sitemap\Model\Url;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class RouterRoutesExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RouterRoutesExtractor extends AbstractExtractor implements ExtractorInterface
{
    /**
     * @param array                $entries
     * @param OutputInterface|null $output
     *
     * @return array
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function extract(array $entries = [], ?OutputInterface $output = null): array
    {
        $section = null;
        if (null !== $output) {
            $this->startTimer();
            $section = $output->section();
            $section->writeln("Generate router entries...");
        }

        $routes = $this->router->getRouteCollection();
        $sitemapRoutes = array();
        foreach ($routes as $name => $route) {
            if (($route->getOption('sitemap') && (true === $route->getOption('sitemap') || 'true' === $route->getOption('sitemap')))) {
                if ($route->getOption('base_route')) {
                    $sitemapRoutes[$route->getOption('base_route')] = $route;
                } else {
                    $sitemapRoutes[$name] = $routes->get($name);
                }
            }
        }

        if (null !== $output) {
            $sectionProgress = $output->section();
            $progress = new ProgressBar($sectionProgress);
            $progress->start(count($sitemapRoutes));
        }

        foreach ($sitemapRoutes as $name => $route) {
            if (str_contains($route->getPath(), "_locale")) {
                foreach ($this->getLanguages() as $language) {
                    $alternates = [];
                    foreach ($this->getLanguages() as $sLanguage) {
                        $loc = $this->router->generate($name, array('_locale' => $sLanguage->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
                        if ($language->getId() !== $sLanguage->getId()) {
                            $alternates[] = (new Alternate())->setLocation($loc)->setLanguage($sLanguage->getId());
                        }
                    }

                    $entries[] = (new Url())
                        ->setLocation($this->router->generate(
                            $name,
                            ['_locale' => $language->getId()],
                            UrlGeneratorInterface::ABSOLUTE_URL
                        ))
                        ->setChangeFrequency($this->getChangeFrequency())
                        ->setPriority($this->getPriority())
                        ->setAlternates($alternates);
                }
            } else {
                $entries[] = (new Url())
                    ->setLocation($this->router->generate($name, [], UrlGeneratorInterface::ABSOLUTE_URL))
                    ->setChangeFrequency($this->getChangeFrequency())
                    ->setPriority($this->getPriority());
            }

            if (null !== $output) {
                $progress->advance();
            }
        }

        if (null !== $output) {
            $progress->finish();
        }
        $section?->overwrite("Generate router entries... OK (" . $this->getTimerValue() . "s)");

        return $entries;
    }
}
