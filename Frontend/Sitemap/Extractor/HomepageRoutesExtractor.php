<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Extractor;

use App\Model\Content;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Sitemap\Model\Alternate;
use SG\CmsBundle\Frontend\Sitemap\Model\Url;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class HomepageRoutesExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HomepageRoutesExtractor extends AbstractExtractor implements ExtractorInterface
{
    /**
     * @param RouterInterface       $router
     * @param ApiClient             $apiClient
     * @param ResourceGuideProvider $resourceGuideProvider
     * @param array                 $settings
     * @param bool                  $multilingual
     * @param string                $homepageIdentifier
     */
    public function __construct(
        RouterInterface $router,
        ApiClient $apiClient,
        ResourceGuideProvider $resourceGuideProvider,
        array $settings = [],
        bool $multilingual = true,
        protected string $homepageIdentifier = "accueil"
    ) {
        parent::__construct($router, $apiClient, $resourceGuideProvider, $settings, $multilingual);
    }

    /**
     * @param array                $entries
     * @param OutputInterface|null $output
     *
     * @return array
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function extract(array $entries = [], ?OutputInterface $output = null): array
    {
        if (null !== $output) {
            $this->startTimer();
            $section = $output->section();
            $section->writeln("Generate homepage entries...");
        }
        $homepage = $this->apiClient->getItem("contents/".$this->homepageIdentifier."?".http_build_query(["override_groups" => ["get"]]), [], Content::class);
        if (null === $homepage) {
            return [];
        }
        $guide = $this->getResourceGuide($homepage);
        if ($this->multilingual) {
            foreach ($this->getLanguages() as $language) {
                $alternates = [];
                foreach ($this->getLanguages() as $sLanguage) {
                    $loc = $this->router->generate('homepage', array('_locale' => $sLanguage->getId()), UrlGeneratorInterface::ABSOLUTE_URL);
                    if ($language->getId() !== $sLanguage->getId()) {
                        $alternates[] = (new Alternate())->setLocation($loc)->setLanguage($sLanguage->getId());
                    }
                }

                $entries[] = (new Url())
                    ->setLocation($this->router->generate(
                        'homepage',
                        ['_locale' => $language->getId()],
                        UrlGeneratorInterface::ABSOLUTE_URL
                    ))
                    ->setChangeFrequency($this->getChangeFrequency($guide))
                    ->setLastModification($homepage->getUpdatedAt()->format('c'))
                    ->setPriority($this->getPriority($guide))
                    ->setAlternates($alternates);
            }
        } else {
            $entries[] = (new Url())
                ->setLocation($this->router->generate('homepage', array(), UrlGeneratorInterface::ABSOLUTE_URL))
                ->setChangeFrequency($this->getChangeFrequency($guide))
                ->setLastModification($homepage->getUpdatedAt()->format("c"))
                ->setPriority($this->getPriority($guide));
        }
        if (null !== $output) {
            $section->overwrite("Generate homepage entries... OK (".$this->getTimerValue()."s)");
        }
        return $entries;
    }
}
