<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Extractor;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Model\Language;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class AbstractExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractExtractor
{
    protected ?array $languages = null;
    private ?int $timer = null;

    /**
     * @param RouterInterface       $router
     * @param ApiClient             $apiClient
     * @param ResourceGuideProvider $resourceGuideProvider
     * @param array                 $settings
     * @param bool                  $multilingual
     */
    public function __construct(
        protected RouterInterface $router,
        protected ApiClient $apiClient,
        protected ResourceGuideProvider $resourceGuideProvider,
        protected array $settings = [],
        protected bool $multilingual = true
    ) {
    }

    /**
     * @param object $resource
     *
     * @return ResourceGuide
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     */
    protected function getResourceGuide(object $resource): ResourceGuide
    {
        return $this->resourceGuideProvider->getGuide($resource->getGuide());
    }

    /**
     * @param ResourceGuide|null $guide
     *
     * @return string
     */
    protected function getChangeFrequency(?ResourceGuide $guide = null): string
    {
        return (null !== $guide && $guide->getSitemapFrequency()) ? $guide->getSitemapFrequency() : "monthly";
    }

    /**
     * @param ResourceGuide|null $guide
     *
     * @return float
     */
    protected function getPriority(?ResourceGuide $guide = null): float
    {
        return (null !== $guide && $guide->getSitemapPriority()) ? $guide->getSitemapPriority() : 0.5;
    }

    /**
     * @return array
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    protected function getLanguages(): array
    {
        if ($this->languages !== null) {
            return $this->languages;
        }
        $this->languages = $this->apiClient->getCollection(
            "languages?enabled=true",
            [],
            Language::class,
            null,
            false,
            false
        );

        return $this->languages;
    }

    /**
     * @return ApiClient
     */
    public function getApiClient(): ApiClient
    {
        return $this->apiClient;
    }

    /**
     * @return void
     */
    protected function startTimer(): void
    {
        $this->timer = hrtime(true);
    }

    /**
     * @return float
     */
    protected function getTimerValue(): float
    {
        $timeEnd = hrtime(true);
        return round(($timeEnd - $this->timer)/1000000000, 2);
    }
}
