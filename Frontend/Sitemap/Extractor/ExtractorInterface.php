<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Extractor;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface ExtractorInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ExtractorInterface
{
    /**
     * @param array                $entries
     * @param OutputInterface|null $output
     *
     * @return array
     */
    public function extract(array $entries = [], ?OutputInterface $output = null): array;
}
