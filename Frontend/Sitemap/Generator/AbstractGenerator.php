<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Generator;

use SG\CmsBundle\Frontend\Sitemap\Extractor\ExtractorInterface;
use Twig\Environment;

/**
 * Class AbstractGenerator
 * Agnostic class generator for xml sitemap
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractGenerator
{
    /**
     * @param Environment $twig
     * @param array       $extractors
     * @param array       $templates
     */
    public function __construct(protected Environment $twig, protected array $extractors = [], protected array $templates = array())
    {
    }

    /**
     * Add an extractor services to the extractors array.
     *
     * @param ExtractorInterface $extractor
     *
     * @return void
     */
    public function addExtractor(ExtractorInterface $extractor): void
    {
        $this->extractors[] = $extractor;
    }
}
