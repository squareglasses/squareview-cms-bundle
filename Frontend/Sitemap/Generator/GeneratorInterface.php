<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Generator;

use Symfony\Component\Console\Output\OutputInterface;

/**
 * Interface GeneratorInterface
 * Interface for sitemap generators
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface GeneratorInterface
{
    /**
     * @param OutputInterface|null $output
     *
     * @return string|array
     */
    public function generate(?OutputInterface $output = null): string|array;
}
