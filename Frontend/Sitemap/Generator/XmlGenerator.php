<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Generator;

use Symfony\Component\Console\Output\OutputInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class XmlGenerator
 * Generator for xml sitemap
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class XmlGenerator extends AbstractGenerator implements GeneratorInterface
{
    /**
     * @param OutputInterface|null $output
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function generate(?OutputInterface $output = null): string
    {
        $urls = array();

        foreach ($this->extractors as $extractor) {
            $urls = $extractor->extract($urls, $output);
        }

        return $this->twig->render('@SGCms/frontend/sitemap/sitemap.xml.twig', array(
            'urls' => $urls
        ));
    }
}
