<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Sitemap\Model;

use App\Model\Content;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;

/**
 * Class Url
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Url
{
    private ?string $location = null;
    private ?string $lastModification = null;
    private ?string $changeFrequency = null;
    private ?float $priority = null;
    /** @var array<Alternate> $alternates  */
    private array $alternates = [];
    private ?Content $resource = null;

    /**
     * @return string|null
     */
    public function getLocation(): ?string
    {
        return $this->location;
    }

    /**
     * @param string|null $location
     *
     * @return $this
     */
    public function setLocation(?string $location): self
    {
        $this->location = $location;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastModification(): ?string
    {
        return $this->lastModification;
    }

    /**
     * @param string|null $lastModification
     *
     * @return $this
     */
    public function setLastModification(?string $lastModification): self
    {
        $this->lastModification = $lastModification;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getChangeFrequency(): ?string
    {
        return $this->changeFrequency;
    }

    /**
     * @param string|null $changeFrequency
     *
     * @return $this
     */
    public function setChangeFrequency(?string $changeFrequency): self
    {
        $this->changeFrequency = $changeFrequency;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getPriority(): ?float
    {
        return $this->priority;
    }

    /**
     * @param float|null $priority
     *
     * @return $this
     */
    public function setPriority(?float $priority): self
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * @return array<Alternate>
     */
    public function getAlternates(): array
    {
        return $this->alternates;
    }

    /**
     * @param array $alternates
     *
     * @return $this
     */
    public function setAlternates(array $alternates): self
    {
        $this->alternates = $alternates;

        return $this;
    }

    /**
     * @param Alternate $alternate
     *
     * @return $this
     */
    public function addAlternate(Alternate $alternate): self
    {
        $this->alternates[] = $alternate;

        return $this;
    }

    /**
     * @return Content|null
     */
    public function getResource(): ?Content
    {
        return $this->resource;
    }

    /**
     * @param Content|null $resource
     *
     * @return Url
     */
    public function setResource(?Content $resource): Url
    {
        $this->resource = $resource;
        return $this;
    }
}
