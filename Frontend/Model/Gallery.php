<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Common\Contracts\GalleryInterface;

/**
 * Class Gallery
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Gallery implements GalleryInterface
{
    protected ?int $id = null;
    protected ?string $name = null;
    protected ?string $identifier = null;

    /** @var Media[] $medias */
    protected array $medias = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return void
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return void
     */
    public function setIdentifier(?string $identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return array
     */
    public function getMedias(): array
    {
        return $this->medias;
    }

    /**
     * @param array $medias
     *
     * @return void
     */
    public function setMedias(array $medias): void
    {
        $this->medias = $medias;
    }

    /**
     * @param Media $media
     *
     * @return void
     */
    public function addMedia(Media $media): void
    {
        $this->medias[] = $media;
    }
}
