<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

/**
 * Class ModuleParameter
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class ModuleParameter
{
    private ?int $id = null;
    private ?string $identifier = null;
    private ?string $value = null;
    private ?string $resourceClass = null;
    private ?string $resourceIdentifier = null;
    private ?array $resource = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return $this
     */
    public function setIdentifier(?string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string|null $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(?string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceIdentifier(): ?string
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param string|null $resourceIdentifier
     *
     * @return $this
     */
    public function setResourceIdentifier(?string $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getResource(): ?array
    {
        return $this->resource;
    }

    /**
     * @param array|null $resource
     *
     * @return void
     */
    public function setResource(?array $resource): void
    {
        $this->resource = $resource;
    }
}
