<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

/**
 * Class Route
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class Route
{
    private ?int $id;
    private string $path;
    private string $name;
    private string $locale;
    protected int $resourceIdentifier;
    protected string $resourceClass;
    protected ?array $options = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return void
     */
    public function setPath(string $path): void
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return void
     */
    public function setLocale(string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int $resourceIdentifier
     *
     * @return void
     */
    public function setResourceIdentifier(int $resourceIdentifier): void
    {
        $this->resourceIdentifier = $resourceIdentifier;
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return $this->resourceClass;
    }

    /**
     * @param string $resourceClass
     *
     * @return void
     */
    public function setResourceClass(string $resourceClass): void
    {
        $this->resourceClass = $resourceClass;
    }

    /**
     * @return string[]|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array<string>|null $options
     *
     * @return void
     */
    public function setOptions(?array $options): void
    {
        $this->options = $options;
    }
}
