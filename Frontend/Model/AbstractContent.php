<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Common\Contracts\HasMediasInterface;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Model\Media;

/**
 * Class AbstractContent
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AbstractContent implements HasMediasInterface
{
    protected ?string $slug = null;
    protected ?string $name = null;
    protected ?string $title = null;
    protected ?string $description = null;
    protected ?string $summary = null;
    protected ?string $guide = null;
    protected ?string $discriminator = null;
    protected ?DateTimeInterface $date = null;
    protected ?DateTimeInterface $updatedAt = null;
    protected ?bool $enabled = false;


    /** @var Collection<Media> $medias */
    protected Collection $medias;

    public function __construct()
    {
        $this->medias       = new ArrayCollection();
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     *
     * @return $this
     */
    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return $this
     */
    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param string|null $summary
     *
     * @return $this
     */
    public function setSummary(?string $summary): self
    {
        $this->summary = $summary;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuide(): ?string
    {
        return $this->guide;
    }

    /**
     * @param string|null $guide
     *
     * @return $this
     */
    public function setGuide(?string $guide): self
    {
        $this->guide = $guide;

        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param DateTimeInterface|null $date
     *
     * @return $this
     */
    public function setDate(?DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @return Collection<MediaModelInterface>
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    /**
     * @param MediaModelInterface $media
     *
     * @return void
     */
    public function addMedia(MediaModelInterface $media): void
    {
        if (!$this->medias->contains($media)) {
            $this->medias[] = $media;
        }
    }

    /**
     * @param MediaModelInterface $media
     *
     * @return void
     */
    public function removeMedia(MediaModelInterface $media): void
    {
        if ($this->medias->contains($media)) {
            $this->medias->removeElement($media);
        }
    }

    /**
     * @param string $identifier
     *
     * @return MediaModelInterface|null
     */
    public function getMediaByIdentifier(string $identifier): ?MediaModelInterface
    {
        foreach ($this->medias as $media) {
            if ($media->getIdentifier() === $identifier) {
                return $media;
            }
        }
        return null;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    /**
     * @param DateTimeInterface|null $updatedAt
     *
     * @return $this
     */
    public function setUpdatedAt(?DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getEnabled(): ?bool
    {
        return $this->enabled;
    }

    /**
     * @param bool|null $enabled
     *
     * @return $this
     */
    public function setEnabled(?bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDiscriminator(): ?string
    {
        return $this->discriminator;
    }

    /**
     * @param string|null $discriminator
     *
     * @return $this
     */
    public function setDiscriminator(?string $discriminator): self
    {
        $this->discriminator = $discriminator;

        return $this;
    }
}
