<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

use SG\CmsBundle\Frontend\Exception\MetaTagValidationException;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validation;

/**
 * Class MetaTag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class MetaTag
{
    private ?string $name = null;
    private ?string $type = null;
    private ?string $content = null;

    public function getName(): ?string
    {
        return $this->formatName($this->name);
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return $this
     */
    public function setContent(?string $content = null): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return match ($this->getType()) {
            'name' => '<meta name="' . $this->getName() . '" content="' . $this->getContent() . '" />',
            'title' => '<title>' . $this->getContent() . '</title>',
            'property' => '<meta property="' . $this->getName() . '" content="' . $this->getContent() . '" />',
            default => "",
        };
    }

    /**
     * @param array $input
     *
     * @return bool
     * @throws MetaTagValidationException
     */
    public static function validateDeserializationInput(array $input): bool
    {
        $validator = Validation::createValidator();
        $constraint = new Assert\Collection([
            "fields" => [
                'name' => new Assert\NotBlank(["message" => "MetaTag configuration requires a 'name' entry."]),
                'type' => new Assert\NotBlank(["message" => "MetaTag configuration requires a 'type' entry."]),
                'content' => new Assert\Optional(),
            ],
            "missingFieldsMessage" => "Field %s is missing in MetaTag deserialization."
        ]);
        $violations = $validator->validate($input, $constraint);
        if (0 !== count($violations)) {
            throw new MetaTagValidationException($violations);
        }

        return true;
    }

    /**
     * @param string $input
     *
     * @return string
     */
    private function formatName(string $input): string
    {
        return strtolower(preg_replace('/(?<!^)[A-Z]/', ':$0', $input));
    }
}
