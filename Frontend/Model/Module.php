<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Exception;
use RuntimeException;
use SG\CmsBundle\Common\Contracts\HasGalleriesTrait;
use SG\CmsBundle\Common\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Common\Contracts\HasMediasInterface;
use SG\CmsBundle\Common\Contracts\HasMediasTrait;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Frontend\Contracts\ModuleInterface;
use SG\CmsBundle\Frontend\Contracts\ModuleGuideInterface;

/**
 * Class Module
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Module implements ModuleInterface, HasMediasInterface, HasGalleriesInterface
{
    use HasMediasTrait;
    use HasGalleriesTrait;

    /**
     * @var ModuleParameter[] $parameters
     */
    private array $parameters = [];
    private ?string $locale = null;
    private ?int $position = null;
    private ?string $zone = null;
    private ?string $guideIdentifier = null;
    private ?ModuleGuideInterface $guide = null;

    /** @var Collection<Media> $medias */
    protected Collection $medias;

    /** @var Collection<Gallery> $galleries */
    protected Collection $galleries;

    public function __construct()
    {
        $this->medias = new ArrayCollection();
        $this->galleries = new ArrayCollection();
    }

    /**
     * @return ModuleParameter[]
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param string $identifier
     *
     * @return ModuleParameter|null
     */
    public function getParameter(string $identifier): ?ModuleParameter
    {
        foreach ($this->parameters as $parameter) {
            if ($parameter->getIdentifier() === $identifier) {
                return $parameter;
            }
        }
        return null;
    }

    /**
     * @param array<ModuleParameter> $parameters
     *
     * @return void
     */
    public function setParameters(array $parameters): void
    {
        $this->parameters = $parameters;
    }

    /**
     * @param ModuleParameter $parameter
     *
     * @return void
     */
    public function addParameter(ModuleParameter $parameter): void
    {
        $this->parameters[] = $parameter;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     *
     * @return void
     */
    public function setLocale(?string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return string|null
     */
    public function getZone(): ?string
    {
        return $this->zone;
    }

    /**
     * @param string|null $zone
     *
     * @return void
     */
    public function setZone(string $zone = null): void
    {
        $this->zone = $zone;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return void
     */
    public function setPosition(?int $position): void
    {
        $this->position = $position;
    }

    /**
     * @return string|null
     */
    public function getGuideIdentifier(): ?string
    {
        return $this->guideIdentifier;
    }

    /**
     * @param string|null $guideIdentifier
     *
     * @return void
     */
    public function setGuideIdentifier(?string $guideIdentifier): void
    {
        $this->guideIdentifier = $guideIdentifier;
    }

    /**
     * @return ModuleGuideInterface|null
     */
    public function getGuide(): ?ModuleGuideInterface
    {
        return $this->guide;
    }

    /**
     * @param ModuleGuideInterface $guide
     *
     * @return void
     */
    public function setGuide(ModuleGuideInterface $guide): void
    {
        $this->guide = $guide;
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function getView(): ?string
    {
        if (!$this->getGuide()) {
            throw new RuntimeException("ResourceGuide not configured");
        }
        return $this->getGuide()->getView();
    }

    /**
     * @return string|null
     * @throws Exception
     */
    public function getPattern(): ?string
    {
        if (!$this->getGuide()) {
            throw new RuntimeException("ResourceGuide not configured");
        }
        return $this->getGuide()->getPattern();
    }

    /**
     * @return null[]|ModuleGuideInterface[]
     */
    public function getViewParameters(): array
    {
        $parameters = [
            'guide' => $this->getGuide()
        ];
        $patternParameters = [];
        foreach ($this->getParameters() as $param) {
            $parameters[$param->getIdentifier()] = $param->getValue();
            $patternParameters["{".$param->getIdentifier()."}"] = $param->getValue();
        }
        $parameters['parameters'] = $patternParameters;
        return $parameters;
    }
}
