<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

use SG\CmsBundle\Frontend\Contracts\ModuleGuideInterface;

/**
 * Class ModuleGuide
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class ModuleGuide implements ModuleGuideInterface
{
    private ?string $name = null;
    private ?string $renderer = null;
    private ?string $view = null;
    private ?string $pattern = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getRenderer(): ?string
    {
        return $this->renderer;
    }

    /**
     * @param string $renderer
     *
     * @return void
     */
    public function setRenderer(string $renderer): void
    {
        $this->renderer = $renderer;
    }

    /**
     * @return string|null
     */
    public function getView(): ?string
    {
        return $this->view;
    }

    /**
     * @param string|null $view
     *
     * @return void
     */
    public function setView(?string $view = null): void
    {
        $this->view = $view;
    }

    /**
     * @return string|null
     */
    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    /**
     * @param string|null $pattern
     *
     * @return void
     */
    public function setPattern(?string $pattern = null): void
    {
        $this->pattern = $pattern;
    }
}
