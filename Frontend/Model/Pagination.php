<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Model;

/**
 * Class Pagination
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Pagination
{
    protected ?int $count = null;
    protected ?int $page = null;
    protected ?int $records = null;
    protected ?int $limit = null;
    protected ?int $pageRange = 5;

    /**
     * @return int|null
     */
    public function getCount(): ?int
    {
        return $this->count;
    }

    /**
     * @param int|null $count
     *
     * @return void
     */
    public function setCount(?int $count): void
    {
        $this->count = $count;
    }

    /**
     * @return int|null
     */
    public function getPage(): ?int
    {
        return $this->page;
    }

    /**
     * @param int|null $page
     *
     * @return void
     */
    public function setPage(?int $page): void
    {
        $this->page = $page;
    }

    /**
     * @return int|null
     */
    public function getRecords(): ?int
    {
        return $this->records;
    }

    /**
     * @param int|null $records
     *
     * @return void
     */
    public function setRecords(?int $records): void
    {
        $this->records = $records;
    }

    /**
     * @return int|null
     */
    public function getLimit(): ?int
    {
        return $this->limit;
    }

    /**
     * @param int|null $limit
     *
     * @return void
     */
    public function setLimit(?int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @return int|null
     */
    public function getPageRange(): ?int
    {
        return $this->pageRange;
    }

    /**
     * @param int|null $pageRange
     *
     * @return void
     */
    public function setPageRange(?int $pageRange): void
    {
        $this->pageRange = $pageRange;
    }

    /**
     * @return array
     */
    public function getViewData(): array
    {
        $pageCount = $this->getCount();
        $current = $this->getPage();

        if ($pageCount < $current) {
            $this->page = $current = $pageCount;
        }

        if ($this->pageRange > $pageCount) {
            $this->pageRange = $pageCount;
        }

        $delta = ceil($this->pageRange / 2);

        if ($current - $delta > $pageCount - $this->pageRange) {
            $pages = range($pageCount - $this->pageRange + 1, $pageCount);
        } else {
            if ($current - $delta < 0) {
                $delta = $current;
            }

            $offset = $current - $delta;
            $pages = range($offset + 1, $offset + $this->pageRange);
        }

        $proximity = floor($this->pageRange / 2);

        $startPage  = $current - $proximity;
        $endPage    = $current + $proximity;

        if ($startPage < 1) {
            $endPage = min($endPage + (1 - $startPage), $pageCount);
            $startPage = 1;
        }

        if ($endPage > $pageCount) {
            $startPage = max($startPage - ($endPage - $pageCount), 1);
            $endPage = $pageCount;
        }

        $viewData = array(
            'last'              => $pageCount,
            'current'           => $current,
            'first'             => 1,
            'pageCount'         => $pageCount,
            'totalCount'        => $this->records,
            'pageRange'         => $this->pageRange,
            'startPage'         => $startPage,
            'endPage'           => $endPage
        );

        if ($current > 1) {
            $viewData['previous'] = $current - 1;
        }

        if ($current < $pageCount) {
            $viewData['next'] = $current + 1;
        }

        $viewData['pagesInRange'] = $pages;
        $viewData['firstPageInRange'] = min($pages);
        $viewData['lastPageInRange']  = max($pages);

        $viewData['currentItemCount'] = $this->count;
        $viewData['firstItemNumber'] = 0;
        $viewData['lastItemNumber'] = 0;
        if ($viewData['totalCount'] > 0) {
            $viewData['firstItemNumber'] = (($current - 1) * $this->limit) + 1;
            $viewData['lastItemNumber'] = $viewData['firstItemNumber'] + $viewData['currentItemCount'] - 1;
        }

        return $viewData;
    }
}
