<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\ReCaptcha\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class IsTrue
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Annotation
 * @Target("PROPERTY")
 */
class IsTrue extends Constraint
{
    public string $message = "Le captcha n'est pas valide. Veuillez soumettre le formulaire de nouveau.";
    public string $invalidHostMessage = "Le captcha n'a pas été traité sur le bon domaine.";
    public string $weakScoreMessage = "Le captcha vous considère comme un robot. Veuillez soumettre le formulaire de nouveau.";

    /**
     * @return array|string|string[]
     */
    public function getTargets(): array|string
    {
        return Constraint::PROPERTY_CONSTRAINT;
    }

    /**
     * @return string
     */
    public function validatedBy(): string
    {
        return 'sg_recaptcha.true';
    }
}
