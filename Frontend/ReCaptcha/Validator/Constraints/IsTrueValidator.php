<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\ReCaptcha\Validator\Constraints;

use JsonException;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class IsTrueValidator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class IsTrueValidator extends ConstraintValidator
{
    /**
     * @param bool            $enabled
     * @param string          $secretKey
     * @param bool            $verifyHost
     * @param float           $scoreThreshold
     * @param RequestStack    $requestStack
     * @param LoggerInterface $logger
     */
    public function __construct(
        protected bool $enabled,
        protected string $secretKey,
        protected bool $verifyHost,
        protected float $scoreThreshold,
        protected RequestStack $requestStack,
        protected LoggerInterface $logger
    ) {
    }

    /**
     * @param            $value
     * @param Constraint $constraint
     *
     * @return void
     * @throws JsonException
     */
    public function validate($value, Constraint $constraint): void
    {
        // if recaptcha is disabled, always valid
        if (!$this->enabled || null === $this->requestStack->getMainRequest()) {
            return;
        }

        if (null === $this->requestStack->getCurrentRequest()) {
            throw new RuntimeException("Request not found");
        }

        if ($this->requestStack->getCurrentRequest()->getMethod() !== "POST") {
            return;
        }

        $response = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=".$this->secretKey.'&response='.$value);
        $decodedResponse = json_decode($response, false, 512, JSON_THROW_ON_ERROR);
        $this->logger->info($response);
        if ($decodedResponse->success === true) {
            if ($this->verifyHost && $decodedResponse->hostname !== $this->requestStack->getMainRequest()->getHost()) {
                $this->logger->info($constraint->invalidHostMessage);
                $this->context->addViolation($constraint->invalidHostMessage);
            }
            if ($decodedResponse->score >= $this->scoreThreshold) {
                return;
            }

            $this->logger->info($constraint->weakScoreMessage);
            $this->context->addViolation($constraint->weakScoreMessage);
        } else {
            $this->logger->info($constraint->message);
            $this->context->addViolation($constraint->message);
        }
    }
}
