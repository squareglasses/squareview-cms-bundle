<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\ReCaptcha\Twig\Extension;

use RuntimeException;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ReCaptchaExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ReCaptchaExtension extends AbstractExtension
{
    /**
     * @param RequestStack $requestStack
     * @param string       $publicKey
     */
    public function __construct(private readonly RequestStack $requestStack, private readonly string $publicKey)
    {
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return array(
            new TwigFunction('init_recaptcha', array($this, 'initRecaptcha'), array(
                'is_safe' => array('html')
            )),
        );
    }

    /**
     * Return the script tag to include google recaptcha
     *
     * @return string
     */
    public function initRecaptcha(): string
    {
        if (null === $this->requestStack->getMainRequest()) {
            throw new RuntimeException("Request not found");
        }
        return '<script src="https://www.google.com/recaptcha/api.js?render='.$this->publicKey.'&hl='.$this->requestStack->getMainRequest()->getLocale().'"></script>';
    }
}
