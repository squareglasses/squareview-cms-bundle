<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\ReCaptcha\Form\Type;

use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReCaptchaType
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ReCaptchaType extends AbstractType
{
    /**
     * @param bool   $enabled
     * @param string $publicKey
     */
    public function __construct(protected bool $enabled, protected string $publicKey)
    {
    }

    /**
     * @param FormView      $view
     * @param FormInterface $form
     * @param array         $options
     *
     * @return void
     */
    public function buildView(FormView $view, FormInterface $form, array $options): void
    {
        $view->vars = array_replace($view->vars, array(
            'recaptcha_enabled' => $this->enabled,
        ));

        if (!$this->enabled) {
            return;
        }

        $view->vars = array_replace($view->vars, array(
            'public_key' => $this->publicKey,
            'action' => $options["action"]
        ));
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults(array(
            'action' => null
        ));
        $resolver->setRequired('action');
        $resolver->addAllowedTypes("action", "string");
    }

    /**
     * @return string|null
     */
    public function getParent(): ?string
    {
        return HiddenType::class;
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return 'recaptcha';
    }
}
