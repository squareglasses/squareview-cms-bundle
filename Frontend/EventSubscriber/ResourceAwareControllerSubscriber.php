<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\EventSubscriber;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Exception\HttpClientResourceNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Controller\CmsHomepageController;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Exception\WebsiteNotFoundException;
use SG\CmsBundle\Common\HttpClient\Response\ResponseParser;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Contracts\ResourceAwareControllerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ResourceAwareControllerSubscriber
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceAwareControllerSubscriber implements EventSubscriberInterface
{
    private string $defaultLocale;
    private ResponseParser $responseParser;

    /**
     * @param ApiClient             $apiClient
     * @param ResourceGuideProvider $guideProvider
     * @param ResourceBag           $cmsBag
     * @param WebsiteBag            $websiteBag
     * @param ConfigurationBag      $configurationBag
     */
    public function __construct(
        private readonly ApiClient             $apiClient,
        private readonly ResourceGuideProvider $guideProvider,
        private readonly ResourceBag           $cmsBag,
        private readonly WebsiteBag            $websiteBag,
        private readonly ConfigurationBag      $configurationBag
    ) {
        $this->defaultLocale    = $configurationBag->getDefaultLocale();
        $this->responseParser   = new ResponseParser();
    }

    /**
     * @param ControllerEvent $event
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws JsonException
     * @throws InvalidFormInputException
     * @throws HttpExceptionInterface
     * @throws Exception
     */
    public function onKernelController(ControllerEvent $event): void
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony, but it may happen.
         * If it is a class, it comes in array format
         * If it is an adr format controller, it comes in a class format, not an array
         */

        if (!is_array($controller) && !$controller instanceof ResourceAwareControllerInterface) {
            return;
        }

        if (is_array($controller)) {
            $controller = $controller[0];
        }

        if ($controller instanceof ResourceAwareControllerInterface) {
            $request = $event->getRequest();
            $locale = $request->get('_locale', $this->defaultLocale);

            if ($controller instanceof CmsHomepageController) {
                $path = 'homepage';
            } else {
                $path = $request->attributes->get('_path');
            }

            // Getting the website and configure the website bag
            if ($this->configurationBag->getWebsiteIdentifier() !== null && $this->websiteBag->getResource() === null) {
                try {
                    $response = $this->apiClient->apiRequest(
                        '/'.$locale.'/website/'.$this->configurationBag->getWebsiteIdentifier(),
                        'GET',
                        []
                    );
                    $this->websiteBag->setRawDatas($response->toArray());
                } catch (HttpClientResourceNotFoundException $e) {
                    throw new WebsiteNotFoundException($this->configurationBag->getWebsiteIdentifier(), $e);
                }
            }

            $endpoint = '/'.$locale.'/'.$path;
            $endpoint = str_replace("?}", "|qtag}", $endpoint);
           // dump($endpoint);die;
            $useCachedResult = true;
            // Handle draft preview
            if ($request->query->get('preview')) {
                $endpoint .= '?'.$request->getQueryString();
                $useCachedResult = false;
            }


            $apiResponse = $this->apiClient->apiRequest($endpoint, 'GET', [], $useCachedResult);
            $this->cmsBag->setResponseHeaders($apiResponse->getHeaders());
            $serializedResource = $this->responseParser->parseOne($apiResponse);

            $guide = $this->guideProvider->getGuide($serializedResource->get('resource')['guide']);

            // Configure controller via cms bag
            $this->cmsBag->setGuide($guide)->setRawDatas($serializedResource->getAttributes());
        }
    }

    /**
     * @return array<string,mixed>
     */
    #[ArrayShape([KernelEvents::CONTROLLER => "array[]"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => [['onKernelController', 100]],
        ];
    }
}
