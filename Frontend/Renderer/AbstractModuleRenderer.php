<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Renderer;

use Exception;
use SG\CmsBundle\Frontend\Contracts\ModuleInterface;
use Twig\Environment;
use SG\CmsBundle\Frontend\Contracts\ModuleRendererInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class AbstractModuleRenderer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractModuleRenderer implements ModuleRendererInterface
{
    /** @var array<string,mixed> $extraViewParameters */
    protected array $extraViewParameters = [];
    protected ?ModuleInterface $module = null;

    /**
     * @param Environment $environment
     */
    public function __construct(protected Environment $environment)
    {
    }

    /**
     * @param ModuleInterface $module
     *
     * @return ModuleRendererInterface
     */
    public function setModule(ModuleInterface $module): ModuleRendererInterface
    {
        $this->module = $module;
        return $this;
    }

    /**
     * @return ModuleRendererInterface
     */
    public function execute(): ModuleRendererInterface
    {
        return $this;
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws Exception
     */
    public function render(): string
    {
        return $this->environment->render(
            $this->getView(),
            $this->getViewParameters()
        );
    }

    /**
     * @return string|null
     * @throws Exception
     */
    protected function getView(): ?string
    {
        return $this->module->getPattern() ? "@SGCms/frontend/modules/pattern.html.twig" : $this->module->getView();
    }

    /**
     * @return array<string,mixed>
     */
    protected function getViewParameters(): array
    {
        return array_merge(
            ['module' => $this->module],
            array_merge($this->module->getViewParameters(), $this->extraViewParameters)
        );
    }
}
