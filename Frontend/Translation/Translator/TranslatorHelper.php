<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Translation\Translator;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\HttpClient\Response\ResponseParser;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class TranslatorHelper
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslatorHelper
{
    /**
     * @param ResourceBag         $resourceBag
     * @param WebsiteBag          $websiteBag
     * @param Translator          $translator
     * @param TranslatorInterface $fallbackTranslator
     * @param RouterInterface     $router
     * @param RequestStack        $requestStack
     * @param ApiClient           $apiClient
     */
    public function __construct(
        private readonly ResourceBag         $resourceBag,
        private readonly WebsiteBag          $websiteBag,
        private readonly Translator          $translator,
        private readonly TranslatorInterface $fallbackTranslator,
        private readonly RouterInterface     $router,
        private readonly RequestStack        $requestStack,
        private readonly ApiClient           $apiClient,
        private readonly ConfigurationBag    $configurationBag
    ) {
    }

    /**
     * @param string      $message
     * @param array       $arguments
     * @param string|null $domain
     * @param string|null $locale
     *
     * @return string
     */
    public function trans(
        string $message,
        array $arguments = [],
        string $domain = null,
        string $locale = null
    ): string {
        $catalog = null;
        if (null !== $domain) {
            $catalog = $this->getCatalogForDomain($domain);
        }

        if (null !== $catalog) {
            $translatedMessage = $this->getTranslatedMessage(
                $catalog,
                $message,
                $arguments,
                $domain,
                $locale
            );
            if ($translatedMessage === $message) {
                $translatedMessage = $this->fallbackTranslator->trans($message, $arguments, $domain, $locale);
            }
        } else {
            $translatedMessage = $this->getTranslatedMessage(
                $this->resourceBag->getResourceTranslations(),
                $message,
                $arguments,
                $domain,
                $locale
            );

            if ($translatedMessage === $message) {
                $translatedMessage = $this->getTranslatedMessage(
                    $this->resourceBag->getLayoutTranslations(),
                    $message,
                    $arguments,
                    $domain,
                    $locale
                );
                if ($translatedMessage === $message) {
                    $translatedMessage = $this->fallbackTranslator->trans($message, $arguments, $domain, $locale);
                }
            }
        }

        return $translatedMessage;
    }

    /**
     * @param string $route
     * @param array  $zones
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function loadResourceTranslations(string $route, array $zones = [], ?string $locale = null): void
    {
        if (null === $this->requestStack->getMainRequest()) {
            throw new RuntimeException("Request is null.");
        }

        if (null === $locale) {
            $locale = $this->requestStack->getMainRequest()->getLocale();
        }
        if (true === $this->configurationBag->isMultilingual()) {
            if (!array_key_exists($route.'.'.$locale, $this->router->getRouteCollection()->all())) {
                throw new RuntimeException("No route found for '".$route.'.'.$locale."'");
            }
            $endpoint = $this->router->generate($route, ['_locale' => $locale]);
        } else {
            if (!array_key_exists($route, $this->router->getRouteCollection()->all())) {
                throw new RuntimeException("No route found for '".$route."'");
            }
            $endpoint = $locale.$this->router->generate($route);
        }

        $responseParser = new ResponseParser();
        $serializedResource = $responseParser
            ->parseOne($this->apiClient->apiRequest($endpoint, 'GET', []));

        $this->resourceBag->addTranslations("resource", $serializedResource->getAttributes()["translations"]["resource"], $zones);
    }

    /**
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws InvalidFormInputException
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws TransportExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function loadGuideTranslations(string $guide, array $zones = [], ?string $locale = null): void
    {
        if (null === $locale) {
            if (null === $this->requestStack->getMainRequest()) {
                $locale = "fr";
            } else {
                $locale = $this->requestStack->getMainRequest()->getLocale();
            }
        }

        $parameters = [
            "locale" =>   $locale,
            "zones" => $zones
        ];

        $responseParser = new ResponseParser();
        $serializedResource = $responseParser
            ->parseOne($this->apiClient->apiRequest("guide_translations/layout_guide/".$guide.'?'.http_build_query($parameters), 'GET', []));

        $this->resourceBag->addTranslations("resource", $serializedResource->getAttributes()["translations"], $zones);
    }

    /**
     * @param string $domain
     *
     * @return array|null
     */
    private function getCatalogForDomain(string $domain): ?array
    {
        // Domain is in a resource properties translation zone
        if (array_key_exists($domain, $this->resourceBag->getResourceTranslations())) {
            return $this->resourceBag->getResourceTranslations();
        }

        // Domain is in a resource layout translation zone
        if (array_key_exists($domain, $this->resourceBag->getLayoutTranslations())) {
            return $this->resourceBag->getLayoutTranslations();
        }

        foreach ($this->resourceBag->getTranslations() as $catalog) {
            if (array_key_exists($domain, $catalog)) {
                return $catalog;
            }
        }

        return $this->websiteBag->getTranslations();
    }

    /**
     * @param array       $catalog
     * @param string      $message
     * @param array       $arguments
     * @param string|null $domain
     * @param string|null $locale
     *
     * @return string
     */
    private function getTranslatedMessage(array $catalog, string $message, array $arguments = [], string $domain = null, string $locale = null): string
    {
        $this->translator->setCatalog($catalog);
        return $this->translator->trans($message, $arguments, $domain, $locale);
    }

    /**
     * @return array
     */
    public function getResourceTranslations(): array
    {
        return $this->resourceBag->getResourceTranslations();
    }

    /**
     * @return array
     */
    public function getLayoutTranslations(): array
    {
        return $this->resourceBag->getLayoutTranslations();
    }

    /**
     * @return array
     */
    public function getGuideTranslations(): array
    {
        return $this->resourceBag->getGuideTranslations();
    }
}
