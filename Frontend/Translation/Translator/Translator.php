<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Translation\Translator;

use InvalidArgumentException;
use RuntimeException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\Formatter\IntlFormatterInterface;
use Symfony\Component\Translation\Formatter\MessageFormatterInterface;

/**
 * Class Translator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Translator
{
    private array $catalog = [];
    private bool $hasIntlFormatter;

    /**
     * @param RequestStack                   $requestStack
     * @param MessageFormatterInterface|null $formatter
     */
    public function __construct(private readonly RequestStack $requestStack, private readonly ?MessageFormatterInterface $formatter = null)
    {
        $this->hasIntlFormatter = $formatter instanceof IntlFormatterInterface;
    }

    /**
     * @param string      $id
     * @param array       $parameters
     * @param string|null $domain
     * @param string|null $locale
     *
     * @return string
     */
    public function trans(
        string $id,
        array $parameters = [],
        string $domain = null,
        string $locale = null
    ): string {
        if (null === $locale) {
            $locale = $this->getLocale();
        }
        $this->assertValidLocale($locale);

        if (null === $domain) {
            $domain = 'messages';
        }

        if (!array_key_exists($domain, $this->catalog) || !array_key_exists($id, $this->catalog[$domain])) {
            return $id;
        }
        $message =  $this->catalog[$domain][$id];
        if (is_array($message) && array_key_exists("message", $message)) {
            $message = $message["message"];
        }
        if (is_array($message)) {
            $message = $message[0];
        }


        if ($this->hasIntlFormatter) {
            return $this->formatter->formatIntl($message, $locale, $parameters);
        }

        return $this->formatter->format($message, $locale, $parameters);
    }

    /**
     * @return string
     */
    private function getLocale(): string
    {
        if (null !== $this->requestStack->getMainRequest()) {
            return $this->requestStack->getMainRequest()->getLocale();
        }
        return "fr";
    }

    /**
     * Asserts that the locale is valid, throws an Exception if not.
     *
     * @param string $locale
     *
     * @return void
     */
    protected function assertValidLocale(string $locale): void
    {
        if (1 !== preg_match('/^[a-z0-9@_\\.\\-]*$/i', $locale)) {
            throw new InvalidArgumentException(sprintf('Invalid "%s" locale.', $locale));
        }
    }

    /**
     * @param array $catalog
     *
     * @return void
     */
    public function setCatalog(array $catalog): void
    {
        $this->catalog = $catalog;
    }
}
