<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Bag;

use JsonException;
use SG\CmsBundle\Frontend\Contracts\WebsiteBagInterface;

/**
 * Class WebsiteBag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class WebsiteBag extends AbstractPublicationBag implements WebsiteBagInterface
{
    /** @var array<string,string> $translations */
    private array $translations = [];

    /**
     * @param array<string,array> $serializedResources
     *
     * @return void
     * @throws JsonException
     */
    public function setRawDatas(array $serializedResources): void
    {
        parent::setRawDatas($serializedResources);

        foreach ($serializedResources as $resourceType => $resource) {
            if ($resourceType === "translations") {
                $this->setTranslations(array_key_exists("layout", $resource) ? $resource['layout'] : []);
            }
        }
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     *
     * @return $this
     */
    public function setTranslations(array $translations): self
    {
        $this->translations = $translations;

        return $this;
    }

    /**
     * @param string $type
     * @param array  $translations
     *
     * @return void
     */
    public function addTranslations(string $type, array $translations): void
    {
        if (array_key_exists($type, $this->translations)) {
            $this->translations[$type] = array_merge_recursive($this->translations[$type], $translations);
        } else {
            $this->translations[$type] = $translations;
        }
    }
}
