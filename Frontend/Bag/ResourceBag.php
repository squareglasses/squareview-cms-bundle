<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Bag;

use JsonException;
use SG\CmsBundle\Frontend\Model\Module;
use SG\CmsBundle\Frontend\Contracts\ModuleInterface;
use SG\CmsBundle\Common\Contracts\ResourceGuideInterface;
use SG\CmsBundle\Frontend\Contracts\ResourceBagInterface;

/**
 * Class ResourceBag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class ResourceBag extends AbstractPublicationBag implements ResourceBagInterface
{
    private ?ResourceGuideInterface $guide = null;

    /** @var array<ModuleInterface> $modules */
    private array $modules = [];

    /** @var array<array> $translations */
    private array $translations = [];

    private array $responseHeaders = [];

    /**
     * @return array
     */
    public function getResponseHeaders(): array
    {
        return $this->responseHeaders;
    }

    /**
     * @param array $responseHeaders
     */
    public function setResponseHeaders(array $responseHeaders): void
    {
        $this->responseHeaders = $responseHeaders;
    }

    public function __toString()
    {
        return (string)$this->getResource()['name'];
    }

    /**
     * @return ResourceGuideInterface|null
     */
    public function getGuide(): ?ResourceGuideInterface
    {
        return $this->guide;
    }

    /**
     * @param ResourceGuideInterface|null $guide
     *
     * @return ResourceBagInterface
     */
    public function setGuide(?ResourceGuideInterface $guide = null): ResourceBagInterface
    {
        $this->guide = $guide;

        return $this;
    }

    /**
     * @return array<ModuleInterface>|null
     */
    public function getModules(): ?array
    {
        return $this->modules;
    }

    /**
     * @param array<ModuleInterface> $modules
     *
     * @return $this
     */
    public function setModules(array $modules): self
    {
        $this->modules = $modules;

        return $this;
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     *
     * @return $this
     */
    public function setTranslations(array $translations): self
    {
        $this->translations = $translations;

        return $this;
    }

    /**
     * @param string $type
     * @param array  $translations
     * @param array  $zones
     *
     * @return void
     */
    public function addTranslations(string $type, array $translations, array $zones = []): void
    {
        if (array_key_exists($type, $this->translations) && is_array($this->translations[$type])) {
            if (count($zones) === 0) {
                $this->translations[$type] = array_merge_recursive($this->translations[$type], $translations);
            } else {
                $this->addZoneTranslations($type, $zones, $translations);
            }
        } else if (count($zones) === 0) {
            $this->translations[$type] = $translations;
        } else {
            $this->addZoneTranslations($type, $zones, $translations);
        }
    }

    /**
     * @param string $type
     * @param array  $zones
     * @param array  $translations
     *
     * @return void
     */
    private function addZoneTranslations(string $type, array $zones, array $translations): void
    {
        if (!array_key_exists($type, $this->translations)) {
            $this->translations[$type] = [];
        }
        if (is_array($this->translations[$type])) {
            foreach ($zones as $zone) {
                if (array_key_exists($zone, $this->translations[$type])) {
                    $this->translations[$type][$zone] = array_merge($this->translations[$type][$zone], $translations[$zone]);
                } else {
                    $this->translations[$type][$zone] = $translations[$zone];
                }
            }
        }
    }

    /**
     * @return array<array>
     */
    public function getLayoutTranslations(): array
    {
        return array_key_exists('layout', $this->translations) ? $this->translations['layout'] : [];
    }

    /**
     * @return array
     */
    public function getResourceTranslations(): array
    {
        return array_key_exists('resource', $this->translations) ? $this->translations['resource'] : [];
    }

    /**
     * @return array
     */
    public function getGuideTranslations(): array
    {
        foreach ($this->translations as $type => $translations) {
            if ($type !== 'resource' && $type !== 'layout') {
                return $translations;
            }
        }
        return [];
    }

    /**
     * @param array<string,array> $serializedResources
     *
     * @throws JsonException
     */
    public function setRawDatas(array $serializedResources): void
    {
        parent::setRawDatas($serializedResources);

        // Modules
        if (array_key_exists('modules', $serializedResources['resource'])) {
            $modules = [];
            foreach ($serializedResources['resource']['modules'] as $moduleArray) {
                $modules[] = $this->serializer->deserialize(json_encode($moduleArray, JSON_THROW_ON_ERROR), Module::class, 'json');
            }
            $this->setModules($modules);
        }

        foreach ($serializedResources as $resourceType => $resource) {
            if ($resourceType === "translations") {
                $this->setTranslations($resource);
            }
        }
    }
}
