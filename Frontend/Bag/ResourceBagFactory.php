<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Bag;

use JsonException;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ResourceBagFactory
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceBagFactory
{
    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param array $resources
     *
     * @return array
     * @throws JsonException
     */
    public function createCollection(array $resources): array
    {
        $resourceBags = [];
        foreach ($resources as $item) {
            $bag = new ResourceBag($this->serializer);
            $bag->setRawDatas(['resource' => $item]);
            $resourceBags[] = $bag;
        }
        return $resourceBags;
    }

    /**
     * @param array $resource
     *
     * @return ResourceBag
     * @throws JsonException
     */
    public function createItem(array $resource): ResourceBag
    {
        $bag = new ResourceBag($this->serializer);
        $bag->setRawDatas(['resource' => $resource]);
        return $bag;
    }
}
