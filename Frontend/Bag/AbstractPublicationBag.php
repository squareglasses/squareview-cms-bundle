<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Bag;

use JsonException;
use SG\CmsBundle\Common\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Common\Contracts\HasMediasInterface;
use SG\CmsBundle\Common\Contracts\GalleryInterface;
use SG\CmsBundle\Common\Contracts\MediaModelInterface;
use SG\CmsBundle\Common\Media\Model\Media;
use SG\CmsBundle\Frontend\Model\Gallery;
use SG\CmsBundle\Frontend\Model\MetaTag;
use Symfony\Component\Serializer\SerializerInterface;
use SG\CmsBundle\Frontend\Contracts\PublicationBagInterface;

/**
 * Class AbstractPublicationBag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractPublicationBag implements PublicationBagInterface, HasMediasInterface, HasGalleriesInterface
{
    protected array $rawData = [];
    protected ?array $resource = null;

    /** @var array<MetaTag> $metaTags */
    protected array $metaTags = [];

    /** @var array<MediaModelInterface> $medias */
    protected array $medias = [];

    /** @var array<GalleryInterface> $galleries */
    protected array $galleries = [];

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(protected SerializerInterface $serializer)
    {
    }

    /**
     * @return array|null
     */
    public function getResource(): ?array
    {
        return $this->resource;
    }

    /**
     * @param array $resource
     *
     * @return PublicationBagInterface
     */
    public function setResource(array $resource): PublicationBagInterface
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @return array<MetaTag>|null
     */
    public function getMetaTags(): ?array
    {
        return $this->metaTags;
    }

    /**
     * @param array<MetaTag> $metaTags
     *
     * @return $this
     */
    public function setMetaTags(array $metaTags): PublicationBagInterface
    {
        $this->metaTags = $metaTags;

        return $this;
    }

    /**
     * @return array
     */
    public function getRawData(): array
    {
        return $this->rawData;
    }

    /**
     * @param array $serializedResources
     *
     * @return void
     * @throws JsonException
     */
    public function setRawDatas(array $serializedResources): void
    {
        $this->rawData = $serializedResources;
        $this->setResource($serializedResources['resource']);

        // Medias
        if (array_key_exists('medias', $serializedResources['resource'])) {
            $medias = [];
            foreach ($serializedResources['resource']['medias'] as $mediaArray) {
                $medias[] = $this->serializer->deserialize(
                    json_encode($mediaArray, JSON_THROW_ON_ERROR),
                    Media::class,
                    'json'
                );
            }
            $this->setMedias($medias);
        }

        // Galleries
        if (array_key_exists('galleries', $serializedResources['resource'])) {
            $galleries = [];
            foreach ($serializedResources['resource']['galleries'] as $galleryArray) {
                $galleries[] = $this->serializer->deserialize(
                    json_encode($galleryArray, JSON_THROW_ON_ERROR),
                    Gallery::class,
                    'json'
                );
            }
            $this->setGalleries($galleries);
        }

        // MetaTags
        if (array_key_exists('metatags', $serializedResources['resource'])) {
            $metaTags = [];
            foreach ($serializedResources['resource']['metatags'] as $metatagArray) {
                $metaTags[] = $this->serializer->deserialize(
                    json_encode($metatagArray, JSON_THROW_ON_ERROR),
                    MetaTag::class,
                    'json'
                );
            }
            $this->setMetaTags($metaTags);
        }
    }

    /**
     * @return array<MediaModelInterface>
     */
    public function getMedias(): array
    {
        return $this->medias;
    }

    /**
     * @param array<MediaModelInterface> $medias
     *
     * @return $this
     */
    public function setMedias(array $medias): PublicationBagInterface
    {
        $this->medias = $medias;

        return $this;
    }

    /**
     * @param string $identifier
     *
     * @return MediaModelInterface|null
     */
    public function getMediaByIdentifier(string $identifier): ?MediaModelInterface
    {
        foreach ($this->medias as $media) {
            if ($media->getIdentifier() === $identifier) {
                return $media;
            }
        }
        return null;
    }

    /**
     * @return array
     */
    public function getGalleries(): array
    {
        return $this->galleries;
    }

    /**
     * @param array $galleries
     */
    public function setGalleries(array $galleries): void
    {
        $this->galleries = $galleries;
    }

    /**
     * @param string $identifier
     *
     * @return GalleryInterface|null
     */
    public function getGalleryByIdentifier(string $identifier): ?GalleryInterface
    {
        foreach ($this->galleries as $gallery) {
            if ($gallery->getIdentifier() === $identifier) {
                return $gallery;
            }
        }
        return null;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset(string $name)
    {
        return array_key_exists($name, $this->resource);
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function __get(string $name)
    {
        return $this->resource[$name] ?? null;
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function get(string $name): mixed
    {
        return $this->__get($name);
    }

    /**
     * @param string $name
     * @param $value
     * @return mixed|null
     */
    public function __set(string $name, $value)
    {
        return $this->resource[$name] = $value;
    }
}
