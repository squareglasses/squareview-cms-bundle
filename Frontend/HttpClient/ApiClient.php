<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\HttpClient;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Frontend\Bag\ResourceBagFactory;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Model\Pagination;
use Symfony\Bundle\SecurityBundle\Security;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use SG\CmsBundle\Common\HttpClient\ApiClient as BaseApiClient;

/**
 * Class ApiClient
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiClient extends BaseApiClient
{
    /**
     * @param HttpClientInterface $apiClient
     * @param SerializerInterface $serializer
     * @param LoggerInterface     $cacheTagsLogger
     * @param ResourceBagFactory  $resourceBagFactory
     * @param string|null         $apiKey
     * @param string|null         $applicationMode
     */
    public function __construct(
        HttpClientInterface                 $apiClient,
        SerializerInterface                 $serializer,
        LoggerInterface                     $cacheTagsLogger,
        Security $security,
        private readonly ResourceBagFactory $resourceBagFactory,
        ?string                             $apiKey,
        ?string                             $applicationMode = 'frontend'
    ) {
        parent::__construct($apiClient, $serializer, $cacheTagsLogger, $security, $apiKey, $applicationMode);
    }

    /**
     * @param string             $endpoint
     * @param array              $options
     * @param string|null        $model
     * @param UserInterface|null $user
     * @param bool               $useCachedResult
     *
     * @return object|null
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getItem(
        string $endpoint,
        array $options = [],
        string $model = null,
        ?UserInterface $user = null,
        bool $useCachedResult = true
    ): ?object {
        if (null !== $user) {
            $cachedResult = $this->userRequest($user, $endpoint, self::METHOD_GET, $options, $useCachedResult);
        } else {
            $cachedResult = $this->apiRequest($endpoint, self::METHOD_GET, $options, $useCachedResult);
        }
        if ($model !== null) {
            $item = $this->serializer->deserialize($cachedResult->getContent(), $model, 'json');
        } else {
            $item = $this->resourceBagFactory->createItem(json_decode(
                $cachedResult->getContent(),
                true,
                512,
                JSON_THROW_ON_ERROR
            ));
        }
        return $item;
    }

    /**
     * @param string             $endpoint
     * @param array              $options
     * @param string|null        $model
     * @param UserInterface|null $user
     * @param bool               $paginate
     * @param bool               $useCachedResult
     * @param array              $cacheTags
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getCollection(
        string $endpoint,
        array $options = [],
        ?string $model = null,
        ?UserInterface $user = null,
        bool $paginate = true,
        bool $useCachedResult = true,
        array $cacheTags = []
    ): array {
        if (null !== $user) {
            $cachedResult = $this->userRequest($user, $endpoint, self::METHOD_GET, $options, $useCachedResult, $cacheTags);
        } else {
            $cachedResult = $this->apiRequest($endpoint, self::METHOD_GET, $options, $useCachedResult, $cacheTags);
        }
        if (null !== $model) {
            $results = $this->serializer->deserialize($cachedResult->getContent(), $model.'[]', 'json');
        } else {
            $results = $this->resourceBagFactory->createCollection(json_decode($cachedResult->getContent(), true, 512, JSON_THROW_ON_ERROR));
        }
        if ($paginate) {
            $pagination = $this->getPagination($cachedResult->getHeaders());
            $computedValue = [
                'collection' => $results,
                'pagination' => $pagination
            ];
        } else {
            $computedValue = $results;
        }
        return $computedValue;
    }


    /**
     * @param string $endpoint
     * @param array  $headers
     * @param bool   $paginate
     * @param bool   $useCachedResult
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function getRawCollection(
        string $endpoint,
        array $headers = [],
        bool $paginate = false,
        bool $useCachedResult = true
    ): array {
        $headers = array_merge([
            'Content-Type' => 'application/json'
        ], $headers);
        $response = $this->apiRequest(
            $endpoint,
            self::METHOD_GET,
            [
                'headers' => $headers
            ],
            $useCachedResult
        );
        if ($paginate) {
            $pagination = $this->getPagination($response->getHeaders());
            $results = $this->resourceBagFactory->createCollection(
                json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR)
            );
            return [
                'collection' => $results,
                'pagination' => $pagination
            ];
        }

        return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
    }

    /**
     * @param array $headers
     *
     * @return Pagination
     * @throws JsonException
     */
    private function getPagination(array $headers): Pagination
    {
        $pagination = [];
        if (array_key_exists("pagination-count", $headers)) {
            $pagination = [
                'count' => (int)$headers['pagination-count'][0],
                'page' => (int)$headers['pagination-page'][0],
                'limit' => (int)$headers['pagination-limit'][0],
                'records' => (int)$headers['pagination-records'][0]
            ];
        }
        return $this->serializer->deserialize(
            json_encode($pagination, JSON_THROW_ON_ERROR),
            Pagination::class,
            'json'
        );
    }
}
