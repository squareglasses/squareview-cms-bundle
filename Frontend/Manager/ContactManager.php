<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Manager;

use App\Model\Contact;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Exception\InvalidContactInputException;
use SG\CmsBundle\Common\HttpClient\ApiClient;
use SG\CmsBundle\Common\Contracts\ContactInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ContactManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ContactManager
{
    /**
     * @param ApiClient           $apiClient
     * @param SerializerInterface $serializer
     */
    public function __construct(
        private readonly ApiClient  $apiClient,
        private readonly SerializerInterface $serializer
    ) {
    }

    /**
     * @param ContactInterface $contact
     *
     * @return ContactInterface|null
     * @throws ClientExceptionInterface
     * @throws InvalidContactInputException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws HttpExceptionInterface
     */
    public function create(ContactInterface $contact): ?ContactInterface
    {
        $response = $this->apiClient->apiRequest('contacts', ApiClient::METHOD_POST, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $this->serializer->serialize($contact, 'json', [])
        ]);

        $statusCode = $response->getStatusCode();

        if ($statusCode === 200) {
            /** @var ContactInterface $contact */
            $contact = $this->serializer->deserialize($response->getContent(), Contact::class, 'json', ['groups' => ['get']]);
            return $contact;
        }

        if ($statusCode === 400) {
            $error = json_decode($response->getContent(false), true, 512, JSON_THROW_ON_ERROR);
            if (array_key_exists("violations", $error)) {
                throw new InvalidContactInputException($error['title'], $error['violations']);
            }
        } elseif ($response->getContent() !== null) {
            throw new RuntimeException($response->getContent());
        }

        throw new InvalidContactInputException("Unknown error");
    }
}
