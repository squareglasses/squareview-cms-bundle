<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Breadcrumbs\Model;

/**
 * Class SingleBreadcrumb
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SingleBreadcrumb
{
    /**
     * @param string $text
     * @param string $url
     * @param array  $attributes
     * @param array  $translationParameters
     */
    public function __construct(
        public string $text = "",
        public string $url = "",
        public array $attributes = [],
        public array $translationParameters = []
    ) {
    }
}
