<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Breadcrumbs\Model;

use ArrayAccess;
use Countable;
use InvalidArgumentException;
use Iterator;

/**
 * Class Breadcrumbs
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Breadcrumbs implements Iterator, ArrayAccess, Countable
{
    public const DEFAULT_NAMESPACE = "default";
    private array $breadcrumbs = array(
        self::DEFAULT_NAMESPACE => array()
    );

    /**
     * @param string $namespace
     *
     * @return array
     */
    public function getNamespaceBreadcrumbs(string $namespace = self::DEFAULT_NAMESPACE): array
    {
        // Check whether requested namespace breadcrumbs is exists
        if (!$this->hasNamespaceBreadcrumbs($namespace)) {
            throw new InvalidArgumentException(sprintf(
                'The breadcrumb namespace "%s" does not exist',
                $namespace
            ));
        }

        return $this->breadcrumbs[$namespace];
    }

    /**
     * @param string $namespace
     *
     * @return bool
     */
    public function hasNamespaceBreadcrumbs(string $namespace = self::DEFAULT_NAMESPACE): bool
    {
        return isset($this->breadcrumbs[$namespace]);
    }

    /**
     * Add an item to the breadcrumbs.
     * This is a shortcut to add item in the default namespace.
     *
     * @param string $text
     * @param string $url
     * @param array  $attributes
     * @param array  $translationParameters
     *
     * @return $this
     */
    public function addItem(
        string $text,
        string $url = "",
        array $attributes = array(),
        array $translationParameters = array()
    ): self {
        return $this->addNamespaceItem(
            self::DEFAULT_NAMESPACE,
            $text,
            $url,
            $attributes,
            $translationParameters
        );
    }

    /**
     * Add an item to the breadcrumbs for a given namespace.
     *
     * @param string $namespace
     * @param string $text
     * @param string $url
     * @param array  $attributes
     * @param array  $translationParameters
     *
     * @return $this
     */
    public function addNamespaceItem(
        string $namespace,
        string $text,
        string $url = "",
        array $attributes = [],
        array $translationParameters = []
    ): self {
        $b = new SingleBreadcrumb($text, $url, $attributes, $translationParameters);
        $this->breadcrumbs[$namespace][] = $b;

        return $this;
    }

    /**
     * Add an item at a specific position in the breadcrumbs array.
     * This is a shortcut to add item in the default namespace.
     *
     * @param int    $position
     * @param string $text
     * @param string $url
     * @param array  $attributes
     * @param array  $translationParameters
     *
     * @return $this
     */
    public function addItemAtPosition(
        int $position,
        string $text,
        string $url = "",
        array $attributes = [],
        array $translationParameters = []
    ): self {
        return $this->addNamespaceItemAtPosition(self::DEFAULT_NAMESPACE, $position, $text, $url, $attributes, $translationParameters);
    }

    /**
     * Add an item at a specific position in the breadcrumbs array.
     *
     * @param string $namespace
     * @param int    $position
     * @param string $text
     * @param string $url
     * @param array  $attributes
     * @param array  $translationParameters
     *
     * @return $this
     */
    public function addNamespaceItemAtPosition(
        string $namespace,
        int $position,
        string $text,
        string $url = "",
        array $attributes = [],
        array $translationParameters = []
    ): self {
        $b = new SingleBreadcrumb($text, $url, $attributes, $translationParameters);
        array_splice($this->breadcrumbs[$namespace], $position, 0, array($b));

        return $this;
    }

    /**
     * @param string $namespace
     *
     * @return $this
     */
    public function clear(string $namespace = ""): self
    {
        if ($namespace !== '') {
            $this->breadcrumbs[$namespace] = array();
        } else {
            $this->breadcrumbs = array();
        }

        return $this;
    }

    /**
     * @param string $namespace
     *
     * @return void
     */
    public function rewind(string $namespace = self::DEFAULT_NAMESPACE): void
    {
        reset($this->breadcrumbs[$namespace]);
    }

    /**
     * @param string $namespace
     *
     * @return mixed
     */
    public function current(string $namespace = self::DEFAULT_NAMESPACE): mixed
    {
        return current($this->breadcrumbs[$namespace]);
    }

    /**
     * @param string $namespace
     *
     * @return string|int|null
     */
    public function key(string $namespace = self::DEFAULT_NAMESPACE): string|int|null
    {
        return key($this->breadcrumbs[$namespace]);
    }

    /**
     * @param string $namespace
     *
     * @return void
     */
    public function next(string $namespace = self::DEFAULT_NAMESPACE): void
    {
        next($this->breadcrumbs[$namespace]);
    }

    /**
     * @param string $namespace
     *
     * @return bool
     */
    public function valid(string $namespace = self::DEFAULT_NAMESPACE): bool
    {
        return null !== key($this->breadcrumbs[$namespace]);
    }

    /**
     * @param mixed  $offset
     * @param string $namespace
     *
     * @return bool
     */
    public function offsetExists(mixed $offset, string $namespace = self::DEFAULT_NAMESPACE): bool
    {
        return isset($this->breadcrumbs[$namespace][$offset]);
    }

    /**
     * @param mixed  $offset
     * @param mixed  $value
     * @param string $namespace
     *
     * @return void
     */
    public function offsetSet(mixed $offset, mixed $value, string $namespace = self::DEFAULT_NAMESPACE): void
    {
        $this->breadcrumbs[$namespace][$offset] = $value;
    }

    /**
     * @param mixed  $offset
     * @param string $namespace
     *
     * @return SingleBreadcrumb|null
     */
    public function offsetGet(mixed $offset, string $namespace = self::DEFAULT_NAMESPACE): ?SingleBreadcrumb
    {
        return $this->breadcrumbs[$namespace][$offset] ?? null;
    }

    /**
     * @param mixed  $offset
     * @param string $namespace
     *
     * @return void
     */
    public function offsetUnset(mixed $offset, string $namespace = self::DEFAULT_NAMESPACE): void
    {
        unset($this->breadcrumbs[$namespace][$offset]);
    }

    /**
     * @param string $namespace
     *
     * @return int
     */
    public function count(string $namespace = self::DEFAULT_NAMESPACE): int
    {
        return count($this->breadcrumbs[$namespace]);
    }
}
