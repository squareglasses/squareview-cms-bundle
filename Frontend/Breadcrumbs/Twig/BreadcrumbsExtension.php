<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Breadcrumbs\Twig;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Breadcrumbs\Model\Breadcrumbs;
use SG\CmsBundle\Frontend\Breadcrumbs\Model\SingleBreadcrumb;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Translation\Translator\TranslatorHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class BreadcrumbsExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class BreadcrumbsExtension extends AbstractExtension
{
    private array $options;

    public function __construct(
        ConfigurationBag                  $configurationBag,
        private readonly Breadcrumbs      $breadcrumbs,
        private readonly RouterInterface  $router,
        private readonly ResourceBag      $resourceBag,
        private readonly ApiClient        $apiClient,
        private readonly TranslatorHelper $translator,
        private readonly RequestStack     $requestStack
    ) {
        $this->options = array_merge($configurationBag->getBreadcrumbsConfiguration(), array(
            'namespace' => Breadcrumbs::DEFAULT_NAMESPACE, // inject default namespace to options
            'depth' => null
        ));
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return array(
            new TwigFunction("render_breadcrumbs", [$this, "renderBreadcrumbs"], [
                "needs_environment" => true,
                "is_safe" => array("html")
            ]),
        );
    }

    /**
     * @return TwigFilter[]
     */
    public function getFilters(): array
    {
        return array(
            new TwigFilter("is_final_breadcrumb", [$this, "isLastBreadcrumb"]),
        );
    }

    /**
     * @return void
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function setupBreadcrumbs(): void
    {
        $bcOptions = $this->options;
        if ($bcOptions["enabled"] && $bcOptions['homeItem']['enabled']) {
            $this->breadcrumbs->addItem(
                $this->translator->trans($bcOptions['homeItem']['label'], [], $bcOptions["translation_domain"]),
                $this->router->generate(
                    $bcOptions['homeItem']['route'],
                    array_key_exists('routeParameters', $bcOptions['homeItem']) ? $bcOptions['homeItem']['routeParameters'] : []
                ),
                array_key_exists('attributes', $bcOptions['homeItem']) ? $bcOptions['homeItem']['attributes'] : []
            );
        }

        $request = $this->requestStack->getMainRequest();
        if (null !== $request && ($resource = $this->resourceBag->getResource())) {
            try {
                $response = $this->apiClient->apiRequest("contents/breadcrumbs/".$resource['id'].'?locale='.$request->getLocale());
                if ($response->getStatusCode() === 200) {
                    try {
                        $items = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
                        $loop = 1;
                        foreach ($items as $item) {
                            $this->breadcrumbs->addItem(
                                $item['label'],
                                $this->getUri($item['route'], $loop === count($items))
                            );
                            $loop++;
                        }
                    } catch (JsonException) {
                        $this->breadcrumbs->addItem(
                            "error"
                        );
                    }
                }
            } catch (\Exception $e) {
                $this->breadcrumbs->addItem(
                    "undefined breadcrumb",
                    ""
                );
            }
        }
    }

    /**
     * @param string $routeName
     * @param bool   $currentItem
     *
     * @return string
     */
    private function getUri(string $routeName, bool $currentItem = false): string
    {
        $request = $this->requestStack->getMainRequest();
        if (null !== $request && $currentItem) {
            return $request->getPathInfo();
        }
        return $this->router->generate($routeName);
    }

    /**
     * @param string $namespace
     *
     * @return array
     */
    public function getBreadcrumbs(string $namespace = Breadcrumbs::DEFAULT_NAMESPACE): array
    {
        return $this->breadcrumbs->getNamespaceBreadcrumbs($namespace);
    }

    /**
     * @param string $namespace
     *
     * @return bool
     */
    public function hasBreadcrumbs(string $namespace = Breadcrumbs::DEFAULT_NAMESPACE): bool
    {
        return $this->breadcrumbs->hasNamespaceBreadcrumbs($namespace);
    }

    /**
     * @param Environment $templating
     * @param array       $options
     *
     * @return string
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws LoaderError
     * @throws RedirectionExceptionInterface
     * @throws RuntimeError
     * @throws ServerExceptionInterface
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     */
    public function renderBreadcrumbs(Environment $templating, array $options = []): string
    {
        $options = $this->resolveOptions($options);
        $this->setupBreadcrumbs();

        // Assign namespace breadcrumbs
        $options["breadcrumbs"] = $this->breadcrumbs->getNamespaceBreadcrumbs($options['namespace']);
        if (array_key_exists('depth', $options)) {
            $options["breadcrumbs"] = array_slice($options["breadcrumbs"], 0, $options["depth"]);
        }

        if (array_key_exists('additionalItems', $options)) {
            foreach ($options['additionalItems'] as $item) {
                $attributes = array_key_exists('attributes', $item) ? $item['attributes'] : array();
                $translationParameters = array_key_exists('translationParameters', $item) ? $item['translationParameters'] : array();
                $options["breadcrumbs"][] = new SingleBreadcrumb($item['label'], $item['url'], $attributes, $translationParameters);
            }
        }

        return $templating->render(
            $options["viewTemplate"],
            $options
        );
    }

    /**
     * Checks if this breadcrumb is the last one in the collection
     *
     * @param SingleBreadcrumb $crumb
     * @param string           $namespace
     *
     * @return bool
     */
    public function isLastBreadcrumb(SingleBreadcrumb $crumb, string $namespace = Breadcrumbs::DEFAULT_NAMESPACE): bool
    {
        $offset = $this->breadcrumbs->count($namespace) - 1;

        return $crumb === $this->breadcrumbs->offsetGet($offset, $namespace);
    }

    /**
     * Merges user-supplied options from the view
     * with base config values
     *
     * @param array $options
     *
     * @return array
     */
    private function resolveOptions(array $options = array()): array
    {
        return array_merge($this->options, $options);
    }
}
