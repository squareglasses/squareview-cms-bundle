<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class FrontendEvents
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class FrontendEvents
{
    /**
     * The CONTACT_SUCCESS event occurs when the profile edit form is submitted successfully.
     *
     * This event allows you to update the contact before pesistance.
     *
     * @Event("SG\CmsBundle\Frontend\Event\ContactEvent")
     */
    public const CONTACT_SUCCESS = 'app.contact.submit.success';

    /**
     * The CONTACT_COMPLETED event occurs after saving the contact.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const CONTACT_COMPLETED = 'app.contact.submit.completed';
}
