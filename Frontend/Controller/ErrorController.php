<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use Exception;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Throwable;
use Twig\Environment;
use Twig\Error\RuntimeError;

/**
 * Class ErrorController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ErrorController extends AbstractController
{
    public const FALLBACK_ERROR_TEMPLATE = 'errors/error.html.twig';

    /**
     * @param Request          $request
     * @param Throwable        $exception
     * @param ApiClient        $apiClient
     * @param WebsiteBag       $websiteBag
     * @param ConfigurationBag $configurationBag
     * @param Environment      $twig
     *
     * @return Response
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function show(
        Request $request,
        Throwable $exception,
        ApiClient $apiClient,
        WebsiteBag $websiteBag,
        ConfigurationBag $configurationBag,
        Environment $twig
    ): Response {
        if (method_exists($exception, 'getStatusCode') && !in_array($exception->getStatusCode(), [500, 503], true)) {
            $template = "errors/error".$exception->getStatusCode().'.html.twig';
            try {
                $response = $apiClient->apiRequest(
                    '/' . $request->getLocale() . '/website/' . $configurationBag->getWebsiteIdentifier(),
                    'GET',
                    []
                );
                $websiteBag->setRawDatas($response->toArray());
            } catch (InvalidArgumentException|Exception) {
                $template = self::FALLBACK_ERROR_TEMPLATE;
            }
        } elseif (method_exists($exception, 'getStatusCode')) {
            $template = "errors/error".$exception->getStatusCode().'.html.twig';
        } elseif ($exception instanceof RuntimeError) {
            $template = "errors/error500.html.twig";
        } else {
            $template = self::FALLBACK_ERROR_TEMPLATE;
        }

        if ($template !== self::FALLBACK_ERROR_TEMPLATE && !$twig->getLoader()->exists($template)) {
            $template = self::FALLBACK_ERROR_TEMPLATE;
        }

        return $this->render($template, ['exception' => $exception]);
    }
}
