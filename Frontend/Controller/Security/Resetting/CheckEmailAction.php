<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Security\Resetting;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\Controller\CmsController;
use SG\CmsBundle\Frontend\Contracts\ResourceAwareControllerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CheckEmailAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CheckEmailAction extends CmsController
{
    /**
     * @param Request          $request
     * @param ConfigurationBag $configurationBag
     *
     * @return Response
     */
    public function __invoke(Request $request, ConfigurationBag $configurationBag): Response
    {
        $username = $request->query->get('username');

        if (empty($username)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->generateUrl('app_resetting_request'));
        }

        return $this->getResponse([
            'username' => $username,
            'tokenLifetime' => ceil($configurationBag->getSecurityResettingRetryTtl() / 3600)
        ]);
    }
}
