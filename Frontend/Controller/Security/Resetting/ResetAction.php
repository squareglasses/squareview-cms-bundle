<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Security\Resetting;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent;
use SG\CmsBundle\Frontend\Security\Form\Type\ResettingFormType;
use SG\CmsBundle\Common\Form\Util\FormErrorParser;
use SG\CmsBundle\Frontend\Controller\CmsController;
use SG\CmsBundle\Frontend\Exception\InvalidUserInputException;
use SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent;
use SG\CmsBundle\Frontend\Security\Event\FormEvent;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Security\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class ResetAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResetAction extends CmsController
{
    /**
     * @param Request                  $request
     * @param string                   $token
     * @param UserManager              $userManager
     * @param TokenGeneratorInterface  $tokenGenerator
     * @param EventDispatcherInterface $eventDispatcher
     * @param SessionInterface         $session
     * @param TranslatorInterface      $translator
     *
     * @return Response
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|HttpExceptionInterface
     */
    public function __invoke(
        Request $request,
        string $token,
        UserManager $userManager,
        TokenGeneratorInterface $tokenGenerator,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        TranslatorInterface $translator
    ): Response {
        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            $session->getFlashBag()->add('error', $translator->trans('error.unknown_user', [], 'form'));
            return $this->redirectToRoute("app_login");
        }

        $event = new GetResponseUserEvent($user, $request);
        $eventDispatcher->dispatch($event, UserEvents::RESETTING_RESET_INITIALIZE);
        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm(ResettingFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $eventDispatcher->dispatch($event, UserEvents::RESETTING_RESET_SUCCESS);

                try {
                    $user = $userManager->resetPassword($user);
                    if (null === $response = $event->getResponse()) {
                        $url = $this->generateUrl('app_profile');
                        $response = new RedirectResponse($url);
                    }

                    $eventDispatcher->dispatch(
                        new FilterUserResponseEvent($user, $request, $response),
                        UserEvents::RESETTING_RESET_COMPLETED
                    );

                    return $response;
                } catch (InvalidUserInputException $e) {
                    FormErrorParser::parseFormErrors($form, $e);
                    $this->addResponseParameter("errors", $this->getFormErrorsAsArray($form));
                }
            } else {
                $this->addResponseParameter("errors", $this->getFormErrorsAsArray($form));
            }
        }

        return $this->getResponse([
            'token' => $token,
            'form' => $form->createView()
        ]);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getFormErrorsAsArray(FormInterface $form): array
    {
        $errors = [];
        foreach ($form->getErrors(true) as $error) {
            if (null !== $error->getOrigin()) {
                $errors[(string)$error->getOrigin()->getPropertyPath()] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }
        return $errors;
    }
}
