<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Security\Resetting;

use DateTime;
use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Mailer\Mailer;
use SG\CmsBundle\Frontend\Controller\CmsController;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Security\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

/**
 * Class RequestAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RequestAction extends CmsController
{
    /**
     * @param Request                  $request
     * @param UserManager              $userManager
     * @param TokenGeneratorInterface  $tokenGenerator
     * @param Mailer                   $mailer
     * @param EventDispatcherInterface $eventDispatcher
     * @param SessionInterface         $session
     * @param ConfigurationBag         $configurationBag
     *
     * @return Response
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws TransportExceptionInterface
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface|HttpExceptionInterface
     */
    public function __invoke(
        Request $request,
        UserManager $userManager,
        TokenGeneratorInterface $tokenGenerator,
        Mailer $mailer,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        ConfigurationBag $configurationBag
    ): Response {
        if ($request->getMethod() === "POST") {
            $username = $request->request->get('username');

            try {
                $user = $userManager->findUser($username);
                if (null === $user) {
                    $session->getFlashBag()->add('error', 'error.unknown_user');
                    $session->set("invalid_username", $username);
                    return $this->redirectToRoute("app_resetting_request");
                }
            } catch (Exception) {
                $session->getFlashBag()->add('error', 'error.unknown_user');
                $session->set("invalid_username", $username);
                return $this->redirectToRoute("app_resetting_request");
            }


            $event = new GetResponseUserEvent($user, $request);
            $eventDispatcher->dispatch($event, UserEvents::RESETTING_SEND_EMAIL_INITIALIZE);
            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            $event = new GetResponseUserEvent($user, $request);
            $eventDispatcher->dispatch($event, UserEvents::RESETTING_RESET_REQUEST);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }
            $user->setPasswordRequestedAt(new DateTime());
            $userManager->updateUser($user, false, ['resetting-request']);

            $event = new GetResponseUserEvent($user, $request);
            $eventDispatcher->dispatch($event, UserEvents::RESETTING_SEND_EMAIL_CONFIRM);

            if (null !== $event->getResponse()) {
                return $event->getResponse();
            }

            $mailer->sendResettingEmailMessage($user);

            $event = new GetResponseUserEvent($user, $request);
            $eventDispatcher->dispatch($event, UserEvents::RESETTING_SEND_EMAIL_COMPLETED);

            return $event->getResponse() ?? new RedirectResponse($this->generateUrl('app_resetting_check_email', ['username' => $username]));
        }


        $this->addResponseParameter("invalid_username", $session->get("invalid_username"));
        $session->set("invalid_username", "");
        return $this->getResponse([]);
    }
}
