<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Security\Activation;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Mailer\Mailer;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Translation\Translator\TranslatorHelper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;

/**
 * Class RequestAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RequestAction extends AbstractController
{
    /**
     * @param Request          $request
     * @param string           $id
     * @param UserManager      $userManager
     * @param Mailer           $mailer
     * @param TranslatorHelper $translatorHelper
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws \Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface
     */
    public function __invoke(
        Request $request,
        string $id,
        UserManager $userManager,
        Mailer $mailer,
        TranslatorHelper $translatorHelper
    ): Response {
        $user = $userManager->findUser($id);
        if (null === $user) {
            throw new NotFoundHttpException("No user found for id '".$id."'.");
        }

        if (null === $user->getConfirmationToken()) {
            throw new NotFoundHttpException("User does not have a confirmation token set, can't be activated.");
        }

        $translatorHelper->loadResourceTranslations("app_activate_request_resource");

        $mailer->sendActivateEmailMessage($user);

        return new JsonResponse("ok");
    }
}
