<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Security\Registration;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Form\Util\FormErrorParser;
use SG\CmsBundle\Frontend\Controller\CmsController;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Exception\InvalidUserInputException;
use SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent;
use SG\CmsBundle\Frontend\Security\Event\FormEvent;
use SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent;
use App\Model\User;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Security\Util\TokenGeneratorInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class RegisterAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RegisterAction extends CmsController
{
    /**
     * @param Request                     $request
     * @param UserPasswordHasherInterface $passwordEncoder
     * @param UserManager                 $userManager
     * @param TokenGeneratorInterface     $tokenGenerator
     * @param EventDispatcherInterface    $eventDispatcher
     * @param SessionInterface            $session
     * @param ConfigurationBag            $configurationBag
     *
     * @return Response
     * @throws InvalidFormInputException
     * @throws InvalidUserInputException
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(
        Request $request,
        UserPasswordHasherInterface $passwordEncoder,
        UserManager $userManager,
        TokenGeneratorInterface $tokenGenerator,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        ConfigurationBag $configurationBag
    ): Response {
        $user = new User();

        $event = new GetResponseUserEvent($user, $request);
        $eventDispatcher->dispatch($event, UserEvents::REGISTRATION_INITIALIZE);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm($configurationBag->getRegistrationFormClass(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setUsername($user->getEmail());

            $event = new FormEvent($form, $request);
            $eventDispatcher->dispatch($event, UserEvents::REGISTRATION_SUCCESS);

            // encode the plain password
            $plainPassword = $form->get('plainPassword')->getData();
            $user->setPassword(
                $passwordEncoder->hashPassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            try {
                $user->setEnabled(true);
                if (null === $user->getConfirmationToken()) {
                    $user->setConfirmationToken($tokenGenerator->generateToken());
                }


                $user = $userManager->createUser($user);
                $user = $userManager->generateToken($user->getUsername(), $plainPassword, $user);
                $session->set('registration_token_'.$user->getId(), [
                    'token' => $user->getToken(),
                    'refreshToken' => $user->getRefreshToken()
                ]);

                $user->setEnabled(false);
                $user = $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl('app_registration_check_email');
                    $response = new RedirectResponse($url);
                }

                $eventDispatcher->dispatch(new FilterUserResponseEvent($user, $request, $response), UserEvents::REGISTRATION_COMPLETED);


                return $response;
            } catch (InvalidUserInputException|InvalidFormInputException $e) {
                FormErrorParser::parseFormErrors($form, $e);
            } catch (Exception $e) {
                throw $e;
            }
        }

        return $this->getResponse([
            'registrationForm' => $form->createView(),
        ]);
    }
}
