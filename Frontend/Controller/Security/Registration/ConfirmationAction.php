<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Security\Registration;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Controller\CmsController;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent;
use SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ConfirmationAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ConfirmationAction extends CmsController
{
    /**
     * @param Request                  $request
     * @param string                   $token
     * @param UserManager              $userManager
     * @param EventDispatcherInterface $eventDispatcher
     * @param SessionInterface         $session
     *
     * @return Response
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(
        Request $request,
        string $token,
        UserManager $userManager,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session
    ): Response {
        $user = $userManager->findUserByConfirmationToken($token);
        if (null === $user) {
            $session->getFlashBag()->add("error", "error.invalid_token");
            return $this->redirectToRoute("app_login");
        }

        $user->setEnabled(true);

        $event = new GetResponseUserEvent($user, $request);
        $eventDispatcher->dispatch($event, UserEvents::REGISTRATION_CONFIRMATION);

        $userManager->updateUser($user, false);

        if (null === $response = $event->getResponse()) {
            $url = $this->generateUrl('app_profile');
            $response = new RedirectResponse($url);
        }

        $event = new FilterUserResponseEvent($user, $request, $response);
        $eventDispatcher->dispatch($event, UserEvents::REGISTRATION_CONFIRMED);

        return $response;
    }
}
