<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use Exception;
use JsonException;
use RuntimeException;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Frontend\Routing\Router;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;
use Symfony\Contracts\Cache\TagAwareCacheInterface;

/**
 * Class CacheController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CacheController extends AbstractController
{
    /**
     * @param KernelInterface $kernel
     */
    public function __construct(private readonly KernelInterface $kernel)
    {
    }

    /**
     * @param Request                $request
     * @param TagAwareCacheInterface $apiCache
     * @param LoggerInterface        $sv4ErrorsLogger
     * @param LoggerInterface        $cacheTagsLogger
     *
     * @return Response
     * @throws JsonException
     */
    public function invalidateTags(Request $request, TagAwareCacheInterface $apiCache, LoggerInterface $sv4ErrorsLogger, LoggerInterface $cacheTagsLogger): Response
    {
        $body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        $tags = [];
        if (array_key_exists("tags", $body)) {
            foreach ($body['tags'] as $tag) {
                if ($tag !== null && $tag !== "") {
                    $tags[] = trim($tag);
                }
            }
        }

        $cacheTagsLogger->info('invalidateTags: tags='.implode(",", $tags));

        try {
            $apiCache->invalidateTags($tags);
            return new JsonResponse('cleared');
        } catch (Exception|InvalidArgumentException $e) {
            $sv4ErrorsLogger->critical('invalidateTags unsuccessful', ['tags' => $tags, 'error' => $e]);
            return new JsonResponse(array('not cleared'));
        }
    }

    /**
     * @param FilesystemOperator     $cacheStorage
     * @param TagAwareCacheInterface $apiCache
     * @param Router                 $router
     * @param LoggerInterface        $sv4ErrorsLogger
     *
     * @return Response
     * @throws FilesystemException
     */
    public function invalidateRouter(
        FilesystemOperator $cacheStorage,
        TagAwareCacheInterface $apiCache,
        Router $router,
        LoggerInterface $sv4ErrorsLogger
    ): Response {
        try {
            $apiCache->invalidateTags(['routes']);
            $router->warmUp($this->kernel->getCacheDir());
            if ($cacheStorage->has($this->kernel->getEnvironment()."/url_generating_routes.php")) {
                $cacheStorage->delete($this->kernel->getEnvironment()."/url_generating_routes.php");
            }
            if ($cacheStorage->has($this->kernel->getEnvironment()."/url_generating_routes.php.meta")) {
                $cacheStorage->delete($this->kernel->getEnvironment()."/url_generating_routes.php.meta");
            }
            if ($cacheStorage->has($this->kernel->getEnvironment()."/url_matching_routes.php")) {
                $cacheStorage->delete($this->kernel->getEnvironment()."/url_matching_routes.php");
            }
            if ($cacheStorage->has($this->kernel->getEnvironment()."/url_matching_routes.php.meta")) {
                $cacheStorage->delete($this->kernel->getEnvironment()."/url_matching_routes.php.meta");
            }
            $router->warmUp($this->kernel->getCacheDir());
        } catch (Exception|InvalidArgumentException $e) {
            $sv4ErrorsLogger->critical('invalidateRouter unsuccessful', ['error' => $e]);
            return new JsonResponse('not cleared');
        }
        return new JsonResponse('cleared');
    }

    /**
     * @param LoggerInterface $sv4ErrorsLogger
     *
     * @return Response
     * @throws Exception
     */
    public function clearCache(LoggerInterface $sv4ErrorsLogger): Response
    {
        $this->runCommand("cache:clear");

        return new JsonResponse('cleared');
    }

    /**
     * @throws Exception
     *
     * @param string $command
     */
    protected function runCommand(string $command): void
    {

        if (array_key_exists("PHP_BIN", $_ENV)) {
            $phpBin = $_ENV["PHP_BIN"];
        } else {
            $phpBin = "php";
        }
        $process = new Process(
            [$phpBin, "./bin/console",  $command]
        );

        $process->setWorkingDirectory($this->kernel->getProjectDir());
        $process->setTimeout(600);

        try {
            $process->mustRun();
        } catch (ProcessFailedException $e) {
            echo $process->getOutput();
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
