<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller\Profile;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use SG\CmsBundle\Common\Form\Util\FormErrorParser;
use SG\CmsBundle\Frontend\Controller\CmsController;
use SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent;
use SG\CmsBundle\Frontend\Security\Event\FormEvent;
use SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Security\Model\UserInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class EditAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class EditAction extends CmsController
{
    /**
     * @param Request                  $request
     * @param UserManager              $userManager
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(
        Request $request,
        UserManager $userManager,
        EventDispatcherInterface $eventDispatcher
    ): Response {
        $user = $this->getUser();
        if (!is_object($user) || !$user instanceof UserInterface) {
            throw new AccessDeniedException('This user does not have access to this section.');
        }

        $event = new GetResponseUserEvent($user, $request);
        $eventDispatcher->dispatch($event, UserEvents::PROFILE_EDIT_INITIALIZE);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $this->createForm($this->configurationBag->getProfileEditFormClass(), $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $event = new FormEvent($form, $request);
            $eventDispatcher->dispatch($event, UserEvents::PROFILE_EDIT_SUCCESS);

            try {
                $userManager->updateUser($user);

                if (null === $response = $event->getResponse()) {
                    $url = $this->generateUrl($this->configurationBag->getProfileRoute());
                    $response = new RedirectResponse($url);
                }
                $eventDispatcher->dispatch(new FilterUserResponseEvent($user, $request, $response), UserEvents::PROFILE_EDIT_COMPLETED);

                return $response;
            } catch (InvalidFormInputException $e) {
                FormErrorParser::parseFormErrors($form, $e);
            }
        }

        return $this->getResponse([
            'form' => $form->createView()
        ]);
    }
}
