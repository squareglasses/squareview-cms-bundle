<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Contracts\ResourceAwareControllerInterface;
use SG\CmsBundle\Frontend\Contracts\CmsControllerInterface;
use SG\CmsBundle\Frontend\Contracts\ResourceBagInterface;
use SG\CmsBundle\Common\Contracts\ResourceGuideInterface;
use SG\CmsBundle\Frontend\Contracts\WebsiteBagInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class FrontendController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FrontendController extends AbstractController
{
    /**
     * @param ApiClient           $apiClient
     * @param SerializerInterface $serializer
     */
    public function __construct(protected ApiClient $apiClient, protected SerializerInterface $serializer)
    {
    }
}
