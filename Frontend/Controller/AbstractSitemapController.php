<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use App\Model\Content;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Routing\Router;
use SG\CmsBundle\Frontend\Translation\Translator\Translator;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class AbstractSitemapController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AbstractSitemapController extends CmsController
{
    /**
     * @param ResourceBag           $cmsBag
     * @param WebsiteBag            $websiteBag
     * @param ConfigurationBag      $configurationBag
     * @param ApiClient             $apiClient
     * @param Router                $router
     * @param ResourceGuideProvider $guideProvider
     * @param Translator            $translator
     * @param RequestStack          $requestStack
     */
    public function __construct(
        ResourceBag $cmsBag,
        WebsiteBag $websiteBag,
        ConfigurationBag $configurationBag,
        ApiClient $apiClient,
        protected Router $router,
        protected ResourceGuideProvider $guideProvider,
        protected Translator $translator,
        protected RequestStack $requestStack
    ) {
        parent::__construct($cmsBag, $websiteBag, $configurationBag, $apiClient);
    }

    /**
     * @param array       $discriminators
     * @param string|int|null $parent
     * @param array       $serializationGroups
     *
     * @return array
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     */
    protected function getTree(array $discriminators, string|int|null $parent = null, array $serializationGroups = ['get']): array
    {
        $urls = [];

        $queryParameters = [
            "enabled" => true,
            "discriminator" => $discriminators,
            "order[position]" => "asc",
            "serialization_groups" => $serializationGroups,
            "locale" => $this->getLocale(),
            "parent" => $parent
        ];

        $pages = $this->apiClient->getCollection(
            "/sitemap?".http_build_query($queryParameters),
            [],
            Content::class,
            null,
            false,
            true,
            $discriminators
        );

        foreach ($pages as $page) {
            $guide = $this->guideProvider->getGuide($page->getGuide());
            if (null !== $guide && $guide->isSitemap()) {
                $urls[] = [
                    'href' => $this->router->generate($page->getSlug()),
                    'label' =>  $page->getName(),
                    'children' => $this->getCustomChildren(
                        $page,
                        $this->getTree($discriminators, $page->getSlug(), $serializationGroups),
                        $serializationGroups
                    )
                ];
            }
        }

        return $urls;
    }

    /**
     * @param object $page
     * @param array  $urls
     * @param array  $serializationGroups
     *
     * @return array
     */
    protected function getCustomChildren(object $page, array $urls, array $serializationGroups): array
    {
        return $urls;
    }

    /**
     * @return string
     */
    protected function getLocale(): string
    {
        $request = $this->requestStack->getMainRequest();
        if (null === $request) {
            return $this->configurationBag->getDefaultLocale();
        }
        return $request->getLocale();
    }
}
