<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class LanguageSwitchController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageSwitchController extends AbstractController
{
    private ?string $defaultLocale;

    /**
     * @param RouterInterface $router
     */
    public function __construct(private readonly RouterInterface $router, private readonly ConfigurationBag $configurationBag)
    {
        $this->defaultLocale = $configurationBag->getDefaultLocale();
    }

    /**
     * @param Request $request
     *
     * @return RedirectResponse
     */
    public function switchLanguage(Request $request): RedirectResponse
    {
        $targetLocale = $request->get('target');
        $referer = $request->getSession()->get('sg.referer');

        // Update locale in the session
        $request->getSession()->set('_locale', $targetLocale);
        if ($referer) {

            if ($referer->getRoute() === 'homepage') {
                $target = $this->getRouteToHomepage($targetLocale);
            } else {
                $route = $referer->getRouteBaseName() ?: $referer->getRoute();
                $parameters = array_merge($referer->getRouteParameters(), $referer->getQueryParameters());
                unset($parameters['_path']);
                if (array_key_exists('_locale', $parameters)) {
                    $parameters['_locale'] = $targetLocale;
                }
                $target = $this->router->generate($route, $parameters);
            }
            if (null !== $target) {
                return new RedirectResponse($target);
            }
        }

        return new RedirectResponse($this->getRouteToHomepage($targetLocale));
    }

    /**
     * @param string $locale
     *
     * @return string
     */
    private function getRouteToHomepage(string $locale): string
    {
        // Force locale in url when it's the default one to bypass browserLocaleGuesser on "/" url
        if ($locale === $this->defaultLocale) {
            return $this->router->generate('homepage', array('_locale' => $locale)).$locale;
        }
        return $this->router->generate('homepage', array('_locale' => $locale));
    }
}
