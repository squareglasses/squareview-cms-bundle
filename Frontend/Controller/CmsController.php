<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Bag\ResourceBag;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Contracts\ResourceAwareControllerInterface;
use SG\CmsBundle\Frontend\Contracts\CmsControllerInterface;
use SG\CmsBundle\Common\Contracts\ResourceGuideInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class CmsController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsController extends AbstractController implements CmsControllerInterface, ResourceAwareControllerInterface
{
    /** @var array<string,mixed> $responseParameters */
    protected array $responseParameters = array();

    /**
     * @param ResourceBag      $cmsBag
     * @param WebsiteBag       $websiteBag
     * @param ConfigurationBag $configurationBag
     * @param ApiClient        $apiClient
     */
    public function __construct(
        protected ResourceBag $cmsBag,
        protected WebsiteBag $websiteBag,
        protected ConfigurationBag $configurationBag,
        protected ApiClient $apiClient
    ) {
    }

    /**
     * Default cms guide action.
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->getResponse();
    }

    /**
     * @return ResourceGuideInterface
     */
    public function getGuide(): ResourceGuideInterface
    {
        return $this->cmsBag->getGuide();
    }

    /**
     * @param string $parameterName
     * @param mixed  $value
     *
     * @return CmsControllerInterface
     */
    public function addResponseParameter(string $parameterName, mixed $value): CmsControllerInterface
    {
        $this->responseParameters[$parameterName] = $value;

        return $this;
    }

    /**
     * @return array<string,mixed>
     */
    public function getResponseParameters(): array
    {
        return array_merge($this->getDefaultResponseParameters(), $this->responseParameters);
    }

    /**
     * @return array<string,mixed>
     */
    #[ArrayShape(['resource' => "\SG\CmsBundle\Frontend\Bag\ResourceBag", 'website' => "\SG\CmsBundle\Frontend\Bag\WebsiteBag"])]
    public function getDefaultResponseParameters(): array
    {
        return [
            'resource'  => $this->cmsBag,
            'website'   => $this->websiteBag
        ];
    }

    /**
     * @param array       $extraParameters
     * @param string|null $view
     *
     * @return Response
     */
    public function getResponse(array $extraParameters = array(), ?string $view = null): Response
    {
        return $this->render($view ?? $this->getGuide()->getView(), array_merge($this->getResponseParameters(), $extraParameters));
    }

    /**
     * @return ResourceBag
     */
    public function getResource(): ResourceBag
    {
        return $this->cmsBag;
    }
}
