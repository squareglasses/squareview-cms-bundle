<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);
// @TODO: To Delete

namespace SG\CmsBundle\Frontend\Controller;

use Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class ExposeConfigurationController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ExposeConfigurationController extends AbstractController
{
    /**
     * @return Response
     */
    public function getConfiguration(): Response
    {
        try {
            return new JsonResponse($this->getParameter("squareview"));
        } catch (Exception) {
            return new JsonResponse(array());
        }
    }
}
