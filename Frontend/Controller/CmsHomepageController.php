<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Controller;

use SG\CmsBundle\Common\HttpClient\Response\ResponseItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * Class CmsHomepageController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsHomepageController extends CmsController
{
}
