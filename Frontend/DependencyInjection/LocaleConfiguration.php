<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class RefererConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LocaleConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('locale_matcher')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('guessing_order')
                            ->beforeNormalization()
                                ->ifString()
                                    ->then(function ($v) {
                                        return array($v);
                                    })
                            ->end()
                            ->isRequired()
                            ->requiresAtLeastOneElement()
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
