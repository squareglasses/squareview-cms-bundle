<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class PaginationConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PaginationConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('pagination')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode("template")
                            ->cannotBeEmpty()
                            ->defaultValue("partials/pagination.html.twig")
                        ->end()
                        ->integerNode("page_range")
                            ->defaultValue(5)
                        ->end()
                        ->scalarNode("page_parameter_name")
                            ->cannotBeEmpty()
                            ->defaultValue("page")
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
