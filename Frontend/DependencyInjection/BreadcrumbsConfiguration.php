<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class BreadcrumbsConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class BreadcrumbsConfiguration
{
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
            ->arrayNode('breadcrumbs')
                ->addDefaultsIfNotSet()
                ->canBeDisabled()
                ->children()
                    ->scalarNode("separator")->defaultValue("/")->end()
                    ->scalarNode("separatorClass")->defaultValue("separator")->end()
                    ->scalarNode("listId")->defaultValue("sg-breadcrumbs")->end()
                    ->scalarNode("listClass")->defaultValue("breadcrumbs")->end()
                    ->scalarNode("itemClass")->defaultValue("")->end()
                    ->scalarNode("linkRel")->defaultValue("")->end()
                    ->arrayNode("homeItem")
                        ->canBeDisabled()
                        ->children()
                            ->scalarNode("label")->defaultValue("breadcrumbs.home")->end()
                            ->scalarNode("route")->defaultValue("homepage")->end()
                            ->arrayNode("routeParameters")->children()->end()->end()
                            ->variableNode("attributes")->end()
                            ->scalarNode("id")->defaultValue("")->end()
                            ->scalarNode("class")->defaultValue("")->end()
                        ->end()
                    ->end()
                    ->scalarNode("translation_domain")->defaultValue("header")->end()
                    ->scalarNode("viewTemplate")->defaultValue("partials/_breadcrumbs.html.twig")->end()
                ->end()
            ->end()
            ->end()
        ;
    }
}
