<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class SecurityConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SecurityConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('security')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('user_class')->defaultValue("SG\CmsBundle\Frontend\Security\Model\User")->end()

                        ->arrayNode('mailer')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('from')->defaultNull()->end()
                            ->end()
                        ->end()
                        ->arrayNode('resetting')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->integerNode('retryTtl')->defaultValue(3600)->end()
                            ->end()
                        ->end()
                        ->arrayNode('registration')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('form')->defaultValue("SG\\CmsBundle\\Frontend\\Security\\Form\\Type\\RegistrationFormType")->end()
                                ->booleanNode('use_username')->defaultTrue()->end()
                            ->end()
                        ->end()
                        ->arrayNode('profile')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('route')->defaultValue("app_profile")->end()
                                ->arrayNode('edit')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('route')->defaultValue("app_profile_edit")->end()
                                        ->scalarNode('form')->defaultValue("SG\\CmsBundle\\Frontend\\Security\\Form\\Type\\UserProfileEditFormType")->end()
                                        ->scalarNode('personal_infos_form')->defaultValue("SG\\CmsBundle\\Frontend\\Security\\Form\\Type\\UserPersonalInfosFormType")->end()
                                    ->end()
                                ->end()
                                ->arrayNode('change_password')
                                    ->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('route')->defaultValue("app_profile_change_password")->end()
                                        ->scalarNode('form')->defaultValue("SG\\CmsBundle\\Frontend\\Security\\Form\\Type\\ChangePasswordFormType")->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
