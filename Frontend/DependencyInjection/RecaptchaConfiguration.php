<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class RecaptchaConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RecaptchaConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('recaptcha')
                    ->addDefaultsIfNotSet()
                    ->canBeEnabled()
                    ->children()
                        ->scalarNode('public_key')->cannotBeEmpty()->defaultValue("")->end()
                        ->scalarNode('private_key')->cannotBeEmpty()->defaultValue("")->end()
                        ->booleanNode('verify_host')->defaultTrue()->end()
                        ->floatNode("score_threshold")->defaultValue(0.5)
                    ->end()
                ->end()
            ->end()
        ;
    }
}
