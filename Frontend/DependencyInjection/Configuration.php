<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class Configuration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Configuration
{
    /**
     * @return mixed
     */
    public function getConfig(): mixed
    {
        $treeBuilder = new TreeBuilder('frontend');
        $rootNode = $treeBuilder->getRootNode()->addDefaultsIfNotSet();

        $securityConfiguration = new SecurityConfiguration();
        $securityConfiguration->appendTo($rootNode);

        $refererConfiguration = new RefererConfiguration();
        $refererConfiguration->appendTo($rootNode);

        $contactConfiguration = new ContactConfiguration();
        $contactConfiguration->appendTo($rootNode);

        $paginationConfiguration = new PaginationConfiguration();
        $paginationConfiguration->appendTo($rootNode);

        $breadcrumbsConfiguration = new BreadcrumbsConfiguration();
        $breadcrumbsConfiguration->appendTo($rootNode);

        $recaptchaConfiguration = new RecaptchaConfiguration();
        $recaptchaConfiguration->appendTo($rootNode);

        $localeConfiguration = new LocaleConfiguration();
        $localeConfiguration->appendTo($rootNode);

        return $rootNode;
    }
}
