<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection\Compiler;

use SG\CmsBundle\Frontend\Provider\ModuleGuideProvider;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class ModuleGuideProviderCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleGuideProviderCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(ModuleGuideProvider::class)) {
            return;
        }
        $guideProviderDefinition = $container->getDefinition(ModuleGuideProvider::class);
        $guidesConfiguration = $container->getParameter('squareview.modules.guides');
        if ($guidesConfiguration) {
            // Adding available guides from current configuration
            $guideProviderDefinition->addMethodCall('setAvailableGuides', [$guidesConfiguration]);

            // Adding module renderer services to the provider
            foreach ($guidesConfiguration as $guideConfiguration) {
                if (($guideConfiguration['renderer'] !== null) && $container->hasDefinition($guideConfiguration['renderer'])) {
                    $guideProviderDefinition->addMethodCall('addRenderer', [$guideConfiguration['renderer'], $container->getDefinition($guideConfiguration['renderer'])]);
                }
            }
        }
    }
}
