<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection\Compiler;

use SG\CmsBundle\Frontend\Routing\Router;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Alias;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;

/**
 * Class OverrideRoutingCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class OverrideRoutingCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
        if (!$container->hasDefinition(Router::class)) {
            return;
        }

        if ($container->hasAlias('router')) {
            // router is an alias.
            // Register a private alias for this service to inject it as the parent
            $container->setAlias('sg_cms.router.parent', new Alias((string) $container->getAlias('router'), false));
        } elseif ($container->hasDefinition('router')) {
            // router is a definition.
            // Register it again as a private service to inject it as the parent
            $definition = $container->getDefinition('router');
            $definition->setPublic(false);
            $container->setDefinition('sg_cms.router.parent', $definition);
        } else {
            throw new ServiceNotFoundException('router', Router::class);
        }

        $container->setAlias('router', Router::class);
    }
}
