<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection\Compiler;

use SG\CmsBundle\Frontend\Locale\Guesser\LocaleGuesserManager;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Compilerpass Class
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class LocaleGuesserCompilerPass implements CompilerPassInterface
{
    /**
     * Compilerpass for Locale Guessers
     *
     * @param ContainerBuilder $container
     */
    public function process(ContainerBuilder $container)
    {
        if (false === $container->hasDefinition(LocaleGuesserManager::class)) {
            return;
        }

        $definition = $container->getDefinition(LocaleGuesserManager::class);
        $taggedServiceIds = $container->findTaggedServiceIds('locale.guesser');
        $neededServices = $container->getParameter('squareview.frontend.locale_matcher.guessing_order');

        foreach ($taggedServiceIds as $id => $tagAttributes) {
            foreach ($tagAttributes as $attributes) {
                if (in_array($attributes['alias'], $neededServices)) {
                    $definition->addMethodCall('addGuesser', array(new Reference($id), $attributes["alias"]));
                }
            }
        }
    }
}
