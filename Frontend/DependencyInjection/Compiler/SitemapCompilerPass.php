<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class SitemapCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SitemapCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
        foreach ($container->findTaggedServiceIds('sitemap.generator') as $generatorId => $generatorAttributes) {
            $definition = $container->findDefinition($generatorId);

            foreach ($container->findTaggedServiceIds('sitemap.extractor') as $id => $attributes) {
                $extractor = $container->getDefinition($id);
                $definition->addMethodCall('addExtractor', array($extractor));
            }
        }
    }
}
