<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class ContactConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ContactConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('contact')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('from_address')->defaultValue("Square Glasses <contact@squareglasses.com>")->cannotBeEmpty()->end()
                        // Array configuration with env variable is not possible for now
                        ->variableNode("to_addresses")->end()
                        ->variableNode("cc_addresses")->end()
                        ->variableNode("bcc_addresses")->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
