<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class RefererConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RefererConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('referer')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->arrayNode('excluded_routes')
                            ->prototype('scalar')->end()
                            ->defaultValue(['switch_language', '_wdt', '_profiler_home', '_profiler_search',
                                '_profiler_search_bar', '_profiler_phpinfo', '_profiler_search_results', '_profiler_open_file',
                                '_profiler', '_profiler_router', '_profiler_exception', '_profiler_exception_css',
                                'api_entrypoint', '_errors', 'app_login', 'app_login.fr', 'app_login.en'])
                        ->end()

                        ->arrayNode('excluded_patterns')
                            ->prototype('scalar')->end()
                            ->defaultValue(['/api/', '/js/', '/.jpg/', '/.jpeg/', '/.png/', '/.bmp/', '/.mp4/', '/.mov/', '/.gif/'])
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
