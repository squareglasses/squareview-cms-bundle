<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator;

/**
 * Interface RouteGeneratorInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface RouteGeneratorInterface
{
    /**
     * @param string $name
     * @param string $path
     * @param string|null $locale
     * @param string|null $controller
     * @param array<string> $methods
     * @param array<string,mixed> $defaults
     * @param array<string,mixed> $requirements
     * @param array<string,mixed> $options
     * @param string|null $host
     * @param array<string> $schemes
     * @param string|null $condition
     * @return mixed
     */
    public function generateRoute(
        string $name,
        string $path,
        string $controller = null,
        string $locale = null,
        array $methods = [],
        array $defaults = [],
        array $requirements = [],
        array $options = [],
        ?string $host = '',
        array $schemes = [],
        ?string $condition = ''
    ): mixed;
}
