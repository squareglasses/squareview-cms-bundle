<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator;

use SG\CmsBundle\Frontend\Routing\Exception\MissingLocaleException;
use Symfony\Component\Routing\Route;

/**
 * Class HomepageRouteGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HomepageRouteGenerator
{
    private bool $multilingual = false;
    private ?string $defaultLocale = null;

    /**
     * @return Route|null
     * @throws MissingLocaleException
     */
    public function generateRoute(): ?Route
    {
        $defaults = [
            '_controller' => 'App\\Controller\HomepageController::default'
        ];

        $methods = ['GET', 'POST'];
        $requirements = [];

        if ($this->multilingual === true) {
            if ($this->defaultLocale === null) {
                throw new MissingLocaleException("Cannot generate I18n homepage route if no locale is given.");
            }
            $path = '/{_locale}';
            $defaults['_locale'] = $this->defaultLocale;
        } else {
            $path = '/';
        }
        return new Route($path, $defaults, $requirements, [], null, [], $methods);
    }

    /**
     * @param bool $multilingual
     *
     * @return void
     */
    public function setMultilingual(bool $multilingual): void
    {
        $this->multilingual = $multilingual;
    }

    /**
     * @return bool
     */
    public function getMultilingual(): bool
    {
        return $this->multilingual;
    }

    /**
     * @param string $locale
     *
     * @return void
     */
    public function setDefaultLocale(string $locale): void
    {
        $this->defaultLocale = $locale;
    }

    /**
     * @return string
     */
    public function getDefaultLocale(): string
    {
        return $this->defaultLocale;
    }
}
