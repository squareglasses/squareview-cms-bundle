<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator;

use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\Routing\Route;

/**
 * Class DefaultRouteGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DefaultRouteGenerator implements RouteGeneratorInterface
{
    /**
     * @param string      $name
     * @param string      $path
     * @param string|null $controller
     * @param string|null $locale
     * @param array       $methods
     * @param array       $defaults
     * @param array       $requirements
     * @param array       $options
     * @param string|null $host
     * @param array       $schemes
     * @param string|null $condition
     *
     * @return array
     */
    #[ArrayShape(['name' => "string", 'route' => "\Symfony\Component\Routing\Route"])]
    public function generateRoute(
        string $name,
        string $path,
        string $controller = null,
        string $locale = null,
        array $methods = [],
        array $defaults = [],
        array $requirements = [],
        array $options = [],
        ?string $host = '',
        array $schemes = [],
        ?string $condition = ''
    ): array {
        $defaults = array_merge($defaults, [
            '_controller' => $controller ?: 'SG\\CmsBundle\Frontend\Controller\CmsController::default',
            '_path' => $path,
            '_route_base_name' => $name
        ]);
        $methods = empty($methods) ? ['GET', 'POST'] : $methods;
        $requirements = empty($requirements) ? ['path' => '.+'] : $requirements;

        return [
            'name' => $name,
            'route' => new Route($path, $defaults, $requirements, $options, $host, $schemes, $methods, $condition)
        ];
    }
}
