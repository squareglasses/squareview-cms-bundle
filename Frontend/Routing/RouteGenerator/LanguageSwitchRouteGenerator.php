<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator;

use Symfony\Component\Routing\Route;

/**
 * Class LanguageSwitchRouteGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageSwitchRouteGenerator
{
    private bool $multilingual = false;

    /**
     * @return Route|null
     */
    public function generateRoute(): ?Route
    {
        if (!$this->multilingual) {
            return null;
        }
        $defaults = ['_controller' => 'SG\\CmsBundle\Frontend\Controller\LanguageSwitchController::switchLanguage'];
        $path = '/switch-language/{target}';
        $methods = ['GET'];
        $requirements = [];

        return new Route($path, $defaults, $requirements, [], null, [], $methods);
    }

    /**
     * @param bool $multilingual
     *
     * @return void
     */
    public function setMultilingual(bool $multilingual): void
    {
        $this->multilingual = $multilingual;
    }

    /**
     * @return bool
     */
    public function getMultilingual(): bool
    {
        return $this->multilingual;
    }
}
