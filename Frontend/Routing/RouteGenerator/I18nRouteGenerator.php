<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator;

use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Frontend\Routing\Exception\MissingLocaleException;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector\RouteNameInflectorInterface;
use Symfony\Component\Routing\Route;

/**
 * Class I18nRouteGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class I18nRouteGenerator implements RouteGeneratorInterface
{
    /**
     * @param RouteNameInflectorInterface $routeNameInflector
     */
    public function __construct(private readonly RouteNameInflectorInterface $routeNameInflector)
    {
    }

    /**
     * @param string      $name
     * @param string      $path
     * @param string|null $controller
     * @param string|null $locale
     * @param array       $methods
     * @param array       $defaults
     * @param array       $requirements
     * @param array       $options
     * @param string|null $host
     * @param array       $schemes
     * @param string|null $condition
     *
     * @return array
     * @throws MissingLocaleException
     */
    #[ArrayShape(['name' => "string", 'route' => "\Symfony\Component\Routing\Route"])] public function generateRoute(
        string $name,
        string $path,
        string $controller = null,
        string $locale = null,
        array $methods = [],
        array $defaults = [],
        array $requirements = [],
        array $options = [],
        ?string $host = '',
        array $schemes = [],
        ?string $condition = ''
    ): array {
        if (null === $locale) {
            throw new MissingLocaleException("Cannot generate I18n route if no locale is given.");
        }
        $localizedPath = '/{_locale}/'.$path;
        $defaults = array_merge($defaults, [
            '_controller' => $controller ?: 'SG\CmsBundle\Frontend\Controller\CmsController::default',
            '_path' => $path,
            '_route_base_name' => $name
        ]);
        $methods = empty($methods) ? ['GET', 'POST'] : $methods;
        $requirements = empty($requirements) ? ['path' => '.+'] : $requirements;
        $options = array_merge($options, [
            'expose' => true
        ]);

        return [
            'name' => $this->routeNameInflector->inflect($name, $locale),
            'route' => new Route($localizedPath, $defaults, $requirements, $options, $host, $schemes, $methods, $condition)
        ];
    }
}
