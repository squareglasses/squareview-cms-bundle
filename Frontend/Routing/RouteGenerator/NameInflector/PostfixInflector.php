<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector;

/**
 * A route name inflector that appends the locale to the routes name.
 * Class PostfixInflector
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PostfixInflector implements RouteNameInflectorInterface
{
    /**
     * @param string $name
     * @param string $locale
     *
     * @return string
     */
    public function inflect(string $name, string $locale): string
    {
        return $name.'.'.$locale;
    }
}
