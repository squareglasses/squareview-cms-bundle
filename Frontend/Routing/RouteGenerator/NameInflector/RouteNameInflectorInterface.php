<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector;

/**
 * Deduce the route name to use for a localized route.
 * Interface RouteNameInflectorInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface RouteNameInflectorInterface
{
    /**
     * Return the route name and return it.
     *
     * @param string $name
     * @param string $locale
     *
     * @return string
     */
    public function inflect(string $name, string $locale): string;
}
