<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing;

use SG\CmsBundle\Frontend\Routing\RouteGenerator\NameInflector\RouteNameInflectorInterface;
use Symfony\Component\HttpKernel\CacheWarmer\WarmableInterface;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Routing\Exception\RouteNotFoundException;
use Symfony\Component\Routing\RequestContext;

/**
 * Class Router
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Router implements RouterInterface, WarmableInterface
{
    /**
     * @param RouterInterface             $router
     * @param RouteNameInflectorInterface $routeNameInflector
     * @param bool                        $multilingual
     * @param string|null                 $defaultLocale
     */
    public function __construct(
        protected RouterInterface                    $router,
        private readonly RouteNameInflectorInterface $routeNameInflector,
        protected bool                               $multilingual = true,
        protected ?string                            $defaultLocale = null
    ) {
    }

    /**
     * Generates a URL from the given parameters.
     *
     * @param string $name
     * @param array  $parameters
     * @param int    $referenceType
     *
     * @return string
     */
    public function generate(string $name, array $parameters = array(), int $referenceType = self::ABSOLUTE_PATH): string
    {
        try {
            if ($name === 'homepage' && $this->multilingual) {
                $parameters['_locale'] = $this->getLocale($parameters);
            }
            return $this->router->generate($name, $parameters, $referenceType);
        } catch (RouteNotFoundException $e) {
            $locale = $this->getLocale($parameters);

            if (null !== $locale) {
                return $this->generateI18n($name, $locale, $parameters, $referenceType);
            }
            throw $e;
        }
    }

    /**
     * Generates a I18N URL from the given parameter
     *
     * @param string $name
     * @param string $locale
     * @param array  $parameters
     * @param int    $referenceType
     *
     * @return string
     */
    protected function generateI18n(string $name, string $locale, array $parameters, int $referenceType = self::ABSOLUTE_PATH): string
    {
        try {
            return $this->router->generate(
                $this->routeNameInflector->inflect($name, $locale),
                $parameters,
                $referenceType
            );
        } catch (RouteNotFoundException) {
            // @TODO Vérifier le bon comportement à adopter ici, si la route existe dans une langue et
            // pas dans une autre ça genere une exception potentiellement
            return "";
        }
    }

    /**
     * @param string $pathinfo
     *
     * @return array
     */
    public function match(string $pathinfo): array
    {
        return $this->router->match($pathinfo);
    }

    /**
     * @return RouteCollection
     */
    public function getRouteCollection(): RouteCollection
    {
        return $this->router->getRouteCollection();
    }

    /**
     * @param RequestContext $context
     *
     * @return void
     */
    public function setContext(RequestContext $context): void
    {
        $this->router->setContext($context);
    }

    /**
     * @return RequestContext
     */
    public function getContext(): RequestContext
    {
        return $this->router->getContext();
    }

    /**
     * Determine the locale to be used with this request
     *
     * @param array $parameters
     *
     * @return string|null
     */
    protected function getLocale(array $parameters): ?string
    {
        if (isset($parameters['_locale'])) {
            return $parameters['_locale'];
        }

        if ($this->getContext()->hasParameter('_locale')) {
            return $this->getContext()->getParameter('_locale');
        }

        return $this->defaultLocale;
    }

    /**
     * Overwrite the locale to be used by default if the current locale could
     * not be found when building the route
     *
     * @param string $locale
     *
     * @return void
     */
    public function setDefaultLocale(string $locale): void
    {
        $this->defaultLocale = $locale;
    }

    /**
     * @param string $cacheDir
     *
     * @return void
     */
    public function warmUp(string $cacheDir): void
    {
        /** @var WarmableInterface */
        $router = $this->router;
        $router->warmUp($cacheDir);
    }
}
