<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Routing;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;
use SG\CmsBundle\Common\HttpClient\Response\ResponseParser;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\HomepageRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\LanguageSwitchRouteGenerator;
use SG\CmsBundle\Frontend\Routing\RouteGenerator\RouteGeneratorInterface;
use Symfony\Component\Config\Loader\Loader;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use SG\CmsBundle\Frontend\Routing\Exception\MissingLocaleException;

/**
 * Class CmsLoader
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsLoader extends Loader
{
    private bool $isLoaded = false;

    /**
     * @param ApiClient                    $apiClient
     * @param RouteGeneratorInterface      $routeGenerator
     * @param HomepageRouteGenerator       $homepageRouteGenerator
     * @param LanguageSwitchRouteGenerator $languageSwitchRouteGenerator
     * @param ResourceGuideProvider        $guideProvider
     */
    public function __construct(
        private readonly ApiClient                    $apiClient,
        private readonly RouteGeneratorInterface      $routeGenerator,
        private readonly HomepageRouteGenerator       $homepageRouteGenerator,
        private readonly LanguageSwitchRouteGenerator $languageSwitchRouteGenerator,
        private readonly ResourceGuideProvider        $guideProvider
    ) {
        parent::__construct();
    }

    /**
     * @param             $resource
     * @param string|null $type
     *
     * @return RouteCollection
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws HttpExceptionInterface|MissingLocaleException
     * @throws Exception
     */
    public function load($resource, string $type = null): RouteCollection
    {
        if (true === $this->isLoaded) {
            throw new RuntimeException('Do not add the "extra" loader twice');
        }

        $routes = new RouteCollection();

        // Adding homepage route, MUST be included before other dynamic routes
        $routes->add('homepage', $this->homepageRouteGenerator->generateRoute());

        // adding language switcher route if necessary
        $route = $this->languageSwitchRouteGenerator->generateRoute();
        if ($route !== null) {
            $routes->add("switch_language", $route);
        }

        // Adding dynamic routes
        try {
            $response = $this->apiClient->apiRequest('routes', 'GET', [], true, ['routes']);
            $responseParser = new ResponseParser();

            $existingRoutes = $responseParser->parse($response);
        } catch (Exception) {
            $existingRoutes = [];
        }

        foreach ($existingRoutes as $routeConfig) {
            if ($routeConfig->get('path') !== 'homepage') {
                $controller = null;
                if ($routeConfig->get('guide')) {
                    $guide = $this->guideProvider->getGuide($routeConfig->get('guide'));
                    if ($guide) {
                        if (null !== $guide->getAction()) {
                            $controller = $guide->getController().'::'.$guide->getAction();
                        } else { // ADR Pattern controller
                            $controller = $guide->getController();
                        }
                    }
                }

                $options = [];
                if ($routeConfig->has('options')) {
                    $options = $routeConfig->get('options');
                }

                $route = $this->routeGenerator->generateRoute(
                    $routeConfig->get('name'),
                    $routeConfig->get('path'),
                    $controller,
                    $routeConfig->get('locale'),
                    [],
                    [],
                    [],
                    $options
                );
                $routes->add($route['name'], $route['route']);
            }
        }
        $this->isLoaded = true;

        return $routes;
    }

    /**
     * @param mixed       $resource
     * @param string|null $type
     *
     * @return bool
     */
    public function supports(mixed $resource, string $type = null): bool
    {
        return 'cms' === $type;
    }
}
