<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Class WebsiteNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class WebsiteNotFoundException extends HttpException
{
    /**
     * @param string         $identifier
     * @param Throwable|null $previous
     * @param int            $code
     * @param array          $headers
     */
    public function __construct(string $identifier, Throwable $previous = null, int $code = 0, array $headers = [])
    {
        parent::__construct(404, "No website found for identifier '".$identifier."'.
        Please verify your 'sg_cms' configuration (in config/packages/sg_cms.yaml).", $previous, $headers, $code);
    }
}
