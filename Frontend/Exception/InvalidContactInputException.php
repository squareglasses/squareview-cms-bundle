<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Exception;

use Exception;
use SG\CmsBundle\Common\Exception\FormErrorExceptionInterface;
use Throwable;

/**
 * Class InvalidContactInputException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class InvalidContactInputException extends Exception implements FormErrorExceptionInterface
{
    /**
     * @param string         $message
     * @param array          $violations
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message, private readonly array $violations = [], int $code = 0, Throwable $previous = null)
    {
        $this->message = $message;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return array
     */
    public function getViolations(): array
    {
        return $this->violations;
    }
}
