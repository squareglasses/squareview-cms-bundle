<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Exception;

use Exception;

/**
 * Class ModuleGuideNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleGuideNotFoundException extends Exception
{
    /**
     * @param string $guideName
     */
    public function __construct(string $guideName)
    {
        $message = "No module guide found for identifier '".$guideName."'.";
        parent::__construct($message);
    }
}
