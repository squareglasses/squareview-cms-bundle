<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Exception;

use Exception;
use SG\CmsBundle\Frontend\Contracts\ModuleGuideInterface;

/**
 * Class ModuleGuideConfigurationException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleGuideConfigurationException extends Exception
{
    /**
     * @param ModuleGuideInterface $guide
     */
    public function __construct(ModuleGuideInterface $guide)
    {
        $message = "ResourceGuide '".$guide->getName()."' is misconfigured: you must specify either 'pattern' or 'view' parameter.";
        parent::__construct($message);
    }
}
