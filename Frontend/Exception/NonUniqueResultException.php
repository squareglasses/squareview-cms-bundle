<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Exception;

use Exception;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class NonUniqueResultException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class NonUniqueResultException extends Exception
{
    /**
     * NonUniqueResultException constructor.
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $info = $response->getInfo();
        $message = "More than one result was found for url '".$info['url']."' although one row or none was expected.";
        parent::__construct($message);
    }
}
