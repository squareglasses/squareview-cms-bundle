<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Provider;

use JsonException;
use RuntimeException;
use Symfony\Component\Serializer\SerializerInterface;
use SG\CmsBundle\Frontend\Exception\ModuleGuideConfigurationException;
use SG\CmsBundle\Frontend\Exception\ModuleGuideNotFoundException;
use SG\CmsBundle\Frontend\Model\ModuleGuide;
use SG\CmsBundle\Frontend\Contracts\ModuleRendererInterface;
use SG\CmsBundle\Frontend\Contracts\ModuleGuideInterface;

/**
 * Class ModuleGuideProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleGuideProvider
{
    /** @var array<string,array> $availableGuides */
    private array $availableGuides = [];

    /** @var array<ModuleRendererInterface> $renderers */
    private array $renderers = [];

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param string $guideName
     *
     * @return ModuleGuideInterface
     * @throws ModuleGuideConfigurationException
     * @throws ModuleGuideNotFoundException
     * @throws JsonException
     */
    public function getGuide(string $guideName): ModuleGuideInterface
    {
        if (!$this->availableGuides) {
            throw new RuntimeException('No modules configured');
        }
        if (!array_key_exists($guideName, $this->availableGuides)) {
            throw new ModuleGuideNotFoundException($guideName);
        }

        $guideData = array_merge($this->availableGuides[$guideName], array('name' => $guideName));

        /** @var ModuleGuideInterface $guide */
        $guide = $this->serializer->deserialize(json_encode($guideData, JSON_THROW_ON_ERROR), ModuleGuide::class, 'json');

        if (!$guide->getPattern() && !$guide->getView()) {
            throw new ModuleGuideConfigurationException($guide);
        }

        return $guide;
    }

    /**
     * @param ModuleGuideInterface $guide
     *
     * @return ModuleRendererInterface
     */
    public function getRenderer(ModuleGuideInterface $guide): ModuleRendererInterface
    {
        return $this->renderers[$guide->getRenderer()];
    }

    /**
     * Called by compiler pass
     *
     * @param string                  $class
     * @param ModuleRendererInterface $renderer
     *
     * @return void
     */
    public function addRenderer(string $class, ModuleRendererInterface $renderer): void
    {
        $this->renderers[$class] = $renderer;
    }

    /**
     * Called by compiler pass
     *
     * @param array $availableGuides
     *
     * @return void
     */
    public function setAvailableGuides(array $availableGuides): void
    {
        $this->availableGuides = $availableGuides;
    }
}
