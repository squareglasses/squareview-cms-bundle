<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Command;

use SG\CmsBundle\Frontend\Sitemap\Generator\XmlGenerator;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;

/**
 * Class SitemapGeneratorCommand
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SitemapGeneratorCommand extends Command
{
    protected ?SymfonyStyle $io = null;
    private int $timer;

    /**
     * @param XmlGenerator $xmlGenerator
     * @param string       $webDir
     */
    public function __construct(private readonly XmlGenerator $xmlGenerator, private readonly string $webDir)
    {
        parent::__construct('frontend:sitemap:generate');
    }

    /**
     * @return void
     */
    protected function configure(): void
    {
        $this
            ->setDescription('Generate a sitemap from published resources')
            ->setHelp('Generate a sitemap from published resources')
        ;
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io           = new SymfonyStyle($input, $output);
        $fs                 = new Filesystem();

        // BEGIN OF COMMAND EXECUTION
        $this->io->title("Starting sitemap generation...");

        $this->startTimer();
        $section = $output->section();
        $section->writeln("Clean up old sitemaps...");
        $this->deleteOldSitemaps();
        $section->overwrite("Clean up old sitemaps... OK (".$this->getTimerValue()."s)");


        $xml = $this->xmlGenerator->generate($output);

        $fs-> dumpFile($this->webDir.'/sitemap.xml', $xml);

        $this->io->success("Done !");

        return 0;
    }

    /**
     * @return void
     */
    private function deleteOldSitemaps(): void
    {
        $finder = new Finder();
        $finder->name("sitemap*.xml")->in($this->webDir);

        foreach ($finder as $file) {
            unlink($file->getRealPath());
        }
    }

    /**
     * @return void
     */
    private function startTimer(): void
    {
        $this->timer = hrtime(true);
    }

    /**
     * @return float
     */
    private function getTimerValue(): float
    {
        $timeEnd = hrtime(true);
        return round(($timeEnd - $this->timer)/1000000000, 2);
    }
}
