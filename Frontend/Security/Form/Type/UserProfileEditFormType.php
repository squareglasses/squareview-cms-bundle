<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Form\Type;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserProfileEditFormType
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserProfileEditFormType extends AbstractType
{
    /**
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(private readonly ConfigurationBag $configurationBag)
    {
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('username', TextType::class, ['required' => true])
            ->add('email', EmailType::class, ['required' => false])
            ->add('personalInfos', $this->configurationBag->getProfilePersonalInfosFormClass())
        ;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => $this->configurationBag->getSecurityUserClass(),
        ]);
    }
}
