<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Form\Type;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

/**
 * Class ResettingFormType
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResettingFormType extends AbstractType
{
    private string $class;

    /**
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(ConfigurationBag $configurationBag)
    {
        $this->class = $configurationBag->getSecurityUserClass();
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     *
     * @return void
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('plainPassword', RepeatedType::class, [
            'type' => PasswordType::class,
            'options' => [
                'translation_domain' => 'form',
                'attr' => [
                    'autocomplete' => 'new-password',
                ],
            ],
            'constraints' => [
                new NotBlank(["message"=>"error.password.required"]),
            ],
            'first_options' => ['label' => 'new_password'],
            'second_options' => ['label' => 'new_password_confirmation'],
            'invalid_message' => 'error.password.mismatch',
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => $this->class,
            'csrf_token_id' => 'resetting',
        ]);
    }
}
