<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use JetBrains\PhpStorm\ArrayShape;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\TokenDeauthenticatedEvent;

/**
 * Class LogoutHandler
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LogoutHandler implements EventSubscriberInterface
{
    public function logout(TokenDeauthenticatedEvent $event): void
    {
        // Your handling here
    }

    #[ArrayShape([TokenDeauthenticatedEvent::class => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            TokenDeauthenticatedEvent::class => 'logout'
        ];
    }
}
