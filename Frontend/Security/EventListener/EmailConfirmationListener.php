<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Common\Mailer\Mailer;
use SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent;
use SG\CmsBundle\Frontend\Security\UserEvents;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Routing\RouterInterface;

/**
 * Class EmailConfirmationListener
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class EmailConfirmationListener implements EventSubscriberInterface
{
    private SessionInterface $session;

    /**
     * @param Mailer          $mailer
     * @param RouterInterface $router
     * @param RequestStack    $requestStack
     */
    public function __construct(private readonly Mailer $mailer, private readonly RouterInterface $router, RequestStack $requestStack)
    {
        $this->session  = $requestStack->getSession();
    }

    /**
     * @return string[]
     */
    #[ArrayShape([UserEvents::REGISTRATION_COMPLETED => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            UserEvents::REGISTRATION_COMPLETED => 'onRegistrationCompleted',
        ];
    }

    /**
     * @param FilterUserResponseEvent $event
     *
     * @return void
     * @throws TransportExceptionInterface
     */
    public function onRegistrationCompleted(FilterUserResponseEvent $event): void
    {
        $user = $event->getUser();
        $this->mailer->sendRegistrationConfirmationEmailMessage($user);

        $this->session->set('app_user_send_confirmation_email/email', $user->getEmail());

        $url = $this->router->generate('app_registration_check_email');
        $event->setResponse(new RedirectResponse($url));
    }
}
