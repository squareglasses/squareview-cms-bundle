<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent;
use SG\CmsBundle\Frontend\Security\Event\UserEvent;
use SG\CmsBundle\Frontend\Security\LoginManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Core\Exception\DisabledException;

/**
 * Class AuthenticationListener
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AuthenticationListener implements EventSubscriberInterface
{
    /**
     * @param LoginManager $loginManager
     */
    public function __construct(private readonly LoginManager $loginManager)
    {
    }

    /**
     * @return string[]
     */
    #[ArrayShape([UserEvents::REGISTRATION_CONFIRMED => "string", UserEvents::RESETTING_RESET_COMPLETED => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            UserEvents::REGISTRATION_CONFIRMED => 'authenticate',
            UserEvents::RESETTING_RESET_COMPLETED => 'authenticate',
        ];
    }

    /**
     * @param FilterUserResponseEvent  $event
     * @param string                   $eventName
     * @param EventDispatcherInterface $eventDispatcher
     *
     * @return void
     */
    public function authenticate(FilterUserResponseEvent $event, string $eventName, EventDispatcherInterface $eventDispatcher): void
    {
        try {
            $this->loginManager->logInUser($event->getUser(), $event->getResponse());

            $eventDispatcher->dispatch(new UserEvent($event->getUser(), $event->getRequest()), UserEvents::SECURITY_IMPLICIT_LOGIN);
        } catch (DisabledException) {
            // We simply do not authenticate users which do not pass the user
            // checker (not enabled, expired, etc.).
        }
    }
}
