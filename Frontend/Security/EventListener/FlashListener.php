<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use InvalidArgumentException;
use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Translation\Translator\TranslatorHelper;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * Class FlashListener
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FlashListener implements EventSubscriberInterface
{
    /**
     * @var string[]
     */
    private static array $successMessages = [
        UserEvents::CHANGE_PASSWORD_COMPLETED => ['message' => 'change_password.success', 'domain' => 'form'],
        UserEvents::PROFILE_EDIT_COMPLETED => ['message' => 'profile_edit.success', 'domain' => 'flashes'],
        UserEvents::REGISTRATION_COMPLETED => ['message' => 'registration.success', 'domain' => 'form'],
        UserEvents::RESETTING_RESET_COMPLETED =>  ['message' => 'flash.success', 'domain' => 'form'],
    ];

    private SessionInterface $session;

    /**
     * @param RequestStack     $requestStack
     * @param TranslatorHelper $translator
     */
    public function __construct(RequestStack $requestStack, private readonly TranslatorHelper $translator)
    {
        $this->session = $requestStack->getSession();
    }

    /**
     * @return string[]
     */
    #[ArrayShape([UserEvents::CHANGE_PASSWORD_COMPLETED => "string", UserEvents::PROFILE_EDIT_COMPLETED => "string", UserEvents::REGISTRATION_COMPLETED => "string", UserEvents::RESETTING_RESET_COMPLETED => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            UserEvents::CHANGE_PASSWORD_COMPLETED   => 'addSuccessFlash',
            UserEvents::PROFILE_EDIT_COMPLETED      => 'addSuccessFlash',
            UserEvents::REGISTRATION_COMPLETED      => 'addSuccessFlash',
            UserEvents::RESETTING_RESET_COMPLETED   => 'addSuccessFlash',
        ];
    }

    /**
     * @param Event $event
     * @param       $eventName
     *
     * @return void
     */
    public function addSuccessFlash(Event $event, $eventName): void
    {
        if (!isset(self::$successMessages[$eventName])) {
            throw new InvalidArgumentException('This event does not correspond to a known flash message');
        }

        $this->session->getFlashBag()->add(
            'success',
            $this->translator->trans(
                self::$successMessages[$eventName]['message'],
                [],
                self::$successMessages[$eventName]['domain']
            )
        );
    }
}
