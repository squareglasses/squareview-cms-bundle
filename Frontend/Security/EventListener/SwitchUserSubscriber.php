<?php
declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\SwitchUserEvent;
use Symfony\Component\Security\Http\SecurityEvents;

/**
 * Class SwitchUserSubscriber
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SwitchUserSubscriber implements EventSubscriberInterface
{
    /**
     * @param SwitchUserEvent $event
     *
     * @return void
     */
    public function onSwitchUser(SwitchUserEvent $event): void
    {
        $event->getTargetUser()->setImpersonated(true);
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            SecurityEvents::SWITCH_USER => 'onSwitchUser',
        ];
    }
}
