<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\Security\Event\FormEvent;
use SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Translation\Translator\TranslatorHelper;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use SG\CmsBundle\Frontend\Security\Model\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;

/**
 * Class ResettingListener
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResettingListener implements EventSubscriberInterface
{
    private SessionInterface $session;

    /**
     * @param UrlGeneratorInterface       $router
     * @param UserPasswordHasherInterface $passwordEncoder
     * @param ConfigurationBag            $configurationBag
     * @param RequestStack                $requestStack
     * @param TranslatorHelper            $translatorHelper
     */
    public function __construct(
        private readonly UrlGeneratorInterface       $router,
        private readonly UserPasswordHasherInterface $passwordEncoder,
        private readonly ConfigurationBag            $configurationBag,
        private readonly RequestStack                $requestStack,
        private readonly TranslatorHelper            $translatorHelper
    ) {
        $this->session          = $requestStack->getSession();
    }

    /**
     * @return string[]
     */
    #[ArrayShape([
        UserEvents::RESETTING_RESET_INITIALIZE => "string",
        UserEvents::RESETTING_RESET_SUCCESS => "string",
        UserEvents::RESETTING_RESET_REQUEST => "string"])
    ]
    public static function getSubscribedEvents(): array
    {
        return [
            UserEvents::RESETTING_RESET_INITIALIZE => 'onResettingResetInitialize',
            UserEvents::RESETTING_RESET_SUCCESS => 'onResettingResetSuccess',
            UserEvents::RESETTING_RESET_REQUEST => 'onResettingResetRequest',
        ];
    }

    /**
     * @param GetResponseUserEvent $event
     *
     * @return void
     */
    public function onResettingResetInitialize(GetResponseUserEvent $event): void
    {
        if (!$event->getUser()->isPasswordRequestNonExpired($this->configurationBag->getSecurityResettingRetryTtl())) {
            $this->session->getFlashBag()->add('error', 'error.request_expired');
            $event->setResponse(new RedirectResponse($this->router->generate('app_resetting_request')));
        }
    }

    /**
     * @param FormEvent $event
     *
     * @return void
     */
    public function onResettingResetSuccess(FormEvent $event): void
    {
        /** @var $user PasswordAuthenticatedUserInterface|UserInterface */
        $user = $event->getForm()->getData();

        // encode the plain password
        $user->setPassword(
            $this->passwordEncoder->hashPassword(
                $user,
                $event->getForm()->get('plainPassword')->getData()
            )
        );

        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);
        $user->setEnabled(true);
    }

    /**
     * @param GetResponseUserEvent $event
     *
     * @return void
     */
    public function onResettingResetRequest(GetResponseUserEvent $event): void
    {
        if (!$event->getUser()->isAccountNonLocked()) {
            $this->session->getFlashBag()->add('error', $this->translatorHelper->trans('error.account_locked', [], 'form', $this->getLocale()));
            $event->setResponse(new RedirectResponse($this->router->generate('app_resetting_request')));
        }

        if ($event->getUser()->getPasswordRequestedAt() !== null
            && true === $event->getUser()->isPasswordRequestNonExpired($this->configurationBag->getSecurityResettingRetryTtl())) {
            $this->session->getFlashBag()->add('error', $this->translatorHelper->trans('error.already_requested', [], 'form', $this->getLocale()));
            $event->setResponse(new RedirectResponse($this->router->generate('app_resetting_request')));
        }
    }

    /**
     * @return string
     */
    private function getLocale(): string
    {
        $request = $this->requestStack->getMainRequest();
        if (null === $request) {
            return 'fr';
        }
        return $request->getLocale();
    }
}
