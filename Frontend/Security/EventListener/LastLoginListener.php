<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\EventListener;

use DateTime;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Security\Event\UserEvent;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use SG\CmsBundle\Frontend\Security\UserEvents;
use SG\CmsBundle\Frontend\Security\Model\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\Security\Http\SecurityEvents;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class LastLoginListener
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LastLoginListener implements EventSubscriberInterface
{
    /**
     * @param UserManager $userManager
     */
    public function __construct(protected UserManager $userManager)
    {
    }

    /**
     * @return array
     */
    #[ArrayShape([UserEvents::SECURITY_IMPLICIT_LOGIN => "string", SecurityEvents::INTERACTIVE_LOGIN => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            UserEvents::SECURITY_IMPLICIT_LOGIN => 'onImplicitLogin',
            SecurityEvents::INTERACTIVE_LOGIN => 'onSecurityInteractiveLogin',
        ];
    }

    /**
     * @param UserEvent $event
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function onImplicitLogin(UserEvent $event): void
    {
        $user = $event->getUser();
        $user->setLastLogin(new DateTime());
        try {
            $this->userManager->updateUser($user);
        } catch (Exception) {
            // Do nothing
        }
    }

    /**
     * @param InteractiveLoginEvent $event
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function onSecurityInteractiveLogin(InteractiveLoginEvent $event): void
    {
        $user = $event->getAuthenticationToken()->getUser();

        if ($user instanceof UserInterface) {
            $user->setLastLogin(new DateTime());
            try {
                $this->userManager->updateUser($user);
            } catch (Exception) {
                // Do nothing
            }
        }
    }
}
