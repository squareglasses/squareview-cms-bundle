<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Backend\Security\Model\User;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class UserProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserProvider implements UserProviderInterface
{
    /**
     * @param UserManager     $userManager
     * @param RequestStack    $requestStack
     * @param LoggerInterface $securityLogger
     */
    public function __construct(
        private readonly UserManager  $userManager,
        private readonly RequestStack $requestStack,
        private readonly LoggerInterface $securityLogger
    ) {
    }

    /**
     * @param string $identifier
     *
     * @return UserInterface
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws HttpExceptionInterface
     */
    public function loadUserByIdentifier(string $identifier): UserInterface
    {
        $this->securityLogger->debug("loadUserByIdentifier");
        $user = $this->userManager->findUser($identifier);
        if ($user) {
            return $user;
        }
        throw new UserNotFoundException();
    }

    /**
     * Refreshes the user after being reloaded from the session.
     *
     * When a user is logged in, at the beginning of each request, the
     * User object is loaded from the session and then this method is
     * called. Your job is to make sure the user's data is still fresh by,
     * for example, re-querying for fresh User data.
     *
     * If your firewall is "stateless: true" (for a pure API), this
     * method is not called.
     *
     * @param UserInterface $user
     *
     * @return UserInterface
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function refreshUser(UserInterface $user): UserInterface
    {
        $this->securityLogger->debug("refreshUser");
        if (!$this->userManager->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        //Skip user refresh on ajax calls
        //On the backend, many ajax calls are made on each page, so this would be a performance issue
        $request = $this->requestStack->getMainRequest();
        if (null !== $request && $request->isXmlHttpRequest()) {
            return $user;
        }

        try {
            // Refresh user data for enabled check
            $refreshedUser = $this->userManager->refreshToken($user);
        } catch (Exception) {
            $this->triggerError(User::USER_NOT_FOUND_MESSAGE);
        }

        // Refresh user data for enabled check
        if (null !== $refreshedUser && !$refreshedUser->isEnabled()) {
            $this->triggerError(User::USER_DISABLED_MESSAGE);
        }
        // Refresh user data for locked check
        if (null !== $refreshedUser && $refreshedUser->isLocked()) {
            $this->triggerError(User::USER_LOCKED_MESSAGE);
        }

        if (null !== $refreshedUser) {
            return $refreshedUser;
        }

        throw new UserNotFoundException();
    }

    /**
     * @param string $message
     *
     * @return void
     */
    private function triggerError(string $message): void
    {
        $request = $this->requestStack->getMainRequest();
        $request?->getSession()->set(
            Security::AUTHENTICATION_ERROR,
            new CustomUserMessageAuthenticationException($message)
        );
        throw new CustomUserMessageAuthenticationException($message);
    }

    /**
     * Tells Symfony to use this provider for this User class.
     *
     * @param string $class
     *
     * @return bool
     */
    public function supportsClass(string $class): bool
    {
        return $this->userManager->getUserClass() === $class;
    }
}
