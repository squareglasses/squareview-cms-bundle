<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Event;

use SG\CmsBundle\Frontend\Security\Model\UserInterface;
use Symfony\Contracts\EventDispatcher\Event;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class UserEvent
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserEvent extends Event
{
    /**
     * @param UserInterface $user
     * @param Request|null  $request
     */
    public function __construct(protected UserInterface $user, protected ?Request $request = null)
    {
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }

    /**
     * @return Request|null
     */
    public function getRequest(): ?Request
    {
        return $this->request;
    }
}
