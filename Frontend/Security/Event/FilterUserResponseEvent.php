<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Event;

use JetBrains\PhpStorm\Pure;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use SG\CmsBundle\Frontend\Security\Model\UserInterface;

/**
 * Class FilterUserResponseEvent
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class FilterUserResponseEvent extends UserEvent
{
    protected ?Response $response = null;

    /**
     * @param UserInterface $user
     * @param Request       $request
     * @param Response      $response
     */
    #[Pure] public function __construct(UserInterface $user, Request $request, Response $response)
    {
        parent::__construct($user, $request);
        $this->response = $response;
    }

    /**
     * @return Response|null
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }

    /**
     * @param Response|null $response
     *
     * @return void
     */
    public function setResponse(?Response $response = null): void
    {
        $this->response = $response;
    }
}
