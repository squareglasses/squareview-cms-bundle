<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Event;

use Symfony\Component\HttpFoundation\Response;

/**
 * Class GetResponseUserEvent
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GetResponseUserEvent extends UserEvent
{
    protected ?Response $response = null;

    /**
     * @param Response|null $response
     *
     * @return void
     */
    public function setResponse(?Response $response = null): void
    {
        $this->response = $response;
    }

    /**
     * @return Response|null
     */
    public function getResponse(): ?Response
    {
        return $this->response;
    }
}
