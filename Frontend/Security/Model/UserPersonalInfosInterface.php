<?php
declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Model;

use App\Model\UserPersonalInfos;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;
use DateTimeInterface;

interface UserPersonalInfosInterface
{
    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;
}
