<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security\Model;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Exception\HttpClientException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Exception\InvalidUserInputException;
use SG\CmsBundle\Common\HttpClient\ApiClient;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;

/**
 * Class UserManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserManager
{
    /**
     * @param ApiClient           $apiClient
     * @param SerializerInterface $serializer
     * @param ConfigurationBag    $configurationBag
     */
    public function __construct(
        private readonly ApiClient           $apiClient,
        private readonly SerializerInterface $serializer,
        private readonly ConfigurationBag    $configurationBag
    ) {
    }

    /**
     * @param string $username
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws HttpExceptionInterface
     */
    public function findUser(string $username): ?UserInterface
    {
        $response = $this->apiClient->apiRequest(
            'users/'.$username.'?serialization_groups[]=get&serialization_groups[]=security',
            ApiClient::METHOD_GET,
            [],
            false
        );

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent());
        }

        return null;
    }

    /**
     * @param string $token
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function findUserByConfirmationToken(string $token): ?UserInterface
    {
        $response = $this->apiClient->apiRequest(
            'users/by-confirmation-token/'.$token,
            ApiClient::METHOD_GET,
            [],
            false
        );
        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent());
        }
        return null;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function refreshUser(UserInterface $user): ?UserInterface
    {
        try {
            $response = $this->apiClient->userRequest(
                $user,
                'users/refresh',
                ApiClient::METHOD_GET,
                [],
                false
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $this->deserializeUser($response->getContent(), $user);
            }
            return null;
        } catch (HttpClientException) {
            return null;
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws InvalidUserInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function createUser(UserInterface $user): ?UserInterface
    {
        if (!$this->isSupported($user)) {
            throw new RuntimeException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        $response = $this->apiClient->apiRequest('users', ApiClient::METHOD_POST, [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => $this->serializer->serialize($user, 'json', ['groups' => 'post'])
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode === 201) {
            return $this->deserializeUser($response->getContent());
        }

        if ($statusCode === 400) {
            $error = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
            if (array_key_exists("violations", $error)) {
                throw new InvalidUserInputException($error['title'], $error['violations']);
            }
        } elseif ($response->getContent() !== null) {
            throw new RuntimeException($response->getContent());
        }

        throw new InvalidUserInputException("Unknown error");
    }

    /**
     * @param UserInterface $user
     * @param bool          $loggedUserRequest
     * @param array         $groups
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function updateUser(UserInterface $user, bool $loggedUserRequest = true, array $groups = []): ?UserInterface
    {
        if (empty($groups)) {
            $groups = ['put'];
        }
        if (!$this->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        $jsonUser = $this->serializer->serialize($user, 'json', ['groups' => $groups]);

        if ($loggedUserRequest) {
            $response = $this->apiClient->userRequest($user, 'users', ApiClient::METHOD_PUT, [
                'body' => $jsonUser
            ]);
        } else {
            $response = $this->apiClient->apiRequest('users/'.$user->getId(), ApiClient::METHOD_PUT, [
                'body' => $jsonUser
            ]);
        }

        return $this->deserializeUser($response->getContent(), $user);
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws InvalidUserInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function updatePassword(UserInterface $user): ?UserInterface
    {
        if (!$this->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        $jsonUser = $this->serializer->serialize($user, 'json', ['groups' => 'change-password']);

        $response = $this->apiClient->userRequest($user, 'users/change-password', ApiClient::METHOD_PUT, [
            'body' => $jsonUser
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent(), $user);
        }

        if ($statusCode === 400) {
            $error = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
            if (array_key_exists("violations", $error)) {
                throw new InvalidUserInputException($error['title'], $error['violations']);
            }
        }
        throw new InvalidUserInputException("Unknown error");
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws InvalidUserInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function resetPassword(UserInterface $user): ?UserInterface
    {
        if (!$this->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        $jsonUser = $this->serializer->serialize($user, 'json', ['groups' => 'reset-password']);
        $response = $this->apiClient->apiRequest('users/reset-password/'.$user->getId(), ApiClient::METHOD_PUT, [
            'body' => $jsonUser
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent(), $user);
        }

        if ($statusCode === 400) {
            $error = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
            if (array_key_exists("violations", $error)) {
                throw new InvalidUserInputException($error['title'], $error['violations']);
            }
        }
        throw new InvalidUserInputException("Unknown error");
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws InvalidUserInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function activate(UserInterface $user): ?UserInterface
    {
        if (!$this->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        $jsonUser = $this->serializer->serialize($user, 'json', ['groups' => ['reset-password', 'activate']]);
        $response = $this->apiClient->apiRequest('users/activate/'.$user->getId(), ApiClient::METHOD_PUT, [
            'body' => $jsonUser
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent(), $user);
        }

        if ($statusCode === 400) {
            $error = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
            if (array_key_exists("violations", $error)) {
                throw new InvalidUserInputException($error['title'], $error['violations']);
            }
        }
        throw new InvalidUserInputException("Unknown error");
    }

    /**
     * @param string        $username
     * @param string        $password
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function generateToken(string $username, string $password, UserInterface $user): ?UserInterface
    {
        $tokenResponse = $this->apiClient->request(
            'authentication_token',
            ApiClient::METHOD_POST,
            [
            'body' => json_encode([
                'username' => $username,
                'password' => $password
            ], JSON_THROW_ON_ERROR)
        ],
            false,
            null,
            false
        );
        if ($tokenResponse->getStatusCode() === 200) {
            return $this->deserializeUser($tokenResponse->getContent(), $user);
        }

        throw new CustomUserMessageAuthenticationException('Cannot authenticate user.');
    }

    /**
     * @param UserInterface|BaseUserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function refreshToken(UserInterface|BaseUserInterface $user): ?UserInterface
    {
//        var_dump($user);die;
        if ($user->isImpersonated()) {
            return $user;
        }
        if (!$user->getRefreshToken()) {
            return null;
        }
        $response = $this->apiClient->request('authentication_token/refresh', ApiClient::METHOD_POST, [
           'body' => json_encode(['refreshToken' => $user->getRefreshToken()], JSON_THROW_ON_ERROR)
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent(), $user);
        }
        return null;
    }

    /**
     * @param string             $serializedValue
     * @param UserInterface|null $user
     *
     * @return UserInterface
     */
    private function deserializeUser(
        string $serializedValue,
        UserInterface $user = null
    ): UserInterface {
        $context = [];
        if ($user !== null) {
            $context = array_merge([
                AbstractNormalizer::OBJECT_TO_POPULATE => $user,
                'groups' => ['get']
            ], $context);
        }
        /** @var UserInterface $user */
        return $this->serializer->deserialize($serializedValue, $this->getUserClass(), 'json', $context);
    }

    /**
     * @param UserInterface|BaseUserInterface $user
     *
     * @return bool
     */
    public function isSupported(UserInterface|BaseUserInterface $user): bool
    {
        if (!is_a($user, $this->getUserClass())) {
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    public function getUserClass(): string
    {
        return $this->configurationBag->getSecurityUserClass();
    }
}
