<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use SG\CmsBundle\Frontend\Security\Model\UserInterface as  FrontendUserInterface;

/**
 * Class UserChecker
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(private readonly ConfigurationBag $configurationBag)
    {
    }

    /**
     * @param UserInterface|FrontendUserInterface $user
     *
     * @return void
     */
    public function checkPreAuth(UserInterface|FrontendUserInterface $user): void
    {
        if (!is_a($user, $this->getUserClass())) {
            return;
        }

        if (!$user->isEnabled()) {
            $ex = new DisabledException('User account is disabled.');
            $ex->setUser($user);
            throw $ex;
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPostAuth(UserInterface $user): void
    {
        if (!is_a($user, $this->getUserClass())) {
            return;
        }
    }

    /**
     * @return string
     */
    private function getUserClass(): string
    {
        return $this->configurationBag->getSecurityUserClass();
    }
}
