<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security;

use Symfony\Contracts\EventDispatcher\Event;

/**
 * Class UserEvents
 * Contains all events thrown in the user management process.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class UserEvents
{
    /**
     * The SECURITY_IMPLICIT_LOGIN event occurs when the user is logged in programmatically.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\SquareviewBundle\Frontend\Security\Event\UserEvent")
     */
    public const SECURITY_IMPLICIT_LOGIN = 'app.security.implicit_login';

    /**
     * The REGISTRATION_INITIALIZE event occurs when the registration process is initialized.
     *
     * This event allows you to modify the default values of the user before binding the form.
     *
     * @Event("SG\SquareviewBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const REGISTRATION_INITIALIZE = 'app.registration.initialize';

    /**
     * The REGISTRATION_SUCCESS event occurs when the registration form is submitted successfully.
     *
     * This event allows you to set the response instead of using the default one.
     *
     * @Event("SG\SquareviewBundle\Frontend\Security\Event\FormEvent")
     */
    public const REGISTRATION_SUCCESS = 'app.registration.success';

    /**
     * The REGISTRATION_COMPLETED event occurs after saving the user in the registration process.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\SquareviewBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const REGISTRATION_COMPLETED = 'app.registration.completed';

    /**
     * The REGISTRATION_CONFIRMATION event occurs just before confirming the account.
     *
     * This event allows you to access the user which will be confirmed.
     *
     * @Event("SG\SquareviewBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const REGISTRATION_CONFIRMATION = 'app.registration.confirmation';

    /**
     * The REGISTRATION_CONFIRMED event occurs after confirming the account.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\SquareviewBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const REGISTRATION_CONFIRMED = 'app.registration.confirmed';

    /**
     * The CHANGE_PASSWORD_INITIALIZE event occurs when the change password process is initialized.
     *
     * This event allows you to modify the default values of the user before binding the form.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const CHANGE_PASSWORD_INITIALIZE = 'app.change_password.initialize';

    /**
     * The CHANGE_PASSWORD_SUCCESS event occurs when the change password form is submitted successfully.
     *
     * This event allows you to set the response instead of using the default one.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FormEvent")
     */
    public const CHANGE_PASSWORD_SUCCESS = 'app.change_password.edit.success';

    /**
     * The CHANGE_PASSWORD_COMPLETED event occurs after saving the user in the change password process.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const CHANGE_PASSWORD_COMPLETED = 'app.change_password.completed';

    /**
     * The ACTIVATION_INITIALIZE event occurs when the activation process is initialized.
     *
     * This event allows you to modify the default values of the user before binding the form.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const ACTIVATION_INITIALIZE = 'app.activation.initialize';

    /**
     * The ACTIVATION_SUCCESS event occurs when the activation form is submitted successfully.
     *
     * This event allows you to set the response instead of using the default one.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FormEvent")
     */
    public const ACTIVATION_SUCCESS = 'app.activation.success';

    /**
     * The CHANGE_PASSWORD_COMPLETED event occurs after saving the user in the activation process.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const ACTIVATION_COMPLETED = 'app.activation.completed';

    /**
     * The PROFILE_EDIT_INITIALIZE event occurs when the profile editing process is initialized.
     *
     * This event allows you to modify the default values of the user before binding the form.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const PROFILE_EDIT_INITIALIZE = 'fos_user.profile.edit.initialize';

    /**
     * The PROFILE_EDIT_SUCCESS event occurs when the profile edit form is submitted successfully.
     *
     * This event allows you to set the response instead of using the default one.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FormEvent")
     */
    public const PROFILE_EDIT_SUCCESS = 'app.profile.edit.success';

    /**
     * The PROFILE_EDIT_COMPLETED event occurs after saving the user in the profile edit process.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const PROFILE_EDIT_COMPLETED = 'app.profile.edit.completed';

    /**
     * The RESETTING_RESET_REQUEST event occurs when a user requests a password reset of the account.
     *
     * This event allows you to check if a user is locked out before requesting a password.
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const RESETTING_RESET_REQUEST = 'app.resetting.request';

    /**
     * The RESETTING_RESET_INITIALIZE event occurs when the resetting process is initialized.
     *
     * This event allows you to set the response to bypass the processing.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const RESETTING_RESET_INITIALIZE = 'app.resetting.reset.initialize';

    /**
     * The RESETTING_RESET_SUCCESS event occurs when the resetting form is submitted successfully.
     *
     * This event allows you to set the response instead of using the default one.
     *
     * @Event("G\CmsBundle\Frontend\Security\Event\FormEvent ")
     */
    public const RESETTING_RESET_SUCCESS = 'app.resetting.reset.success';

    /**
     * The RESETTING_RESET_COMPLETED event occurs after saving the user in the resetting process.
     *
     * This event allows you to access the response which will be sent.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\FilterUserResponseEvent")
     */
    public const RESETTING_RESET_COMPLETED = 'app.resetting.reset.completed';

    /**
     * The RESETTING_SEND_EMAIL_INITIALIZE event occurs when send email process is initialized.
     *
     * This event allows you to set the response to bypass the email confirmation processing.
     * The event listener method receives a FOS\UserBundle\Event\GetResponseNullableUserEvent instance.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const RESETTING_SEND_EMAIL_INITIALIZE = 'app.resetting.send_email.initialize';

    /**
     * The RESETTING_SEND_EMAIL_CONFIRM event occurs when all prerequisites to send email are
     * confirmed and before the mail is sent.
     *
     * This event allows you to set the response to bypass the email sending.
     * The event listener method receives an SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent instance.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const RESETTING_SEND_EMAIL_CONFIRM = 'app.resetting.send_email.confirm';

    /**
     * The RESETTING_SEND_EMAIL_COMPLETED event occurs after the email is sent.
     *
     * This event allows you to set the response to bypass the redirection after the email is sent.
     * The event listener method receives a FOS\UserBundle\Event\GetResponseUserEvent instance.
     *
     * @Event("SG\CmsBundle\Frontend\Security\Event\GetResponseUserEvent")
     */
    public const RESETTING_SEND_EMAIL_COMPLETED = 'app.resetting.send_email.completed';
//
//    /**
//     * The USER_CREATED event occurs when the user is created with UserManipulator.
//     *
//     * This event allows you to access the created user and to add some behaviour after the creation.
//     *
//     * @Event("FOS\UserBundle\Event\UserEvent")
//     */
//    const USER_CREATED = 'fos_user.user.created';
//
//    /**
//     * The USER_PASSWORD_CHANGED event occurs when the user is created with UserManipulator.
//     *
//     * This event allows you to access the created user and to add some behaviour after the password change.
//     *
//     * @Event("FOS\UserBundle\Event\UserEvent")
//     */
//    const USER_PASSWORD_CHANGED = 'fos_user.user.password_changed';

//
}
