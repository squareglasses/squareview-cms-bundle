<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Security;

use JsonException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Security\Model\UserManager;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use SG\CmsBundle\Frontend\Security\Model\UserInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Http\Authentication\UserAuthenticatorInterface;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\RememberMeBadge;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Abstracts process for manually logging in a user.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LoginManager
{
    private SessionInterface $session;

    /**
     * @param UserChecker                $userChecker
     * @param UserManager                $userManager
     * @param RequestStack               $requestStack
     * @param UserAuthenticatorInterface $authenticatorManager
     * @param LoginFormAuthenticator     $authenticator
     */
    public function __construct(
        private readonly UserChecker                $userChecker,
        private readonly UserManager                $userManager,
        private readonly RequestStack               $requestStack,
        private readonly UserAuthenticatorInterface $authenticatorManager,
        private readonly LoginFormAuthenticator     $authenticator
    ) {
        $this->session = $requestStack->getSession();
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    final public function logInUser(UserInterface $user): void
    {
        $this->userChecker->checkPreAuth($user);
        $request = $this->requestStack->getMainRequest();
        if (null === $request) {
            return;
        }

        if ($this->session->has('registration_token_'.$user->getId())) {
            $tokens = $this->session->get('registration_token_'.$user->getId());
            // update user's tokens to allow auto-login
            // TODO: Check a better way to doing this
            $user->setToken($tokens['token']);
        }

        $this->authenticatorManager->authenticateUser(
            $user,
            $this->authenticator,
            $request,
            [
                new RememberMeBadge()
            ]
        );
    }

    /**
     * @param UserInterface $user
     *
     * @return JWTUserToken|null
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws HttpExceptionInterface
     */
    protected function createToken(UserInterface $user): ?JWTUserToken
    {
        if (null !== $user->getPlainPassword()) {
            $user = $this->userManager->generateToken($user->getUsername(), $user->getPlainPassword(), $user);
            $user->setPlainPassword(null);
        } else {
            if ($this->session->has('registration_token_'.$user->getId())) {
                $tokens = $this->session->get('registration_token_'.$user->getId());
                // update user's tokens to allow auto-login
                $user->setToken($tokens['token']);
                $user->setRefreshToken($tokens['refreshToken']);
                $this->session->remove('registration_token_'.$user->getId());
            }
            $user = $this->userManager->refreshToken($user);
        }

        if (!$user) {
            return null;
        }
        return new JWTUserToken($user->getRoles(), $user, null, null);
    }
}
