<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Referer\EventListener;

use SG\CmsBundle\Frontend\Referer\Referer;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\ResponseEvent;

/**
 * Class RefererListener
 * Referer listener listen to kernel response event to define or not the
 * current resource as the referer.
 * Referer is used to know which was the last page viewed.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RefererListener
{
    /**
     * @param array<string> $excludedRoutes
     * @param array<string> $excludedPatterns
     */
    public function __construct(protected array $excludedRoutes = [], protected array $excludedPatterns = [])
    {
    }

    /**
     * @param ResponseEvent $event
     *
     * @return void
     */
    public function onKernelResponse(ResponseEvent $event): void
    {
        $request = $event->getRequest();
        $session = $request->getSession();

        //isMasterRequest
        if (!$event->isMainRequest()
                || $request->isXmlHttpRequest()
                || $request->attributes->get('_route') === null
        ) {
            return;
        }

        if ($request->getRequestFormat() === "json") {
            return;
        }

        if (!$this->isRefererAllowed($request)) {
            return;
        }

        // Create and configure the referer, then set the session
        $session->set('sg.referer', new Referer($request));
    }

    /**
     * Check the request to determine if the referer has to be change or not
     *
     * @param Request $request
     *
     * @return bool
     */
    protected function isRefererAllowed(Request $request): bool
    {
        // Check excluded routes
        if (in_array($request->attributes->get('_route'), $this->excludedRoutes, true)) {
            return false;
        }

        if ($request->isXmlHttpRequest()) {
            return false;
        }

        // Check excluded patterns/regex
        foreach ($this->excludedPatterns as $pattern) {
            if (preg_match($pattern, $request->getUri())) {
                return false;
            }
        }

        return true;
    }
}
