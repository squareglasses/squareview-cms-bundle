<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Referer;

use Symfony\Component\HttpFoundation\Request;

/**
 * Referer is used to know which was the last page viewed.
 * It contains useful request values.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Referer
{
    protected string $uri;
    protected string $route;
    protected ?string $routeBaseName = null;
    protected ?string $path = null;

    /** @var array<string,mixed> $routeParameters */
    protected array $routeParameters = [];

    /** @var array<string,mixed> $queryParameters */
    protected array $queryParameters = [];

    /**
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->resolve($request);
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function resolve(Request $request): self
    {
        $this->uri = $request->getUri();
        $this->route = $request->attributes->get('_route');
        $this->routeBaseName = $request->attributes->get('_route_base_name');
        $this->path = $request->attributes->get('_path');
        $this->queryParameters = $request->query->all();
        $this->routeParameters = $request->attributes->get('_route_params');
        return $this;
    }

    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }

    /**
     * @return array<string,mixed>
     */
    public function getRouteParameters(): array
    {
        return $this->routeParameters;
    }

    /**
     * @return array<string,mixed>
     */
    public function getQueryParameters(): array
    {
        return $this->queryParameters;
    }

    /**
     * @param string $parameterName
     *
     * @return mixed|null
     */
    public function getRouteParameter(string $parameterName): mixed
    {
        return $this->routeParameters[$parameterName] ?? null;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @return string|null
     */
    public function getRouteBaseName(): ?string
    {
        return $this->routeBaseName;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }
}
