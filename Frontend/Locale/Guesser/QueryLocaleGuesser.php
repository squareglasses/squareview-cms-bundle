<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Guesser;

use SG\CmsBundle\Frontend\Locale\LocaleInformation\LocaleExtractor;
use SG\CmsBundle\Frontend\Locale\Validator\MetaValidator;
use Symfony\Component\HttpFoundation\Request;


/**
 * This guesser class checks the query parameter for a var
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class QueryLocaleGuesser extends AbstractLocaleGuesser
{
    private string $queryParameterName = "_locale";

    /**
     * @param MetaValidator $metaValidator
     */
    public function __construct(private readonly MetaValidator $metaValidator)
    {
    }

    /**
     * Guess the locale based on the query parameter variable
     *
     * @param Request $request
     * @return string|null
     */
    public function guessLocale(Request $request): ?string
    {
        $localeValidator = $this->metaValidator;
        if ($request->query->has($this->queryParameterName)) {
            if ($localeValidator->isAllowed($request->query->get($this->queryParameterName))) {
                return LocaleExtractor::extractLocale($request->query->get($this->queryParameterName));
            }
        }

        return null;
    }
}
