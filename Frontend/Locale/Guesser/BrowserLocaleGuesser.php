<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Guesser;

use SG\CmsBundle\Frontend\Locale\LocaleInformation\LocaleExtractor;
use SG\CmsBundle\Frontend\Locale\Validator\MetaValidator;
use Symfony\Component\HttpFoundation\Request;

/**
 * Locale Guesser for detecing the locale from the browser Accept-language string
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class BrowserLocaleGuesser extends AbstractLocaleGuesser
{
    /**
     * @param MetaValidator $metaValidator
     */
    public function __construct(private readonly MetaValidator $metaValidator)
    {
    }

    /**
     * Guess the locale based on the browser settings
     *
     * @param Request $request
     *
     * @return boolean
     */
    public function guessLocale(Request $request): ?string
    {
        // Get the preferred locale from the Browser.
        $preferredLocale = $request->getPreferredLanguage();
        $availableLocales = $request->getLanguages();

        if (!$preferredLocale or count($availableLocales) === 0) {
            return null;
        }

        // If the preferred primary locale is allowed, return the locale.
        if ($this->metaValidator->isAllowed($preferredLocale)) {
            return LocaleExtractor::extractLocale($preferredLocale);
        }

        // Get a list of available and allowed locales and return the first result
        $validator = $this->metaValidator;
        $matchLocale = function ($v) use ($validator) {
            return $validator->isAllowed($v);
        };

        $result = array_values(array_filter($availableLocales, $matchLocale));
        if (!empty($result)) {
            return LocaleExtractor::extractLocale($result[0]);;
        }

        return null;
    }
}
