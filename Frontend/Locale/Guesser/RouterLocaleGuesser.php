<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Guesser;

use SG\CmsBundle\Frontend\Locale\LocaleInformation\LocaleExtractor;
use SG\CmsBundle\Frontend\Locale\Validator\MetaValidator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Locale Guesser for detecing the locale in the router
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class RouterLocaleGuesser extends AbstractLocaleGuesser
{
    /**
     * @param MetaValidator $metaValidator
     */
    public function __construct(private readonly MetaValidator $metaValidator)
    {
    }

    /**
     * Method that guess the locale based on the Router parameters
     * @param Request $request
     * @return string|null
     */
    public function guessLocale(Request $request): ?string
    {
        $localeValidator = $this->metaValidator;
        if ($locale = $request->attributes->get('_locale')) {
            if ($localeValidator->isAllowed($locale)) {
                return LocaleExtractor::extractLocale($locale);
            } else {
                throw new NotFoundHttpException("Locale '".$locale."' is not allowed.");
            }
        }

        return null;
    }
}
