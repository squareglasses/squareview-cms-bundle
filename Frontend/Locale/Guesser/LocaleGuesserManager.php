<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Guesser;

use Psr\Log\LoggerInterface;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use SG\CmsBundle\Frontend\Locale\Guesser\LocaleGuesserInterface;

/**
 * Locale Guesser Manager
 *
 * This class is responsible for adding services with the 'sg.locale.guesser'
 * alias tag and run the detection.
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class LocaleGuesserManager
{
    private array $guessingOrder = [];
    private array $guessers = [];
    private array $preferredLocales = [];

    /**
     * Constructor
     *
     * @param array           $guessingOrder Config Value for the guessing order
     * @param LoggerInterface $logger        The Logger
     */
    public function __construct(private readonly ConfigurationBag $configurationBag, private readonly ?LoggerInterface $logger = null)
    {
        $this->guessingOrder = $this->configurationBag->getLocaleGuessingOrder();
    }

    /**
     * Adds a guesser to this manager
     *
     * @param LocaleGuesserInterface $guesser The Guesser Service
     * @param string                 $alias   Alias of the Service
     */
    public function addGuesser(LocaleGuesserInterface $guesser, string $alias): void
    {
        $this->guessers[$alias] = $guesser;
    }

    /**
     * Returns the guesser
     *
     * @param string $alias
     *
     * @return LocaleGuesserInterface|null
     */
    public function getGuesser(string $alias): ?LocaleGuesserInterface
    {
        if (array_key_exists($alias, $this->guessers)) {
            return $this->guessers[$alias];
        } else {
            return null;
        }
    }

    /**
     * Removes a guesser from this manager
     *
     * @param string $alias
     *
     * @return bool
     */
    public function removeGuesser(string $alias): void
    {
        unset($this->guessers[$alias]);
    }

    /**
     * Loops through all the activated Locale Guessers and
     * calls the guessLocale methode and passing the current request.
     *
     * @param Request $request
     *
     * @return string|null false if no locale is identified
     */
    public function runLocaleGuessing(Request $request): ?string
    {
        $this->preferredLocales = $request->getLanguages();
        foreach ($this->configurationBag->getLocaleGuessingOrder() as $guesser) {
            $guesserService = $this->getGuesser($guesser);
            if (null === $guesserService) {
                throw new InvalidConfigurationException(sprintf('Locale guesser service "%s" does not exist.', $guesser));
            }

            $this->logEvent('Locale %s guessing service loaded', ucfirst($guesser));
            $guessedLocale = $guesserService->guessLocale($request);

            if (null !== $guessedLocale) {
                $locale = $guessedLocale;
                $this->logEvent('Locale has been identified by guessing service: "%s"', ucfirst($guesser));

                return $locale;
            }
            $this->logEvent('Locale has not been identified by the "%s" guessing service', ucfirst($guesser));
        }
        return null;
    }

    /**
     * Log detection events
     *
     * @param string $logMessage
     * @param string $parameters
     */
    private function logEvent($logMessage, $parameters = null)
    {
        if (null !== $this->logger) {
            $this->logger->debug(sprintf($logMessage, $parameters));
        }
    }

    /**
     * Retrieves the detected preferred locales
     *
     * @return array
     */
//    public function getPreferredLocales()
//    {
//        return $this->preferredLocales;
//    }

    /**
     * Returns the current guessingorder
     *
     * @return array
     */
    public function getGuessingOrder(): array
    {
        return $this->configurationBag->getLocaleGuessingOrder();
    }
}
