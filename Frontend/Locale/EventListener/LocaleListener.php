<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\EventListener;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\Locale\Guesser\BrowserLocaleGuesser;
use SG\CmsBundle\Frontend\Locale\Guesser\LocaleGuesserManager;
use SG\CmsBundle\Frontend\Locale\LocaleInformation\AllowedLocalesProvider;

use SG\CmsBundle\Frontend\Locale\LocaleInformation\LocaleExtractor;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


/**
 * Locale Listener
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class LocaleListener implements EventSubscriberInterface
{
    private ?string $defaultLocale;
    private ?string $excludedPattern = "_wdt";
    private bool $multilingual;

    /**
     * @param ConfigurationBag $configurationBag
     * @param AllowedLocalesProvider $allowedLocalesProvider
     * @param LocaleGuesserManager $guesserManager
     * @param BrowserLocaleGuesser $browserLocaleGuesser
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        ConfigurationBag $configurationBag,
        private readonly AllowedLocalesProvider $allowedLocalesProvider,
        private readonly LocaleGuesserManager $guesserManager,
        private readonly BrowserLocaleGuesser $browserLocaleGuesser,
        private readonly ?LoggerInterface $logger = null
    ) {
        $this->defaultLocale = $configurationBag->getDefaultLocale();
        $this->multilingual = $configurationBag->isMultilingual();
    }

    public static function getSubscribedEvents(): array
    {
        return array(
            // must be registered after the Router to have access to the _locale and before the Symfony LocaleListener
            KernelEvents::REQUEST => array(array('onKernelRequest', 24))
        );
    }

    /**
     * Called at the "kernel.request" event
     *
     * Call the LocaleGuesserManager to guess the locale
     * by the activated guessers
     *
     * Sets the identified locale as default locale to the request
     *
     * @param RequestEvent $event
     */
    public function onKernelRequest(RequestEvent $event)
    {
        if (!$event->isMainRequest()) {
            return;
        }
        $request = $event->getRequest();
        if ($this->excludedPattern && preg_match(sprintf('#%s#', $this->excludedPattern), $request->getPathInfo())) {
            return;
        }

        // Force browser locale guesser for homepage because router always sets a default locale
        // browserLocaleGuesser in LocaleGuesserManager will never be used
        if ($request->getRequestUri() === '/' && $this->multilingual) {
            $browserLocale = $this->browserLocaleGuesser->guessLocale($request);
            if ($browserLocale && $browserLocale !== $this->defaultLocale) {
                $event->setResponse(new RedirectResponse($request->getUri().$browserLocale));
            }
        }

        if (!$this->multilingual) {
            $request->setLocale($this->defaultLocale);
            $request->attributes->set('_locale', $this->defaultLocale);
            return;
        }

        // If request contains a locale, check if the locale is allowed
        $queryLocale = LocaleExtractor::getQueryLocale($request);
        if (null !== $queryLocale && !$this->allowedLocalesProvider->isLocaleAllowed($queryLocale)) {
            throw new NotFoundHttpException('Locale "'.$queryLocale.'" is not allowed.');
        }

        $locale = $this->guesserManager->runLocaleGuessing($request);
        if ($locale) {
            $this->logEvent('Setting [ %s ] as locale for the (Sub-)Request', $locale);
            $request->setLocale($locale);
            $request->attributes->set('_locale', $locale);
        }
    }

    /**
     * Log detection events
     *
     * @param string $logMessage
     * @param string|null $parameters
     */
    private function logEvent(string $logMessage, ?string $parameters = null)
    {
        $this->logger?->info(sprintf($logMessage, $parameters));
    }
}
