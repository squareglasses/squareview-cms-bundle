<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * LocaleAllowed Constraint
 *
 * @Annotation
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class LocaleAllowed extends Constraint
{
    /**
     * @var string
     */
    public $message = 'The locale "%string%" is not allowed by application configuration.';

    /**
     * {@inheritDoc}
     */
    public function validatedBy()
    {
        return LocaleAllowedValidator::class;
    }
}
