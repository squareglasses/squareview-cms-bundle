<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Validator;

use Symfony\Component\Validator\Constraint;

/**
 * Locale Constraint
 *
 * @Annotation
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class Locale extends Constraint
{
    /**
     * @var string
     */
    public $message = 'The locale "%string%" is not a valid locale';

    /**
     * {@inheritDoc}
     */
    public function validatedBy(): string
    {
        return LocaleValidator::class;
    }
}
