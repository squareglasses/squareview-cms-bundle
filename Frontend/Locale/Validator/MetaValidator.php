<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Validator;

use SG\CmsBundle\Frontend\Locale\LocaleInformation\LocaleExtractor;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * This Metavalidator uses the LocaleAllowed and Locale validators for checks inside a guesser
 *
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class MetaValidator
{
    /**
     * Constructor
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(private readonly ValidatorInterface $validator)
    {
    }

    /**
     * Checks if a locale is allowed and valid
     *
     * @param string $locale
     *
     * @return bool
     */
    public function isAllowed(string $locale): bool
    {
        $locale = LocaleExtractor::extractLocale($locale);
        $errorListLocale = $this->validator->validate($locale, new Locale);
        $errorListLocaleAllowed = $this->validator->validate($locale, new LocaleAllowed);

//        var_dump($errorListLocale);
//        var_dump($errorListLocaleAllowed);
////        var_dump((count($errorListLocale) == 0 && count($errorListLocaleAllowed) == 0));
//        die;

        return (count($errorListLocale) == 0 && count($errorListLocaleAllowed) == 0);
    }
}
