<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\Validator;

use SG\CmsBundle\Frontend\Locale\LocaleInformation\AllowedLocalesProvider;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Validator to check if a locale is allowed by the configuration
 *
 * @author Florent Chaboud <florent@sqsuareglasses.com>
 */
class LocaleAllowedValidator extends ConstraintValidator
{
    /**
     * @var AllowedLocalesProvider
     */
    private $allowedLocalesProvider;

    /**
     * @var bool
     */
    private $strictMode;

    /**
     * @var bool
     */
    private $intlExtension;

    /**
     * Constructor
     *
     * @param AllowedLocalesProvider  $allowedLocalesProvider  Allowed locales provided by service
     * @param bool                    $strictMode              Match locales strict (e.g. de_DE will not match allowedLocale de)
     * @param bool                    $intlExtension           Whether the intl extension is installed
     */
    public function __construct(AllowedLocalesProvider $allowedLocalesProvider = null, bool $strictMode = false, bool $intlExtension = true)
    {
        $this->allowedLocalesProvider = $allowedLocalesProvider;
        $this->strictMode = $strictMode;
        $this->intlExtension = $intlExtension;
    }

    /**
     * Validates a Locale
     *
     * @param string     $locale     The locale to be validated
     * @param Constraint $constraint Locale Constraint
     *
     * @throws UnexpectedTypeException
     */
    public function validate(mixed $locale, Constraint $constraint): void
    {
        if (null === $locale || '' === $locale) {
            return;
        }

        if (!is_scalar($locale) && !(is_object($locale) && method_exists($locale, '__toString'))) {
            throw new UnexpectedTypeException($locale, 'string');
        }

        $locale = (string) $locale;

        if ($this->strictMode) {
            if (!in_array($locale, $this->getAllowedLocales())) {
                $this->context->addViolation($constraint->message, array('%string%' => $locale));
            }
        } else {
            if ($this->intlExtension) {
                $primary = \Locale::getPrimaryLanguage($locale);
            } else {
                $splittedLocale = explode('_', $locale);
                $primary = count($splittedLocale) > 1 ? $splittedLocale[0] : $locale;
            }
            if (!in_array($locale, $this->getAllowedLocales()) && (!in_array($primary, $this->getAllowedLocales()))) {
                $this->context->addViolation($constraint->message, array('%string%' => $locale));
            }
        }
    }

    /**
     * Get the allowed locales from provider
     *
     * @return array
     */
    protected function getAllowedLocales()
    {
        return $this->allowedLocalesProvider->getAllowedLocales();
    }
}
