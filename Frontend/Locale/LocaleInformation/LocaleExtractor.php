<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\LocaleInformation;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class LocaleExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LocaleExtractor
{
    public static function extractLocale(string $locale): string
    {
        if (preg_match("/_/", $locale)) {
            return \Symfony\Component\Intl\Locale::getFallback($locale);
        }
        return $locale;
    }

    public static function getQueryLocale(Request $request): ?string
    {
        return $request->query->get("_locale");
    }
}
