<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Locale\LocaleInformation;


use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Common\Bag\ConfigurationBag;

/**
 * @author Florent Chaboud <florent@squareglasses.com/>
 */
class AllowedLocalesProvider
{

    /**
     * Constructor
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(private readonly ConfigurationBag $configurationBag)
    {
    }

    /**
     * Check if locale is allowed
     *
     * @param string $locale
     */
    public function isLocaleAllowed($locale)
    {
        return in_array($locale, $this->getAllowedLocales());
    }

    /**
     * Return a list of the allowed locales
     *
     * @return array
     */
    public function getAllowedLocales()
    {
        return $this->configurationBag->getAllowedLocales();
    }
}
