<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\Menu\MenuFactory;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class MenuExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MenuExtension extends AbstractExtension
{
    /**
     * @param Environment $environment
     * @param MenuFactory $menuFactory
     */
    public function __construct(private readonly Environment $environment, private readonly MenuFactory $menuFactory)
    {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'render_menu',
                [$this, 'renderMenu'],
                ['is_safe' => ['html']]
            )
        ];
    }

    /**
     * @param string      $menuIdentifier
     * @param string|null $template
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function renderMenu(string $menuIdentifier, ?string $template = null): string
    {
        $menu = $this->menuFactory->generate($menuIdentifier);
        return $this->environment->render($template ?? "menus/default.html.twig", [
            'menu' => $menu
        ]);
    }
}
