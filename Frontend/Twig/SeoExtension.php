<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use JsonException;
use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Exception\MetaTagValidationException;
use SG\CmsBundle\Frontend\Model\MetaTag;
use Symfony\Component\Serializer\SerializerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class SeoExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SeoExtension extends AbstractExtension
{
    /**
     * @param SerializerInterface $serializer
     * @param WebsiteBag          $websiteBag
     */
    public function __construct(private readonly SerializerInterface $serializer, private readonly WebsiteBag $websiteBag)
    {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'render_meta_tags',
                [$this, 'renderMetasTags'],
                ['is_safe' => ['html']]
            )
        ];
    }

    /**
     * @param array<MetaTag>   $metaTags
     * @param array<int,array> $customTags
     *
     * @return string
     * @throws MetaTagValidationException
     * @throws JsonException
     */
    public function renderMetasTags(array $metaTags, array $customTags = [], array $fallbackTags = []): string
    {
        // Merge custom tags
        if (count($customTags) > 0) {
            $metaTags = $this->mergeTags($metaTags, $customTags);
        }

        $metaTags = $this->mergeTags($this->parseMetaTags($fallbackTags), $metaTags);

        // Merge website tags for fallback
        $metaTags = $this->mergeFallbackTags($metaTags);

        $html = '';
        foreach ($metaTags as $metaTag) {
            $html.=$metaTag->getHtml().'
        ';
        }
        return $html;
    }

    /**
     * @param array $tagsArray
     *
     * @return array
     * @throws MetaTagValidationException
     * @throws JsonException
     */
    private function parseMetaTags(array $tagsArray): array
    {
        $tags = [];
        foreach ($tagsArray as $customTagArray) {
            if ($customTagArray instanceof MetaTag) {
                $tags[] = $customTagArray;
            } else if (MetaTag::validateDeserializationInput($customTagArray)) {
                $metaTag = $this->serializer->deserialize(json_encode($customTagArray, JSON_THROW_ON_ERROR), MetaTag::class, 'json');
                $tags[] = $metaTag;
            }
        }

        return $tags;
    }


    /**
     * @param array $metaTags
     * @param array $customTags
     *
     * @return array
     * @throws JsonException
     * @throws MetaTagValidationException
     */
    private function mergeTags(array $metaTags, array $customTags): array
    {
        $tagsToMerge = [];
        foreach ($customTags as $customTagArray) {
            if ($customTagArray instanceof MetaTag) {
                $tagsToMerge[] = $customTagArray;
            } else if (MetaTag::validateDeserializationInput($customTagArray)) {
                $metaTag = $this->serializer->deserialize(json_encode($customTagArray, JSON_THROW_ON_ERROR), MetaTag::class, 'json');
                $tagsToMerge[] = $metaTag;
            }
        }

        // Check if some tags to merge override resource tags and override them if necessary
        foreach ($metaTags as $key => $metaTag) {
            foreach ($tagsToMerge as $toMergeKey => $tagToMerge) {
                if ($this->isSame($metaTag, $tagToMerge)) {
                    $metaTags[$key] = $tagToMerge;
                    unset($tagsToMerge[$toMergeKey]);
                }
            }
        }

        // Add new tags to merge to the array of tags
        foreach ($tagsToMerge as $tagToMerge) {
            $metaTags[] = $tagToMerge;
        }
        return $metaTags;
    }

    /**
     * @param array<MetaTag> $metaTags
     *
     * @return array<MetaTag>
     */
    private function mergeFallbackTags(array $metaTags): array
    {
        foreach ($this->websiteBag->getMetaTags() as $fallbackTag) {
            if (!$this->tagAlreadyExists($metaTags, $fallbackTag)) {
                $metaTags[] = $fallbackTag;
            }
        }
        return $metaTags;
    }

    /**
     * @param array<MetaTag> $metaTags
     * @param MetaTag        $tag
     *
     * @return bool
     */
    private function tagAlreadyExists(array $metaTags, MetaTag $tag): bool
    {
        foreach ($metaTags as $existingTag) {
            if ($existingTag->getType() === $tag->getType() && $existingTag->getName() === $tag->getName()) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param MetaTag $first
     * @param MetaTag $second
     *
     * @return bool
     */
    private function isSame(MetaTag $first, MetaTag $second): bool
    {
        return $first->getName() === $second->getName() && $first->getType() === $second->getType();
    }
}
