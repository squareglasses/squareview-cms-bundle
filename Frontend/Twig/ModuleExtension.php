<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use SG\CmsBundle\Frontend\Exception\ModuleGuideConfigurationException;
use SG\CmsBundle\Frontend\Exception\ModuleGuideNotFoundException;
use SG\CmsBundle\Frontend\Provider\ModuleGuideProvider;
use SG\CmsBundle\Frontend\Contracts\ModuleInterface;
use SG\CmsBundle\Frontend\Contracts\ResourceBagInterface;
use SG\CmsBundle\Common\Contracts\ResourceGuideInterface;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class ModuleExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleExtension extends AbstractExtension
{
    /**
     * @param ModuleGuideProvider $guideProvider
     */
    public function __construct(
        private readonly ModuleGuideProvider $guideProvider,
        private readonly ResourceGuideProvider $resourceGuideProvider
    ) {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'render_modules',
                [$this, 'renderModules'],
                ['is_safe' => ['html']]
            )
        ];
    }

    /**
     * @param ResourceBagInterface $cmsBag
     * @param string|null          $zoneName
     *
     * @return string
     * @throws LoaderError
     * @throws ModuleGuideConfigurationException
     * @throws ModuleGuideNotFoundException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws JsonException
     */
    public function renderModules(ResourceBagInterface $cmsBag, ?string $zoneName = null): string
    {
        $html = '';
        if (null === $cmsBag->getGuide() && $cmsBag->get("guide")) {
            $guide = $this->resourceGuideProvider->getGuide($cmsBag->get("guide"));
            $cmsBag->setGuide($guide);
        }
        $renderedModules = $this->parseModules($cmsBag->getModules(), $cmsBag->getGuide(), $zoneName);
        foreach ($renderedModules as $zonedModules) {
            foreach ($zonedModules['modules'] as $renderedModule) {
                $html.= $renderedModule['html'];
            }
        }
        return $html;
    }

    /**
     * @param array                  $modules
     * @param ResourceGuideInterface $resourceGuide
     * @param string|null            $zoneName
     *
     * @return array[]
     * @throws LoaderError
     * @throws ModuleGuideConfigurationException
     * @throws ModuleGuideNotFoundException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws JsonException
     * @throws Exception
     */
    private function parseModules(array $modules, ResourceGuideInterface $resourceGuide, ?string $zoneName = null): array
    {
        $renderedModules = [];
        $modules = $this->sortModules($modules);

        foreach ($modules as $module) {
            $zone = $this->getZone($resourceGuide, $module->getZone());
            $zone['object']->addModule($module);
            if ($zone['name'] === $zoneName || $zoneName === null) {
                if (!array_key_exists($zone['name'], $renderedModules)) {
                    $renderedModules[$zone['name']] = [
                        'position' => $zone['position'],
                        'modules' => []
                    ];
                }
                $renderedModules[$zone['name']]['modules'][] = [
                    'position' => $module->getPosition(),
                    'zone_position' => $zone['name'],
                    'html' => $this->renderModule($module)
                ];
            }
        }
        return $this->sortZones($renderedModules);
    }

    /**
     * @param ResourceGuideInterface $guide
     * @param string|null            $zoneName
     *
     * @return array
     * @throws Exception
     */
    #[ArrayShape(['name' => "null|string", 'position' => "int|null", 'object' => "\SG\CmsBundle\Common\Contracts\ZoneInterface"])]
    private function getZone(ResourceGuideInterface $guide, ?string $zoneName = null): array
    {
        $zoneName = $zoneName ?: 'default';
        $zone = $guide->getZone($zoneName);

        return [
            'name' => $zone->getName(),
            'position' => $zone->getPosition(),
            'object' => $zone
        ];
    }

    /**
     * @param array<ModuleInterface> $modules
     *
     * @return array<ModuleInterface>
     */
    private function sortModules(array $modules): array
    {
        usort($modules, static function ($a, $b) {
            return $a->getPosition() <=> $b->getPosition();
        });
        return $modules;
    }

    /**
     * @param array<string,array> $renderedModules
     *
     * @return array<string,array>
     */
    private function sortZones(array $renderedModules): array
    {
        usort($renderedModules, static function ($a, $b) {
            return $a['position'] <=> $b['position'];
        });
        return $renderedModules;
    }

    /**
     * @param ModuleInterface $module
     *
     * @return string
     * @throws LoaderError
     * @throws ModuleGuideConfigurationException
     * @throws ModuleGuideNotFoundException
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws JsonException
     */
    private function renderModule(ModuleInterface $module): string
    {
        $guide = $this->guideProvider->getGuide($module->getGuideIdentifier());
        $renderer = $this->guideProvider->getRenderer($guide);
        $module->setGuide($guide);

        return $renderer->setModule($module)->execute()->render();
    }
}
