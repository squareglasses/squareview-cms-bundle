<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use RuntimeException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Frontend\Model\Pagination;
use Symfony\Component\HttpFoundation\RequestStack;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class PaginationExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PaginationExtension extends AbstractExtension
{
    /**
     * @param Environment      $environment
     * @param RequestStack     $requestStack
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(
        private readonly Environment  $environment,
        private readonly RequestStack $requestStack,
        private readonly ConfigurationBag $configurationBag
    ) {
    }

    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'render_pagination',
                [$this, 'renderPagination'],
                ['is_safe' => ['html']]
            )
        ];
    }

    /**
     * @param Pagination $pagination
     *
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function renderPagination(Pagination $pagination): string
    {
        $pagination->setPageRange($this->configurationBag->getPaginationConfiguration("page_range"));
        $request = $this->requestStack->getMainRequest();
        if (null === $request) {
            throw new RuntimeException("Request is null");
        }
        return $this->environment->render(
            $this->configurationBag->getPaginationConfiguration("template"),
            array_merge($pagination->getViewData(), [
                'route' => $request->get('_route'),
                'query' => $request->query->all(),
                'pageParameterName' => $this->configurationBag->getPaginationConfiguration("page_parameter_name"),
            ])
        );
    }
}
