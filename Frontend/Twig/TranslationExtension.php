<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use SG\CmsBundle\Frontend\Bag\WebsiteBag;
use SG\CmsBundle\Frontend\Translation\Translator\TranslatorHelper;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;

/**
 * Class TranslationExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class TranslationExtension extends AbstractExtension
{
    /**
     * @param Environment      $environment
     * @param TranslatorHelper $helper
     */
    public function __construct(
        private readonly Environment $environment,
        private readonly TranslatorHelper $helper,
        private readonly WebsiteBag $websiteBag
    ) {
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter(
                'trans',
                [$this, 'trans'],
                []
            )
        ];
    }

    /**
     * @return TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'js_translations',
                [$this, 'getJsTranslations'],
                [
                    "is_safe" => ["html"]
                ]
            )
        ];
    }

    /**
     * @param string      $message
     * @param array       $arguments
     * @param string|null $domain
     * @param string|null $locale
     * @param int|null    $count
     *
     * @return string
     */
    public function trans(
        string $message,
        array $arguments = [],
        string $domain = null,
        string $locale = null,
        int $count = null
    ): string {
        return $this->helper->trans($message, $arguments, $domain, $locale, $count);
    }

    /**
     * @return string
     * @throws LoaderError
     * @throws RuntimeError
     * @throws SyntaxError
     */
    public function getJsTranslations(bool $json = false): string
    {
        $catalogs = [];
        foreach ($this->helper->getResourceTranslations() as $catalogName => $translations) {
            $catalogs[$catalogName] = $translations;
        }
        foreach ($this->helper->getLayoutTranslations() as $catalogName => $translations) {
            $catalogs[$catalogName] = $translations;
        }
        foreach ($this->helper->getGuideTranslations() as $catalogName => $translations) {
            if (array_key_exists($catalogName, $catalogs)) {
                $catalogs[$catalogName] = array_merge($catalogs[$catalogName], $translations);
            } else {
                $catalogs[$catalogName] = $translations;
            }
        }
        $catalogs = array_merge($catalogs, $this->websiteBag->getTranslations());

        if ($json) {
            return $this->environment->render("@SGCms/frontend/translations/js_translations.json.twig", [
                'catalogs' => $catalogs
            ]);
        } else {
            return $this->environment->render("@SGCms/frontend/translations/js_translations.html.twig", [
                'catalogs' => $catalogs
            ]);
        }
    }
}
