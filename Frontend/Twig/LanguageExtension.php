<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class LanguageExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageExtension extends AbstractExtension
{
    /**
     * @return array|TwigFunction[]
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction(
                'language_switch',
                [LanguageRuntime::class, 'renderLanguageSwitch'],
                ['is_safe' => ['html']]
            )
        ];
    }
}
