<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Twig;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\HttpClient\Response\ResponseParser;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Error\SyntaxError;
use Twig\Extension\RuntimeExtensionInterface;

/**
 * Class LanguageRuntime
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageRuntime implements RuntimeExtensionInterface
{
    /**
     * LanguageRuntime constructor.
     * @param Environment $environment
     * @param ApiClient $apiClient
     * @param ResponseParser $responseParser
     */
    public function __construct(
        private readonly Environment $environment,
        private ApiClient            $apiClient,
        private ResponseParser       $responseParser
    ) {
    }

    /**
     * @param string|null $template
     *
     * @return string
     * @throws ClientExceptionInterface
     * @throws LoaderError
     * @throws RedirectionExceptionInterface
     * @throws RuntimeError
     * @throws ServerExceptionInterface
     * @throws SyntaxError
     * @throws TransportExceptionInterface
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws HttpExceptionInterface
     */
    public function renderLanguageSwitch(?string $template = null): string
    {
        $template = $template ?? "@SGCms/frontend/locale/___language_switch.html.twig";
        // @TODO passer par la gateway au lieu d'appeler directement le endpoint languages
        $response = $this->apiClient->apiRequest('languages?enabled=true');
        $languages = $this->responseParser->parse($response);
        return $this->environment->render($template, [
            'languages' => $languages
        ]);
    }

    /**
     * Used for testing
     *
     * @param ApiClient $apiClient
     *
     * @return void
     */
    public function setApiClient(ApiClient $apiClient): void
    {
        $this->apiClient = $apiClient;
    }

    /**
     * Used for testing
     *
     * @param ResponseParser $responseParser
     *
     * @return void
     */
    public function setResponseParser(ResponseParser $responseParser): void
    {
        $this->responseParser = $responseParser;
    }
}
