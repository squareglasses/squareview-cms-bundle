<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Frontend\Menu;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use RuntimeException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Frontend\HttpClient\ApiClient;
use SG\CmsBundle\Common\Menu\Model\Menu;
use SG\CmsBundle\Common\Menu\Model\MenuItem;
use SG\CmsBundle\Common\Contracts\MenuItemInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class MenuFactory
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MenuFactory
{
    /**
     * @param RequestStack        $requestStack
     * @param ApiClient           $apiClient
     * @param SerializerInterface $serializer
     * @param RouterInterface     $router
     */
    public function __construct(
        private readonly RequestStack        $requestStack,
        private readonly ApiClient           $apiClient,
        private readonly SerializerInterface $serializer,
        private readonly RouterInterface     $router
    ) {
    }

    /**
     * @param string $menuIdentifier
     *
     * @return Menu
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws HttpExceptionInterface
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function generate(string $menuIdentifier): Menu
    {
        if (null === $this->requestStack->getMainRequest()) {
            throw new RuntimeException("Request is null");
        }
        $response = $this->apiClient->apiRequest('menus/'.$this->requestStack->getMainRequest()->getLocale().'/'.$menuIdentifier.'/published');
        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            /** @var Menu $menu */
            $content = $response->getContent();
            $menu = $this->serializer->deserialize($content, Menu::class, 'json', []);
        } else {
            throw new RuntimeException("Menu not found for identifier ".$menuIdentifier);
        }
        return $this->processItems($menu);
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     * @throws Exception
     */
    private function processItems(Menu $menu): Menu
    {
        foreach ($menu->getItems() as $item) {
            $this->processItem($item);
        }
        return $menu;
    }

    /**
     * @param MenuItem      $item
     * @param MenuItem|null $parentItem
     *
     * @return void
     * @throws Exception
     */
    private function processItem(MenuItem $item, ?MenuItem $parentItem = null): void
    {
        $uri = "";
        switch ($item->getKind()) {
            case MenuItemInterface::KIND_RESOURCE:
            case MenuItemInterface::KIND_ROUTE:
                if (!$item->getRoute()) {
                    throw new RuntimeException("Route not configured"); //@TODO Customize exception
                }
            $uri = $this->router->generate($item->getRoute());
            break;
//            case MenuItemInterface::KIND_ROUTE:
////                if (!$item->getRoute()) {
////                    throw new RuntimeException("Route not configured"); //@TODO Customize exception
////                }
//                $uri = $this->router->generate($item->getValue());
//                break;
            case MenuItemInterface::KIND_URL:
                $uri = $item->getValue();
                break;
        }

        if ($item->getAnchor()) {
            $uri.="#".$item->getAnchor();
        }
        $item->setUri($uri);

        $liAttributes = $item->getLiAttributes();

        if ($this->isActive($item)) {
            $item->addLiAttribute("class", "active ".$item->getCssClasses());
            $item->setIsCurrent(true);
            $parentItem?->addLiAttribute("class", "active");
        } else {
            $item->addLiAttribute("class", $item->getCssClasses() ?? "");
        }

        if ($item->getTarget()) {
            $item->addLinkAttribute("target", $item->getTarget());
        }

        foreach ($item->getChildren() as $subItem) {
            $this->processItem($subItem, $item);
        }
    }

    /**
     * @param MenuItem $item
     *
     * @return bool
     */
    private function isActive(MenuItem $item): bool
    {
        $request = $this->requestStack->getMainRequest();
        if (!$request) {
            return false;
        }
        // Classic routes
        $routeName = $item->getRoute();
        if ($routeName === $request->get('_route')) {
            return true;
        }

        // Localized routes
        if ($routeName.'.'.$request->getLocale() === $request->get('_route')) {
            return true;
        }

        if (null !== $item->getUri() && preg_match("^".$item->getUri()."^", $request->getRequestUri())) {
            return true;
        }

        return false;
    }
}
