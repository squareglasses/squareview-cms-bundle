<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ApiClient;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Backend\ApiClient\Model\ApiRequest;
use Exception;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Common\Contracts\HttpClientResponseInterface;
use SG\CmsBundle\Common\HttpClient\ApiClient as SquareViewApiClient;
use SG\CmsBundle\Backend\Exception\InvalidFormInputException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ApiClient
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiClient
{
    /**
     * @param SquareViewApiClient $apiClient
     * @param LoggerInterface     $apiLogger
     */
    public function __construct(private readonly SquareViewApiClient $apiClient, private readonly LoggerInterface $apiLogger)
    {
    }

    /**
     * @param ApiRequest $apiRequest
     *
     * @return HttpClientResponseInterface
     * @throws JsonException
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function request(ApiRequest $apiRequest): HttpClientResponseInterface
    {
        $this->logRequest($apiRequest);

        //Base options?
        $options = [];

        if ($apiRequest->getUser()) {
            try {
                $response = $this->apiClient->userRequest(
                    $apiRequest->getUser(),
                    $apiRequest->getEndpoint(),
                    $apiRequest->getMethod(),
                    $this->buildRequestOptions($apiRequest, $options)
                );
            } catch (Exception $e) {
                $this->logError($e);
                throw $e;
            }
        } else {
            try {
                $response = $this->apiClient->apiRequest(
                    $apiRequest->getEndpoint(),
                    $apiRequest->getMethod(),
                    $this->buildRequestOptions($apiRequest, $options)
                );
            } catch (Exception $e) {
                $this->logError($e);
                throw $e;
            }
        }

        $this->logResponse($response);

        return $response;
    }

    /**
     * @param ApiRequest $apiRequest
     * @param array      $options
     *
     * @return array
     */
    protected function buildRequestOptions(ApiRequest $apiRequest, array $options = []): array
    {
        $filters = $apiRequest->getFilters();
        $query = array_merge($filters, $apiRequest->getQuery());

        $options['query'] = array_key_exists('query', $options) ? array_merge($options['query'], $query) : $query;
        if ($apiRequest->getFormData()) {
            $options['headers'] = $apiRequest->getFormData()->getPreparedHeaders()->toArray();
            $options['body'] = $apiRequest->getFormData()->bodyToString();
        } else {
            $options['body'] = $apiRequest->getBody() ?: null;
        }

        return $options;
    }

    /**
     * @param ApiRequest $apiRequest
     *
     * @return void
     */
    private function logRequest(ApiRequest $apiRequest): void
    {
        $this->apiLogger->info(sprintf(
            "API Request > %s %s?%s %s [Body: '%s'] [RequestOptions: %s]",
            $apiRequest->getMethod(),
            $apiRequest->getEndpoint(),
            http_build_query($apiRequest->getQuery()),
            $apiRequest->getUser() ? '[User: '.$apiRequest->getUser().']' : '[User: ApiClient]',
            $apiRequest->getBody(),
            var_export($this->buildRequestOptions($apiRequest), true)
        ));
    }

    /**
     * @param HttpClientResponseInterface|null $response
     *
     * @return void
     */
    private function logResponse(?HttpClientResponseInterface $response): void
    {
        if ($response) {
            $this->apiLogger->info(sprintf(
                "API Response > [%s] %s",
                $response->getStatusCode(),
                $response->getContent()
            ));
        } else {
            $this->apiLogger->info("No response");
        }
    }

    /**
     * @param Exception $e
     *
     * @return void
     */
    private function logError(Exception $e): void
    {
        $this->apiLogger->info(sprintf(
            "API Error > [%s] %s (%s:%s) - Trace: %s",
            $e->getCode(),
            $e->getMessage(),
            $e->getFile(),
            $e->getLine(),
            $e->getTraceAsString()
        ));
    }
}
