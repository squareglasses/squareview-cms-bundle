<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ApiClient;

use RuntimeException;
use SG\CmsBundle\Backend\ApiClient\Model\ApiRequest;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\HeaderUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mime\Part\DataPart;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ApiRequestBuilder
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiRequestBuilder
{
    private string $apiName;
    private string $apiType;
    private array $handledRequestParameters = [];

    /**
     * RequestBuilder constructor.
     * @param SerializerInterface $serializer
     * @param array $apisConfiguration
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly array               $apisConfiguration
    ) {
    }

    /**
     * @param string        $apiReference
     * @param Request       $request
     * @param UserInterface $user
     *
     * @return ApiRequest
     */
    public function build(string $apiReference, Request $request, UserInterface $user): ApiRequest
    {
        $this->deconstructApiReference($apiReference);

        if (!array_key_exists($this->apiName, $this->apisConfiguration)) {
            throw new ConfigurationException(sprintf(
                "No api named '%s' found in apis configuration. Trying to call it via ApiClient\RequestBuilder",
                $this->apiName,
            ));
        }

        $apiNameDefinition = $this->apisConfiguration[$this->apiName];
        if (!array_key_exists($this->apiType, $apiNameDefinition)) {
            throw new ConfigurationException(sprintf(
                "No type '%s' found for api named '%s' in apis configuration. Trying to call it via ApiClient\RequestBuilder",
                $this->apiType,
                $this->apiName,
            ));
        }

        $apiTypeDefinition = $apiNameDefinition[$this->apiType];
        if ($request->getMethod() !== $apiTypeDefinition['method']) {
            throw new ConfigurationException(sprintf(
                "Method '%s' not found for api reference '%s.%s' of the apis configuration",
                $request->getMethod(),
                $this->apiName,
                $this->apiType
            ));
        }

        $api = $this->buildApi($apiTypeDefinition, $request, $user);

        return $this->serializer->denormalize(
            $api,
            ApiRequest::class
        );
    }

    /**
     * @param string $locale
     * @param string $discriminator
     * @param string $id
     *
     * @return ApiRequest
     */
    public function buildPublish(string $locale, string $discriminator, string $id): ApiRequest
    {
        return (new ApiRequest())
            ->setMethod('PUT')
            ->setEndpoint(sprintf(
                '/publish/%s/%s/%s',
                $locale,
                $discriminator,
                $id
            ))
        ;
    }

    /**
     * @param string $apiReference
     *
     * @return void
     */
    public function deconstructApiReference(string $apiReference): void
    {
        $deconstructed = explode('_', $apiReference);
        $apiType = array_pop($deconstructed);
        $apiName = implode('_', $deconstructed);
        $this->apiName = $apiName;
        $this->apiType = $apiType;
    }

    /**
     * @param array         $apiData
     * @param Request       $request
     * @param UserInterface $user
     *
     * @return array
     */
    private function buildApi(array $apiData, Request $request, UserInterface $user): array
    {
        $api = [];
        $api['method'] = $request->getMethod();
        $api['endpoint'] = $this->buildEndpointParameters($apiData['endpoint'], $request);

        /** @var UploadedFile $file */
        if ($request->files !== null && $file = $request->files->get('file')) {
            if ($file->getError()) {
                throw new RuntimeException('Error uploading file: '.$file->getErrorMessage());
            }
            $api['form_data'] = new FormDataPart([
                'file' => DataPart::fromPath(
                    $file->getPathname(),
                    rawurlencode($file->getClientOriginalName())
                )
            ]);
        } elseif ($request->getContent()) {
            $api['body'] = $request->getContent();
        }

        $api['query'] = $this->buildQuery($request);

        //Use user connected to the backend, else will use the api_client user (if config 'secured: false')
        if (!array_key_exists('secured', $apiData) || $apiData['secured'] === true) {
            $api['user'] = $user;
        }

        return $api;
    }

    /**
     * @param string     $endpoint
     * @param Request    $request
     *
     * @return string
     */
    private function buildEndpointParameters(string $endpoint, Request $request): string
    {
        $parameters = $this->getRequestParameters($request);
        $body = json_decode($request->getContent(), true);
        /* Parametres dans l'url du endpoint */
        if (!str_contains($endpoint, "{")) {
            return $endpoint;
        }

        preg_match_all('/\{(.*?)\}/', $endpoint, $matches);
        foreach ($matches[0] as $match) {
            $parameterName = str_replace(['{', '}'], "", $match);
            if (array_key_exists($parameterName, $parameters)) { //parameter in query
                $endpoint = str_replace($match, $parameters[$parameterName], $endpoint);
                $this->handledRequestParameters[] = $parameterName;
            } else if ($body && array_key_exists($parameterName, $body)) {
                $endpoint = str_replace($match, (string)$body[$parameterName], $endpoint);
                $this->handledRequestParameters[] = $parameterName;
            } else {
                throw new ConfigurationException(sprintf(
                    "Missing parameter '%s' in the request, when trying to build endpoint '%s'
                        for action '%s' of section '%s'",
                    $parameterName,
                    $endpoint,
                    $this->apiType,
                    $this->apiName
                ));
            }
        }
        return $endpoint;
    }

    /**
     * @param Request $request
     *
     * @return array
     */
    private function buildQuery(Request $request): array
    {
        $parameters = $this->getRequestParameters($request);

        $query = [];
        foreach ($parameters as $parameterName => $parameterValue) {
            if (!in_array($parameterName, $this->handledRequestParameters, true)) {
                $query[$parameterName] = $parameterValue;
            }
        }

        return $query;
    }

    /**
     * Get parameters without converting dots "." to underscores "_" like the default $request->query
     *
     * @param Request $request
     *
     * @return array
     */
    private function getRequestParameters(Request $request): array
    {
        $queryString = "";
        if (str_contains($request->getRequestUri(), '?')) {
            $params = explode("?", $request->getRequestUri());
            $queryString = $params[1];
        }

        return HeaderUtils::parseQuery($queryString);
    }
}
