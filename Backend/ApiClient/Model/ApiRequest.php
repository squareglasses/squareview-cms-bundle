<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ApiClient\Model;

use SG\CmsBundle\Backend\Security\Model\UserInterface;
use Symfony\Component\Mime\Part\Multipart\FormDataPart;

/**
 * Class ApiRequest
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiRequest
{
    private string $method;
    private string $endpoint;
    private ?string $body = null;
    private array $filters;
    private array $sorters;
    private array $query;
    private ?UserInterface $user = null;
    private ?FormDataPart $formData = null; //for files

    public function __construct()
    {
        $this->filters = [];
        $this->sorters = [];
        $this->query = [];
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->method." ".$this->endpoint;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     *
     * @return $this
     */
    public function setMethod(string $method): ApiRequest
    {
        $this->method = $method;

        return $this;
    }

    /**
     * @return string
     */
    public function getEndpoint(): string
    {
        return $this->endpoint;
    }

    /**
     * @param string $endpoint
     *
     * @return $this
     */
    public function setEndpoint(string $endpoint): ApiRequest
    {
        $this->endpoint = $endpoint;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getBody(): ?string
    {
        return $this->body;
    }

    /**
     * @param string|null $body
     *
     * @return $this
     */
    public function setBody(?string $body): ApiRequest
    {
        $this->body = $body;

        return $this;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return $this->filters;
    }

    /**
     * @param array $filters
     *
     * @return $this
     */
    public function setFilters(array $filters): ApiRequest
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @return array
     */
    public function getSorters(): array
    {
        return $this->sorters;
    }

    /**
     * @param array $sorters
     *
     * @return $this
     */
    public function setSorters(array $sorters): ApiRequest
    {
        $this->sorters = $sorters;

        return $this;
    }

    /**
     * @return array
     */
    public function getQuery(): array
    {
        return $this->query;
    }

    /**
     * @param array $query
     *
     * @return $this
     */
    public function setQuery(array $query): ApiRequest
    {
        $this->query = $query;

        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * @param UserInterface|null $user
     *
     * @return $this
     */
    public function setUser(?UserInterface $user): ApiRequest
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return FormDataPart|null
     */
    public function getFormData(): ?FormDataPart
    {
        return $this->formData;
    }

    /**
     * @param FormDataPart|null $formData
     *
     * @return $this
     */
    public function setFormData(?FormDataPart $formData): ApiRequest
    {
        $this->formData = $formData;

        return $this;
    }
}
