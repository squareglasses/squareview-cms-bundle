<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Security\Resetting;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CheckEmailAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resetting/check-your-emails", name="app_resetting_check_email")
 */
class CheckEmailAction extends AbstractController
{
    /**
     * @param Request          $request
     * @param ConfigurationBag $configurationBag
     *
     * @return Response
     */
    public function __invoke(Request $request, ConfigurationBag $configurationBag): Response
    {
        $username = $request->query->get('username');

        if (empty($username)) {
            // the user does not come from the sendEmail action
            return new RedirectResponse($this->generateUrl('app_resetting_request'));
        }

        return $this->render("@SGCms/backend/security/resetting/check_email.html.twig", [
            'tokenLifetime' => ceil($configurationBag->getSecurityResettingRetryTtl() / 3600)
        ]);
    }
}
