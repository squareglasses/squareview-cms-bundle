<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Security\Resetting;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Backend\Security\Form\Type\ResettingFormType;
use SG\CmsBundle\Backend\Security\Model\UserInterface;
use SG\CmsBundle\Common\Form\Util\FormErrorParser;
use SG\CmsBundle\Backend\Exception\InvalidUserInputException;
use SG\CmsBundle\Backend\Security\Model\UserManager;
use SG\CmsBundle\Backend\Security\Util\TokenGeneratorInterface;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ResetAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resetting/new-password/{token}", name="app_resetting_reset")
 */
class ResetAction extends AbstractController
{
    /**
     * @param Request                     $request
     * @param string                      $token
     * @param UserManager                 $userManager
     * @param TokenGeneratorInterface     $tokenGenerator
     * @param EventDispatcherInterface    $eventDispatcher
     * @param SessionInterface            $session
     * @param UserPasswordHasherInterface $passwordEncoder
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws InvalidFormInputException
     */
    public function __invoke(
        Request $request,
        string $token,
        UserManager $userManager,
        TokenGeneratorInterface $tokenGenerator,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        UserPasswordHasherInterface $passwordEncoder
    ): Response {
        /** @var PasswordAuthenticatedUserInterface|UserInterface $user */
        $user = $userManager->findUserByConfirmationToken($token);

        if (null === $user) {
            $session->getFlashBag()->add('error', "Aucun utilisateur trouvé.");
            return $this->redirectToRoute("backend");
        }

        $form = $this->createForm(ResettingFormType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {
                $user->setPassword(
                    $passwordEncoder->hashPassword(
                        $user,
                        $form->get('plainPassword')->getData()
                    )
                );
                $userManager->resetPassword($user);

                return new RedirectResponse( $this->generateUrl('backend'));
            } catch (InvalidUserInputException $e) {
                FormErrorParser::parseFormErrors($form, $e);
            }
        }

        return $this->render("@SGCms/backend/security/resetting/reset.html.twig", [
            'token' => $token,
            'form' => $form->createView()
        ]);
    }
}
