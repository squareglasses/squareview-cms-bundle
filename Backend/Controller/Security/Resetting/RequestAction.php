<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Security\Resetting;

use DateTime;
use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Backend\Mailer\Mailer;
use SG\CmsBundle\Backend\Exception\InvalidFormInputException;
use SG\CmsBundle\Backend\Security\Model\UserManager;
use SG\CmsBundle\Backend\Security\Util\TokenGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class RequestAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resetting/request", name="app_resetting_request")
 */
class RequestAction extends AbstractController
{
    /**
     * @param Request                  $request
     * @param UserManager              $userManager
     * @param TokenGeneratorInterface  $tokenGenerator
     * @param Mailer                   $mailer
     * @param EventDispatcherInterface $eventDispatcher
     * @param SessionInterface         $session
     * @param ConfigurationBag         $configurationBag
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidFormInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws HttpExceptionInterface
     * @throws TransportExceptionInterface|\Symfony\Component\Mailer\Exception\TransportExceptionInterface
     * @throws \SG\CmsBundle\Frontend\Exception\InvalidFormInputException
     */
    public function __invoke(
        Request $request,
        UserManager $userManager,
        TokenGeneratorInterface $tokenGenerator,
        Mailer $mailer,
        EventDispatcherInterface $eventDispatcher,
        SessionInterface $session,
        ConfigurationBag $configurationBag
    ): Response {
        if ($request->getMethod() === "POST") {
            $username = $request->request->get('username');

            try {
                $user = $userManager->findUser($username);
                if (null === $user) {
                    $session->getFlashBag()->add('error', 'Aucun utilisateur ne correspond à cet identifiant');
                    return $this->redirectToRoute("app_resetting_request");
                }
            } catch (Exception) {
                $session->getFlashBag()->add('error', 'Aucun utilisateur ne correspond à cet identifiant');
                return $this->redirectToRoute("app_resetting_request");
            }

            if (null !== $user->getConfirmationToken() && $user->isPasswordRequestNonExpired(7200)) {
                $session->getFlashBag()->add('error', 'Vous avez déjà effectué une demande de renouvellement.');
                return $this->redirectToRoute("app_resetting_request");
            }

            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }
            $user->setPasswordRequestedAt(new DateTime());
            $userManager->updateUser($user, false, ['resetting-request']);

            $mailer->sendResettingEmailMessage($user);

            return new RedirectResponse($this->generateUrl('app_resetting_check_email', ['username' => $username]));
        }

        return $this->render("@SGCms/backend/security/resetting/request.html.twig");
    }
}
