<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use Exception;
use SG\CmsBundle\Backend\ConfigurationBuilder\RoutingBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class RoutingAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/routing",
 *     name="routing",
 *     methods={"GET"},
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Returns all available dynamic routes for the backend",
 * )
 * @OA\Tag(name="API")
 */
class RoutingAction extends AbstractController
{
    /**
     * @param RoutingBuilder $routingBuilder
     *
     * @return Response
     */
    public function __invoke(RoutingBuilder $routingBuilder): Response
    {
        try {
            $routing = $routingBuilder->build();
        } catch (Exception $e) {
            return new JsonResponse("Error building admin routing: " . $e->getMessage(), 400);
        }

        return new JsonResponse($routingBuilder->serialize($routing));
    }
}
