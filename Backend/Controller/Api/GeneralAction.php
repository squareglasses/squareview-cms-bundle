<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use Exception;
use SG\CmsBundle\Backend\ConfigurationBuilder\GeneralBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class GeneralAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/general",
 *     name="general",
 *     methods={"GET"},
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Returns the general configuration of the backend",
 * )
 * @OA\Tag(name="API")
 */
class GeneralAction extends AbstractController
{
    /**
     * @param GeneralBuilder $generalBuilder
     *
     * @return Response
     */
    public function __invoke(GeneralBuilder $generalBuilder): Response
    {
        try {
            $general = $generalBuilder->build();
        } catch (Exception $e) {
            return new JsonResponse([
                "error" => sprintf(
                    "Error building admin general - %s:%s %s",
                    $e->getFile(),
                    $e->getCode(),
                    $e->getMessage()
                ),
                "trace" => $e->getTrace()
            ], 400);
        }

        return new JsonResponse($generalBuilder->serialize($general));
    }
}
