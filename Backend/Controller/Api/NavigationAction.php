<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use Exception;
use SG\CmsBundle\Backend\ConfigurationBuilder\NavigationBuilder;
use SG\CmsBundle\Backend\ConfigurationBuilder\RoutingBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class NavigationAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/navigation",
 *     name="navigation",
 *     methods={"GET"},
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Returns the configuration of the backend menu",
 * )
 * @OA\Tag(name="API")
 */
class NavigationAction extends AbstractController
{
    /**
     * @param NavigationBuilder $navigationBuilder
     * @param RoutingBuilder    $routingBuilder
     *
     * @return Response
     */
    public function __invoke(NavigationBuilder $navigationBuilder, RoutingBuilder $routingBuilder): Response
    {
        try {
            $routing = $routingBuilder->build();
            $navigation = $navigationBuilder->build($routing);
        } catch (Exception $e) {
            return new JsonResponse("Error building admin navigation: " . $e->getMessage(), 400);
        }

        return new JsonResponse($navigationBuilder->serialize($navigation));
    }
}
