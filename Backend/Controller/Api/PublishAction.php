<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Backend\ApiClient\ApiClient;
use SG\CmsBundle\Backend\ApiClient\ApiRequestBuilder;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class PublishAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/publish/{locale}/{discriminator}/{id}",
 *     name="publish",
 *     methods={"PUT"},
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Call Squareview's Publish endpoint",
 * )
 * @OA\Tag(name="API")
 */
class PublishAction extends AbstractController
{
    /**
     * @param ApiRequestBuilder $apiRequestBuilder
     * @param ApiClient         $apiClient
     * @param Request           $request
     * @param string            $locale
     * @param string            $discriminator
     * @param string            $id
     *
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(
        ApiRequestBuilder $apiRequestBuilder,
        ApiClient $apiClient,
        Request $request,
        string $locale,
        string $discriminator,
        string $id
    ): JsonResponse {
        $apiRequest = $apiRequestBuilder->buildPublish($locale, $discriminator, $id);

        try {
            $response = $apiClient->request($apiRequest);

            return new JsonResponse($response->getContent(), $response->getStatusCode(), [], true);
        } catch (JsonException | InvalidArgumentException | InvalidFormInputException $e) {
            return new JsonResponse($e->getMessage(), 400);
        }
    }
}
