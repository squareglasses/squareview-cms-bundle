<?php
declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Backend\ApiClient\ApiClient;
use SG\CmsBundle\Backend\ApiClient\ApiRequestBuilder;
use SG\CmsBundle\Backend\ApiClient\Model\ApiRequest;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class CacheClearAction
 * @package SG\CmsBundle\Backend\Controller\Api
 * @author Rémi Komornicki <remi@squareglasses.com>
 * @Route(
 *     "/api/cache-clear",
 *     name="cache_clear",
 *     methods={"POST"},
 *     options={"expose"=true}
 * )
 * @OA\Response(
 *     response=200,
 *     description="Call Squareview's Cache Clear endpoint",
 * )
 * @OA\Tag(name="API")
 */
class CacheClearAction extends AbstractController
{
    /**
     * @param ApiClient $apiClient
     * @param Request $request
     * @return JsonResponse
     */
    public function __invoke(
        ApiClient $apiClient,
        Request $request
    ): JsonResponse
    {
        $apiRequest = (new ApiRequest())
            ->setMethod('POST')
            ->setEndpoint('/cache/clear-frontend');

        try {
            $response = $apiClient->request($apiRequest);

            return new JsonResponse($response->getContent(), $response->getStatusCode(), [], true);
        } catch (JsonException | InvalidArgumentException | InvalidFormInputException $e) {
            return new JsonResponse($e->getMessage(), 400);
        }
    }
}
