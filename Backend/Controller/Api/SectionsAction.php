<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use Exception;
use SG\CmsBundle\Backend\ConfigurationBuilder\SectionsBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class SectionsAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/sections",
 *     name="sections",
 *     methods={"GET"},
 *     options={"expose"=true}
 * )
 * @OA\Response(
 *     response=200,
 *     description="Returns the configuration of all the sections",
 * )
 * @OA\Tag(name="API")
 */
class SectionsAction extends AbstractController
{
    /**
     * @param SectionsBuilder $sectionsBuilder
     *
     * @return Response
     */
    public function __invoke(SectionsBuilder $sectionsBuilder): Response
    {
        try {
            $sections = $sectionsBuilder->build();
        } catch (Exception $e) {
            return new JsonResponse([
                "error" => sprintf(
                    "Error building admin sections - %s:%s %s",
                    $e->getFile(),
                    $e->getCode(),
                    $e->getMessage()
                ),
                "trace" => $e->getTrace()
            ], 400);
        }

        return new JsonResponse($sectionsBuilder->serialize($sections));
    }
}
