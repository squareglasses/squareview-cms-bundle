<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use Exception;
use SG\CmsBundle\Backend\ConfigurationBuilder\GuidesBuilder;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class GuidesAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/guides",
 *     name="guides",
 *     methods={"GET"},
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Returns all available frontend guides for the backend",
 * )
 * @OA\Tag(name="API")
 */
class GuidesAction extends AbstractController
{
    /**
     * @param GuidesBuilder $guidesBuilder
     * @return JsonResponse
     */
    public function __invoke(GuidesBuilder $guidesBuilder): JsonResponse
    {
        try {
            $guides = $guidesBuilder->build();
        } catch (Exception $e) {
            return new JsonResponse([
                "error" => sprintf(
                    "Error building guides: %s:%s - %s",
                    $e->getFile(),
                    $e->getLine(),
                    $e->getMessage()
                ),
                "trace" => $e->getTrace()
            ], 400);
        }

        return new JsonResponse($guidesBuilder->serialize($guides));
    }
}
