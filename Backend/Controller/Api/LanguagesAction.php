<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Backend\ApiClient\ApiClient;
use SG\CmsBundle\Backend\ApiClient\Model\ApiRequest;
use SG\CmsBundle\Backend\ConfigurationBuilder\LanguagesBuilder;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class LanguagesAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/languages",
 *     name="languages",
 *     methods={"GET"},
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Returns the list of languages of the website and the associated configuration",
 * )
 * @OA\Tag(name="API")
 */
class LanguagesAction extends AbstractController
{
    /**
     * @param LanguagesBuilder $languagesBuilder
     * @param ApiClient        $apiClient
     *
     * @return Response
     * @throws JsonException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(LanguagesBuilder $languagesBuilder, ApiClient $apiClient): Response
    {
        $apiRequest = (new ApiRequest())
            ->setEndpoint('languages')
            ->setMethod('GET')
        ;

        try {
            $response = $apiClient->request($apiRequest);
        } catch (JsonException | InvalidFormInputException | InvalidArgumentException $e) {
            return new JsonResponse("Error building admin languages: " . $e->getMessage(), 400);
        }

        $languages = $languagesBuilder->build($response->getContent());

        return new JsonResponse($languagesBuilder->serialize($languages));
    }
}
