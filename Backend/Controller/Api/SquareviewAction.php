<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller\Api;

use SG\CmsBundle\Backend\ApiClient\ApiClient;
use SG\CmsBundle\Backend\ApiClient\ApiRequestBuilder;
use Exception;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use SG\CmsBundle\Common\Contracts\HttpClientResponseInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class SquareviewAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 *
 * @Route(
 *     "/api/squareview/{apiReference}",
 *     name="squareview",
 *     options={"expose"=true}
 * )
 *
 * @OA\Response(
 *     response=200,
 *     description="Call Squareview's API",
 * )
 * @OA\Tag(name="API")
 */
class SquareviewAction extends AbstractController
{
    /**
     * @param ApiRequestBuilder $apiRequestBuilder
     * @param ApiClient         $apiClient
     * @param Request           $request
     * @param string            $apiReference
     *
     * @return JsonResponse
     * @throws ClientExceptionInterface
     * @throws InvalidArgumentException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     */
    public function __invoke(
        ApiRequestBuilder $apiRequestBuilder,
        ApiClient $apiClient,
        Request $request,
        string $apiReference,
    ): JsonResponse {
        $apiRequest = $apiRequestBuilder->build($apiReference, $request, $this->getUser());
        try {
            /* @var ResponseInterface $response */
            $response = $apiClient->request($apiRequest);
            $paginationHeaders = $this->getPaginationHeaders($response);

            return new JsonResponse($response->getContent(false), $response->getStatusCode(), $paginationHeaders, true);
        } catch (InvalidFormInputException $e) {
            return new JsonResponse(['message' => $e->getMessage(), 'violations' => $e->getViolations()], $e->getCode(), []);
        } catch (TransportExceptionInterface $e) {
            return new JsonResponse($e->getMessage(), 503, [], true);
        } catch (Exception $e) {
            return new JsonResponse(sprintf('"%s" in %s on line %s)', $e->getMessage(), $e->getFile(), $e->getLine()), 500, [], true);
        }
    }

    /**
     * @param HttpClientResponseInterface $response
     *
     * @return array
     */
    private function getPaginationHeaders(HttpClientResponseInterface $response): array
    {
        $paginationHeaders = [];
        foreach ($response->getHeaders() as $header => $value) {
            if (str_contains($header, 'pagination')) {
                $paginationHeaders[$header] = $value;
            }
        }

        return $paginationHeaders;
    }
}
