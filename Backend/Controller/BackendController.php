<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BackendController
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class BackendController extends AbstractController
{
    /**
     * @Route("/", name="backend")
     *
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('@SGCms/backend/index.html.twig');
    }
}
