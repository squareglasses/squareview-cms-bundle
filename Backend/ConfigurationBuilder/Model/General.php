<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class General
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class General
{
    private string $websiteIdentifier;
    private bool $multilingual = false;
    private string $title;
    private ?string $titleImage = null;
    private ?array $titleSplit = null;
    private ?string $homeImage = null;
    private bool $cacheClearable = true;
    private bool $errorReport = true;
    private ?string $squareviewVersion = null;

    /** @var array<string,MetaTag>|null */
    private ?array $metaTags = null;

    /** @var array<string,LayoutTranslation>|null */
    private ?array $layoutTranslations = null;

    /**
     * @return string
     */
    public function getWebsiteIdentifier(): string
    {
        return $this->websiteIdentifier;
    }

    /**
     * @param string $websiteIdentifier
     *
     * @return General
     */
    public function setWebsiteIdentifier(string $websiteIdentifier): General
    {
        $this->websiteIdentifier = $websiteIdentifier;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMultilingual(): bool
    {
        return $this->multilingual;
    }

    /**
     * @param bool $multilingual
     *
     * @return General
     */
    public function setMultilingual(bool $multilingual): General
    {
        $this->multilingual = $multilingual;

        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return General
     */
    public function setTitle(string $title): General
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitleImage(): ?string
    {
        return $this->titleImage;
    }

    /**
     * @param string|null $titleImage
     *
     * @return General
     */
    public function setTitleImage(?string $titleImage): General
    {
        $this->titleImage = $titleImage;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getTitleSplit(): ?array
    {
        return $this->titleSplit;
    }

    /**
     * @param array|null $titleSplit
     *
     * @return General
     */
    public function setTitleSplit(?array $titleSplit): General
    {
        $this->titleSplit = $titleSplit;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getHomeImage(): ?string
    {
        return $this->homeImage;
    }

    /**
     * @param string|null $homeImage
     *
     * @return General
     */
    public function setHomeImage(?string $homeImage): General
    {
        $this->homeImage = $homeImage;

        return $this;
    }

    /**
     * @return bool
     */
    public function isCacheClearable(): bool
    {
        return $this->cacheClearable;
    }

    /**
     * @param bool $cacheClearable
     * @return General
     */
    public function setCacheClearable(bool $cacheClearable): General
    {
        $this->cacheClearable = $cacheClearable;
        return $this;
    }

    /**
     * @return bool
     */
    public function isErrorReport(): bool
    {
        return $this->errorReport;
    }

    /**
     * @param bool $errorReport
     * @return General
     */
    public function setErrorReport(bool $errorReport): General
    {
        $this->errorReport = $errorReport;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSquareviewVersion(): ?string
    {
        return $this->squareviewVersion;
    }

    /**
     * @param string|null $squareviewVersion
     *
     * @return General
     */
    public function setSquareviewVersion(?string $squareviewVersion): General
    {
        $this->squareviewVersion = $squareviewVersion;

        return $this;
    }

    /**
     * @return MetaTag[]|null
     */
    public function getMetaTags(): ?array
    {
        return $this->metaTags;
    }

    /**
     * @param MetaTag $metaTag
     *
     * @return General
     */
    public function addMetaTag(MetaTag $metaTag): General
    {
        $this->metaTags[] = $metaTag;

        return $this;
    }

    /**
     * @param MetaTag[]|null $metaTags
     *
     * @return General
     */
    public function setMetaTags(?array $metaTags): General
    {
        $this->metaTags = $metaTags;

        return $this;
    }

    /**
     * @return LayoutTranslation[]|null
     */
    public function getLayoutTranslations(): ?array
    {
        return $this->layoutTranslations;
    }

    /**
     * @param LayoutTranslation $layoutTranslation
     *
     * @return General
     */
    public function addLayoutTranslation(LayoutTranslation $layoutTranslation): General
    {
        $this->layoutTranslations[] = $layoutTranslation;

        return $this;
    }
}
