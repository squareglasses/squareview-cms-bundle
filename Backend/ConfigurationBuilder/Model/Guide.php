<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionInterface;

/**
 * Class Guide
 * Overrides Section configuration if element has a guide
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Guide
{
    private string $name;
    private string $section;

    /** @var array<string,ActionInterface>|null */
    private ?array $actions = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Guide
     */
    public function setName(string $name): Guide
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @param string $section
     *
     * @return Guide
     */
    public function setSection(string $section): Guide
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return array<string,ActionInterface>|null
     */
    public function getActions(): ?array
    {
        return $this->actions;
    }

    /**
     * @param Action $action
     *
     * @return Guide
     */
    public function addAction(Action $action): Guide
    {
        $this->actions[] = $action;

        return $this;
    }

    /**
     * @param array<string,ActionInterface>|null $actions
     *
     * @return Guide
     */
    public function setActions(?array $actions): Guide
    {
        $this->actions = $actions;

        return $this;
    }
}
