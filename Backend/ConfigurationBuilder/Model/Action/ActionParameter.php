<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;

class ActionParameter
{
    const TYPE_STATIC = 'static';
    const TYPE_DYNAMIC = 'dynamic';

    protected string $name;
    protected string $type; //static or dynamic
    protected string $value;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ActionParameter
     */
    public function setName(string $name): ActionParameter
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ActionParameter
     */
    public function setType(string $type): ActionParameter
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }

    /**
     * @param string $value
     * @return ActionParameter
     */
    public function setValue(string $value): ActionParameter
    {
        $this->value = $value;
        return $this;
    }
}
