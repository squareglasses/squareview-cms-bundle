<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\NewAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;

/**
 * Class NewAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class NewAction extends Action
{
    private string $generateSlugFrom;
    private bool $editAfterCreation = false;

    /**
     * @return string
     */
    public function getGenerateSlugFrom(): string
    {
        return $this->generateSlugFrom;
    }

    /**
     * @param string $generateSlugFrom
     *
     * @return NewAction
     */
    public function setGenerateSlugFrom(string $generateSlugFrom): NewAction
    {
        $this->generateSlugFrom = $generateSlugFrom;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEditAfterCreation(): bool
    {
        return $this->editAfterCreation;
    }

    /**
     * @param bool $editAfterCreation
     *
     * @return NewAction
     */
    public function setEditAfterCreation(bool $editAfterCreation): NewAction
    {
        $this->editAfterCreation = $editAfterCreation;

        return $this;
    }
}
