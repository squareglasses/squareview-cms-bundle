<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;


interface HasActionParametersInterface
{
    public function getParameters(): ?array;
    public function setParameters(?array $parameters): self;
    public function addParameter(ActionParameter $parameter): self;
}
