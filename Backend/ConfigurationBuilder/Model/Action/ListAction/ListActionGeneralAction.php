<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction;

/**
 * Class ListActionGeneralAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ListActionGeneralAction
{
    private string $name;
    private ?string $path;
    private ?string $label = null;
    private ?array $pathParameters = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ListActionGeneralAction
     */
    public function setName(string $name): ListActionGeneralAction
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     *
     * @return ListActionGeneralAction
     */
    public function setPath(?string $path): ListActionGeneralAction
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return ListActionGeneralAction
     */
    public function setLabel(?string $label): ListActionGeneralAction
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getPathParameters(): ?array
    {
        return $this->pathParameters;
    }

    /**
     * @param array|null $pathParameters
     *
     * @return $this
     */
    public function setPathParameters(?array $pathParameters): ListActionGeneralAction
    {
        $this->pathParameters = $pathParameters;

        return $this;
    }
}
