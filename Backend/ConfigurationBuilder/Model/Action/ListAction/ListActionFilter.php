<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction;

/**
 * Class ListActionFilter
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ListActionFilter
{
    private string $name;
    private string $type;
    private string $label;
    private string $strategy;
    private ?array $options = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ListActionFilter
     */
    public function setName(string $name): ListActionFilter
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     * @return ListActionFilter
     */
    public function setType(string $type): ListActionFilter
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     * @return ListActionFilter
     */
    public function setLabel(string $label): ListActionFilter
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getStrategy(): string
    {
        return $this->strategy;
    }

    /**
     * @param string $strategy
     * @return ListActionFilter
     */
    public function setStrategy(string $strategy): ListActionFilter
    {
        $this->strategy = $strategy;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): ListActionFilter
    {
        $this->options = $options;

        return $this;
    }
}
