<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction;

/**
 * Class ListActionColumn
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ListActionColumn
{
    private string $name;
    private ?string $type = null;
    private ?string $label = null;
    private ?string $description = null;
    private ?bool $translated = null;
    private ?ListActionColumn $mergedColumnAsLabel = null;
    private ?string $fallback = null;
    private ?bool $clickable = null;
    private ?bool $orderable = null;
    private ?array $options = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ListActionColumn
     */
    public function setName(string $name): ListActionColumn
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return ListActionColumn
     */
    public function setType(?string $type): ListActionColumn
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return ListActionColumn
     */
    public function setLabel(?string $label): ListActionColumn
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getTranslated(): ?bool
    {
        return $this->translated;
    }

    /**
     * @param bool|null $translated
     *
     * @return ListActionColumn
     */
    public function setTranslated(?bool $translated): ListActionColumn
    {
        $this->translated = $translated;

        return $this;
    }

    /**
     * @return ListActionColumn|null
     */
    public function getMergedColumnAsLabel(): ?ListActionColumn
    {
        return $this->mergedColumnAsLabel;
    }

    /**
     * @param ListActionColumn|null $mergedColumnAsLabel
     *
     * @return ListActionColumn
     */
    public function setMergedColumnAsLabel(?ListActionColumn $mergedColumnAsLabel): ListActionColumn
    {
        $this->mergedColumnAsLabel = $mergedColumnAsLabel;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFallback(): ?string
    {
        return $this->fallback;
    }

    /**
     * @param string|null $fallback
     *
     * @return ListActionColumn
     */
    public function setFallback(?string $fallback): ListActionColumn
    {
        $this->fallback = $fallback;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getClickable(): ?bool
    {
        return $this->clickable;
    }

    /**
     * @param bool|null $clickable
     *
     * @return ListActionColumn
     */
    public function setClickable(?bool $clickable): ListActionColumn
    {
        $this->clickable = $clickable;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getOrderable(): ?bool
    {
        return $this->orderable;
    }

    /**
     * @param bool|null $orderable
     */
    public function setOrderable(?bool $orderable): void
    {
        $this->orderable = $orderable;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return ListActionColumn
     */
    public function setOptions(?array $options): ListActionColumn
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): ListActionColumn
    {
        $this->description = $description;
        return $this;
    }
}
