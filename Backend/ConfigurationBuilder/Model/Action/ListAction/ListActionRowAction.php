<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction;

/**
 * Class ListActionRowAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ListActionRowAction
{
    private string $name;
    private string $type;
    private ?string $path = null;
    private ?string $label = null;
    private ?string $count = null;
    private ?string $childrenLabel = null;
    private ?array $pathParameters = null;
    private ?array $options = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ListActionRowAction
     */
    public function setName(string $name): ListActionRowAction
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     *
     * @return ListActionRowAction
     */
    public function setPath(?string $path): ListActionRowAction
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return ListActionRowAction
     */
    public function setLabel(?string $label): ListActionRowAction
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCount(): ?string
    {
        return $this->count;
    }

    /**
     * @param string|null $count
     *
     * @return ListActionRowAction
     */
    public function setCount(?string $count): ListActionRowAction
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getChildrenLabel(): ?string
    {
        return $this->childrenLabel;
    }

    /**
     * @param string|null $childrenLabel
     *
     * @return $this
     */
    public function setChildrenLabel(?string $childrenLabel): ListActionRowAction
    {
        $this->childrenLabel = $childrenLabel;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getPathParameters(): ?array
    {
        return $this->pathParameters;
    }

    /**
     * @param array|null $pathParameters
     *
     * @return $this
     */
    public function setPathParameters(?array $pathParameters): ListActionRowAction
    {
        $this->pathParameters = $pathParameters;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): ListActionRowAction
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): ListActionRowAction
    {
        $this->type = $type;

        return $this;
    }
}
