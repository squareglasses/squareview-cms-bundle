<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;

/**
 * Class ListAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ListAction extends Action
{
    private string $title;
    private string $enabledProperty = 'enabled';
    private ?string $mainRowAction = null;
    private string $style = 'normal';
    private string $optionsPosition = 'right';

    private ?string $defaultOrder = null;
    private ?array $order = null;

    /* @var array<string,ListActionColumn>|null */
    private ?array $columns = null;

    /* @var array<string,ListActionFilter>|null */
    private ?array $filters = null;

    /* @var array<string,ListActionGeneralAction>|null */
    private ?array $generalActions = null;

    /* @var array<string,ListActionRowAction>|null */
    private ?array $rowActions = null;

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): ListAction
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getEnabledProperty(): string
    {
        return $this->enabledProperty;
    }

    /**
     * @param string $enabledProperty
     *
     * @return $this
     */
    public function setEnabledProperty(string $enabledProperty): ListAction
    {
        $this->enabledProperty = $enabledProperty;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getMainRowAction(): ?string
    {
        return $this->mainRowAction;
    }

    /**
     * @param string|null $mainRowAction
     *
     * @return $this
     */
    public function setMainRowAction(?string $mainRowAction): ListAction
    {
        $this->mainRowAction = $mainRowAction;

        return $this;
    }

    /**
     * @return string
     */
    public function getStyle(): string
    {
        return $this->style;
    }

    /**
     * @param string $style
     *
     * @return $this
     */
    public function setStyle(string $style): ListAction
    {
        $this->style = $style;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultOrder(): ?string
    {
        return $this->defaultOrder;
    }

    /**
     * @param string|null $defaultOrder
     *
     * @return $this
     */
    public function setDefaultOrder(?string $defaultOrder): ListAction
    {
        $this->defaultOrder = $defaultOrder;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOrder(): ?array
    {
        return $this->order;
    }

    /**
     * @param array|null $order
     *
     * @return $this
     */
    public function setOrder(?array $order): ListAction
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return array<string,ListActionColumn>|null
     */
    public function getColumns(): ?array
    {
        return $this->columns;
    }

    /**
     * @param ListActionColumn $column
     *
     * @return $this
     */
    public function addColumn(ListActionColumn $column): ListAction
    {
        $this->columns[] = $column;

        return $this;
    }

    /**
     * @param ListActionColumn[]|null $columns
     *
     * @return ListAction
     */
    public function setColumns(?array $columns): ListAction
    {
        $this->columns = $columns;

        return $this;
    }

    /**
     * @return array<string,ListActionFilter>|null
     */
    public function getFilters(): ?array
    {
        return $this->filters;
    }

    /**
     * @param ListActionFilter $filter
     *
     * @return $this
     */
    public function addFilter(ListActionFilter $filter): ListAction
    {
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * @param ListActionFilter[]|null $filters
     *
     * @return ListAction
     */
    public function setFilters(?array $filters): ListAction
    {
        $this->filters = $filters;

        return $this;
    }

    /**
     * @return array<string,ListActionGeneralAction>|null
     */
    public function getGeneralActions(): ?array
    {
        return $this->generalActions;
    }

    /**
     * @param ListActionGeneralAction $generalAction
     *
     * @return ListAction
     */
    public function addGeneralAction(ListActionGeneralAction $generalAction): ListAction
    {
        $this->generalActions[] = $generalAction;

        return $this;
    }

    /**
     * @param ListActionGeneralAction[]|null $generalActions
     *
     * @return ListAction
     */
    public function setGeneralActions(?array $generalActions): ListAction
    {
        $this->generalActions = $generalActions;

        return $this;
    }

    /**
     * @return array<string,ListActionRowAction>|null
     */
    public function getRowActions(): ?array
    {
        return $this->rowActions;
    }

    /**
     * @param ListActionRowAction $rowAction
     *
     * @return ListAction
     */
    public function addRowAction(ListActionRowAction $rowAction): ListAction
    {
        $this->rowActions[] = $rowAction;

        return $this;
    }

    /**
     * @param ListActionRowAction[]|null $rowActions
     *
     * @return ListAction
     */
    public function setRowActions(?array $rowActions): ListAction
    {
        $this->rowActions = $rowActions;

        return $this;
    }

    public function getOptionsPosition(): string
    {
        return $this->optionsPosition;
    }

    /**
     * @param string $optionsPosition
     *
     * @return $this
     */
    public function setOptionsPosition(string $optionsPosition): ListAction
    {
        $this->optionsPosition = $optionsPosition;

        return $this;
    }
}
