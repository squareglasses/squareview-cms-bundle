<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\DeleteAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;

/**
 * Class DeleteAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class DeleteAction extends Action
{
}
