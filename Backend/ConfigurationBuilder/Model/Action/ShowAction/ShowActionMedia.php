<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction;

/**
 * Class ShowActionMedia
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowActionMedia
{
    private string $identifier;
    private ?string $label = null;
    private ?string $provider = null;
    private ?string $format = null;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): ShowActionMedia
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return $this
     */
    public function setLabel(?string $label): ShowActionMedia
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvider(): ?string
    {
        return $this->provider;
    }

    /**
     * @param string|null $provider
     *
     * @return $this
     */
    public function setProvider(?string $provider): ShowActionMedia
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     *
     * @return $this
     */
    public function setFormat(?string $format): ShowActionMedia
    {
        $this->format = $format;

        return $this;
    }
}
