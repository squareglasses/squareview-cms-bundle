<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionApi;
use SG\CmsBundle\Backend\Exception\ConfigurationException;

/**
 * Class ShowActionZone
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowActionZone
{
    public const ZONE_TYPE_PROPERTIES = 'properties';
    public const ZONE_TYPE_COLLECTION = 'collection';
    //TODO later?
    //public const ZONE_TYPE_MEDIAS = 'medias';
    //public const ZONE_TYPE_TRANSLATIONS = 'translations';
    //public const ZONE_TYPE_GALLERY = 'gallery';


    public const ZONE_TYPES = [
        self::ZONE_TYPE_PROPERTIES,
        self::ZONE_TYPE_COLLECTION,
        //self::ZONE_TYPE_MEDIAS,
        //self::ZONE_TYPE_TRANSLATIONS,
        //self::ZONE_TYPE_GALLERY,
    ];

    private string $name;
    private ?string $label = null;
    private ?string $type = null;
    private int $position;
    private ?string $api = null;
    private ?array $options = null;

    /* @var array<string,ShowActionField>|null */
    private ?array $fields = null;

//    /* @var array<string,ShowActionMedia>|null */
//    private ?array $medias = null;
//
//    private ?ShowActionGallery $gallery = null;


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): ShowActionZone
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return $this
     */
    public function setLabel(?string $label): ShowActionZone
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): ShowActionZone
    {
        if ($type !== null && !in_array($type, self::ZONE_TYPES)) {
            throw new ConfigurationException('Zone type '.$type.' is not implemented. Available types are: '.implode(', ', self::ZONE_TYPES).'.');
        }
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }


    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition(int $position): ShowActionZone
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getApi(): ?string
    {
        return $this->api;
    }

    /**
     * @param string|null $api
     *
     * @return $this
     */
    public function setApi(?string $api): ShowActionZone
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @return array<string,ShowActionField>|null
     */
    public function getFields(): ?array
    {
        return $this->fields;
    }

    /**
     * @param ShowActionField $field
     *
     * @return $this
     */
    public function addField(ShowActionField $field): ShowActionZone
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * @param ShowActionField[] $fields
     *
     * @return ShowActionZone
     */
    public function setFields(?array $fields): ShowActionZone
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): ShowActionZone
    {
        $this->options = $options;

        return $this;
    }
}
