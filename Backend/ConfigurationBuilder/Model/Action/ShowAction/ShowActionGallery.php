<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction;

/**
 * Class ShowActionGallery
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowActionGallery
{
    private string $identifier;
    private string $thumbnailFormat;
    private string $fullSizeFormat;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): ShowActionGallery
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getThumbnailFormat(): string
    {
        return $this->thumbnailFormat;
    }

    /**
     * @param string $thumbnailFormat
     *
     * @return $this
     */
    public function setThumbnailFormat(string $thumbnailFormat): ShowActionGallery
    {
        $this->thumbnailFormat = $thumbnailFormat;

        return $this;
    }

    /**
     * @return string
     */
    public function getFullSizeFormat(): string
    {
        return $this->fullSizeFormat;
    }

    /**
     * @param string $fullSizeFormat
     *
     * @return $this
     */
    public function setFullSizeFormat(string $fullSizeFormat): ShowActionGallery
    {
        $this->fullSizeFormat = $fullSizeFormat;

        return $this;
    }
}
