<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;

/**
 * Class ShowAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowAction extends Action
{
    private string $title;
    private string $mainTitleProperty = 'name';

    /* @var array<string,ShowActionZone>|null */
    private ?array $zones = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): ShowAction
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainTitleProperty(): string
    {
        return $this->mainTitleProperty;
    }

    /**
     * @param string $mainTitleProperty
     *
     * @return $this
     */
    public function setMainTitleProperty(string $mainTitleProperty): ShowAction
    {
        $this->mainTitleProperty = $mainTitleProperty;

        return $this;
    }

    /**
     * @return array<string,ShowActionZone>|null
     */
    public function getZones(): ?array
    {
        return $this->zones;
    }

    /**
     * @param ShowActionZone $zone
     *
     * @return $this
     */
    public function addZone(ShowActionZone $zone): ShowAction
    {
        $this->zones[] = $zone;

        return $this;
    }

    /**
     * @param array|null $zones
     *
     * @return $this
     */
    public function setZones(?array $zones): ShowAction
    {
        $this->zones = $zones;

        return $this;
    }
}
