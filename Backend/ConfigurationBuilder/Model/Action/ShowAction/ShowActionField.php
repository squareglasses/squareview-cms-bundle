<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\HasActionParametersInterface;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\HasActionParametersTrait;

/**
 * Class ShowActionField
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowActionField implements HasActionParametersInterface
{
    use HasActionParametersTrait;

    private string $identifier;
    private ?string $label = null;
    private ?string $description = null;
    private ?string $type = null;
    private ?array $options = null;
    private ?string $api = null;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): ShowActionField
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return $this
     */
    public function setLabel(?string $label): ShowActionField
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): ShowActionField
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): ShowActionField
    {
        $this->options = $options;

        return $this;
    }
    /**
     * @return string|null
     */
    public function getApi(): ?string
    {
        return $this->api;
    }

    /**
     * @param string|null $api
     *
     * @return $this
     */
    public function setApi(?string $api): ShowActionField
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): ShowActionField
    {
        $this->description = $description;
        return $this;
    }
}
