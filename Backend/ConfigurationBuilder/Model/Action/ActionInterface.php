<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;

/**
 * Interface ActionInterface
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
interface ActionInterface
{
    /**
     * @return string
     */
    public function getPath(): string;

    /**
     * @param string $path
     *
     * @return Action
     */
    public function setPath(string $path): Action;
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @param string $name
     *
     * @return Action
     */
    public function setName(string $name): Action;

    /**
     * @return string
     */
    public function getType(): string;

    /**
     * @param string $type
     *
     * @return Action
     */
    public function setType(string $type): Action;
}
