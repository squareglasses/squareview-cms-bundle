<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;

/**
 * Class BackAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class BackAction
{
    private ?string $name = null;
    private ?array $pathParameters = null;

    /**
     * @param string $name
     * @param array|null $pathParameters
     */
    public function __construct(?string $name = null, ?array $path_parameters = [])
    {
        $this->name = $name;
        $this->pathParameters = $path_parameters;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): BackAction
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getPathParameters(): ?array
    {
        return $this->pathParameters;
    }

    /**
     * @param array|null $pathParameters
     *
     * @return $this
     */
    public function setPathParameters(?array $pathParameters): BackAction
    {
        $this->pathParameters = $pathParameters;

        return $this;
    }
}
