<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;

/**
 * Class Action
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
abstract class Action implements ActionInterface
{
    public const TYPE_EDIT       = 'edit';
    public const TYPE_LIST       = 'list';
    public const TYPE_NEW        = 'new';
    public const TYPE_SHOW       = 'show';
    public const TYPE_DELETE     = 'delete';
    public const TYPE_CUSTOM     = 'custom';

    public const TYPES = [
        self::TYPE_EDIT,
        self::TYPE_LIST,
        self::TYPE_NEW,
        self::TYPE_SHOW,
        self::TYPE_DELETE,
        self::TYPE_CUSTOM,
    ];

    protected string $section;
    protected string $path;
    protected string $name;
    protected string $type;
    protected ?BackAction $backAction = null;

    /* @var array<ActionApi> */
    protected array $apis;

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @param string $section
     *
     * @return Action
     */
    public function setSection(string $section): Action
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return Action
     */
    public function setPath(string $path): Action
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Action
     */
    public function setName(string $name): Action
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Action
     */
    public function setType(string $type): Action
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array<string,ActionApi>
     */
    public function getApis(): array
    {
        return $this->apis;
    }

    /**
     * @param ActionApi $api
     *
     * @return $this
     */
    public function addApi(ActionApi $api): Action
    {
        $this->apis[] = $api;

        return $this;
    }

    /**
     * @param ActionApi[] $apis
     *
     * @return Action
     */
    public function setApis(array $apis): Action
    {
        $this->apis = $apis;

        return $this;
    }

    /**
     * @return BackAction
     */
    public function getBackAction(): ?BackAction
    {
        return $this->backAction;
    }

    /**
     * @param BackAction $backAction
     *
     * @return Action
     */
    public function setBackAction(array $backAction): Action
    {
        $this->backAction = new BackAction(...$backAction);

        return $this;
    }
}
