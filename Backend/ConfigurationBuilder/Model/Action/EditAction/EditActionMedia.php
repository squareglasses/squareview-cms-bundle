<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction;

/**
 * Class EditActionMedia
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class EditActionMedia
{
    private string $identifier;
    private ?string $label = null;
    private ?string $provider = null;
    private ?string $format = null;
    private ?bool $required = null;
    private ?string $description = null;
    private ?string $dimensions = null;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): EditActionMedia
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return $this
     */
    public function setLabel(?string $label): EditActionMedia
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvider(): ?string
    {
        return $this->provider;
    }

    /**
     * @param string|null $provider
     *
     * @return $this
     */
    public function setProvider(?string $provider): EditActionMedia
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     *
     * @return $this
     */
    public function setFormat(?string $format): EditActionMedia
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRequired(): ?bool
    {
        return $this->required;
    }

    /**
     * @param bool|null $required
     *
     * @return $this
     */
    public function setRequired(?bool $required): EditActionMedia
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): EditActionMedia
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDimensions(): ?string
    {
        return $this->dimensions;
    }

    /**
     * @param string|null $dimensions
     *
     * @return $this
     */
    public function setDimensions(?string $dimensions): EditActionMedia
    {
        $this->dimensions = $dimensions;

        return $this;
    }
}
