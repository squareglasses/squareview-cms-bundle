<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\HasActionParametersInterface;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\HasActionParametersTrait;

/**
 * Class EditActionField
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class EditActionField implements HasActionParametersInterface
{
    use HasActionParametersTrait;

    private string $identifier;
    private ?string $label = null;
    private ?string $description = null;
    private ?string $type = null;
    private ?bool $required = null;
    private ?bool $readonly = null;
    private ?array $options = null;
    private ?string $api = null;
    private ?string $defaultValue = null;

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): EditActionField
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return $this
     */
    public function setLabel(?string $label): EditActionField
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): EditActionField
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getRequired(): ?bool
    {
        return $this->required;
    }

    /**
     * @param bool|null $required
     *
     * @return $this
     */
    public function setRequired(?bool $required): EditActionField
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return bool|null
     */
    public function getReadonly(): ?bool
    {
        return $this->readonly;
    }

    /**
     * @param bool|null $readonly
     *
     * @return $this
     */
    public function setReadonly(?bool $readonly): EditActionField
    {
        $this->readonly = $readonly;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): EditActionField
    {
        $this->options = $options;

        return $this;
    }
    /**
     * @return string|null
     */
    public function getApi(): ?string
    {
        return $this->api;
    }

    /**
     * @param string|null $api
     *
     * @return $this
     */
    public function setApi(?string $api): EditActionField
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     * @return $this
     */
    public function setDescription(?string $description): EditActionField
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @param string|null $defaultValue
     *
     * @return EditActionField
     */
    public function setDefaultValue(?string $defaultValue): EditActionField
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }
}
