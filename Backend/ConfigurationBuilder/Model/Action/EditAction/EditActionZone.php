<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionApi;
use SG\CmsBundle\Backend\Exception\ConfigurationException;

/**
 * Class EditActionZone
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class EditActionZone
{
    public const ZONE_TYPE_PROPERTIES = 'properties';
    public const ZONE_TYPE_MODULES = 'modules';
    public const ZONE_TYPE_MEDIAS = 'medias';
    public const ZONE_TYPE_TRANSLATIONS = 'translations';
    public const ZONE_TYPE_GALLERY = 'gallery';
    public const ZONE_TYPE_COLLECTION = 'collection';

    public const ZONE_TYPES = [
        self::ZONE_TYPE_PROPERTIES,
        self::ZONE_TYPE_MODULES,
        self::ZONE_TYPE_MEDIAS,
        self::ZONE_TYPE_TRANSLATIONS,
        self::ZONE_TYPE_GALLERY,
        self::ZONE_TYPE_COLLECTION
    ];

    private string $name;
    private ?string $label = null;
    private ?string $type = null;
    private int $position;
    private ?string $api = null;

    private bool $expanded = true;

    /* @var array<string,EditActionField>|null */
    private ?array $fields = null;

    /* @var array<string,EditActionMedia>|null */
    private ?array $medias = null;

    private ?EditActionGallery $gallery = null;
    private ?array $options = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): EditActionZone
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return $this
     */
    public function setLabel(?string $label): EditActionZone
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return $this
     */
    public function setType(?string $type): EditActionZone
    {
        if ($type !== null && !in_array($type, self::ZONE_TYPES)) {
            throw new ConfigurationException('Zone type '.$type.' is not implemented. Available types are: '.implode(', ', self::ZONE_TYPES).'.');
        }
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }


    /**
     * @param int $position
     *
     * @return $this
     */
    public function setPosition(int $position): EditActionZone
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @param bool $expanded
     *
     * @return $this
     */
    public function setExpanded(bool $expanded): EditActionZone
    {
        $this->expanded = $expanded;

        return $this;
    }

    /**
     * @return bool
     */
    public function getExpanded(): bool
    {
        return $this->expanded;
    }

    /**
     * @return string|null
     */
    public function getApi(): ?string
    {
        return $this->api;
    }

    /**
     * @param string|null $api
     *
     * @return $this
     */
    public function setApi(?string $api): EditActionZone
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @return array<string,EditActionField>|null
     */
    public function getFields(): ?array
    {
        return $this->fields;
    }

    /**
     * @param EditActionField $field
     *
     * @return $this
     */
    public function addField(EditActionField $field): EditActionZone
    {
        $this->fields[] = $field;

        return $this;
    }

    /**
     * @param EditActionField[] $fields
     *
     * @return EditActionZone
     */
    public function setFields(?array $fields): EditActionZone
    {
        $this->fields = $fields;

        return $this;
    }

    /**
     * @return array<string,EditActionMedia>|null
     */
    public function getMedias(): ?array
    {
        return $this->medias;
    }

    /**
     * @param EditActionMedia $media
     *
     * @return $this
     */
    public function addMedia(EditActionMedia $media): EditActionZone
    {
        $this->medias[] = $media;

        return $this;
    }

    /**
     * @param EditActionMedia[] $medias
     *
     * @return EditActionZone
     */
    public function setMedias(?array $medias): EditActionZone
    {
        $this->medias = $medias;

        return $this;
    }

    /**
     * @return EditActionGallery|null
     */
    public function getGallery(): ?EditActionGallery
    {
        return $this->gallery;
    }

    /**
     * @param EditActionGallery|null $gallery
     *
     * @return $this
     */
    public function setGallery(?EditActionGallery $gallery): EditActionZone
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): EditActionZone
    {
        $this->options = $options;

        return $this;
    }
}
