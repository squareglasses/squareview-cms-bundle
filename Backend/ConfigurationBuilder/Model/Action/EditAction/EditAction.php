<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;

/**
 * Class EditAction
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class EditAction extends Action
{
    private string $title;
    private string $mainTitleProperty = 'name';
    private bool $mainTitleReadonly = false;
    private bool $mainTitleRequired = true;
    private bool $mainTitleVisible = true;
    private ?string $enabledProperty = 'enabled';
    private ?array $enabledLabels = null;

    /* @var array<string,EditActionZone>|null */
    private ?array $zones = [];

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title): EditAction
    {
        $this->title = $title;

        return $this;
    }

    /**
     * @return string
     */
    public function getMainTitleProperty(): string
    {
        return $this->mainTitleProperty;
    }

    /**
     * @param string $mainTitleProperty
     *
     * @return $this
     */
    public function setMainTitleProperty(string $mainTitleProperty): EditAction
    {
        $this->mainTitleProperty = $mainTitleProperty;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMainTitleReadonly(): bool
    {
        return $this->mainTitleReadonly;
    }

    /**
     * @param bool $mainTitleReadonly
     *
     * @return $this
     */
    public function setMainTitleReadonly(bool $mainTitleReadonly): EditAction
    {
        $this->mainTitleReadonly = $mainTitleReadonly;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMainTitleRequired(): bool
    {
        return $this->mainTitleRequired;
    }

    /**
     * @param bool $mainTitleRequired
     *
     * @return $this
     */
    public function setMainTitleRequired(bool $mainTitleRequired): EditAction
    {
        $this->mainTitleRequired = $mainTitleRequired;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMainTitleVisible(): bool
    {
        return $this->mainTitleVisible;
    }

    /**
     * @param bool $mainTitleVisible
     *
     * @return $this
     */
    public function setMainTitleVisible(bool $mainTitleVisible): EditAction
    {
        $this->mainTitleVisible = $mainTitleVisible;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getEnabledProperty(): ?string
    {
        return $this->enabledProperty;
    }

    /**
     * @param string|null $enabledProperty
     *
     * @return $this
     */
    public function setEnabledProperty(?string $enabledProperty): EditAction
    {
        $this->enabledProperty = $enabledProperty;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getEnabledLabels(): ?array
    {
        return $this->enabledLabels;
    }

    /**
     * @param array|null $enabledLabels
     *
     * @return $this
     */
    public function setEnabledLabels(?array $enabledLabels): EditAction
    {
        $this->enabledLabels = $enabledLabels;

        return $this;
    }

    /**
     * @return array<string,EditActionZone>|null
     */
    public function getZones(): ?array
    {
        return $this->zones;
    }

    /**
     * @param EditActionZone $zone
     *
     * @return $this
     */
    public function addZone(EditActionZone $zone): EditAction
    {
        $this->zones[] = $zone;

        return $this;
    }

    /**
     * @param array|null $zones
     *
     * @return $this
     */
    public function setZones(?array $zones): EditAction
    {
        $this->zones = $zones;

        return $this;
    }
}
