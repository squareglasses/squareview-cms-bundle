<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\CustomAction;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;

/**
 * Class CustomAction
 *
 * @author Marie-Lou PERREAU <marielou@squareglasses.com>
 */
class CustomAction extends Action
{
}
