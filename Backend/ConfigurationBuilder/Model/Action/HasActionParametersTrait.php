<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;

trait HasActionParametersTrait
{
    /* @var array<string,ActionParameter>|null */
    private ?array $parameters = null;

    /**
     * @return ActionParameter[]|null
     */
    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    /**
     * @param ActionParameter[]|null $parameters
     * @return self
     */
    public function setParameters(?array $parameters): self
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @param ActionParameter|null $parameter
     * @return self
     */
    public function addParameter(ActionParameter $parameter): self
    {
        $this->parameters[] = $parameter;
        return $this;
    }
}
