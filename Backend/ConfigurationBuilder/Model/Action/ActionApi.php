<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action;

/**
 * Class ActionApi
 *
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
class ActionApi
{
    protected string $reference;
    protected ?array $staticParameters;
    protected ?array $renamePathParameters;

    /**
     * @return string
     */
    public function getReference(): string
    {
        return $this->reference;
    }

    /**
     * @param string $reference
     *
     * @return ActionApi
     */
    public function setReference(string $reference): ActionApi
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getStaticParameters(): ?array
    {
        return $this->staticParameters;
    }

    /**
     * @param array|null $staticParameters
     *
     * @return ActionApi
     */
    public function setStaticParameters(?array $staticParameters): ActionApi
    {
        $this->staticParameters = $staticParameters;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRenamePathParameters(): ?array
    {
        return $this->renamePathParameters;
    }

    /**
     * @param array|null $renamePathParameters
     *
     * @return ActionApi
     */
    public function setRenamePathParameters(?array $renamePathParameters): ActionApi
    {
        $this->renamePathParameters = $renamePathParameters;

        return $this;
    }
}
