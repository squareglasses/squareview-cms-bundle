<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Guides
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Guides
{
    /* @var Guide[] */
    private array $guides = [];

    /**
     * @return array
     */
    public function getGuides(): array
    {
        return $this->guides;
    }

    /**
     * @param Guide $guide
     *
     * @return Guides
     */
    public function addGuide(Guide $guide): Guides
    {
        $this->guides[] = $guide;
        return $this;
    }
}
