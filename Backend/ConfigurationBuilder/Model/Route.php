<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Route
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Route
{
    private string $name;
    private string $path;
    private string $section;
    private string $type;
    private ?array $restrictions = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Route
     */
    public function setName(string $name): Route
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return Route
     */
    public function setPath(string $path): Route
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getSection(): string
    {
        return $this->section;
    }

    /**
     * @param string $section
     *
     * @return Route
     */
    public function setSection(string $section): Route
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return Route
     */
    public function setType(string $type): Route
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getRestrictions(): ?array
    {
        return $this->restrictions;
    }

    /**
     * @param array|null $restrictions
     * @return Route
     */
    public function setRestrictions(?array $restrictions): Route
    {
        $this->restrictions = $restrictions;
        return $this;
    }
}
