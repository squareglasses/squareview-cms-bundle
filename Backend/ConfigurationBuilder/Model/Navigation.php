<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Navigation
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Navigation
{
    private array $items = [];

    /**
     * @return array
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param NavigationItem $item
     *
     * @return Navigation
     */
    public function addItem(NavigationItem $item): Navigation
    {
        $this->items[] = $item;

        return $this;
    }
}
