<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Language
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Language
{
    private string $id;
    private string $name;
    private bool $enabled = false;
    private bool $main = false;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Language
     */
    public function setName(string $name): Language
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return Language
     */
    public function setId(string $id): Language
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return Language
     */
    public function setEnabled(bool $enabled): Language
    {
        $this->enabled = $enabled;

        return $this;
    }

    /**
     * @return bool
     */
    public function isMain(): bool
    {
        return $this->main;
    }

    /**
     * @param bool $main
     *
     * @return Language
     */
    public function setMain(bool $main): Language
    {
        $this->main = $main;

        return $this;
    }
}
