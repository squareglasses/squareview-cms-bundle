<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class LayoutTranslation
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class LayoutTranslation
{
    private string $name; //guide name
    private ?string $label = null;

    /** @var array<string,LayoutTranslationZone>|null */
    private ?array $zones = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return LayoutTranslation
     */
    public function setName(string $name): LayoutTranslation
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return LayoutTranslation
     */
    public function setLabel(?string $label): LayoutTranslation
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return LayoutTranslationZone[]|null
     */
    public function getZones(): ?array
    {
        return $this->zones;
    }

    /**
     * @param LayoutTranslationZone $zone
     *
     * @return LayoutTranslation
     */
    public function addZone(LayoutTranslationZone $zone): LayoutTranslation
    {
        $this->zones[] = $zone;

        return $this;
    }
}
