<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class ModuleGuides
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ModuleGuides
{
    private array $guides;

    /**
     * @return array
     */
    public function getGuides(): array
    {
        return $this->guides;
    }

    /**
     * @param ModuleGuide $guide
     *
     * @return ModuleGuides
     */
    public function addGuide(ModuleGuide $guide): ModuleGuides
    {
        $this->guides[] = $guide;

        return $this;
    }
}
