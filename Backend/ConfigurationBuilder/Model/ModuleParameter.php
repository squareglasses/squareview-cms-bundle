<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class ModuleParameter
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ModuleParameter
{
    private string $name;
    private string $label;
    private ?string $description = null;
    private string $formType;
    private ?string $defaultValue = null;
    private bool $required = true;
    private ?array $choices;
    private ?string $format = null;
    private ?string $provider = null;
    private ?string $dimensions = null;
    private ?string $api = null;
    private ?array $options = null;
    private ?string $boGridColumn = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ModuleParameter
     */
    public function setName(string $name): ModuleParameter
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return ModuleParameter
     */
    public function setLabel(string $label): ModuleParameter
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return ModuleParameter
     */
    public function setDescription(?string $description = null): ModuleParameter
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return string
     */
    public function getFormType(): string
    {
        return $this->formType;
    }

    /**
     * @param string $formType
     *
     * @return ModuleParameter
     */
    public function setFormType(string $formType): ModuleParameter
    {
        $this->formType = $formType;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultValue(): ?string
    {
        return $this->defaultValue;
    }

    /**
     * @param string|null $defaultValue
     *
     * @return ModuleParameter
     */
    public function setDefaultValue(?string $defaultValue): ModuleParameter
    {
        $this->defaultValue = $defaultValue;

        return $this;
    }

    /**
     * @return bool
     */
    public function isRequired(): bool
    {
        return $this->required;
    }

    /**
     * @param bool $required
     *
     * @return ModuleParameter
     */
    public function setRequired(bool $required): ModuleParameter
    {
        $this->required = $required;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getChoices(): ?array
    {
        return $this->choices;
    }

    /**
     * @param array|null $choices
     *
     * @return ModuleParameter
     */
    public function setChoices(?array $choices): ModuleParameter
    {
        $this->choices = $choices;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     *
     * @return ModuleParameter
     */
    public function setFormat(?string $format): ModuleParameter
    {
        $this->format = $format;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getProvider(): ?string
    {
        return $this->provider;
    }

    /**
     * @param string|null $provider
     *
     * @return ModuleParameter
     */
    public function setProvider(?string $provider): ModuleParameter
    {
        $this->provider = $provider;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDimensions(): ?string
    {
        return $this->dimensions;
    }

    /**
     * @param string|null $dimensions
     *
     * @return ModuleParameter
     */
    public function setDimensions(?string $dimensions): ModuleParameter
    {
        $this->dimensions = $dimensions;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getApi(): ?string
    {
        return $this->api;
    }

    /**
     * @param string|null $api
     *
     * @return ModuleParameter
     */
    public function setApi(?string $api): ModuleParameter
    {
        $this->api = $api;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param array|null $options
     *
     * @return ModuleParameter
     */
    public function setOptions(?array $options): ModuleParameter
    {
        $this->options = $options;

        return $this;
    }


    /**
     * @return string|null
     */
    public function getBoGridColumn(): ?string
    {
        return $this->boGridColumn;
    }

    /**
     * @param string|null $boGridColumn
     *
     * @return ModuleParameter
     */
    public function setBoGridColumn(?string $boGridColumn): ModuleParameter
    {
        $this->boGridColumn = $boGridColumn;

        return $this;
    }
}
