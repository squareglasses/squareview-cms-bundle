<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class LayoutTranslationZone
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class LayoutTranslationZone
{
    private string $name; //zone name
    private string $label;
    private bool $expanded = true;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return LayoutTranslationZone
     */
    public function setName(string $name): LayoutTranslationZone
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return LayoutTranslationZone
     */
    public function setLabel(string $label): LayoutTranslationZone
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return bool
     */
    public function isExpanded(): bool
    {
        return $this->expanded;
    }

    /**
     * @param bool $expanded
     */
    public function setExpanded(bool $expanded): void
    {
        $this->expanded = $expanded;
    }
}
