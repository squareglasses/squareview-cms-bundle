<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionInterface;

/**
 * Class Section
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Section
{
    public const PUBLISHABLE      = 'publishable';
    public const TRANSLATABLE     = 'translatable';
    public const ROUTABLE         = 'routable';
    public const POSITIONABLE     = 'positionable';
    public const METATAGGABLE     = 'metataggable';

    public const FEATURES = [
        self::PUBLISHABLE,
        self::TRANSLATABLE,
        self::ROUTABLE,
        self::POSITIONABLE,
        self::METATAGGABLE
    ];
    private string $name;

    /** @var string[]|null */
    private ?array $features = null;

    /** @var array<string,ActionInterface>|null */
    private ?array $actions = null;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return Section
     */
    public function setName(string $name): Section
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getFeatures(): ?array
    {
        return $this->features;
    }

    /**
     * @param string $feature
     *
     * @return Section
     */
    public function addFeature(string $feature): Section
    {
        $this->features[] = $feature;

        return $this;
    }

    /**
     * @param string[]|null $features
     *
     * @return Section
     */
    public function setFeatures(?array $features): Section
    {
        $this->features = $features;

        return $this;
    }

    /**
     * @return array<string,ActionInterface>|null
     */
    public function getActions(): ?array
    {
        return $this->actions;
    }

    /**
     * @param Action $action
     *
     * @return Section
     */
    public function addAction(Action $action): Section
    {
        $this->actions[] = $action;

        return $this;
    }

    /**
     * @param array<string,ActionInterface> $actions
     *
     * @return Section
     */
    public function setActions(array $actions): Section
    {
        $this->actions = $actions;

        return $this;
    }
}
