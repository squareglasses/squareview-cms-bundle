<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Sections
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Sections
{
    private array $sections;

    /**
     * @return array
     */
    public function getSections(): array
    {
        return $this->sections;
    }

    /**
     * @param Section $section
     *
     * @return Sections
     */
    public function addSection(Section $section): Sections
    {
        $this->sections[] = $section;

        return $this;
    }
}
