<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class NavigationItem
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class NavigationItem
{
    private string $name;
    private ?string $action = null;
    private string $label;
    private string $path;
    private ?Navigation $children;
    private ?string $section = null;
    private ?array $subSections = null;
    private ?array $restrictions = null;

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return NavigationItem
     */
    public function setName(string $name): NavigationItem
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAction(): ?string
    {
        return $this->action;
    }

    /**
     * @param string|null $action
     *
     * @return NavigationItem
     */
    public function setAction(?string $action): NavigationItem
    {
        $this->action = $action;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return NavigationItem
     */
    public function setLabel(string $label): NavigationItem
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return NavigationItem
     */
    public function setPath(string $path): NavigationItem
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return Navigation|null
     */
    public function getChildren(): ?Navigation
    {
        return $this->children;
    }

    /**
     * @param Navigation|null $children
     *
     * @return NavigationItem
     */
    public function setChildren(?Navigation $children): NavigationItem
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSection(): ?string
    {
        return $this->section;
    }

    /**
     * @param string|null $section
     *
     * @return NavigationItem
     */
    public function setSection(?string $section): NavigationItem
    {
        $this->section = $section;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getSubSections(): ?array
    {
        return $this->subSections;
    }

    /**
     * @param array|null $subSections
     *
     * @return NavigationItem
     */
    public function setSubSections(?array $subSections): NavigationItem
    {
        $this->subSections = $subSections;

        return $this;
    }

    public function getRestrictions(): ?array
    {
        return $this->restrictions;
    }

    public function setRestrictions(?array $restrictions): NavigationItem
    {
        $this->restrictions = $restrictions;

        return $this;
    }
}
