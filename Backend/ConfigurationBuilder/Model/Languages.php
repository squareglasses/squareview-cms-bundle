<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Languages
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Languages
{
    /** @var Language[]  */
    private array $languages;

    /**
     * @return Language[]
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param Language[] $languages
     *
     * @return Languages
     */
    public function setLanguages(array $languages): Languages
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * @param Language $language
     */
    public function addLanguage(Language $language): void
    {
        $this->languages[] = $language;
    }
}
