<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class ModuleGuide
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ModuleGuide
{
    private string $name;
    private string $label;
    private string $description;
    private string $icon;
    private ?string $pattern;
    private ?array $parameters;
    private ?string $boGridColumns;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return ModuleGuide
     */
    public function setName(string $name): ModuleGuide
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getLabel(): string
    {
        return $this->label;
    }

    /**
     * @param string $label
     *
     * @return ModuleGuide
     */
    public function setLabel(string $label): ModuleGuide
    {
        $this->label = $label;

        return $this;
    }

    /**
     * @return string
     */
    public function getIcon(): string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     *
     * @return ModuleGuide
     */
    public function setIcon(string $icon): ModuleGuide
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPattern(): ?string
    {
        return $this->pattern;
    }

    /**
     * @param string|null $pattern
     *
     * @return ModuleGuide
     */
    public function setPattern(?string $pattern): ModuleGuide
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getParameters(): ?array
    {
        return $this->parameters;
    }

    /**
     * @param array|null $parameters
     *
     * @return ModuleGuide
     */
    public function setParameters(?array $parameters): ModuleGuide
    {
        $this->parameters = $parameters;

        return $this;
    }

    /**
     * @param ModuleParameter $parameter
     *
     * @return $this
     */
    public function addParameter(ModuleParameter $parameter): ModuleGuide
    {
        $this->parameters[] = $parameter;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return $this
     */
    public function setDescription(string $description): ModuleGuide
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getBoGridColumns(): ?string
    {
        return $this->boGridColumns;
    }

    /**
     * @param string|null $boGridColumns
     *
     * @return ModuleGuide
     */
    public function setBoGridColumns(?string $boGridColumns): ModuleGuide
    {
        $this->boGridColumns = $boGridColumns;

        return $this;
    }
}
