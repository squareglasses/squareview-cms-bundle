<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Model;

/**
 * Class Routing
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class Routing
{
    private array $routes;

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param Route $route
     *
     * @return Routing
     */
    public function addRoute(Route $route): Routing
    {
        $this->routes[] = $route;

        return $this;
    }

    /**
     * @param string $section
     * @param string $name
     *
     * @return Route|null
     */
    public function getRoute(string $section, string $name): ?Route
    {
        foreach ($this->routes as $route) {
            if ($route->getSection() === $section && $route->getName() === $name) {
                return $route;
            }
        }

        return null;
    }
}
