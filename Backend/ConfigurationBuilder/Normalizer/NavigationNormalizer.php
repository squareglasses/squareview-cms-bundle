<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\Exception\RoutingException;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Navigation;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\NavigationItem;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Route;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Routing;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class NavigationNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class NavigationNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        $navigation = [];

        /* @var NavigationItem $navigationItem */
        foreach ($object->getItems() as $navigationItem) {
            //@todo try adding array<NavigationItem> to annotation and see if this can be executed automatically
            $children = $navigationItem->getChildren()
                ? $this->normalize($navigationItem->getChildren(), $format, $context)
                : null
            ;

            $itemName = $navigationItem->getName();
            $navigation[$itemName] = [
                'name' => $itemName,
                'action' => $navigationItem->getAction(),
                'label' => $navigationItem->getLabel(),
                'path' => $navigationItem->getPath(),
                'children' => $children,
                'section' => $navigationItem->getSection(),
                'subSections' => $navigationItem->getSubSections(),
                'restrictions' => $navigationItem->getRestrictions()
            ];
        }

        return $navigation;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return Navigation
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Navigation
    {
        $navigation = new Navigation();

        /* @var Routing $routing */
        $routing = $context['routing'];
        $sections = $context['sections'];

        foreach ($data as $itemName => $item) {
            if (!array_key_exists('label', $item)) {
                throw new RoutingException(
                    'No label configured for navigation element "'.$itemName.'"'
                );
            }

            $section = $item['section'] ?? null;
            $sectionConfiguration = $section && array_key_exists($section, $sections) ? $sections[$section] : null;

            //If restrictions is empty: authorized for everyone that has access to backend
            //If restrictions contain roles: user must have a common role to access navigation item
            $restrictions =
                ($sectionConfiguration && array_key_exists('restrictions', $sectionConfiguration) && !empty($sectionConfiguration['restrictions']))
                ? $sectionConfiguration['restrictions']
                : null;

            //Always add ROLE_SUPER_ADMIN role to all restrictions so it can access everything
            if($restrictions && !in_array('ROLE_SUPER_ADMIN', $restrictions)) {
                $restrictions[] = 'ROLE_SUPER_ADMIN';
            }

            $subSections = $item['subSections'] ?? null;
            $action = $item['action'] ?? null;

            /* @var NavigationItem $navigationItem */
            $navigationItem = $this->normalizer->denormalize([
                'name' => $itemName,
                'action' => $action ?: null,
                'label' => $item['label'],
                'path' => $this->getPath($item, $routing),
                'section' => $section,
                'subSections' => $subSections,
                'restrictions' => $restrictions
            ], NavigationItem::class);

            $childrenNavigation = array_key_exists('children', $item) ?
                $this->denormalize(
                    $item['children'],
                    Navigation::class,
                    $format,
                    $context
                ) : null
            ;

            $navigationItem->setChildren($childrenNavigation);

            $navigation->addItem($navigationItem);
        }

        return $navigation;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Navigation;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === Navigation::class;
    }

    /**
     * @param array   $item
     * @param Routing $routing
     *
     * @return string
     */
    private function getPath(array $item, Routing $routing): string
    {
        $path = '';
        if (array_key_exists('section', $item)) {
            $sectionName = $item['section'];
            $actionName = array_key_exists('action', $item) ? $item['action'] : Action::TYPE_LIST;

            /* @var Route $route */
            $route = $routing->getRoute($sectionName, $actionName);

            if (!$route) {
                throw new RoutingException(sprintf(
                    'No route found for section "%s" and action "%s" configured in sections,
                        yet called in navigation item "%s"',
                    $sectionName,
                    $actionName,
                    $item['label']
                ));
            }

            $path = $route->getPath();
        } elseif (array_key_exists('path', $item)) {
            $path = $item['path'];
        }

        return $path;
    }
}
