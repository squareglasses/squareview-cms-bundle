<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction\EditActionField;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction\EditActionGallery;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction\EditActionMedia;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction\EditActionZone;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EditActionZoneNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class EditActionZoneNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        //Extract fields to denormalize them separately
        $fields = $this->extractForSerialization('fields', $data) ?: [];
        $medias = $this->extractForSerialization('medias', $data) ?: [];
        $galleryIdentifier = $this->extractForSerialization('gallery', $data) ?: null;

        /* @var EditActionZone $Zone */
        $zone = $this->normalizer->denormalize($data, $type, $format, $context);

        //Denormalize fields
        foreach ($fields as $fieldIdentifier => $fieldData) {
            $fieldData['identifier'] = $fieldIdentifier;

            //Default label to capitalized identifier
            if (!array_key_exists('label', $fieldData) && $context['use_defaults']) {
                $fieldData['label'] = ucfirst(str_replace("_", " ", $fieldIdentifier));
            }

            $parameters = null;
            if (array_key_exists('parameters', $fieldData)) {
                $parameters = $this->extractForSerialization('parameters', $fieldData) ?: [];
            }

            /* @var EditActionField $field */
            $field = $this->normalizer->denormalize($fieldData, EditActionField::class);

            if ($parameters) {
                $this->denormalizeActionParameters($field, $parameters, $this->normalizer);
            }

            $zone->addField($field);
        }

        //Denormalize medias
        if ($zone->getType() === EditActionZone::ZONE_TYPE_MEDIAS) {
            $mediaDefinitions = $context['medias'];
            foreach ($medias as $mediaIdentifier => $mediaData) {
                if (!array_key_exists($mediaIdentifier, $mediaDefinitions)) {
                    throw new ConfigurationException(sprintf(
                        "Media identifier '%s' for zone '%s' was not found in media definitions of cms.yml",
                        $mediaIdentifier,
                        $zone->getName()
                    ));
                }

                $mediaDefinition = $mediaDefinitions[$mediaIdentifier];
                $mediaData['identifier'] = $mediaIdentifier;
                $mediaData = $this->mergeWithDefinitions($mediaData, $mediaDefinition);

                //Default label to capitalized identifier
                if (!array_key_exists('label', $mediaData) && $context['use_defaults']) {
                    $mediaData['label'] = ucfirst(str_replace("_", " ", $mediaIdentifier));
                }

                //Default label to capitalized identifier
                if (!array_key_exists('required', $mediaData)) {
                    $mediaData['required'] = false;
                }

                /* @var EditActionMedia $media */
                $media = $this->normalizer->denormalize($mediaData, EditActionMedia::class);

                $zone->addMedia($media);
            }
        }

        if ($zone->getType() === EditActionZone::ZONE_TYPE_GALLERY) {
            $galleriesDefinitions = $context['galleries'];
            if ($galleryIdentifier) {
                if (!array_key_exists($galleryIdentifier, $galleriesDefinitions)) {
                    throw new ConfigurationException(sprintf(
                        "Gallery identifier '%s' for zone '%s' was not found in gallery definitions of cms.yml",
                        $galleryIdentifier,
                        $zone->getName()
                    ));
                }

                $galleryData = $galleriesDefinitions[$galleryIdentifier];
                $galleryData['identifier'] = $galleryIdentifier;

                /* @var EditActionGallery $gallery */
                $gallery = $this->normalizer->denormalize($galleryData, EditActionGallery::class);

                $zone->setGallery($gallery);
            }
        }

        return $zone;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof EditActionZone;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === EditActionZone::class;
    }
}
