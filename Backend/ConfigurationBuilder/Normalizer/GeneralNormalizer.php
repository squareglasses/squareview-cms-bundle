<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\General;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\LayoutTranslation;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\MetaTag;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class GeneralNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class GeneralNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer            $normalizer
     * @param LayoutTranslationNormalizer $translationNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly LayoutTranslationNormalizer $translationNormalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $metaTags = $this->extractForSerialization('meta_tags', $data) ?: [];
        $layoutTranslations = $this->extractForSerialization('layout_translations', $data) ?: [];

        $data['website_identifier'] = $context['website_identifier'];
        $data['multilingual'] = $context['multilingual'];
        $data['squareview_version'] = $context['squareview_version'];

        $general = $this->normalizer->denormalize($data, General::class, null, $context);

        foreach ($metaTags as $metaTagName => $metaTagData) {
            $metaTagData['name'] = $metaTagName;

            /* @var MetaTag $metaTag */
            $metaTag = $this->normalizer->denormalize($metaTagData, MetaTag::class, null, $context);

            $general->addMetaTag($metaTag);
        }

        foreach ($layoutTranslations as $guideName => $layoutTranslationsData) {
            $layoutTranslationsData['name'] = $guideName;

            /* @var LayoutTranslation $layoutTranslation */
            $layoutTranslation = $this->translationNormalizer->denormalize($layoutTranslationsData, LayoutTranslation::class, null, $context);

            $general->addLayoutTranslation($layoutTranslation);
        }

        return $general;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof General;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === General::class;
    }
}
