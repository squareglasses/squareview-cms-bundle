<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\ModuleGuide;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\ModuleGuides;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ModuleGuidesNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ModuleGuidesNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param ObjectNormalizer      $normalizer
     * @param ModuleGuideNormalizer $moduleGuideNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ModuleGuideNormalizer $moduleGuideNormalizer)
    {
    }

    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        $moduleGuides = $this->normalizer->normalize($object, $format, $context);

        return $moduleGuides['guides'];
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return ModuleGuides
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): ModuleGuides
    {
        $moduleGuides = new ModuleGuides();

        foreach ($data as $guideName => $guideData) {
            $guideData['name'] = $guideName;

            /* @var ModuleGuide $guide */
            $guide = $this->moduleGuideNormalizer->denormalize($guideData, ModuleGuide::class, null, $context);

            $moduleGuides->addGuide($guide);
        }

        return $moduleGuides;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof ModuleGuides;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === ModuleGuides::class;
    }
}
