<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowActionZone;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ShowActionNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowActionNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer         $normalizer
     * @param ShowActionZoneNormalizer $zoneNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ShowActionZoneNormalizer $zoneNormalizer)
    {
    }

    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $zones = $this->extractForSerialization('zones', $data) ?: [];
        $editAction = $this->normalizer->denormalize($data, ShowAction::class);

        //Preparing data for zone denormalization
        $position = 0;
        foreach ($zones as $zoneName => $zoneData) {
            if ($zoneData !== null) {
                $zoneData['name'] = $zoneName;
                $zoneData['position'] = $position; //Handle positioning by order of appearance in list

                //Make 'api' key equal to its type if it is not overridden by another value
                if (!array_key_exists('api', $zoneData) && array_key_exists('type', $zoneData)) {
                    $zoneData['api'] = $zoneData['type'];
                }

                /* @var ShowActionZone $zone */
                $zone = $this->zoneNormalizer->denormalize($zoneData, ShowActionZone::class, null, $context);
            } else {
                $zone = new ShowActionZone();
                $zone->setName($zoneName);
                $zone->setPosition($position);
            }

            $editAction->addZone($zone);
            $position++;
        }

        return $editAction;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof ShowAction;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === ShowAction::class;
    }
}
