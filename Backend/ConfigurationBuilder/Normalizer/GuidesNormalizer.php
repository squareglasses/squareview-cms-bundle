<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Guide;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Guides;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class GuidesNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class GuidesNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     * @param GuideNormalizer  $guideNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly GuideNormalizer $guideNormalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        $guides = $this->normalizer->normalize($object, $format, $context);
        return $guides['guides'];
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return Guides
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Guides
    {
        $guides = new Guides();
        $context['all_actions'] = $this->listAllActions($data);
        //$frontendGuides = $context['frontend_guides']; @TODO use this if useful ?

        foreach ($data as $sectionName => $sectionGuideData) {
            $sectionGuides = $sectionGuideData['guides'];
            foreach ($sectionGuides as $guideName => $guideData) {
                $guideData['name'] = $guideName;

                /* @var Guide $guide */
                $context['section'] = $sectionName;
                $guide = $this->guideNormalizer->denormalize($guideData, Guide::class, null, $context);

                $guides->addGuide($guide);
            }
        }

        return $guides;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Guides;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === Guides::class;
    }
}
