<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\Exception\RoutingException;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Route;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Routing;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class RoutingNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class RoutingNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        $routing = $this->normalizer->normalize($object, $format, $context);
        return $routing['routes'];
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return Routing
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Routing
    {
        $routing = new Routing();
        foreach ($data as $sectionName => $section) {
            if (!array_key_exists('actions', $section)) {
                throw new RoutingException('Section '.$sectionName.' missing "actions" configuration.');
            }

            //If restrictions is empty: authorized for everyone that has access to backend
            //If restrictions contain roles: user must have a common role to access navigation item
            $restrictions =
                (array_key_exists('restrictions', $section) && !empty($section['restrictions']))
                    ? $section['restrictions']
                    : null;

            //Always add ROLE_SUPER_ADMIN role to all restrictions so it can access everything
            if($restrictions && !in_array('ROLE_SUPER_ADMIN', $restrictions)) {
                $restrictions[] = 'ROLE_SUPER_ADMIN';
            }

            foreach ($section['actions'] as $actionName => $action) {

                $actionType = $this->extractActionType($actionName);

                if($actionType !== Action::TYPE_DELETE && $actionType !== Action::TYPE_CUSTOM) {
                    $routeData = [
                        'name' => $actionName,
                        'path' => $action['path'],
                        'section' => $sectionName,
                        'type' => $actionType,
                        'restrictions' => $restrictions
                    ];

                    /* @var Route */
                    $route = $this->normalizer->denormalize($routeData, Route::class);

                    $routing->addRoute($route);
                }
            }
        }

        return $routing;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Routing;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === Routing::class;
    }
}
