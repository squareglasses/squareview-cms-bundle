<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\ModuleGuide;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\ModuleParameter;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ModuleGuideNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ModuleGuideNormalizer implements DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $parameters = $this->extractForSerialization('parameters', $data);
        $mediaDefinitions = $context['medias'];

        $moduleGuide = $this->normalizer->denormalize($data, ModuleGuide::class);
        foreach ($parameters as $parameterName => $parameterData) {
            $parameterData['name'] = $parameterName;

            //Handle media parameters: add media configuration
            if (array_key_exists('form_type', $parameterData) && $parameterData['form_type'] === 'media') {
                $mediaIdentifier = $parameterName;
                if (!array_key_exists($mediaIdentifier, $mediaDefinitions)) {
                    throw new ConfigurationException(sprintf(
                        "Media identifier '%s' for parameter of module '%s' was not found in media definitions of cms.yml",
                        $mediaIdentifier,
                        $data['name']
                    ));
                }

                $mediaDefinition = $mediaDefinitions[$mediaIdentifier];
                $parameterData = $this->mergeWithDefinitions($parameterData, $mediaDefinition);

                //Default label to capitalized identifier
                if (!array_key_exists('label', $parameterData)) {
                    $parameterData['label'] = ucfirst(str_replace("_", " ", $mediaIdentifier));
                }
            }

            /* @var ModuleParameter $parameter */
            $parameter = $this->normalizer->denormalize($parameterData, ModuleParameter::class);

            $moduleGuide->addParameter($parameter);
        }

        return $moduleGuide;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === ModuleGuide::class;
    }
}
