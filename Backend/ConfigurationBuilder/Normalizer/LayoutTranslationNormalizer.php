<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\LayoutTranslation;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\LayoutTranslationZone;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class LayoutTranslationNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class LayoutTranslationNormalizer implements DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $zones = $this->extractForSerialization('zones', $data) ?: [];
        $layoutTranslation = $this->normalizer->denormalize($data, $type, $format, $context);

        foreach ($zones as $zoneName => $zoneData) {
            $zoneData['name'] = $zoneName;

            /* @var LayoutTranslationZone $layoutTranslationZone */
            $layoutTranslationZone = $this->normalizer->denormalize($zoneData, LayoutTranslationZone::class, null, $context);

            $layoutTranslation->addZone($layoutTranslationZone);
        }

        return $layoutTranslation;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === LayoutTranslation::class;
    }
}
