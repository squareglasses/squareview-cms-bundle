<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionInterface;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Guide;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class GuideNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class GuideNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     * @param ActionNormalizer $actionNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ActionNormalizer $actionNormalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $actions = $this->extractForSerialization('actions', $data) ?: [];
        $sectionName = $context['section'];
        $data['section'] = $sectionName;
        $guide = $this->normalizer->denormalize($data, Guide::class);

        foreach ($actions as $actionName => $actionData) {
            $actionType = $this->extractActionType($actionName);
            $actionData['guide'] = $guide->getName();
            $actionData['section'] = $sectionName;
            $actionData['name'] = $actionName;
            $actionData['type'] = $actionType;

            /* @var ActionInterface $action */
            $context['actions'] = null;
            $context['use_defaults'] = false;
            $action = $this->actionNormalizer->denormalize($actionData, ActionInterface::class, null, $context);

            $guide->addAction($action);
        }

        return $guide;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Guide;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === Guide::class;
    }
}
