<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionParameter;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\HasActionParametersInterface;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Trait NormalizerHelper
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
trait NormalizerHelper
{
    /**
     * @param $key
     * @param $data
     *
     * @return mixed|null
     */
    public function extractForSerialization($key, &$data): mixed
    {
        if (array_key_exists($key, $data)) {
            $extracted = $data[$key];
            unset($data[$key]);
            return $extracted;
        }

        return null;
    }

    /**
     * @param array $data
     * @param array $definitions
     *
     * @return array
     */
    public function mergeWithDefinitions(array $data, array $definitions): array
    {
        foreach ($data as $key => $value) {
            if (($value === null || $value === '') && array_key_exists($key, $definitions)) {
                $data[$key] = $definitions[$key];
            }
        }

        foreach ($definitions as $key => $value) {
            if (!array_key_exists($key, $data)) {
                $data[$key] = $value;
            }
        }

        return $data;
    }

    public function denormalizeActionParameters(HasActionParametersInterface &$action, ?array $data, ObjectNormalizer $normalizer)
    {
        if($data) {
            foreach ($data as $parameterName => $parameterData) {
//                if ($parameterData === null) {
//                    $parameterData = [];
//                }
                $parameterData['name'] = $parameterName;
                if ((!array_key_exists('value', $parameterData) || $parameterData['value'] === null)
                    && (!array_key_exists('type', $parameterData) || $parameterData['type'] === ActionParameter::TYPE_DYNAMIC)) {
                    $parameterData['value'] = $parameterName;
                }
                $parameter = $normalizer->denormalize($parameterData, ActionParameter::class);
                $action->addParameter($parameter);
            }
        }
    }

    /**
     * @param mixed $data
     * @return array
     */
    public function listAllActions(mixed $data): array
    {
        $actions = [];
        foreach ($data as $sectionData) {
            $actionsData = $this->extractForSerialization('actions', $sectionData) ?: [];
            foreach ($actionsData as $actionDataName => $actionData) {
                if (array_key_exists($actionDataName, $actions)) {
                    throw new ConfigurationException(sprintf(
                        "The action name '%s' must be unique",
                        $actionDataName,
                    ));
                }
                $actions[$actionDataName] = $actionData;
            }
        }
        return $actions;
    }

    public function extractActionType(string $actionName): string
    {
        $deconstructed = explode('#', $actionName);
        $actionType = $deconstructed[0];

        if (!in_array($actionType, Action::TYPES)) {
            throw new ConfigurationException(sprintf(
                "The action named '%s' must have a valid type before '#'. Valid types are : %s",
                $actionName,
                implode(', ', Action::TYPES)
            ));
        }

        return $actionType;
    }
}
