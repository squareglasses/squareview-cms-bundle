<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionInterface;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Section;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class SectionNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class SectionNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     * @param ActionNormalizer $actionNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ActionNormalizer $actionNormalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $actions = $this->extractForSerialization('actions', $data) ?: [];
        $section = $this->normalizer->denormalize($data, Section::class);

        if (in_array(Section::POSITIONABLE, $data['features'])) {
            $this->checkIfValidPositionableConfig($actions, $section->getName());
        }

        foreach ($actions as $actionName => $actionData) {
            $actionType = $this->extractActionType($actionName);
            $actionData['section'] = $section->getName();
            $actionData['name'] = $actionName;
            $actionData['type'] = $actionType;


            /* @var ActionInterface $action */
            $context['actions'] = $actions;
            $context['use_defaults'] = true;
            $action = $this->actionNormalizer->denormalize($actionData, ActionInterface::class, null, $context);

            $section->addAction($action);
        }
        return $section;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Section;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === Section::class;
    }

    /**
     * @param array $actions
     * @param string $sectionName
     * @return void
     */
    private function checkIfValidPositionableConfig(array $actions, string $sectionName): void
    {
        $listActions = array_filter($actions, function($actionName) {
            return $this->extractActionType($actionName) === Action::TYPE_LIST;
        }, ARRAY_FILTER_USE_KEY);

        $apiOrder = [];
        foreach ($listActions as $listActionName => $listAction) {
            if (array_key_exists('order', $listAction['apis'])) {
                $apiOrder[] = $listAction['apis']['order']['reference'];
            }
            if (count($listAction['order']) || array_key_exists('default_order', $listAction)) {
                throw new ConfigurationException(sprintf(
                    "Using %s feature, please remove 'order' and 'default_order' options in action named %s for section %s",
                    Section::POSITIONABLE,
                    $listActionName,
                    $sectionName
                ));
            }
        }

        if (!count($apiOrder)) {
            throw new ConfigurationException(sprintf(
                "Using %s feature, but missing api named 'order' in action named %s for section %s",
                Section::POSITIONABLE,
                implode(' or ', array_keys($listActions)),
                $sectionName
            ));
        }
    }
}
