<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction\ListAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction\ListActionColumn;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction\ListActionFilter;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction\ListActionGeneralAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction\ListActionRowAction;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ListActionNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ListActionNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        $columns = $this->extractForSerialization('columns', $data) ?: [];
        $filters = $this->extractForSerialization('filters', $data) ?: [];
        $generalActions = $this->extractForSerialization('general_actions', $data) ?: [];
        $rowActions = $this->extractForSerialization('row_actions', $data) ?: [];
        $otherActions = $context['all_actions'];

        $listAction = $this->normalizer->denormalize($data, $type, $format, $context);

        foreach ($columns as $columnName => $columnData) {
            $mergedColumnAsLabel = null;
            if (($columnData !== null) && array_key_exists('merged_column_as_label', $columnData)) {
                $mergedColumnAsLabel = $this->normalizer->denormalize(
                    $columnData['merged_column_as_label'],
                    ListActionColumn::class
                );

                unset($columnData['merged_column_as_label']);
            }

            $columnData['name'] = $columnName;
            $column = $this->normalizer->denormalize($columnData, ListActionColumn::class);
            if ($mergedColumnAsLabel) {
                $column->setMergedColumnAsLabel($mergedColumnAsLabel);
            }

            $listAction->addColumn($column);
        }

        foreach ($filters as $filterName => $filterData) {
            $filterData['name'] = $filterName;
            $filter = $this->normalizer->denormalize($filterData, ListActionFilter::class);
            $listAction->addFilter($filter);
        }

        $listAction->setGeneralActions(null);

        foreach ($generalActions as $generalActionName => $generalActionData) {
            if (!array_key_exists($generalActionName, $otherActions)) {
                throw new ConfigurationException(sprintf(
                    "The general action '%s' of the list action '%s' must be an existing action",
                    $generalActionName,
                    $listAction->getName()
                ));
            }

            $generalActionData['name'] = $generalActionName;

            if ($otherActions) {
                $otherActionData = $otherActions[$generalActionName];
                if ($generalActionName !== Action::TYPE_DELETE) {
                    $generalActionData['path'] = $otherActionData['path'];
                }
            }

            /** @var ListActionGeneralAction $generalAction */
            $generalAction = $this->normalizer->denormalize($generalActionData, ListActionGeneralAction::class);

            $listAction->addGeneralAction($generalAction);
        }

        $listAction->setRowActions(null);
        foreach ($rowActions as $rowActionName => $rowActionData) {
            $rowActionData['name'] = $rowActionName;
            $rowActionType = $this->extractActionType($rowActionName);
            $rowActionData['type'] = $rowActionType;

            if ($otherActions) {
                if (!array_key_exists($rowActionName, $otherActions)) {
                    throw new ConfigurationException(sprintf(
                        "The row action '%s' of the list action '%s' must be an existing action",
                        $rowActionName,
                        $listAction->getName()
                    ));
                }

                $otherActionData = $otherActions[$rowActionName];
                if ($rowActionType !== Action::TYPE_DELETE && $rowActionType !== Action::TYPE_CUSTOM) {
                    $rowActionData['path'] = $otherActionData['path'];
                }
            }

            $rowAction = $this->normalizer->denormalize($rowActionData, ListActionRowAction::class);
            $listAction->addRowAction($rowAction);
        }
        return $listAction;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof ListAction;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === ListAction::class;
    }
}
