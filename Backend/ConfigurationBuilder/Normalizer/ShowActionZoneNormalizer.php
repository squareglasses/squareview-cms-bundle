<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowActionField;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowActionGallery;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowActionMedia;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowActionZone;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ShowActionZoneNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ShowActionZoneNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;

    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): mixed
    {
        //Extract fields to denormalize them separately
        $fields = $this->extractForSerialization('fields', $data) ?: [];

        /* @var ShowActionZone $Zone */
        $zone = $this->normalizer->denormalize($data, $type, $format, $context);

        //Denormalize fields
        foreach ($fields as $fieldIdentifier => $fieldData) {
            $fieldData['identifier'] = $fieldIdentifier;

            //Default label to capitalized identifier
            if (!array_key_exists('label', $fieldData) && $context['use_defaults']) {
                $fieldData['label'] = ucfirst(str_replace("_", " ", $fieldIdentifier));
            }

            $parameters = null;
            if (array_key_exists('parameters', $fieldData)) {
                $parameters = $this->extractForSerialization('parameters', $fieldData) ?: [];
            }

            /* @var ShowActionField $field */
            $field = $this->normalizer->denormalize($fieldData, ShowActionField::class);

            if ($parameters) {
                $this->denormalizeActionParameters($field, $parameters, $this->normalizer);
            }

            $zone->addField($field);
        }

        return $zone;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof ShowActionZone;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === ShowActionZone::class;
    }
}
