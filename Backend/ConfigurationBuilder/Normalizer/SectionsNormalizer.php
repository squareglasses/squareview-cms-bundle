<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Section;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Sections;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class SectionsNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class SectionsNormalizer implements NormalizerInterface, DenormalizerInterface
{
    use NormalizerHelper;
    /**
     * @param ObjectNormalizer  $normalizer
     * @param SectionNormalizer $sectionNormalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly SectionNormalizer $sectionNormalizer)
    {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        $sections = $this->normalizer->normalize($object, $format, $context);

        return $sections['sections'];
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return Sections
     * @throws ExceptionInterface
     */
    public function denormalize(mixed $data, string $type, string $format = null, array $context = []): Sections
    {
        $sections = new Sections();

        $context['all_actions'] = $this->listAllActions($data);

        foreach ($data as $sectionName => $sectionData) {
            $sectionData['name'] = $sectionName;

            /* @var Section $section */
            $section = $this->sectionNormalizer->denormalize($sectionData, Section::class, null, $context);

            $sections->addSection($section);
        }

        return $sections;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Sections;
    }

    /**
     * @param mixed       $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization(mixed $data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === Sections::class;
    }
}
