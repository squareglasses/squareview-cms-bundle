<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder\Normalizer;

use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\Action;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ActionInterface;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\CustomAction\CustomAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\DeleteAction\DeleteAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\EditAction\EditAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ListAction\ListAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\NewAction\NewAction;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Action\ShowAction\ShowAction;
use SG\CmsBundle\Backend\Exception\ConfigurationException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ActionNormalizer
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ActionNormalizer implements NormalizerInterface, DenormalizerInterface
{
    /**
     * @param ObjectNormalizer $normalizer
     * @param ListActionNormalizer $listActionNormalizer
     * @param EditActionNormalizer $editActionNormalizer
     * @param DeleteActionNormalizer $deleteActionNormalizer
     * @param NewActionNormalizer $newActionNormalizer
     * @param ShowActionNormalizer $showActionNormalizer
     * @param CustomActionNormalizer $customActionNormalizer
     */
    public function __construct(
        private readonly ObjectNormalizer       $normalizer,
        private readonly ListActionNormalizer   $listActionNormalizer,
        private readonly EditActionNormalizer   $editActionNormalizer,
        private readonly DeleteActionNormalizer $deleteActionNormalizer,
        private readonly NewActionNormalizer    $newActionNormalizer,
        private readonly ShowActionNormalizer   $showActionNormalizer,
        private readonly CustomActionNormalizer $customActionNormalizer
    ) {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array
     * @throws ExceptionInterface
     */
    public function normalize(mixed $object, string $format = null, array $context = []): array
    {
        if ($object instanceof EditAction) {
            return $this->editActionNormalizer->normalize($object, $format, $context);
        }

        if ($object instanceof ListAction) {
            return $this->listActionNormalizer->normalize($object, $format, $context);
        }

        if ($object instanceof DeleteAction) {
            return $this->deleteActionNormalizer->normalize($object, $format, $context);
        }

        if ($object instanceof NewAction) {
            return $this->newActionNormalizer->normalize($object, $format, $context);
        }

        if ($object instanceof ShowAction) {
            return $this->showActionNormalizer->normalize($object, $format, $context);
        }

        if ($object instanceof CustomAction) {
            return $this->customActionNormalizer->normalize($object, $format, $context);
        }

        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param             $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed|object
     * @throws ExceptionInterface
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): mixed
    {
        switch ($data['type']) {
            case Action::TYPE_LIST:
                $actionNormalizer = $this->listActionNormalizer;
                $actionClass = ListAction::class;
                break;
            case Action::TYPE_EDIT:
                $actionNormalizer = $this->editActionNormalizer;
                $actionClass = EditAction::class;
                break;
            case Action::TYPE_DELETE:
                $actionNormalizer = $this->deleteActionNormalizer;
                $actionClass = DeleteAction::class;
                break;
            case Action::TYPE_NEW:
                $actionNormalizer = $this->newActionNormalizer;
                $actionClass = NewAction::class;
                break;
            case Action::TYPE_SHOW:
                $actionNormalizer = $this->showActionNormalizer;
                $actionClass = ShowAction::class;
                break;
            case Action::TYPE_CUSTOM:
                $actionNormalizer = $this->customActionNormalizer;
                $actionClass = CustomAction::class;
                break;

            default:
                throw new ConfigurationException("Action name '".$data['name']."' has not been implemented");
        }

        return $actionNormalizer->denormalize($data, $actionClass, $format, $context);
    }

    /**
     * @param             $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof ActionInterface;
    }

    /**
     * @param             $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
    {
        return is_array($data) && $type === ActionInterface::class;
    }
}
