<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use JsonException;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Languages;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class LanguagesBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class LanguagesBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param string              $defaultLocale
     */
    public function __construct(private readonly SerializerInterface $serializer, private readonly string $defaultLocale)
    {
    }

    /**
     * @param $data
     *
     * @return Languages
     * @throws JsonException
     */
    public function build($data): Languages
    {
        return $this->serializer->denormalize(
            ['languages' => json_decode($data, true, 512, JSON_THROW_ON_ERROR)],
            Languages::class,
            'json',
            ['main' => $this->defaultLocale]
        );
    }

    /**
     * @param Languages $languages
     *
     * @return array
     */
    public function serialize(Languages $languages): array
    {
        return $this->serializer->normalize($languages);
    }
}
