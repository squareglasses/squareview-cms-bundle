<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use Exception;
use JsonException;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\General;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class GeneralBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class GeneralBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param array $generalConfiguration
     * @param string $websiteIdentifier
     * @param bool $multilingual
     * @param FilesystemOperator $vendorStorage
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly array               $generalConfiguration,
        private readonly string              $websiteIdentifier,
        private readonly bool                $multilingual,
        private readonly FilesystemOperator  $vendorStorage
    ) {
    }

    /**
     * @return General
     * @throws JsonException
     */
    public function build(): General
    {
        return $this->serializer->denormalize(
            $this->generalConfiguration,
            General::class,
            null,
            [
                'website_identifier' => $this->websiteIdentifier,
                'multilingual' => $this->multilingual,
                'squareview_version' => $this->getVendorSquareviewVersion()
            ]
        );
    }

    /**
     * @param General $general
     *
     * @return array
     */
    public function serialize(General $general): array
    {
        return $this->serializer->normalize($general);
    }

    /**
     * @return string|null
     * @throws JsonException
     */
    private function getVendorSquareviewVersion(): ?string
    {
        try {
            $composerFile = $this->vendorStorage->read('composer/installed.json');
        } catch (Exception|FilesystemException) {
            return null;
        }

        $packages = json_decode($composerFile, false, 512, JSON_THROW_ON_ERROR)->packages;
        foreach ($packages as $package) {
            if (isset($package->name) && $package->name === 'squareglasses/squareview-cms-bundle') {
                return $package->version;
            }
        }
        return null;
    }
}
