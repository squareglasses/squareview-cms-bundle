<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Sections;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class SectionsBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class SectionsBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param array               $sectionsConfiguration
     * @param array               $mediasConfiguration
     * @param array               $galleriesConfiguration
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly array               $sectionsConfiguration,
        private readonly array               $mediasConfiguration,
        private readonly array               $galleriesConfiguration
    ) {
    }

    /**
     * @return Sections
     */
    public function build(): Sections
    {
        return $this->serializer->denormalize(
            $this->sectionsConfiguration,
            Sections::class,
            null,
            [
                'medias' => $this->mediasConfiguration,
                'galleries' => $this->galleriesConfiguration
            ]
        );
    }

    /**
     * @param Sections $sections
     *
     * @return array
     */
    public function serialize(Sections $sections): array
    {
        return $this->serializer->normalize($sections);
    }
}
