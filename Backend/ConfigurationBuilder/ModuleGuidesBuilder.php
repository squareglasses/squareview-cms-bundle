<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\ModuleGuides;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ModuleGuidesBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class ModuleGuidesBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param array               $moduleGuidesConfiguration
     * @param array               $mediasConfiguration
     * @param array               $galleriesConfiguration
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly array               $moduleGuidesConfiguration,
        private readonly array               $mediasConfiguration,
        private readonly array               $galleriesConfiguration
    ) {
    }

    /**
     * @return ModuleGuides
     */
    public function build(): ModuleGuides
    {
        return $this->serializer->denormalize(
            $this->moduleGuidesConfiguration,
            ModuleGuides::class,
            null,
            [
                'medias' => $this->mediasConfiguration,
                'galleries' => $this->galleriesConfiguration
            ]
        );
    }

    /**
     * @param ModuleGuides $moduleGuides
     *
     * @return array
     */
    public function serialize(ModuleGuides $moduleGuides): array
    {
        return $this->serializer->normalize($moduleGuides);
    }
}
