<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Navigation;
use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Routing;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class NavigationBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class NavigationBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param array               $navigationConfiguration
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly array $navigationConfiguration,
        private readonly array $sectionsConfiguration
    )
    {
    }

    /**
     * @param Routing $routing
     *
     * @return Navigation
     */
    public function build(Routing $routing): Navigation
    {
        return $this->serializer->denormalize(
            $this->navigationConfiguration,
            Navigation::class,
            null,
            [
                'routing' => $routing,
                'sections' => $this->sectionsConfiguration
            ]
        );
    }

    /**
     * @param Navigation $navigation
     *
     * @return array
     */
    public function serialize(Navigation $navigation): array
    {
        return $this->serializer->normalize($navigation);
    }
}
