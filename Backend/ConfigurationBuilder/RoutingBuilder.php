<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Routing;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class RoutingBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class RoutingBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param array               $sectionsConfiguration
     */
    public function __construct(private readonly SerializerInterface $serializer, private readonly array $sectionsConfiguration)
    {
    }

    /**
     * @return Routing
     */
    public function build(): Routing
    {
        return $this->serializer->denormalize(
            $this->sectionsConfiguration,
            Routing::class
        );
    }

    /**
     * @param Routing $routing
     *
     * @return array
     */
    public function serialize(Routing $routing): array
    {
        return $this->serializer->normalize($routing);
    }
}
