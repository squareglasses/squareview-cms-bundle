<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\ConfigurationBuilder;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Guides;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class GuidesBuilder
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 */
class GuidesBuilder
{
    /**
     * @param SerializerInterface $serializer
     * @param array               $guidesConfiguration
     * @param array               $frontendGuidesConfiguration
     * @param array               $mediasConfiguration
     * @param array               $galleriesConfiguration
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly array               $guidesConfiguration,
        private readonly array               $frontendGuidesConfiguration,
        private readonly array               $mediasConfiguration,
        private readonly array               $galleriesConfiguration
    ) {
    }

    /**
     * @return Guides
     */
    public function build(): Guides
    {
        return $this->serializer->denormalize(
            $this->guidesConfiguration,
            Guides::class,
            null,
            [
                'frontend_guides' => $this->frontendGuidesConfiguration,
                'medias' => $this->mediasConfiguration,
                'galleries' => $this->galleriesConfiguration
            ]
        );
    }

    /**
     * @param Guides $guides
     *
     * @return array
     */
    public function serialize(Guides $guides): array
    {
        return $this->serializer->normalize($guides);
    }
}
