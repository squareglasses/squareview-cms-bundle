<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Mailer;

use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class Mailer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Mailer
{
    /**
     * @param MailerInterface  $mailer
     * @param RouterInterface  $router
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(
        protected MailerInterface $mailer,
        protected RouterInterface $router,
        protected ConfigurationBag $configurationBag
    ) {
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     * @throws TransportExceptionInterface
     */
    public function sendResettingEmailMessage(UserInterface $user): void
    {
        $to = $user->getPersonalInfos() !== null ?
            $user->getFullName()." <".$user->getEmail().">"
            : $user->getEmail();
        $email = (new TemplatedEmail())
            ->from(Address::create($this->configurationBag->getSecurityMailerFromAddress()))
            ->to($to)
            ->priority(Email::PRIORITY_HIGH)
            ->subject("Renouvelez votre mot de passe")
            ->htmlTemplate('@SGCms/backend/emails/resetting.html.twig')
            ->context([
                'user' => $user,
                'confirmationUrl' => $this->router->generate(
                    "app_resetting_reset",
                    ['token'=>$user->getConfirmationToken()],
                    UrlGeneratorInterface::ABSOLUTE_URL
                )
            ]);

        $this->mailer->send($email);
    }

    /**
     * @param string      $fromAddress
     * @param array       $toAddresses
     * @param string      $template
     * @param array       $context
     * @param string      $subject
     * @param string      $translationDomain
     * @param string|null $replyToAddress
     * @param array       $ccAddresses
     * @param array       $bccAddresses
     *
     * @return void
     * @throws TransportExceptionInterface
     */
    public function sendEmailMessage(
        string $fromAddress,
        array $toAddresses,
        string $template,
        array $context = [],
        string $subject = "subject",
        string $replyToAddress = null,
        array $ccAddresses = [],
        array $bccAddresses = []
    ): void {
        $email = (new TemplatedEmail())
            ->from(Address::create($fromAddress))
            ->replyTo($replyToAddress !== null ?
                Address::create($replyToAddress) : Address::create($fromAddress))
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($context);

        foreach ($toAddresses as $to) {
            $email->addTo(Address::create($to));
        }
        foreach ($ccAddresses as $cc) {
            $email->addCc(Address::create($cc));
        }
        foreach ($bccAddresses as $bcc) {
            $email->addBcc(Address::create($bcc));
        }
        $this->mailer->send($email);
    }
}
