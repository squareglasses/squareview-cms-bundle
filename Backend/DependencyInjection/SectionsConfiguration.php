<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\DependencyInjection;

use SG\CmsBundle\Backend\ConfigurationBuilder\Model\Section;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class SectionsConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SectionsConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('sections')
                    ->useAttributeAsKey('name')
                    ->arrayPrototype()
                        ->children()
                            ->arrayNode('restrictions')
                                ->scalarPrototype()->end()
                            ->end()
                            ->arrayNode('features')->isRequired()
                                ->enumPrototype()
                                    ->values(Section::FEATURES)
                                ->end()
                            ->end()
                            ->arrayNode('actions')->isRequired()
                                ->useAttributeAsKey('name')
                                ->arrayPrototype()
                                    ->children()
                                        //COMMONS
                                        ->scalarNode('title')->end()
                                        ->scalarNode('path')->end()
                                        ->append($this->addBackActionNode())
                                        ->append($this->addApisNode())

                                        //LIST
                                        ->append($this->addActionListColumnsNode())
                                        ->arrayNode('order')
                                            ->useAttributeAsKey('name')
                                            ->scalarPrototype()->end()
                                        ->end()
                                        ->scalarNode('default_order')->end()
                                        ->append($this->addActionListRowActionsNode())
                                        ->append($this->addActionListGeneralActionsNode())
                                        ->scalarNode('main_row_action')->end()
                                        ->append($this->addFiltersNode())
                                        ->scalarNode('style')->end()
                                        ->scalarNode('options_position')->end()

                                        // EDIT
                                        ->variableNode('zones')
                                            // TODO
                                        ->end()
//                                        ->arrayNode('type')->isRequired()
//                                            ->enumPrototype()
//                                                ->values(['medias', 'properties', 'gallery', 'modules', 'translations', 'collection'])
//                                            ->end()
//                                        ->end()
                                        ->booleanNode('main_title_readonly')->end()
                                        ->booleanNode('main_title_required')->end()
                                        ->booleanNode('main_title_visible')->end()
                                        ->scalarNode('main_title_property')->end()
                                        ->scalarNode('enabled_property')->end()
                                        ->arrayNode('enabled_labels')
                                            ->children()
                                                ->scalarNode('enabled')->end()
                                                ->scalarNode('disabled')->end()
                                            ->end()
                                        ->end()

                                        // NEW
                                        ->scalarNode('generate_slug_from')->end()
                                        ->booleanNode('edit_after_creation')->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    public function addActionListColumnsNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('columns');
        return $treeBuilder->getRootNode()
            ->useAttributeAsKey('name')
            ->arrayPrototype()
                ->children()
                    ->enumNode('type')
                        ->values(['string', 'datetime', 'date', 'boolean', 'list', 'percentage'])
                    ->end()
                    ->scalarNode('label')->end()
                    ->scalarNode('fallback')->end()
                    ->booleanNode('clickable')->end()
                    ->booleanNode('orderable')->defaultTrue()->end()
                    ->booleanNode('translated')->end()
                    ->variableNode('options')
                        // TODO: Fix config
                    ->end()
                    ->arrayNode('merged_column_as_label')
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('label')->end()
                            ->enumNode('type')
                                ->values(['string', 'datetime', 'date', 'boolean'])
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    public function addActionListRowActionsNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('row_actions');
        return $treeBuilder->getRootNode()
            ->useAttributeAsKey('name')
            ->arrayPrototype()
                ->children()
                    ->scalarNode('label')->isRequired()->end()
                    ->scalarNode('action')->end()
                    ->scalarNode('count')->end()
                    ->variableNode('options')->end()
                    ->scalarNode('children_label')->end()
                    ->arrayNode('path_parameters')
                        ->useAttributeAsKey('parameter')
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    public function addActionListGeneralActionsNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('general_actions');
        return $treeBuilder->getRootNode()
            ->useAttributeAsKey('name')
            ->arrayPrototype()
                ->children()
                    ->scalarNode('label')->isRequired()->end()
                    ->arrayNode('path_parameters')
                        ->useAttributeAsKey('parameter')
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    public function addBackActionNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('back_action');
        return $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('name')->end()
                ->arrayNode('path_parameters')
                    ->useAttributeAsKey('parameter')
                    ->scalarPrototype()->end()
                ->end()
            ->end()
        ;
    }

    /**
     * @return NodeDefinition
     */
    public function addApisNode(): NodeDefinition
    {
        $treeBuilder = new TreeBuilder('apis');
        return $treeBuilder->getRootNode()
            ->isRequired()
            ->useAttributeAsKey('name')
            ->arrayPrototype()
                ->children()
                    ->scalarNode('reference')->isRequired()->end()
                    ->arrayNode('static_parameters')
                        ->useAttributeAsKey('parameter')
                        ->variablePrototype()->end()
                    ->end()
                    ->arrayNode('rename_path_parameters')
                        ->useAttributeAsKey('parameter')
                        ->scalarPrototype()->end()
                    ->end()
                ->end()
            ->end()
        ;
    }

    public function addFiltersNode(): NodeDefinition
        {
            $treeBuilder = new TreeBuilder('filters');
            return $treeBuilder->getRootNode()
                ->useAttributeAsKey('name')
                ->arrayPrototype()
                    ->children()
                        ->enumNode('type')
                            ->defaultValue('text')
                            ->values(['text', 'toggle', 'select'])
                        ->end()
                        ->scalarNode('label')->isRequired()->end()
                        ->enumNode('strategy')
                            ->defaultValue('partial')
                            ->values(['exact', 'partial', 'different'])
                        ->end()
                        ->variableNode('options')->end()
                    ->end()
                ->end()
            ;
        }
}
