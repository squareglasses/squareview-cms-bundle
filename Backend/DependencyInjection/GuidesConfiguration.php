<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class GuidesConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GuidesConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('guides')
                    ->useAttributeAsKey('section')
                    ->arrayPrototype()
                        ->children()
                            ->arrayNode('guides')
                                ->useAttributeAsKey('name')
                                ->arrayPrototype()
                                    ->children()
                                        ->variableNode('actions')
                                        //TODO define fixed configuration when it will be settled
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
