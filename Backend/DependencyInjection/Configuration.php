<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class Configuration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Configuration
{
    /**
     * @return ArrayNodeDefinition|NodeDefinition
     */
    public function getConfig(): ArrayNodeDefinition|NodeDefinition
    {
        $treeBuilder = new TreeBuilder('backend');
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('general')
                    ->children()
                        ->scalarNode('title')->end()
                        ->scalarNode('title_image')->end()
                        ->arrayNode('title_split')
                            ->scalarPrototype()->end()
                        ->end()
                        ->scalarNode('home_image')->end()
                        ->booleanNode('cache_clearable')->end()
                        ->booleanNode('error_report')->end()
                        ->arrayNode('apis')
                            ->useAttributeAsKey('name')
                            ->arrayPrototype()
                                ->useAttributeAsKey('type')
                                ->arrayPrototype()
                                    ->children()
                                        ->enumNode('method')->values(['GET', 'POST', 'PUT', 'DELETE'])->isRequired()->end()
                                        ->scalarNode('endpoint')->isRequired()->end()
                                        ->booleanNode('secured')->defaultTrue()->end()
                                    ->end()
                                ->end()
                            ->end()
                        ->end()
                        ->variableNode('meta_tags')
                            //TODO
                        ->end()
                        ->variableNode('layout_translations')
                        //TODO
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        $navigationConfiguration = new NavigationConfiguration();
        $navigationConfiguration->appendTo($rootNode);

        $sectionsConfiguration = new SectionsConfiguration();
        $sectionsConfiguration->appendTo($rootNode);

        $guidesConfiguration = new GuidesConfiguration();
        $guidesConfiguration->appendTo($rootNode);

        return $rootNode;
    }
}
