<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Security\Util;

use Exception;

/**
 * Class TokenGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TokenGenerator implements TokenGeneratorInterface
{
    /**
     * @return string
     * @throws Exception
     */
    public function generateToken(): string
    {
        return rtrim(strtr(base64_encode(random_bytes(32)), '+/', '-_'), '=');
    }
}
