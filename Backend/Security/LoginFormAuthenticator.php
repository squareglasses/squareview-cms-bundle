<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Security;

use Exception;
use SG\CmsBundle\Backend\Security\Model\UserManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Http\Authenticator\AbstractAuthenticator;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\CsrfTokenBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Badge\UserBadge;
use Symfony\Component\Security\Http\Authenticator\Passport\Credentials\PasswordCredentials;
use Symfony\Component\Security\Http\Authenticator\Passport\Passport;

/**
 * Class LoginFormAuthenticator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LoginFormAuthenticator extends AbstractAuthenticator
{
    public const LOGIN_ROUTE = 'backend_login';

    /**
     * @param UrlGeneratorInterface $urlGenerator
     * @param UserManager           $userManager
     */
    public function __construct(private readonly UrlGeneratorInterface $urlGenerator, private readonly UserManager $userManager)
    {
    }

    /**
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request): bool
    {
        return (self::LOGIN_ROUTE === $request->attributes->get('_route')
                ||self::LOGIN_ROUTE === $request->attributes->get('_route_base_name')
        )
            && $request->isMethod('POST');
    }

    /**
     * @param Request $request
     *
     * @return Passport
     */
    public function authenticate(Request $request): Passport
    {
        $username = $request->request->get('username');
        $password = $request->request->get('password');
        $csrfToken = $request->request->get('csrf_token');

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $username
        );

        return new Passport(
            new UserBadge($username, function ($userIdentifier) use ($request, $password) {
                try {
                    $user = $this->userManager->findUser($userIdentifier);
                } catch (Exception) {
                    $this->triggerError($request, 'Username could not be found.');
                }
                if (null === $user) {
                    $this->triggerError($request, 'Username could not be found.');
                }

                try {
                    $this->userManager->generateToken($userIdentifier, $password, $user);
                } catch (Exception $e) {
                    $this->triggerError($request, $e->getMessage());
                }
                return $user;
            }),
            new PasswordCredentials($password),
            [
                new CsrfTokenBadge('authenticate', $csrfToken),
                //new RememberMeBadge()
            ]
        );
    }

    /**
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $firewallName
     *
     * @return Response
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, string $firewallName): Response
    {
        // on success, let the request continue
        return new RedirectResponse($this->urlGenerator->generate("backend"));
    }

    /**
     * @param Request                 $request
     * @param AuthenticationException $exception
     *
     * @return Response
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception): Response
    {
//        $request->getSession()->set(
//            Security::AUTHENTICATION_ERROR,
//            strtr($exception->getMessageKey(), $exception->getMessageData())
//        );

        return new RedirectResponse($this->getLoginUrl($request));
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getLoginUrl(Request $request): string
    {
        return $this->urlGenerator->generate(self::LOGIN_ROUTE);
    }

    /**
     * @param Request $request
     * @param string  $message
     *
     * @return void
     */
    private function triggerError(Request $request, string $message): void
    {
        $request->getSession()->set(
            Security::AUTHENTICATION_ERROR,
            new CustomUserMessageAuthenticationException($message)
        );
        throw new CustomUserMessageAuthenticationException($message);
    }
}
