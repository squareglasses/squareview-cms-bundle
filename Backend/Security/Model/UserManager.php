<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Security\Model;

use Exception;
use JsonException;
use Psr\Cache\InvalidArgumentException;
use SG\CmsBundle\Common\Exception\HttpClientException;
use SG\CmsBundle\Common\HttpClient\ApiClient;
use SG\CmsBundle\Backend\Exception\InvalidUserInputException;
use SG\CmsBundle\Frontend\Exception\InvalidFormInputException;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class UserManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserManager
{
    /**
     * @param ApiClient           $apiClient
     * @param SerializerInterface $serializer
     */
    public function __construct(private readonly ApiClient $apiClient, private readonly SerializerInterface $serializer)
    {
    }

    /**
     * @param string $username
     *
     * @return UserInterface|null
     * @throws InvalidArgumentException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|HttpExceptionInterface
     */
    public function findUser(string $username): ?UserInterface
    {
        try {
            $response = $this->apiClient->apiRequest(
                'users/'.$username.'?serialization_groups[]=get&serialization_groups[]=security',
                ApiClient::METHOD_GET,
                [],
                false
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $this->deserializeUser($response->getContent());
            }
        } catch (Exception) {
            $e = new UserNotFoundException();
            $e->setUserIdentifier($username);
            throw $e;
        }

        return null;
    }

    /**
     * @param string $token
     *
     * @return \SG\CmsBundle\Frontend\Security\Model\UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|InvalidFormInputException
     */
    public function findUserByConfirmationToken(string $token): ?UserInterface
    {
        $response = $this->apiClient->apiRequest(
            'users/by-confirmation-token/'.$token,
            ApiClient::METHOD_GET,
            [],
            false
        );
        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent());
        }
        return null;
    }

    /**
     * @param UserInterface $user
     * @param bool          $loggedUserRequest
     * @param array         $groups
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|InvalidFormInputException
     */
    public function updateUser(UserInterface $user, bool $loggedUserRequest = true, array $groups = []): ?UserInterface
    {
        if (empty($groups)) {
            $groups = ['put'];
        }
        if (!$this->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }
        $jsonUser = $this->serializer->serialize($user, 'json', ['groups' => $groups]);

        if ($loggedUserRequest) {
            $response = $this->apiClient->userRequest($user, 'users', ApiClient::METHOD_PUT, [
                'body' => $jsonUser
            ]);
        } else {
            $response = $this->apiClient->apiRequest('users/'.$user->getId(), ApiClient::METHOD_PUT, [
                'body' => $jsonUser
            ]);
        }

        return $this->deserializeUser($response->getContent(), $user);
    }

    /**
     * @param string             $serializedValue
     * @param UserInterface|null $user
     *
     * @return UserInterface
     */
    private function deserializeUser(string $serializedValue, UserInterface $user = null): UserInterface
    {
        $context = [];
        if ($user !== null) {
            $context = [
                AbstractNormalizer::OBJECT_TO_POPULATE => $user,
                'groups' => ['get']
            ];
        }
        /** @var UserInterface $user */
        return $this->serializer->deserialize($serializedValue, User::class, 'json', $context);
    }

    /**
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isSupported(UserInterface $user): bool
    {
        if (!is_a($user, User::class)) {
            return false;
        }
        return true;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|InvalidFormInputException
     */
    public function refreshUser(UserInterface $user): ?UserInterface
    {
        try {
            $response = $this->apiClient->userRequest(
                $user,
                'users/refresh',
                ApiClient::METHOD_GET,
                [],
                false
            );
            $statusCode = $response->getStatusCode();
            if ($statusCode === 200) {
                return $this->deserializeUser($response->getContent(), $user);
            }
            return null;
        } catch (HttpClientException) {
            return null;
        }
    }

    /**
     * @param string        $username
     * @param string        $password
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|InvalidFormInputException
     */
    public function generateToken(string $username, string $password, UserInterface $user): ?UserInterface
    {
        $tokenResponse = $this->apiClient->request(
            'authentication_token',
            ApiClient::METHOD_POST,
            [
                'body' => json_encode([
                    'username' => $username,
                    'password' => $password
                ], JSON_THROW_ON_ERROR)
            ],
            false,
            null,
            false
        );
        if ($tokenResponse->getStatusCode() === 200) {
            return $this->deserializeUser($tokenResponse->getContent(), $user);
        }

        throw new CustomUserMessageAuthenticationException('Cannot authenticate user.');
    }

    /**
     * @param UserInterface|BaseUserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|InvalidFormInputException
     */
    public function refreshToken(UserInterface|BaseUserInterface $user): ?UserInterface
    {
        if (!$user->getRefreshToken()) {
            return null;
        }
        $response = $this->apiClient->request('authentication_token/refresh', ApiClient::METHOD_POST, [
            'body' => json_encode(['refreshToken' => $user->getRefreshToken()], JSON_THROW_ON_ERROR)
        ]);
        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent(), $user);
        }
        return null;
    }

    /**
     * @param UserInterface $user
     *
     * @return UserInterface|null
     * @throws ClientExceptionInterface
     * @throws HttpExceptionInterface
     * @throws InvalidArgumentException
     * @throws InvalidUserInputException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|InvalidFormInputException
     */
    public function resetPassword(UserInterface $user): ?UserInterface
    {
        if (!$this->isSupported($user)) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        // Reset password request
        //$user->setConfirmationToken(null);
        //$user->setPasswordRequestedAt(null);

        $jsonUser = $this->serializer->serialize($user, 'json', ['groups' => 'reset-password']);
        $response = $this->apiClient->apiRequest('users/reset-password/'.$user->getId(), ApiClient::METHOD_PUT, [
            'body' => $jsonUser
        ]);

        $statusCode = $response->getStatusCode();
        if ($statusCode === 200) {
            return $this->deserializeUser($response->getContent(), $user);
        }

        if ($statusCode === 400) {
            $error = json_decode($response->getContent(false), true, 512, JSON_THROW_ON_ERROR);
            if (array_key_exists("violations", $error)) {
                throw new InvalidUserInputException($error['title'], $error['violations']);
            }
        }
        throw new InvalidUserInputException("Unknown error");
    }
}
