<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Security\Model;

use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use DateTimeInterface;

/**
 * Class User
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    public const USER_LOCKED_MESSAGE = 'Your user account is locked.';
    public const USER_DISABLED_MESSAGE = 'Your user account is disabled.';
    public const USER_NOT_FOUND_MESSAGE = 'Your user account is disabled or no longer exists.';

    /**
     * @Groups({"get"})
     */
    protected ?string $id = null;

    /**
     * @Groups({"get", "post", "put"})
     */
    protected ?string $username = null;

    /**
     * @Groups({"get", "post", "put"})
     */
    protected ?string $email = null;

    /**
     * @Groups({"get", "post", "put"})
     */
    protected ?bool $termsAccepted = false;

    /**
     * @Groups({"get"})
     */
    protected array $roles = [];

    /**
     * @Groups({"get"})
     */
    protected ?string $token = null;

    /**
     * @Groups({"get"})
     */
    protected ?string $refreshToken = null;

    /**
     * @Groups({"get", "post", "put", "resetting-request", "reset-password"})
     */
    protected ?string $confirmationToken = null;

    /**
     * @Groups({"get", "resetting-request", "reset-password"})
     */
    protected ?DateTimeInterface $passwordRequestedAt = null;

    /**
     * @Groups({"get", "post", "change-password", "reset-password"})
     */
    protected ?string $salt = null;

    /**
     * @Groups({"get", "post", "change-password", "reset-password"})
     */
    protected ?string $password = null;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     */
    protected ?string $plainPassword = null;

    /**
     * @Groups({"get", "post", "put", "reset-password"})
     */
    protected bool $enabled = false;

    /**
     * @Groups({"get"})
     */
    protected bool $locked = false;

    /**
     * @Groups({"get", "post", "update-last-login"})
     */
    protected ?DateTimeInterface $lastLogin = null;

    /**
     * @Groups({"get", "post", "put"})
     */
    protected ?array $personalInfos = null;

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->getUsername();
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     */
    public function setUsername(string $username): void
    {
        $this->username = $username;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     */
    public function setEmail(string $email = null): void
    {
        $this->email = $email;
    }

    /**
     * @return bool|null
     */
    public function getTermsAccepted(): ?bool
    {
        return $this->termsAccepted;
    }

    /**
     * @param bool|null $termsAccepted
     */
    public function setTermsAccepted(?bool $termsAccepted): void
    {
        $this->termsAccepted = $termsAccepted;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $token
     */
    public function setToken(?string $token): void
    {
        $this->token = $token;
    }

    /**
     * @return string|null
     */
    public function getRefreshToken(): ?string
    {
        return $this->refreshToken;
    }

    /**
     * @param string|null $refreshToken
     */
    public function setRefreshToken(?string $refreshToken): void
    {
        $this->refreshToken = $refreshToken;
    }

    /**
     * @return string|null
     */
    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    /**
     * @param string|null $confirmationToken
     */
    public function setConfirmationToken(?string $confirmationToken): void
    {
        $this->confirmationToken = $confirmationToken;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPasswordRequestedAt(): ?DateTimeInterface
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param DateTimeInterface|null $passwordRequestedAt
     */
    public function setPasswordRequestedAt(?DateTimeInterface $passwordRequestedAt): void
    {
        $this->passwordRequestedAt = $passwordRequestedAt;
    }

    /**
     * @param int $ttl
     * @return bool
     */
    public function isPasswordRequestNonExpired(int $ttl): bool
    {
        return $this->getPasswordRequestedAt() instanceof DateTimeInterface &&
            $this->getPasswordRequestedAt()->getTimestamp() + $ttl > time();
    }

    /**
     * @see UserInterface
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void
    {
        $this->password = $password;
    }

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string
    {
        return $this->plainPassword;
    }

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword): void
    {
        $this->plainPassword = $plainPassword;
    }

    /**
     * @return string|null
     */
    public function getSalt(): ?string
    {
        return $this->salt;
    }

    /**
     * @param string|null $salt
     * @return $this
     */
    public function setSalt(?string $salt = null): self
    {
        $this->salt = $salt;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        $this->plainPassword = null;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void
    {
        $this->enabled = $enabled;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @return bool
     */
    public function isAccountNonLocked(): bool
    {
        return !$this->locked;
    }

    /**
     * @param bool $locked
     */
    public function setLocked(bool $locked): void
    {
        $this->locked = $locked;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTimeInterface|null $lastLogin
     */
    public function setLastLogin(?DateTimeInterface $lastLogin = null): void
    {
        $this->lastLogin = $lastLogin;
    }

    /**
     * @return array|null
     */
    public function getPersonalInfos(): ?array
    {
        return $this->personalInfos;
    }

    /**
     * @param array|null $personalInfos
     */
    public function setPersonalInfos(?array $personalInfos): void
    {
        $this->personalInfos = $personalInfos;
    }

    public function getFullName(): ?string
    {
        if ($this->personalInfos === null) {
            return null;
        }
        return $this->personalInfos['firstName']. ' '.$this->personalInfos['lastName'] ?? $this->getUsername();
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        if ($this->personalInfos === null) {
            return null;
        }
        return $this->personalInfos['firstName'] ?? null;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        if ($this->personalInfos === null) {
            return null;
        }
        return $this->personalInfos['lastName'] ?? null;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        if ($this->personalInfos === null) {
            return null;
        }
        return $this->personalInfos['phone'] ?? null;
    }
}
