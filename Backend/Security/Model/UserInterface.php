<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Security\Model;

use Symfony\Component\Security\Core\User\UserInterface as BaseUserInterface;
use DateTimeInterface;

/**
 * Interface UserInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface UserInterface extends BaseUserInterface
{
    /**
     * @return string
     */
    public function __toString(): string;

    /**
     * @return string|null
     */
    public function getId(): ?string;

    /**
     * @param string $id
     *
     * @return $this
     */
    public function setId(string $id): self;

    /**
     * @return string|null
     */
    public function getUsername(): ?string;

    /**
     * @param string $username
     */
    public function setUsername(string $username): void;

    /**
     * @return string|null
     */
    public function getEmail(): ?string;

    /**
     * @param string|null $email
     */
    public function setEmail(string $email = null): void;

    /**
     * @return string|null
     */
    public function getToken(): ?string;

    /**
     * @param string|null $token
     */
    public function setToken(?string $token): void;

    /**
     * @return string|null
     */
    public function getRefreshToken(): ?string;

    /**
     * @param string|null $refreshToken
     */
    public function setRefreshToken(?string $refreshToken): void;

    /**
     * @return string|null
     */
    public function getConfirmationToken(): ?string;

    /**
     * @param string|null $confirmationToken
     */
    public function setConfirmationToken(?string $confirmationToken): void;

    /**
     * @return DateTimeInterface|null
     */
    public function getPasswordRequestedAt(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $passwordRequestedAt
     */
    public function setPasswordRequestedAt(?DateTimeInterface $passwordRequestedAt): void;

    /**
     * @param int $ttl
     *
     * @return bool
     */
    public function isPasswordRequestNonExpired(int $ttl): bool;

    /**
     * @return array
     */
    public function getRoles(): array;

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self;

    /**
     * @return string|null
     */
    public function getPassword(): ?string;

    /**
     * @param string|null $password
     */
    public function setPassword(?string $password): void;

    /**
     * @return string|null
     */
    public function getPlainPassword(): ?string;

    /**
     * @param string|null $plainPassword
     */
    public function setPlainPassword(?string $plainPassword): void;

    /**
     * @return string|null
     */
    public function getSalt(): ?string;

    /**
     * @return void
     */
    public function eraseCredentials(): void;

    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @param bool $enabled
     */
    public function setEnabled(bool $enabled): void;

    /**
     * @return bool
     */
    public function isLocked(): bool;

    /**
     * @return bool
     */
    public function isAccountNonLocked(): bool;

    /**
     * @param bool $locked
     */
    public function setLocked(bool $locked): void;

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface;

    /**
     * @param DateTimeInterface|null $lastLogin
     */
    public function setLastLogin(?DateTimeInterface $lastLogin = null): void;

    /**
     * @return array|null
     */
    public function getPersonalInfos(): ?array;

    /**
     * @param array|null $personalInfos
     */
    public function setPersonalInfos(?array $personalInfos): void;

    /**
     * @return string|null
     */
    public function getFirstName(): ?string;

    /**
     * @return string|null
     */
    public function getLastName(): ?string;
}
