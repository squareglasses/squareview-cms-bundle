<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Backend\Security;

use SG\CmsBundle\Backend\Security\Model\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!is_a($user, $this->getUserClass())) {
            return;
        }

        if ($user->isLocked()) {
            throw new CustomUserMessageAccountStatusException(User::USER_LOCKED_MESSAGE);
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPostAuth(UserInterface $user): void
    {
        if (!is_a($user, $this->getUserClass())) {
            return;
        }
    }

    /**
     * @return string
     */
    private function getUserClass(): string
    {
        return User::class;
    }
}
