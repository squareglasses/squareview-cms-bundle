<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Entity\Menu;

/**
 * Class MenuRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MenuRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Menu::class);
    }

    /**
     * @param string $identifier
     *
     * @return Menu|null
     * @throws NonUniqueResultException
     */
    public function findOneBySlugOrId(string $identifier): ?Menu
    {
        return $this->createQueryBuilder("m")
            ->where("m.id = :val")
            ->orWhere("m.slug = :val")
            ->setParameter('val', $identifier)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
