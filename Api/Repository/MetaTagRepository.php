<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use SG\CmsBundle\Api\Entity\MetaTag;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MetaTag|null find($id, $lockMode = null, $lockVersion = null)
 * @method MetaTag|null findOneBy(array $criteria, array $orderBy = null)
 * @method MetaTag[]    findAll()
 * @method MetaTag[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MetaTagRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MetaTag::class);
    }
}
