<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Entity\Translation;
use SG\CmsBundle\Api\Entity\TranslationKey;

/**
 * Class TranslationKeyRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationKeyRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TranslationKey::class);
    }

    /**
     * @param string      $name
     * @param string      $zone
     * @param int|null $resourceIdentifier
     * @param string|null $resourceClass
     *
     * @return TranslationKey|null
     * @throws NonUniqueResultException
     */
    public function findExisting(string $name, string $zone, ?int $resourceIdentifier, ?string $resourceClass): ?TranslationKey
    {
        $qb = $this->createQueryBuilder("tk")
            ->andWhere("tk.name = :name")
            ->andWhere("tk.zone = :zone")
            ->setParameter('name', $name)
            ->setParameter('zone', $zone);

        if (null === $resourceClass) {
            $qb->andWhere("tk.resourceClass is NULL");
        } else {
            $qb->andWhere("tk.resourceClass = :resourceClass")
                ->setParameter("resourceClass",  $resourceClass);
        }
        if (null === $resourceIdentifier) {
            $qb->andWhere("tk.resourceIdentifier is NULL");
        } else {
            $qb->andWhere("tk.resourceIdentifier = :resourceIdentifier")
                ->setParameter("resourceIdentifier",  $resourceIdentifier);
        }
        $qb->orderBy("tk.createdAt", "DESC")
            ->setMaxResults(1);
        return $qb->getQuery()->getOneOrNullResult();
    }
}
