<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
abstract class UserRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     * @param string          $userClass
     */
    public function __construct(ManagerRegistry $registry, string $userClass = User::class)
    {
        parent::__construct($registry, $userClass);
    }

    /**
     * @param string $id
     *
     * @return User|null
     * @throws NonUniqueResultException
     */
    public function findByUsernameEmailOrId(string $id): ?User
    {
        return $this->createQueryBuilder('u')
            ->andWhere('u.id = :val')
            ->orWhere('u.username = :val')
            ->orWhere('u.email = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
