<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\Mapping\MappingException;
use ReflectionException;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Entity\Route;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Route|null find($id, $lockMode = null, $lockVersion = null)
 * @method Route|null findOneBy(array $criteria, array $orderBy = null)
 * @method Route[]    findAll()
 * @method Route[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RouteRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Route::class);
    }

    /**
     * @param EntityManagerInterface $entityManager
     *
     * @return $this
     */
    public function setEntityManager(EntityManagerInterface $entityManager): self
    {
        $this->_em = $entityManager;

        return $this;
    }

    /**
     * @param Routable $routable
     * @param string   $locale
     *
     * @return Route|null
     * @throws MappingException
     * @throws NonUniqueResultException
     * @throws ReflectionException
     */
    public function findOneByResource(Routable $routable, string $locale): ?Route
    {
        $entityName = $this->_em->getMetadataFactory()->getMetadataFor(get_class($routable))->getName();
        return $this->findOneByResourceClassAndIdentifier(
            $entityName,
            $routable->getResourceIdentifier(),
            $locale
        );
    }

    /**
     * @param string      $class
     * @param int $identifier
     * @param string      $locale
     *
     * @return Route|null
     * @throws NonUniqueResultException
     */
    public function findOneByResourceClassAndIdentifier(string $class, int $identifier, string $locale): ?Route
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.resourceClass = :class')
            ->andWhere('r.resourceIdentifier = :identifier')
            ->andWhere('r.locale = :locale')
            ->setParameter('class', $class)
            ->setParameter('identifier', $identifier)
            ->setParameter('locale', $locale)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    /**
     * @param Routable $routable
     *
     * @return array
     * @throws MappingException
     * @throws ReflectionException
     */
    public function findByResource(Routable $routable): array
    {
        $entityName = $this->_em->getMetadataFactory()->getMetadataFor(get_class($routable))->getName();
        return $this->findByResourceClassAndIdentifier(
            $entityName,
            $routable->getResourceIdentifier()
        );
    }

    /**
     * @param string      $class
     * @param int $identifier
     *
     * @return array
     */
    public function findByResourceClassAndIdentifier(string $class, int $identifier): array
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.resourceClass = :class')
            ->andWhere('r.resourceIdentifier = :identifier')
            ->setParameter('class', $class)
            ->setParameter('identifier', $identifier)
            ->getQuery()
            ->getResult()
        ;
    }
}
