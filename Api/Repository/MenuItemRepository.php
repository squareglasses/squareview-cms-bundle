<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Entity\MenuItem;

/**
 * Class MenuItemRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MenuItemRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuItem::class);
    }

    /**
     * @param CmsResourceInterface $resource
     *
     * @return float|int|mixed|string
     */
    public function removeByResource(CmsResourceInterface $resource): mixed
    {
        return $this->createQueryBuilder("mi")
            ->delete()
            ->where("mi.targetResourceId = :resourceIdentifier")
            ->andWhere("mi.targetResourceClass = :resourceClass")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->getQuery()->getResult();
    }

    /**
     * @param string|int $id
     *
     * @return MenuItem|null
     * @throws NonUniqueResultException
     */
    public function findOneBySlugOrId(string|int $id): ?MenuItem
    {
        return $this->createQueryBuilder('item')
            ->andWhere('item.id = :val')
            ->orWhere('item.slug = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
