<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Entity\Gallery;

/**
 * Class GalleryRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GalleryRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Gallery::class);
    }

    /**
     * @param HasGalleriesInterface $resource
     *
     * @return array
     */
    public function findByResource(HasGalleriesInterface $resource): array
    {
        return $this->createQueryBuilder("rhm")
            ->where("rhm.resourceIdentifier = :resourceIdentifier")
            ->andWhere("rhm.resourceClass = :resourceClass")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->getQuery()->getResult();
    }
}
