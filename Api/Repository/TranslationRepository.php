<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Entity\Translation;
use SG\CmsBundle\Api\Entity\TranslationKey;

/**
 * Class TranslationRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Translation::class);
    }

    /**
     * @param TranslationKey $translationKey
     * @param string         $locale
     *
     * @return Translation|null
     * @throws NonUniqueResultException
     */
    public function findOneByKeyAndLocale(TranslationKey $translationKey, string $locale): ?Translation
    {
        $qb = $this->createQueryBuilder("t")
            ->innerJoin('t.language', 'l')
            ->innerJoin('t.translationKey', 'tk')
            ->where("l.id = :locale")
            ->andWhere("tk.id = :keyId")
            ->setParameter('keyId', $translationKey->getId())
            ->setParameter('locale', $locale);
        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @param Translatable $translatable
     * @param string       $locale
     *
     * @return array
     */
    public function findByTranslatable(Translatable $translatable, string $locale): array
    {
        $qb = $this->createQueryBuilder("t")
            ->innerJoin('t.language', 'l')
            ->innerJoin('t.translationKey', 'tk')
            ->where("l.id = :locale")
            ->andWhere("tk.resourceClass = :resourceClass")
            ->andWhere("tk.resourceIdentifier = :resourceIdentifier")
            ->setParameter('resourceClass', $translatable->getTranslatableClass())
            ->setParameter('resourceIdentifier', $translatable->getTranslatableIdentifier())
            ->setParameter('locale', $locale)
        ;
        return $qb->getQuery()->getResult();
    }
}
