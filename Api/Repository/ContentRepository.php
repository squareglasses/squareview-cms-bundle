<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use App\Entity\Content;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Entity\AbstractContent;
use SG\CmsBundle\Common\Bag\ConfigurationBag;

/**
 * @method AbstractContent|null find($id, $lockMode = null, $lockVersion = null)
 * @method AbstractContent|null findOneBy(array $criteria, array $orderBy = null)
 * @method AbstractContent[]    findAll()
 * @method AbstractContent[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry  $registry
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(ManagerRegistry $registry, ConfigurationBag $configurationBag)
    {
        $contentClass = $configurationBag->getContentClass();
        parent::__construct($registry, $contentClass);
    }

    /**
     * @param string $id
     *
     * @return Content|null
     * @throws NonUniqueResultException
     */
    public function findBySlugOrId(string $id): ?Content
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.id = :val')
            ->orWhere('c.slug = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
}
