<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Contracts\WebsiteRepositoryInterface;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Api\Contracts\WebsiteInterface;

/**
 * @method WebsiteInterface|null find($id, $lockMode = null, $lockVersion = null)
 * @method WebsiteInterface|null findOneBy(array $criteria, array $orderBy = null)
 * @method WebsiteInterface[]    findAll()
 * @method WebsiteInterface[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WebsiteRepository extends ServiceEntityRepository implements WebsiteRepositoryInterface
{
    /**
     * @param ManagerRegistry  $registry
     * @param ConfigurationBag $configurationBag
     */
    public function __construct(ManagerRegistry $registry, ConfigurationBag $configurationBag)
    {
        parent::__construct($registry, $configurationBag->getWebsiteClass());
    }

    /**
     * @param string $identifier
     *
     * @return WebsiteInterface|null
     * @throws NonUniqueResultException
     */
    public function findOneBySlugOrId(string $identifier): ?WebsiteInterface
    {
        return $this->createQueryBuilder("w")
            ->where("w.id = :id")
            ->orWhere("w.slug = :id")
            ->setParameter('id', $identifier)
            ->getQuery()->getOneOrNullResult();
    }
}
