<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\ORM\NonUniqueResultException;
use SG\CmsBundle\Api\Entity\TranslationsPublication;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class TranslationsPublicationRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationsPublicationRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TranslationsPublication::class);
    }

    /**
     * @param string $locale
     * @param string $guideIdentifier
     *
     * @return TranslationsPublication|null
     * @throws NonUniqueResultException
     */
    public function findActive(string $locale, string $guideIdentifier): ?TranslationsPublication
    {
        return $this->createQueryBuilder("publication")
            ->where("publication.locale = :locale")
            ->andWhere("publication.guideIdentifier = :guideIdentifier")
            ->setParameter("locale", $locale)
            ->setParameter("guideIdentifier", $guideIdentifier)
            ->orderBy("publication.version", "DESC")
            ->setFirstResult(0)
            ->setMaxResults(1)
            ->getQuery()->getOneOrNullResult();
    }
}
