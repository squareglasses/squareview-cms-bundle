<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;

/**
 * Class ResourceHasMediaRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceHasMediaRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ResourceHasMedia::class);
    }

    /**
     * @param HasMediasInterface $resource
     *
     * @return float|int|mixed|string
     */
    public function findByResource(HasMediasInterface $resource): mixed
    {
        return $this->createQueryBuilder("rhm")
            ->where("rhm.resourceIdentifier = :resourceIdentifier")
            ->andWhere("rhm.resourceClass = :resourceClass")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->getQuery()->getResult();
    }

    /**
     * @param HasMediasInterface $resource
     *
     * @return float|int|mixed|string
     */
    public function removeByResource(HasMediasInterface $resource): mixed
    {
        return $this->createQueryBuilder("rhm")
            ->delete()
            ->where("rhm.resourceIdentifier = :resourceIdentifier")
            ->andWhere("rhm.resourceClass = :resourceClass")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->getQuery()->getResult();
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     *
     * @return ResourceHasMedia|null
     * @throws NonUniqueResultException
     */
    public function findByResourceAndIdentifier(HasMediasInterface $resource, string $identifier): ?ResourceHasMedia
    {
        return $this->createQueryBuilder("rhm")
            ->andWhere('rhm.resourceClass = :rClass')
            ->andWhere('rhm.resourceIdentifier = :rId')
            ->andWhere('rhm.identifier = :identifier')
            ->setParameter('rClass', $resource->getResourceClass())
            ->setParameter('rId', $resource->getResourceIdentifier())
            ->setParameter('identifier', $identifier)
            ->getQuery()->getOneOrNullResult();
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     *
     * @return bool
     * @throws NonUniqueResultException
     */
    public function resourceHasMediaByIdentifier(HasMediasInterface $resource, string $identifier): bool
    {
        $res = $this->createQueryBuilder("rhm")
            ->select("rhm.id as total")
            ->andWhere('rhm.resourceClass = :rClass')
            ->andWhere('rhm.resourceIdentifier = :rId')
            ->andWhere('rhm.identifier = :identifier')
            ->setParameter('rClass', $resource->getResourceClass())
            ->setParameter('rId', $resource->getResourceIdentifier())
            ->setParameter('identifier', $identifier)
            ->getQuery()->getOneOrNullResult(AbstractQuery::HYDRATE_SCALAR);
        return $res !== null && count($res) === 1;
    }
}
