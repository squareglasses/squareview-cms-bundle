<?php

namespace SG\CmsBundle\Api\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;

/**
 * Class MediaRepository
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaRepository extends ServiceEntityRepository
{
    /**
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Media::class);
    }
}
