<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface GalleryHasMediaInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface GalleryHasMediaInterface
{
    /*
     * @param boolean $enabled
     *
     * @return void
     */
    //public function setEnabled($enabled);

    /*
     * @return boolean
     */
    //public function getEnabled();

    /*
     * Get the gallery
     *
     * @return GalleryInterface
     */
    //public function getGallery();

    /*
     * Set the gallery
     *
     * @param GalleryInterface $gallery
     */
    //public function setGallery(GalleryInterface $gallery = null);

    /*
     * Set the media
     *
     * @param MediaInterface $media
     */
    //public function setMedia(MediaInterface $media = null);

    /*
     * Get the media
     *
     * @return MediaInterface
     */
    //public function getMedia();

    /*
     * Set the position
     *
     * @param int $position
     * @return int
     */
    //public function setPosition($position);

    /*
     * Get the position
     *
     * @return int
     */
    //public function getPosition();

    /*
     * Set the update time
     *
     * @param \DateTime|null $updatedAt
     */
    //public function setUpdatedAt(\DateTime $updatedAt = null);

    /*
     * Get the update time
     *
     * @return \DateTime
     */
    //public function getUpdatedAt();

    /*
     * Set the creation time
     *
     * @param \DateTime|null $createdAt
     */
    //public function setCreatedAt(\DateTime $createdAt = null);

    /*
     * Get the creation time
     *
     * @return \DateTime
     */
    //public function getCreatedAt();

    /*
     * @return string
     */
    //public function __toString();
}
