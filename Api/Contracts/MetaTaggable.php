<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\MetaTag;

/**
 * Interface MetaTaggable
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface MetaTaggable
{
    /**
     * Get the identifier of the taggable resource.
     * @return int
     */
    public function getResourceIdentifier(): int;

    /**
     * Get the class namespace of the taggable resource.
     *
     * @return string
     */
    public function getResourceClass(): string;

    /**
     * @return string
     */
    public function getDiscriminator(): string;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return MetaTag[]
     */
    public function getMetatags(): array;

    /**
     * @param MetaTag[] $metatags
     */
    public function setMetatags(array $metatags): void;
}
