<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface MediaInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface MediaInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self;

    /**
     * @return string
     */
    public function getProviderName(): string;

    /**
     * @param string $providerName
     * @return $this
     */
    public function setProviderName(string $providerName): self;

    /**
     * @return string
     */
    public function getProviderReference(): string;

    /**
     * @param string $providerReference
     * @return $this
     */
    public function setProviderReference(string $providerReference): self;

    /**
     * @return array|null
     */
    public function getProviderMetadata(): ?array;

    /**
     * @param array $metadata
     * @return $this
     */
    public function setProviderMetadata(array $metadata): self;

    /**
     * @return string|null
     */
    public function getContentType(): ?string;

    /**
     * @param string $contentType
     * @return $this
     */
    public function setContentType(string $contentType): self;

    /**
     * @return int|null
     */
    public function getContentSize(): ?int;

    /**
     * @param int|null $size
     * @return $this
     */
    public function setContentSize(int $size = null): self;

    /**
     * @return int|null
     */
    public function getWidth(): ?int;

    /**
     * @param int|null $width
     * @return $this
     */
    public function setWidth(int $width = null): self;

    /**
     * @return int|null
     */
    public function getHeight(): ?int;

    /**
     * @param int|null $height
     * @return $this
     */
    public function setHeight(int $height = null): self;

    /**
     * @return float|null
     */
    public function getLength(): ?string;

    /**
     * @param string|null $length
     * @return $this
     */
    public function setLength(string $length = null): self;

    /**
     * @return string|null
     */
    public function getAuthor(): ?string;

    /**
     * @param string|null $author
     * @return $this
     */
    public function setAuthor(string $author = null): self;
}
