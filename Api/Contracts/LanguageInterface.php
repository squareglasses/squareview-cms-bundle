<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface LanguageInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface LanguageInterface
{
//    /**
//     * @return string|null
//     */
//    public function getId(): ?string;
//
//    /**
//     * @param string $locale
//     */
//    public function setId(string $locale): void;
//
//    /**
//     * @return string
//     */
//    public function getName();
//
//    /**
//     * @param string $name
//     */
//    public function setName(string $name): void;
//
//    /**
//     * @return int
//     */
//    public function getPosition(): ?int;
//
//    /**
//     * @param int|null $position
//     */
//    public function setPosition(?int $position = null): void;
//
//    /**
//     * @return bool
//     */
//    public function getEnabled(): bool;
//
//    /**
//     * @param bool|null $enabled
//     */
//    public function setEnabled(?bool $enabled = null): void;
}
