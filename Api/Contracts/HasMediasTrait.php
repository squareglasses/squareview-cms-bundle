<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Symfony\Component\Serializer\Annotation\Groups;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;

/**
 * Trait HasMediasTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait HasMediasTrait
{
    /**
     * To expose discriminator (found in interface) by default in GET serialization, needed for media management
     * @var string
     * @Groups({"get"})
     */
    protected string $discriminator;

    /**
     * @var ResourceHasMedia[]
     * @Groups({"medias", "publish"})
     */
    protected array $medias = [];

    /**
     * @return ResourceHasMedia[]
     */
    public function getMedias(): array
    {
        return $this->medias;
    }

    /**
     * @param ResourceHasMedia[] $medias
     */
    public function setMedias(array $medias): void
    {
        $this->medias = $medias;
    }
}
