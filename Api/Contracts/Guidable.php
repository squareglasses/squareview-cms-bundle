<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface Guidable
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface Guidable
{
    /**
     * @return string|null
     */
    public function getGuide(): ?string;

    /**
     * @param string $guide
     * @return Guidable
     */
    public function setGuide(string $guide): Guidable;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return string
     */
    public function getDiscriminator(): string;
}
