<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Api\Entity\ModuleParameter;

/**
 * Interface ModuleInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ModuleInterface
{
    /**
     * @return int|null
     */
    public function getId(): int|null;

    /**
     * @return int
     */
    public function getModulableIdentifier(): int;

    /**
     * @param int $modulableIdentifier
     * @return $this
     */
    public function setModulableIdentifier(int $modulableIdentifier): self;

    /**
     * @return string
     */
    public function getModulableClass(): string;

    /**
     * @param string $modulableClass
     * @return ModuleInterface
     */
    public function setModulableClass(string $modulableClass): self;

    /**
     * @return string
     */
    public function getGuideIdentifier(): string;

    /**
     * @param string $guideIdentifier
     * @return $this
     */
    public function setGuideIdentifier(string $guideIdentifier): self;

    /**
     * @return Collection
     */
    public function getParameters(): Collection;

    /**
     * @param ModuleParameter $parameter
     * @return $this
     */
    public function addParameter(ModuleParameter $parameter): self;

    /**
     * @param ModuleParameter $parameter
     * @return $this
     */
    public function removeParameter(ModuleParameter $parameter): self;

    /**
     * @return int|null
     */
    public function getPosition(): ?int;

    /**
     * @param int|null $position
     * @return $this
     */
    public function setPosition(?int $position = null): self;

    /**
     * @return string
     */
    public function getLocale(): string;

    /**
     * @param string $locale
     * @return $this
     */
    public function setLocale(string $locale): self;

    /**
     * @return string|null
     */
    public function getZone(): ?string;

    /**
     * @param string|null $zone
     * @return $this
     */
    public function setZone(?string $zone = null): self;
}
