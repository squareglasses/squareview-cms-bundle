<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use ReflectionException;

/**
 * Interface WebsiteInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface WebsiteInterface
{
    /**
     * @return int|null
     */
    public function getId(): ?int;

    /**
     * @return int
     */
    public function getResourceIdentifier(): int;

    /**
     * @return string
     * @throws ReflectionException
     */
    public function getResourceClass(): string;

    /**
     * @return string
     */
    public function getDiscriminator(): string;

    /**
     * @return string|null
     */
    public function getName(): ?string;

    /**
     * @param string $name
     */
    public function setName(string $name): void;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @param string|null $slug
     * @return $this
     */
    public function setSlug(?string $slug = null): self;
}
