<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface UserInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface UserInterface extends \Symfony\Component\Security\Core\User\UserInterface
{
    public const ROLE_DEFAULT = "ROLE_USER";

    public function getApiKey(): ?string;
}
