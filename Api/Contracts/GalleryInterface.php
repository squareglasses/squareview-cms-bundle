<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface GalleryInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface GalleryInterface
{
//    /**
//     * Get name.
//     *
//     * @return string $name
//     */
//    public function getName();
//
//    /**
//     * Set name.
//     *
//     * @param string $name
//     */
//    public function setName($name);
//
//    /**
//     * Get updated_at.
//     *
//     * @return \Datetime $updatedAt
//     */
//    public function getUpdatedAt();
//
//    /**
//     * Set updated_at.
//     *
//     * @param \Datetime $updatedAt
//     */
//    public function setUpdatedAt(\DateTime $updatedAt);
//
//    /**
//     * Get created_at.
//     *
//     * @return \Datetime $createdAt
//     */
//    public function getCreatedAt();
//
//    /**
//     * Set created_at.
//     *
//     * @param \Datetime $createdAt
//     */
//    public function setCreatedAt(\DateTime $createdAt);
//
//    /**
//     * Get the default format.
//     *
//     * @return string
//     */
//    public function getDefaultFormat();
//
//    /**
//     * Set the default format.
//     *
//     * @param string $defaultFormat
//     */
//    public function setDefaultFormat($defaultFormat);
//
//    /**
//     * Return a string representation of the gallery.
//     *
//     * @return string
//     */
//    public function __toString();
}
