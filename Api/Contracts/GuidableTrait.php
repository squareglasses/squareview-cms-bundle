<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait GuidableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait GuidableTrait
{
    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"get", "publish"})
     */
    protected ?string $guide = null;

    /**
     * @return string|null
     */
    public function getGuide(): ?string
    {
        return $this->guide;
    }

    /**
     * @param string $guide
     * @return Guidable
     */
    public function setGuide(string $guide): Guidable
    {
        $this->guide = $guide;

        return $this;
    }
}
