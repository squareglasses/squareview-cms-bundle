<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Api\Entity\Language;

/**
 * Trait LanguageEnableableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait LanguageEnableableTrait
{
    /**
     * @var Collection<Language>|null
     */
    protected ?Collection $languages = null;

    /**
     * @return Collection<Language>
     */
    public function getLanguages(): Collection
    {
        if (!$this->languages) {
            $this->languages = new ArrayCollection();
        }
        return $this->languages;
    }

    /**
     * @param LanguageInterface $language
     *
     * @return LanguageEnableable
     */
    public function addLanguage(LanguageInterface $language): LanguageEnableable
    {
        if (!$this->languages) {
            $this->languages = new ArrayCollection();
        }
        if (!$this->languages->contains($language)) {
            $this->languages[] = $language;
        }
        return $this;
    }

    /**
     * @param LanguageInterface $language
     * @return bool
     */
    public function hasLanguage(LanguageInterface $language): bool
    {
        return $this->languages->contains($language);
    }

    /**
     * @param LanguageInterface $language
     *
     * @return LanguageEnableable
     */
    public function removeLanguage(LanguageInterface $language): LanguageEnableable
    {
        if ($this->languages->contains($language)) {
            $this->languages->removeElement($language);
        }
        return $this;
    }

    /**
     * @return LanguageEnableable
     */
    public function resetLanguages(): LanguageEnableable
    {
        foreach ($this->languages as $language) {
            $this->removeLanguage($language);
        }
        return $this;
    }

    /**
     * @param Collection $languages
     *
     * @return LanguageEnableable
     */
    public function setLanguages(Collection $languages): LanguageEnableable
    {
        $this->languages = $languages;
        return $this;
    }
}
