<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface ModuleParameterInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ModuleParameterInterface
{
    /**
     * @return ModuleInterface
     */
    public function getModule(): ModuleInterface;

    /**
     * @param ModuleInterface $module
     */
    public function setModule(ModuleInterface $module): self;

    /**
     * @return string|null
     */
    public function getValue(): ?string;

    /**
     * @param string|null $value
     */
    public function setValue(?string $value = null): self;

    /**
     * @return string
     */
    public function getIdentifier(): string;

    /**
     * @param string|null $identifier
     *
     * @return $this
     */
    public function setIdentifier(?string $identifier = null): self;
}
