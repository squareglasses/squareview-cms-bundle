<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\Gallery;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait HasGalleriesTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait HasGalleriesTrait
{
    /**
     * @var Gallery[]
     * @Groups({"galleries", "publish"})
     */
    protected array $galleries = [];

    /**
     * @var string
     * @Groups({"get"})
     */
    protected string $discriminator;

    /**
     * @return Gallery[]
     */
    public function getGalleries(): array
    {
        return $this->galleries;
    }

    /**
     * @param Gallery[] $galleries
     */
    public function setGalleries(array $galleries): void
    {
        $this->galleries = $galleries;
    }
}
