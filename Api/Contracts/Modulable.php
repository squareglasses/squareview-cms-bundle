<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\Module;

/**
 * Interface Modulable
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface Modulable
{
    /**
     * @return int
     */
    public function getResourceIdentifier(): int;

    /**
     * @return string
     */
    public function getResourceClass(): string;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return string
     */
    public function getDiscriminator(): string;

    /**
     * @return Module[]
     */
    public function getModules(): array;

    /**
     * @param Module[] $modules
     */
    public function setModules(array $modules): void;
}
