<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Doctrine\ORM\NonUniqueResultException;

/**
 * Interface WebsiteRepositoryInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface WebsiteRepositoryInterface
{
    /**
     * @param string $identifier
     * @return WebsiteInterface|null
     * @throws NonUniqueResultException
     */
    public function findOneBySlugOrId(string $identifier): ?WebsiteInterface;
}
