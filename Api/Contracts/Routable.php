<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

/**
 * Interface Routable
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface Routable
{
    /**
     * @return int
     */
    public function getResourceIdentifier(): int;

    /**
     * @return string
     */
    public function getResourceClass(): string;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;
}
