<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\MetaTag;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait MetaTaggableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait MetaTaggableTrait
{
    /**
     * @var MetaTag[]
     * @Groups({"metatags", "publish"})
     */
    protected array $metatags = [];

    /**
     * @return MetaTag[]
     */
    public function getMetatags(): array
    {
        return $this->metatags;
    }

    /**
     * @param MetaTag[] $metatags
     */
    public function setMetatags(array $metatags): void
    {
        $this->metatags = $metatags;
    }
}
