<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\Module;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait ModulableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait ModulableTrait
{
    /**
     * @var Module[]
     * @Groups({"modules", "publish"})
     */
    protected array $modules = [];

    /**
     * @return Module[]
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * @param Module[] $modules
     */
    public function setModules(array $modules): void
    {
        $this->modules = $modules;
    }
}
