<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Doctrine\Common\Util\ClassUtils;
use ReflectionException;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait CmsResourceTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait CmsResourceTrait
{
    /**
     * To expose resourceClass in serialization, needed on the backend for entity fields
     * @var string
     * @Groups({"expose_class"})
     */
    protected string $resourceClass;

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return ClassUtils::getClass($this);
    }
}
