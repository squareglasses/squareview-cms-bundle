<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\Gallery;

/**
 * Interface for objects that contains one or more galleries of medias.
 *
 * @author  Florent <florent@squareglasses.com>
 */
interface HasGalleriesInterface
{
    /**
     * @return int
     */
    public function getResourceIdentifier(): int;

    /**
     * @return string
     */
    public function getResourceClass(): string;

    /**
     * @return Gallery[]
     */
    public function getGalleries(): array;

    /**
     * @param Gallery[] $galleries
     */
    public function setGalleries(array $galleries): void;

    /**
     * @return string
     */
    public function getDiscriminator(): string;
}
