<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use SG\CmsBundle\Api\Entity\ResourceHasMedia;

/**
 * Interface HasMediasInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface HasMediasInterface
{
    /**
     * @return int
     */
    public function getResourceIdentifier(): int|string;

    /**
     * @return string
     */
    public function getResourceClass(): string;

    /**
     * @return string
     */
    public function getDiscriminator(): string;

    /**
     * @return ResourceHasMedia[]
     */
    public function getMedias(): array;

    /**
     * @param ResourceHasMedia[] $medias
     */
    public function setMedias(array $medias): void;

    /**
     * @return string
     */
    public function __toString(): string;
}
