<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Contracts;

use Doctrine\Common\Collections\Collection;

/**
 * Interface LanguageEnableable
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface LanguageEnableable
{
    /**
     * @return Collection<LanguageInterface>
     */
    public function getLanguages(): Collection;

    /**
     * @param LanguageInterface $language
     * @return $this
     */
    public function addLanguage(LanguageInterface $language): LanguageEnableable;

    /**
     * @param LanguageInterface $language
     * @return bool
     */
    public function hasLanguage(LanguageInterface $language): bool;

    /**
     * @param LanguageInterface $language
     * @return $this
     */
    public function removeLanguage(LanguageInterface $language): LanguageEnableable;
}
