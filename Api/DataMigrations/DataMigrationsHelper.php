<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations;

use SG\CmsBundle\Api\Entity\DataVersion;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use SG\CmsBundle\Api\DataMigrations\Model\AbstractDataMigration;
use SG\CmsBundle\Api\DataMigrations\Model\DataMigrationInterface;

/**
 * Class DataMigrationsHelper
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DataMigrationsHelper
{
    /**
     * DataMigrationsHelper constructor.
     *
     * @param EntityManagerInterface $manager
     * @param ContainerInterface     $container
     * @param string                 $projectDir
     * @param Finder|null            $finder Allow to pass a mock object for testing
     */
    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly ContainerInterface     $container,
        private readonly string                 $projectDir,
        private ?Finder                         $finder = null
    ) {
        if (null === $finder) {
            $finder = new Finder();
            $this->finder       = $finder;
        }
    }

    /**
     * @param DataMigrationInterface $migration
     *
     * @return bool
     */
    public function isExecuted(DataMigrationInterface $migration): bool
    {
        $version = $this
            ->manager
            ->getRepository(DataVersion::class)
            ->find($migration->version());

        return $version !== null;
    }

    /**
     * @param DataMigrationInterface $migration
     *
     * @return void
     */
    public function setExecuted(DataMigrationInterface $migration): void
    {
        $version = new DataVersion($migration->version());
        $this->manager->persist($version);
        $this->manager->flush();
    }

    /**
     * @param DataMigrationInterface $migration
     *
     * @return void
     */
    public function setUnexecuted(DataMigrationInterface $migration): void
    {
        $version = $this
            ->manager
            ->getRepository(DataVersion::class)
            ->find($migration->version());

        if ($version) {
            $this->manager->remove($version);
            $this->manager->flush();
        }
    }

    /**
     * @return DataMigrationInterface|null
     */
    public function current(): ?DataMigrationInterface
    {
        $current = $this
            ->manager
            ->createQueryBuilder()
            ->select('v.version as version')
            ->from(DataVersion::class, 'v')
            ->orderBy('v.version')
            ->setMaxResults(1)
            ->getQuery()
            ->getResult();

        if (0 === count($current)) {
            return null;
        }

        return $this->find($current[0]['version']);
    }

    /**
     * @return ContainerInterface|null
     */
    public function getContainer(): ?ContainerInterface
    {
        return $this->container;
    }

    /**
     * @return string
     */
    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    /**
     * @return iterable
     */
    public function findAllClassName(): iterable
    {
        $files = $this
            ->finder
            ->in($this->getMigrationsPath())
            ->name('*.php')
            ->sortByName();

        /** @var SplFileInfo $file */
        foreach ($files as $file) {
            preg_match('#Version(\d+)\.php$#i', $file->getFilename(), $matches);

            if (count($matches) < 2) {
                continue;
            }

            [, $version] = $matches;
            $classname = $this->getMigrationClassName($version);

            if (is_subclass_of($classname, AbstractDataMigration::class)) {
                yield $classname;
            }
        }
    }

    /**
     * @param string $version
     *
     * @return string|null
     */
    public function findClassName(string $version): ?string
    {
        $classname = $this->getMigrationClassName($version);

        if (is_subclass_of($classname, AbstractDataMigration::class)) {
            return $classname;
        }

        return null;
    }

    /**
     * @return DataMigrationInterface[]
     */
    public function findAll(): iterable
    {
        foreach ($this->findAllClassName() as $classname) {
            yield $this->retrieve($classname);
        }
    }

    /**
     * @return DataMigrationInterface[]
     */
    public function findAllToExecute(): array
    {
        $executedVersions = $this
            ->manager
            ->createQueryBuilder()
            ->select('v.version as version')
            ->from(DataVersion::class, 'v')
            ->orderBy('v.version', 'ASC')
            ->getQuery()
            ->getScalarResult();

        $executedMigrations = array_filter(
            array_map(
                function (string $version): ?string {
                    return $this->findClassName($version);
                },
                array_column($executedVersions, 'version')
            )
        );

        $result = [];

        foreach ($this->findAllClassName() as $classname) {
            if (!in_array($classname, $executedMigrations, true)) {
                $result[] = $this->retrieve($classname);
            }
        }

        return $result;
    }

    /**
     * @param string $version
     *
     * @return DataMigrationInterface|null
     */
    public function find(string $version): ?DataMigrationInterface
    {
        $classname = $this->findClassName($version);

        if (null === $classname) {
            return null;
        }

        return $this->retrieve($classname);
    }

    /**
     * @return string
     */
    private function getMigrationsPath(): string
    {
        return sprintf('%s/src/Migrations/Data', $this->projectDir);
    }

    /**
     * @param string $version
     *
     * @return string
     */
    private function getMigrationClassName(string $version): string
    {
        return sprintf('App\Migrations\Data\Version%s', $version);
    }

    /**
     * @param string $classname
     *
     * @return DataMigrationInterface
     */
    private function retrieve(string $classname): DataMigrationInterface
    {
        $migration = $this->container->get($classname);

        if (null === $migration) {
            throw new RuntimeException(sprintf('data migration "%s" not found', $classname));
        }

        if (!$migration instanceof DataMigrationInterface) {
            throw new RuntimeException(sprintf('data migration must implement %s', DataMigrationInterface::class));
        }

        return $migration;
    }
}
