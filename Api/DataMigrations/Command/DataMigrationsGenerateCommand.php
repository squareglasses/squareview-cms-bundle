<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Command;

use Exception;
use DateTime;
use DateTimeZone;
use RuntimeException;
use SG\CmsBundle\Api\DataMigrations\DataMigrationsHelper;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use SG\CmsBundle\Api\DataMigrations\Model\AbstractCmsMigration;
use SG\CmsBundle\Api\DataMigrations\Model\AbstractDataMigration;
use SG\CmsBundle\Api\DataMigrations\Model\AbstractCommandMigration;

/**
 * Class DataMigrationsGenerateCommand
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DataMigrationsGenerateCommand extends Command
{
    private static string $template
        = '<?php declare(strict_types=1);

namespace App\Migrations\Data;

use Doctrine\Persistence\ObjectManager;
use <classname>;

/**
 * Class Version<version>
 * @package App\Migrations\Data
 * @author Square Glasses <dev@squareglasses.com>
 */
class Version<version> extends <class>
{
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return \'\';
    }

    /**
     * @param ObjectManager $manager
     */
    public function up(ObjectManager $manager): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param ObjectManager $manager
     */
    public function down(ObjectManager $manager): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
';

    private ?string $projectDir;


    /**
     * @param DataMigrationsHelper $helper
     */
    public function __construct(DataMigrationsHelper $helper)
    {
        parent::__construct();
        $this->projectDir = $helper->getProjectDir();
    }

    /**
     * @return void
     */
    public function configure(): void
    {
        $this
            ->setName('data:migrations:generate')
            ->addOption(
                'cms',
                'c',
                InputOption::VALUE_NONE,
                'Create a cms data migration'
            )
            ->addOption(
                'command',
                'o',
                InputOption::VALUE_NONE,
                'Create a command data migration'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $migrationClass = AbstractDataMigration::class;

        if ($input->getOption('cms')) {
            $migrationClass = AbstractCmsMigration::class;
        }

        if ($input->getOption('command')) {
            $migrationClass = AbstractCommandMigration::class;
        }

        $version = $this->generateVersionNumber();
        $path    = $this->generateMigration($version, $migrationClass);

        $output->writeln(sprintf('Generated new data migration class to "<info>%s</info>"', $path));

        return 0;
    }

    /**
     * @param string $version
     * @param string $migrationClass
     *
     * @return string
     */
    private function generateMigration(string $version, string $migrationClass): string
    {
        $acceptedMigration = [
            AbstractDataMigration::class,
            AbstractCmsMigration::class,
            AbstractCommandMigration::class
        ];

        if (!in_array($migrationClass, $acceptedMigration, true)) {
            throw new RuntimeException('cannot make a migration of this type');
        }

        $code = str_replace(
            [
                '<version>',
                '<classname>',
                '<class>'
            ],
            [
                $version,
                $migrationClass,
                preg_replace('#(\w+\\\\)*(\w+)#', '$2', $migrationClass)

            ],
            self::$template
        );

        $path = sprintf(
            '%s/src/Migrations/Data/Version%s.php',
            $this->projectDir,
            $version
        );

        file_put_contents($path, $code);

        return $path;
    }

    /**
     * @return string
     * @throws Exception
     */
    private function generateVersionNumber(): string
    {
        return (new DateTime('now', new DateTimeZone('UTC')))->format('YmdHis');
    }
}
