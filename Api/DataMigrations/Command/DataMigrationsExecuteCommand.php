<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Command;

use Exception;
use RuntimeException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Stopwatch\Stopwatch;
use Symfony\Component\Console\Command\Command;
use SG\CmsBundle\Api\DataMigrations\DataMigrationsHelper;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;

/**
 * Class DataMigrationsExecuteCommand
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DataMigrationsExecuteCommand extends Command
{
    public const UP   = 'up';
    public const DOWN = 'down';

    /**
     * @param DataMigrationsHelper   $helper
     * @param EntityManagerInterface $manager
     * @param Stopwatch              $watch
     */
    public function __construct(
        private readonly DataMigrationsHelper   $helper,
        private readonly EntityManagerInterface $manager,
        private readonly Stopwatch              $watch
    ) {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function configure(): void
    {
        $this
            ->setName('data:migrations:execute')
            ->setDescription('Execute a single migration version up or down manually.')
            ->addArgument('version', InputArgument::REQUIRED, 'The version to execute.')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Execute the migration as a dry run.')
            ->addOption('up', null, InputOption::VALUE_NONE, 'Execute the migration up.')
            ->addOption('down', null, InputOption::VALUE_NONE, 'Execute the migration down.')
            ->setHelp(
                <<<EOT
The <info>%command.name%</info> command executes a single data migration version up or down manually:

    <info>%command.full_name% YYYYMMDDHHMMSS</info>

If no <comment>--up</comment> or <comment>--down</comment> option is specified it defaults to up:

    <info>%command.full_name% YYYYMMDDHHMMSS --down</info>

You can also execute the data migration as a <comment>--dry-run</comment>:

    <info>%command.full_name% YYYYMMDDHHMMSS --dry-run</info>

Or you can also execute the migration without a warning message which you need to interact with:

    <info>%command.full_name% YYYYMMDDHHMMSS --no-interaction</info>
EOT
            );

        parent::configure();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $version   = $input->getArgument('version');
        $direction = $input->getOption('down') ? self::DOWN : self::UP;

        if ($input->isInteractive()) {
            $question = 'WARNING! You are about to execute a data migration ' .
                        'that could result in data mutation and lost. ' .
                        'Are you sure you wish to continue? (y/n)';

            $execute = $this->askConfirmation($question, $input, $output);
        } else {
            $execute = true;
        }

        if (false === $execute) {
            $output->writeln('<error>DataMigration cancelled!</error>');
            return 0;
        }

        $this->runMigration(
            new SymfonyStyle($input, $output),
            $version,
            $direction,
            (bool) $input->getOption('dry-run')
        );
        return 0;
    }

    /**
     * @param string          $question
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return bool
     */
    private function askConfirmation(string $question, InputInterface $input, OutputInterface $output): bool
    {
        return (bool) $this
            ->getHelper('question')
            ->ask($input, $output, new ConfirmationQuestion($question));
    }

    /**
     * @param SymfonyStyle $io
     * @param string       $version
     * @param string       $direction
     * @param bool         $dryRun
     *
     * @return void
     * @throws Exception
     */
    private function runMigration(SymfonyStyle $io, string $version, string $direction, bool $dryRun): void
    {
        if (!in_array($direction, [self::UP, self::DOWN], true)) {
            throw new RuntimeException('invalid direction');
        }

        $migration = $this->helper->find($version);

        if (null === $migration) {
            throw new RuntimeException(sprintf('Could not find data migration version %s', $version));
        }

        $executed = $this->helper->isExecuted($migration);

        if (self::UP === $direction && $executed) {
            throw new RuntimeException(sprintf('data migration version %s already executed', $version));
        }

        if (self::DOWN === $direction && !$executed) {
            throw new RuntimeException(sprintf('cannot down unexecuted data migration version %s', $version));
        }

        $watcher = $this->watch->start($version);

        $io->text(
            sprintf(
                '<info>%s</info> %s <comment>%s</comment>',
                self::UP === $direction ? '++' : '--',
                self::UP === $direction ? 'migrating' : 'reverting',
                $version
            )
        );

        $io->text(sprintf("\n   <comment>-></comment> %s\n", ucfirst($migration->getDescription())));

        if (false === $dryRun) {
            $migration->getDescription();

            try {
                $this->manager->beginTransaction();

                $migration->$direction($this->manager);

                $this->manager->flush();
                $this->manager->commit();
            } catch (Exception $e) {
                $this->manager->rollback();
                throw $e;
            }
        }

        $watcher->stop();

        $io->text(
            sprintf(
                '<info>%s</info> %s (<comment>%ss</comment>)',
                self::UP === $direction ? '++' : '--',
                self::UP === $direction ? 'migrated' : 'reverted',
                $watcher->getDuration()/100
            )
        );

        if (self::UP === $direction) {
            $this->helper->setExecuted($migration);
        } else {
            $this->helper->setUnexecuted($migration);
        }
    }
}
