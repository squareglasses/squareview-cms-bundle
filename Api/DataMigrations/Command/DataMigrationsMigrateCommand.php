<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Command;

use Exception;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Stopwatch\Stopwatch;
use SG\CmsBundle\Api\DataMigrations\DataMigrationsHelper;
use SG\CmsBundle\Api\DataMigrations\Model\DataMigrationInterface;

/**
 * Class DataMigrationsMigrateCommand
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DataMigrationsMigrateCommand extends Command
{
    public const UP   = 'up';

    /**
     * @param DataMigrationsHelper   $helper
     * @param EntityManagerInterface $manager
     * @param Stopwatch              $watch
     */
    public function __construct(
        private readonly DataMigrationsHelper   $helper,
        private readonly EntityManagerInterface $manager,
        private readonly Stopwatch $watch
    ) {
        parent::__construct();
    }

    /**
     * @return void
     */
    public function configure(): void
    {
        $this
            ->setName('data:migrations:migrate')
            ->setDescription('Synchronise current database with data migration versions.');

        parent::configure();
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int
     * @throws Exception
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $this->outputHeader($output);

        if ($input->isInteractive()) {
            $question = 'WARNING! You are about to execute a data migration ' .
                        'that could result in data mutation and lost. ' .
                        'Are you sure you wish to continue? (y/n)';

            $execute = $this->askConfirmation($question, $input, $output);
        } else {
            $execute = true;
        }

        $watcher = $this->watch->start(self::class);

        if (false === $execute) {
            $output->writeln('<error>DataMigration cancelled!</error>');
            return 0;
        }

        $current    = $this->helper->current();
        $migrations = $this->helper->findAllToExecute();

        if (0 === count($migrations)) {
            $output->writeln("\n<comment>No data migrations to execute.</comment>");
            return 0;
        }

        $last = $migrations[count($migrations)-1];

        $io->text(
            sprintf(
                'Migrating <info>up</info> to <comment>%s</comment> from <comment>%s</comment>',
                $last->version(),
                $current ? $current->version() : 'undefined'
            )
        );

        foreach ($migrations as $migration) {
            $this->runMigration($io, $migration);
        }

        $watcher->stop();
        $output->writeln("\n<comment>------------------------</comment>\n");

        $io->text(sprintf('<info>++</info> finished in %ss', $watcher->getDuration() / 100));
        $io->text(sprintf('<info>++</info> %d migrations executed', count($migrations)));
        return 0;
    }

    /**
     * @param SymfonyStyle           $io
     * @param DataMigrationInterface $migration
     *
     * @return void
     * @throws Exception
     */
    private function runMigration(
        SymfonyStyle $io,
        DataMigrationInterface $migration
    ): void {
        $migration->setIo($io);
        $watcher = $this->watch->start($migration->version());

        $io->text(sprintf("\n <info>++</info> migrating <comment>%s</comment>", $migration->version()));
        $io->text(sprintf("\n   <comment>-></comment> %s\n", ucfirst($migration->getDescription())));

        $migration->getDescription();

        try {
            $this->manager->beginTransaction();

            $migration->up($this->manager);

            $this->manager->flush();
            $this->manager->commit();
        } catch (Exception $e) {
            $this->manager->rollback();
            throw $e;
        }

        $this->helper->setExecuted($migration);

        $watcher->stop();

        $io->text(sprintf('<info>++</info> migrated (%ss)', $watcher->getDuration() / 100));
    }

    /**
     * @param OutputInterface $output
     *
     * @return void
     */
    private function outputHeader(OutputInterface $output): void
    {
        $title = str_repeat(' ', 20) . "Database Data Migrations" . str_repeat(' ', 20);

        $output->writeln('<question>' . str_repeat(' ', strlen($title)) . '</question>');
        $output->writeln('<question>' . $title . '</question>');
        $output->writeln('<question>' . str_repeat(' ', strlen($title)) . '</question>');
        $output->writeln('');
    }

    /**
     * @param string          $question
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return bool
     */
    private function askConfirmation(string $question, InputInterface $input, OutputInterface $output): bool
    {
        return (bool)$this
            ->getHelper('question')
            ->ask($input, $output, new ConfirmationQuestion($question));
    }
}
