<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Model;

use Doctrine\Persistence\ObjectManager;
use Exception;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use ReflectionException;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\LanguageEnableable;
use SG\CmsBundle\Api\Contracts\LanguageInterface;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\DataFixtures\CmsFixturesTrait;
use SG\CmsBundle\Api\DataFixtures\Model\CmsResourceFixtureInterface;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\AbstractContent;
use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Serializer\MenuSerializer;
use SG\CmsBundle\Api\Translation\Exception\InvalidCatalogTypeException;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\CacheManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class AbstractCmsMigration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractCmsMigration extends AbstractDataMigration implements CmsResourceFixtureInterface
{
    use CmsFixturesTrait;

    /**
     * @param MediaManager          $mediaManager
     * @param HasMediaManager       $hasMediaManager
     * @param MetaTagManager        $metaTagManager
     * @param FilesystemOperator    $localStorage
     * @param ResourceGuideProvider $guideProvider
     * @param TranslationManager    $translationManager
     * @param ConfigurationBag      $configurationBag
     * @param SerializerInterface   $serializer
     * @param MenuSerializer        $menuSerializer
     * @param PublicationManager    $versionableManager
     * @param CacheManager          $cacheManager
     * @param PublicationManager    $publicationManager
     */
    public function __construct(
        protected MediaManager           $mediaManager,
        protected HasMediaManager        $hasMediaManager,
        protected MetaTagManager         $metaTagManager,
        FilesystemOperator               $localStorage,
        protected ResourceGuideProvider  $guideProvider,
        protected TranslationManager     $translationManager,
        protected ConfigurationBag       $configurationBag,
        protected SerializerInterface    $serializer,
        protected MenuSerializer         $menuSerializer,
        protected PublicationManager     $versionableManager,
        protected CacheManager           $cacheManager,
        protected PublicationManager     $publicationManager
    ) {
        $this->filesystem           = $localStorage;
        $this->classes              = $configurationBag->getClasses();
        $this->contentClass         = $this->classes['content'] ?? null;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws InvalidCatalogTypeException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     * @throws FilesystemException
     * @throws ReflectionException
     * @throws Exception
     */
    public function process(ObjectManager $manager, bool $invalidateCache = true): void
    {
        // Manage all resources pages
        $this->generateResources();
        $this->generateMetatags();
        $this->generateRoutes();
        $this->generateMedias();
        $this->generateGalleries();
        $this->prepareResourcesTranslations();
        $manager->flush();

        $this->generateModules($this->resources);
        $manager->flush();


        $this->translationManager->multipleTranslate($this->translations);
        $this->publishVersionableResources();
        if ($invalidateCache) {
            $this->invalidateCacheForFlushedResources();
        }
        $this->publishLayoutGuideTranslations($invalidateCache);

        $manager->flush();
    }

    /**
     * @return ObjectManager
     */
    public function getManager(): ObjectManager
    {
        return $this->manager;
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return void
     */
    public function log(string $message, string $type = 'info'): void
    {
        if (null === $this->getIo()) {
            return;
        }
        switch ($type) {
            case "error":
                $this->getIo()->error($message);
                break;
            case "warning":
                $this->getIo()->warning($message);
                break;
            case "caution":
                $this->getIo()->caution($message);
                break;
            default:
                $this->getIo()->info($message);
                break;
        }
    }

    /**
     * @param string $slug
     *
     * @return void
     */
    protected function removeContent(string $slug): void
    {
        $content = $this->getContent($slug);
        if (null === $content) {
            return;
        }

        foreach ($content->getVersions() as $version) {
            $this->manager->remove($version);
        }
        $this->manager->remove($content);
    }

    /**
     * @param string $slug
     *
     * @return AbstractContent|null
     */
    protected function getContent(string $slug): ?AbstractContent
    {
        return $this->manager->getRepository($this->configurationBag->getContentClass())
            ->findOneBy(["slug" => $slug]);
    }

    /**
     * @param LanguageEnableable $languageEnableable
     * @param string             $locale
     *
     * @return void
     */
    protected function addLanguage(LanguageEnableable $languageEnableable, string $locale): void
    {
        /** @var LanguageInterface $language */
        $language = $this->manager->getRepository(Language::class)->findOneBy(["id" => $locale]);
        $languageEnableable->addLanguage($language);
        $this->manager->persist($languageEnableable);
    }

    /**
     * @param string $slug
     * @param array  $translations
     * @param string $locale
     * @param bool   $onlyDraft
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    protected function saveContentTranslations(string $slug, array $translations, string $locale = 'fr', bool $onlyDraft = false): void
    {
        $content = $this->getContent($slug);
        if (null !== $content) {
            $this->updateTranslations($content->getDraftVersion(), $translations, $locale);
            if ($onlyDraft === false) {
                $this->updateTranslations($content->getCurrentVersion($locale), $translations, $locale);
                $this->cacheManager->invalidateCacheForResource($content);
            }
        }
    }

    /**
     * @param Translatable $translatable
     * @param array        $translations
     * @param string       $locale
     *
     * @return void
     * @throws TranslationClientException
     */
    private function updateTranslations(Translatable $translatable, array $translations, string $locale = 'fr'): void
    {
        $this->translationManager->updateTranslatablePropertiesTranslations(
            $translatable,
            $translations,
            $locale
        );
    }

    /**
     * @param CmsResourceInterface $resource
     * @param array $translations
     * @param string $zone
     * @param string $locale
     * @return void
     * @throws ResourceGuideNotFoundException
     * @throws \JsonException
     */
    protected function saveResourceGuideTranslations(string $slug, array $translations, string $zone = 'default', string $locale = 'fr', string $resourceClass = null): void
    {
        if (null === $resourceClass) {
            $resourceClass = $this->configurationBag->getContentClass();
        }
        $resource = $this->manager->getRepository($resourceClass)
            ->findOneBy(["slug" => $slug]);

        if (null === $resource) {
            throw new RuntimeException(sprintf("Resource not found with slug %s and class %s", $slug, $resourceClass));
        }

        $guidable = $resource;
        if ($resource instanceof Versionable) {
            $guidable = $resource->getDraftVersion();
            $this->flushedResources[$slug] = $resource;
        }

        $guide = null;
        if ($guidable instanceof Guidable) {
            $guide = $this->guideProvider->getGuide($guidable->getGuide());
        }
        if (null === $guide) {
            throw new RuntimeException("Guide configuration not found for Guidable resource given");
        }

        foreach ($translations as $key => $value) {
            $this->addTranslation((new PendingTranslation())
                ->setKey($key)
                ->setMessage($value)
                ->setLabel($locale === $this->configurationBag->getDefaultLocale() ? (string)$value : null)
                ->setLocale($locale)
                ->setGuide($guide->getName())
                ->setZone($zone)
                ->setType(TranslationKey::TYPE_RESOURCE_GUIDE)
                ->setResourceClass($guidable->getResourceClass())
                ->setResourceIdentifier($guidable->getResourceIdentifier()));
        }
    }

    private function invalidateCacheForFlushedResources()
    {
        foreach ($this->flushedResources as $resource) {
            $this->cacheManager->invalidateCacheForResource($resource);
        }
    }
}
