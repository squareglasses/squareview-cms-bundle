<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Model;

/**
 * Interface DataVersionInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface DataVersionInterface
{
    /**
     * @return string|null
     */
    public function getVersion(): ?string;

    /**
     * @param string|null $version
     *
     * @return DataVersionInterface
     */
    public function setVersion(?string $version): DataVersionInterface;
}
