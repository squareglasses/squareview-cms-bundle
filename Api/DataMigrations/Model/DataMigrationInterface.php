<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Model;

use Doctrine\Persistence\ObjectManager;

/**
 * Interface DataMigrationInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface DataMigrationInterface
{
    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function up(ObjectManager $manager): void;

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function down(ObjectManager $manager): void;

    /**
     * @return string
     */
    public function version(): string;

    /**
     * @return string
     */
    public function getDescription(): string;
}
