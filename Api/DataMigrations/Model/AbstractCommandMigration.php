<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Model;

use RuntimeException;
use SG\CmsBundle\Api\DataMigrations\DataMigrationsHelper;
use Symfony\Component\Process\Process;
use Symfony\Component\Process\Exception\ProcessFailedException;

/**
 * Class AbstractCommandMigration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractCommandMigration extends AbstractDataMigration
{
    /**
     * @param DataMigrationsHelper $helper
     */
    public function __construct(private readonly DataMigrationsHelper $helper)
    {
    }

    /**
     * @param string $command
     *
     * @return void
     */
    protected function runCommand(string $command): void
    {
        $process = new Process(['./bin/console', $command], $this->helper->getProjectDir());
        $process->setTimeout(600);

        try {
            $process->mustRun();
            echo $process->getOutput();
        } catch (ProcessFailedException $e) {
            echo $process->getOutput();
            throw new RuntimeException($e->getMessage(), $e->getCode(), $e);
        }
    }
}
