<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataMigrations\Model;

use RuntimeException;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class AbstractDataMigration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractDataMigration implements DataMigrationInterface
{
    protected ?SymfonyStyle $io = null;

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function up(ObjectManager $manager): void
    {
        throw new RuntimeException('must be implemented');
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function down(ObjectManager $manager): void
    {
        throw new RuntimeException('must be implemented');
    }

    /**
     * @return string
     */
    public function version(): string
    {
        preg_match('#Version(\d+)$#', get_class($this), $matches);
        [, $version] = $matches;
        return $version;
    }

    /**
     * @return SymfonyStyle|null
     */
    public function getIo(): ?SymfonyStyle
    {
        return $this->io;
    }

    /**
     * @param SymfonyStyle|null $io
     *
     * @return $this
     */
    public function setIo(?SymfonyStyle $io): AbstractDataMigration
    {
        $this->io = $io;
        return $this;
    }
}
