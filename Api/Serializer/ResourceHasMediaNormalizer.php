<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use ArrayObject;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class ResourceHasMediaNormalizer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceHasMediaNormalizer implements NormalizerInterface
{
    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array|ArrayObject|bool|float|int|string|null
     * @throws ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = []): float|array|bool|ArrayObject|int|string|null
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        if (array_key_exists('media', $data)) {
            $normalizedMedia = $data['media'];
            unset($data['media']);
        } else {
            $normalizedMedia = [];
        }
        if (array_key_exists('id', $data)) {
            $data['resourceHasMediaId'] = $data['id'];
            unset($data['id']);
        }

        return array_merge($normalizedMedia, $data);
    }

    /**
     * @param             $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization($data, string $format = null, array $context = []): bool
    {
        return $data instanceof ResourceHasMedia;
    }
}
