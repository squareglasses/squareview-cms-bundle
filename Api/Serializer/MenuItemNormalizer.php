<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use ArrayObject;
use Exception;
use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Api\Manager\MenuItemManager;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class MenuItemNormalizer
 *
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
class MenuItemNormalizer implements NormalizerInterface
{
    /**
     * @param ObjectNormalizer   $normalizer
     * @param MenuItemManager    $menuItemManager
     * @param RequestStack       $requestStack
     * @param ConfigurationBag   $configurationBag
     * @param TranslationManager $translationManager
     */
    public function __construct(
        private readonly ObjectNormalizer         $normalizer,
        private readonly MenuItemManager          $menuItemManager,
        private readonly RequestStack             $requestStack,
        private readonly ConfigurationBag         $configurationBag,
        private readonly TranslationManager       $translationManager,
    ) {
    }

    /**
     * @param mixed       $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array|ArrayObject|bool|float|int|mixed|string|null
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function normalize(mixed $object, string $format = null, array $context = []): mixed
    {
        $serializationGroups = array_key_exists('groups', $context) ? $context['groups'] : false;

        if (!is_array($serializationGroups)) {
            $serializationGroups = [$serializationGroups];
        }

        if ($serializationGroups
            && in_array('targetResource', $serializationGroups, true)
            && $object->getResourceIdentifier()
            && $object->getResourceClass()
        ) {
            $object->setTargetResource($this->menuItemManager->findResource($object));
        }

        $this->translationManager->loadTranslatablePropertiesTranslations(
            $object,
            $this->getLocale(),
            $serializationGroups,
        );

        return $this->normalizer->normalize($object, $format, $context);
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
//        $serializationGroups = array_key_exists('groups', $context) ? $context['groups'] : false;
//
//        if (!is_array($serializationGroups)) {
//            $serializationGroups = [$serializationGroups];
//        }
//        if ($data instanceof MenuItem && $serializationGroups
//            && in_array('targetResource', $serializationGroups, true)
//            && $data->getResourceIdentifier()
//            && $data->getResourceClass()
//        ) {
//            return true;
//        }
//
//        return false;
        return $data instanceof MenuItem;
    }

    /**
     * @return string
     */
    private function getLocale(): string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (null !== $request) { // Console command context has no request
            if (!in_array($request->getLocale(), $this->configurationBag->getAllowedLocales(), true)) {
                return $this->configurationBag->getDefaultLocale();
            }
            return $request->get('locale', $this->configurationBag->getDefaultLocale());
        }
        return $this->configurationBag->getDefaultLocale();
    }
}
