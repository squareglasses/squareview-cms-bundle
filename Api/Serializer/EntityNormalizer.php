<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use Doctrine\Persistence\ManagerRegistry;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class EntityNormalizer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class EntityNormalizer implements DenormalizerInterface
{
    /**
     * @param ObjectNormalizer $normalizer
     * @param ManagerRegistry  $managerRegistry
     */
    public function __construct(private readonly ObjectNormalizer $normalizer, private readonly ManagerRegistry $managerRegistry)
    {
    }

    /**
     * @param             $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsDenormalization($data, string $type, string $format = null, array $context = []): bool
    {
        return (str_starts_with($type, 'App\\Entity\\') || str_starts_with($type, 'SG\\CmsBundle\\Api\\Entity')) &&
            (is_numeric($data) || is_string($data) || (is_array($data) && isset($data['id']) && null !== $data['id']));
    }

    /**
     * @param             $data
     * @param string      $type
     * @param string|null $format
     * @param array       $context
     *
     * @return object
     * @throws ExceptionInterface
     * @throws ReflectionException
     */
    public function denormalize($data, string $type, string $format = null, array $context = []): object
    {
        if (is_numeric($data) || is_string($data)) {
            $objectId = $data;
        } else {
            $objectId = $data['id'];
        }
        if ($object = $this->managerRegistry->getManager()->getRepository($type)->findOneBy(['id' => $objectId])) {
            return $this->normalizer->denormalize($data, $type, $format, array_merge($context, [
                AbstractNormalizer::OBJECT_TO_POPULATE => $object,
            ]));
        }

        $class = new ReflectionClass($type);
        if ($class->hasProperty("slug") && $object = $this->managerRegistry->getManager()->getRepository($type)->findOneBy(['slug' => $objectId])) {
            return $this->normalizer->denormalize($data, $type, $format, array_merge($context, [
                AbstractNormalizer::OBJECT_TO_POPULATE => $object,
            ]));
        }
        return $this->normalizer->denormalize($data, $type, $format, $context);
    }
}
