<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer\Util;

use Doctrine\Common\Annotations\Reader;
use RuntimeException;
use ReflectionClass;
use ReflectionException;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class SerializationGroupHelper
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SerializationGroupHelper
{
    /**
     * @param Reader $annotationReader
     */
    public function __construct(private readonly Reader $annotationReader)
    {
    }

    /**
     * @param string $property
     * @param array  $groups
     * @param object $object
     *
     * @return bool
     */
    public function propertyInGroups(string $property, array $groups, object $object): bool
    {
        foreach ($this->getReflectionClass(get_class($object))->getProperties() as $classProperty) {
            if ($classProperty->getName() === $property) {
                $groupsAnnotation = $this->annotationReader->getPropertyAnnotation($classProperty, Groups::class);
                if (null !== $groupsAnnotation) {
                    return count(array_intersect($groups, $groupsAnnotation->getGroups())) > 0;
                }
            }
        }
        return false;
    }

    /**
     * @param string $className
     *
     * @return ReflectionClass
     */
    private function getReflectionClass(string $className): ReflectionClass
    {
        try {
            $reflectionClass = new ReflectionClass($className);
        } catch (ReflectionException $e) {
            throw new RuntimeException('Failed to read annotation on class "'.$className.'" : '.$e->getMessage());
        }
        return $reflectionClass;
    }
}
