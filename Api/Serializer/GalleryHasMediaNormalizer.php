<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use ArrayObject;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class GalleryHasMediaNormalizer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GalleryHasMediaNormalizer implements NormalizerInterface
{
    /**
     * @param ObjectNormalizer $normalizer
     */
    public function __construct(private readonly ObjectNormalizer $normalizer)
    {
    }

    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array|ArrayObject|bool|float|int|string|null
     * @throws ExceptionInterface
     */
    public function normalize($object, string $format = null, array $context = []): float|array|bool|ArrayObject|int|string|null
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        if (array_key_exists('media', $data)) {
            $normalizedMedia = $data['media'];
            unset($data['media']);
            if (array_key_exists('id', $data)) {
                $data['galleryHasMediaId'] = $data['id'];
                unset($data['id']);
            }

            return array_merge($normalizedMedia, $data);
        }
        return $data;
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof GalleryHasMedia;
    }
}
