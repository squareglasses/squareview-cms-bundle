<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Common\Contracts\MenuItemInterface;
use Symfony\Component\Serializer\SerializerInterface;
use SG\CmsBundle\Common\Menu\Model\Menu as MenuModel;
use SG\CmsBundle\Common\Menu\Model\MenuItem as MenuItemModel;

/**
 * Class MenuSerializer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MenuSerializer
{
    /**
     * @param SerializerInterface    $serializer
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        private readonly SerializerInterface $serializer,
        private readonly EntityManagerInterface $entityManager
    ) {
    }

    /**
     * @param Menu   $menu
     * @param string $locale
     *
     * @return string
     */
    public function serialize(Menu $menu, string $locale): string
    {
        $menuArray = $this->serializer->serialize($this->computeMenu($menu, $locale), "json", ['groups' => 'get']);
        $computedMenu = $this->serializer->deserialize($menuArray, MenuModel::class, 'json');
        $this->configureMenu($menu, $computedMenu);

        return $this->serializer->serialize($computedMenu, 'json');
    }

    /**
     * @param Menu      $menu
     * @param MenuModel $menuModel
     *
     * @return void
     */
    private function configureMenu(Menu $menu, MenuModel $menuModel): void
    {
        $menuModel->setAttributes(['id' => $menu->getSlug()]);
        foreach ($menuModel->getItems() as $item) {
            $this->configureItem($item, $menuModel);
        }
    }

    /**
     * @param MenuItemModel $item
     * @param MenuModel     $menu
     *
     * @return void
     */
    private function configureItem(MenuItemModel $item, MenuModel $menu): void
    {
        $menuSlug = $menu->getSlug();
        $item->setUlAttributes(["id" => $menuSlug."-item-".$item->getSlug()."-ul"]);
        $item->setLiAttributes(["id" => $menuSlug."-item-".$item->getSlug()."-li"]);
        $item->addLinkAttribute("id", $menuSlug."-item-".$item->getSlug()."-link");
        $item->setLabelAttributes(["id" => $menuSlug."-item-".$item->getSlug()."-label"]);
        if (null !== $item->getTarget()) {
            $item->addLinkAttribute("target", $item->getTarget());
        }
        foreach ($item->getItems() as $subItem) {
            $this->configureItem($subItem, $menu);
        }
    }

    /**
     * @param Menu   $menu
     * @param string $locale
     *
     * @return Menu
     */
    private function computeMenu(Menu $menu, string $locale): Menu
    {
        foreach ($menu->getItems() as $item) {
            $this->computeItem($item, $locale);
        }
        return $menu;
    }

    /**
     * @param MenuItem $item
     * @param string   $locale
     *
     * @return void
     */
    private function computeItem(MenuItem $item, string $locale): void
    {
        if ($item->getKind() === MenuItemInterface::KIND_RESOURCE
            && null !== $item->getTargetResourceId()
            && null !== $item->getTargetResourceClass()
        ) {
            $route = $this->entityManager
                ->getRepository(Route::class)
                ->findOneByResourceClassAndIdentifier(
                    $item->getTargetResourceClass(),
                    $item->getTargetResourceId(),
                    $locale
                );
            if ($route) {
                $item->setRoute($route->getName());
            }
        }
    }
}
