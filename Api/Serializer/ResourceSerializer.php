<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Common\ResourceGuide\Model\Zone;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class ResourceSerializer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceSerializer
{
    private int $depth = 1;

    /**
     * @param SerializerInterface $serializer
     * @param ResourceGuideProvider $guideProvider
     * @param TranslationManager $translationManager
     */
    public function __construct(
        private readonly SerializerInterface   $serializer,
        private readonly ResourceGuideProvider $guideProvider,
        private readonly TranslationManager    $translationManager
    ) {
    }

    /**
     * @param CmsResourceInterface $publishable
     * @param string $locale
     * @param array $context
     * @return array
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     * @throws TranslationClientException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function serialize(CmsResourceInterface $publishable, string $locale, array $context = []): array
    {
        $response = [];
        $context['groups'] = "publish";
        $response['resource'] = json_decode($this->serializer->serialize($publishable, 'json', $context), true, 512, JSON_THROW_ON_ERROR);

        if ($publishable instanceof Versionable) {
            $showDraft = $this->hasCorrectDraftHash($publishable, $locale, $context);
            if ($showDraft) {
                $version = $publishable->getDraftVersion();
            } else {
                $context["published"] = true;
                $version = $publishable->getCurrentVersion($locale);
            }

            $serializedVersion = json_decode(
                $this->serializer->serialize(
                    $version,
                    'json',
                    $context
                ),
                true,
                512,
                JSON_THROW_ON_ERROR
            );

            if (null !== $serializedVersion) {
                $response['resource'] = array_merge($response['resource'], $serializedVersion);
                $serializedTranslations = [];
                // getTranslations is always empty for draft, we need to rebuild the data
                if ($showDraft && $version instanceof Guidable) {
                    $translations = $this->translationManager->getResourceGuideTranslations($version);
                    foreach ($translations as $translation) {
                        $serializedTranslations[$translation['locale']]['guide'][$translation['zone']][$translation['key']] = $translation['message'];
                    }
                } else {
                    $serializedTranslations = $version->getTranslations();
                }
                $response['translations'] = [
                    'resource' => array_key_exists($locale, $serializedTranslations) ? $serializedTranslations[$locale]["guide"] : []
                ];
            }

        } elseif ($publishable instanceof Guidable) {
            $context['groups'] = ["get", "medias"];
            $response['resource'] = json_decode(
                $this->serializer->serialize(
                    $publishable,
                    'json',
                    $context
                ),
                true,
                512,
                JSON_THROW_ON_ERROR
            );
            $response['translations'] = $this->serializeGuideTranslations($publishable, $locale);
        }
        return $response;
    }

    /**
     * @param Guidable $resource
     * @param string $locale
     * @return array[]
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     */
    #[ArrayShape(['resource' => "array"])]
    private function serializeGuideTranslations(Guidable $resource, string $locale): array
    {
        $guideTranslations = [];

        $guide = $this->guideProvider->getGuide($resource->getGuide());

        if (null !== $guide) {
            try {
                foreach ($guide->getZones() as $zone) {
                    if ($zone->getType() === Zone::TYPE_TRANSLATIONS && !$zone->isLayout()) {
                        $translations = $this->translationManager->getTranslations(
                            $locale,
                            $resource->getResourceIdentifier(),
                            $resource->getResourceClass(),
                            $guide->getName(),
                            $zone->getName(),
                            TranslationKey::TYPE_RESOURCE_GUIDE,
                        );
                        $guideTranslations[$zone->getName()] = [];
                        foreach ($translations as $translation) {
                            $guideTranslations[$zone->getName()][$translation['key']] = $translation['message'];
                        }
                    }
                }
            } catch (Exception $e) {
                $this->log(preg_replace('~[\r\n]+~', ' ', $e->getMessage()));
            }
        }

        return ['resource' => $guideTranslations];
    }

    /**
     * @param string $message
     *
     * @return void
     */
    private function log(string $message): void
    {
        $output = new ConsoleOutput();
        $output->writeln("<comment>     Serializer warning: ".$message."</comment>");
    }

    /**
     * @param CmsResourceInterface $publishable
     * @param string $locale
     * @param array $context
     * @return bool
     */
    public function hasCorrectDraftHash(CmsResourceInterface $publishable, string $locale, array $context): bool
    {
        return array_key_exists('draft', $context) && $context['draft'] === $publishable->getDraftVersion()->getHashByLocale($locale);
    }
}
