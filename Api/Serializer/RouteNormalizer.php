<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use ArrayObject;
use Doctrine\Persistence\ManagerRegistry;
use Exception;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Route;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

/**
 * Class RouteNormalizer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RouteNormalizer implements NormalizerInterface
{
    /**
     * @param ObjectNormalizer $normalizer
     * @param ManagerRegistry  $managerRegistry
     * @param RequestStack     $requestStack
     */
    public function __construct(
        private readonly ObjectNormalizer $normalizer,
        private readonly ManagerRegistry  $managerRegistry,
        private readonly RequestStack     $requestStack
    ) {
    }

    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return array|ArrayObject|bool|float|int|mixed|string|null
     * @throws ExceptionInterface
     * @throws Exception
     */
    public function normalize($object, string $format = null, array $context = []): mixed
    {
        $data = $this->normalizer->normalize($object, $format, $context);
        if (is_array($data)) {
            $data['guide'] = null;

            // Getting associated resource
            if (isset($data['resourceIdentifier'])) {
                $resource = $this->managerRegistry->getRepository($data['resourceClass'])
                    ->findOneBy(['id' => $data['resourceIdentifier']]);
                if ($resource) {
                    if ($resource instanceof Versionable && count($resource->getVersions()) > 0) {
                        if ($resource->getCurrentVersion($this->getLocale())) {
                            $data['guide'] = $resource->getCurrentVersion($this->getLocale())->getGuide();
                        } else {
                            $data['guide'] = $resource->getDraftVersion()->getGuide();
                        }
                    } elseif ($resource instanceof Guidable) {
                        $data['guide'] = $resource->getGuide();
                    }
                }
            }
        }

        return $data;
    }

    /**
     * @return string
     */
    private function getLocale(): string
    {
        if (null === $this->requestStack->getCurrentRequest()) {
            throw new RuntimeException("Request not found");
        }
        return $this->requestStack->getCurrentRequest()->getLocale();
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof Route;
    }
}
