<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use Exception;
use JsonException;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\ModuleParameterInterface;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Contracts\WebsiteInterface;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\MetaTag;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use SG\CmsBundle\Api\Manager\GalleryManager;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Serializer\Util\SerializationGroupHelper;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ResourceNormalizer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceNormalizer implements NormalizerInterface, SerializerAwareInterface
{
    private int $depth = 1;
    private SerializerInterface $serializer;

    /**
     * @param ObjectNormalizer         $normalizer
     * @param RequestStack             $requestStack
     * @param ConfigurationBag         $configurationBag
     * @param HasMediaManager          $hasMediaManager
     * @param GalleryManager           $galleryManager
     * @param TranslationManager       $translationManager
     * @param MetaTagManager           $metaTagManager
     * @param ModuleManager            $moduleManager
     * @param SerializationGroupHelper $serializationGroupHelper
     */
    public function __construct(
        private readonly ObjectNormalizer         $normalizer,
        private readonly RequestStack             $requestStack,
        private readonly ConfigurationBag         $configurationBag,
        private readonly HasMediaManager          $hasMediaManager,
        private readonly GalleryManager           $galleryManager,
        private readonly TranslationManager       $translationManager,
        private readonly MetaTagManager           $metaTagManager,
        private readonly ModuleManager            $moduleManager,
        private readonly SerializationGroupHelper $serializationGroupHelper
    ) {
    }

    /**
     * @param             $object
     * @param string|null $format
     * @param array       $context
     *
     * @return mixed
     * @throws ExceptionInterface
     * @throws JsonException
     * @throws TranslationClientException
     * @throws Exception
     */
    public function normalize($object, string $format = null, array $context = []): mixed
    {
        // Fix groups variable if it is a string instead of an array
        $serializationGroups = array_key_exists('groups', $context) ? $context['groups'] : [];
        if (!is_array($serializationGroups)) {
            $serializationGroups = [$serializationGroups];
        }

        $publishedVersion = array_key_exists('published', $context) ? $context['published'] : false;

        // If this is a collection serialization, set to true to optimize translation calls
        $isCollection = array_key_exists('collection', $context) && $context['collection'];

        // Load all nested relations into object
        $this->preloadObject($object, $serializationGroups, $isCollection, $publishedVersion);

        if ($object instanceof Versionable) {
            if ($this->depth > 1) { // Prevent infinite loop from module's resources serialization
                $serializationGroups = $this->removeSerializationGroups($serializationGroups, ["publish", "modules"]);
            }
            $this->depth++;
            $version = $publishedVersion === true ? $object->getCurrentVersion($this->getLocale()) : $object->getDraftVersion();

            if (count($serializationGroups) > 0) {
                $versionArray = json_decode($this->serializer->serialize($version, "json", [
                    'groups' => $serializationGroups,
                    'collection' => $isCollection,
                    'published' => $publishedVersion,
                ]), true, 512, JSON_THROW_ON_ERROR);
            } else {
                $versionArray = [];
            }


            $versionHashes = [];
            foreach ($object->getVersions() as $v) {
                if ($v->isCurrentVersion()) {
                    $versionHashes[$v->getLocale()] = [
                        'date' => $v->getUpdatedAt(),
                        'hash' => $v->getHash()
                    ];
                }
            }
        }

        // Serialize object
        $data = $this->normalizer->normalize($object, $format, $context);

        // Add extra data for versionable objects (for backend use)
        if ($object instanceof Versionable) {
            $data = array_merge($versionArray, $data);
            $data['versionHashes'] = $versionHashes;
        }

        return $data;
    }

    /**
     * @param array $serializationGroups
     * @param array $groupNames
     *
     * @return array
     */
    private function removeSerializationGroups(array $serializationGroups, array $groupNames): array
    {
        foreach ($serializationGroups as $key => $serializationGroup) {
            if (in_array($serializationGroup, $groupNames, true)) {
                unset($serializationGroups[$key]);
            }
        }
        return $serializationGroups;
    }

    /**
     * @param object $object
     * @param array  $serializationGroups
     * @param bool   $isCollection
     * @param bool   $publishedVersion
     *
     * @return void
     * @throws TranslationClientException
     */
    private function preloadObject(object $object, array $serializationGroups, bool $isCollection, bool $publishedVersion = false): void
    {
        if ($object instanceof Translatable && (false === $publishedVersion || !$object instanceof Version)) {
            $this->translationManager->loadTranslatablePropertiesTranslations(
                $object,
                $this->getLocale(),
                $serializationGroups,
                $isCollection
            );
        }

        // Load medias into object if necessary
        if ($object instanceof HasMediasInterface
            && $this->serializationGroupHelper->propertyInGroups("medias", $serializationGroups, $object)
        ) {
            $this->hasMediaManager->loadMedias($object);
        }

        // Load meta tags into object if necessary
        if ($object instanceof MetaTaggable
            && $this->serializationGroupHelper->propertyInGroups("metatags", $serializationGroups, $object)
        ) {
            $this->metaTagManager->loadMetaTags($object, $this->getLocale());
        }

        // Load modules into object if necessary
        if ($object instanceof Modulable
            && $this->serializationGroupHelper->propertyInGroups("modules", $serializationGroups, $object)
        ) {
            $this->moduleManager->loadModules($object, $this->getLocale());
        }

        if ($object instanceof HasGalleriesInterface
            && $this->serializationGroupHelper->propertyInGroups("galleries", $serializationGroups, $object)
        ) {
            $this->galleryManager->loadGalleries($object);
        }
    }

    /**
     * @param mixed       $data
     * @param string|null $format
     * @param array       $context
     *
     * @return bool
     */
    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        return $data instanceof HasMediasInterface
            || $data instanceof HasGalleriesInterface
            || $data instanceof Translatable
            || $data instanceof MetaTaggable
            || $data instanceof Modulable
            || $data instanceof Versionable
            || $data instanceof WebsiteInterface;
    }

    /**
     * @param SerializerInterface $serializer
     *
     * @return void
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @return string
     */
    private function getLocale(): string
    {
        $request = $this->requestStack->getCurrentRequest();
        if (null !== $request) { // Console command context has no request
            if (!in_array($request->getLocale(), $this->configurationBag->getAllowedLocales(), true)) {
                return $this->configurationBag->getDefaultLocale();
            }
            return $request->get('locale', $this->configurationBag->getDefaultLocale());
        }
        return $this->configurationBag->getDefaultLocale();
    }
}
