<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Serializer;

use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class GatewayResource
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GatewayResource
{
    /**
     * @Groups({"get"})
     */
    protected int|null $resourceId = null;

    /**
     * @Groups({"get"})
     */
    protected ?string $resourceClass = null;

    /**
     * @var array<string> $response
     * @Groups({"get"})
     */
    protected array $resource = [];

    /**
     * @var array<string> $translations
     * @Groups({"get"})
     */
    protected array $translations = [];

    /**
     * @Groups({"get"})
     */
    protected string $locale;

    /**
     * @return int|null
     */
    public function getResourceId(): ?int
    {
        return $this->resourceId;
    }

    /**
     * @param int|null $resourceId
     *
     * @return $this
     */
    public function setResourceId(?int $resourceId): self
    {
        $this->resourceId = $resourceId;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string|null $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(?string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }

    /**
     * @return array
     */
    public function getResource(): array
    {
        return $this->resource;
    }

    /**
     * @param array $resource
     *
     * @return $this
     */
    public function setResource(array $resource): self
    {
        $this->resource = $resource;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     *
     * @return $this
     */
    public function setTranslations(array $translations): self
    {
        $this->translations = $translations;

        return $this;
    }
}
