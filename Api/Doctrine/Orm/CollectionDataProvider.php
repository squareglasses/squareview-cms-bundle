<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm;

use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use JetBrains\PhpStorm\ArrayShape;
use RuntimeException;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\FilterExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGenerator;
use SG\CmsBundle\Common\Cache\TagExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class CollectionDataProvider
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CollectionDataProvider
{
    private array $extensions = [];

    /**
     * @param ManagerRegistry     $managerRegistry
     * @param FilterExtension     $filterExtension
     * @param SerializerInterface $serializer
     * @param RequestStack        $requestStack
     */
    public function __construct(
        private readonly ManagerRegistry     $managerRegistry,
        private readonly FilterExtension     $filterExtension,
        private readonly SerializerInterface $serializer,
        private readonly RequestStack        $requestStack
    ) {
        $this->addExtension($this->filterExtension);
    }

    /**
     * @param QueryCollectionExtensionInterface $extension
     *
     * @return void
     */
    public function addExtension(QueryCollectionExtensionInterface $extension): void
    {
        $this->extensions[] = $extension;
    }

    /**
     * @param string $filterClass
     * @param array  $properties
     * @param array  $options
     *
     * @return void
     */
    public function addFilter(string $filterClass, array $properties, array $options = []): void
    {
        $this->filterExtension->addFilter($filterClass, $properties, $options);
    }

    /**
     * @param string|null $filterMode
     * @return void
     */
    public function setFilterMode(?string $filterMode = null): void
    {
        $this->filterExtension->setFilterMode($filterMode);
    }

    /**
     * @param string $resourceClass
     *
     * @return bool
     */
    public function supports(string $resourceClass): bool
    {
        return $this->managerRegistry->getManagerForClass($resourceClass) instanceof EntityManagerInterface;
    }

    /**
     * @param string            $resourceClass
     * @param QueryBuilder|null $queryBuilder
     *
     * @return float|int|mixed|string
     */
    public function getCollection(string $resourceClass, ?QueryBuilder $queryBuilder = null): mixed
    {
        $queryBuilder = $this->getQueryBuilder($resourceClass, $queryBuilder);
        return $queryBuilder->getQuery()->getResult();
    }

    /**
     * @param string            $resourceClass
     * @param QueryBuilder|null $customQueryBuilder
     *
     * @return QueryBuilder
     */
    public function getQueryBuilder(string $resourceClass, ?QueryBuilder $customQueryBuilder = null): QueryBuilder
    {
        if (null === $customQueryBuilder) {
            /** @var EntityManagerInterface $manager */
            $manager = $this->managerRegistry->getManager();

            $repository = $manager
                ->getRepository($resourceClass)
            ;

            if (!method_exists($repository, 'createQueryBuilder')) {
                throw new RuntimeException('The repository class must have a "createQueryBuilder" method.');
            }

            $queryBuilder = $repository->createQueryBuilder('o');
        } else {
            $queryBuilder = $customQueryBuilder;
        }

        return $queryBuilder;
    }

    /**
     * @param string            $resourceClass
     * @param QueryBuilder|null $queryBuilder
     *
     * @return array
     */
    #[ArrayShape(['results' => "mixed", 'headers' => "array"])]
    public function getArrayResults(string $resourceClass, ?QueryBuilder $queryBuilder = null): array
    {
        $queryBuilder   = $this->getQueryBuilder($resourceClass, $queryBuilder);
        $queryBuilder   = $this->applyExtensions($queryBuilder, $resourceClass);
        $results        = $this->getCollection($resourceClass, $queryBuilder);

        $headers = TagExtractor::getCacheHeaders($resourceClass);

        return [
            'results' => $results,
            'headers' => $headers
        ];
    }

    /**
     * @param string            $resourceClass
     * @param QueryBuilder|null $queryBuilder
     * @param array             $serializationGroups
     * @param array             $cacheTags
     * @param bool              $cacheEnabled
     *
     * @return JsonResponse
     */
    public function getResponse(
        string $resourceClass,
        ?QueryBuilder $queryBuilder = null,
        array $serializationGroups = ['get'],
        array $cacheTags = [],
        bool $cacheEnabled = true
    ): JsonResponse {
        $queryBuilder   = $this->getQueryBuilder($resourceClass, $queryBuilder);
        $queryBuilder   = $this->applyExtensions($queryBuilder, $resourceClass);
        $results        = $this->getCollection($resourceClass, $queryBuilder);

        $headers = $cacheEnabled === true ? TagExtractor::getCacheHeaders($resourceClass, $results, $cacheTags) : [];
        $response = new JsonResponse($this->serializer->serialize(
            $results,
            "json",
            [
                'groups' => $serializationGroups,
                'collection' => true,
                'published' => $this->isPublishedVersionsQuery()
            ]
        ), 200, $headers, true);

        return $this->updateResponseWithPagination($response, $queryBuilder);
    }

    /**
     * @param JsonResponse $response
     * @param QueryBuilder $queryBuilder
     *
     * @return JsonResponse
     */
    private function updateResponseWithPagination(JsonResponse $response, QueryBuilder $queryBuilder): JsonResponse
    {
        if (null !== $pagination = $this->getPagination()) {
            [$totalRecords, $totalPages, $currentPage, $limit] = $pagination->getPaginationData($queryBuilder);
            $response->headers->set("Pagination-count", (string)$totalPages);
            $response->headers->set("Pagination-page", (string)$currentPage);
            $response->headers->set("Pagination-limit", (string)$limit);
            $response->headers->set("Pagination-records", (string)$totalRecords);
        }
        return $response;
    }

    /**
     * @param QueryBuilder $queryBuilder
     * @param string       $resourceClass
     *
     * @return QueryBuilder
     */
    private function applyExtensions(QueryBuilder $queryBuilder, string $resourceClass): QueryBuilder
    {
        $queryNameGenerator = new QueryNameGenerator();
        foreach ($this->extensions as $extension) {
            $extension->applyToCollection($queryBuilder, $queryNameGenerator, $resourceClass);
        }
        return $queryBuilder;
    }

    /**
     * @return PaginationExtension|null
     */
    private function getPagination(): ?PaginationExtension
    {
        foreach ($this->extensions as $extension) {
            if ($extension instanceof PaginationExtension) {
                return $extension;
            }
        }
        return null;
    }

    /**
     * @return bool
     */
    private function isPublishedVersionsQuery(): bool
    {
        $request = $this->requestStack->getMainRequest();
        if (null !== $request) {
            if ($request->headers->has("Client-Origin")
                && $request->headers->get("Client-Origin") === 'frontend') {
                return true;
            }

            return (bool)$request->get("published", false) === true;
        }
        return false;
    }
}
