<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Extension;

use App\Kernel;
use Doctrine\ORM\QueryBuilder;
use Doctrine\ORM\Tools\Pagination\Paginator;
use SG\CmsBundle\Api\Doctrine\Orm\Util\Pagination;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Psr\Container\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class PaginationExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class PaginationExtension implements QueryCollectionExtensionInterface
{
    private ContainerInterface $serviceLocator;
    private Pagination $pagination;

    public function __construct(Kernel $kernel, private RequestStack $requestStack)
    {
        $this->serviceLocator = $kernel->getContainer();
    }

    /**
     * @param array $options
     *
     * @return $this
     */
    public function configure(array $options): self
    {
        $this->pagination = (new Pagination($options))
            ->setRequest($this->requestStack->getCurrentRequest());
        return $this;
    }

    /**
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     *
     * @return void
     */
    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass): void
    {
        if (null === $pagination = $this->getPagination($queryBuilder)) {
            return;
        }

        [$offset, $limit] = $pagination;
        $queryBuilder
            ->setFirstResult($offset)
            ->setMaxResults($limit);
    }

    /**
     * @param QueryBuilder $queryBuilder
     *
     * @return array
     */
    public function getPaginationData(QueryBuilder $queryBuilder): array
    {
        [$currentPage, $offset, $limit] = $this->pagination->getPagination();
        $paginator = new Paginator($queryBuilder->getQuery(), $fetchJoinCollection = true);
        $totalRows = count($paginator);
        $totalPages = $limit > 0 ? ceil($totalRows/$limit) : 1;
        return [$totalRows, $totalPages, $currentPage, $limit];
    }

    /**
     * @param QueryBuilder $queryBuilder
     *
     * @return array|null
     */
    private function getPagination(QueryBuilder $queryBuilder): ?array
    {
        if (null !== $this->requestStack && null === $this->requestStack->getCurrentRequest()) {
            return null;
        }

        return \array_slice($this->pagination->getPagination(), 1);
    }
}
