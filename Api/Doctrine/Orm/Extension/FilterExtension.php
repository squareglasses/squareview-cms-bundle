<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Extension;

use Doctrine\ORM\QueryBuilder;
use Exception;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\FilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\OrderFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class FilterExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class FilterExtension implements QueryCollectionExtensionInterface
{
    const FILTER_MODE_AND = 'and';
    const FILTER_MODE_OR = 'or';

    private array $filters = [];
    private string $filterMode = self::FILTER_MODE_AND;

    /**
     * @param Container $serviceLocator
     */
    public function __construct(private readonly Container $serviceLocator)
    {
    }

    /**
     * @param string $filterClass
     * @param array  $properties
     * @param array  $options
     *
     * @return void
     */
    public function addFilter(string $filterClass, array $properties, array $options = []): void
    {
        if (!array_key_exists($filterClass, $this->filters)) {
            $this->filters[$filterClass] = $properties;
        } else {
            $this->filters[$filterClass] = array_merge($this->filters[$filterClass], $properties);
        }
    }

    /**
     * @param string|null $filterMode
     * @return void
     */
    public function setFilterMode(?string $filterMode = null): void
    {
        if ($filterMode === null) {
            return;
        } else if (in_array($filterMode, [
            self::FILTER_MODE_AND,
            self::FILTER_MODE_OR
        ])) {
            $this->filterMode = $filterMode;
            return;
        } else {
            throw new \LogicException("Filter mode ".$filterMode." not implemented");
        }
    }

    /**
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     *
     * @return void
     * @throws Exception
     */
    public function applyToCollection(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass): void
    {
        if (empty($this->filters)) {
            return;
        }

        $orderFilters = [];

        foreach ($this->filters as $filterId => $properties) {
            $filter = $this->getFilter($filterId);
            if ($filter instanceof FilterInterface) {
                if ($filter instanceof OrderFilter) {
                    $orderFilters[$filterId] = $properties;
                    continue;
                }
                $filter->apply($queryBuilder, $queryNameGenerator, $resourceClass, $this->filterMode, $properties);
            }
        }

        foreach ($orderFilters as $filterId => $properties) {
            $filter->apply($queryBuilder, $queryNameGenerator, $resourceClass, $this->filterMode, $properties);
        }
    }

    /**
     * @param string $filterId
     *
     * @return FilterInterface|null
     * @throws Exception
     */
    private function getFilter(string $filterId): ?object
    {
        if ($this->serviceLocator->has($filterId)) {
            return $this->serviceLocator->get($filterId);
        }
        return null;
    }
}
