<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Filter;

use Doctrine\ORM\QueryBuilder;
use Hoa\Iterator\Filter;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\FilterExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;

interface FilterInterface
{
    /**
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param array $properties
     * @return void
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $filterMode = FilterExtension::FILTER_MODE_AND, array $properties = []): void;
}
