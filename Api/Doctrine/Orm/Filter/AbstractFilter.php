<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Filter;

use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\NullLogger;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Api\Doctrine\Common\PropertyHelperTrait;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use SG\CmsBundle\Api\Util\RequestParser;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\NameConverter\NameConverterInterface;
use function array_key_exists;

/**
 * Class AbstractFilter
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractFilter implements FilterInterface
{
    use PropertyHelperTrait;

    protected array $properties = [];
    protected string $filterMode;

    /**
     * @param ManagerRegistry             $managerRegistry
     * @param RequestStack                $requestStack
     * @param LoggerInterface|null        $logger
     * @param NameConverterInterface|null $nameConverter
     */
    public function __construct(
        protected ManagerRegistry           $managerRegistry,
        protected RequestStack              $requestStack,
        protected ?LoggerInterface          $logger = null,
        protected ?NameConverterInterface   $nameConverter = null
    ) {
        $this->logger           = $logger ?? new NullLogger();
    }

    /**
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     * @param string                      $filterMode (and/or)
     * @param array                       $properties (default [])
     *
     * @return void
     */
    public function apply(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass, string $filterMode = FilterExtension::FILTER_MODE_AND, array $properties = []): void
    {
        if (null === $this->requestStack || null === $request = $this->requestStack->getCurrentRequest()) {
            return;
        }

        $this->properties = $properties;
        $this->filterMode = $filterMode;

        foreach ($this->extractProperties($request, $resourceClass) as $property => $value) {
            $this->filterProperty($this->denormalizePropertyName($property), $value, $queryBuilder, $queryNameGenerator, $resourceClass);
        }
    }

    /**
     * Passes a property through the filter.
     *
     * @param string                      $property
     * @param                             $value
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     * @param string                      $filterMode (and/or)
     *
     * @return void
     */
    abstract protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass): void;

    /**
     * @return ManagerRegistry
     */
    protected function getManagerRegistry(): ManagerRegistry
    {
        return $this->managerRegistry;
    }

    /**
     * @return LoggerInterface
     */
    protected function getLogger(): LoggerInterface
    {
        return $this->logger;
    }

    /**
     * Extracts properties to filter from the request.
     *
     * @param Request $request
     * @param string  $resourceClass
     *
     * @return array
     */
    protected function extractProperties(Request $request, string $resourceClass): array
    {
        $needsFixing = false;
        if (null !== $this->properties) {
            foreach ($this->properties as $property => $value) {
                if (($this->isPropertyNested($property, $resourceClass) || $this->isPropertyEmbedded($property, $resourceClass))
                    && $request->query->has(str_replace('.', '_', $property))) {
                    $needsFixing = true;
                }
            }
        }

        if ($needsFixing) {
            $request = RequestParser::parseAndDuplicateRequest($request);
        }

        return $request->query->all();
    }

    /**
     * Determines whether the given property is enabled.
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return bool
     */
    protected function isPropertyEnabled(string $property, string $resourceClass): bool
    {
        if (null === $this->properties) {
            // to ensure sanity, nested properties must still be explicitly enabled
            return !$this->isPropertyNested($property, $resourceClass);
        }

        return array_key_exists($property, $this->properties);
    }

    /**
     * @param string $property
     *
     * @return string
     */
    protected function denormalizePropertyName(string $property): string
    {
        if (!$this->nameConverter instanceof NameConverterInterface) {
            return $property;
        }

        return implode('.', array_map([$this->nameConverter, 'denormalize'], explode('.', $property)));
    }

    /**
     * @param string $property
     *
     * @return string
     */
    protected function normalizePropertyName(string $property): string
    {
        if (!$this->nameConverter instanceof NameConverterInterface) {
            return $property;
        }

        return implode('.', array_map([$this->nameConverter, 'normalize'], explode('.', $property)));
    }
}
