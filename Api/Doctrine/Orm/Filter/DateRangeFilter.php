<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Filter;

use DateTime;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterTrait;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\FilterExtension;
use SG\CmsBundle\Api\Exception\InvalidArgumentException;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;

/**
 * Class DateRangeFilter
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DateRangeFilter extends AbstractFilter implements SearchFilterInterface
{
    use SearchFilterTrait;

    public const DOCTRINE_INTEGER_TYPE = IntegerType::class;

    /**
     * @param string                      $property
     * @param                             $value
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     *
     * @return void
     * @throws Exception
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass): void
    {
        if (
            null === $value ||
            !$this->isPropertyEnabled($property, $resourceClass) ||
            !$this->isPropertyMapped($property, $resourceClass, true)
        ) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $field = $property;

        $associations = [];
        if ($this->isPropertyNested($property, $resourceClass)) {
            [$alias, $field, $associations] = $this->addJoinsForNestedProperty($property, $alias, $queryBuilder, $queryNameGenerator, $resourceClass, Join::LEFT_JOIN);
        }

        $values = $this->normalizeValues((array) $value, $property);

        if (null === $values) {
            return;
        }

        $metadata = $this->getNestedMetadata($resourceClass, $associations);

        $doctrineTypeField = $this->getDoctrineFieldType($property, $resourceClass);

        if ($metadata->hasField($field)) {
            if (!$this->hasValidValues($values, $doctrineTypeField)) {
                $this->logger->notice('Invalid filter ignored', [
                    'exception' => new InvalidArgumentException(sprintf('Values for field "%s" are not valid according to the doctrine type.', $field)),
                ]);

                return;
            }

            if (array_key_exists("min", $values)) {
                $this->addWhere(">=", $queryBuilder, $queryNameGenerator, $alias, $field, $doctrineTypeField, $values["min"]);
            }
            if (array_key_exists("max", $values)) {
                $this->addWhere("<=", $queryBuilder, $queryNameGenerator, $alias, $field, $doctrineTypeField, $values["max"]);
            }
        }
    }

    /**
     * @param array  $values
     * @param string $property
     *
     * @return array|null
     */
    protected function normalizeValues(array $values, string $property): ?array
    {
        try {
            foreach ($values as $key => $value) {
                if ($key === "max") {
                    $values[$key] = new DateTime($value." 23:59:59");
                } else {
                    $values[$key] = new DateTime($value);
                }
            }
        } catch (\Exception $e) {
            $this->getLogger()->notice('Invalid date value, filter ignored', [
                'exception' => new InvalidArgumentException($e->getMessage()),
            ]);

            return null;
        }
        return $values;
    }

    /**
     * @param string                      $operator
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $alias
     * @param string                      $field
     * @param                             $fieldType
     * @param                             $value
     *
     * @return void
     */
    protected function addWhere(string $operator, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $field, $fieldType, $value): void
    {
        $valueParameter = $queryNameGenerator->generateParameterName($field);
        $queryBuilder
            ->andWhere(sprintf('%s.%s'.' '.$operator.' '.':%s', $alias, $field, $valueParameter))
            ->setParameter($valueParameter, $value, $fieldType);
    }
}
