<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Filter;


use Doctrine\DBAL\Types\BooleanType;
use Doctrine\DBAL\Types\Type as DBALType;
use LogicException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterTrait;
use SG\CmsBundle\Api\Exception\InvalidArgumentException;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\FilterExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use function in_array;

/**
 * Class BooleanFilter
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class BooleanFilter extends AbstractFilter implements SearchFilterInterface
{
    use SearchFilterTrait;

    public const DOCTRINE_BOOLEAN_TYPES = [
        'boolean' => true,
    ];

    /**
     * @param string                      $property
     * @param                             $value
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $resourceClass
     *
     * @return void
     */
    protected function filterProperty(string $property, $value, QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $resourceClass): void
    {
        if (
            !$this->isPropertyEnabled($property, $resourceClass) ||
            !$this->isPropertyMapped($property, $resourceClass, true) ||
            !$this->isBooleanField($property, $resourceClass)
        ) {
            return;
        }

        $value = $this->normalizeValue($value, $property);
        if (null === $value) {
            return;
        }

        $alias = $queryBuilder->getRootAliases()[0];
        $field = $property;

        if ($this->isPropertyNested($property, $resourceClass)) {
            [$alias, $field] = $this->addJoinsForNestedProperty($property, $alias, $queryBuilder, $queryNameGenerator, $resourceClass, Join::LEFT_JOIN);
        }

        $valueParameter = $queryNameGenerator->generateParameterName($field);

        switch($this->filterMode) {
            case FilterExtension::FILTER_MODE_AND:
                $queryBuilder->andWhere(sprintf('%s.%s = :%s', $alias, $field, $valueParameter));
                break;
            case FilterExtension::FILTER_MODE_OR:
                $queryBuilder->orWhere(sprintf('%s.%s = :%s', $alias, $field, $valueParameter));
                break;
            default:
                throw new \LogicException("Filter mode ".$filterMode." not implemented");
        }

        $queryBuilder->setParameter($valueParameter, $value);
    }

    /**
     * Determines whether the given property refers to a boolean field.
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return bool
     */
    protected function isBooleanField(string $property, string $resourceClass): bool
    {
        return isset(self::DOCTRINE_BOOLEAN_TYPES[(string) $this->getDoctrineFieldType($property, $resourceClass)]);
    }

    /**
     * @param        $value
     * @param string $property
     *
     * @return bool|null
     */
    private function normalizeValue($value, string $property): ?bool
    {
        if (in_array($value, [true, 'true', '1'], true)) {
            return true;
        }

        if (in_array($value, [false, 'false', '0'], true)) {
            return false;
        }

//        $this->getLogger()->notice('Invalid filter ignored', [
//            'exception' => new InvalidArgumentException(sprintf('Invalid boolean value for "%s" property, expected one of ( "%s" )', $property, implode('" | "', [
//                'true',
//                'false',
//                '1',
//                '0',
//            ]))),
//        ]);

        return null;
    }
}
