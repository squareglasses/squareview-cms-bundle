<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Util;

/**
 * Class QueryNameGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class QueryNameGenerator implements QueryNameGeneratorInterface
{
    private int $incrementedAssociation = 1;
    private int $incrementedName = 1;

    /**
     * @param string $association
     *
     * @return string
     */
    public function generateJoinAlias(string $association): string
    {
        return sprintf('%s_a%d', $association, $this->incrementedAssociation++);
    }

    /**
     * @param string $name
     *
     * @return string
     */
    public function generateParameterName(string $name): string
    {
        return sprintf('%s_p%d', str_replace('.', '_', $name), $this->incrementedName++);
    }
}
