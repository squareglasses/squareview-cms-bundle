<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Util;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Utility functions for working with Doctrine ORM query.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class QueryChecker
{
    /**
     * Determines whether the QueryBuilder already has a left join.
     *
     * @param QueryBuilder $queryBuilder
     *
     * @return bool
     */
    public static function hasLeftJoin(QueryBuilder $queryBuilder): bool
    {
        foreach ($queryBuilder->getDQLPart('join') as $joins) {
            foreach ($joins as $join) {
                if (Join::LEFT_JOIN === $join->getJoinType()) {
                    return true;
                }
            }
        }

        return false;
    }
}
