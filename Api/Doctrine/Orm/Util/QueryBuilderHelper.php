<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Util;

use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * Class QueryBuilderHelper
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class QueryBuilderHelper
{
    /**
     * Adds a join to the QueryBuilder if none exists.
     *
     * @param QueryBuilder                $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string                      $alias
     * @param string                      $association
     * @param string|null                 $joinType
     * @param string|null                 $conditionType
     * @param string|null                 $condition
     * @param string|null                 $originAlias
     * @param string|null                 $newAlias
     *
     * @return string
     */
    public static function addJoinOnce(QueryBuilder $queryBuilder, QueryNameGeneratorInterface $queryNameGenerator, string $alias, string $association, string $joinType = null, string $conditionType = null, string $condition = null, string $originAlias = null, string $newAlias = null): string
    {
        $join = self::getExistingJoin($queryBuilder, $alias, $association, $originAlias);

        if (null !== $join) {
            return $join->getAlias();
        }

        $associationAlias = $newAlias ?? $queryNameGenerator->generateJoinAlias($association);
        $query = "$alias.$association";

        if (Join::LEFT_JOIN === $joinType || QueryChecker::hasLeftJoin($queryBuilder)) {
            $queryBuilder->leftJoin($query, $associationAlias, $conditionType, $condition);
        } else {
            $queryBuilder->innerJoin($query, $associationAlias, $conditionType, $condition);
        }

        return $associationAlias;
    }

    /**
     * Gets the existing join from QueryBuilder DQL parts.
     *
     * @param QueryBuilder $queryBuilder
     * @param string       $alias
     * @param string       $association
     * @param string|null  $originAlias
     *
     * @return Join|null
     */
    private static function getExistingJoin(QueryBuilder $queryBuilder, string $alias, string $association, string $originAlias = null): ?Join
    {
        $parts = $queryBuilder->getDQLPart('join');
        $rootAlias = $originAlias ?? $queryBuilder->getRootAliases()[0];

        if (!isset($parts[$rootAlias])) {
            return null;
        }

        foreach ($parts[$rootAlias] as $join) {
            /** @var Join $join */
            if (sprintf('%s.%s', $alias, $association) === $join->getJoin()) {
                return $join;
            }
        }

        return null;
    }
}
