<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Util;

use SG\CmsBundle\Api\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Pagination
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class Pagination
{
    private array $options;
    private ?Request $request = null;

    /**
     * @param array $options
     */
    public function __construct(array $options = [])
    {
        $this->options = array_merge([
            'items_per_page' => 30,
            'page_default' => 1,
            'page_parameter_name' => 'page',
            'enabled_parameter_name' => 'pagination',
            'items_per_page_parameter_name' => 'itemsPerPage',
            'maximum_items_per_page' => null
        ], $options);
    }

    /**
     * @param Request $request
     *
     * @return $this
     */
    public function setRequest(Request $request): self
    {
        $this->request = $request;
        return $this;
    }

    /**
     * Gets info about the pagination.
     *
     * Returns an array with the following info as values:
     *   - the page {@see Pagination::getPage()}
     *   - the offset {@see Pagination::getOffset()}
     *   - the limit {@see Pagination::getLimit()}
     *
     * @throws InvalidArgumentException
     */
    public function getPagination(): array
    {
        $page = $this->getPage();
        $limit = $this->getLimit();

        if (0 === $limit && 1 < $page) {
            throw new InvalidArgumentException('Page should not be greater than 1 if limit is equal to 0');
        }

        return [$page, $this->getOffset(), $limit];
    }

    /**
     * @return int
     */
    private function getPage(): int
    {
        $page = (int)$this->request->get($this->options['page_parameter_name'], $this->options['page_default']);

        if (1 > $page) {
            throw new InvalidArgumentException('Page should not be less than 1');
        }

        return $page;
    }

    /**
     * @return int
     */
    private function getLimit(): int
    {
        $limit = $this->options['items_per_page'];
        $limit = (int) $this->request->get($this->options['items_per_page_parameter_name'], $limit);
        $maxItemsPerPage = $this->options['maximum_items_per_page'];

        if (null !== $maxItemsPerPage && $limit > $maxItemsPerPage) {
            $limit = $maxItemsPerPage;
        }

        if (0 > $limit) {
            throw new InvalidArgumentException('Limit should not be less than 0');
        }

        return $limit;
    }

    /**
     * @return int
     */
    public function getOffset(): int
    {
        $limit = $this->getLimit();
        return ($this->getPage() - 1) * $limit;
    }
}
