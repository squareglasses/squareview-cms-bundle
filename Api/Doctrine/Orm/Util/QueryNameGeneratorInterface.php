<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Orm\Util;

/**
 * Interface QueryNameGeneratorInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface QueryNameGeneratorInterface
{
    /**
     * Generates a cacheable alias for DQL join.
     *
     * @param string $association
     *
     * @return string
     */
    public function generateJoinAlias(string $association): string;

    /**
     * Generates a cacheable parameter name for DQL query.
     *
     * @param string $name
     *
     * @return string
     */
    public function generateParameterName(string $name): string;
}
