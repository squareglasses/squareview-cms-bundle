<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Common;

use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use JetBrains\PhpStorm\ArrayShape;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryBuilderHelper;
use SG\CmsBundle\Api\Doctrine\Orm\Util\QueryNameGeneratorInterface;
use SG\CmsBundle\Api\Exception\InvalidArgumentException;
use Symfony\Component\HttpFoundation\Request;
use function array_slice;
use function count;

/**
 * Trait PropertyHelperTrait
 * Helper trait for getting information regarding a property using the resource metadata.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait PropertyHelperTrait
{
    abstract protected function getManagerRegistry(): ManagerRegistry;

    /**
     * Determines whether the given property is mapped.
     *
     * @param string $property
     * @param string $resourceClass
     * @param bool   $allowAssociation
     *
     * @return bool
     */
    protected function isPropertyMapped(string $property, string $resourceClass, bool $allowAssociation = false): bool
    {
        // replace currentVersion relation to versions
        $property = str_replace(array(".currentVersion"), array(".versions"), $property);

        if ($this->isPropertyNested($property, $resourceClass)) {
            $propertyParts = $this->splitPropertyParts($property, $resourceClass);
            $metadata = $this->getNestedMetadata($resourceClass, $propertyParts['associations']);
            $property = $propertyParts['field'];
        } else {
            $metadata = $this->getClassMetadata($resourceClass);
        }

        return $metadata->hasField($property) || ($allowAssociation && $metadata->hasAssociation($property));
    }

    /**
     * Determines whether the given property is nested.
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return bool
     */
    protected function isPropertyNested(string $property, string $resourceClass): bool
    {
        $pos = strpos($property, '.');
        if (false === $pos) {
            return false;
        }
        return $this->getClassMetadata($resourceClass)->hasAssociation(substr($property, 0, $pos));
    }

    /**
     * Determines whether the given property is embedded.
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return bool
     */
    protected function isPropertyEmbedded(string $property, string $resourceClass): bool
    {
        return str_contains($property, '.') && $this->getClassMetadata($resourceClass)->hasField($property);
    }

    /**
     * Splits the given property into parts.
     *
     * Returns an array with the following keys:
     *   - associations: array of associations according to nesting order
     *   - field: string holding the actual field (leaf node)
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return array
     */
    #[ArrayShape(['associations' => "string[]", 'field' => "string"])]
    protected function splitPropertyParts(string $property, string $resourceClass): array
    {
        // replace currentVersion relation to versions
        $property = str_replace(".currentVersion", ".versions", $property);

        $parts = explode('.', $property);

        $metadata = $this->getClassMetadata($resourceClass);
        $slice = 0;

        foreach ($parts as $part) {
            if ($metadata->hasAssociation($part)) {
                $metadata = $this->getClassMetadata($metadata->getAssociationTargetClass($part));
                ++$slice;
            }
        }

        if (count($parts) === $slice) {
            --$slice;
        }

        return [
            'associations' => array_slice($parts, 0, $slice),
            'field' => implode('.', array_slice($parts, $slice)),
        ];
    }

    /**
     *  Gets the Doctrine Type of given property/resourceClass.
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return string|null
     */
    protected function getDoctrineFieldType(string $property, string $resourceClass): ?string
    {
        $propertyParts = $this->splitPropertyParts($property, $resourceClass);
        $metadata = $this->getNestedMetadata($resourceClass, $propertyParts['associations']);

        return $metadata->getTypeOfField($propertyParts['field']);
    }

    /**
     * Gets nested class metadata for the given resource.
     *
     * @param string $resourceClass
     * @param array  $associations
     *
     * @return ClassMetadata
     */
    protected function getNestedMetadata(string $resourceClass, array $associations): ClassMetadata
    {
        $metadata = $this->getClassMetadata($resourceClass);

        foreach ($associations as $association) {
            if ($metadata->hasAssociation($association)) {
                $associationClass = $metadata->getAssociationTargetClass($association);

                $metadata = $this->getClassMetadata($associationClass);
            }
        }

        return $metadata;
    }

    /**
     * Gets class metadata for the given resource.
     *
     * @param string $resourceClass
     *
     * @return ClassMetadata
     */
    protected function getClassMetadata(string $resourceClass): ClassMetadata
    {
        return $this
            ->getManagerRegistry()
            ->getManagerForClass($resourceClass)
            ->getClassMetadata($resourceClass);
    }

    /**
     * Adds the necessary joins for a nested property.
     *
     * @param string $property
     * @param string $rootAlias
     * @param QueryBuilder $queryBuilder
     * @param QueryNameGeneratorInterface $queryNameGenerator
     * @param string $resourceClass
     * @param string $joinType
     * @param Request|null $request
     *
     * @return array An array where the first element is the join $alias of the leaf entity,
     *               the second element is the $field name
     *               the third element is the $associations array
     */
    protected function addJoinsForNestedProperty(
        string $property,
        string $rootAlias,
        QueryBuilder $queryBuilder,
        QueryNameGeneratorInterface $queryNameGenerator,
        string $resourceClass,
        string $joinType,
        ?Request $request = null
    ): array {
        $propertyParts = $this->splitPropertyParts($property, $resourceClass);
        $parentAlias = $rootAlias;
        $alias = null;

        foreach ($propertyParts['associations'] as $association) {
            $alias = QueryBuilderHelper::addJoinOnce($queryBuilder, $queryNameGenerator, $parentAlias, $association, $joinType);
            $parentAlias = $alias;

            if ($association === 'versions' && null !== $request) {
                $queryBuilder
                    ->andWhere($alias.'.currentVersion = :currentVersionOrder')
                    ->andWhere($alias.'.locale = :currentVersionLocale')
                    ->setparameter('currentVersionOrder', true)
                    ->setparameter('currentVersionLocale', $request->getLocale())
                ;
            }
        }

        if (null === $alias) {
            throw new InvalidArgumentException(sprintf('Cannot add joins for property "%s" - property is not nested.', $property));
        }

        return [$alias, $propertyParts['field'], $propertyParts['associations']];
    }
}
