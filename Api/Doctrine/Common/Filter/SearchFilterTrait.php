<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Common\Filter;

use Doctrine\DBAL\Exception;
use Doctrine\DBAL\Types\IntegerType;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Api\Exception\InvalidArgumentException;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use function is_int;
use function is_string;

/**
 * Trait for filtering the collection by given properties.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait SearchFilterTrait
{
    protected PropertyAccessorInterface $propertyAccessor;

    /**
     * @return LoggerInterface
     */
    abstract protected function getLogger(): LoggerInterface;

    /**
     * @param string $property
     *
     * @return string
     */
    abstract protected function normalizePropertyName(string $property): string;

    /**
     * @param array  $values
     * @param string $property
     *
     * @return array|null
     */
    protected function normalizeValues(array $values, string $property): ?array
    {
        foreach ($values as $key => $value) {
            if (!is_int($key) || !is_string($value)) {
                unset($values[$key]);
            }
        }

        if (empty($values)) {
            $this->getLogger()->notice('Invalid filter ignored', [
                'exception' => new InvalidArgumentException(sprintf('At least one value is required, multiple values should be in "%1$s[]=firstvalue&%1$s[]=secondvalue" format', $property)),
            ]);

            return null;
        }

        return array_values($values);
    }

    /**
     * When the field should be an integer, check that the given value is a valid one.
     *
     * @param array $values
     * @param       $type
     *
     * @return bool
     * @throws Exception
     */
    protected function hasValidValues(array $values, $type = null): bool
    {
        foreach ($values as $value) {
            if (null !== $value && false === filter_var($value, FILTER_VALIDATE_INT) && self::DOCTRINE_INTEGER_TYPE === get_class(IntegerType::getType($type))) {
                return false;
            }
        }

        return true;
    }
}
