<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Common\Filter;

/**
 * Interface for filtering the collection by whether a property value exists or not.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ExistsFilterInterface
{
    public const QUERY_PARAMETER_KEY = 'exists';
}
