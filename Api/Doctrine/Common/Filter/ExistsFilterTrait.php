<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Common\Filter;

use Psr\Log\LoggerInterface;
use SG\CmsBundle\Api\Doctrine\Common\PropertyHelperTrait;
use SG\CmsBundle\Api\Exception\InvalidArgumentException;
use function in_array;
use function is_array;

/**
 * Trait for filtering the collection by given properties.
 *
 * @author Rémi Komornicki <remi@squareglasses.com>
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait ExistsFilterTrait
{
    use PropertyHelperTrait;

    private string $existsParameterName = ExistsFilterInterface::QUERY_PARAMETER_KEY;

    /**
     * @param string $resourceClass
     *
     * @return array
     */
    public function getDescription(string $resourceClass): array
    {
        $description = [];

        //$properties = $this->getProperties();
        //if (null === $properties) {
        $properties = array_fill_keys($this->getClassMetadata($resourceClass)->getFieldNames(), null);
        //}

        foreach ($properties as $property => $unused) {
            if (!$this->isPropertyMapped($property, $resourceClass, true) || !$this->isNullableField($property, $resourceClass)) {
                continue;
            }
            $propertyName = $this->normalizePropertyName($property);
            $description[sprintf('%s[%s]', $this->existsParameterName, $propertyName)] = [
                'property' => $propertyName,
                'type' => 'bool',
                'required' => false,
            ];
        }

        return $description;
    }

    /**
     * Determines whether the given property refers to a nullable field.
     *
     * @param string $property
     * @param string $resourceClass
     *
     * @return bool
     */
    abstract protected function isNullableField(string $property, string $resourceClass): bool;

    /**
     * @return LoggerInterface
     */
    abstract protected function getLogger(): LoggerInterface;

    /**
     * @param string $property
     *
     * @return string
     */
    abstract protected function normalizePropertyName(string $property): string;

    /**
     * @param        $value
     * @param string $property
     *
     * @return bool|null
     */
    private function normalizeValue($value, string $property): ?bool
    {
        if (is_array($value) && isset($value[self::QUERY_PARAMETER_KEY])) {
            @trigger_error(
                sprintf('The ExistsFilter syntax "%s[exists]=true/false" is deprecated since 2.5. Use the syntax "%s[%s]=true/false" instead.', $property, $this->existsParameterName, $property),
                E_USER_DEPRECATED
            );
            $value = $value[self::QUERY_PARAMETER_KEY];
        }

        if (in_array($value, [true, 'true', '1', '', null], true)) {
            return true;
        }

        if (in_array($value, [false, 'false', '0'], true)) {
            return false;
        }

        $this->getLogger()->notice('Invalid filter ignored', [
            'exception' => new InvalidArgumentException(sprintf('Invalid value for "%s[%s]", expected one of ( "%s" )', $this->existsParameterName, $property, implode('" | "', [
                'true',
                'false',
                '1',
                '0',
            ]))),
        ]);

        return null;
    }
}
