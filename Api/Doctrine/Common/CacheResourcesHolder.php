<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Common;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Entity\Route;

/**
 * Class CacheResourcesHolder
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CacheResourcesHolder
{
    private Collection $resources;
    private array $tags = [];
    private Collection $routes;

    public function __construct()
    {
        $this->resources = new ArrayCollection();
        $this->routes = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getResources(): Collection
    {
        return $this->resources;
    }

    /**
     * @param Collection $resources
     *
     * @return void
     */
    public function setResources(Collection $resources): void
    {
        $this->resources = $resources;
    }

    /**
     * @param Cacheable $resource
     *
     * @return $this
     */
    public function addResource(Cacheable $resource): self
    {
        if (!$this->resources->contains($resource)) {
            $this->resources[] = $resource;
        }
        return $this;
    }

    /**
     * @return array
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param array $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }

    /**
     * @param array $tags
     *
     * @return $this
     */
    public function addTags(array $tags): self
    {
        $this->tags = array_merge($this->tags, $tags);
        return $this;
    }

    /**
     * @return Collection
     */
    public function getRoutes(): Collection
    {
        return $this->routes;
    }

    /**
     * @param Collection $routes
     *
     * @return void
     */
    public function setRoutes(Collection $routes): void
    {
        $this->routes = $routes;
    }

    /**
     * @param Route $route
     *
     * @return $this
     */
    public function addRoute(Route $route): self
    {
        if (!$this->routes->contains($route)) {
            $this->routes[] = $route;
        }
        return $this;
    }
}
