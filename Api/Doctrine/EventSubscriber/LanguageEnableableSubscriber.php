<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Exception;
use Metadata\ClassHierarchyMetadata;
use Metadata\MergeableClassMetadata;
use Metadata\MergeableInterface;
use Metadata\MetadataFactory;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\LanguageEnableable;
use Doctrine\ORM\Events;

/**
 * Class LanguageEnableableSubscriber
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class LanguageEnableableSubscriber implements EventSubscriber
{
    /** @var array<string,mixed> $cache */
    public array $cache = array();

    /**
     * TranslatableListener constructor.
     * @param MetadataFactory $metadataFactory
     * @param string|null $languageClass
     */
    public function __construct(private readonly MetadataFactory $metadataFactory, private readonly ?string $languageClass = null)
    {
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata
        ];
    }

    /**
     * @param LoadClassMetadataEventArgs $eventArgs
     *
     * @return void
     * @throws Exception
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $classMetadata = $eventArgs->getClassMetadata();
        $reflexionClass = $classMetadata->reflClass;

        if (null === $reflexionClass || $reflexionClass->isAbstract()) {
            return;
        }

        if ($reflexionClass->implementsInterface(LanguageEnableable::class)) {
            $this->mapLanguageEnableable($classMetadata, $eventArgs);
        }
    }

    /**
     * Add mapping data to a language enableable entity
     *
     * @param ClassMetadata              $mapping
     * @param LoadClassMetadataEventArgs $eventArgs
     *
     * @return void
     * @throws Exception
     */
    private function mapLanguageEnableable(ClassMetadata $mapping, LoadClassMetadataEventArgs $eventArgs): void
    {
        $namingStrategy = $eventArgs
            ->getEntityManager()
            ->getConfiguration()
            ->getNamingStrategy()
        ;

        $metadata = $this->getClassMetadata($mapping->name);

        if ($this->languageClass !== null) {
            if (!class_exists($this->languageClass)) {
                throw new RuntimeException("Language class '".$this->languageClass."' does not exists.");
            }
            if (!$mapping->hasAssociation("languages")) {
                $mapping->mapManyToMany(array(
                    'targetEntity' => $this->languageClass,
                    'fieldName' => 'languages',
                    'fetch'         => ClassMetadataInfo::FETCH_EXTRA_LAZY,
                    'cascade' => array('persist'),
                    'orderBy' => array('position' => 'DESC'),
                    'joinTable' => array(
                        'joinColumns' => array(
                            array(
                                'name' => $namingStrategy->joinKeyColumnName($metadata->name),
                                'referencedColumnName' => $namingStrategy->referenceColumnName(),
                                'onDelete' => 'CASCADE',
                                'onUpdate' => 'CASCADE',
                            ),
                        ),
                        'inverseJoinColumns' => array(
                            array(
                                'name' => 'language_id',
                                'referencedColumnName' => $namingStrategy->referenceColumnName()
                            ),
                        ),
                    )
                ));
            }
        }
    }

    /**
     * Get translatable metadata
     *
     * @param string $className
     *
     * @return ClassHierarchyMetadata|\Metadata\ClassMetadata|MergeableClassMetadata|MergeableInterface|mixed|null
     */
    public function getClassMetadata(string $className): mixed
    {
        if (array_key_exists($className, $this->cache)) {
            return $this->cache[$className];
        }

        $metadata = $this->metadataFactory->getMetadataForClass($className);
        $this->cache[$className] = $metadata;
        return $metadata;
    }

    /**
     * @param string $className
     *
     * @return bool
     */
    public function isCached(string $className): bool
    {
        return array_key_exists($className, $this->cache);
    }
}
