<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\EventSubscriber;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use Psr\Log\LoggerInterface;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Common\Cache\CacheManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class CacheClearerSubscriber
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CacheClearerSubscriber implements EventSubscriberInterface
{
    /**
     * @param CacheResourcesHolder $cacheResourcesHolder
     * @param CacheManager $cacheManager
     * @param LoggerInterface $sv4ErrorsLogger
     */
    public function __construct(
        private readonly CacheResourcesHolder $cacheResourcesHolder,
        private readonly CacheManager         $cacheManager,
        private readonly LoggerInterface      $sv4ErrorsLogger
    ) {
    }

    /**
     * @return string[]
     */
    #[ArrayShape([KernelEvents::TERMINATE => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::TERMINATE => "onKernelFinishRequest"
        ];
    }

    /**
     * @param TerminateEvent $event
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function onKernelFinishRequest(TerminateEvent $event): void
    {
        try {
            $tags = $this->cacheResourcesHolder->getTags();

            foreach ($this->cacheResourcesHolder->getResources() as $resource) {
                $tags = array_merge($tags, $this->cacheManager->getResourceTags($resource));
            }

            if (count($tags) > 0) {
                $this->cacheManager->invalidateCacheTags($tags);
            }

            if ($this->cacheResourcesHolder->getRoutes()->count() > 0) {
                $this->cacheManager->invalidateRouterCache();
            }
        } catch (Exception $exception) {
            $this->sv4ErrorsLogger->critical('Errors onKernelFinishRequest', ['error' => $exception]);
        }
    }
}
