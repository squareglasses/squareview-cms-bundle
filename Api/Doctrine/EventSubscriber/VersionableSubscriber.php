<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Events;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Metadata\MergeableInterface;
use Metadata\MetadataFactory;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;

/**
 * Class VersionableSubscriber
 * Load versions on demand
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class VersionableSubscriber implements EventSubscriber
{
    private array $cache = [];

    /**
     * @param MetadataFactory $metadataFactory
     */
    public function __construct(private readonly MetadataFactory $metadataFactory)
    {
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents(): array
    {
        return array(
            Events::loadClassMetadata
        );
    }

    /**
     * Add mapping to versionable entities
     *
     * @param LoadClassMetadataEventArgs $eventArgs
     *
     * @return void
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs): void
    {
        $classMetadata = $eventArgs->getClassMetadata();
        $reflexionClass = $classMetadata->reflClass;

        if (!$reflexionClass || $reflexionClass->isAbstract()) {
            return;
        }

        if ($reflexionClass->implementsInterface(Versionable::class)) {
            $this->mapVersionable($classMetadata);
        }

        if ($reflexionClass->implementsInterface(Version::class)) {
            $this->mapVersion($classMetadata, $eventArgs);
        }
    }

    /**
     * Add mapping data to a version entity
     *
     * @param ClassMetadata              $mapping
     * @param LoadClassMetadataEventArgs $eventArgs
     *
     * @return void
     */
    private function mapVersion(ClassMetadata $mapping, LoadClassMetadataEventArgs $eventArgs): void
    {
        $namingStrategy = $eventArgs
            ->getEntityManager()
            ->getConfiguration()
            ->getNamingStrategy()
        ;

        $metadata = $this->getMetadata($mapping->name);

        // Map versionable relation
        if (!$mapping->hasAssociation($metadata->versionable->name)) {
            $targetMetadata = $this->getMetadata($metadata->targetEntity);

            $mapping->mapManyToOne(array(
                'fieldName'    => $metadata->versionable->name,
                'targetEntity' => $metadata->targetEntity,
                'inversedBy'   => $targetMetadata->versions->name,
                'cascade'      => array('persist'),
                'joinColumns'  => array(array(
                    'name'                 => 'versionable_id',
                    'referencedColumnName' => $namingStrategy->referenceColumnName(),
                    'onDelete'             => 'CASCADE',
                    'nullable'             => false,
                )),
            ));
        }
    }

    /**
     * Add mapping data to a versionable entity
     *
     * @param ClassMetadata $mapping
     *
     * @return void
     */
    private function mapVersionable(ClassMetadata $mapping): void
    {
        $metadata = $this->getMetadata($mapping->name);

        if (!$mapping->hasAssociation($metadata->versions->name)) {
            $targetMetadata = $this->getMetadata($metadata->targetEntity);
            $mapping->mapOneToMany(array(
                'fieldName'     => $metadata->versions->name,
                'targetEntity'  => $metadata->targetEntity,
                'mappedBy'      => $targetMetadata->versionable->name,
                'fetch'         => ClassMetadataInfo::FETCH_EXTRA_LAZY,
                'cascade'       => array('persist', 'merge', 'remove'),
                'orphanRemoval' => true,
            ));
        }
    }

    /**
     * @param string $className
     *
     * @return MergeableInterface
     */
    public function getMetadata(string $className): MergeableInterface
    {
        if (array_key_exists($className, $this->cache)) {
            return $this->cache[$className];
        }

        if ($metadata = $this->metadataFactory->getMetadataForClass($className)) {
            $metadata->validate();
        }

        $this->cache[$className] = $metadata;
        return $metadata;
    }
}
