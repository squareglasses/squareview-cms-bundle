<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\EventSubscriber;

use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Events;
use Doctrine\Persistence\Event\LifecycleEventArgs;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Repository\MenuItemRepository;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Cache\CacheManager;

/**
 * Class EntitySubscriber
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class EntitySubscriber implements EventSubscriber
{
    /**
     * @param CacheManager $cacheManager
     * @param EntityManagerInterface $em
     * @param HasMediaManager $hasMediaManager
     * @param CacheResourcesHolder $cacheResourcesHolder
     * @param ModuleManager $moduleManager
     * @param TranslationManager $translationManager
     * @param MenuItemRepository $menuItemRepository
     * @param MetaTagManager $metaTagManager
     */
    public function __construct(
        private readonly CacheManager           $cacheManager,
        private readonly EntityManagerInterface $em,
        private readonly HasMediaManager        $hasMediaManager,
        private readonly CacheResourcesHolder   $cacheResourcesHolder,
        private readonly ModuleManager          $moduleManager,
        private readonly TranslationManager     $translationManager,
        private readonly MenuItemRepository     $menuItemRepository,
        private readonly MetaTagManager         $metaTagManager
    ) {
    }

    /**
     * @return array|string[]
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::postPersist,
            Events::postUpdate,
            Events::preRemove
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @return void
     */
    public function postUpdate(LifecycleEventArgs $args): void
    {
        $this->manageEntity($args->getObject());
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @return void
     */
    public function postPersist(LifecycleEventArgs $args): void
    {
        $this->manageEntity($args->getObject());
    }

    /**
     * @param LifecycleEventArgs $args
     *
     * @return void
     */
    public function preRemove(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();
        if ($entity instanceof Cacheable) {
            $this->cacheResourcesHolder->addTags($this->cacheManager->getResourceTags($entity));
        }

        // Delete associated routes
        if ($entity instanceof Routable) {
            foreach ($this->em->getRepository(Route::class)->findByResource($entity) as $route) {
                $this->em->remove($route);
            }
        }

        // Delete associated medias
        if ($entity instanceof HasMediasInterface) {
            $this->hasMediaManager->removeResourceMedias($entity);
        }

        // Delete associated modules
        if ($entity instanceof Modulable) {
            $this->moduleManager->deleteAllModulesForResource($entity);
        }

        // Delete associated metaTags
        if ($entity instanceof MetaTaggable) {
            $this->metaTagManager->deleteAllMetaTagsForResource($entity);
        }

        // Delete translations
        if ($entity instanceof Translatable) {
            $this->translationManager->deleteResourceTranslations($entity);
        }

        // Delete associated menu items
        if ($entity instanceof CmsResourceInterface) {
            $this->menuItemRepository->removeByResource($entity);
        }
    }


    /**
     * @param object $entity
     *
     * @return void
     */
    public function manageEntity(object $entity): void
    {
        if ($entity instanceof Cacheable && !($entity instanceof Versionable)) {
            $this->cacheResourcesHolder->addResource($entity);
        }
        if ($entity instanceof Route) {
            $this->cacheResourcesHolder->addRoute($entity);
        }
    }
}
