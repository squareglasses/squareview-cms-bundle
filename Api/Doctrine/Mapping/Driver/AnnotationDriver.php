<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Mapping\Driver;

use Doctrine\Common\Annotations\Reader;
use Metadata\PropertyMetadata;
use ReflectionClass;
use Metadata\Driver\DriverInterface;
use Metadata\ClassMetadata;
use SG\CmsBundle\Api\Doctrine\Mapping\VersionableMetadata;
use SG\CmsBundle\Api\Doctrine\Mapping\VersionMetadata;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Annotation\Versions;
use SG\CmsBundle\Api\Doctrine\Annotation\Versionable as VersionableAnnotation;
use SG\CmsBundle\Api\Doctrine\Mapping\TranslatableMetadata;
use SG\CmsBundle\Api\Contracts\Translatable;

/**
 * Class AnnotationDriver
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AnnotationDriver implements DriverInterface
{
    /**
     * @param Reader $reader
     */
    public function __construct(private readonly Reader $reader)
    {
    }

    /**
     * @param ReflectionClass $class
     *
     * @return ClassMetadata|null
     */
    public function loadMetadataForClass(ReflectionClass $class): ?ClassMetadata
    {
        if ($class->implementsInterface(Versionable::class)) {
            return $this->loadVersionableMetadata($class);
        }

        if ($class->implementsInterface(Version::class)) {
            return $this->loadVersionMetadata($class);
        }

        if ($class->implementsInterface(Translatable::class)) {
            return $this->loadTranslatableMetadata($class);
        }

        return null;
    }

    /**
     * @param ReflectionClass $class
     *
     * @return ClassMetadata
     */
    private function loadVersionableMetadata(ReflectionClass $class): ClassMetadata
    {
        $classMetadata = new VersionableMetadata($class->name);

        foreach ($class->getProperties() as $property) {
            if ($property->class !== $class->name) {
                continue;
            }

            $propertyMetadata = new PropertyMetadata($class->name, $property->getName());

            if ($annotation = $this->reader->getPropertyAnnotation($property, Versions::class)) {
                $classMetadata->targetEntity = $annotation->targetEntity;
                $classMetadata->versions = $propertyMetadata;
                $classMetadata->addPropertyMetadata($propertyMetadata);
            }
        }

        return $classMetadata;
    }

    /**
     * Load version metadata for a version class
     *
     * @param ReflectionClass $class
     *
     * @return VersionMetadata
     */
    private function loadVersionMetadata(ReflectionClass $class): VersionMetadata
    {
        $classMetadata = new VersionMetadata($class->name);

        foreach ($class->getProperties() as $property) {
            if ($property->class !== $class->name) {
                continue;
            }

            $propertyMetadata = new PropertyMetadata($class->name, $property->getName());

            if ($annotation = $this->reader->getPropertyAnnotation($property, VersionableAnnotation::class)) {
                $classMetadata->targetEntity = $annotation->targetEntity;
                $classMetadata->versionable = $propertyMetadata;
                $classMetadata->addPropertyMetadata($propertyMetadata);
            }
        }

        return $classMetadata;
    }

    /**
     * @param ReflectionClass<Translatable> $class
     *
     * @return ClassMetadata
     */
    private function loadTranslatableMetadata(ReflectionClass $class): ClassMetadata
    {
        return new TranslatableMetadata($class->name);
    }
}
