<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Mapping\Driver;

use Doctrine\ORM\Mapping\Driver\AnnotationDriver as DoctrineAnnotationDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriver;
use Doctrine\Persistence\Mapping\Driver\MappingDriverChain;
use Metadata\Driver\DriverChain;
use ReflectionException;

/**
 * Class DoctrineAdapter
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DoctrineAdapter
{
    /**
     * Create a driver from a Doctrine metadata driver
     *
     * @param MappingDriver $omDriver
     *
     * @return DriverChain|AnnotationDriver|null
     * @throws ReflectionException
     */
    public static function fromMetadataDriver(MappingDriver $omDriver): DriverChain|AnnotationDriver|null
    {
        if ($omDriver instanceof MappingDriverChain) {
            $drivers = array();
            foreach ($omDriver->getDrivers() as $nestedOmDriver) {
                $drivers[] = self::fromMetadataDriver($nestedOmDriver);
            }

            return new DriverChain($drivers);
        }

        if ($omDriver instanceof DoctrineAnnotationDriver) {
            return new AnnotationDriver($omDriver->getReader());
        }
        return null;
    }
}
