<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Mapping;

use InvalidArgumentException;
use Metadata\MergeableClassMetadata;
use Metadata\MergeableInterface;
use Metadata\PropertyMetadata;

/**
 * Class VersionableMetadata
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class VersionableMetadata extends MergeableClassMetadata
{
    public string $targetEntity;
    public PropertyMetadata $versions;

    /**
     * @return void
     * @throws MappingException
     */
    public function validate(): void
    {
        if (!$this->targetEntity) {
            throw new MappingException(sprintf('No versions targetEntity specified for %s', $this->name));
        }
    }

    /**
     * @param MergeableInterface $object
     *
     * @return void
     */
    public function merge(MergeableInterface $object): void
    {
        if (!$object instanceof self) {
            throw new InvalidArgumentException(sprintf('$object must be an instance of %s.', __CLASS__));
        }

        parent::merge($object);

        if ($object->targetEntity) {
            $this->targetEntity = $object->targetEntity;
        }

        if ($object->versions) {
            $this->versions = $object->versions;
        }
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize(array(
            $this->targetEntity,
            $this->versions?->name,
            parent::serialize(),
        ));
    }

    /**
     * @param $str
     *
     * @return void
     */
    public function unserialize($str): void
    {
        [
            $this->targetEntity,
            $versions,
            $parent
        ] = unserialize($str);

        parent::unserialize($parent);

        if ($versions) {
            $this->versions = $this->propertyMetadata[$versions];
        }
    }
}
