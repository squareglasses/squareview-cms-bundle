<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Mapping;

use InvalidArgumentException;
use Metadata\MergeableClassMetadata;
use Metadata\MergeableInterface;

/**
 * Class TranslatableMetadata
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslatableMetadata extends MergeableClassMetadata
{
    /**
     * @param MergeableInterface $object
     *
     * @return void
     */
    public function merge(MergeableInterface $object): void
    {
        if (!$object instanceof self) {
            throw new InvalidArgumentException(sprintf('$object must be an instance of %s.', __CLASS__));
        }

        parent::merge($object);
    }

    /**
     * @return void
     */
    public function validate(): void
    {
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize(array(
            parent::serialize(),
        ));
    }

    /**
     * @param string $str
     */
    public function unserialize($str): void
    {
        [
            $parent
        ] = unserialize($str);

        parent::unserialize($parent);
    }
}
