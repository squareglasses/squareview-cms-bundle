<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Mapping;

use Doctrine\ORM\Mapping\MappingException;
use InvalidArgumentException;
use Metadata\MergeableClassMetadata;
use Metadata\MergeableInterface;
use Metadata\PropertyMetadata;

/**
 * Class VersionMetadata
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class VersionMetadata extends MergeableClassMetadata
{
    public string $targetEntity;
    public PropertyMetadata $versionable;

    /**
     * @return void
     * @throws MappingException
     */
    public function validate(): void
    {
        if (!$this->targetEntity) {
            throw new MappingException(sprintf('No versionable targetEntity specified for %s', $this->name));
        }
    }

    /**
     * @param MergeableInterface $object
     *
     * @return void
     */
    public function merge(MergeableInterface $object): void
    {
        if (!$object instanceof self) {
            throw new InvalidArgumentException(sprintf('$object must be an instance of %s.', __CLASS__));
        }

        parent::merge($object);

        if ($object->targetEntity) {
            $this->targetEntity = $object->targetEntity;
        }

        if ($object->versionable) {
            $this->versionable = $object->versionable;
        }
    }

    /**
     * @return string
     */
    public function serialize(): string
    {
        return serialize(array(
            $this->targetEntity,
            $this->versionable?->name,
            parent::serialize(),
        ));
    }

    /**
     * @param $str
     *
     * @return void
     */
    public function unserialize($str): void
    {
        [
            $this->targetEntity,
            $versionable,
            $parent
        ] = unserialize($str);

        parent::unserialize($parent);

        if ($versionable) {
            $this->versionable = $this->propertyMetadata[$versionable];
        }
    }
}
