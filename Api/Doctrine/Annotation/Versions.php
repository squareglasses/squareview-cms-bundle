<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Annotation;

/**
 * Class Versions
 *
 * This annotation indicates the one-to-many relation to the versions.
 *
 * @Annotation
 * @Target("PROPERTY")
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Versions
{
    /**
     * @Required
     */
    public string $targetEntity;
}
