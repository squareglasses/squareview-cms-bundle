<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Annotation;

/**
 * Class Translation
 *
 * @Annotation
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class Translation
{
    public array $options = [];
}
