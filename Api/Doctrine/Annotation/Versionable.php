<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Annotation;

/**
 * Class Versionable
 *
 * This annotation indicates the many-to-one relation to the versionable.
 *
 * @Annotation
 * @Target("PROPERTY")
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Versionable
{
    /**
     * @Required
     */
    public string $targetEntity;
}
