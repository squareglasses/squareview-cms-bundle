<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Annotation;

use Doctrine\Common\Annotations\Reader;
use ReflectionClass;
use ReflectionException;
use RuntimeException;

/**
 * Class EntityTranslationPropertiesExtractor
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class EntityTranslationPropertiesExtractor
{
    /**
     * @param Reader $annotationReader
     */
    public function __construct(private readonly Reader $annotationReader)
    {
    }

    /**
     * Extract all the translatable properties of an entity class and returns an array of properties names and options
     *
     * @param string $className
     *
     * @return array
     */
    public function extractTranslationProperties(string $className): array
    {
        $translatableProperties = [];
        foreach ($this->getReflectionClass($className)->getProperties() as $property) {
            $translatableAnnotation = $this->annotationReader->getPropertyAnnotation($property, Translation::class);
            if (null !== $translatableAnnotation) {
                $translatableProperties[$property->getName()] = $translatableAnnotation->options;
            }
        }

        return $translatableProperties;
    }

    /**
     * @param string $property
     * @param string $className
     *
     * @return bool
     */
    public function isTranslation(string $property, string $className): bool
    {
        foreach ($this->getReflectionClass($className)->getProperties() as $classProperty) {
            $translatableAnnotation = $this->annotationReader->getPropertyAnnotation($classProperty, Translation::class);
            if (null !== $translatableAnnotation && $classProperty->name === $property) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param string $className
     *
     * @return ReflectionClass
     */
    private function getReflectionClass(string $className): ReflectionClass
    {
        try {
            $reflectionClass = new ReflectionClass($className);
        } catch (ReflectionException $e) {
            throw new RuntimeException('Failed to read annotation on class "'.$className.'" '.$e->getMessage());
        }
        return $reflectionClass;
    }
}
