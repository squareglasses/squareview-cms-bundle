<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Contracts;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Trait VersionableTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait VersionableTrait
{
    protected ?Collection $versions = null;

    /**
     * @return Collection
     */
    public function getVersions(): Collection
    {
        if (null === $this->versions) {
            $this->versions = new ArrayCollection();
        }
        return $this->versions;
    }

    /**
     * @param Version $version
     *
     * @return Versionable
     */
    public function addVersion(Version $version): Versionable
    {
        if (!$this->versions->contains($version)) {
            $this->versions[] = $version;
            $version->setVersionable($this);
        }

        return $this;
    }

    /**
     * @param Version $version
     *
     * @return Versionable
     */
    public function removeVersion(Version $version): Versionable
    {
        if ($this->versions->removeElement($version)) {
            $version->setVersionable();
        }

        return $this;
    }
}
