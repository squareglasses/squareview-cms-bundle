<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Contracts;

use SG\CmsBundle\Api\Doctrine\Annotation\ExcludeFromPublication;
use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Entity\AbstractContentVersion;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Trait VersionTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait VersionTrait
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get"})
     */
    protected ?int $id;

    /**
     * @var array<string>|null
     * @ORM\Column(type="json", nullable=true)
     */
    protected ?array $translations = [];

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @ExcludeFromPublication
     */
    protected bool $currentDraft = false;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @ExcludeFromPublication
     */
    protected bool $currentVersion = false;

    /**
     * @var Versionable|null
     */
    protected ?Versionable $versionable = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=4, nullable=true)
     * @ExcludeFromPublication
     */
    protected ?string $locale = null;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     * @ExcludeFromPublication
     */
    protected ?string $hash = null;

    /**
     * @var array<string>
     * @ORM\Column(type="json", nullable=true)
     * @ExcludeFromPublication
     */
    protected ?array $changes = [];

    /**
     * @var array<string>
     * @ORM\Column(type="json", nullable=true)
     * @ExcludeFromPublication
     * @Groups({"get", "get_collection_backend"})
     */
    protected array $draftHashes = [];

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return AbstractContentVersion|VersionTrait
     */
    public function setId(?int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return bool
     */
    public function isCurrentDraft(): bool
    {
        return $this->currentDraft;
    }

    /**
     * @param bool $currentDraft
     *
     * @return Version
     */
    public function setCurrentDraft(bool $currentDraft): Version
    {
        $this->currentDraft = $currentDraft;
        return $this;
    }

    /**
     * @return Versionable|null
     */
    public function getVersionable(): ?Versionable
    {
        return $this->versionable;
    }

    /**
     * @param Versionable|null $versionable
     *
     * @return Version
     */
    public function setVersionable(Versionable $versionable = null): Version
    {
        $this->versionable = $versionable;
        return $this;
//        if ($this->versionable == $versionable) {
//            return $this;
//        }
//
//        $old = $this->versionable;
//        $this->versionable = $versionable;
//
//        if ($old !== null) {
//            $old->removeVersion($this);
//        }
//
//        if ($versionable !== null) {
//            $versionable->addVersion($this);
//        }
//
//        return $this;
    }

    /**
     * @return array|null
     */
    public function getTranslations(): ?array
    {
        return $this->translations;
    }

    /**
     * @param array|null $translations
     */
    public function setTranslations(?array $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     */
    public function setLocale(?string $locale): void
    {
        $this->locale = $locale;
    }

    /**
     * @return bool
     */
    public function isCurrentVersion(): bool
    {
        return $this->currentVersion;
    }

    /**
     * @param bool $currentVersion
     */
    public function setCurrentVersion(bool $currentVersion): void
    {
        $this->currentVersion = $currentVersion;
    }

    /**
     * @return string|null
     */
    public function getHash(): ?string
    {
        return $this->hash;
    }

    /**
     * @param string $locale
     * @return string|null
     */
    public function getHashByLocale(string $locale): ?string
    {
        foreach ($this->draftHashes as $l => $hash) {
            if ($l === $locale) {
                return $hash['hash'];
            }
        }
        return null;
    }

    /**
     * @param string|null $hash
     */
    public function setHash(?string $hash): void
    {
        $this->hash = $hash;
    }

    /**
     * @return array
     */
    public function getDraftHashes(): array
    {
        return $this->draftHashes;
    }

    /**
     * @param array $draftHashes
     */
    public function setDraftHashes(array $draftHashes): void
    {
        $this->draftHashes = $draftHashes;
    }

    /**
     * @return array|null
     */
    public function getChanges(): ?array
    {
        return $this->changes;
    }

    /**
     * @param array|null $changes
     */
    public function setChanges(?array $changes): void
    {
        $this->changes = $changes;
    }
}
