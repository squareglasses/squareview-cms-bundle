<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Contracts;

/**
 * Interface Version
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface Version
{
    /**
     * @return bool
     */
    public function isCurrentDraft(): bool;

    /**
     * @param bool $currentDraft
     *
     * @return Version
     */
    public function setCurrentDraft(bool $currentDraft): Version;

    /**
     * @return Versionable|null
     */
    public function getVersionable(): ?Versionable;

    /**
     * @param Versionable|null $versionable
     *
     * @return Version
     */
    public function setVersionable(?Versionable $versionable = null): Version;

    /**
     * @return array|null
     */
    public function getTranslations(): ?array;

    /**
     * @param array|null $translations
     *
     * @return void
     */
    public function setTranslations(?array $translations): void;

    /**
     * @return string|null
     */
    public function getLocale(): ?string;

    /**
     * @param string|null $locale
     *
     * @return void
     */
    public function setLocale(?string $locale): void;

    /**
     * @return bool
     */
    public function isCurrentVersion(): bool;

    /**
     * @param bool $currentVersion
     *
     * @return void
     */
    public function setCurrentVersion(bool $currentVersion): void;

    /**
     * @return string|null
     */
    public function getHash(): ?string;

    /**
     * @param string|null $hash
     *
     * @return void
     */
    public function setHash(?string $hash): void;

    /**
     * @return array
     */
    public function getDraftHashes(): array;

    /**
     * @param array $draftHashes
     *
     * @return void
     */
    public function setDraftHashes(array $draftHashes): void;
}
