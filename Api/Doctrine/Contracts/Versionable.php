<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Doctrine\Contracts;

use Doctrine\Common\Collections\Collection;

/**
 * Interface Versionable
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface Versionable
{
    /**
     * @return Collection
     */
    public function getVersions(): Collection;

    /**
     * @return Version
     */
    public function getDraftVersion(): Version;

    /**
     * @param string $locale
     *
     * @return Version|null
     */
    public function getCurrentVersion(string $locale): ?Version;

    /**
     * @param Version $version
     *
     * @return $this
     */
    public function addVersion(Version $version): self;

    /**
     * @param Version $version
     *
     * @return $this
     */
    public function removeVersion(Version $version): self;

    /**
     * @return string|null
     */
    public function getSlug(): ?string;

    /**
     * @return string|null
     */
    public function getDiscriminator(): ?string;
}
