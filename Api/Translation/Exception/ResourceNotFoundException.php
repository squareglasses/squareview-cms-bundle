<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation\Exception;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Throwable;

/**
 * Class ResourceNotFoundException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceNotFoundException extends HttpException
{
    /**
     * @param string|null     $message
     * @param Throwable|null $previous
     * @param int             $code
     * @param array           $headers
     */
    public function __construct(string $message = null, Throwable $previous = null, int $code = 0, array $headers = [])
    {
        parent::__construct(404, $message, $previous, $headers, $code);
    }
}
