<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation\Exception;

use Exception;
use Throwable;

/**
 * Class TranslationClientException
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationClientException extends Exception
{
    /**
     * @param string|null     $message
     * @param int             $code
     * @param Throwable|null $previous
     */
    public function __construct(string $message = null, int $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
