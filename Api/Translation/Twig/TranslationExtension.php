<?php declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation\Twig;

use SG\CmsBundle\Api\Translation\Translator;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

/**
 * Class TranslationExtension
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class TranslationExtension extends AbstractExtension
{
    /**
     * @param Translator  $translator
     */
    public function __construct(
        private readonly Translator $translator
    ) {
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters(): array
    {
        return [
            new TwigFilter(
                'trans',
                [$this, 'trans'],
                []
            )
        ];
    }

    /**
     * @param string      $message
     * @param array       $arguments
     * @param string|null $domain
     * @param string|null $locale
     *
     * @return string
     */
    public function trans(
        string $message,
        array $arguments = [],
        string $domain = null,
        string $locale = null
    ): string
    {
        return $this->translator->trans($message, $arguments, $domain, $locale);
    }
}
