<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation\Generator;

use SG\CmsBundle\Api\Translation\Model\Catalog;

/**
 * Class CatalogSlugGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class CatalogSlugGenerator
{
    public const SEPARATOR = '.';

    /**
     * @param string $discriminator
     * @param string $domain
     * @param string $zone
     *
     * @return string
     */
    public static function generate(string $discriminator, string $domain, string $zone): string
    {
        return $discriminator.self::SEPARATOR.$domain.self::SEPARATOR.$zone;
    }

    /**
     * @param Catalog $catalog
     *
     * @return string
     */
    public static function generateFromCatalog(Catalog $catalog): string
    {
        return $catalog->getDiscriminator().self::SEPARATOR.$catalog->getDomain().self::SEPARATOR.$catalog->getZone();
    }
}
