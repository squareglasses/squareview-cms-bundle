<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation\Generator;

use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Contracts\ModuleInterface;
use SG\CmsBundle\Api\Contracts\Translatable;

/**
 * Class TranslationKeyGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class TranslationKeyGenerator
{
    public const SEPARATOR = '.';

    /**
     * @param Translatable $translatable
     * @param string       $property
     *
     * @return string
     */
    public static function generateKey(Translatable $translatable, string $property): string
    {
        return $translatable->getDiscriminator().self::SEPARATOR.$translatable->getSlug().self::SEPARATOR.$property;
    }

    /**
     * @param ModuleInterface $module
     * @param string          $property
     *
     * @return string
     */
    public static function generateModuleParameterKey(ModuleInterface $module, string $property): string
    {
        return $module->getGuideIdentifier().self::SEPARATOR.$module->getIdentifier().self::SEPARATOR.$property;
    }

    /**
     * @param MetaTag $metaTag
     *
     * @return string
     */
    public static function generateMetaTagKey(MetaTag $metaTag): string
    {
        return $metaTag->getName().self::SEPARATOR.'content';
    }
}
