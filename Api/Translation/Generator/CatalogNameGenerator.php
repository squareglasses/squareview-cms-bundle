<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation\Generator;

use SG\CmsBundle\Api\Translation\Model\Catalog;
use SG\CmsBundle\Api\Contracts\Translatable;

/**
 * Class CatalogNameGenerator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class CatalogNameGenerator
{
    public const SEPARATOR = '.';

    /**
     * @param Translatable $translatable
     *
     * @return string
     */
    public static function generateKey(Translatable $translatable): string
    {
        return $translatable->getDiscriminator().self::SEPARATOR.$translatable->getSlug();
    }

    /**
     * @param Catalog $catalog
     *
     * @return string
     */
    public static function generateKeyFromCatalog(Catalog $catalog): string
    {
        return $catalog->getDomain().self::SEPARATOR.$catalog->getZone();
    }
}
