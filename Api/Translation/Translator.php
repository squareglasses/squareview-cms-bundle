<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Entity\TranslationsPublication;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Translation\Formatter\MessageFormatterInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class Translator
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Translator
{
    private array $translations = [];

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly MessageFormatterInterface $formatter,
        private readonly RequestStack $requestStack,
        private readonly TranslatorInterface $fallbackTranslator
    ) {
    }

    public function translate(string $translationKey, string $zone): string
    {
        if (array_key_exists($zone, $this->translations)) {
            foreach ($this->translations[$zone] as $key => $message) {
                if ($key === $translationKey) {
                    return $message;
                }
            }
        }

        return $translationKey;
    }

    /**
     * @param string|null $id
     * @param array       $parameters
     * @param string|null $domain
     * @param string|null $locale
     *
     * @return string
     */
    public function trans(?string $id, array $parameters = [], string $domain = null, string $locale = null): string
    {
        if (null === $id || '' === $id) {
            return '';
        }

        if (null === $locale) {
            $locale = $this->getLocale();
        }

        if (null === $domain) {
            $domain = 'messages';
        }

        $message = $this->formatter->formatIntl($this->translate($id, $domain), $locale, $parameters);
        if ($message === $id) {
            return $this->fallbackTranslator->trans($id, $parameters, $domain, $locale);
        }
        return $message;
    }

    public function loadLayoutGuideTranslations(string $guideIdentifier, string $locale = "fr"): void
    {
        /** @var TranslationsPublication $publication */
        $publication = $this->entityManager
            ->getRepository(TranslationsPublication::class)
            ->findActive($locale, $guideIdentifier);

        $this->mergeTranslations($publication->getTranslations());
    }

    /**
     * @param array $translations
     *
     * @return void
     */
    private function mergeTranslations(array $translations): void
    {
        $this->translations = array_merge($this->translations, $translations);
    }

    /**
     * @return string
     */
    private function getLocale(): string
    {
        return $this->requestStack->getMainRequest()->getLocale();
    }
}
