<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation;

use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Translation\Exception\ResourceNotFoundException;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\Generator\CatalogSlugGenerator;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Serializer\SerializerAwareInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

/**
 * Class TranslationClient
 *
 * @deprecated
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationClient implements SerializerAwareInterface
{
    public const METHOD_GET    = 'GET';
    public const METHOD_POST   = 'POST';

    protected HttpClientInterface $client;
    protected ?SerializerInterface $serializer = null;
    private ?string $apiBearerToken = null;

    /**
     * @param HttpClientInterface   $translateClient
     * @param ParameterBagInterface $parameterBag
     */
    public function __construct(
        protected HttpClientInterface $translateClient,
        protected ParameterBagInterface $parameterBag
    ) {
        $this->client = $translateClient;
    }

    /**
     * @return SerializerInterface
     */
    public function getSerializer(): SerializerInterface
    {
        return $this->serializer;
    }

    /**
     * @param SerializerInterface $serializer
     *
     * @return void
     */
    public function setSerializer(SerializerInterface $serializer): void
    {
        $this->serializer = $serializer;
    }

    /**
     * @param string  $computedName
     * @param string  $value
     * @param string  $locale
     * @param Catalog $catalog
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function translate(
        string $computedName,
        string $value,
        string $locale,
        Catalog $catalog
    ): void {

        $endpoint = 'api/translations';
        $options = [
            'headers' => $this->getHeaders(),
            'body' => json_encode([
                'locale' => $locale,
                'catalog' => $this->serializer->serialize($catalog, "json", []),
                'environment' => $this->parameterBag->get("squareview.translations.environment"),
                'project' => $this->parameterBag->get("squareview.translations.project"),
                'key' => $computedName,
                'message' => $value
            ], JSON_THROW_ON_ERROR)
        ];
        try {
            $response = $this->client->request("POST", $endpoint, $options);
        } catch (Exception $e) {
            //var_dump($response->getContent());die;
            $this->checkResponseSuccess($response);
        }
    }

    /**
     * @param array $translations
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function multipleTranslate(array $translations): void
    {

        $endpoint = 'api/translations/multiples';
        $options = [
            'headers' => $this->getHeaders(),
            'body' => json_encode([
                'environment' => $this->parameterBag->get("squareview.translations.environment"),
                'project' => $this->parameterBag->get("squareview.translations.project"),
                'translations' => $translations
            ], JSON_THROW_ON_ERROR)
        ];
        try {
            $this->client->request("POST", $endpoint, $options);
        } catch (Exception $e) {
            var_dump($e->getMessage());
        }
    }

    /**
     * @param string $locale
     * @param string $domain
     * @param string $zone
     * @param string $type
     * @param string $discriminator
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function getTranslations(string $locale, string $domain, string $zone, string $type, string $discriminator): array
    {

        $catalogSlug = CatalogSlugGenerator::generate($discriminator, $domain, $zone);
        $endpoint = 'api/catalogs/'.$catalogSlug.'/translations';
        $options = [
            'headers' => $this->getHeaders(),
            'body' => json_encode([
                'locale' => $locale,
                'environment' => $this->parameterBag->get("squareview.translations.environment"),
                'project' => $this->parameterBag->get("squareview.translations.project"),
            ], JSON_THROW_ON_ERROR)
        ];
        try {
            $response = $this->client->request(self::METHOD_GET, $endpoint, $options);
            return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->checkResponseSuccess($response);
        }
        return [];
    }

    /**
     * @param string|null $locale
     * @param string|null $domain
     * @param string|null $zone
     * @param string|null $type
     * @param string|null $discriminator
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function searchTranslations(
        string $locale = null,
        ?string $domain = null,
        ?string $zone = null,
        ?string $type = null,
        ?string $discriminator = null
    ): array {

        $endpoint = 'api/translations/search';
        $options = [
            'headers' => $this->getHeaders(),
            'query' => [
                'locale' => $locale,
                'environment' => $this->parameterBag->get("squareview.translations.environment"),
                'project' => $this->parameterBag->get("squareview.translations.project"),
                'domain' => $domain,
                'zone' => $zone,
                'type' => $type,
                'discriminator' => $discriminator
            ]
        ];
        try {
            $response = $this->client->request(self::METHOD_GET, $endpoint, $options);
            return json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
        } catch (Exception $e) {
            $this->checkResponseSuccess($response);
        }
        return [];
    }

    /**
     * @return string[]
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    #[ArrayShape(['Accept' => "string", 'Content-Type' => "string", 'Authorization' => "string"])]
    private function getHeaders(): array
    {
        if (null === $this->apiBearerToken) {
            $this->apiBearerToken = $this->getApiAuthorization();
        }
        return [
            'Accept' => 'application/json',
            'Content-Type' => 'application/json',
            'Authorization' => 'Bearer '.$this->apiBearerToken
        ];
    }

    /**
     * @return string
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function getApiAuthorization(): string
    {
        try {
            $response = $this->client->request(self::METHOD_POST, "api/authentication_token", [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json'
                ],
                'body' => json_encode([
                    'username' => $this->parameterBag->get("squareview.translations.api_user"),
                    "password" => $this->parameterBag->get("squareview.translations.api_password")
                ], JSON_THROW_ON_ERROR)
            ]);
            $result = json_decode($response->getContent(), true, 512, JSON_THROW_ON_ERROR);
            return $result['token'];
        } catch (Exception $e) {
            // In case of the SquareTranslate version is not secured yet
            return 'unsecure';
        }
    }

    /**
     * @param Translatable $resource
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function deleteResourceTranslations(Translatable $resource): void
    {

        try {
            $options = [
                'headers' => $this->getHeaders()
            ];
            $response = $this->client->request("DELETE", "api/catalog/".
                $this->parameterBag->get("squareview.translations.project").'/'.
                $this->parameterBag->get("squareview.translations.environment").'/'.
                $resource->getSlug()."/".
                $resource->getDiscriminator(), $options);
        } catch (Exception $e) {
            var_dump($e);
            die;
        }
    }

    /**
     * @param ResponseInterface $response
     *
     * @return void
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    protected function checkResponseSuccess(ResponseInterface $response): void
    {
        switch ($response->getStatusCode()) {
            case 200:
            case 201:
                // Do nothing
                break;
            case 404:
                $info = $response->getInfo();
                throw new ResourceNotFoundException("Translation resource not found for ".$info['url']);
            case 400:
            case 405:
            default:
                throw new TranslationClientException('An unknown error occurred on the SquareTranslate API for the Translation endpoint, status code is "'
                    . $response->getStatusCode() . '"');
        }
    }
}
