<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Translation;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use JsonException;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\Doctrine\Annotation\EntityTranslationPropertiesExtractor;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Entity\Translation;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Serializer\Util\SerializationGroupHelper;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\Generator\TranslationKeyGenerator;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Common\Cache\CacheManager;
use SG\CmsBundle\Common\Contracts\ResourceGuideInterface;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class TranslationManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationManager
{
    /**
     * TranslationManager constructor.
     *
     * @param EntityTranslationPropertiesExtractor $propertiesExtractor
     * @param SerializationGroupHelper             $serializationGroupHelper
     * @param CacheManager                         $cacheManager
     * @param RequestStack                         $requestStack
     * @param EntityManagerInterface               $entityManager
     */
    public function __construct(
        private readonly EntityTranslationPropertiesExtractor $propertiesExtractor,
        private readonly SerializationGroupHelper             $serializationGroupHelper,
        private readonly CacheManager                         $cacheManager,
        private readonly RequestStack                         $requestStack,
        private readonly EntityManagerInterface               $entityManager,
    ) {
    }

    /**
     * Save a single translation on translate server
     *
     * @param Translatable $translatable
     * @param string       $key
     * @param string       $value
     * @param string       $locale
     * @param string       $zone
     * @param string       $type
     *
     * @return void
     */
    public function translate(
        Translatable $translatable,
        string $key,
        string $value,
        string $locale,
        string $zone,
        string $type
    ): void {
        $this->saveTranslation(
            $key,
            $value,
            $locale,
            $zone,
            $type,
            $translatable instanceof Guidable ? $translatable->getGuide() : null,
            $translatable->getTranslatableIdentifier(),
            $translatable->getTranslatableClass()
        );
    }

    /**
     * @param string      $key
     * @param string      $value
     * @param string      $locale
     * @param string      $zone
     * @param string      $type
     * @param string|null $guide
     * @param string|null $resourceIdentifier
     * @param string|null $resourceClass
     * @param string|null $label
     *
     * @return void
     */
    public function saveTranslation(
        string $key,
        string $value,
        string $locale,
        string $zone,
        string $type,
        ?string $guide = null,
        ?int $resourceIdentifier = null,
        ?string $resourceClass = null,
        ?string $label = null
    ): void {
        $translationKeyEntity = $this->entityManager
            ->getRepository(TranslationKey::class)
            ->findExisting($key, $zone, $resourceIdentifier, $resourceClass);
        if (null === $translationKeyEntity) {
            $translationKeyEntity = (new TranslationKey())
                ->setName($key)
                ->setLabel($label)
                ->setZone($zone)
                ->setType($type)
                ->setResourceClass($resourceClass)
                ->setResourceIdentifier($resourceIdentifier)
                ->setGuide($guide);
            $this->entityManager->persist($translationKeyEntity);
        }

        $translationEntity = $this->entityManager
            ->getRepository(Translation::class)
            ->findOneByKeyAndLocale($translationKeyEntity, $locale);

        if (null === $translationEntity) {
            $translationEntity = (new Translation())
                ->setLanguage($this->entityManager->getRepository(Language::class)->findOneBy(["id" => $locale]))
                ->setTranslationKey($translationKeyEntity);
        }
        $translationEntity->setMessage($value);
        $this->entityManager->persist($translationEntity);
        $this->entityManager->flush();
    }

    /**
     * Save an array of PendingTranslation objects on database.
     * This is mainly used by fixtures and data migrations.
     *
     * @param array $translations
     *
     * @return void
     * @throws NonUniqueResultException
     */
    public function multipleTranslate(
        array $translations
    ): void {
        $translationsKeys = [];
        /** @var PendingTranslation $translation */
        foreach ($translations as $translation) {
            $translationKey = $translation->getKey().'-'.$translation->getGuide().'-'.$translation->getResourceClass().'-'.$translation->getResourceIdentifier()."-".$translation->getZone();
            if (array_key_exists($translationKey, $translationsKeys)) {
                $translationKeyEntity = $translationsKeys[$translationKey];
            } else {
                $qb = $this->entityManager
                    ->getRepository(TranslationKey::class)
                    ->createQueryBuilder("tk")
                    ->where("tk.name = :name")
                    ->andWhere("tk.zone = :zone")
                    ->setParameter("name",  $translation->getKey())
                    ->setParameter("zone",  $translation->getZone());
                if (null === $translation->getGuide()) {
                    $qb->andWhere("tk.guide is NULL");
                } else {
                    $qb->andWhere("tk.guide = :guide")
                        ->setParameter("guide",  $translation->getGuide());
                }
                if (null === $translation->getResourceClass()) {
                    $qb->andWhere("tk.resourceClass is NULL");
                } else {
                    $qb->andWhere("tk.resourceClass = :resourceClass")
                    ->setParameter("resourceClass",  $translation->getResourceClass());
                }
                if (null === $translation->getResourceIdentifier()) {
                    $qb->andWhere("tk.resourceIdentifier is NULL");
                } else {
                    $qb->andWhere("tk.resourceIdentifier = :resourceIdentifier")
                        ->setParameter("resourceIdentifier",  $translation->getResourceIdentifier());
                }
                $translationKeyEntity = $qb->getQuery()->getOneOrNullResult();

                if (null === $translationKeyEntity) {
                    $translationKeyEntity = (new TranslationKey())
                        ->setName($translation->getKey())
                        ->setLabel($translation->getLabel())
                        ->setZone($translation->getZone())
                        ->setGuide($translation->getGuide())
                        ->setType($translation->getType())
                        ->setResourceClass($translation->getResourceClass())
                        ->setResourceIdentifier($translation->getResourceIdentifier());
                    $this->entityManager->persist($translationKeyEntity);
                }
                $translationsKeys[$translationKey] = $translationKeyEntity;
            }

            $translationEntity = $this->entityManager
                ->getRepository(Translation::class)
                ->findOneByKeyAndLocale($translationKeyEntity, $translation->getLocale());
            if (null === $translationEntity) {
                $translationEntity = (new Translation())
                    ->setLanguage($this->entityManager->getRepository(Language::class)->findOneBy(["id" => $translation->getLocale()]))
                    ->setTranslationKey($translationKeyEntity);
            }
            $translationEntity->setMessage($translation->getMessage());
            $this->entityManager->persist($translationEntity);
        }
        $this->entityManager->flush();
    }


    public function getTranslations(string $locale, string $resourceIdentifier, string $resourceClass, ?string $guide = null, ?string $zone = null, ?string $type = null): array
    {
        $queryBuilder = $this->entityManager
            ->getRepository(Translation::class)
            ->createQueryBuilder("t")
            ->innerJoin("t.translationKey", "tk")
            ->innerJoin("t.language", "l")
            ->select(["t.message AS message", "tk.name AS key"])
            ->where("l.id = :locale")
            ->andWhere("tk.resourceIdentifier = :resourceIdentifier")
            ->andWhere("tk.resourceClass = :resourceClass")
            ->setParameter("resourceIdentifier", $resourceIdentifier)
            ->setParameter("resourceClass", $resourceClass)
            ->setParameter("locale", $locale)
            ;

        if (null !== $guide) {
            $queryBuilder
                ->andWhere("tk.guide = :guide")
                ->setParameter("guide", $guide);
        }

        if (null !== $zone) {
            $queryBuilder
                ->andWhere("tk.zone = :zone")
                ->setParameter("zone", $zone);
        }

        if (null !== $type) {
            $queryBuilder
                ->andWhere("tk.type = :type")
                ->setParameter("type", $type);
        }

        return $queryBuilder->getQuery()->getScalarResult();
    }

    /**
     * Load all properties translations into a Translatable entity for the given locale.
     * If it's a collection call, use cached translations
     *
     * @param Translatable $translatable
     * @param string       $locale
     * @param array        $serializationGroups
     *
     * @return void
     * @throws TranslationClientException|JsonException
     */
    public function loadTranslatablePropertiesTranslations(
        Translatable $translatable,
        string $locale,
        array $serializationGroups
    ): void {
        $translationProperties = $this->propertiesExtractor->extractTranslationProperties(get_class($translatable));

        if ($translatable instanceof Version ) {
            $translations = $this->entityManager
                ->getRepository(Translation::class)
                ->findByTranslatable($translatable->getVersionable()->getDraftVersion(), $locale);
        } else {
            $translations = $this->entityManager
                ->getRepository(Translation::class)
                ->findByTranslatable($translatable, $locale);
        }

        foreach ($translationProperties as $property => $options) {
            if ($this->serializationGroupHelper->propertyInGroups($property, $serializationGroups, $translatable)) {
                $translationKey = TranslationKeyGenerator::generateKey($translatable, $property);

                foreach ($translations as $translation) {
                    if (null !== $translation->getTranslationKey() && $translation->getTranslationKey()->getName() === $translationKey) {
                        $translatable->{'set' . ucfirst(strtolower($property))}($translation->getMessage());
                    }
                }
            }
        }
    }

    /**
     * @param Guidable $resource
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     * @throws JsonException
     */
    public function getResourceGuideTranslations(Guidable $resource): array
    {
        $queryBuilder = $this->entityManager
            ->getRepository(Translation::class)
            ->createQueryBuilder("t")
            ->innerJoin("t.translationKey", "tk")
            ->innerJoin("t.language", "l")
            ->select(["t.message AS message", "tk.name AS key", "l.id as locale", "tk.zone as zone", "tk.label as label"])
            ->where("tk.type = :type")
            ->andWhere("tk.resourceIdentifier = :resourceIdentifier")
            ->andWhere("tk.resourceClass = :resourceClass")
            ->andWhere("tk.guide = :guide")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->setParameter("type", TranslationKey::TYPE_RESOURCE_GUIDE)
            ->setParameter("guide", $resource->getGuide())
        ;

        return $queryBuilder->getQuery()->getScalarResult();
    }

    /**
     * @param ResourceGuideInterface $guide
     *
     * @return array
     */
    public function getLayoutGuideTranslations(ResourceGuideInterface $guide): array
    {
        $queryBuilder = $this->entityManager
            ->getRepository(Translation::class)
            ->createQueryBuilder("t")
            ->innerJoin("t.translationKey", "tk")
            ->innerJoin("t.language", "l")
            ->select(["t.message AS message", "tk.name AS key", "l.id as locale", "tk.zone as zone", "tk.label as label"])
            ->where("tk.type = :type")
            ->andWhere("tk.guide = :guide")
            ->setParameter("type", TranslationKey::TYPE_LAYOUT_GUIDE)
            ->setParameter("guide", $guide->getName())
        ;

        return $queryBuilder->getQuery()->getScalarResult();
    }

    /**
     * @param Translatable $resource
     *
     * @return void
     */
    public function deleteResourceTranslations(Translatable $resource): void
    {
        $this->entityManager
            ->getRepository(TranslationKey::class)
            ->createQueryBuilder("tk")
            ->delete()
            ->where("tk.resourceClass = :resourceClass")
            ->andWhere("tk.resourceIdentifier = :resourceIdentifier")
            ->setParameter("resourceClass", $resource->getTranslatableClass())
            ->setParameter("resourceIdentifier", $resource->getTranslatableIdentifier())
            ->getQuery()->execute()
        ;
    }

    /**
     * @param Translatable $translatable
     * @param array        $normalizedData
     * @param string       $locale
     *
     * @return array
     * @throws TranslationClientException
     */
    public function updateTranslatablePropertiesTranslations(
        Translatable $translatable,
        array $normalizedData,
        string $locale
    ): array {
        try {
            foreach ($normalizedData as $property => $value) {
                if ($this->propertiesExtractor->isTranslation($property, get_class($translatable))) {
                    $computedName = TranslationKeyGenerator::generateKey($translatable, $property);
                    $translatable->{'set' . ucfirst(strtolower($property))}($computedName);
                    $this->translate($translatable, $computedName, $value, $locale, "properties", TranslationKey::TYPE_RESOURCE);
                }
            }
            if ($translatable instanceof Cacheable && !$translatable instanceof Versionable) {
                $this->cacheManager->invalidateCacheForResource($translatable);
            }
        } catch (Exception $e) {
            $this->log($e->getMessage(), $e);
        } catch (ServerExceptionInterface | TransportExceptionInterface | ClientExceptionInterface
        | RedirectionExceptionInterface $e) {
            $this->log($e->getMessage(), $e);
        }

        return $normalizedData;
    }

    /**
     * @param array       $guideTranslations
     * @param string|null $locale
     * @param string|null $zone
     * @param bool        $withLabels
     *
     * @return array
     */
    public function formatTranslationsAsArray(array $guideTranslations, string $locale = null, string $zone = null, bool $withLabels = false): array
    {
        $translations = [];
        foreach ($guideTranslations as $translation) {
            if ($locale && $translation['locale'] !== $locale) {
                continue;
            }
            if ($zone && $translation['zone'] !== $zone) {
                continue;
            }
            if ($locale && $zone) {
                $translations[$translation['key']] = $this->formatTranslationMessage($translation, $withLabels);
            } elseif ($locale && !$zone) {
                if (!array_key_exists($translation['zone'], $translations)) {
                    $translations[$translation['zone']] = [];
                }
                $translations[$translation['zone']][$translation['key']] = $this->formatTranslationMessage($translation, $withLabels);
            } elseif (!$locale && $zone) {
                if (!array_key_exists($translation['locale'], $translations)) {
                    $translations[$translation['locale']] = [];
                }
                $translations[$translation['locale']][$translation['key']] = $this->formatTranslationMessage($translation, $withLabels);
            } else {
                if (!array_key_exists($translation['locale'], $translations)) {
                    $translations[$translation['locale']] = [];
                }
                if (!array_key_exists($translation['zone'], $translations)) {
                    $translations[$translation['locale']][$translation['zone']] = [];
                }
                $translations[$translation['locale']][$translation['zone']][$translation['key']] = $this->formatTranslationMessage($translation, $withLabels);
            }
        }
        return $translations;
    }

    /**
     * @param array $translation
     * @param bool  $withLabels
     *
     * @return array|mixed
     */
    private function formatTranslationMessage(array $translation, bool $withLabels = false): mixed
    {
        if ($withLabels) {
            return [
                'label' => array_key_exists('label', $translation) && $translation['label'] ? $translation['label'] : $translation['key'],
                'message' => $translation['message']
            ];
        }

        return $translation['message'];
    }

    /**
     * @param string         $message
     * @param Exception|null $e
     *
     * @return void
     * @throws TranslationClientException
     */
    private function log(string $message, Exception $e = null): void
    {
        if (null === $this->requestStack->getCurrentRequest()) {
            $output = new ConsoleOutput();
            $output->writeln("<comment>     Serializer warning: ".$message."</comment>");
        } else { // If there is no request, script is executed in console command context.
            if (null !== $e) {
                throw new TranslationClientException($e->getFile().":".$e->getLine()." - ".$message, 0, $e);
            }
            throw new TranslationClientException($message, 0, $e);
        }
    }
}
