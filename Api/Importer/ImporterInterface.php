<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square GLasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Interface ImporterInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ImporterInterface
{
    public function import():void;

    public function getIo(): SymfonyStyle;

    /**
     * @return OutputInterface|null
     */
    public function getOutput(): ?OutputInterface;

    /**
     * @param OutputInterface|null $output
     *
     * @return ImporterInterface
     */
    public function setOutput(?OutputInterface $output): ImporterInterface;

    /**
     * @return InputInterface|null
     */
    public function getInput(): ?InputInterface;

    /**
     * @param InputInterface|null $input
     *
     * @return ImporterInterface
     */
    public function setInput(?InputInterface $input): ImporterInterface;
}
