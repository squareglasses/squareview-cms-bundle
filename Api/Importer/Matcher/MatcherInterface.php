<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square GLasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Matcher;

/**
 * Interface MatcherInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface MatcherInterface
{
    public static function matchIdentifier(string $oldIdentifier): mixed;
}
