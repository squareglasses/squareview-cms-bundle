<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Extractor;

use SG\CmsBundle\Api\Importer\ImporterInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class Extractor
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class Extractor implements ExtractorInterface
{
    protected ?ImporterInterface $importer;
    protected ?string $message = null;

    public function __construct(protected readonly ManagerRegistry $doctrine)
    {

    }

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface
    {
        return $this->importer;
    }

    /**
     * @param ImporterInterface|null $importer
     * @return ExtractorInterface
     */
    public function setImporter(?ImporterInterface $importer): ExtractorInterface
    {
        $this->importer = $importer;
        return $this;
    }
}
