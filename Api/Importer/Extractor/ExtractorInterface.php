<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square GLasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Extractor;

use SG\CmsBundle\Api\Importer\ImporterInterface;

/**
 * Interface ExtractorInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface ExtractorInterface
{
    public function extract():array;

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface;

    /**
     * @param ImporterInterface|null $importer
     *
     * @return ExtractorInterface
     */
    public function setImporter(?ImporterInterface $importer): ExtractorInterface;
}
