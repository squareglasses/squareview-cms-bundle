<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Extractor;

use SG\CmsBundle\Api\Importer\ImporterInterface;
use Doctrine\Persistence\ManagerRegistry;

/**
 * Class Extractor
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
abstract class Sv4Extractor extends Extractor
{
    protected function extractMedias(array $entity, string $className): array
    {
        $className = str_replace("\\", "\\\\\\", $className);
        $entityManager = $this->doctrine->getManager('import')->getConnection();

        $sql = "SELECT * from resource_has_media WHERE resource_class='".$className."' AND resource_identifier='".$entity['id']."';";
        $stmt = $entityManager->prepare($sql);
        $resourceHasMedias = $stmt->executeQuery()->fetchAllAssociative();

        $medias = [];
        foreach ($resourceHasMedias as $rhm) {
            $sql = "SELECT * from media WHERE id='".$rhm['media_id']."';";
            $stmt = $entityManager->prepare($sql);
            $media = $stmt->executeQuery()->fetchAssociative();
            $medias[] = array_merge($rhm, $media);
        }

        return $medias;
    }

    protected function extractGallery(array $entity, string $className): array
    {
        $className = str_replace("\\", "\\\\\\", $className);
        $entityManager = $this->doctrine->getManager('import')->getConnection();

        $sql = "SELECT * from gallery WHERE resource_class='".$className."' AND resource_identifier='".$entity['id']."';";
        $stmt = $entityManager->prepare($sql);
        $galleries = $stmt->executeQuery()->fetchAllAssociative();

        $results = [];
        foreach ($galleries as $gallery) {
            $sql = "SELECT * from gallery_medias WHERE gallery_id='".$gallery['id']."';";
            $stmt = $entityManager->prepare($sql);
            $galleryMedias = $stmt->executeQuery()->fetchAllAssociative();

            $extractedGms = [];
            foreach ($galleryMedias as $gm) {
                $sql = "SELECT * from media WHERE id='".$gm['media_id']."';";
                $stmt = $entityManager->prepare($sql);
                $media = $stmt->executeQuery()->fetchAssociative();
                $gm['media_provider_name'] = $media["provider_name"];
                $gm['media_provider_reference'] = $media["provider_reference"];
                $extractedGms[] = $gm;
            }
            $gallery["gallery_medias"] = $extractedGms;

            $results[] = $gallery;
        }

        return $results;
    }
}
