<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Extractor;

use SG\CmsBundle\Api\Importer\ImporterInterface;

/**
 * Class Sv3ContentExtractor
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class Sv3ContentExtractor extends Extractor
{
    abstract public function getDiscriminator(): string;

    public function extract():array
    {
        $section = $this->importer->getOutput()->section();
        $message = $this->message ?? "* Extracting contents...";
        $section->writeln($message);

        $results = [];
        $entityManager = $this->doctrine->getManager('import')->getConnection();

        $sql = "SELECT * from app_content WHERE discriminator='".$this->getDiscriminator()."';";
        $stmt = $entityManager->prepare($sql);
        $contents = $stmt->executeQuery()->fetchAllAssociative();
        foreach ($contents as $index => $content) {
            $result = null;

            // Getting current version
            $sql = "SELECT * from app_content_version WHERE versionable_id=".$content['id']." AND is_draft = 0;";
            $stmt = $entityManager->prepare($sql);
            $version = $stmt->executeQuery()->fetchAssociative();
            if (false !== $version) {
                $sql = "SELECT * from app_content_version_translations WHERE translatable_id=".$version['id'].";";
                $stmt = $entityManager->prepare($sql);
                $translations = $stmt->executeQuery()->fetchAllAssociative();
                foreach ($translations as $translation) {
                    $version['translations'][$translation['locale']] = $translation;
                }


                // Getting route
                $sql = "SELECT * from app_routes WHERE resource_type='BackendBundle\\\Entity\\\Content' AND resource_id=".$content['id']." AND locale = 'fr';";
                $stmt = $entityManager->prepare($sql);
                $content['routes'] = ["fr" => $stmt->executeQuery()->fetchAssociative()];

                // Getting metatags
                $sql = "SELECT * from app_metatags, app_metatags_translations WHERE resource_type=\"BackendBundle\\\Entity\\\ContentVersion\"
                 AND resource_id=".$version['id']."
                 AND app_metatags.id = translatable_id;";
                $stmt = $entityManager->prepare($sql);
                $version['metatags'] = $stmt->executeQuery()->fetchAllAssociative();

                // Getting modules/spells
                $version['spells'] = [];
                $sql = "SELECT * from app_spells WHERE resource_id=".$version['id'].";";
                $stmt = $entityManager->prepare($sql);
                $spells = $stmt->executeQuery()->fetchAllAssociative();
                foreach ($spells as $spell) {
                    $sql = "SELECT * from app_spell_parameters, app_spell_parameter_translations WHERE spell_id=".$spell['id']." AND app_spell_parameters.id = translatable_id AND locale='fr';";
                    $stmt = $entityManager->prepare($sql);
                    $parameters = $stmt->executeQuery()->fetchAllAssociative();

                    $sql = "SELECT * from app_spell_parameters WHERE spell_id=".$spell['id']." AND recipe_identifier = 'media';";
                    $stmt = $entityManager->prepare($sql);
                    $mediaParameters = $stmt->executeQuery()->fetchAllAssociative();
                    $arraysToMerge = [$parameters, $mediaParameters];
                    $parameters = array_merge(...$arraysToMerge);

                    foreach ($parameters as $key => $parameter) {
                        $sql = "SELECT * from app_media_resources, app_medias WHERE resource_id=".$parameter['id']." AND resource_type LIKE '%SpellParameter' AND app_medias.id=media_id;";
                        $stmt = $entityManager->prepare($sql);
                        $parameters[$key]['medias'] = $stmt->executeQuery()->fetchAllAssociative();
                    }
                    $spell["parameters"] = $parameters;
                    $version['spells'][] = $spell;

//                    if ($spell["recipe_identifier"] === "image_text") {
//                        var_dump($parameters[$key]['medias']);
//                    }
                }

                // Getting medias
                $sql = "SELECT * from app_media_resources, app_medias WHERE resource_id=".$version['id']." AND resource_type LIKE '%ContentVersion' AND app_medias.id=media_id;";
                $stmt = $entityManager->prepare($sql);
                $version['medias'] = $stmt->executeQuery()->fetchAllAssociative();


                $content['version'] = $version;
                $result = $content;
                if (null !== $result) {
                    $results[] = $result;
                }
            }
        }

        $section->overwrite($message." Done!");

        return $results;
    }
}
