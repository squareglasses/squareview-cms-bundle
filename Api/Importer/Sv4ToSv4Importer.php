<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer;

use SG\CmsBundle\Api\Importer\Extractor\ExtractorInterface;
use SG\CmsBundle\Api\Importer\Loader\LoaderInterface;
use SG\CmsBundle\Api\Importer\Loader\Sv4ToSv4Loader;
use SG\CmsBundle\Api\Importer\Transformer\Sv4ToSv4Transformer;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class Importer
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
abstract class Sv4ToSv4Importer
{
    protected ?OutputInterface $output = null;
    protected ?InputInterface $input = null;
    protected ?ExtractorInterface $extractor = null;
    protected ?Sv4ToSv4Transformer $transformer = null;
    protected ?Sv4ToSv4Loader $loader = null;
    protected ?string $message = null;

    public function import(): void
    {
        $io = $this->getIo();

        $io->section($this->message ?? "Importing contents...");
        $results = $this->extractor->setImporter($this)->extract();
        $transformedResults = $this->transformer->setImporter($this)->transform($results);
        $this->loader->setImporter($this)->load($transformedResults);
    }

    public function getIo(): SymfonyStyle
    {
        return new SymfonyStyle($this->input, $this->output);
    }

    /**
     * @return OutputInterface|null
     */
    public function getOutput(): ?OutputInterface
    {
        return $this->output;
    }

    /**
     * @param OutputInterface|null $output
     *
     * @return ImporterInterface
     */
    public function setOutput(?OutputInterface $output): ImporterInterface
    {
        $this->output = $output;
        return $this;
    }

    /**
     * @return InputInterface|null
     */
    public function getInput(): ?InputInterface
    {
        return $this->input;
    }

    /**
     * @param InputInterface|null $input
     *
     * @return ImporterInterface
     */
    public function setInput(?InputInterface $input): ImporterInterface
    {
        $this->input = $input;
        return $this;
    }
}
