<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Loader;

use App\Entity\Content;
use DateTime;
use League\Flysystem\FilesystemException;
use Exception;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Entity\Gallery;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Entity\ModuleParameter;
use SG\CmsBundle\Api\Importer\ImporterInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use JsonException;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\Generator\TranslationKeyGenerator;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
abstract class Sv4ToSv4Loader
{
    protected ?ImporterInterface $importer;
    protected array $availableLocales = [];
    protected array $translations = [];
    protected ?string $message = null;

    public function __construct(
        protected readonly EntityManagerInterface $manager,
        protected readonly PublicationManager     $publicationManager,
        protected readonly TranslationManager     $translationManager,
        protected readonly ModuleManager          $moduleManager,
        protected readonly MetaTagManager         $metatagManager,
        protected readonly MediaManager           $mediaManager,
        protected readonly HasMediaManager $hasMediaManager
    ) {

    }

    /**
     * @param array $data
     * @return array
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws NonUniqueResultException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function load(array $data):array
    {
        $results = [];
        $io = $this->getIo();
        $section = $this->importer->getOutput()->section();
        $message = $this->message ?? "* Loading contents...";
        $section->writeln($message);

        /** @var array $resource */
        foreach($io->progressIterate($data) as $resource) {
            $results[] = $this->loadRow($resource);
        }
        $this->manager->flush();
        $section->overwrite($message." Done !");

        return $results;
    }

    /**
     * @param array $metatags
     * @param object $entity
     * @return void
     */
    protected function loadMetatags(array $metatags, object $entity): void
    {
        if ($entity instanceof MetaTaggable) {
            /** @var MetaTag $metatag */
            foreach ($metatags as $metaTag) {
                $metaTag->setResourceIdentifier($entity->getResourceIdentifier());
                $metaTag->setResourceClass($entity->getResourceClass());
                $this->manager->persist($metaTag);
                $this->manager->flush();
            }
        }
    }

    /**
     * @param array  $routes
     * @param object $entity
     *
     * @return void
     */
    protected function loadRoutes(array $routes, object $entity): void
    {
        if ($entity instanceof Routable) {
            /** @var \SG\CmsBundle\Api\Importer\Model\Route $route */
            foreach ($routes as $route) {
                $this->generateRoute($route->getLocale(), $route->getPath(), $entity, $route->getName(), $route->getOptions());
            }
        }
    }

    /**
     * @param array $modules
     * @param object $entity
     * @param string|null $locale
     * @return void
     * @throws FilesystemException
     */
    protected function loadModules(array $modules, object $entity, ?string $locale = null): void
    {
        if ($entity instanceof Modulable) {
            foreach ($modules as $datum) {
                $module = (new Module())
                    ->setIdentifier($datum["identifier"])
                    ->setGuideIdentifier($datum["guide_identifier"])
                    ->setLocale($locale ?: $datum["locale"])
                    ->setPosition($datum["position"])
                    ->setZone($datum["zone"])
                    ->setCreatedAt(new DateTime($datum["created_at"]))
                    ->setUpdatedAt(new DateTime($datum["updated_at"]))
                    ->setModulableClass($entity->getResourceClass())
                    ->setModulableIdentifier($entity->getResourceIdentifier())
                ;
                $this->persistAndFlush($module);

                foreach ($datum["parameters"] as $parameterData) {
                    $parameter = (new ModuleParameter())
                        ->setIdentifier($parameterData["identifier"])
                        ->setValue($parameterData["value"])
                    ;
                    if ($parameterData["resource_class"] && $parameterData["resource_identifier"] && $parameterData["resource_slug"]) {
                        $resource = $this->manager->getRepository($parameterData["resource_class"])->findOneBy(["slug" => $parameterData["resource_slug"]]);
                        $parameter->setResourceIdentifier($resource->getId());
                        $parameter->setResourceClass($parameterData["resource_class"]);
                    }
                    $module->addParameter($parameter);
                    $parameter->setModule($module);
                    $this->persistAndFlush($module);
                    $this->persistAndFlush($parameter);
                }

                $this->loadMedias($datum["medias"], $module);
            }
        }
    }

    /**
     * @param array $galleries
     * @param object $entity
     * @return void
     * @throws Exception
     */
    protected function loadGalleries(array $galleries, object $entity): void
    {
         if ($entity instanceof HasGalleriesInterface) {
            foreach ($galleries as $datum) {
                $gallery = (new Gallery())
                    ->setName($datum["name"])
                    ->setIdentifier($datum["identifier"])
                    ->setResourceClass($datum["resource_class"])
                    ->setResourceIdentifier($entity->getId())
                    ->setCreatedAt(new DateTime($datum["created_at"]))
                    ->setUpdatedAt(new DateTime($datum["updated_at"]))
                ;
                $this->persistAndFlush($gallery);

                foreach ($datum["gallery_medias"] as $gm) {
                    $media = $this->manager->getRepository(Media::class)->findOneBy([
                        "providerReference" => $gm["media_provider_reference"
                    ]]);

                    if ($media) {
                        $ghm = (new GalleryHasMedia())
                            ->setMedia($media)
                            ->setGallery($gallery)
                            ->setName($gm["name"])
                            ->setIdentifier($gm["identifier"])
                            ->setAlt($gm["alt"])
                            ->setPosition($gm["position"])
                        ;
                        $this->persistAndFlush($ghm);
                    }
                }
            }
         }
    }

    /**
     * @param array $medias
     * @param object $entity
     * @return void
     * @throws FilesystemException
     */
    protected function loadMedias(array $medias, object $entity): void
    {
        if ($entity instanceof HasMediasInterface) {
            foreach ($medias as $mediaConfig) {
                $media = $this->mediaManager->createMedia($mediaConfig["provider_name"], $mediaConfig["provider_reference"]);
                $this->hasMediaManager->addMediaToResource($media, $entity, ["identifier" => $mediaConfig["identifier"], "alt" => $mediaConfig["alt"]]);
            }
        }
    }

    /**
     * @param object $resource
     * @param object $entity
     * @return void
     */
    protected function loadTranslations(object $resource, object $entity): void
    {
        if ($entity instanceof Translatable) {
            foreach ($resource->getTranslations() as $locale => $properties) {
                if (!in_array($locale, $this->availableLocales, true)) {
                    $this->availableLocales[] = $locale;
                }
                foreach ($properties as $property => $value) {
                    if (null !== $value) {
                        $methodName = "set" . ucfirst($property);
                        $computedName = null;
                        if (method_exists($entity, $methodName)) {
                            $computedName = TranslationKeyGenerator::generateKey($entity, $property);
                        } else {
                            echo ("Property " . $property . " not found in class "
                                . get_class($entity) . ".");
                        }
                        if (null !== $computedName) {
                            $this->translations[$computedName] = (new PendingTranslation())
                                ->setKey($computedName)
                                ->setMessage($value)
                                ->setLabel($value)
                                ->setLocale($locale)
                                ->setGuide($resource->getGuide())
                                ->setZone("properties")
                                ->setType(TranslationKey::TYPE_RESOURCE)
                                ->setResourceClass($entity->getResourceClass())
                                ->setResourceIdentifier($entity->getResourceIdentifier());
                            $entity->{"set" . ucfirst($property)}($computedName);
                        }
                    }
                }
            }
            $this->persistAndFlush($entity);
        }
    }

    /**
     * @param object $resource
     * @param object $target
     * @return void
     */
    protected function loadProperties(object $resource, object $target): void
    {
        foreach ($resource->getProperties() as $property => $value) {
            $methodName = "set" . ucfirst($property);
            if (method_exists($target, $methodName)) {
                $target->{"set" . ucfirst($property)}($value);
            }
        }
        $this->persistAndFlush($target);
    }

    /**
     * @param string      $locale
     * @param string      $path
     * @param Content     $entity
     * @param string|null $name
     * @param array|null  $options
     *
     * @return void
     */
    protected function generateRoute(string $locale, string $path, Content $entity, ?string $name, ?array $options = null): void
    {
        $route = new Route();
        $routeName = $name ?? $entity->getSlug();

        if (null !== $options) {
            $route->setOptions($options);
        }
        $route->setPath($path);
        $route->setLocale($locale);
        $route->setName($routeName);
        $route->setResourceIdentifier($entity->getResourceIdentifier());
        $route->setResourceClass($entity->getResourceClass());
        $this->manager->persist($route);
    }

    protected function getItemRoute(): bool
    {
        return false;
    }

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface
    {
        return $this->importer;
    }

    /**
     * @param ImporterInterface|null $importer
     *
     * @return Sv4ToSv4Loader
     */
    public function setImporter(?ImporterInterface $importer): Sv4ToSv4Loader
    {
        $this->importer = $importer;
        return $this;
    }

    /**
     * @return SymfonyStyle
     */
    protected function getIo(): SymfonyStyle
    {
        return $this->importer->getIo();
    }

    /**
     * @param object $entity
     * @return void
     */
    protected function persistAndFlush(object $entity): void
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }
}
