<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Loader;

use App\Entity\Content;
use League\Flysystem\FilesystemException;
use SG\CmsBundle\Api\Importer\ImporterInterface;
use SG\CmsBundle\Api\Importer\Model\CmsResource;
use SG\CmsBundle\Api\Importer\Model\Media;
use SG\CmsBundle\Api\Importer\Model\Metatag;
use SG\CmsBundle\Api\Importer\Model\Module;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use JsonException;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\Generator\TranslationKeyGenerator;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class BlogLoader
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class Loader
{
    protected ?ImporterInterface $importer;
    protected array $availableLocales = [];
    protected array $translations = [];
    protected ?string $message = null;

    public function __construct(
        protected readonly EntityManagerInterface $manager,
        protected readonly PublicationManager     $publicationManager,
        protected readonly TranslationManager     $translationManager,
        protected readonly ModuleManager          $moduleManager,
        protected readonly MetaTagManager         $metatagManager,
        protected readonly MediaManager           $mediaManager,
        protected readonly HasMediaManager $hasMediaManager
    ) {

    }

    /**
     * @param array $data
     * @return array
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws NonUniqueResultException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function load(array $data):array
    {
        $results = [];
        $io = $this->getIo();
        $section = $this->importer->getOutput()->section();
        $message = $this->message ?? "* Loading contents...";
        $section->writeln($message);
        /** @var CmsResource $resource */
        foreach($io->progressIterate($data) as $resource) {
            $results[] = $this->loadRow($resource);
        }
        $this->manager->flush();
        $section->overwrite($message." Done !");

        return $results;
    }

    /**
     * @param array $metatags
     * @param object $entity
     * @return void
     */
    protected function loadMetatags(array $metatags, object $entity): void
    {
        if ($entity instanceof MetaTaggable) {
            /** @var Metatag $metatag */
            foreach ($metatags as $metatag) {
                $this->metatagManager->addMetaTagToResource(
                    $entity,
                    $metatag->getName(),
                    $metatag->getContent(),
                    $metatag->getLocale(),
                    $metatag->getType()
                );
            }
        }
    }

    /**
     * @param array  $routes
     * @param object $entity
     *
     * @return void
     */
    protected function loadRoutes(array $routes, object $entity): void
    {
        if ($entity instanceof Routable) {
            /** @var \SG\CmsBundle\Api\Importer\Model\Route $route */
            foreach ($routes as $route) {
                $this->generateRoute($route->getLocale(), $route->getPath(), $entity, $route->getName(), $route->getOptions());
            }
        }
    }

    /**
     * @param array $modules
     * @param object $entity
     * @return void
     */
    protected function loadModules(array $modules, object $entity): void
    {
        if ($entity instanceof Modulable) {
            /** @var Module $module */
            foreach ($modules as $module) {
                $moduleEntity = $this->moduleManager->addModuleToResource($entity, $module->getGuideIdentifier(), $module->getLocale(), $module->getZone());
                foreach ($module->getParameters() as $identifier => $value) {
                    if ($value["value"] === null) {
                        $value["value"] = "";
                    }
                    $this->moduleManager->addParameter($identifier, $this->updateLinks($value["value"]));
                }

                $this->loadMedias($module->getMedias(), $moduleEntity);
            }
        }
    }

    /**
     * @param array       $mediaConfigs
     * @param object      $entity
     * @param string|null $alt
     *
     * @return void
     * @throws FilesystemException
     */
    protected function loadMedias(array $mediaConfigs, object $entity, ?string $alt = null): void
    {
        if ($entity instanceof HasMediasInterface) {
            /** @var Media $mediaConfig */
            foreach ($mediaConfigs as $mediaConfig) {
                $media = $this->mediaManager->createMedia($mediaConfig->getProviderName(), $mediaConfig->getFilename());
                $this->hasMediaManager->addMediaToResource($media, $entity, ["identifier" => $mediaConfig->getIdentifier(), "alt" => $alt]);
            }
        }
    }

    /**
     * @param object $resource
     * @param object $entity
     * @return void
     */
    protected function loadTranslations(object $resource, object $entity): void
    {
        if ($entity instanceof Translatable) {
            foreach ($resource->getTranslations() as $locale => $properties) {
                if (!in_array($locale, $this->availableLocales, true)) {
                    $this->availableLocales[] = $locale;
                }
                foreach ($properties as $property => $value) {
                    if (null !== $value) {
                        $methodName = "set" . ucfirst($property);
                        $computedName = null;
                        if (method_exists($entity, $methodName)) {
                            $computedName = TranslationKeyGenerator::generateKey($entity, $property);
                        } else {
                            echo ("Property " . $property . " not found in class "
                                . get_class($entity) . ".");
                        }
                        if (null !== $computedName) {
                            $this->translations[$computedName] = (new PendingTranslation())
                                ->setKey($computedName)
                                ->setMessage($this->updateLinks($value))
                                ->setLabel($value)
                                ->setLocale($locale)
                                ->setGuide($resource->getGuide())
                                ->setZone("properties")
                                ->setType(TranslationKey::TYPE_RESOURCE)
                                ->setResourceClass($entity->getResourceClass())
                                ->setResourceIdentifier($entity->getResourceIdentifier());
                            $entity->{"set" . ucfirst($property)}($computedName);
                        }
                    }
                }
            }
            $this->persistAndFlush($entity);
        }
    }

    protected function updateLinks(?string $oldText = null): ?string
    {
        return $oldText;
    }

    /**
     * @param object $resource
     * @param object $target
     * @return void
     */
    protected function loadProperties(object $resource, object $target): void
    {
        foreach ($resource->getProperties() as $property => $value) {
            $methodName = "set" . ucfirst($property);
            if (method_exists($target, $methodName)) {
                $target->{"set" . ucfirst($property)}($value);
            }
        }
        $this->persistAndFlush($target);
    }

    /**
     * @param string      $locale
     * @param string      $path
     * @param Content     $entity
     * @param string|null $name
     * @param array|null  $options
     *
     * @return void
     */
    protected function generateRoute(string $locale, string $path, Content $entity, ?string $name, ?array $options = null): void
    {
        $route = new Route();
        $routeName = $name ?? $entity->getSlug();

        if (null !== $options) {
            $route->setOptions($options);
        }
        $route->setPath($path);
        $route->setLocale($locale);
        $route->setName($routeName);
        $route->setResourceIdentifier($entity->getResourceIdentifier());
        $route->setResourceClass($entity->getResourceClass());
        if (array_key_exists("priority", $options)) {
            $route->setPriority($options["priority"]);
            unset($options["priority"]);
        }
        $this->manager->persist($route);
    }

    protected function getItemRoute(): bool
    {
        return false;
    }

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface
    {
        return $this->importer;
    }

    /**
     * @param ImporterInterface|null $importer
     *
     * @return LoaderInterface
     */
    public function setImporter(?ImporterInterface $importer): LoaderInterface
    {
        $this->importer = $importer;
        return $this;
    }

    /**
     * @return SymfonyStyle
     */
    protected function getIo(): SymfonyStyle
    {
        return $this->importer->getIo();
    }

    /**
     * @param object $entity
     * @return void
     */
    protected function persistAndFlush(object $entity): void
    {
        $this->manager->persist($entity);
        $this->manager->flush();
    }
}
