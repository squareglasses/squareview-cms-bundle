<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Loader;

use App\Entity\Content;
use App\Entity\ContentVersion;
use SG\CmsBundle\Api\Importer\ImporterInterface;
use SG\CmsBundle\Api\Importer\Model\CmsResource;
use SG\CmsBundle\Api\Importer\Model\Media;
use SG\CmsBundle\Api\Importer\Model\Metatag;
use SG\CmsBundle\Api\Importer\Model\Module;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use JsonException;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\Generator\TranslationKeyGenerator;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class Sv4ContentLoader
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Sv4ContentLoader extends Loader implements LoaderInterface
{
    /**
     * @param CmsResource $resource
     *
     * @return object
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws NonUniqueResultException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function loadRow(CmsResource $resource): object
    {
        $draft = null;
        $entity = $this->manager->getRepository(Content::class)->findOneBy(["slug" => $resource->getSlug()]);
        if (null === $entity) {
            $entity = (new Content());
        }
        $entity
            ->setSlug($resource->getSlug())
            ->setDiscriminator($resource->getDiscriminator())
            ->setPosition($resource->getPosition())
            ->setDefaultName($resource->getDefaultName())
            ->setEnabled($resource->isEnabled())
            ;

        // Manage parent
        if (null !== $resource->getParentSlug()) {
            $parent = $this->manager->getRepository(Content::class)->findOneBy(["slug" => $resource->getParentSlug()]);
            if (null !== $parent) {
                $entity->setParent($parent);
            }
        }

        $this->loadProperties($resource, $entity);
        $this->loadTranslations($resource, $entity);
        $this->loadMedias($resource->getMedias(), $entity);
        $this->loadModules($resource->getModules(), $entity);
        $this->loadMetatags($resource->getMetatags(), $entity);
        $this->loadRoutes($resource->getRoutes(), $entity);

        if ($resourceVersion = $resource->getVersion()) {
            $versionClass = $this->publicationManager->getVersionClass($entity);

            try {
                $draft = $entity->getDraftVersion();
            } catch (RuntimeException $e) {
                /** @var ContentVersion $draft */
                $draft = new $versionClass();
            }

            $draft->setCurrentDraft(true);
            $draft->setGuide($resourceVersion->getGuide());
            $draft->setDate($resourceVersion->getDate());
            $entity->addVersion($draft);

            $this->loadProperties($resourceVersion, $draft);
            $this->loadTranslations($resourceVersion, $draft);
            $this->loadMedias($resourceVersion->getMedias(), $draft);
            $this->loadModules($resourceVersion->getModules(), $draft);
            $this->loadMetatags($resourceVersion->getMetatags(), $draft);
        }

        $this->translationManager->multipleTranslate($this->translations);

        // Publish draft version
        if ($entity instanceof Versionable && null !== $draft) {
            foreach ($this->availableLocales as $locale) {
                $draftHashes = $draft->getDraftHashes();
                $versionHash = array_key_exists($locale, $draftHashes) && array_key_exists('hash', $draftHashes[$locale])
                    ? $draftHashes[$locale]['hash']
                    : $locale."-fixture";
                $this->publicationManager->publish($entity, $locale, $versionHash);
            }
        }

        // Manage route
//        if ($this->getItemRoute()) {
//            $this->generateRoute($locale, "", $entity);
//        }

        $this->manager->clear();

        return $entity;
    }
}
