<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square GLasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Loader;

use SG\CmsBundle\Api\Importer\ImporterInterface;
use SG\CmsBundle\Api\Importer\Model\CmsResource;

/**
 * Interface LoaderInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface LoaderInterface
{
    /**
     * @param array $data
     * @return array
     */
    public function load(array $data):array;

    /**
     * @param CmsResource $resource
     * @return object
     */
    public function loadRow(CmsResource $resource): object;

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface;

    /**
     * @param ImporterInterface|null $importer
     *
     * @return LoaderInterface
     */
    public function setImporter(?ImporterInterface $importer): LoaderInterface;
}
