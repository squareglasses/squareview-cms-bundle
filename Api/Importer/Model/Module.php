<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

/**
 * Class Module
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Module
{
    private ?string $guideIdentifier = null;
    private ?string $locale = null;
    private ?string $zone = null;
    private ?string $content = null;
    private ?int $position = null;
    private array $parameters = [];

    /** @var array<Media> $medias */
    protected array $medias = [];

    /**
     * @return string|null
     */
    public function getGuideIdentifier(): ?string
    {
        return $this->guideIdentifier;
    }

    /**
     * @param string|null $guideIdentifier
     * @return Module
     */
    public function setGuideIdentifier(?string $guideIdentifier): Module
    {
        $this->guideIdentifier = $guideIdentifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     * @return Module
     */
    public function setLocale(?string $locale): Module
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getZone(): ?string
    {
        return $this->zone;
    }

    /**
     * @param string|null $zone
     * @return Module
     */
    public function setZone(?string $zone): Module
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     * @return Module
     */
    public function setContent(?string $content): Module
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     * @return Module
     */
    public function setPosition(?int $position): Module
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @param array $parameters
     * @return Module
     */
    public function setParameters(array $parameters): Module
    {
        $this->parameters = $parameters;
        return $this;
    }

    /**
     * @param string $identifier
     * @param array $value
     * @return $this
     */
    public function addParameter(string $identifier, array $value): self
    {
        $this->parameters[$identifier] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getMedias(): array
    {
        return $this->medias;
    }

    /**
     * @param array $medias
     * @return Module
     */
    public function setMedias(array $medias): Module
    {
        $this->medias = $medias;
        return $this;
    }
}
