<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

/**
 * Class Metatag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Metatag
{
    private ?string $name = null;
    private ?string $type = null;
    private ?string $content = null;
    private ?string $locale = null;

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return Metatag
     */
    public function setName(?string $name): Metatag
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return Metatag
     */
    public function setType(?string $type): Metatag
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return Metatag
     */
    public function setContent(?string $content): Metatag
    {
        $this->content = $content;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     *
     * @return Metatag
     */
    public function setLocale(?string $locale): Metatag
    {
        $this->locale = $locale;
        return $this;
    }
}
