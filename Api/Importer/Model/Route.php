<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

/**
 * Class Route
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Route
{
    private ?string $path = null;
    private ?string $name = null;
    private ?string $locale = null;
    private array $options = [];

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string|null $path
     *
     * @return Route
     */
    public function setPath(?string $path): Route
    {
        $this->path = $path;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return Route
     */
    public function setName(?string $name): Route
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string|null $locale
     *
     * @return Route
     */
    public function setLocale(?string $locale): Route
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return array
     */
    public function getOptions(): array
    {
        return $this->options;
    }

    /**
     * @param array $options
     *
     * @return Route
     */
    public function setOptions(array $options): Route
    {
        $this->options = $options;
        return $this;
    }
}
