<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

/**
 * Class Media
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Media
{
    private ?string $identifier = null;
    private ?string $filename = null;
    private ?string $providerName = null;
    private ?string $format = null;

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return Media
     */
    public function setIdentifier(?string $identifier): Media
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFilename(): ?string
    {
        return $this->filename;
    }

    /**
     * @param string|null $filename
     *
     * @return Media
     */
    public function setFilename(?string $filename): Media
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getProviderName(): ?string
    {
        return $this->providerName;
    }

    /**
     * @param string|null $providerName
     *
     * @return Media
     */
    public function setProviderName(?string $providerName): Media
    {
        $this->providerName = $providerName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFormat(): ?string
    {
        return $this->format;
    }

    /**
     * @param string|null $format
     *
     * @return Media
     */
    public function setFormat(?string $format): Media
    {
        $this->format = $format;
        return $this;
    }
}
