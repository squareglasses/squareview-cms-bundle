<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

use DateTimeInterface;

/**
 * Class CmsResource
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsResourceVersion extends BaseCmsResource
{
    protected ?string $guide = null;
    protected ?DateTimeInterface $date = null;
    protected ?string $locale = null;
    protected array $translations = [];


    /**
     * @return string|null
     */
    public function getGuide(): ?string
    {
        return $this->guide;
    }

    /**
     * @param string|null $guide
     *
     * @return CmsResourceVersion
     */
    public function setGuide(?string $guide): CmsResourceVersion
    {
        $this->guide = $guide;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDate(): ?DateTimeInterface
    {
        return $this->date;
    }

    /**
     * @param DateTimeInterface|null $date
     *
     * @return CmsResourceVersion
     */
    public function setDate(?DateTimeInterface $date): CmsResourceVersion
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     *
     * @return CmsResourceVersion
     */
    public function setTranslations(array $translations): CmsResourceVersion
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @param string      $locale
     * @param string      $property
     * @param string|null $value
     *
     * @return $this
     */
    public function addTranslation(string $locale, string $property, ?string $value = null): self
    {
        if (!array_key_exists($locale, $this->translations)) {
            $this->translations[$locale] = [];
        }
        $this->translations[$locale][$property] = $value;
        return $this;
    }
}
