<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

/**
 * Class BaseCmsResource
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class BaseCmsResource
{
    /** @var array<string, mixed> $properties */
    protected array $properties = [];

    /** @var array<Media> $medias */
    protected array $medias = [];

    protected array $modules = [];

    /** @var array<Metatag>  */
    protected array $metatags = [];

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     * @return $this
     */
    public function setProperties(array $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @param string $property
     * @param mixed $value
     * @return $this
     */
    public function addProperty(string $property, mixed $value): self
    {
        $this->properties[$property] = $value;
        return $this;
    }

    /**
     * @return array<Media>
     */
    public function getMedias(): array
    {
        return $this->medias;
    }

    /**
     * @param array<Media> $medias
     * @return $this
     */
    public function setMedias(array $medias): self
    {
        $this->medias = $medias;
        return $this;
    }

    /**
     * @return array<Module>
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * @param array<Module> $modules
     * @return $this
     */
    public function setModules(array $modules): self
    {
        $this->modules = $modules;
        return $this;
    }

    /**
     * @return array<Metatag>
     */
    public function getMetatags(): array
    {
        return $this->metatags;
    }

    /**
     * @param array<Metatag> $metatags
     * @return $this
     */
    public function setMetatags(array $metatags): self
    {
        $this->metatags = $metatags;
        return $this;
    }

    /**
     * @param Metatag $metatag
     * @return $this
     */
    public function addMetatag(Metatag $metatag): self
    {
        $this->metatags[] = $metatag;
        return $this;
    }
}
