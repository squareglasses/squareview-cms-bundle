<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Model;

/**
 * Class CmsResource
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CmsResource extends BaseCmsResource
{
    protected ?string $slug = null;
    protected ?string $discriminator = null;
    protected ?string $defaultName = null;
    protected bool $enabled = false;
    protected ?int $position = null;
    protected array $routes = [];
    protected array $relations = [];
    protected ?string $parentSlug = null;

    protected ?CmsResourceVersion $version = null;

    /**
     * @return CmsResourceVersion|null
     */
    public function getVersion(): ?CmsResourceVersion
    {
        return $this->version;
    }

    /**
     * @param CmsResourceVersion|null $version
     *
     * @return CmsResource
     */
    public function setVersion(?CmsResourceVersion $version): CmsResource
    {
        $this->version = $version;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     *
     * @return CmsResource
     */
    public function setSlug(?string $slug): CmsResource
    {
        $this->slug = $slug;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDiscriminator(): ?string
    {
        return $this->discriminator;
    }

    /**
     * @param string|null $discriminator
     *
     * @return CmsResource
     */
    public function setDiscriminator(?string $discriminator): CmsResource
    {
        $this->discriminator = $discriminator;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultName(): ?string
    {
        return $this->defaultName;
    }

    /**
     * @param string|null $defaultName
     *
     * @return CmsResource
     */
    public function setDefaultName(?string $defaultName): CmsResource
    {
        $this->defaultName = $defaultName;
        return $this;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return CmsResource
     */
    public function setEnabled(bool $enabled): CmsResource
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return CmsResource
     */
    public function setPosition(?int $position): CmsResource
    {
        $this->position = $position;
        return $this;
    }

    /**
     * @return array<Route>
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param array<Route> $routes
     *
     * @return CmsResource
     */
    public function setRoutes(array $routes): CmsResource
    {
        $this->routes = $routes;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getParentSlug(): ?string
    {
        return $this->parentSlug;
    }

    /**
     * @param string|null $parentSlug
     *
     * @return CmsResource
     */
    public function setParentSlug(?string $parentSlug): CmsResource
    {
        $this->parentSlug = $parentSlug;
        return $this;
    }

    /**
     * @param string $type
     * @param array  $values
     *
     * @return $this
     */
    public function addRelation(string $type, array $values): self
    {
        $this->relations[$type] = $values;
        return $this;
    }

    /**
     * @param string $type
     *
     * @return array|null
     */
    public function getRelationsByType(string $type): ?array
    {
        if (!array_key_exists($type, $this->relations)) {
            return null;
        }
        return $this->relations[$type];
    }

    /**
     * @return array
     */
    public function getRelations(): array
    {
        return $this->relations;
    }

    /**
     * @param array $relations
     *
     * @return CmsResource
     */
    public function setRelations(array $relations): CmsResource
    {
        $this->relations = $relations;
        return $this;
    }
}
