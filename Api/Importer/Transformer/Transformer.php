<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Transformer;

use SG\CmsBundle\Api\Importer\ImporterInterface;
use SG\CmsBundle\Api\Importer\Matcher\MediaMatcher;
use SG\CmsBundle\Api\Importer\Matcher\MetatagMatcher;
use SG\CmsBundle\Api\Importer\Matcher\ModuleMatcher;
use SG\CmsBundle\Api\Importer\Model\CmsResource;
use SG\CmsBundle\Api\Importer\Model\CmsResourceVersion;
use SG\CmsBundle\Api\Importer\Model\Media;
use SG\CmsBundle\Api\Importer\Model\Metatag;
use SG\CmsBundle\Api\Importer\Model\Module;
use SG\CmsBundle\Api\Importer\Model\Route;
use DateTime;
use Exception;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class BlogTransformer
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Transformer
{
    protected ?ImporterInterface $importer;
    protected ?string $message = null;

    /**
     * @throws Exception
     */
    public function transform(array $results): array
    {
        $io = $this->getIo();
        $section = $this->importer->getOutput()->section();
        $message = $this->message ?? "* Transforming contents...";
        $section->writeln($message);
        $data = [];

        foreach ($io->progressIterate($results) as $result) {
            $data[] = $this->transformRow($result);
        }
        $section->overwrite($message." Done !");
        return $data;
    }

    /**
     * @param array $data
     * @return array<Metatag>
     */
    protected function transformMetatags(array $data): array
    {
        $metatags = [];
        foreach ($data as $datum) {
            $metatags[] = (new Metatag())
                ->setType($this->getMetatagMatcherClass()::matchIdentifier($datum["kind"]))
                ->setName($datum["kind"])
                ->setContent($datum["value"])
                ->setLocale($datum["locale"]);
        }
        return $metatags;
    }

    /**
     * @param array $data
     * @return array<Media>
     */
    protected function transformMedias(array $data): array
    {
        $medias = [];
        foreach ($data as $datum) {
            $medias[] = (new Media())
            ->setFilename($datum["provider_reference"])
            ->setFormat($datum["format"])
            ->setIdentifier($this->getMediaMatcherClass()::matchIdentifier($datum["format"]))
            ->setProviderName(str_contains($datum["provider_name"], "image") ? "image" : "file");
        }
        return $medias;
    }

    /**
     * @param array $data
     * @return array
     */
    protected function transformModules(array $data): array
    {
        $modules = [];
        foreach ($data as $datum) {
            $subModules = [];

            $match = $this->getModuleMatcherClass()::matchIdentifier($datum["recipe_identifier"]);
            $medias = [];
            $module = null;
            foreach ($datum["parameters"] as $param) {
                if (array_key_exists("locale", $param)) {
                    if (!array_key_exists($param["locale"], $subModules)) {
                        $module = (new Module())
                            ->setLocale($param["locale"])
                            ->setGuideIdentifier($match["identifier"])
                            ->setZone("modules")
                            ->setParameters(array_key_exists("additionalParameters", $match) ? $match["additionalParameters"] : []);
                        $subModules[$param["locale"]] = $module;
                    }
                    $subModules[$param["locale"]]->addParameter($this->getModuleMatcherClass()::matchModuleParameterIdentifier($param["recipe_identifier"]), [
                        "value" => $param["value"]
                    ]);
                }
                if (array_key_exists("medias", $param)) {
                    foreach ($param['medias'] as $index => $mediaConfig) {

                        $newIdentifier = $this->getMediaMatcherClass()::matchIdentifier($mediaConfig["format"]);
                        if (is_array($newIdentifier)) {
                            $newIdentifier = $newIdentifier[$index];
                        }
                        $medias[] = (new Media())
                            ->setIdentifier($newIdentifier)
                            ->setFilename($mediaConfig["provider_reference"])
                            ->setProviderName(str_contains($mediaConfig["provider_name"], "image") ? "image" : "file");
                    }
                }
            }
            $module->setMedias($medias);
            $modules[] = $module;
        }

        return $modules;
    }

    /**
     * @param array       $data
     * @param string|null $name
     *
     * @return array
     */
    protected function transformRoutes(array $data, ?string $name = null): array
    {
        $routes = [];
        foreach ($data as $datum) {
            $routes[] = (new Route())
                ->setPath($datum["path"])
                ->setName($name)
                ->setLocale($datum["locale"])
                ->setOptions([]);
        }
        return $routes;
    }

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface
    {
        return $this->importer;
    }

    /**
     * @param ImporterInterface|null $importer
     *
     * @return Transformer
     */
    public function setImporter(?ImporterInterface $importer): Transformer
    {
        $this->importer = $importer;
        return $this;
    }

    protected function getIo(): SymfonyStyle
    {
        return $this->importer->getIo();
    }
}
