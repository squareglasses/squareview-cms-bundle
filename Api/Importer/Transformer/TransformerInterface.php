<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square GLasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle\Api\Importer\Transformer;

use SG\CmsBundle\Api\Importer\ImporterInterface;
use SG\CmsBundle\Api\Importer\Model\CmsResource;
use Exception;

/**
 * Interface TransformerInterface
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface TransformerInterface
{
    public function transform(array $results): array;

    /**
     * @throws Exception
     */
    public function transformRow(array $row): CmsResource;

    /**
     * @return ImporterInterface|null
     */
    public function getImporter(): ?ImporterInterface;

    /**
     * @param ImporterInterface|null $importer
     *
     * @return Transformer
     */
    public function setImporter(?ImporterInterface $importer): Transformer;

    public function getMediaMatcherClass(): string;
    public function getModuleMatcherClass(): string;
    public function getMetatagMatcherClass(): string;
}
