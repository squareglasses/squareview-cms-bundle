<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Util;

use Symfony\Component\HttpFoundation\Request;

/**
 * Class ApiHelper
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ApiHelper
{
    /**
     * @param Request    $request
     * @param array|null $defaults
     *
     * @return array|string[]
     */
    public static function getSerializeGroupsFromRequest(Request $request, array $defaults = null): array
    {
        $groups = $defaults ?? ['get'];

        return $request->get('serialization_groups') ?: $groups;
    }
}
