<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Util;

use JsonException;
use SG\CmsBundle\Api\Translation\Translator;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * Class ConstraintViolationResponse
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
final class ConstraintViolationResponse extends JsonResponse
{
    /**
     * @param ConstraintViolationListInterface $errors
     * @param string|null                      $title
     * @param array                            $headers
     *
     * @throws JsonException
     */
    public function __construct(ConstraintViolationListInterface $errors, string $title = null, array $headers = [], ?Translator $translator = null)
    {
        $content = [];
        $content['title'] = $title ?: "Some request fields are not valid.";
        $violations = [];
        foreach ($errors as $error) {
            $violations[$error->getPropertyPath()] = $translator !== null ? $translator->trans($error->getMessage(), [], 'errors') : $error->getMessage();
        }
        $content['violations'] = $violations;
        parent::__construct(json_encode($content, JSON_THROW_ON_ERROR), Response::HTTP_BAD_REQUEST, $headers, true);
    }
}
