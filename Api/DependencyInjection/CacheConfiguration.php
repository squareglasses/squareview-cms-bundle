<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class CacheConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class CacheConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('cache')
                    ->children()
                        ->arrayNode('resources')
                            ->info('The list of cache tags to invalidate by entity slug')
                            ->useAttributeAsKey('name')
                            ->prototype('scalar')->end()
                        ->end()
                        ->arrayNode('discriminators')
                            ->info('The list of cache tags to invalidate by entity discriminator')
                            ->useAttributeAsKey('name')
                            ->prototype('scalar')->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
