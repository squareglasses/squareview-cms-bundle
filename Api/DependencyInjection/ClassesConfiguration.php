<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class ClassesConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ClassesConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('classes')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('user')->defaultValue("App\\Entity\\User")->cannotBeEmpty()->end()
                        ->scalarNode('content')->defaultValue("App\\Entity\\Content")->cannotBeEmpty()->end()
                        ->scalarNode('content_version')->defaultValue("App\\Entity\\ContentVersion")->cannotBeEmpty()->end()
                        ->scalarNode('language')->defaultValue("SG\\CmsBundle\\Api\\Entity\\Language")->cannotBeEmpty()->end()
                        ->scalarNode('website')->defaultValue("App\\Entity\\Website")->cannotBeEmpty()->end()
                        ->scalarNode('contact')->defaultValue("App\\Entity\\Contact")->cannotBeEmpty()->end()
                    ->end()
                ->end()
                ->arrayNode('repositories')
                    ->addDefaultsIfNotSet()
                    ->children()
                        ->scalarNode('website')->defaultValue("App\\Repository\\WebsiteRepository")->cannotBeEmpty()->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
