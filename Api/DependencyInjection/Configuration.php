<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\NodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;

/**
 * Class Configuration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class Configuration
{
    /**
     * @return ArrayNodeDefinition|NodeDefinition
     */
    public function getConfig(): ArrayNodeDefinition|NodeDefinition
    {
        $treeBuilder = new TreeBuilder('api');
        $rootNode = $treeBuilder->getRootNode()->addDefaultsIfNotSet();

        $classesConfiguration = new ClassesConfiguration();
        $classesConfiguration->appendTo($rootNode);

        $classesConfiguration = new DiscriminatorsConfiguration();
        $classesConfiguration->appendTo($rootNode);

        $cacheConfiguration = new CacheConfiguration();
        $cacheConfiguration->appendTo($rootNode);

        return $rootNode;
    }
}
