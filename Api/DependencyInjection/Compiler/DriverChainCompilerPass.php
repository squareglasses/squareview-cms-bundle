<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DependencyInjection\Compiler;

use Metadata\Driver\DriverInterface;
use SG\CmsBundle\Api\Doctrine\Mapping\Driver\DoctrineAdapter;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Definition;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Class DriverChainCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DriverChainCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
        // Don't compile services if it's not api mode
        if ($container->getParameter("squareview.mode") !== 'api') {
            return;
        }

        $driver = $container->getDefinition('sg_cms.driver_chain');

        foreach ($container->getParameter('doctrine.entity_managers') as $name => $manager) {
            $adapter = new Definition(
                DriverInterface::class,
                array(
                    new Reference(sprintf('doctrine.orm.%s_metadata_driver', $name)),
                )
            );

            $class = DoctrineAdapter::class;
            $method = 'fromMetadataDriver';

            $adapter->setFactory([$class, $method]);

            $driver->addMethodCall('addDriver', array($adapter));
        }
    }
}
