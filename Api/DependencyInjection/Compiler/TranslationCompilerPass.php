<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DependencyInjection\Compiler;

use SG\CmsBundle\Api\Translation\TranslationClient;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;

/**
 * Class TranslationCompilerPass
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class TranslationCompilerPass implements CompilerPassInterface
{
    /**
     * @param ContainerBuilder $container
     *
     * @return void
     */
    public function process(ContainerBuilder $container): void
    {
//        if (!$container->hasDefinition(TranslationClient::class)) {
//            return;
//        }
//        $clientDefinition = $container->getDefinition(TranslationClient::class);
//        if ($container->hasDefinition("serializer")) {
//            $clientDefinition->addMethodCall("setSerializer", [$container->getDefinition("serializer")]);
//        }
    }
}
