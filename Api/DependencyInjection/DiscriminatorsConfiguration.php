<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;

/**
 * Class DiscriminatorsConfiguration
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class DiscriminatorsConfiguration
{
    /**
     * @param ArrayNodeDefinition $node
     *
     * @return void
     */
    public function appendTo(ArrayNodeDefinition $node): void
    {
        $node
            ->children()
                ->arrayNode('discriminators')
                    ->info('The list of available discriminators and associated classes in the application.')
                    ->useAttributeAsKey('name')
                    ->prototype('array')
                        ->children()
                            ->scalarNode('class')
                            ->isRequired()
                            ->cannotBeEmpty()
                            ->info('The entity class associated with the discriminator.')
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;
    }
}
