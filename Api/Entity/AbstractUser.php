<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use SG\CmsBundle\Api\Contracts\UserInterface;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\Validator\Constraints as Assert;
use DateTimeInterface;
use OpenApi\Annotations as OA;

/**
 * Class AbstractUser
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * UniqueEntity(fields={"email"}, message="errors.email.unique")
 * @UniqueEntity(fields={"username"}, message="errors.username.unique")
 */
abstract class AbstractUser implements UserInterface, PasswordAuthenticatedUserInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"get", "post"})
     */
    protected UuidInterface|string|null $id;

    /**
     * Column needs to be nullable to allow null value from form validation
     *
     * @ORM\Column(type="string", length=180, unique=true, nullable=true)
     * @Groups({"get", "put", "post"})
     * @Assert\Email(message="errors.email.invalid")
     * @Assert\NotBlank(message="errors.email.notblank")
     */
    protected ?string $email = null;

    /**
     * @ORM\Column(type="string", length=180, unique=true)
     * @Groups({"get", "put", "post"})
     * @Assert\NotBlank(message="errors.username.notblank")
     */
    protected ?string $username = null;

    /**
     * @ORM\Column(type="json")
     * @Groups({"get"})
     * @var string[] $roles
     * @OA\Property(type="array", @OA\Items(type="string"))
     */
    protected array $roles = [];

    /**
     * @var string|null The hashed password
     * @ORM\Column(type="string")
     * @Groups({"security", "change-password", "reset-password"})
     * @Assert\NotBlank(message="errors.password.notblank", groups={"registration", "change-password", "reset-password"})
     */
    protected ?string $password = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get", "post", "activate"})
     */
    protected ?bool $termsAccepted = false;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"get", "security", "put", "post", "reset-password"})
     */
    protected bool $enabled = false;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"get", "security"})
     */
    protected bool $locked = false;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"apiKey"})
     */
    protected ?string $apiKey = null;

    /**
     * @var string|null
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "security", "put", "post", "reset-password"})
     */
    protected ?string $confirmationToken = null;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"security", "put", "reset-password"})
     */
    protected ?DateTimeInterface $passwordRequestedAt = null;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"security", "put", "invite", "get"})
     */
    protected ?DateTimeInterface $registrationNotifiedAt = null;

    /**
     * @var DateTimeInterface|null
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get", "put"})
     */
    protected ?DateTimeInterface $lastLogin = null;

    /**
     * @ORM\ManyToMany(targetEntity="SG\CmsBundle\Api\Entity\Group")
     * @ORM\JoinTable(name="user_groups",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="group_id", referencedColumnName="id")}
     * )
     * @Groups({"get", "put", "post"})
     * @var Collection<Group>|array<string>
     * @OA\Property(type="array", @OA\Items(type=Group::class))
     */
    protected Collection|array $groups;

    public function __construct()
    {
        $this->groups = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getUserIdentifier(): string
    {
        return $this->username;
    }

    /**
     * @return UuidInterface|string|null
     */
    public function getId(): UuidInterface|string|null
    {
        return $this->id;
    }

    /**
     * @param UuidInterface|string|null $id
     *
     * @return void
     */
    public function setId(UuidInterface|string|null $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return $this
     */
    public function setEmail(?string $email = null): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUsername(): ?string
    {
        return $this->username;
    }

    /**
     * @param string $username
     *
     * @return $this
     */
    public function setUsername(string $username): self
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getConfirmationToken(): ?string
    {
        return $this->confirmationToken;
    }

    /**
     * @param string|null $confirmationToken
     *
     * @return $this
     */
    public function setConfirmationToken(?string $confirmationToken): self
    {
        $this->confirmationToken = $confirmationToken;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getPasswordRequestedAt(): ?DateTimeInterface
    {
        return $this->passwordRequestedAt;
    }

    /**
     * @param DateTimeInterface|null $passwordRequestedAt
     *
     * @return $this
     */
    public function setPasswordRequestedAt(?DateTimeInterface $passwordRequestedAt = null): self
    {
        $this->passwordRequestedAt = $passwordRequestedAt;
        return $this;
    }

    /**
     * @return bool
     */
    public function isTermsAccepted(): bool
    {
        return $this->termsAccepted;
    }

    /**
     * @param bool $termsAccepted
     *
     * @return $this
     */
    public function setTermsAccepted(bool $termsAccepted): self
    {
        $this->termsAccepted = $termsAccepted;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiKey(): ?string
    {
        return $this->apiKey;
    }

    /**
     * @param string|null $apiKey
     *
     * @return $this
     */
    public function setApiKey(?string $apiKey): self
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * @return bool
     */
    public function isLocked(): bool
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     *
     * @return $this
     */
    public function setLocked(bool $locked): self
    {
        $this->locked = $locked;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getRoles(): array
    {
        $roles = $this->roles;

        foreach ($this->getGroups() as $group) {
            $roles = array_merge($roles, $group->getRoles());
        }

        // we need to make sure to have at least one role
        $roles[] = 'ROLE_USER';

        return array_values(array_unique($roles));
    }

    /**
     * @param string[] $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @param string $role
     *
     * @return $this
     */
    public function addRole(string $role): self
    {
        $role = strtoupper($role);
        if ($role === static::ROLE_DEFAULT) {
            return $this;
        }

        if (!in_array($role, $this->roles, true)) {
            $this->roles[] = $role;
        }

        return $this;
    }

    /**
     * @param string $role
     *
     * @return bool
     */
    public function hasRole(string $role): bool
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    /**
     * @see UserInterface
     */
    public function getPassword(): string
    {
        return (string) $this->password;
    }

    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword(string $password): self
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @see UserInterface
     */
    public function eraseCredentials(): void
    {
        // If you store any temporary, sensitive data on the user, clear it here
        // $this->plainPassword = null;
    }

    /**
     * @return bool
     */
    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $enabled
     *
     * @return $this
     */
    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getLastLogin(): ?DateTimeInterface
    {
        return $this->lastLogin;
    }

    /**
     * @param DateTimeInterface $lastLogin
     *
     * @return $this
     */
    public function setLastLogin(DateTimeInterface $lastLogin): self
    {
        $this->lastLogin = $lastLogin;
        return $this;
    }

    /**
     * @return array|ArrayCollection|Collection
     */
    public function getGroups(): array|ArrayCollection|Collection
    {
        return $this->groups;
    }

    /**
     * @param array|ArrayCollection|Collection $groups
     *
     * @return $this
     */
    public function setGroups(array|ArrayCollection|Collection $groups): self
    {
        $this->groups = $groups;
        return $this;
    }

    /**
     * @param Group $group
     *
     * @return $this
     */
    public function addGroup(Group $group): self
    {
        if (!$this->groups->contains($group)) {
            $this->groups[] = $group;
        }
        return $this;
    }

    /**
     * @param Group $group
     *
     * @return $this
     */
    public function removeGroup(Group $group): self
    {
        if ($this->groups->contains($group)) {
            $this->groups->removeElement($group);
        }
        return $this;
    }

    /**
     * @param string $groupSlug
     *
     * @return bool
     */
    public function hasGroup(string $groupSlug): bool
    {
        foreach ($this->groups as $group) {
            if ($group->getSlug() === $groupSlug) {
                return true;
            }
        }
        return false;
    }

    public function getRegistrationNotifiedAt(): ?DateTimeInterface
    {
        return $this->registrationNotifiedAt;
    }

    public function setRegistrationNotifiedAt(?DateTimeInterface $registrationNotifiedAt): AbstractUser
    {
        $this->registrationNotifiedAt = $registrationNotifiedAt;
        return $this;
    }
}
