<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;

/**
 * Class ResourceHasMedia
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity()
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="resource_class_idx", columns={"resource_class"}),
 *     @ORM\Index(name="resource_identifier_idx", columns={"resource_identifier"}),
 *     @ORM\Index(name="identifier_idx", columns={"identifier"})
 *   }
 * )
 */
class ResourceHasMedia
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "medias"})
     */
    private null|int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "put", "post", "medias", "publish", "media_collection"})
     */
    protected ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "put", "post", "medias", "publish", "media_collection"})
     */
    protected ?string $alt = null;

    /**
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="resourceHasMedias")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id")
     * @Groups({"get", "medias", "publish", "put", "post"})
     */
    protected Media $media;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"get", "medias", "publish", "media_collection", "post"})
     */
    protected string $identifier;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"get", "medias", "publish", "media_collection"})
     */
    protected string $resourceClass;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"get", "medias", "publish", "media_collection"})
     */
    protected string|int $resourceIdentifier;

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(int|null $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(string $name = null): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Media
     */
    public function getMedia(): Media
    {
        return $this->media;
    }

    /**
     * @param Media $media
     *
     * @return $this
     */
    public function setMedia(Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return $this->resourceClass;
    }

    /**
     * @param string $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): string|int
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int $resourceIdentifier
     *
     * @return $this
     */
    public function setResourceIdentifier(string|int $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string|null $alt
     *
     * @return $this
     */
    public function setAlt(string $alt = null): self
    {
        $this->alt = $alt;

        return $this;
    }
}
