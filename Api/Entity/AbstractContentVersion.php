<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use ReflectionClass;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\GuidableTrait;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Contracts\HasGalleriesTrait;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\HasMediasTrait;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\MetaTaggableTrait;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\ModulableTrait;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Doctrine\Annotation\Translation;
use SG\CmsBundle\Api\Doctrine\Contracts\VersionTrait;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use DateTime;

/**
 * Class AbstractContentVersion
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractContentVersion implements Translatable, Version, Guidable, MetaTaggable, Modulable, HasMediasInterface, HasGalleriesInterface
{
    use VersionTrait;
    use GuidableTrait;
    use TimestampableTrait;
    use HasMediasTrait;
    use MetaTaggableTrait;
    use ModulableTrait;
    use HasGalleriesTrait;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $guide = null;

    /**
     * @Groups({"get", "post", "put", "publish", "breadcrumbs"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Translation()
     */
    protected ?string $name = null;

    /**
     * @Groups({"get", "post", "put", "publish", "breadcrumbs"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Translation()
     */
    protected ?string $title = null;

    /**
     * @Groups({"get", "post", "put", "publish", "breadcrumbs"})
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Translation()
     */
    protected ?string $breadcrumbsLabel = null;

    /**
     * @Groups({"get", "post", "put", "publish"})
     * @ORM\Column(type="text", nullable=true)
     * @Translation()
     */
    protected ?string $description = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     * @Translation()
     */
    protected ?string $summary = null;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?DateTime $date = null;

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    /**
     * @return int
     */
    public function getTranslatableIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getTranslatableClass(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return null !== $this->getVersionable() ? $this->getVersionable()->getDiscriminator() : "";
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return null !== $this->getVersionable() ? $this->getVersionable()->getSlug() : "";
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return void
     */
    public function setName(?string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string|null $title
     *
     * @return void
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return void
     */
    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTimeInterface|null $date
     *
     * @return void
     */
    public function setDate(?DateTimeInterface $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getSummary(): ?string
    {
        return $this->summary;
    }

    /**
     * @param string|null $summary
     *
     * @return void
     */
    public function setSummary(?string $summary): void
    {
        $this->summary = $summary;
    }

    /**
     * @return string|null
     */
    public function getBreadcrumbsLabel(): ?string
    {
        return $this->breadcrumbsLabel;
    }

    /**
     * @param string|null $breadcrumbsLabel
     *
     * @return $this
     */
    public function setBreadcrumbsLabel(?string $breadcrumbsLabel): self
    {
        $this->breadcrumbsLabel = $breadcrumbsLabel;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        if (null === $this->getVersionable()) {
            return "#".$this->getId();
        }
        if ($this->isCurrentDraft()) {
            return "#".$this->getId()." - ".$this->getVersionable()->getDefaultName()." (Brouillon)";
        }
        return "#".$this->getId()." - ".$this->getName()." (".$this->getLocale().")";
    }
}
