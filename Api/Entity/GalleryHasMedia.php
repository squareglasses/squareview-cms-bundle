<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Contracts\GalleryHasMediaInterface;
use SG\CmsBundle\Api\Contracts\GalleryInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;

/**
 * Class GalleryHasMedia
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity
 * @ORM\Table(
 *   name="gallery_medias",
 *   indexes={
 *     @ORM\Index(name="position_idx", columns={"position"})
 *   }
 * )
 */
class GalleryHasMedia implements GalleryHasMediaInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "galleries"})
     */
    private null|int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "galleries", "publish", "post"})
     */
    protected ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"get", "galleries", "publish", "post"})
     */
    protected string $identifier;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "galleries", "publish", "put", "post"})
     */
    protected ?string $alt = null;

    /**
     * @ORM\ManyToOne(targetEntity="Media", inversedBy="galleries")
     * @ORM\JoinColumn(name="media_id", referencedColumnName="id", onDelete="SET NULL")
     * @Groups({"get", "galleries", "publish", "put", "post"})
     */
    protected Media $media;

    /**
     * @var GalleryInterface The associated gallery
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="medias")
     * @ORM\JoinColumn(name="gallery_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected GalleryInterface $gallery;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "galleries", "publish", "put", "post"})
     */
    protected ?int $position = 0;

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @return Media
     */
    public function getMedia(): Media
    {
        return $this->media;
    }

    /**
     * @param Media $media
     *
     * @return $this
     */
    public function setMedia(Media $media): self
    {
        $this->media = $media;

        return $this;
    }

    /**
     * @return GalleryInterface
     */
    public function getGallery(): GalleryInterface
    {
        return $this->gallery;
    }

    /**
     * @param GalleryInterface $gallery
     *
     * @return $this
     */
    public function setGallery(GalleryInterface $gallery): self
    {
        $this->gallery = $gallery;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return $this
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAlt(): ?string
    {
        return $this->alt;
    }

    /**
     * @param string|null $alt
     *
     * @return $this
     */
    public function setAlt(?string $alt): self
    {
        $this->alt = $alt;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;

        return $this;
    }
}
