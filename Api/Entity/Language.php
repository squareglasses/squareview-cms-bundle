<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Contracts\LanguageInterface;

/**
 * Class Language
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\LanguageRepository")
 */
class Language implements LanguageInterface
{
    /**
     * The ISO 639-1 code of the language (locale)
     *
     * @ORM\Id()
     * @ORM\Column(type="string", length=2)
     * @Groups({"get"})
     */
    private string $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"get"})
     */
    private string $name;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get"})
     */
    private ?int $position = null;

    /**
     * @ORM\Column(type="boolean", nullable=false)
     * @Groups({"get"})
     */
    private ?bool $enabled = false;

    /**
     * @return string|null
     */
    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * @param string $locale
     *
     * @return void
     */
    public function setId(string $locale): void
    {
        $this->id = $locale;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return void
     */
    public function setPosition(?int $position = null): void
    {
        $this->position = $position;
    }

    /**
     * @return bool
     */
    public function getEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool|null $enabled
     *
     * @return void
     */
    public function setEnabled(?bool $enabled = null): void
    {
        $this->enabled = $enabled;
    }
}
