<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\ModuleInterface;
use SG\CmsBundle\Api\Contracts\ModuleParameterInterface;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class ModuleParameter
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity()
 * @ORM\Table(name="module_parameter")
 */
class ModuleParameter implements ModuleParameterInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     * @Groups({"get", "modules", "publish"})
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Module", inversedBy="parameters")
     * @ORM\JoinColumn(name="module_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected ModuleInterface $module;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"get", "modules", "post", "publish"})
     */
    private ?string $identifier = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get", "modules", "post", "publish", "put"})
     */
    private ?string $value = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "modules", "post", "publish", "put"})
     */
    protected ?string $resourceClass = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"get", "modules", "post", "publish", "put"})
     */
    protected ?string $resourceIdentifier = null;

    /**
     * @Groups({"modules", "publish"})
     */
    protected ?CmsResourceInterface $resource = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->getIdentifier();
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return "module_parameter";
    }

    /**
     * @return ModuleInterface
     */
    public function getModule(): ModuleInterface
    {
        return $this->module;
    }

    /**
     * @param ModuleInterface $module
     *
     * @return $this
     */
    public function setModule(ModuleInterface $module): self
    {
        $this->module = $module;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setValue(?string $value = null): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return $this
     */
    public function setIdentifier(?string $identifier = null): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string|null $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(?string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceIdentifier(): ?string
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param string|null $resourceIdentifier
     *
     * @return $this
     */
    public function setResourceIdentifier(?string $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;

        return $this;
    }

    /**
     * @return CmsResourceInterface|null
     */
    public function getResource(): ?CmsResourceInterface
    {
        return $this->resource;
    }

    /**
     * @param CmsResourceInterface|null $resource
     */
    public function setResource(?CmsResourceInterface $resource): void
    {
        $this->resource = $resource;
    }
}
