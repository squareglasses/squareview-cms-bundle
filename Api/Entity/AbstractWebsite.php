<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ReflectionClass;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\GuidableTrait;
use SG\CmsBundle\Api\Contracts\HasMediasTrait;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\MetaTaggableTrait;
use SG\CmsBundle\Api\Contracts\Publishable;
use SG\CmsBundle\Api\Contracts\WebsiteInterface;
use SG\CmsBundle\Common\Contracts\SluggableTrait;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Contracts\LanguageEnableableTrait;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class AbstractWebsite
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractWebsite implements WebsiteInterface, CmsResourceInterface, Publishable, Translatable, MetaTaggable, Guidable, Cacheable
{
    use LanguageEnableableTrait;
    use TimestampableTrait;
    use SluggableTrait;
    use GuidableTrait;
    use HasMediasTrait;
    use MetaTaggableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups("get")
     */
    protected int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get"})
     */
    protected string $name;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Gedmo\Slug(fields={"name"}, updatable=false, unique=true)
     * @Groups({"get"})
     */
    protected ?string $slug = null;

    /**
     * @Groups({"get"})
     */
    protected string $discriminator;

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(?int $id = null): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    /**
     * @return int
     */
    public function getTranslatableIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getTranslatableClass(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return "website";
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return void
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }
}
