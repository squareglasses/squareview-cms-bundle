<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Contracts\GalleryHasMediaInterface;
use SG\CmsBundle\Api\Contracts\GalleryInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;

/**
 * Class Gallery
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity
 * @ORM\Table(
 *   name="gallery",
 *   indexes={
 *     @ORM\Index(name="resource_class_idx", columns={"resource_class"}),
 *     @ORM\Index(name="resource_identifier_idx", columns={"resource_identifier"}),
 *     @ORM\Index(name="identifier_idx", columns={"identifier"})
 *   }
 * )
 */
class Gallery implements GalleryInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "galleries", "publish"})
     */
    private ?int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "galleries", "publish", "post"})
     */
    protected ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"get", "galleries", "publish", "post"})
     */
    protected string $identifier;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"get", "galleries", "publish"})
     */
    protected string $resourceClass;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Groups({"get", "galleries", "publish"})
     */
    protected int $resourceIdentifier;

    /**
     * @ORM\OneToMany(targetEntity="GalleryHasMedia", mappedBy="gallery", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"get", "galleries", "publish"})
     */
    protected Collection $medias;

    /**
     * Gallery constructor.
     */
    public function __construct()
    {
        $this->medias = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @param string $identifier
     *
     * @return $this
     */
    public function setIdentifier(string $identifier): self
    {
        $this->identifier = $identifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return $this->resourceClass;
    }

    /**
     * @param string $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;
        return $this;
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int $resourceIdentifier
     *
     * @return $this
     */
    public function setResourceIdentifier(int $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getMedias(): Collection
    {
        return $this->medias;
    }

    /**
     * @param Collection $medias
     *
     * @return $this
     */
    public function setMedias(Collection $medias): self
    {
        $this->medias = $medias;
        return $this;
    }

    /**
     * @param GalleryHasMediaInterface $media
     *
     * @return $this
     */
    public function addMedia(GalleryHasMediaInterface $media): self
    {
        if (!$this->medias->contains($media)) {
            $media->setGallery($this);
            $this->medias[] = $media;
        }

        return $this;
    }

    /**
     * @param GalleryHasMediaInterface $media
     *
     * @return $this
     */
    public function removeMedia(GalleryHasMediaInterface $media): self
    {
        if ($this->medias->contains($media)) {
            $this->medias->removeElement($media);
        }
        return $this;
    }
}
