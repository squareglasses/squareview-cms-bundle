<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ReflectionClass;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Doctrine\Annotation\Translation;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Translatable;
use Doctrine\Common\Collections\ArrayCollection;
use SG\CmsBundle\Api\Contracts\LanguageEnableableTrait;
use SG\CmsBundle\Common\Contracts\SluggableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;

/**
 * Class MenuItem
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\MenuItemRepository")
 * @ORM\Table(
 *   name="menu_items",
 *   indexes={
 *     @ORM\Index(name="position_idx", columns={"position"}),
 *     @ORM\Index(name="level_idx", columns={"level"})
 *   }
 * )
 */
class MenuItem implements Translatable, CmsResourceInterface, Cacheable
{
    use LanguageEnableableTrait;
    use TimestampableTrait;
    use SluggableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups("get")
     */
    protected int|null $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     * @Translation()
     */
    protected ?string $name = null;

    /**
     * @ORM\Column(type="string", length=255, unique=true, nullable=true)
     * @Gedmo\Slug(fields={"name"}, updatable=false, unique=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $slug = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?int $position = null;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $value = null;

    /**
     * @Groups({"targetResource"})
     */
    protected ?Routable $targetResource = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $targetResourceClass = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected int|null $targetResourceId = null;


    /**
     * @ORM\Column(type="string", length=45, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $target = null;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $anchor = null;

    /**
     * @ORM\Column(type="string", length=128, nullable=false)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected string $kind;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $cssClasses = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?int $level = null;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected bool $childrenDisplay = false;

    /**
     * @ORM\Column(type="string", length=155, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $queryString = null;

    /**
     * @ORM\ManyToOne(targetEntity="Menu", inversedBy="items")
     * @ORM\JoinColumn(name="menu_id", referencedColumnName="id")
     * @Groups({"get", "post", "put"})
     */
    protected Menu $menu;

    /**
     * @ORM\ManyToOne(targetEntity="MenuItem", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     * @Groups({"post", "parents"})
     */
    protected ?MenuItem $parent = null;

    /**
     * @ORM\OneToMany(targetEntity="MenuItem", mappedBy="parent", cascade={"remove"})
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"publish", "children"})
     */
    protected ?Collection $children = null;

    /**
     * @Groups({"get", "count"})
     */
    protected int $childrenCount = 0;

    /**
     * @var string|null
     * @Groups({"get", "publish"})
     */
    protected ?string $route = null;

    /**
     * @return string|null
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string|null $route
     *
     * @return void
     */
    public function setRoute(?string $route): void
    {
        $this->route = $route;
    }

    /**
     * MenuItem constructor.
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return 'menu_item';
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    /**
     * @return int
     */
    public function getTranslatableIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getTranslatableClass(): string
    {
        return MenuItem::class;
    }

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string|null $name
     *
     * @return $this
     */
    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return $this
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;
    }

    /**
     * @param string|null $value
     *
     * @return $this
     */
    public function setValue(?string $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTarget(): ?string
    {
        return $this->target;
    }

    /**
     * @param string|null $target
     *
     * @return $this
     */
    public function setTarget(?string $target): self
    {
        $this->target = $target;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAnchor(): ?string
    {
        return $this->anchor;
    }

    /**
     * @param string|null $anchor
     *
     * @return $this
     */
    public function setAnchor(?string $anchor): self
    {
        $this->anchor = $anchor;

        return $this;
    }

    /**
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * @param string $kind
     *
     * @return $this
     */
    public function setKind(string $kind): self
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCssClasses(): ?string
    {
        return $this->cssClasses;
    }

    /**
     * @param string|null $cssClasses
     *
     * @return $this
     */
    public function setCssClasses(?string $cssClasses): self
    {
        $this->cssClasses = $cssClasses;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getLevel(): ?int
    {
        return $this->level;
    }

    /**
     * @param int|null $level
     *
     * @return $this
     */
    public function setLevel(?int $level): self
    {
        $this->level = $level;

        return $this;
    }

    /**
     * @return bool
     */
    public function isChildrenDisplay(): bool
    {
        return $this->childrenDisplay;
    }

    /**
     * @param bool $childrenDisplay
     *
     * @return $this
     */
    public function setChildrenDisplay(bool $childrenDisplay): self
    {
        $this->childrenDisplay = $childrenDisplay;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getQueryString(): ?string
    {
        return $this->queryString;
    }

    /**
     * @param string|null $queryString
     *
     * @return $this
     */
    public function setQueryString(?string $queryString): self
    {
        $this->queryString = $queryString;

        return $this;
    }

    /**
     * @return Menu
     */
    public function getMenu(): Menu
    {
        return $this->menu;
    }

    /**
     * @param Menu $menu
     *
     * @return $this
     */
    public function setMenu(Menu $menu): self
    {
        $this->menu = $menu;

        return $this;
    }

    /**
     * @return MenuItem|null
     */
    public function getParent(): ?MenuItem
    {
        return $this->parent;
    }

    /**
     * @param MenuItem|null $parent
     *
     * @return $this
     */
    public function setParent(?MenuItem $parent = null): self
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * @return Collection|null
     */
    public function getChildren(): ?Collection
    {
        return $this->children;
    }

    /**
     * @param Collection|null $children
     *
     * @return $this
     */
    public function setChildren(?Collection $children): self
    {
        $this->children = $children;

        return $this;
    }

    /**
     * @param MenuItem $item
     *
     * @return $this
     */
    public function addChild(MenuItem $item): self
    {
        if (!$this->children->contains($item)) {
            $item->setParent($this);
            $this->children[] = $item;
        }

        return $this;
    }

    /**
     * @param MenuItem $item
     *
     * @return $this
     */
    public function removeChild(MenuItem $item): self
    {
        if ($this->children->contains($item)) {
            $this->children->removeElement($item);
        }

        return $this;
    }

    /**
     * @return string|null
     */
    public function getTargetResourceClass(): ?string
    {
        return $this->targetResourceClass;
    }

    /**
     * @param string|null $targetResourceClass
     */
    public function setTargetResourceClass(string|null $targetResourceClass): void
    {
        $this->targetResourceClass = $targetResourceClass;
    }

    /**
     * @return int|null
     */
    public function getTargetResourceId(): int|null
    {
        return $this->targetResourceId;
    }

    /**
     * @param int|null $targetResourceId
     *
     * @return void
     */
    public function setTargetResourceId(int|null $targetResourceId): void
    {
        $this->targetResourceId = $targetResourceId;
    }

    /**
     * @return int
     */
    public function getChildrenCount(): int
    {
        return $this->children->count();
    }

    /**
     * @return Routable|null
     */
    public function getTargetResource(): ?Routable
    {
        return $this->targetResource;
    }

    /**
     * @param Routable|null $targetResource
     */
    public function setTargetResource(?Routable $targetResource): void
    {
        $this->targetResource = $targetResource;
    }
}
