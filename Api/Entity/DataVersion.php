<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\DataMigrations\Model\DataVersionInterface;

/**
 * Class DataVersion
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Table(name="data_migration_versions")
 * @ORM\Entity()
 */
class DataVersion implements DataVersionInterface
{
    /**
     * @ORM\Column(name="version", type="string")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="NONE")
     */
    private ?string $version;

    /**
     * @param string|null $version
     */
    public function __construct(string $version = null)
    {
        $this->version = $version;
    }

    /**
     * @return string|null
     */
    public function getVersion(): ?string
    {
        return $this->version;
    }

    /**
     * @param string|null $version
     *
     * @return $this
     */
    public function setVersion(?string $version): self
    {
        $this->version = $version;

        return $this;
    }
}
