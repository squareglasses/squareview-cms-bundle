<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class TranslationKey
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\TranslationKeyRepository")
 * @ORM\Table(
 *   uniqueConstraints={@ORM\UniqueConstraint(columns={"name", "zone", "resource_identifier", "resource_class"})},
 *   indexes={
 *     @ORM\Index(name="name_idx", columns={"name"}),
 *     @ORM\Index(name="type_idx", columns={"type"}),
 *     @ORM\Index(name="zone_idx", columns={"zone"}),
 *     @ORM\Index(name="resource_identifier_idx", columns={"resource_identifier"}),
 *     @ORM\Index(name="resource_class_idx", columns={"resource_class"}),
 *     @ORM\Index(name="guide_idx", columns={"guide"})
 *   }
 * )
 */
class TranslationKey
{
    public const TYPE_RESOURCE = "resource";
    public const TYPE_RESOURCE_GUIDE = "resource_guide";
    public const TYPE_LAYOUT_GUIDE = "layout_guide";

    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get"})
     */
    private ?int $id = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "post", "read"})
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post", "read"})
     */
    private ?string $label = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post", "read"})
     */
    protected ?string $type = null;

    /**
     * @ORM\Column(type="string", length=64)
     * @Groups({"get", "post", "read"})
     */
    protected string $zone;

    /**
     * Identify the associated resource with a unique id.
     * It should be the guid of the resource. Must be unique.
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "post", "read"})
     */
    protected ?int $resourceIdentifier = null;

    /**
     * Identify the associated resource with its class namespace.
     *
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"get", "post", "read"})
     */
    protected ?string $resourceClass = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post", "read"})
     */
    protected ?string $guide = null;

    /**
     * @var Collection<Translation>|null
     * @ORM\OneToMany(targetEntity="Translation", mappedBy="translationKey")
     * @Groups({"get", "post"})
     */
    private ?Collection $translations;

    /**
     * TranslationKey constructor.
     */
    public function __construct()
    {
        $this->translations = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->name;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return TranslationKey
     */
    public function setId(?int $id): TranslationKey
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return TranslationKey
     */
    public function setName(string $name): TranslationKey
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return TranslationKey
     */
    public function setLabel(?string $label): TranslationKey
    {
        $this->label = $label;
        return $this;
    }

    /**
     * @return string
     */
    public function getZone(): string
    {
        return $this->zone;
    }

    /**
     * @param string $zone
     *
     * @return TranslationKey
     */
    public function setZone(string $zone): TranslationKey
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuide(): ?string
    {
        return $this->guide;
    }

    /**
     * @param string|null $guide
     *
     * @return TranslationKey
     */
    public function setGuide(?string $guide): TranslationKey
    {
        $this->guide = $guide;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getResourceIdentifier(): ?int
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int|null $resourceIdentifier
     *
     * @return TranslationKey
     */
    public function setResourceIdentifier(?int $resourceIdentifier): TranslationKey
    {
        $this->resourceIdentifier = $resourceIdentifier;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string|null $resourceClass
     *
     * @return TranslationKey
     */
    public function setResourceClass(?string $resourceClass): TranslationKey
    {
        $this->resourceClass = $resourceClass;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     *
     * @return TranslationKey
     */
    public function setType(?string $type): TranslationKey
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return ArrayCollection|Collection|null
     */
    public function getTranslations(): ArrayCollection|Collection|null
    {
        return $this->translations;
    }

    /**
     * @param ArrayCollection|Collection|null $translations
     *
     * @return TranslationKey
     */
    public function setTranslations(ArrayCollection|Collection|null $translations): TranslationKey
    {
        $this->translations = $translations;
        return $this;
    }

    /**
     * @param Translation $translation
     * @return $this
     */
    public function addTranslation(Translation $translation): self
    {
        if (!$this->translations->contains($translation)) {
            $this->translations[] = $translation;
            $translation->setTranslationKey($this);
        }

        return $this;
    }

    /**
     * @param Translation $translation
     * @return $this
     */
    public function removeTranslation(Translation $translation): self
    {
        if ($this->translations->contains($translation)) {
            $this->translations->removeElement($translation);
            // set the owning side to null (unless already changed)
            if ($translation->getTranslationKey() === $this) {
                $translation->setTranslationKey(null);
            }
        }

        return $this;
    }
}
