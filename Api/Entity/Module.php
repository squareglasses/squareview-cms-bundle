<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use ReflectionClass;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Contracts\HasGalleriesTrait;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\HasMediasTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Contracts\ModuleInterface;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\MaxDepth;

/**
 * Class Module
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\ModuleRepository")
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="modulable_class_idx", columns={"modulable_class"}),
 *     @ORM\Index(name="modulable_identifier_idx", columns={"modulable_identifier"}),
 *     @ORM\Index(name="locale_idx", columns={"locale"}),
 *     @ORM\Index(name="position_idx", columns={"position"}),
 *     @ORM\Index(name="zone_idx", columns={"zone"}),
 *     @ORM\Index(name="identifier_idx", columns={"identifier"})
 *   }
 * )
 */
class Module implements ModuleInterface, HasMediasInterface, HasGalleriesInterface
{
    use TimestampableTrait;
    use HasMediasTrait;
    use HasGalleriesTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "modules", "publish"})
     */
    protected int|null $id;

    /**
     * @var ResourceHasMedia[]
     * @Groups({"get", "medias", "publish", "modules"})
     */
    protected array $medias = [];

    /**
     * @var Gallery[]
     * @Groups({"get", "medias", "galleries", "publish", "modules"})
     */
    protected array $galleries = [];

    /**
     * @ORM\Column(type="string", length=128, nullable=false)
     * @Groups({"get", "modules", "publish"})
     */
    protected ?string $identifier = null;

    /**
     * @ORM\Column(type="string", length=128, nullable=false)
     * @Groups({"get", "modules", "post", "publish"})
     */
    protected string $guideIdentifier;

    /**
     * @ORM\Column(type="integer", nullable=false)
     * @Groups({"get", "modules", "publish"})
     */
    protected int $modulableIdentifier;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"get", "modules", "publish"})
     */
    protected string $modulableClass;

    /**
     * @ORM\Column(type="string", nullable=false)
     * @Groups({"get", "modules", "publish"})
     */
    protected string $locale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "modules", "post", "publish"})
     */
    protected ?int $position = null;

    /**
     * @ORM\Column(type="string", nullable=true)
     * @Groups({"get", "modules", "post", "publish"})
     */
    protected ?string $zone = null;

    /**
     * @var Collection<ModuleParameter> $parameters
     * @ORM\OneToMany(targetEntity="ModuleParameter", mappedBy="module", cascade={"persist", "remove"}, orphanRemoval=true)
     * @Groups({"get", "modules", "post", "publish", "put"})
     * @MaxDepth(1)
     */
    protected Collection $parameters;

    /**
     * @param string|null $fixedIdentifier
     */
    public function __construct(?string $fixedIdentifier = null)
    {
        $this->parameters = new ArrayCollection();

        if ($fixedIdentifier) {
            $this->setIdentifier($fixedIdentifier);
        } else {
            $this->generateIdentifier();
        }
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return $this->getGuideIdentifier();
    }

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(int|null $id = null): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        $reflectionClass = new ReflectionClass($this);
        if (str_contains($reflectionClass->getName(), "Proxies")) {
            return $reflectionClass->getParentClass()->getName();
        }
        return $reflectionClass->getName();
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getModulableIdentifier(): int
    {
        return $this->modulableIdentifier;
    }

    /**
     * @param int $modulableIdentifier
     *
     * @return $this
     */
    public function setModulableIdentifier(int $modulableIdentifier): self
    {
        $this->modulableIdentifier = $modulableIdentifier;

        return $this;
    }

    /**
     * @return string
     */
    public function getModulableClass(): string
    {
        return $this->modulableClass;
    }

    /**
     * @param string $modulableClass
     *
     * @return $this
     */
    public function setModulableClass(string $modulableClass): self
    {
        $this->modulableClass = $modulableClass;

        return $this;
    }

    /**
     * @return string
     */
    public function getGuideIdentifier(): string
    {
        return $this->guideIdentifier;
    }

    /**
     * @param string $guideIdentifier
     *
     * @return $this
     */
    public function setGuideIdentifier(string $guideIdentifier): self
    {
        $this->guideIdentifier = $guideIdentifier;

        return $this;
    }

    /**
     * @return Collection<int,ModuleParameter>
     */
    public function getParameters(): Collection
    {
        return $this->parameters;
    }

    /**
     * @param ModuleParameter $parameter
     *
     * @return $this
     */
    public function addParameter(ModuleParameter $parameter): self
    {
        if (!$this->parameters->contains($parameter)) {
            $parameter->setModule($this);
            $this->parameters[] = $parameter;
        }

        return $this;
    }

    /**
     * @param ModuleParameter $parameter
     *
     * @return $this
     */
    public function removeParameter(ModuleParameter $parameter): self
    {
        if ($this->parameters->contains($parameter)) {
            $this->parameters->removeElement($parameter);
        }

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     *
     * @return $this
     */
    public function setPosition(?int $position = null): self
    {
        $this->position = $position;

        return $this;
    }

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZone(): ?string
    {
        return $this->zone;
    }

    /**
     * @param string|null $zone
     *
     * @return $this
     */
    public function setZone(?string $zone = null): self
    {
        $this->zone = $zone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getIdentifier(): ?string
    {
        return $this->identifier;
    }

    /**
     * @param string|null $identifier
     *
     * @return $this
     */
    public function setIdentifier(?string $identifier = null): self
    {
        $this->identifier = $identifier;

        return $this;
    }

    /**
     * @return string
     */
    public function generateIdentifier(): string
    {
        $this->identifier = uniqid('', true);

        return $this->identifier;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->guideIdentifier;
    }
}
