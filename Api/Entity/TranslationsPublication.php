<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;

/**
 * Class TranslationsPublication
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\TranslationsPublicationRepository")
 */
class TranslationsPublication
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups("get")
     */
    protected int|null $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "post"})
     */
    protected ?string $guideIdentifier = null;

    /**
     * @var array<string> $translations
     * @ORM\Column(type="json")
     * @Groups({"get"})
     */
    protected array $translations = [];

    /**
     * @ORM\Column(type="string", length=4)
     * @Groups({"get", "post"})
     */
    protected string $locale;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    protected ?int $version = null;

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(int|null $id = null): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getGuideIdentifier(): ?string
    {
        return $this->guideIdentifier;
    }

    /**
     * @param string|null $guideIdentifier
     *
     * @return void
     */
    public function setGuideIdentifier(?string $guideIdentifier): void
    {
        $this->guideIdentifier = $guideIdentifier;
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     *
     * @return void
     */
    public function setTranslations(array $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getVersion(): ?int
    {
        return $this->version;
    }

    /**
     * @param int|null $version
     * @return $this
     */
    public function setVersion(?int $version): self
    {
        $this->version = $version;

        return $this;
    }
}
