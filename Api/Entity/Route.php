<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use OpenApi\Annotations as OA;

/**
 * Class Route
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\RouteRepository")
 * @UniqueEntity(
 *     fields={"path", "locale", "resourceIdentifier"},
 * )
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="resource_class_idx", columns={"resource_class"}),
 *     @ORM\Index(name="resource_identifier_idx", columns={"resource_identifier"}),
 *     @ORM\Index(name="locale_idx", columns={"locale"})
 *   }
 * )
 */
class Route
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups("get")
     */
    protected int|null $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "put"})
     */
    private string $path;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get"})
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=8)
     * @Groups({"get"})
     */
    private string $locale;

    /**
     * Identify the associated resource with a unique id.
     * It should be the guid of the resource. Must be unique.
     *
     * @ORM\Column(type="integer")
     * @Groups({"get"})
     */
    protected int $resourceIdentifier;

    /**
     * Identify the associated resource with its class namespace.
     *
     * @ORM\Column(type="string", length=255)
     * @Groups({"get"})
     */
    protected string $resourceClass;

    /**
     * @ORM\Column(type="json", nullable=true)
     * @Groups({"get"})
     * @var string[]|null $options
     * @OA\Property(type="array", @OA\Items(type="string"))
     */
    private ?array $options = [];

    /**
     * Identify the associated resource with a unique id.
     * It should be the guid of the resource. Must be unique.
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "put", "post"})
     */
    protected ?int $priority = null;

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     *
     * @return void
     */
    public function setId(int|null $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getPath(): ?string
    {
        return $this->path;
    }

    /**
     * @param string $path
     *
     * @return $this
     */
    public function setPath(string $path): self
    {
        $this->path = $path;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getResourceIdentifier(): int|null
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int $resourceIdentifier
     *
     * @return $this
     */
    public function setResourceIdentifier(int $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getOptions(): ?array
    {
        return $this->options;
    }

    /**
     * @param string[]|null $options
     *
     * @return $this
     */
    public function setOptions(?array $options): self
    {
        $this->options = $options;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPriority(): ?int
    {
        return $this->priority;
    }

    /**
     * @param int|null $priority
     *
     * @return Route
     */
    public function setPriority(?int $priority): Route
    {
        $this->priority = $priority;
        return $this;
    }
}
