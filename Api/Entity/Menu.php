<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use ReflectionClass;
use Symfony\Component\Serializer\Annotation\Groups;
use Gedmo\Mapping\Annotation as Gedmo;
use SG\CmsBundle\Common\Contracts\SluggableTrait;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;

/**
 * Class Menu
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\MenuRepository")
 * @ORM\Table(name="menus")
 */
class Menu
{
    use TimestampableTrait;
    use SluggableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "menu"})
     */
    protected int $id;

    /**
     * @ORM\OneToMany(targetEntity="MenuItem", mappedBy="menu")
     * @ORM\OrderBy({"position" = "ASC"})
     * @Groups({"post", "put", "publish", "items"})
     */
    protected ?Collection $items = null;

    /**
     * @ORM\Column(name="name", type="string", length=255)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected string $name;

    /**
     * @Gedmo\Slug(fields={"name"}, updatable=false, unique=true)
     * @ORM\Column(length=255, unique=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $slug = null;

    /**
     * @ORM\Column(name="css_classes", type="string", length=255, nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $cssClasses = null;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $description = null;

    /**
     * @var string[]
     */
    protected array $languages = [];

    /**
     * @var int
     * @Groups({"get"})
    */
    protected int $itemsCount = 0;

    /**
     * Menu constructor.
     */
    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getResourceIdentifier(): string
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        return (new ReflectionClass($this))->getName();
    }

    /**
     * @return Collection|null
     */
    public function getItems(): ?Collection
    {
        return $this->items;
    }

    /**
     * @param Collection|null $items
     *
     * @return $this
     */
    public function setItems(?Collection $items): self
    {
        $this->items = $items;

        return $this;
    }

    /**
     * @param MenuItem $item
     *
     * @return void
     */
    public function addItem(MenuItem $item): void
    {
        if (!$this->items->contains($item)) {
            $item->setMenu($this);
            $this->items[] = $item;
        }
    }

    /**
     * @param MenuItem $item
     *
     * @return void
     */
    public function removeItem(MenuItem $item): void
    {
        if ($this->items->contains($item)) {
            $this->items->removeElement($item);
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getSlug(): string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     *
     * @return $this
     */
    public function setSlug(?string $slug = null): self
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCssClasses(): ?string
    {
        return $this->cssClasses;
    }

    /**
     * @param string|null $cssClasses
     *
     * @return $this
     */
    public function setCssClasses(?string $cssClasses): self
    {
        $this->cssClasses = $cssClasses;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param string|null $description
     *
     * @return $this
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     *
     * @return $this
     */
    public function setLanguages(array $languages): self
    {
        $this->languages = $languages;

        return $this;
    }

    /**
     * @return int
     */
    public function getItemsCount(): int
    {
        return $this->items->count();
    }
}
