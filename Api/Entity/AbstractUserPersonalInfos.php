<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Api\Contracts\UserInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Ramsey\Uuid\Doctrine\UuidGenerator;
use Ramsey\Uuid\UuidInterface;

/**
 * Class AbstractUserPersonalInfos
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AbstractUserPersonalInfos
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid", unique=true)
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\CustomIdGenerator(class=UuidGenerator::class)
     * @Groups({"get", "put"})
     */
    protected UuidInterface|string $id;

    protected ?UserInterface $user = null;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Groups({"get", "put", "post"})
     * Assert\NotBlank(message="errors.firstName.notblank")
     */
    protected ?string $firstName = null;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Groups({"get", "put", "post"})
     * Assert\NotBlank(message="errors.lastName.notblank")
     */
    protected ?string $lastName = null;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Groups({"get", "put", "post"})
     */
    protected ?string $phone = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "put", "post"})
     */
    protected ?string $address = null;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     * @Groups({"get", "put", "post"})
     */
    protected ?string $zipcode = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "put", "post"})
     */
    protected ?string $city = null;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     * @Groups({"get", "put", "post"})
     */
    protected ?string $country = null;

    /**
     * @return UuidInterface|string
     */
    public function getId(): UuidInterface|string
    {
        return $this->id;
    }

    /**
     * @param UuidInterface|string $id
     *
     * @return void
     */
    public function setId(UuidInterface|string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return $this
     */
    public function setFirstName(?string $firstName): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return $this
     */
    public function setLastName(?string $lastName): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return UserInterface|null
     */
    public function getUser(): ?UserInterface
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     *
     * @return $this
     */
    public function setUser(UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @param string|null $phone
     *
     * @return $this
     */
    public function setPhone(?string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAddress(): ?string
    {
        return $this->address;
    }

    /**
     * @param string|null $address
     *
     * @return $this
     */
    public function setAddress(?string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getZipcode(): ?string
    {
        return $this->zipcode;
    }

    /**
     * @param string|null $zipcode
     *
     * @return $this
     */
    public function setZipcode(?string $zipcode): self
    {
        $this->zipcode = $zipcode;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCity(): ?string
    {
        return $this->city;
    }

    /**
     * @param string|null $city
     *
     * @return $this
     */
    public function setCity(?string $city): self
    {
        $this->city = $city;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getCountry(): ?string
    {
        return $this->country;
    }

    /**
     * @param string|null $country
     *
     * @return $this
     */
    public function setCountry(?string $country): self
    {
        $this->country = $country;

        return $this;
    }
}
