<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use ReflectionClass;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Doctrine\Annotation\Translation;
use SG\CmsBundle\Api\Contracts\CmsResourceTrait;
use SG\CmsBundle\Api\Contracts\Enableable;
use SG\CmsBundle\Api\Contracts\LanguageEnableable;
use SG\CmsBundle\Api\Contracts\Publishable;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\LanguageEnableableTrait;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Doctrine\Contracts\VersionableTrait;
use SG\CmsBundle\Common\Contracts\EnableableTrait;
use SG\CmsBundle\Common\Contracts\PositionableTrait;
use SG\CmsBundle\Common\Contracts\SluggableTrait;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class AbstractContent
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractContent implements Versionable, Routable, CmsResourceInterface, LanguageEnableable, Cacheable, Publishable, Enableable
{
    use TimestampableTrait;
    use SluggableTrait;
    use LanguageEnableableTrait;
    use EnableableTrait;
    use VersionableTrait;
    use CmsResourceTrait;
    use PositionableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get","publish"})
     */
    protected int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "post", "put", "publish"})
     */
    protected ?string $discriminator = null;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "post", "put", "publish"})
     * @Translation()
     */
    protected string $defaultName;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"get", "post", "put", "publish", "parents"})
     * @Gedmo\Slug(fields={"defaultName"}, updatable=false, unique=true)
     */
    protected ?string $slug = null;

    /**
     * AbstractContent constructor.
     */
    public function __construct()
    {
        $this->languages = new ArrayCollection();
    }

    /**
     * @return array<string>
     */
    public function getCacheTags(): array
    {
        return [];
    }

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getResourceIdentifier(): int
    {
        return $this->getId();
    }

    /**
     * @return string
     */
    public function getResourceClass(): string
    {
        $reflectionClass = new ReflectionClass($this);
        if (str_contains($reflectionClass->getName(), "Proxies")) {
            return $reflectionClass->getParentClass()->getName();
        }
        return $reflectionClass->getName();
    }

    /**
     * @return string|null
     */
    public function getDiscriminator(): ?string
    {
        return $this->discriminator;
    }

    /**
     * @param string $discriminator
     *
     * @return $this
     */
    public function setDiscriminator(string $discriminator): self
    {
        $this->discriminator = $discriminator;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDefaultName(): ?string
    {
        return $this->defaultName;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setDefaultName(string $name): self
    {
        $this->defaultName = $name;
        return $this;
    }
}
