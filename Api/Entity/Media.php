<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use SG\CmsBundle\Api\Contracts\MediaInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use OpenApi\Annotations as OA;

/**
 * Class Media
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity()
 * @UniqueEntity("providerReference")
 */
class Media implements MediaInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     */
    protected int $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection", "post"})
     */
    protected string $providerName;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection", "post"})
     */
    protected ?string $originalName = null;

    /**
     * @ORM\Column(type="string", length=255, unique=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection", "post"})
     */
    protected string $providerReference;

    /**
     * @var string[]
     * @ORM\Column(type="json", nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     * @OA\Property(type="array", @OA\Items(type="string"))
     */
    protected ?array $providerMetadata = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     */
    protected ?string $contentType = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     */
    protected ?int $contentSize = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     */
    protected ?int $width = null;

    /**
     * @ORM\Column(type="integer", nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     */
    protected ?int $height = null;

    /**
     * @ORM\Column(type="decimal", nullable=true)
     * @Groups({"get", "medias", "galleries", "publish", "media_collection"})
     */
    protected ?string $length = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "medias", "galleries", "publish"})
     */
    protected ?string $author = null;

    /**
     * @var Collection<ResourceHasMedia>
     * @ORM\OneToMany(targetEntity="ResourceHasMedia", mappedBy="media")
     * @Groups({"media_collection"})
     */
    protected Collection $resourceHasMedias;

    /**
     * @var Collection<GalleryHasMedia>
     * @ORM\OneToMany(targetEntity="GalleryHasMedia", mappedBy="media")
     */
    protected Collection $galleries;

    public function __construct()
    {
        $this->resourceHasMedias    = new ArrayCollection();
        $this->galleries            = new ArrayCollection();
    }

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return string
     */
    public function getProviderName(): string
    {
        return $this->providerName;
    }

    /**
     * @param string $providerName
     *
     * @return $this
     */
    public function setProviderName(string $providerName): self
    {
        $this->providerName = $providerName;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getOriginalName(): ?string
    {
        return $this->originalName;
    }

    /**
     * @param string|null $originalName
     *
     * @return $this
     */
    public function setOriginalName(?string $originalName): self
    {
        $this->originalName = $originalName;

        return $this;
    }

    /**
     * @return string
     */
    public function getProviderReference(): string
    {
        return $this->providerReference;
    }

    /**
     * @param string $providerReference
     *
     * @return $this
     */
    public function setProviderReference(string $providerReference): self
    {
        $this->providerReference = $providerReference;

        return $this;
    }

    /**
     * @return string[]|null
     */
    public function getProviderMetadata(): ?array
    {
        return $this->providerMetadata;
    }

    /**
     * @param array<string> $metadata
     *
     * @return $this
     */
    public function setProviderMetadata(array $metadata): self
    {
        $this->providerMetadata = $metadata;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContentType(): ?string
    {
        return $this->contentType;
    }

    /**
     * @param string $contentType
     *
     * @return $this
     */
    public function setContentType(string $contentType): self
    {
        $this->contentType = $contentType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getContentSize(): ?int
    {
        return $this->contentSize;
    }

    /**
     * @param int|null $size
     *
     * @return $this
     */
    public function setContentSize(int $size = null): self
    {
        $this->contentSize = $size;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getWidth(): ?int
    {
        return $this->width;
    }

    /**
     * @param int|null $width
     *
     * @return $this
     */
    public function setWidth(int $width = null): self
    {
        $this->width = $width;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getHeight(): ?int
    {
        return $this->height;
    }

    /**
     * @param int|null $height
     *
     * @return $this
     */
    public function setHeight(int $height = null): self
    {
        $this->height = $height;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLength(): ?string
    {
        return $this->length;
    }

    /**
     * @param string|null $length
     *
     * @return $this
     */
    public function setLength(string $length = null): self
    {
        $this->length = $length;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getAuthor(): ?string
    {
        return $this->author;
    }

    /**
     * @param string|null $author
     *
     * @return $this
     */
    public function setAuthor(string $author = null): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getResourceHasMedias(): Collection
    {
        return $this->resourceHasMedias;
    }

    /**
     * @param ResourceHasMedia $resourceHasMedia
     *
     * @return $this
     */
    public function addResourceHasMedia(ResourceHasMedia $resourceHasMedia): Media
    {
        $this->resourceHasMedias[] = $resourceHasMedia;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getGalleries(): Collection
    {
        return $this->galleries;
    }

    /**
     * @param Gallery $gallery
     *
     * @return $this
     */
    public function addGallery(Gallery $gallery): Media
    {
        $this->galleries[] = $gallery;

        return $this;
    }
}
