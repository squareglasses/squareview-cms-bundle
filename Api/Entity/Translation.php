<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * Class Translation
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity(repositoryClass="SG\CmsBundle\Api\Repository\TranslationRepository")
 */
class Translation
{
    use TimestampableTrait;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"get"})
     */
    private int $id;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get", "post"})
     */
    private ?string $message = null;

    /**
     * @ORM\ManyToOne(targetEntity="TranslationKey", inversedBy="translations")
     * @ORM\JoinColumn(nullable=false, onDelete="CASCADE")
     * Groups({"get", "post"})
     *
     */
    private ?TranslationKey $translationKey = null;

    /**
     * @ORM\ManyToOne(targetEntity="Language")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Language $language = null;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     *
     * @return Translation
     */
    public function setMessage(?string $message): Translation
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return TranslationKey|null
     */
    public function getTranslationKey(): ?TranslationKey
    {
        return $this->translationKey;
    }

    /**
     * @param TranslationKey|null $translationKey
     *
     * @return Translation
     */
    public function setTranslationKey(?TranslationKey $translationKey): Translation
    {
        $this->translationKey = $translationKey;
        return $this;
    }

    /**
     * @return Language|null
     */
    public function getLanguage(): ?Language
    {
        return $this->language;
    }

    /**
     * @param Language|null $language
     *
     * @return Translation
     */
    public function setLanguage(?Language $language): Translation
    {
        $this->language = $language;
        return $this;
    }
}
