<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;

/**
 * Class MetaTag
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @ORM\Entity()
 * @ORM\Table(
 *   indexes={
 *     @ORM\Index(name="resource_class_idx", columns={"resource_class"}),
 *     @ORM\Index(name="resource_identifier_idx", columns={"resource_identifier"}),
 *     @ORM\Index(name="locale_idx", columns={"locale"})
 *   }
 * )
 */
class MetaTag
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups({"get", "publish"})
     */
    protected int $id;

    /**
     *  Identify the associated resource with a unique id.
     *  It should be the guid of the resource or its unique slug
     *
     * @ORM\Column(type="integer", length=255)
     * @Groups({"get", "post"})
     */
    protected int $resourceIdentifier;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"get", "post"})
     */
    protected string $resourceClass;

    /**
     * @var string $name Specifies a name for the metadata
     *
     * @ORM\Column(type="string", length=128)
     * @Groups({"get", "publish", "post"})
     */
    protected string $name;

    /**
     * @var string|null $content Gives the value associated with the http-equiv or name attribute
     *
     * @ORM\Column(type="text", length=255, nullable=true)
     * @Groups({"get", "publish", "post", "put"})
     */
    protected ?string $content = null;

    /**
     * @ORM\Column(type="string")
     * @Groups({"get", "publish", "post"})
     */
    protected string $type;

    /**
     * @var string $locale The locale used for this tag content.
     *
     * @ORM\Column(type="string", length=16)
     * @Groups({"get", "publish", "post"})
     */
    protected string $locale;

    public function __construct()
    {
        $this->setType("name");
    }

    /**
     * @return int|null
     */
    public function getId(): int|null
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getResourceIdentifier(): ?int
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int $resourceIdentifier
     *
     * @return $this
     */
    public function setResourceIdentifier(int $resourceIdentifier): self
    {
        $this->resourceIdentifier = $resourceIdentifier;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string $resourceClass
     *
     * @return $this
     */
    public function setResourceClass(string $resourceClass): self
    {
        $this->resourceClass = $resourceClass;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getContent(): ?string
    {
        return $this->content;
    }

    /**
     * @param string|null $content
     *
     * @return $this
     */
    public function setContent(?string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return $this
     */
    public function setType(string $type): self
    {
        // @TODO: Add values check
        $this->type = $type;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getLocale(): ?string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return $this
     */
    public function setLocale(string $locale): self
    {
        $this->locale = $locale;

        return $this;
    }

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->getName();
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return "metatag";
    }
}
