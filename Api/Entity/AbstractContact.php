<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Entity;

use Doctrine\ORM\Mapping as ORM;
use SG\CmsBundle\Common\Contracts\ContactInterface;
use SG\CmsBundle\Common\Contracts\TimestampableTrait;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstractContact
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractContact implements ContactInterface
{
    use TimestampableTrait;

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Groups("get")
     */
    protected int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     * @Groups({"get", "post"})
     * @Assert\Email(message="errors.email.invalid")
     * @Assert\NotBlank(message="errors.email.notblank")
     */
    protected ?string $email = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post"})
     */
    protected ?string $firstName = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post"})
     */
    protected ?string $lastName = null;

    /**
     * @ORM\Column(type="string", length=180, nullable=true)
     * @Groups({"get", "post"})
     */
    protected ?string $phoneNumber = null;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Groups({"get", "post"})
     */
    protected ?string $company = null;

    /**
     * @ORM\Column(type="boolean")
     * @Groups({"get", "post", "put"})
     */
    protected bool $treated = false;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Groups({"get", "post"})
     */
    protected ?string $message = null;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     *
     * @return void
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getEmail(): ?string
    {
        return $this->email;
    }

    /**
     * @param string|null $email
     *
     * @return void
     */
    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $firstName
     *
     * @return void
     */
    public function setFirstName(?string $firstName): void
    {
        $this->firstName = $firstName;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string|null $lastName
     *
     * @return void
     */
    public function setLastName(?string $lastName): void
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string|null
     */
    public function getPhoneNumber(): ?string
    {
        return $this->phoneNumber;
    }

    /**
     * @param string|null $phoneNumber
     *
     * @return void
     */
    public function setPhoneNumber(?string $phoneNumber): void
    {
        $this->phoneNumber = $phoneNumber;
    }

    /**
     * @return string|null
     */
    public function getCompany(): ?string
    {
        return $this->company;
    }

    /**
     * @param string|null $company
     *
     * @return void
     */
    public function setCompany(?string $company): void
    {
        $this->company = $company;
    }

    /**
     * @return bool
     */
    public function isTreated(): bool
    {
        return $this->treated;
    }

    /**
     * @param bool $treated
     *
     * @return void
     */
    public function setTreated(bool $treated): void
    {
        $this->treated = $treated;
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     *
     * @return void
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }
}
