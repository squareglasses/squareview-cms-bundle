<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Route;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Common\Filter\OrderFilterInterface;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\OrderFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\SearchFilter;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route as RouteAnnotation;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use SG\CmsBundle\Api\Entity\Route;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @RouteAnnotation("/routes", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Route resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="Route collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Route::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="path",
     *     in="query",
     *     description="The route's path"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The route's locale"
     * )
     * @OA\Tag(name="Routes")
     *
     * @param ManagerRegistry        $managerRegistry
     * @param EntityManagerInterface $entityManager
     *
     * @return Response
     */
    public function __invoke(ManagerRegistry $managerRegistry, EntityManagerInterface $entityManager): Response
    {
        $this->addFilter(SearchFilter::class, ['path' => SearchFilterInterface::STRATEGY_EXACT]);
        $this->addFilter(SearchFilter::class, ['locale' => SearchFilterInterface::STRATEGY_EXACT]);

        $queryBuilder = $entityManager->getRepository(Route::class)->createQueryBuilder("node");
        $queryBuilder->addOrderBy("node.priority", "DESC");
        $resources = $this->dataProvider->getCollection(Route::class, $queryBuilder);

        return new JsonResponse($this->serializer->serialize($resources, "json", ['groups' => 'get']), 200, [], true);
    }
}
