<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Route;

use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route as RouteAnnotation;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemForResourceAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @RouteAnnotation("/routes/{locale}/{resourceIdentifier}", methods={"GET"})
 */
class GetItemForResourceAction extends AbstractItemAction
{
    /**
     * Retrieves a Route resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Route resource response.",
     *     @Model(type=Route::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="path",
     *     description="The route locale"
     * )
     * @OA\Parameter(
     *     name="resourceIdentifier",
     *     in="path",
     *     description="The route resource identifier"
     * )
     * @OA\Tag(name="Routes")
     *
     * @param Request $request
     * @param string  $locale
     * @param string  $resourceIdentifier
     *
     * @return Response
     */
    public function __invoke(Request $request, string $locale, string $resourceIdentifier): Response
    {
        $route = $this->getRepository(Route::class)->findOneBy(['locale' => $locale, 'resourceIdentifier' => $resourceIdentifier]);
        if (!$route) {
            throw new ResourceNotFoundException("No Route found for locale '".$locale."' and resourceIdentifier '".$resourceIdentifier."'");
        }
        return new JsonResponse($this->serializer->serialize($route, "json", ['groups' => 'get']), 200, [], true);
    }
}
