<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Route;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use SG\CmsBundle\Api\Entity\Route as RouteEntity;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/routes/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Update a Route resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Return 200 Status Code when route is updated."
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     @OA\Schema(type="integer"),
     *     description="The route id"
     * )
     * @OA\RequestBody (
     *     description="The route json",
     *     @Model(type=RouteEntity::class, groups={"put"})
     * )
     * @OA\Tag(name="Routes")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        /** @var RouteEntity $route */
        $route = $this->entityManager
            ->getRepository(RouteEntity::class)
            ->findOneBy(['id'=>$id]);
        if (!is_a($route, RouteEntity::class)) {
            throw new AccessDeniedHttpException('No route found for id "'.$id.'".');
        }

        $this->serializer->deserialize($request->getContent(), RouteEntity::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $route,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($route);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($route);

        return new JsonResponse(null, 200);
    }
}
