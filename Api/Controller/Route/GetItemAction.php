<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Route;

use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route as RouteAnnotation;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @RouteAnnotation("/routes/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Route resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Route resource response.",
     *     @Model(type=Route::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     @OA\Schema(type="integer"),
     *     description="The route id"
     * )
     * @OA\Tag(name="Routes")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $content = $this->getRepository(Route::class)->find($id);
        if (!$content) {
            throw new ResourceNotFoundException("No Content found for id '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($content, "json", ['groups' => 'get']), 200, [], true);
    }
}
