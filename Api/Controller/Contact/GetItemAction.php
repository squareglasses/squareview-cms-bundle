<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Contact;

use App\Entity\Contact;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Route("/contacts/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Contact resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content resource response.",
     *     @Model(type=Contact::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The content resource id"
     * )
     * @OA\Tag(name="Contacts")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $contact = $this->getRepository($this->configurationBag->getContactClass())->findOneBy(['id' => $id]);
        if (!$contact) {
            throw new ResourceNotFoundException("No Contact found for id or slug '".$id."'");
        }

        return new JsonResponse($this->serializer->serialize($contact, "json", ['groups' => 'get']), 200, [], true);
    }
}
