<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Contact;

use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Contact;

/**
 * List the contacts.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contacts", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Contact resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="Contact collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Contact::class, groups={"get"}))
     *     )
     * )
     * @OA\Tag(name="Contacts")
     *
     * @param Request             $request
     * @param PaginationExtension $paginationExtension
     *
     * @return Response
     */
    public function __invoke(Request $request, PaginationExtension $paginationExtension): Response
    {
        if ($request->get('pagination', true) !== 'false'
            && $request->get('pagination', true) !== false
        ) {
            $this->addExtension($paginationExtension, [
                'items_per_page' => 20
            ]);
        }

        return $this->dataProvider->getResponse($this->configurationBag->getContactClass(), null, ['get']);
    }
}
