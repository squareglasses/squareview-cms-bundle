<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Contact;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use App\Entity\Contact;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contacts/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a  Contact resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Contact resource response.",
     *     @Model(type=Contact::class, groups={"get"})
     * )
     * @OA\RequestBody(
     *     description="The contact json",
     *     @Model(type=Contact::class, groups={"put"})
     * )
     * @OA\Tag(name="Contacts")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        /** @var Contact $contact */
        $contact = $this->entityManager
            ->getRepository(Contact::class)
            ->findOneBy(['id' => $id]);
        if (!is_a($contact, Contact::class)) {
            throw new AccessDeniedHttpException('No Contact found for id "' . $id . '".');
        }
        $requestBody = $request->getContent();

        $this->serializer->deserialize($requestBody, Contact::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $contact,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($contact);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }
        $this->persistAndFlush($contact);

        return new JsonResponse($this->serializer->serialize(
            $contact,
            "json",
            ['groups' => ['get']]
        ), 200, [], true);
    }
}
