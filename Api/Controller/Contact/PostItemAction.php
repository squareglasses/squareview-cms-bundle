<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Contact;

use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use App\Entity\Contact;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contacts", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Post a new Contact resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Contact resource response.",
     *     @Model(type=Contact::class, groups={"get"})
     * )
     * @OA\RequestBody(
     *     description="The contact json",
     *     @Model(type=Contact::class, groups={"post"})
     * )
     * @OA\Tag(name="Contacts")
     *
     * @param Request $request
     *
     * @return Response
     * @throws JsonException
     */
    public function __invoke(Request $request): Response
    {
        $contact = $this->serializer->deserialize($request->getContent(), $this->configurationBag->getContactClass(), 'json', [
            'groups' => 'post'
        ]);

        $errors = $this->validator->validate($contact);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($contact);

        return new JsonResponse($this->serializer->serialize($contact, "json", ['groups' => 'get']), 200, [], true);
    }
}
