<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Contact;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contacts/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a Contact resource.
     *
     * @OA\Response(
     *     response=204,
     *     description="Contact has been deleted"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Contact id"
     * )
     * @OA\Tag(name="Contacts")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param string                 $id
     *
     * @return Response
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager, string $id): Response
    {
        $contact = $this->getRepository($this->configurationBag->getContactClass())->find($id);
        if (!$contact) {
            throw new ResourceNotFoundException("No Contact found for id '".$id."'");
        }

        $entityManager->remove($contact);
        $entityManager->flush();

        return new JsonResponse(null, 204);
    }
}
