<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\ResourceHasMedia;

use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resource_has_medias", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of ResourceHasMedia resources.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="ResourceHasMedia collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=ResourceHasMedia::class, groups={"get"}))
     *     )
     * )
     * @OA\Tag(name="ResourceHasMedias")
     *
     * @return Response
     */
    public function __invoke(): Response
    {
        $resources = $this->dataProvider->getCollection(ResourceHasMedia::class);

        return new JsonResponse($this->serializer->serialize($resources, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
