<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\ResourceHasMedia;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resource_has_medias/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a ResourceHasMedia (change media associated to the resource)
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Updated draft hashes of the modulable resource."
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The resourceHasMedia id"
     * )
     * @OA\RequestBody (
     *     description="The resourceHasMedia json",
     *     @Model(type=ResourceHasMedia::class, groups={"put"})
     * )
     * @OA\Tag(name="ResourceHasMedias")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $resourceHasMedia = $this->entityManager
            ->getRepository(ResourceHasMedia::class)
            ->find($id);

        if (!is_a($resourceHasMedia, ResourceHasMedia::class)) {
            throw new AccessDeniedHttpException('No resourceHasMedia found for id "' . $id . '".');
        }

        //TODO: make the change of media (given an id) work
        $this->serializer->deserialize($request->getContent(), ResourceHasMedia::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $resourceHasMedia,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($resourceHasMedia);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($resourceHasMedia);

        //TODO: Handle draft hashes

        return new JsonResponse($this->serializer->serialize($resourceHasMedia, 'json', ['groups' => 'medias']), Response::HTTP_OK, [], true);
    }
}
