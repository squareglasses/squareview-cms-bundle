<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\ResourceHasMedia;

use Exception;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resource_has_medias/{discriminator}/{resourceIdentifier}", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a ResourceHasMedia (associates a media to a resource)
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="Updated draft hashes of the modulable resource."
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The resource to associate media to's discriminator"
     * )
     * @OA\Parameter(
     *     name="resourceIdentifier",
     *     in="path",
     *     description="The resource to associate media to's identifier"
     * )
     * @OA\RequestBody (
     *     description="The resourceHasMedia json",
     *     @Model(type=ResourceHasMedia::class, groups={"post"})
     * )
     * @OA\Tag(name="ResourceHasMedias")
     *
     * @param Request $request
     * @param string  $discriminator
     * @param string  $resourceIdentifier
     *
     * @return JsonResponse
     * @throws Exception
     */
    public function __invoke(Request $request, string $discriminator, string $resourceIdentifier): JsonResponse
    {
        $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
        if (!$resourceClass) {
            throw new NotFoundHttpException('No discriminator configured for "'.$discriminator.'" under "sg_cms.api.discriminators".');
        }

        /** @var HasMediasInterface $resource */
        $resource = $this->entityManager
            ->getRepository($resourceClass)
            ->find($resourceIdentifier)
        ;

        if ($resource instanceof Versionable) {
            $resource = $resource->getDraftVersion();
        }

        if (null === $resource) {
            throw new NotFoundHttpException('No modulable resource found for class "'.$resourceClass.'" and id "' . $resourceIdentifier . '".');
        }

        /** @var ResourceHasMedia $resourceHasMedia */
        $resourceHasMedia = $this->serializer->deserialize($request->getContent(), ResourceHasMedia::class, 'json', [
            'groups' => 'post'
        ]);

        $resourceHasMedia
            ->setResourceClass($resource->getResourceClass())
            ->setResourceIdentifier($resource->getResourceIdentifier());

        $errors = $this->validator->validate($resourceHasMedia);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($resourceHasMedia);

        return new JsonResponse($this->serializer->serialize($resourceHasMedia, 'json', ['groups' => 'medias']), Response::HTTP_CREATED, [], true);
    }
}
