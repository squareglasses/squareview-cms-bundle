<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\ResourceHasMedia;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/resource_has_medias/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a ResourceHasMedia (remove media associated to the resource)
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Media has been disassociated from resource"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The resourceHasMedia id to delete"
     * )
     * @OA\Tag(name="ResourceHasMedias")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $resourceHasMedia = $this->entityManager
            ->getRepository(ResourceHasMedia::class)
            ->find($id);

        if (!is_a($resourceHasMedia, ResourceHasMedia::class)) {
            throw new AccessDeniedHttpException('No resourceHasMedia found for id "' . $id . '".');
        }

        $this->entityManager->remove($resourceHasMedia);
        $this->entityManager->flush();

        //Handle draft hashes ?

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
