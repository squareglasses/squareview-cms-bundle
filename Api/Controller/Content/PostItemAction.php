<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\LanguageEnableable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Manager\RouteManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Entity\Route as RouteEntity;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use App\Entity\Content;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a Content resource.
     *
     * @OA\Response(
     *     response=201,
     *     description="The created resource"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The locale of the first draft and translations"
     * )
     * @OA\RequestBody (
     *     description="The content json",
     *     @Model(type=Content::class, groups={"post"})
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param CacheResourcesHolder $cacheResourcesHolder
     * @return Response
     *
     * @throws JsonException
     * @throws TranslationClientException
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager, CacheResourcesHolder $cacheResourcesHolder, RouteManager $routeManager): Response
    {
        $requestBody = $request->getContent();
        $data = json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR);

        $content = $this->serializer->deserialize(
            $requestBody,
            $this->configurationBag->getContentClass(),
            'json',
            [
                'groups' => 'post'
            ]
        );

        $errors = $this->validator->validate($content);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($content);

        // Translate properties if necessary
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());
        if ($content instanceof Translatable) {
            $this->translationsManager->updateTranslatablePropertiesTranslations(
                $content,
                json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR),
                $locale
            );
            $this->persistAndFlush($content);
        }

        if ($content instanceof Versionable) {
            $draft = $this->serializer->deserialize(
                $requestBody,
                $this->configurationBag->getContentVersionClass(),
                'json',
                [
                    'groups' => 'post'
                ]
            );

            $draft->setVersionable($content)
                ->setCurrentDraft(true)
                ->setLocale($locale)
            ;
            $content->addVersion($draft);

            $errors = $this->validator->validate($draft);
            if (count($errors) > 0) {
                return new ConstraintViolationResponse($errors);
            }

            $this->persistAndFlush($draft);
            $this->persistAndFlush($content);

            if ($draft instanceof Translatable) {
                $this->translationsManager->updateTranslatablePropertiesTranslations(
                    $draft,
                    json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR),
                    $locale
                );
                $this->persistAndFlush($draft);
            }

            if (isset($data['hash'])) {
                $draftHashes = $draft->getDraftHashes();
                $draftHashes[$locale] = [
                    'hash' => $data['hash'],
                    'date' => new DateTime()
                ];
                $draft->setDraftHashes($draftHashes);

                $this->persistAndFlush($draft);
            }

            //Add languages, all 'enabled' by default
            $enabledLanguages = $entityManager->getRepository(Language::class)->findBy(['enabled' => true]);
            foreach ($enabledLanguages as $language) {
                if ($content instanceof LanguageEnableable) {
                    $content->addLanguage($language);
                }
                if ($content instanceof Versionable && $draft instanceof LanguageEnableable) {
                    $draft->addLanguage($language);
                }
            }

            //Generate route for locale
            foreach ($enabledLanguages as $language) {
                $route = new RouteEntity();
                if ($content->getParent() !== null) {
                    $parentRoute = $this->getRepository(RouteEntity::class)
                        ->findOneBy([
                            'locale' => $language->getId(),
                            'resourceIdentifier' => $content->getParent()->getResourceIdentifier(),
                            'resourceClass' => $content->getParent()->getResourceClass()
                        ]);
                    $path = $parentRoute->getPath()."/".$content->getSlug();
                } else {
                    $path = $content->getSlug();
                }
                $route->setPath($path);
                $route->setLocale($language->getId());
                $route->setName($content->getSlug());
                $route->setResourceIdentifier($content->getResourceIdentifier());
                $route->setResourceClass($content->getResourceClass());
                $this->persistAndFlush($route);
            }
        }

        $serializedContent = $this->serializer->normalize($content, "json", [
            'groups' => [
                'get',
                'medias',
                'galleries',
                'modules',
                'metaTags'
            ]
        ]);

        //We want parents to be normalized in a simpler way, so we normalize them separately before merging both normalizations
        //TODO: maybe this could be done in a nice way
        $serializedContentParents = $this->serializer->normalize($content, "json", [
            'groups' => 'parents'
        ]);

        $cacheResourcesHolder->addResource($content);
        $route = $routeManager->getRouteByResource($content, $locale);
        if (null !== $route) {
            $cacheResourcesHolder->addRoute($route);
        }

        return new JsonResponse(array_merge($serializedContent, $serializedContentParents), 200, [], false);
    }
}
