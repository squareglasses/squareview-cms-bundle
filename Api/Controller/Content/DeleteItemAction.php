<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Manager\ResourceManager;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a Content resource.
     *
     * @OA\Response(
     *     response=204,
     *     description="Content and its versions have been deleted"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Content id"
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request         $request
     * @param ResourceManager $manager
     * @param string          $id
     *
     * @return Response
     */
    public function __invoke(Request $request, ResourceManager $manager, string $id): Response
    {
        $content = $this->getRepository($this->configurationBag->getContentClass())->find($id);
        if (!$content) {
            throw new ResourceNotFoundException("No Content found for id '".$id."'");
        }

        $manager->deleteResource($content);

        return new JsonResponse(null, 204);
    }
}
