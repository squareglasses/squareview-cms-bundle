<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use DateTime;
use JsonException;
use SG\CmsBundle\Api\Contracts\Enableable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Entity\AbstractContent;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use SG\CmsBundle\Api\Contracts\Translatable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use App\Entity\Content;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a Content resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Updated draft hashes of the resource"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The content resource id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\RequestBody (
     *     description="The content json",
     *     @Model(type=Content::class, groups={"put"})
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request              $request
     * @param string               $id
     * @param CacheResourcesHolder $cacheResourcesHolder
     *
     * @return Response
     * @throws JsonException
     * @throws TranslationClientException
     */
    public function __invoke(Request $request, string $id, CacheResourcesHolder $cacheResourcesHolder): Response
    {
        /** @var Translatable|Enableable $content */
        $content = $this->entityManager
            ->getRepository($this->configurationBag->getContentClass())
            ->find($id);
        if (!is_a($content, $this->configurationBag->getContentClass())) {
            throw new AccessDeniedHttpException('No content found for id "'.$id.'".');
        }

        $isEnabled = $content->isEnabled();

        $requestBody = $request->getContent();
        $data = json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR);

        $this->serializer->deserialize(
            $requestBody,
            $this->configurationBag->getContentClass(),
            'json',
            [
                AbstractNormalizer::OBJECT_TO_POPULATE => $content,
                AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
                'groups' => 'put'
            ]
        );

        $errors = $this->validator->validate($content);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        // Translate properties if necessary
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());

        if ($content instanceof Translatable) {
            $this->translationsManager->updateTranslatablePropertiesTranslations(
                $content,
                $data,
                $locale
            );
            $this->persistAndFlush($content);
        }

        $draft = $content->getDraftVersion();
        $this->serializer->deserialize(
            $requestBody,
            $this->configurationBag->getContentVersionClass(),
            'json',
            [
                AbstractNormalizer::OBJECT_TO_POPULATE => $draft,
                AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
                'groups' => 'put'
            ]
        );

        $errors = $this->validator->validate($draft);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->translationsManager->updateTranslatablePropertiesTranslations(
            $draft,
            $data,
            $locale
        );

        if (isset($data['hash'])) {
            $draftHashes = $draft->getDraftHashes();
            $draftHashes[$locale] = [
                'hash' => $data['hash'],
                'date' => new DateTime()
            ];
            $draft->setDraftHashes($draftHashes);
        }

        $this->persistAndFlush($draft);

        if ($content instanceof AbstractContent) {
            if ((isset($data['name'])) && ($locale === $this->configurationBag->getDefaultLocale())) {
                $content->setDefaultName($data['name']);
            }
            $this->persistAndFlush($content);
        }

        // Invalidate cache if necessary
        if ($isEnabled !== $content->isEnabled() && $content->isEnabled() === false) {
            $cacheResourcesHolder->addResource($content);
        }

        return new JsonResponse($draft->getDraftHashes(), 200);
    }
}
