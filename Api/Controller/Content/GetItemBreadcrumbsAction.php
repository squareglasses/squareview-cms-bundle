<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use ReflectionException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Manager\BreadcrumbsManager;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use App\Entity\Content;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class GetItemBreadcrumbsAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents/breadcrumbs/{id}", methods={"GET"})
 */
class GetItemBreadcrumbsAction extends AbstractItemAction
{
    private BreadcrumbsManager $breadcrumbsManager;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $publicationEntityManager,
        ConfigurationBag $configurationBag,
        ValidatorInterface $validator,
        TranslationManager $translationManager,
        BreadcrumbsManager $breadcrumbsManager,
        TagExtractor $tagExtractor
    ) {
        parent::__construct(
            $serializer,
            $publicationEntityManager,
            $configurationBag,
            $validator,
            $translationManager,
            $tagExtractor
        );
        $this->breadcrumbsManager = $breadcrumbsManager;
    }

    /**
     * Retrieves a Content resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content resource response.",
     *     @Model(type=Content::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The content resource id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     *
     * @throws MappingException
     * @throws NonUniqueResultException
     * @throws ReflectionException
     * @throws Exception
     */
    public function __invoke(Request $request, string $id): Response
    {
        $content = $this->getRepository($this->configurationBag->getContentClass())->findBySlugOrId($id);
        if (!$content) {
            throw new ResourceNotFoundException("No Content found for id or slug '".$id."'");
        }
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());
        $breadcrumbs = [];
        $serializedContent = $this->serializer->normalize($content, "json", [
            'groups' => [
                'breadcrumbs'
            ]
        ]);
        $tags = $this->tagExtractor->generateTags($content);
        $breadcrumbs[] = [
            'label' => BreadcrumbsManager::getLabelFromSerializedEntity($serializedContent),
            'route' => $this->breadcrumbsManager->getRouteForResource($content, $locale)
        ];

        $loopContent = $content;
        while ($parent = $loopContent->getParent()) {
            $serializedParent = $this->serializer->normalize($parent, "json", [
                'groups' => [
                    'breadcrumbs'
                ]
            ]);
            $breadcrumbs[] = [
                'label' => BreadcrumbsManager::getLabelFromSerializedEntity($serializedParent),
                'route' => $this->breadcrumbsManager->getRouteForResource($parent, $locale)
            ];
            $tags = array_merge($tags, $this->tagExtractor->generateTags($parent));
            $loopContent = $parent;
        }

        return new JsonResponse(array_reverse($breadcrumbs), 200, [
            'cache-tags' => implode(',', $tags)
        ], false);
    }
}
