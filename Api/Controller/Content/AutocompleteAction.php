<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use App\Entity\ContentVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Content;
use RuntimeException;
use SG\CmsBundle\Api\Entity\Route as CmsRoute;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AutocompleteAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents/autocomplete", methods={"GET"})
 */
class AutocompleteAction extends AbstractCollectionAction
{
    /**
     * Search for contents.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content collection response.",
     *     @OA\Schema(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Content::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="search",
     *     in="query",
     *     description="The search string"
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request                $request
     * @param PaginationExtension    $paginationExtension
     * @param EntityManagerInterface $manager
     *
     * @return Response
     */
    public function __invoke(
        Request $request,
        PaginationExtension $paginationExtension,
        EntityManagerInterface $manager
    ): Response {
        $this->addExtension($paginationExtension, [
            'items_per_page' => 100
        ]);

        $contentRepository = $manager->getRepository($this->configurationBag->getContentClass());

        if (!method_exists($contentRepository, 'createQueryBuilder')) {
            throw new RuntimeException('The repository class must have a "createQueryBuilder" method.');
        }

        $contents = $contentRepository->createQueryBuilder('c')
            ->innerJoin(CmsRoute::class, 'r', Join::WITH, 'r.resourceIdentifier = c.id')
            ->innerJoin(ContentVersion::class, 'cv', Join::WITH, 'cv.versionable = c.id')
            ->andWhere('r.locale = :locale')
            ->andWhere('c.enabled = 1')
            ->andWhere('r.path NOT LIKE :path')
            ->andWhere('cv.currentVersion = 1')
            ->andWhere('cv.name LIKE :expr')
            ->orWhere('c.defaultName LIKE :expr')
            ->setParameter('locale', $request->getLocale())
            ->setParameter('path', '%{%')
            ->setParameter('expr', '%'.$request->get('search').'%')
            ->addOrderBy('cv.name', 'ASC')
        ;

        return $this->dataProvider->getResponse($this->configurationBag->getContentClass(), $contents, ['get', 'expose_class']);
    }
}
