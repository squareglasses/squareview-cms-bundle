<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use Exception;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Util\ApiHelper;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use App\Entity\Content;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Content resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content resource response.",
     *     @Model(type=Content::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The content resource id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\Parameter(
     *      name="serialization_groups[]",
     *      in="query",
     *      @OA\Schema(type="array", @OA\Items(type="string")),
     *      description="Array of specific serialization groups"
     *  )
     * @OA\Tag(name="Contents")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function __invoke(Request $request, string $id): Response
    {
        $content = $this->getRepository($this->configurationBag->getContentClass())->findBySlugOrId($id);
        if (!$content) {
            throw new ResourceNotFoundException("No Content found for id or slug '".$id."'");
        }

        $locale = $request->query->get('locale');

        if (null !== $request->get("override_groups")) {
            $groups = $request->get("override_groups");
        } else {
            $groups = [
                'get',
                'medias',
                'galleries',
                'modules',
                'metatags',
            ];
        }

        $serializedContent = $this->serializer->normalize($content, "json", [
            'groups' => array_merge(ApiHelper::getSerializeGroupsFromRequest($request), $groups)
        ]);

        //We want parents to be normalized in a simpler way, so we normalize them separately before merging both normalizations
        //TODO: maybe this could be done in a nice way
        $serializedContentParents = $this->serializer->normalize($content, "json", [
            'groups' => 'parents'
        ]);

        //ResourceGuide translations
        $guidable = $content;
        if ($guidable instanceof Versionable) {
            $guidable = $guidable->getDraftVersion();
        }
        if ($guidable instanceof Guidable) {
            try {
                $guideTranslations = $this->translationsManager->getResourceGuideTranslations($guidable);
            } catch (Exception $e) {
                throw new RuntimeException("Error getting translations from SquareTranslate : ".$e->getMessae());
            }
            $translations = $this->translationsManager->formatTranslationsAsArray($guideTranslations, $locale, null, true);
            $serializedContent['translations'] = $translations;
        }

        return new JsonResponse(array_merge($serializedContent, $serializedContentParents), 200, [
            'cache-tags' => implode(',', $this->tagExtractor->generateTags($content))
        ], false);
    }
}
