<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use App\Entity\ContentVersion;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use RuntimeException;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\BooleanFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\SearchFilter;
use SG\CmsBundle\Api\Entity\Route as CmsRoute;
use SG\CmsBundle\Api\Util\ApiHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Content;

/**
 * Class GetSitemapAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/sitemap", methods={"GET"})
 */
class GetSitemapAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Content resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content collection response.",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Content::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="query",
     *     description="The content's discriminator"
     * )
     * @OA\Parameter(
     *     name="discriminator[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="The content's discriminator"
     * )
     * @OA\Parameter(
     *     name="serialization_groups[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="Array of specific serialization groups"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="An excluded id"
     * )
     * @OA\Parameter(
     *     name="id[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="Array of excluded ids"
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request                $request
     * @param EntityManagerInterface $manager
     *
     * @return Response
     */
    public function __invoke(Request $request, EntityManagerInterface $manager): Response
    {
        $this->addFilter(BooleanFilter::class, ['enabled' => SearchFilterInterface::STRATEGY_EXACT]);
        $this->addFilter(SearchFilter::class, ['id' => SearchFilterInterface::STRATEGY_DIFFERENT]);

        $cacheTags = [];
        if ($request->get('discriminator')) {
            $discriminators = $request->get('discriminator');
            if (is_array($discriminators)) {
                $cacheTags = array_merge($cacheTags, $discriminators);
            } else {
                $cacheTags[] = $request->get('discriminator');
            }
        }

        $contentRepository = $manager->getRepository($this->configurationBag->getContentClass());

        if (!method_exists($contentRepository, 'createQueryBuilder')) {
            throw new RuntimeException('The repository class must have a "createQueryBuilder" method.');
        }

        $queryBuilder = $contentRepository->createQueryBuilder('c');
        $queryBuilder
            ->andWhere($queryBuilder->expr()->in("c.discriminator", $request->get("discriminator")))
            ->innerJoin(CmsRoute::class, 'r', Join::WITH, 'r.resourceIdentifier = c.id')
            ->innerJoin(ContentVersion::class, 'cv', Join::WITH, 'cv.versionable = c.id')
            ->andWhere('r.locale = :locale')
            ->andWhere('c.enabled = 1')
            ->andWhere('cv.currentVersion = 1')
            ->setParameter('locale', $request->getLocale())
            ->addOrderBy('c.position', 'ASC')
        ;

        if (null === $request->get("parent")) {
            $queryBuilder->andWhere($queryBuilder->expr()->isNull('c.parent'));
        } else {
            $queryBuilder
                ->innerJoin("c.parent", "p")
                ->andWhere("p.slug = :parentSlug")
                ->setParameter("parentSlug", $request->get("parent"));
        }

        return $this->dataProvider->getResponse(
            $this->configurationBag->getContentClass(),
            $queryBuilder,
            ApiHelper::getSerializeGroupsFromRequest($request, ["get", "children"]),
            $cacheTags
        );
    }
}
