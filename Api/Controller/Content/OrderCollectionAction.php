<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class OrderCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents/order", methods={"PUT"})
 */
class OrderCollectionAction extends AbstractItemAction
{
    private CacheResourcesHolder $cacheResourcesHolder;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        ConfigurationBag $configurationBag,
        ValidatorInterface $validator,
        TranslationManager $translationsManager,
        TagExtractor $tagExtractor,
        CacheResourcesHolder $cacheResourcesHolder
    ) {
        parent::__construct($serializer, $entityManager, $configurationBag, $validator, $translationsManager, $tagExtractor);
        $this->cacheResourcesHolder = $cacheResourcesHolder;
    }

    /**
     * Updates positions for the given Content's ids array.
     *
     * @OA\Response(
     *     response=204,
     *     description="Returns 204 status code if positions are updated"
     * )
     * @OA\RequestBody (
     *     description="The array of content's id, in the desired order.",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"))
     *     )
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request $request
     *
     * @return Response
     *
     * @throws JsonException
     */
    public function __invoke(Request $request): Response
    {
        $ids = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        foreach ($ids as $position => $id) {
            $content = $this->entityManager
                ->getRepository($this->configurationBag->getContentClass())
                ->findOneBy(['id' => $id]);
            if (null === $content) {
                throw new ResourceNotFoundException("No Content found for id or slug '".$id."'");
            }
            $content->setPosition($position);
            $this->entityManager->persist($content);
            $this->cacheResourcesHolder->addResource($content);
        }
        $this->entityManager->flush();

        return new JsonResponse(Response::HTTP_NO_CONTENT, 200);
    }
}
