<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Content;

use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Common\Filter\OrderFilterInterface;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\FilterExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\BooleanFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\OrderFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\SearchFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\ExistsFilter;
use SG\CmsBundle\Api\Util\ApiHelper;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Content;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/contents", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Content resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content collection response.",
     *     @OA\JsonContent(
     *        type="array",
     *        @OA\Items(ref=@Model(type=Content::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="query",
     *     description="The content's discriminator"
     * )
     * @OA\Parameter(
     *     name="discriminator[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="The content's discriminator"
     * )
     * @OA\Parameter(
     *     name="name",
     *     in="query",
     *     description="The content's name"
     * )
     * @OA\Parameter(
     *     name="title",
     *     in="query",
     *     description="The content's title"
     * )
     * @OA\Parameter(
     *     name="parent.name",
     *     in="query",
     *     description="The content parent's name"
     * )
     * @OA\Parameter(
     *     name="parent.slug",
     *     in="query",
     *     description="The content parent's slug"
     * )
     * @OA\Parameter(
     *     name="exists[parent]",
     *     in="query",
     *     @OA\Schema(type="boolean"),
     *     description="If content has a parent"
     * )
     * @OA\Parameter(
     *     name="order[name]",
     *     in="query",
     *     description="The content's discriminator"
     * )
     * @OA\Parameter(
     *     name="order[title]",
     *     in="query",
     *     description="The content's discriminator"
     * )
     * @OA\Parameter(
     *     name="order[position]",
     *     in="query",
     *     description="Order results by position"
     * )
     * @OA\Parameter(
     *     name="order[versions.date]",
     *     in="query",
     *     description="Order results by date"
     * )
     * @OA\Parameter(
     *     name="enabled",
     *     in="query",
     *     @OA\Schema(type="boolean"),
     *     description="If content is enabled"
     * )
     * @OA\Parameter(
     *     name="order[position]",
     *     in="query",
     *     description="Order results by position"
     * )
     * @OA\Parameter(
     *     name="serialization_groups[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="Array of specific serialization groups"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="query",
     *     description="An excluded id"
     * )
     * @OA\Parameter(
     *     name="id[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="Array of excluded ids"
     * )
     * @OA\Parameter(
     *     name="items",
     *     in="query",
     *     @OA\Schema(type="integer"),
     *     description="The number of items per page for pagination"
     * )
     * @OA\Tag(name="Contents")
     *
     * @param Request             $request
     * @param PaginationExtension $paginationExtension
     *
     * @return Response
     */
    public function __invoke(Request $request, PaginationExtension $paginationExtension): Response
    {
        $this->setFilterMode($request->get('mode'));
        $this->addFilter(SearchFilter::class, ['discriminator' => SearchFilterInterface::STRATEGY_IN]);
        //@TODO: filter by title and name, but they are in the version, and possibly translated...
        //$this->addFilter(SearchFilter::class, ['title' => SearchFilter::STRATEGY_PARTIAL]);
        //$this->addFilter(SearchFilter::class, ['name' => SearchFilter::STRATEGY_PARTIAL]);

        $this->addFilter(SearchFilter::class, ['parent.name' => SearchFilterInterface::STRATEGY_PARTIAL]);
        $this->addFilter(SearchFilter::class, ['parent.slug' => SearchFilterInterface::STRATEGY_EXACT]);
        $this->addFilter(ExistsFilter::class, ['parent']);
        $this->addFilter(BooleanFilter::class, ['enabled' => SearchFilterInterface::STRATEGY_EXACT]);
        $this->addFilter(OrderFilter::class, [
            'createdAt' => 'desc',
            'updatedAt' => 'desc',
            'position' => 'asc',
            'versions.date' => 'desc',
            'name' => [
                'default_direction' => 'asc',
                'nulls_comparison' => OrderFilterInterface::NULLS_SMALLEST
            ],
            'title' => [
                'default_direction' => 'asc',
                'nulls_comparison' => OrderFilterInterface::NULLS_SMALLEST
            ]
        ]);
        $this->addFilter(SearchFilter::class, ['id' => SearchFilterInterface::STRATEGY_DIFFERENT]);

        $itemsPerPage = $request->query->has('items') && $request->query->get('items') > 0 ? $request->query->get('items') : 10;
        $this->addExtension($paginationExtension, [
            'items_per_page' => $itemsPerPage
        ]);

        $cacheTags = [];
        if ($request->get('discriminator')) {
            $discriminators = $request->get('discriminator');
            if (is_array($discriminators)) {
                $cacheTags = array_merge($cacheTags, $discriminators);
            } else {
                $cacheTags[] = $request->get('discriminator');
            }
        }

        return $this->dataProvider->getResponse(
            $this->configurationBag->getContentClass(),
            null,
            ApiHelper::getSerializeGroupsFromRequest($request),
            $cacheTags
        );
    }
}
