<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Media;

use Exception;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use RuntimeException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Manager\MediaManager;
use OpenApi\Annotations as OA;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use SG\CmsBundle\Api\Entity\Media;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Route("/medias", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a Media resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="The created media",
     *     @Model(type=Media::class, groups={"get"})
     * )
     * @OA\Parameter(
     *      name="provider",
     *      in="query",
     *      required=false,
     *      description="The uploaded media provider",
     * )
     * OA\Parameter(
     *      name="file",
     *      in="formData",
     *      required=true,
     *      type="file",
     *      description="The uploaded media file"
     * )
     * @OA\Tag(name="Medias")
     *
     * @param Request            $request
     * @param MediaManager       $mediaManager
     * @param FilesystemOperator $localStorage
     *
     * @return Response
     * @throws FilesystemException
     * @throws Exception
     */
    public function __invoke(Request $request, MediaManager $mediaManager, FilesystemOperator $localStorage): Response
    {
        /** @var UploadedFile $uploadedFile */
        $uploadedFile = $request->files->get('file');

        if (null === $uploadedFile) {
            throw new RuntimeException("Error while uploading the media");
        }

        /** @var string $provider */
        $provider = $request->query->has('provider') ? $request->query->get('provider') : 'image';

        $originalName = rawurldecode($uploadedFile->getClientOriginalName());
        $uniqueFilename = MediaManager::getUniqueFilename($originalName);

        $localStorage->write($uniqueFilename, file_get_contents($uploadedFile->getPathname()));

        $media = $mediaManager->createMedia($provider, $uniqueFilename, $originalName);

        return new JsonResponse($this->serializer->serialize($media, "json", ['groups' => 'get']), Response::HTTP_CREATED, [], true);
    }
}
