<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Media;

use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Media\Provider\Pool;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetMediaAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/medias/reference/{id}/{filename}", requirements={"filename": ".+"}, methods={"GET"}, defaults={"filename"=""})
 */
class GetMediaAction extends AbstractItemAction
{
    /**
     * Retrieves a Media resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Media resource response.",
     *     @Model(type=Media::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Media resource id"
     * )
     * @OA\Parameter(
     *     name="filename",
     *     in="path",
     *     required=false,
     *     description="The displayed filename"
     * )
     * @OA\Tag(name="Medias")
     *
     * @param Request     $request
     * @param Pool        $pool
     * @param string      $id
     * @param string|null $filename
     *
     * @return JsonResponse|StreamedResponse|Response
     */
    public function __invoke(Request $request, Pool $pool, string $id, ?string $filename = null): JsonResponse|StreamedResponse|Response
    {
        $media = $this->getRepository(Media::class)->findOneBy(['id'=>$id]);
        if (!$media) {
            return new JsonResponse('');
        }
        $provider = $pool->getProvider($media->getProviderName());
        $fileData = $provider->getReferenceMedia($media);
        return new StreamedResponse(function () use ($fileData) {
            fpassthru($fileData['stream']);
            exit();
            }, Response::HTTP_OK, [
                        'Content-Transfer-Encoding', 'binary',
                        'Content-Type' => preg_match("/.svg/", $filename) ? "image/svg+xml" : $fileData['mimetype'],
                        'Content-Length' => $fileData['size'],
                    ]);
    }
}
