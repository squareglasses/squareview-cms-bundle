<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Media;

use Exception;
use SG\CmsBundle\Api\Contracts\ModuleInterface;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MediaManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Common\Media\Exception\MediaNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/medias/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a Media by id.
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Media has been deleted"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Media id to delete"
     * )
     * @OA\Tag(name="Medias")
     *
     * @param Request         $request
     * @param MediaManager    $mediaManager
     * @param HasMediaManager $hasMediaManager
     * @param ModuleManager   $moduleManager
     * @param string          $id
     *
     * @return JsonResponse
     */
    public function __invoke(
        Request $request,
        MediaManager $mediaManager,
        HasMediaManager $hasMediaManager,
        ModuleManager $moduleManager,
        string $id
    ): JsonResponse {
        $media = $this->entityManager
            ->getRepository(Media::class)
            ->find($id);

        if (!is_a($media, Media::class)) {
            throw new MediaNotFoundException($id);
        }
        // check if media is used in resources
        $resourcesHasMedia = $this->entityManager
            ->getRepository(ResourceHasMedia::class)
            ->findBy(['media' => $media]);

        if (!empty($resourcesHasMedia)) {
            $blockingResources = [];
            $nonBlockingResources = [];

            foreach ($resourcesHasMedia as $resourceHasMedia) {
                $resource = $hasMediaManager->findResource($resourceHasMedia);

                if ($resource instanceof ModuleInterface) {
                    $resource = $moduleManager->findResource($resource);
                }

                //if no resource found, remove resourceHasMedia line to clean DB
                if (!$resource) {
                    $this->entityManager->remove($resourceHasMedia);
                } elseif ($resource instanceof Version) {
                    if ($resource->isCurrentVersion() || $resource->isCurrentDraft()) {
                        $blockingResources[] = (string)$resource;
                    } else {
                        $nonBlockingResources[$resourceHasMedia->getId()] = $resource;
                    }
                } else {
                    $blockingResources[] = (string)$resource;
                }
            }

            if (!empty($blockingResources)) {
                return new JsonResponse($this->serializer->serialize(array_values(array_unique($blockingResources)), "json", ['groups' => 'get']), Response::HTTP_CONFLICT, [], true);
            }

            if (!empty($nonBlockingResources)) {
                // There's no blockingResources so nonBlockingResources = resourcesHasMedia
                // We can loop through resourcesHasMedia to associate versionable with its resourceHasMedia object
                foreach ($resourcesHasMedia as $resourceHasMedia) {

                    // Store changes in content_version field for versionable resource
                    $versionable = $nonBlockingResources[$resourceHasMedia->getId()];
                    $changes = $versionable->getChanges() ?: [];
                    $datas = [
                        "action" => "media_deleted",
                        "info" => [
                            "media" => $media->getId(),
                        ]
                    ];
                    if ($resourceHasMedia->getResourceClass() === Module::class) {
                        $datas["info"]["module"] = $resourceHasMedia->getResourceIdentifier();
                    }
                    $changes[] = $datas;
                    $versionable->setChanges($changes);
                    $this->entityManager->persist($versionable);

                    // Delete resource_has_media line
                    $this->entityManager->remove($resourceHasMedia);
                }
            }
        }

        // Then proceed with media deletion
        try {
            $mediaManager->deleteMedia($media);
            $this->entityManager->flush();
            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], $e->getCode() ?: Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
