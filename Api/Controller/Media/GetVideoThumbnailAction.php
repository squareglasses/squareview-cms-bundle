<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Media;

use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Media\Provider\Pool;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetVideoThumbnailAction
 *
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
#[Route('/video-thumbnails/{filename}', requirements: ['filename' => '.+'], defaults: ['filename' => ''], methods: ['GET'])]
class GetVideoThumbnailAction extends AbstractItemAction
{
    /**
     * Find a video thumbnail file
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Thumbnail file",
     * )
     * @OA\Parameter(
     *      name="filename",
     *      in="path",
     *      description="The displayed filename",
     * )

     * @OA\Tag(name="Medias")
     *
     *
     * @param FilesystemOperator $localStorage
     * @param string $filename
     * @return JsonResponse|StreamedResponse|Response
     * @throws FilesystemException
     */
    public function __invoke(FilesystemOperator $localStorage, string $filename): JsonResponse|StreamedResponse|Response
    {
        if (!$localStorage->has("video_thumbnails/".$filename)) {
            return new JsonResponse('');
        }

        $thumbnailLocation = "video_thumbnails/".$filename;
        $fileData = [
            'stream' => $localStorage->readStream($thumbnailLocation),
            'mimetype' => $localStorage->mimeType($thumbnailLocation),
            'size' => $localStorage->fileSize($thumbnailLocation)
        ];

        return new StreamedResponse(function () use ($fileData) {
            fpassthru($fileData['stream']);
            exit();
            }, Response::HTTP_OK, [
                        'Content-Transfer-Encoding', 'binary',
                        'Content-Type' => $fileData['mimetype'],
                        'Content-Length' => $fileData['size'],
                    ]);
    }
}
