<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Media;

use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/medias/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Media resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Media resource response.",
     *     @Model(type=Media::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Media resource id"
     * )
     * @OA\Tag(name="Medias")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $resource = $this->getRepository(Media::class)->findOneBy(['id' => $id]);
        if (!$resource) {
            $resource = $this->getRepository(Media::class)->findOneBy(['providerReference' => $id]);
            if (!$resource) {
                throw new ResourceNotFoundException("No Media found for id/filename '".$id."'");
            }
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
