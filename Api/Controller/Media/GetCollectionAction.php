<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Media;

use SG\CmsBundle\Api\Doctrine\Orm\Filter\OrderFilter;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/medias", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Media resources.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Media collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Media::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="page",
     *     in="query",
     *     @OA\Schema(type="integer"),
     *     description="The current page of the pagination. If no parameter given, result will not be paginated",
     *     required=false
     * )
     * @OA\Parameter(
     *     name="items",
     *     in="query",
     *     @OA\Schema(type="integer"),
     *     description="The number of items per page for pagination"
     * )
     * @OA\Tag(name="Medias")
     *
     * @param Request             $request
     * @param PaginationExtension $paginationExtension
     *
     * @return Response
     */
    public function __invoke(Request $request, PaginationExtension $paginationExtension): Response
    {
        $page = $request->query->has('page');

        if ($page) {
            $itemsPerPage = $request->query->has('items') ? $request->query->get('items') : 20;
            //$resources = $this->dataProvider->getCollection(Media::class);
            $this->addExtension($paginationExtension, [
                'items_per_page' => $itemsPerPage
            ]);
            $this->addFilter(OrderFilter::class, [
                'updatedAt' => 'desc'
            ]);
        }

        return $this->dataProvider->getResponse(
            Media::class,
            null,
            ['get'],
            [],
            false
        );
    }
}
