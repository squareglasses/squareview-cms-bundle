<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Language;

use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\BooleanFilter;
use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * List the languages.
 *
 * @Route("/languages", methods={"GET"})
 *
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Language resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="Language collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Language::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="enabled",
     *     in="query",
     *     @OA\Schema(type="boolean"),
     *     description="The enabled state"
     * )
     * @OA\Tag(name="Languages")
     * @return Response
     */
    public function __invoke(): Response
    {
        $this->addFilter(BooleanFilter::class, ['enabled' => SearchFilterInterface::STRATEGY_EXACT]);
        return $this->dataProvider->getResponse(
            Language::class,
            null,
            ['groups' => 'get']
        );
    }
}
