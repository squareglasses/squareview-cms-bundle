<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Language;

use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Repository\LanguageRepository;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @Route("/languages/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Language resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Language resource response.",
     *     @Model(type=Language::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Language resource id"
     * )
     * @OA\Tag(name="Languages")
     *
     * @param Request $request
     * @param string $id
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $resource = $this->getRepository(Language::class)->findOneBy(['id' => $id]);
        if (!$resource) {
            throw new ResourceNotFoundException("No Language found for id '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", ['groups' => 'get']), 200, [], true);
    }
}
