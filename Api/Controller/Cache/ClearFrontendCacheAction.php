<?php
declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Cache;

use App\Entity\Contact;
use SG\CmsBundle\Common\HttpClient\FrontendApiClient;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * @Route("/cache/clear-frontend", methods={"POST"})
 */
class ClearFrontendCacheAction extends AbstractController
{
    /**
     * @param FrontendApiClient $client
     */
    public function __construct(protected FrontendApiClient $client)
    {
    }

    /**
     * Clear frontend's cache.
     *
     * @OA\Response(
     *     response=200,
     *     description="Content resource response."
     * )
     * @OA\Tag(name="Cache")
     *
     * @param Request $request
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    public function __invoke(Request $request): Response
    {
        $this->client->request(
            'POST',
            "api/cache/clear"
        );

        return new JsonResponse("cleared");
    }
}
