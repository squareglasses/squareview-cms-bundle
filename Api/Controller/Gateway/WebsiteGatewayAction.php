<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Gateway;

use App\Entity\Website;
use Exception;
use JsonException;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Serializer\GatewayResource;
use SG\CmsBundle\Api\Serializer\ResourceSerializer;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use SG\CmsBundle\Api\Contracts\Publishable;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class WebsiteGatewayAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class WebsiteGatewayAction extends AbstractItemAction
{
    /**
     * Retrieves a Website resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Gateway resource response."
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="path",
     *     description="The Gateway locale"
     * )
     * @OA\Parameter(
     *     name="slug",
     *     in="path",
     *     description="The Website identifier"
     * )
     * @OA\Tag(name="Gateway")
     *
     * @param Request                             $request
     * @param string                              $locale
     * @param string                              $slug
     * @param ResourceSerializer                  $resourceSerializer
     * @param PublicationManager                  $publicationManager
     *
     * @return Response
     *
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function __invoke(
        Request $request,
        string $locale,
        string $slug,
        ResourceSerializer $resourceSerializer,
        PublicationManager $publicationManager,
        HasMediaManager $hasMediaManager
    ): Response {
        /** @var Publishable $resource */
        $website = $this->getRepository(Website::class)
            ->findOneBy(['slug' => $slug]);
        if (!$website) {
            throw new ResourceNotFoundException("Website '".$slug."' not found.");
        }
        $hasMediaManager->loadMedias($website);
        // Website resource is serialized
        $serializedResource = $resourceSerializer->serialize($website, $locale, ["groups" => ["get", "medias"]]);
        // We compute translations with layout ones
        $translations = array_merge(
            $serializedResource['translations'] ?? [],
                $publicationManager->getLayoutTranslations($website, $locale)
        );

        // We create the gateway resource for result
        $gatewayResource = (new GatewayResource())
            ->setResourceClass($website->getResourceClass())
            ->setResourceId($website->getResourceIdentifier())
            ->setLocale($locale)
            ->setResource($serializedResource['resource'])
            ->setTranslations($translations);

        $headers = [];
        if ($website instanceof Cacheable) {
            $headers = [
                'cache-tags' => implode(',', $this->tagExtractor->generateTags($website))
            ];
        }

        return new JsonResponse($this->serializer->serialize($gatewayResource, "json", ['groups' => ['get', 'metatags']]), 200, $headers, true);
    }
}
