<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Gateway;

use Doctrine\ORM\NonUniqueResultException;
use JsonException;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Enableable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Serializer\GatewayResource;
use SG\CmsBundle\Api\Serializer\ResourceSerializer;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use SG\CmsBundle\Api\Contracts\Publishable;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;

/**
 * Class GatewayAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GatewayAction extends AbstractItemAction
{
    /**
     * Retrieves a CmsResourceInterface resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Gateway resource response."
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="path",
     *     description="The Gateway locale"
     * )
     * @OA\Parameter(
     *     name="path",
     *     in="path",
     *     description="The Gateway path"
     * )
     * @OA\Parameter(
     *     name="preview",
     *     in="query",
     *     description="The draft hash"
     * )
     * @OA\Tag(name="Gateway")
     *
     * @param Request $request
     * @param string $locale
     * @param string $path
     * @param ResourceSerializer $resourceSerializer
     * @param PublicationManager $publicationManager
     *
     * @return Response
     *
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     * @throws NonUniqueResultException
     */
    public function __invoke(
        Request $request,
        string $locale,
        string $path,
        ResourceSerializer $resourceSerializer,
        PublicationManager $publicationManager
    ): Response {

        // Replace ? hook by the right thing
        $path = str_replace("|qtag}", "?}", $path);

        /** @var Route $route */
        $route = $this->getRepository(Route::class)
            ->findOneBy(["locale" => $locale, "path" => $path]);
        if (!$route) {
            throw new ResourceNotFoundException("No Route found for locale '".$locale."' and path '".$path."'.");
        }

        // Route is matching, retrieving the associated resource
        if (!$route->getResourceIdentifier() || !$route->getResourceClass()) {
            throw new ResourceNotFoundException("Route '".$route->getName()."' has no resource configured.");
        }

        /** @var Publishable|CmsResourceInterface $resource */
        $resource = $this->getRepository($route->getResourceClass())
            ->findOneBy(['id' => $route->getResourceIdentifier()]);
        if (!$resource) {
            throw new ResourceNotFoundException("Route '".$route->getName()."' is associated to unknown resource (".$route->getResourceClass()."/".$route->getResourceIdentifier().").");
        }

        if ($resource instanceof Enableable || method_exists($resource, 'isEnabled')) {
            if ($resource->isEnabled() === false && !$request->query->has('preview')) {
                throw new ResourceNotFoundException("Resource '".$resource->getResourceIdentifier()."' is disabled.");
            }
        }

        // Resource is serialized
        $context = $request->query->get('preview')
            ? ["draft" => $request->query->get('preview')]
            : ["published" => true];
        $serializedResource = $resourceSerializer->serialize($resource, $locale, $context);

        // We compute translations with layout ones
        $translations = array_merge(
            $serializedResource['translations'],
            $publicationManager->getGuideTranslationsForResource($resource, $locale)
        );

        // We create the gateway resource for result
        $gatewayResource = (new GatewayResource())
            ->setResourceClass($resource->getResourceClass())
            ->setResourceId($resource->getResourceIdentifier())
            ->setLocale($locale)
            ->setResource($serializedResource['resource'])
            ->setTranslations($translations);

        $headers = [];

        if ($request->query->get('preview') && !$resourceSerializer->hasCorrectDraftHash($resource, $locale, $context)) {
            $headers['sg-preview-hash'] = 'invalid';
        } else {
            $headers['sg-preview-hash'] = 'valid';
        }

        if ($resource instanceof Cacheable) {
            $headers['cache-tags'] = implode(',', $this->tagExtractor->generateTags($resource));
        }
        return new JsonResponse(
            $this->serializer->serialize(
                $gatewayResource,
                "json",
                ['groups' => ['get', 'medias', 'galleries', 'modules', 'metatags']]
            ),
            Response::HTTP_OK,
            $headers,
            true
        );
    }
}
