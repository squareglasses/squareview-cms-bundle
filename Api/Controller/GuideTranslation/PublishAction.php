<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\GuideTranslation;

use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class PublishAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/guide_translations/publish/{locale}/{discriminator}", methods={"POST"})
 */
class PublishAction extends AbstractItemAction
{
    private PublicationManager $publicationManager;
    private CacheResourcesHolder $cacheResourcesHolder;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $publicationEntityManager,
        ConfigurationBag $configurationBag,
        ValidatorInterface $validator,
        TranslationManager $translationManager,
        TagExtractor $tagExtractor,
        PublicationManager $publicationManager,
        CacheResourcesHolder $cacheResourcesHolder
    ) {
        parent::__construct(
            $serializer,
            $publicationEntityManager,
            $configurationBag,
            $validator,
            $translationManager,
            $tagExtractor
        );

        $this->publicationManager = $publicationManager;
        $this->cacheResourcesHolder = $cacheResourcesHolder;
    }

    /**
     * Publish a TranslationsPublication resource.
     *
     * @OA\Response(
     *     response=201,
     *     description="Returns 201 when guide is published."
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="path",
     *     description="The guide translation publication locale"
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The guide translation discriminator (= guide identifier)"
     * )
     * @OA\Tag(name="GuideTranslation")
     *
     * @param Request $request
     * @param string  $locale
     * @param string  $discriminator
     *
     * @return Response
     *
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws JsonException
     */
    public function __invoke(Request $request, string $locale, string $discriminator): Response
    {
        $publication = $this->publicationManager->publishLayoutGuideTranslations($discriminator, $locale);

        $this->cacheResourcesHolder->addTags([$discriminator]); //Invalidate all resources with this guide identifier (discriminator)
        $this->entityManager->flush();

        return new JsonResponse(['publication_date' => $publication->getCreatedAt() ? $publication->getCreatedAt()->format('c') : null], 201);
    }
}
