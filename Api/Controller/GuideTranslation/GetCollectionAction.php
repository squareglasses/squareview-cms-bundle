<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\GuideTranslation;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use LogicException;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Entity\TranslationsPublication;
use SG\CmsBundle\Api\Translation\Model\Catalog;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class GetCollectionAction
 * List the translations of a guide.
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/guide_translations/{type}/{discriminator}", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of translations for a guide (types: resource_guide or layout_guide)
     *
     * @OA\Response(
     *     response=200,
     *     description="Translations collection response."
     * )
     * @OA\Parameter(
     *     name="type",
     *     in="path",
     *     description="The type of translations: resource_guide or layout_guide"
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The translation catalog's discriminator. Resource discriminator if resource_guide or Guide name if layout_guide"
     * )
     * @OA\Parameter(
     *     name="zone",
     *     in="query",
     *     description="The zone of requested translations. If empty, get all zones."
     * )
     * @OA\Parameter(
     *     name="resourceId",
     *     in="query",
     *     description="The resourceId if resource_guide, if layout_guide leave empty"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The locale. If empty, get all locales"
     * )
     * @OA\Tag(name="GuideTranslation")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param TranslationManager     $translationManager
     * @param ResourceGuideProvider  $guideProvider
     * @param string                 $type
     * @param string                 $discriminator
     *
     * @return Response
     *
     * @throws NonUniqueResultException
     * @throws ResourceGuideNotFoundException
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager, TranslationManager $translationManager, ResourceGuideProvider $guideProvider, string $type, string $discriminator): Response
    {
        $locale = $this->getLocale($request);
        $zone = $request->query->get('zone');

        switch ($type) {
            case TranslationKey::TYPE_LAYOUT_GUIDE:
                $guide = $guideProvider->getGuide($discriminator);
                if (null === $guide) {
                    throw new RuntimeException("Guide not found for discriminator '".$discriminator."'.");
                }
                try {
                    $guideTranslations = $translationManager->getLayoutGuideTranslations($guide);
                } catch (Exception $e) {
                    throw new RuntimeException("Error getting translations from SquareTranslate : ".$e->getMessage());
                }
                $translations = $translationManager->formatTranslationsAsArray($guideTranslations, $locale, $zone, true);

                $activePublication = $entityManager->getRepository(TranslationsPublication::class)->findActive($locale, $discriminator);

                return $this->json([
                    'translations' => $translations,
                    'lastPublishedAt' => ($activePublication && $activePublication->getCreatedAt()) ? $activePublication->getCreatedAt()->format('c') : null
                ]);

//            case TranslationKey::TYPE_RESOURCE_GUIDE: //This is currently not used by the backend, as translations are added to the content on the content's GetItem, to better handle version hashes
//                $resourceId = $request->query->get('resourceId');
//                if (!$resourceId) {
//                    throw new BadRequestException('GET parameter resourceId missing in query for type = resource_guide');
//                }
//                $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
//                if (!$resourceClass) {
//                    throw new BadRequestException('No resource class found for discriminator '.$discriminator.'. Make sure it is defined in the the api\'s cms.yaml');
//                }
//                $resource = $entityManager->getRepository($resourceClass)
//                    ->findOneBy(['id' => $resourceId]);
//
//                if ($resource instanceof Versionable) {
//                    $resource = $resource->getDraftVersion();
//                }
//
//                if ($resource instanceof Guidable) {
//                    try {
//                        $guideTranslations = $translationManager->getResourceGuideTranslations($resource);
//                    } catch (Exception $e) {
//                        throw new RuntimeException("Error getting translations from SquareTranslate : ". $e->getMessage());
//                    }
//                    $translations = $translationManager->formatTranslationsAsArray($guideTranslations, $locale, $zone, true);
//                } else {
//                    throw new LogicException("Given resource is not Guidable, it does not have any resource_guide translations.");
//                }
//                return $this->json(['translations' => $translations]);


            default:
                throw new BadRequestException('The "type" parameter in path must be one of: resource_guide or layout_guide.');
        }
    }
}
