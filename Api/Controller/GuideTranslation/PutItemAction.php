<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\GuideTranslation;

use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use RuntimeException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Translation\Exception\InvalidCatalogTypeException;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/guide_translations/{type}/{discriminator}/{zone}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a Content resource.
     *
     * @OA\Parameter(
     *     name="type",
     *     example="layout_guide",
     *     in="path",
     *     description="The type of translation: resource_guide or layout_guide"
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     example="layout",
     *     in="path",
     *     description="The translation catalog's discriminator. Resource discriminator if resource_guide or Guide name if layout_guide"
     * )
     * @OA\Parameter(
     *     name="zone",
     *     example="header",
     *     in="path",
     *     description="The zone of updated translation."
     * )
     * @OA\Parameter(
     *     name="resourceId",
     *     in="query",
     *     description="The resourceId if resource_guide, if layout_guide leave empty"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     example="fr",
     *     in="query",
     *     description="The locale. If empty, get all locales"
     * )
     *
     * @OA\RequestBody (
     *     description="The TranslationKey json",
     *     @Model(type=TranslationKey::class, groups={"put"})
     * )
     *
     * @OA\Tag(name="GuideTranslation")
     *
     * @param Request                $request
     * @param TranslationManager     $translationManager
     * @param EntityManagerInterface $entityManager
     * @param string                 $type
     * @param string                 $discriminator
     * @param string                 $zone
     *
     * @return Response
     * @throws InvalidCatalogTypeException
     * @throws JsonException
     */
    public function __invoke(Request $request, TranslationManager $translationManager, EntityManagerInterface $entityManager, string $type, string $discriminator, string $zone): Response
    {
        $requestContent = $request->getContent();
        $data = json_decode($requestContent, true, 512, JSON_THROW_ON_ERROR);
        $locale = $request->query->get('locale');

        switch ($type) {
            case TranslationKey::TYPE_RESOURCE_GUIDE:
                $resourceId = $request->query->get('resourceId');
                if (!$resourceId) {
                    throw new BadRequestException('GET parameter resourceId missing in query for type = resource_guide');
                }
                $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
                if (!$resourceClass) {
                    throw new BadRequestException('No resource class found for discriminator '.$discriminator.'. Make sure it is defined in the the api\'s cms.yaml');
                }
                $resource = $entityManager->getRepository($resourceClass)
                    ->findOneBy(['id' => $resourceId]);
                if ($resource instanceof Versionable) {
                    $resource = $resource->getDraftVersion();
                }
                try {
                    $translationManager->translate($resource, $data['key'], $data['message'], $locale, $zone, TranslationKey::TYPE_RESOURCE_GUIDE);
                } catch (TranslationClientException|TransportExceptionInterface $e) {
                    throw new RuntimeException('Error updating translation : '.$e->getMessage());
                }
            break;
            case TranslationKey::TYPE_LAYOUT_GUIDE:
                try {
                    $translationManager->saveTranslation($data['key'], $data['message'], $locale, $zone, TranslationKey::TYPE_LAYOUT_GUIDE, $discriminator);
                } catch (TranslationClientException|TransportExceptionInterface $e) {
                    throw new RuntimeException('Error updating translation in SquareTranslate : '.$e->getMessage());
                }
                break;
        }

        return new JsonResponse(null, 200);
    }
}
