<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class AbstractItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractItemAction extends AbstractController
{
    /**
     * @param SerializerInterface    $serializer
     * @param EntityManagerInterface $entityManager
     * @param ConfigurationBag       $configurationBag
     * @param ValidatorInterface     $validator
     * @param TranslationManager     $translationsManager
     * @param TagExtractor           $tagExtractor
     */
    public function __construct(protected SerializerInterface $serializer, protected EntityManagerInterface $entityManager, protected ConfigurationBag $configurationBag, protected ValidatorInterface $validator, protected TranslationManager $translationsManager, protected TagExtractor $tagExtractor)
    {
    }

    /**
     * @param string $className
     *
     * @return ObjectRepository
     */
    protected function getRepository(string $className): ObjectRepository
    {
        return $this->entityManager->getRepository($className);
    }

    /**
     * @param object $object
     *
     * @return void
     */
    protected function persist(object $object): void
    {
        $this->entityManager->persist($object);
    }

    /**
     * @param object $object
     *
     * @return void
     */
    protected function persistAndFlush(object $object): void
    {
        $this->persist($object);
        $this->entityManager->flush();
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getLocale(Request $request): string
    {
        return $request->get('locale') ?: $request->getLocale();
    }
}
