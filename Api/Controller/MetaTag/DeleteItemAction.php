<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MetaTag;

use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/meta_tags/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a Module by id.
     *
     * @OA\Response(
     *     response=204,
     *     description="MetaTag has been deleted"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The MetaTag id"
     * )
     * @OA\Tag(name="MetaTags")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $metaTag = $this->getRepository(MetaTag::class)->find($id);
        if (!$metaTag) {
            throw new ResourceNotFoundException("No MetaTag found for id '".$id."'");
        }

        $this->entityManager->remove($metaTag);
        $this->entityManager->flush();

        return new JsonResponse(null, 204);
    }
}
