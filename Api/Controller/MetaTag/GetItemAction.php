<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MetaTag;

use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/meta_tags/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a MetaTag resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="MetaTag resource response.",
     *     @Model(type=MetaTag::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The MetaTag resource id"
     * )
     * @OA\Tag(name="MetaTags")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $resource = $this->getRepository(MetaTag::class)->findOneBy(['id' => $id]);
        if (!$resource) {
            throw new ResourceNotFoundException("No MetaTag found for id '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", ['groups' => 'get']), 200, [], true);
    }
}
