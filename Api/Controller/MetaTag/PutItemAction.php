<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MetaTag;

use DateTime;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use SG\CmsBundle\Api\Contracts\Translatable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use SG\CmsBundle\Api\Entity\MetaTag;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/meta_tags/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a MetaTag.
     *
     * @OA\Response(
     *     response=200,
     *     description="Updated draft hashes of the metaTaggable resource."
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The metatag id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\RequestBody (
     *     description="The metatag json",
     *     @Model(type=MetaTag::class, groups={"put"})
     * )
     * @OA\Tag(name="MetaTags")
     *
     * @param Request        $request
     * @param MetaTagManager $metaTagManager
     * @param string         $id
     *
     * @return JsonResponse
     * @throws JsonException
     */
    public function __invoke(Request $request, MetaTagManager $metaTagManager, string $id): JsonResponse
    {
        /** @var Translatable|MetaTag $metaTag */
        $metaTag = $this->entityManager
            ->getRepository(MetaTag::class)
            ->findOneBy(['id' => $id]);
        if (!is_a($metaTag, MetaTag::class)) {
            throw new AccessDeniedHttpException('No metaTag found for id "' . $id . '".');
        }

        $metaTaggable = $metaTagManager->findResource($metaTag);
        if (null === $metaTaggable) {
            throw new AccessDeniedHttpException('No metaTaggable resource found for class "'.$metaTag->getResourceClass().'" and id "' . $metaTag->getResourceIdentifier() . '".');
        }

        $requestContent = $request->getContent();

        $this->serializer->deserialize($requestContent, MetaTag::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $metaTag,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($metaTag);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());
        $data = json_decode($requestContent, true, 512, JSON_THROW_ON_ERROR);
        $this->persistAndFlush($metaTag);

        if (isset($data['hash']) && $metaTaggable instanceof Version) {
            $draftHashes = $metaTaggable->getDraftHashes();
            $draftHashes[$locale] = [
                'hash' => $data['hash'],
                'date' => new DateTime()
            ];
            $metaTaggable->setDraftHashes($draftHashes);
            $this->persistAndFlush($metaTaggable);

            return new JsonResponse($metaTaggable->getDraftHashes(), 200);
        }

        return new JsonResponse($metaTaggable, 200);
    }
}
