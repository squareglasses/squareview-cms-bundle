<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MetaTag;

use DateTime;
use Exception;
use JsonException;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use SG\CmsBundle\Api\Entity\MetaTag;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/meta_tags/{discriminator}/{id}", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a MetaTag.
     *
     * @OA\Response(
     *     response=201,
     *     description="The created MetaTag resource",
     *     @Model(type=MetaTag::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The metaTaggable resource's discriminator"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The metaTaggable resource identifier"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The locale of the metaTag"
     * )
     * @OA\RequestBody (
     *     description="The metaTag json",
     *     @Model(type=MetaTag::class, groups={"post"})
     * )
     * @OA\Tag(name="MetaTags")
     *
     * @param Request $request
     * @param string  $discriminator
     * @param string  $id
     *
     * @return JsonResponse
     * @throws JsonException
     * @throws Exception
     */
    public function __invoke(Request $request, string $discriminator, string $id): JsonResponse
    {
        $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
        if (!$resourceClass) {
            throw new AccessDeniedHttpException('No discriminator configured for "'.$discriminator.'" under "sg_cms.api.discriminators".');
        }

        /** @var MetaTaggable $metaTaggable */
        $metaTaggable = $this->entityManager
            ->getRepository($resourceClass)
            ->findOneBy(['id' => $id]);

        if (null === $metaTaggable) {
            throw new AccessDeniedHttpException('No MetaTaggable resource found for class "'.$resourceClass.'" and id "' . $id . '".');
        }

        if ($metaTaggable instanceof Versionable) {
            $metaTaggable = $metaTaggable->getDraftVersion();
        }

        /** @var MetaTag $metaTag */
        $metaTag = $this->serializer->deserialize($request->getContent(), MetaTag::class, 'json', [
            'groups' => 'post'
        ]);

        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());

        $metaTag
            ->setResourceClass($metaTaggable->getResourceClass())
            ->setResourceIdentifier($metaTaggable->getResourceIdentifier())
            ->setLocale($locale);

        $errors = $this->validator->validate($metaTag);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->persistAndFlush($metaTag);

        if (isset($data['hash'])) {
            $draftHashes = $metaTaggable->getDraftHashes();
            $draftHashes[$locale] = [
                'hash' => $data['hash'],
                'date' => new DateTime()
            ];
            $metaTaggable->setDraftHashes($draftHashes);
            $this->persistAndFlush($metaTaggable);
        }

        return new JsonResponse($this->serializer->serialize($metaTag, "json", ['groups' => 'get']), 201, [], true);
    }
}
