<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MetaTag;

use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/meta_tags", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of MetaTag resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="MetaTag collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=MetaTag::class, groups={"get"}))
     *     )
     * )
     * @OA\Tag(name="MetaTags")
     *
     * @return Response
     */
    public function __invoke(): Response
    {
        $resources = $this->dataProvider->getCollection(MetaTag::class);

        return new JsonResponse($this->serializer->serialize($resources, "json", ['groups' => 'get']), 200, [], true);
    }
}
