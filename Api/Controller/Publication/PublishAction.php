<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Publication;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\Mapping\MappingException;
use Exception;
use ReflectionException;
use SG\CmsBundle\Api\Contracts\Cacheable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Manager\RouteManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class PublishAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/publish/{locale}/{discriminator}/{id}", methods={"PUT"})
 */
class PublishAction extends AbstractItemAction
{
    private PublicationManager $versionableManager;
    private CacheResourcesHolder $cacheResourcesHolder;
    private RouteManager $routeManager;

    public function __construct(
        SerializerInterface    $serializer,
        EntityManagerInterface $publicationEntityManager,
        ConfigurationBag       $configurationBag,
        ValidatorInterface     $validator,
        TranslationManager     $translationManager,
        PublicationManager     $versionableManager,
        TagExtractor           $tagExtractor,
        CacheResourcesHolder   $cacheResourcesHolder,
        RouteManager           $routeManager
    ) {
        parent::__construct(
            $serializer,
            $publicationEntityManager,
            $configurationBag,
            $validator,
            $translationManager,
            $tagExtractor
        );
        $this->versionableManager   = $versionableManager;
        $this->cacheResourcesHolder = $cacheResourcesHolder;
        $this->routeManager         = $routeManager;
    }

    /**
     * Publish a Versionable resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Versionable resource response."
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The versionable resource id"
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The versionable resource discriminator"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="path",
     *     description="The versionable resource locale"
     * )
     * @OA\Tag(name="Publications")
     *
     * @param Request $request
     * @param string  $locale
     * @param string  $discriminator
     * @param string  $id
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ReflectionException
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     * @throws NonUniqueResultException
     * @throws MappingException
     * @throws Exception
     */
    public function __invoke(Request $request, string $locale, string $discriminator, string $id): Response
    {
        $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
        if (!$resourceClass) {
            throw new AccessDeniedHttpException('No discriminator configured for "'.$discriminator.
                '" under "sg_cms.api.discriminators".');
        }
        /** @var Versionable $versionable */
        $versionable = $this->entityManager
            ->getRepository($resourceClass)
            ->find($id);
        if (!$versionable) {
            throw new AccessDeniedHttpException('No versionable found for id "'.$id.
                '" and class "'.$resourceClass.'".');
        }
        if (!$versionable instanceof Versionable) {
            throw new AccessDeniedHttpException('Class "'.$resourceClass.'" does not implement Versionable".');
        }

        $firstPublication = false;
        if (null === $versionable->getCurrentVersion($locale)) {
            $firstPublication = true;
        }

        $this->versionableManager->publish(
            $versionable,
            $locale,
            $versionable->getDraftVersion()->getHashByLocale($locale)
        );

        $this->entityManager->refresh($versionable);

        // For publishable entities, cache invalidation is done manually, we don't need to empty the cache on every flush
        if ($versionable instanceof Cacheable) {
            $this->cacheResourcesHolder->addResource($versionable);
            if ($firstPublication) {
                $route = $this->routeManager->getRouteByResource($versionable, $locale);
                if (null !== $route) {
                    $this->cacheResourcesHolder->addRoute($route);
                }
            }
        }

        return new JsonResponse([
            'date' => $versionable->getCurrentVersion($locale)->getUpdatedAt(),
            'hash' => $versionable->getCurrentVersion($locale)->getHash(),
        ], 200);
    }
}
