<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class ChangePasswordAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ChangePasswordAction extends AbstractItemAction
{
    /**
     * Retrieves a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\RequestBody (
     *     description="The User json",
     *     @Model(type=User::class, groups={"put"})
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request  $request
     * @param Security $security
     *
     * @return Response
     * @throws JsonException
     */
    public function __invoke(Request $request, Security $security): Response
    {
        $user = $security->getUser();
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('Invalid credentials.');
        }

        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $user,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'change-password'
        ]);

        $errors = $this->validator->validate($user, null, ["change-password"]);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }
        $this->persistAndFlush($user);

        return new JsonResponse($this->serializer->serialize($user, "json", ['groups' => 'get']), 200, [], true);
    }
}
