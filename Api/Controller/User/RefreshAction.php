<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Security\Core\Security;

/**
 * Class RefreshAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RefreshAction extends AbstractItemAction
{
    protected Security $security;

    /**
     * Retrieves a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request  $request
     * @param Security $security
     *
     * @return Response
     */
    public function __invoke(Request $request, Security $security): Response
    {
        return new JsonResponse($this->serializer->serialize($security->getUser(), "json", ['groups' => 'get']), 200, [], true);
    }
}
