<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\BooleanFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\OrderFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\SearchFilter;
use SG\CmsBundle\Api\Util\ApiHelper;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/users", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of User resources.
     *
     * @OA\Parameter(
     *     name="order[email]",
     *     in="query",
     *     description="Order results by email"
     * )
     * @OA\Parameter(
     *     name="order[name]",
     *     in="query",
     *     description="Order results by name"
     * )
     * @OA\Parameter(
     *     name="order[createdAt]",
     *     in="query",
     *     description="Order results by createdAt datetime"
     * )
     * @OA\Parameter(
     *     name="serialization_groups[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="Array of specific serialization groups"
     * )
     * @OA\Parameter(
     *     name="items",
     *     in="query",
     *     @OA\Schema(type="integer"),
     *     description="The number of items per page for pagination"
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request             $request
     * @param PaginationExtension $paginationExtension
     *
     * @return Response
     */
    public function __invoke(Request $request, PaginationExtension $paginationExtension): Response
    {
        $this->addFilter(SearchFilter::class, ['email' => SearchFilterInterface::STRATEGY_PARTIAL]);
        $this->addFilter(SearchFilter::class, ['personalInfos.firstName' => SearchFilterInterface::STRATEGY_PARTIAL]);
        $this->addFilter(SearchFilter::class, ['personalInfos.lastName' => SearchFilterInterface::STRATEGY_PARTIAL]);
        $this->addFilter(BooleanFilter::class, ['enabled' => SearchFilterInterface::STRATEGY_EXACT]);
        $this->addFilter(OrderFilter::class, [
            'createdAt' => 'desc',
            'email' => 'asc',
            'name' => 'asc'
        ]);

        $itemsPerPage = $request->query->has('items') ? $request->query->get('items') : 20;
        $this->addExtension($paginationExtension, [
            'items_per_page' => $itemsPerPage
        ]);

        return $this->dataProvider->getResponse(
            User::class,
            null,
            ApiHelper::getSerializeGroupsFromRequest($request)
        );
    }
}
