<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Repository\UserRepository;
use SG\CmsBundle\Api\Util\ApiHelper;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The User id or username"
     * )
     * @OA\Parameter(
     *     name="serialization_groups[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     @OA\Items(
     *         type="string"
     *     ),
     *     description="Array of specific serialization groups"
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        /** @var UserRepository $repo */
        $repo = $this->getRepository(User::class);
        $resource = $repo->findByUsernameEmailOrId($id);
        if (!$resource) {
            throw new ResourceNotFoundException("No User found for identifier '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize(
            $resource,
            "json",
            [
                'groups' => ApiHelper::getSerializeGroupsFromRequest($request),
                AbstractObjectNormalizer::ENABLE_MAX_DEPTH => true,
                AbstractNormalizer::CIRCULAR_REFERENCE_HANDLER => function ($object) {
                    return $object->getId();
                }
            ]
        ), 200, [], true);
    }
}
