<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use SG\CmsBundle\Common\HttpClient\FrontendApiClient;
use SG\CmsBundle\Frontend\Security\Util\TokenGenerator;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

/**
 * Class ActivateRequestAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/users/{id}/invite", methods={"GET"})
 */
class InviteAction extends AbstractItemAction
{

    /**
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManager
     * @param ConfigurationBag $configurationBag
     * @param ValidatorInterface $validator
     * @param TranslationManager $translationsManager
     * @param TagExtractor $tagExtractor
     * @param FrontendApiClient $client
     */
    public function __construct(
        protected SerializerInterface $serializer,
        protected EntityManagerInterface $entityManager,
        protected ConfigurationBag $configurationBag,
        protected ValidatorInterface $validator,
        protected TranslationManager $translationsManager,
        protected TagExtractor $tagExtractor,
        private FrontendApiClient $client
    )
    {
        parent::__construct($serializer, $entityManager, $configurationBag, $validator, $translationsManager, $tagExtractor);
    }

    /**
     * Send an email for account activation.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The User id"
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request        $request
     * @param string         $id
     * @param TokenGenerator $tokenGenerator
     *
     * @return Response
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function __invoke(Request $request, string $id, TokenGenerator $tokenGenerator): Response
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id'=>$id]);
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('No user found for id "'.$id.'".');
        }

        $user->setRegistrationNotifiedAt(new DateTime());

        if (null === $user->getConfirmationToken()) {
            $user->setConfirmationToken($tokenGenerator->generateToken());
        }
        $this->persistAndFlush($user);

        $options = '';
        if (method_exists(User::class, 'getLocale')) {
            $options = '?_locale='.$user->getLocale();
        }

        $response = $this->client->request(
            'POST',
            "api/activer-un-compte/".$user->getId().$options,
        );

        return new JsonResponse($this->serializer->serialize($user, "json", ['groups' => 'get']), 200, [], true);
    }
}
