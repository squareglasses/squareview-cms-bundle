<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ApiHelper;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Retrieves a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The User id"
     * )
     * @OA\RequestBody (
     *     description="The User json",
     *     @Model(type=User::class, groups={"put"})
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id'=>$id]);
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('No user found for id "'.$id.'".');
        }

        $body = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        if (array_key_exists("groups", $body) && is_string($body["groups"])) {
            $groups = [];
            $groupIds = explode(",", $body["groups"]);
            foreach ($groupIds as $groupId) {
                $groups[] = ["id" => $groupId];
            }
            $body["groups"] = $groups;
        }

        $this->serializer->deserialize(json_encode($body, JSON_THROW_ON_ERROR), User::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $user,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }
        $this->persistAndFlush($user);

        return new JsonResponse($this->serializer->serialize($user, "json", ['groups' => ApiHelper::getSerializeGroupsFromRequest($request)]), 200, [], true);
    }
}
