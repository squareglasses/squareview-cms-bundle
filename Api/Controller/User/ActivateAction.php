<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use DateTime;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ActivateAction extends AbstractItemAction
{
    /**
     * Activate a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\RequestBody (
     *     description="The User json",
     *     @Model(type=User::class, groups={"put"})
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request            $request
     * @param ValidatorInterface $validator
     * @param string             $id
     *
     * @return Response
     * @throws JsonException
     */
    public function __invoke(Request $request, ValidatorInterface $validator, string $id): Response
    {
        $user = $this->entityManager->getRepository(User::class)->findOneBy(['id' => $id]);
        if (!$user instanceof User) {
            throw new AccessDeniedHttpException('No user found for id "'.$id.'".');
        }

        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $user,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => ['reset-password', "activate"]
        ]);

        $errors = $this->validator->validate($user, null, ["reset-password", "activate"]);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }
        $user->setConfirmationToken(null);
        $user->setPasswordRequestedAt(null);
        if (method_exists($user, "setPasswordCreatedAt")) {
            $user->setPasswordCreatedAt(new DateTime());
        }
        $user->setEnabled(true);

        $this->persistAndFlush($user);

        return new JsonResponse($this->serializer->serialize($user, "json", ['groups' => 'get']), 200, [], true);
    }
}
