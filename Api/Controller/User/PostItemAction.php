<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\Group;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/users", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Post a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     *
     * @OA\RequestBody (
     *     description="The User json",
     *     @Model(type=User::class, groups={"post"})
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request $request
     *
     * @return Response
     */
    public function __invoke(Request $request): Response
    {
        //$user = $this->security->getUser();
        $user = new User();
        $context = [
            AbstractNormalizer::OBJECT_TO_POPULATE => $user,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => ['post', 'security']
        ];
        $user = $this->serializer->deserialize($request->getContent(), User::class, 'json', $context);

        $errors = $this->validator->validate($user);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $group = $this->entityManager->getRepository(Group::class)->findOneBy(['slug' => 'utilisateur']);
        if ($group) {
            $user->addGroup($group);
        }

        $this->persistAndFlush($user);

        return new JsonResponse($this->serializer->serialize($user, "json", ['groups' => 'get']), 201, [], true);
    }
}
