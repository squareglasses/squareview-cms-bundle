<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\User;

use App\Entity\User;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemByConfirmationTokenAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/users/by-confirmation-token/{token}", methods={"GET"}, priority=3)
 */
class GetItemByConfirmationTokenAction extends AbstractItemAction
{
    /**
     * Retrieves a User resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="User resource response.",
     *     @Model(type=User::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="token",
     *     in="path",
     *     description="The User confirmation token"
     * )
     * @OA\Tag(name="Users")
     *
     * @param Request $request
     * @param string  $token
     *
     * @return Response
     */
    public function __invoke(Request $request, string $token): Response
    {
        $resource = $this->getRepository(User::class)->findOneBy(['confirmationToken' => $token]);
        if (!$resource) {
            throw new ResourceNotFoundException("No User found for token '".$token."'");
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", ['groups' => ['get', 'security']]), 200, [], true);
    }
}
