<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Module;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use SG\CmsBundle\Api\Entity\Module;

/**
 * Class OrderCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/modules/order/{discriminator}/{resourceIdentifier}/{zone}", methods={"PUT"})
 */
class OrderCollectionAction extends AbstractCollectionAction
{
    /**
     * Updates the order of a resource's modules for a given zone.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Updated draft hashes of the modulable resource."
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The module's resource (modulable) discriminator - this is to check validity"
     * )
     * @OA\Parameter(
     *     name="resourceIdentifier",
     *     in="path",
     *     description="The module's resource (modulable) identifier - this is to check validity"
     * )
     * @OA\Parameter(
     *     name="zone",
     *     in="path",
     *     description="The module's zone in which they are to be ordered - this is to check validity"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\RequestBody (
     *     description="An object with a hash (key 'hash') and an array of module ids (key 'modules') to set positions in that order",
     *     @OA\Schema(type="object",
     *         @OA\Property(property="hash", type="string"),
     *         @OA\Property(property="modules", type="array", @OA\Items(type="string"))
     *     )
     * )
     * @OA\Tag(name="Modules")
     *
     * @param Request                $request
     * @param EntityManagerInterface $entityManager
     * @param ModuleManager          $moduleManager
     * @param string                 $discriminator
     * @param string                 $resourceIdentifier
     * @param string                 $zone
     *
     * @return JsonResponse
     * @throws TranslationClientException|JsonException
     */
    public function __invoke(Request $request, EntityManagerInterface $entityManager, ModuleManager $moduleManager, string $discriminator, string $resourceIdentifier, string $zone): JsonResponse
    {
        $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
        if (!$resourceClass) {
            throw new NotFoundHttpException('No discriminator configured for "'.$discriminator.'" under "sg_cms.api.discriminators".');
        }
        /** @var Modulable $modulable */
        $modulable = $entityManager
            ->getRepository($resourceClass)
            ->find($resourceIdentifier);

        if (!$modulable) {
            throw new NotFoundHttpException('No modulable found for id "'.$resourceIdentifier.'" and class "'.$resourceClass.'".');
        }

        $modulableDraft = $modulable->getDraftVersion();

        if (!$modulableDraft instanceof Modulable) {
            throw new NotFoundHttpException('Class "'.get_class($modulableDraft).'" does not implement Modulable".');
        }

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        if (!isset($data['modules'])) {
            throw new BadRequestHttpException("'modules' key missing, needed to proceed to re-ordering");
        }

        $moduleIds = $data['modules'];
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());
        $moduleManager->loadModules($modulableDraft, $locale, false);

        $existingModulesInZone = array_reduce($modulableDraft->getModules(), static function ($carry, Module $module) use ($zone) {
            if ($module->getZone() === $zone) {
                $carry[$module->getId()] = $module;
            }
            return $carry;
        }, []);

        foreach ($moduleIds as $position => $moduleId) {
            if (!array_key_exists($moduleId, $existingModulesInZone)) {
                throw new NotFoundHttpException("No module with id ".$moduleId." found in resource id ".$resourceIdentifier." and zone ".$zone);
            }
            $module = $existingModulesInZone[$moduleId];
            $module->setPosition($position);
            $entityManager->persist($module);
            unset($existingModulesInZone[$moduleId]);
        }

        if (!empty($existingModulesInZone)) {
            throw new BadRequestHttpException("All modules of resource id ".$resourceIdentifier." and zone ".$zone." were not found in request body. Can't order.");
        }

        $entityManager->flush();

        if (isset($data['hash'])) {
            $draftHashes = $modulableDraft->getDraftHashes();
            $draftHashes[$locale] = [
                'hash' => $data['hash'],
                'date' => new DateTime()
            ];
            $modulableDraft->setDraftHashes($draftHashes);
            $entityManager->persist($modulableDraft);
            $entityManager->flush();
        }

        return new JsonResponse($modulableDraft->getDraftHashes(), Response::HTTP_OK);
    }
}
