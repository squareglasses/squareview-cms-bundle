<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Module;

use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/modules/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a Module by id.
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="Module has been deleted"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Module id"
     * )
     * @OA\Tag(name="Modules")
     *
     * @param Request       $request
     * @param ModuleManager $moduleManager
     * @param string        $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, ModuleManager $moduleManager, string $id): JsonResponse
    {
        $module = $this->getRepository(Module::class)->find($id);
        if (!$module) {
            throw new ResourceNotFoundException("No Module found for id '".$id."'");
        }

        $moduleManager->deleteModule($module);

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }
}
