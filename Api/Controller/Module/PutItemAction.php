<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Module;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\ModuleParameter;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\Generator\TranslationKeyGenerator;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\TagExtractor;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use SG\CmsBundle\Api\Entity\Module;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/modules/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    protected SerializerInterface $serializer;
    protected ConfigurationBag $configurationBag;
    protected EntityManagerInterface $entityManager;
    protected ValidatorInterface $validator;
    protected TranslationManager $translationsManager;
    private ModuleManager $moduleManager;

    /**
     * AbstractItemAction constructor.
     * @param SerializerInterface $serializer
     * @param EntityManagerInterface $entityManager
     * @param ConfigurationBag $configurationBag
     * @param ValidatorInterface $validator
     * @param TranslationManager $translationsManager
     * @param ModuleManager $moduleManager
     * @param TagExtractor $tagExtractor
     */
    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $entityManager,
        ConfigurationBag $configurationBag,
        ValidatorInterface $validator,
        TranslationManager $translationsManager,
        ModuleManager $moduleManager,
        TagExtractor $tagExtractor
    ) {
        $this->moduleManager = $moduleManager;
        parent::__construct(
            $serializer,
            $entityManager,
            $configurationBag,
            $validator,
            $translationsManager,
            $tagExtractor
        );
    }

    /**
     * Updates a Module.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Updated draft hashes of the modulable resource."
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The module id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\RequestBody (
     *     description="The module json",
     *     @Model(type=Module::class, groups={"put"})
     * )
     * @OA\Tag(name="Modules")
     *
     * @param Request $request
     * @param string $id
     * @return JsonResponse
     * @throws TranslationClientException
     * @throws JsonException
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        /** @var Translatable|Module $module */
        $module = $this->entityManager
            ->getRepository(Module::class)
            ->find($id);
        if (!is_a($module, Module::class)) {
            throw new AccessDeniedHttpException('No module found for id "' . $id . '".');
        }

        $modulable = $this->moduleManager->findResource($module);
        if (null === $modulable) {
            throw new AccessDeniedHttpException('No modulable resource found for class "'.$module->getResourceClass().'" and id "' . $module->getResourceIdentifier() . '".');
        }

        //Extract parameters that have no ids to create them (case edit modules AFTER new parameters added to module definition)
        //@TODO: do this in a cleaner way
        $requestContent = $request->getContent();
        $data = json_decode($requestContent, true, 512, JSON_THROW_ON_ERROR);
        $newParameters = null;

        foreach ($data['parameters'] as $key => $parameter) {
            if ($parameter['id'] === null) {
                $moduleParameter = new ModuleParameter();
                $moduleParameter->setIdentifier($parameter['identifier']);
                $computedName = TranslationKeyGenerator::generateModuleParameterKey($module, $parameter['identifier']);
                $moduleParameter->setValue($computedName);
                $this->persistAndFlush($moduleParameter);
                $module->addParameter($moduleParameter);
                $data['parameters'][$key]['id'] = $moduleParameter->getId();
                $newParameters[$parameter['identifier']] = $moduleParameter->getId();
            }
        }
        if ($newParameters) {
            $requestContent = json_encode($data, JSON_THROW_ON_ERROR);
        }

        $this->serializer->deserialize($requestContent, Module::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $module,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($module);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());

        $this->persistAndFlush($module);

        if (isset($data['hash'])) {
            $draftHashes = $modulable->getDraftHashes();
            $draftHashes[$locale] = [
                'hash' => $data['hash'],
                'date' => new DateTime()
            ];
            $modulable->setDraftHashes($draftHashes);
            $this->persistAndFlush($modulable);
        }

        if ($newParameters) {
            return new JsonResponse(array_merge(
                ['new_parameters' => $newParameters],
                $modulable->getDraftHashes()
            ), Response::HTTP_OK);
        }
        return new JsonResponse($modulable->getDraftHashes(), Response::HTTP_OK);
    }
}
