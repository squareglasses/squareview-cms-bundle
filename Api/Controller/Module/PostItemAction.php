<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Module;

use DateTime;
use JsonException;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use SG\CmsBundle\Api\Entity\Module;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/modules/{discriminator}/{id}", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a Module.
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="The created Module resource",
     *     @Model(type=Module::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The modulable resource's discriminator"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The modulable resource identifier"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The locale of the module"
     * )
     * @OA\RequestBody (
     *     description="The module json",
     *     @Model(type=Module::class, groups={"post"})
     * )
     * @OA\Tag(name="Modules")
     *
     * @param Request $request
     * @param string  $discriminator
     * @param string  $id
     *
     * @return JsonResponse
     * @throws JsonException
     */
    public function __invoke(Request $request, string $discriminator, string $id): JsonResponse
    {
        $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
        if (!$resourceClass) {
            throw new AccessDeniedHttpException('No discriminator configured for "'.$discriminator.'" under "sg_cms.api.discriminators".');
        }

        /** @var Modulable $modulable */
        $modulable = $this->entityManager
            ->getRepository($resourceClass)
            ->findOneBy(['id' => $id])
            ->getDraftVersion()
        ;

        if (null === $modulable) {
            throw new AccessDeniedHttpException('No modulable resource found for class "'.$resourceClass.'" and id "' . $id . '".');
        }

        /** @var Module $module */
        $module = $this->serializer->deserialize($request->getContent(), Module::class, 'json', [
            'groups' => 'post'
        ]);

        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());

        $module
            ->setModulableClass(get_class($modulable))
            ->setModulableIdentifier((int) $modulable->getId())
            ->setLocale($locale);

        $errors = $this->validator->validate($module);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $data = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);

        $this->persistAndFlush($module);

        if (isset($data['hash'])) {
            $draftHashes = $modulable->getDraftHashes();
            $draftHashes[$locale] = [
                'hash' => $data['hash'],
                'date' => new DateTime()
            ];
            $modulable->setDraftHashes($draftHashes);
            $this->persistAndFlush($modulable);
        }

        return new JsonResponse($this->serializer->serialize($module, "json", ['groups' => 'get']), Response::HTTP_CREATED, [], true);
    }
}
