<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Module;

use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\SearchFilter;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/modules", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves a collection of Modules - WIP (needs some work).
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Module collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Module::class, groups={"get"}))
     *     )
     * )
     * @OA\Parameter(
     *     name="resourceIdentifier",
     *     in="query",
     *     description="The resource id of the modules"
     * )
     * @OA\Tag(name="Modules")
     *
     * @return JsonResponse
     */
    public function __invoke(): JsonResponse
    {
        //TODO: This has not been used yet, needs some work
        $this->addFilter(SearchFilter::class, ['resourceIdentifier' => SearchFilterInterface::STRATEGY_EXACT]);

        $resources = $this->dataProvider->getCollection(Module::class);

        return new JsonResponse($this->serializer->serialize($resources, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
