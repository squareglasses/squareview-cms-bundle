<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Module;

use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/modules/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Module by id.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Module resource response.",
     *     @Model(type=Module::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Module resource id"
     * )
     * @OA\Tag(name="Modules")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $resource = $this->getRepository(Module::class)->find($id);
        if (!$resource) {
            throw new ResourceNotFoundException("No Module found for id '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
