<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Website;

use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use SG\CmsBundle\Api\Contracts\Translatable;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use App\Entity\Content;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/websites/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a Website resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Updated serialized website"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The website id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\RequestBody (
     *     description="The website json",
     *     @Model(type=Content::class, groups={"put"})
     * )
     * @OA\Tag(name="Websites")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     * @throws JsonException
     * @throws TranslationClientException
     */
    public function __invoke(Request $request, string $id): Response
    {
        /** @var Translatable $website */
        $website = $this->entityManager
            ->getRepository($this->configurationBag->getWebsiteClass())
            ->findOneBy(['id'=>$id]);
        if (!is_a($website, $this->configurationBag->getWebsiteClass())) {
            throw new AccessDeniedHttpException('No website found for id "'.$id.'".');
        }

        $requestBody = $request->getContent();
        $this->serializer->deserialize(
            $requestBody,
            $this->configurationBag->getWebsiteClass(),
            'json',
            [
                AbstractNormalizer::OBJECT_TO_POPULATE => $website,
                AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
                'groups' => 'put'
            ]
        );

        $errors = $this->validator->validate($website);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        // Translate properties if necessary
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());
        if ($website instanceof Translatable) {
            $this->translationsManager->updateTranslatablePropertiesTranslations(
                $website,
                json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR),
                $locale
            );
            $this->persistAndFlush($website);
        }

        return new JsonResponse($website, 200);
    }
}
