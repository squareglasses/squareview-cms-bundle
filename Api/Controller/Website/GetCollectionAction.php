<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Website;

use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use App\Entity\Website;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/websites", methods={"GET"})
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Website resources.
     *
     * @OA\Response(
     *     response=200,
     *     description="Website collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Website::class, groups={"get"}))
     *     )
     * )
     * @OA\Tag(name="Websites")
     *
     * @return Response
     */
    public function __invoke(): Response
    {
        $resources = $this->dataProvider->getCollection(Website::class);

        return new JsonResponse($this->serializer->serialize($resources, "json", ['groups' => 'get']), 200, [], true);
    }
}
