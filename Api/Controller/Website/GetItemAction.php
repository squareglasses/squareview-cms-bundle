<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Website;

use App\Entity\Website;
use Doctrine\ORM\NonUniqueResultException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Repository\WebsiteRepository;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/websites/{id}", methods={"GET"})
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Website resource.
     *
     * @OA\Response(
     *     response=200,
     *     description="Website resource response.",
     *     @Model(type=Website::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Website id or slug"
     * )
     * @OA\Tag(name="Websites")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     * @throws NonUniqueResultException
     */
    public function __invoke(Request $request, string $id): Response
    {
        /** @var WebsiteRepository $repo */
        $repo = $this->getRepository($this->configurationBag->getWebsiteClass());
        $resource = $repo->findOneBySlugOrId($id);
        if (!$resource) {
            throw new ResourceNotFoundException("No Website found for id '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", [
            'groups' => [
                'get',
                'metatags',
                'medias'
            ]
        ]), 200, [], true);
    }
}
