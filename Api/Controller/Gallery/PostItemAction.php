<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Gallery;

use Exception;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Gallery;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/gallery/{discriminator}/{resourceIdentifier}", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a Gallery associated to a resource
     *
     * @OA\Response(
     *     response=200,
     *     description="Updated draft hashes of the modulable resource.",
     *     @Model(type=Gallery::class, groups={"galleries"})
     * )
     * @OA\Parameter(
     *     name="discriminator",
     *     in="path",
     *     description="The resource to associate gallery to's discriminator"
     * )
     * @OA\Parameter(
     *     name="resourceIdentifier",
     *     in="path",
     *     description="The resource to associate gallery to's identifier"
     * )
     * @OA\RequestBody (
     *     description="The gallery json",
     *     @Model(type=Gallery::class, groups={"post"})
     * )
     * @OA\Tag(name="Gallery")
     *
     * @param Request $request
     * @param string  $discriminator
     * @param string  $resourceIdentifier
     *
     * @return JsonResponse
     *
     * @throws Exception
     */
    public function __invoke(Request $request, string $discriminator, string $resourceIdentifier): JsonResponse
    {
        $resourceClass = $this->configurationBag->getResourceClassByDiscriminator($discriminator);
        if (!$resourceClass) {
            throw new NotFoundHttpException('No discriminator configured for "'.$discriminator.'" under "sg_cms.api.discriminators".');
        }

        /** @var HasGalleriesInterface $resource */
        $resource = $this->entityManager
            ->getRepository($resourceClass)
            ->findOneBy(['id' => $resourceIdentifier])
        ;

        if ($resource instanceof Versionable) {
            $resource = $resource->getDraftVersion();
        }

        if (!$resource instanceof HasGalleriesInterface) {
            throw new NotFoundHttpException('No HasGalleriesInterface resource found for class "'.$resourceClass.'" and id "' . $resourceIdentifier . '".');
        }

        /** @var Gallery $gallery */
        $gallery = $this->serializer->deserialize($request->getContent(), Gallery::class, 'json', [
            'groups' => 'post'
        ]);

        $gallery
            ->setResourceClass($resource->getResourceClass())
            ->setResourceIdentifier($resource->getResourceIdentifier());

        $errors = $this->validator->validate($gallery);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($gallery);

        return new JsonResponse($this->serializer->serialize($gallery, 'json', ['groups' => 'galleries']), 201, [], true);
    }
}
