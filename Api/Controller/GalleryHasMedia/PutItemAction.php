<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\GalleryHasMedia;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/gallery_has_medias/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a GalleryHasMedia (change media associated to a gallery)
     *
     * @OA\Response(
     *     response=200,
     *     description="Updated draft hashes of the modulable resource."
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The galleryHasMedia id"
     * )
     * @OA\RequestBody(
     *     description="The galleryHasMedia json",
     *     @Model(type=GalleryHasMedia::class, groups={"put"})
     * )
     * @OA\Tag(name="GalleryHasMedias")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $galleryHasMedia = $this->entityManager
            ->getRepository(GalleryHasMedia::class)
            ->findOneBy(['id' => $id]);

        if (!is_a($galleryHasMedia, GalleryHasMedia::class)) {
            throw new NotFoundHttpException('No $galleryHasMedia found for id "' . $id . '".');
        }

        $this->serializer->deserialize($request->getContent(), GalleryHasMedia::class, 'json', [
            AbstractNormalizer::OBJECT_TO_POPULATE => $galleryHasMedia,
            AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
            'groups' => 'put'
        ]);

        $errors = $this->validator->validate($galleryHasMedia);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($galleryHasMedia);

        //TODO: Handle draft hashes

        return new JsonResponse($this->serializer->serialize($galleryHasMedia, 'json', ['groups' => 'galleries']), 200, [], true);
    }
}
