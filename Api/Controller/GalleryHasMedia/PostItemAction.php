<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\GalleryHasMedia;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use SG\CmsBundle\Api\Entity\Gallery;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/gallery_has_medias/{galleryId}", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a GalleryHasMedia (associates a media to a gallery)
     *
     * @OA\Response(
     *     response=200,
     *     description="Updated draft hashes of the gallery's resource."
     * )
     * @OA\Parameter(
     *     name="galleryId",
     *     in="path",
     *     description="The id of the gallery the media is added to"
     * )
     * @OA\RequestBody(
     *     description="The galleryHasMedia json",
     *     @Model(type=GalleryHasMedia::class, groups={"post"})
     * )
     * @OA\Tag(name="GalleryHasMedias")
     *
     * @param Request $request
     * @param int     $galleryId
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, int $galleryId): JsonResponse
    {
        /** @var Gallery $gallery */
        $gallery = $this->entityManager
            ->getRepository(Gallery::class)
            ->find($galleryId)
        ;

        if (null === $gallery) {
            throw new NotFoundHttpException('No gallery found for id "' . $galleryId . '".');
        }

        /** @var GalleryHasMedia $galleryHasMedia */
        $galleryHasMedia = $this->serializer->deserialize($request->getContent(), GalleryHasMedia::class, 'json', [
            'groups' => 'post'
        ]);

        $galleryHasMedia->setGallery($gallery);

        $errors = $this->validator->validate($galleryHasMedia);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($galleryHasMedia);

        return new JsonResponse($this->serializer->serialize($galleryHasMedia, 'json', ['groups' => 'galleries']), 201, [], true);
    }
}
