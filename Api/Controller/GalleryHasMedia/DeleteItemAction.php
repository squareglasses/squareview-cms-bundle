<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\GalleryHasMedia;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/gallery_has_medias/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a GalleryMedia (remove media associated to a gallery)
     *
     * @OA\Response(
     *     response=204,
     *     description="Media has been disassociated from gallery"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The galleryHasMedia id to delete"
     * )
     * @OA\Tag(name="GalleryHasMedias")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $galleryHasMedia = $this->entityManager
            ->getRepository(GalleryHasMedia::class)
            ->findOneBy(['id' => $id]);

        if (!is_a($galleryHasMedia, GalleryHasMedia::class)) {
            throw new AccessDeniedHttpException('No galleryHasMedia found for id "' . $id . '".');
        }

        $this->entityManager->remove($galleryHasMedia);
        $this->entityManager->flush();

        //Handle draft hashes ?

        return new JsonResponse(null, 204);
    }
}
