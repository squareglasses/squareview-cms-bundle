<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller;

use SG\CmsBundle\Api\Doctrine\Orm\CollectionDataProvider;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\FilterExtension;
use SG\CmsBundle\Api\Doctrine\Orm\Extension\QueryCollectionExtensionInterface;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class AbstractCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class AbstractCollectionAction extends AbstractController
{
    /**
     * @param SerializerInterface    $serializer
     * @param CollectionDataProvider $dataProvider
     * @param ConfigurationBag       $configurationBag
     */
    public function __construct(protected SerializerInterface $serializer, protected CollectionDataProvider $dataProvider, protected ConfigurationBag $configurationBag)
    {
    }

    /**
     * @param string $filterClass
     * @param array  $properties
     * @param array  $options
     *
     * @return void
     */
    protected function addFilter(string $filterClass, array $properties, array $options = []): void
    {
        $this->dataProvider->addFilter($filterClass, $properties, $options);
    }

    /**
     * @param string|null $filterMode
     * @return void
     */
    protected function setFilterMode(?string $filterMode = null): void
    {
        $this->dataProvider->setFilterMode($filterMode);
    }

    /**
     * @param QueryCollectionExtensionInterface $extension
     * @param array                             $options
     *
     * @return void
     */
    protected function addExtension(QueryCollectionExtensionInterface $extension, array $options = []): void
    {
        $this->dataProvider->addExtension($extension->configure($options));
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    protected function getLocale(Request $request): string
    {
        return $request->get('locale') ?: $request->getLocale();
    }
}
