<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Menu;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 * @package SG\CmsBundle\Api\Controller\Menu
 * @author Florent Chaboud <florent@squareglasses.com>
 * @Route("/menus/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a Menu resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Updated Menu"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Menu resource id"
     * )
     * @OA\RequestBody (
     *     description="The Menu json",
     *     @Model(type=Menu::class, groups={"put"})
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $menu = $this->entityManager
            ->getRepository(Menu::class)
            ->findOneBySlugOrId($id);
        if (!is_a($menu, Menu::class)) {
            throw new AccessDeniedHttpException('No Menu found for id or slug "'.$id.'".');
        }

        $this->serializer->deserialize(
            $request->getContent(),
            Menu::class,
            'json',
            [
                AbstractNormalizer::OBJECT_TO_POPULATE => $menu,
                AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
                'groups' => 'put'
            ]
        );

        $errors = $this->validator->validate($menu);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($menu);

        return new JsonResponse($this->serializer->serialize($menu, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
