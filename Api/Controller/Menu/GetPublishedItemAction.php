<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Menu;

use Exception;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Repository\MenuRepository;
use SG\CmsBundle\Common\Cache\TagExtractor;
use SG\CmsBundle\Common\Contracts\MenuItemInterface;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetPublishedItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GetPublishedItemAction extends AbstractItemAction
{
    /**
     * Retrieves a published Menu resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Menu resource response.",
     *     @Model(type=Menu::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Menu resource id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="path",
     *     description="The Menu resource locale"
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request      $request
     * @param string       $id
     * @param string       $locale
     * @param TagExtractor $tagExtractor
     *
     * @return Response
     * @throws Exception
     */
    public function __invoke(Request $request, string $id, string $locale, TagExtractor $tagExtractor): Response
    {
        /** @var MenuRepository $repo */
        $repo = $this->getRepository(Menu::class);
        $menu = $repo->findOneBySlugOrId($id);
        if (!$menu) {
            throw new ResourceNotFoundException("No Menu found for id or slug '".$id."'");
        }

        //@TODO: Find a best way to set route name. This way is too much heavy in queries
        $cacheTags = [];
        /** @var MenuItem $menuItem */
        foreach ($menu->getItems() as $menuItem) {
            if ($menuItem->getParent() !== null) {
                $menu->removeItem($menuItem);
            } else {
                $this->retrieveRouteName($menuItem, $locale);
                $cacheTags = $tagExtractor->mergeTags($cacheTags, $tagExtractor->generateTags($menuItem));
                if ($menuItem->getTargetResourceClass() !== null && $menuItem->getTargetResourceId() !== null) {
                    $cacheTags[] = $tagExtractor::stringify($menuItem->getTargetResourceClass().'-'.$menuItem->getTargetResourceId());
                }
            }
        }

        return new JsonResponse($this->serializer->serialize($menu, 'json', ['groups' => 'publish']), Response::HTTP_OK, [
            'cache-tags' => implode(',', $cacheTags)
        ], true);
    }

    private function retrieveRouteName(MenuItem $menuItem, string $locale): void
    {
        if ($menuItem->getKind() === MenuItemInterface::KIND_RESOURCE) {
            $route = $this->getRepository(Route::class)
                ->findOneBy([
                    "locale" => $locale,
                    "resourceIdentifier" => $menuItem->getTargetResourceId(),
                    "resourceClass" => $menuItem->getTargetResourceClass()
                ]);
            if ($route) {
                $menuItem->setRoute($route->getName());
            }
            if (null !== $menuItem->getChildren() && $menuItem->getChildren()->count() > 0) {
                foreach ($menuItem->getChildren() as $subMenuItem) {
                    $this->retrieveRouteName($subMenuItem, $locale);
                }
            }
        } else if ($menuItem->getKind() === MenuItemInterface::KIND_ROUTE) {
            $menuItem->setRoute($menuItem->getValue());
        }
    }
}
