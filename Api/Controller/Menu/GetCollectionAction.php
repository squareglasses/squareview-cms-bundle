<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Menu;

use SG\CmsBundle\Api\Doctrine\Orm\Extension\PaginationExtension;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GetCollectionAction extends AbstractCollectionAction
{
    /**
     * Retrieves the collection of Menu resources.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Menu collection response.",
     *     @OA\JsonContent(
     *       type="array",
     *       @OA\Items(ref=@Model(type=Menu::class, groups={"get"}))
     *     )
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param PaginationExtension $paginationExtension
     *
     * @return Response
     */
    public function __invoke(Request $request, PaginationExtension $paginationExtension): Response
    {
        if ($request->get('pagination', true) !== 'false'
            && $request->get('pagination', true) !== false
        ) {
            $this->addExtension($paginationExtension, [
                'items_per_page' => 20
            ]);
        }

        return $this->dataProvider->getResponse(Menu::class, null, ['get']);
    }
}
