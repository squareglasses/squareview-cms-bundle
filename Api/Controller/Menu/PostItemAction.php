<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Menu;

use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/menus", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a Menu.
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="The created Menu resource",
     *     @Model(type=Menu::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The locale of the menu"
     * )
     * @OA\RequestBody (
     *     description="The Menu json",
     *     @Model(type=Menu::class, groups={"post"})
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $discriminator
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request): JsonResponse
    {
        $requestBody = $request->getContent();
        $menu = $this->serializer->deserialize($requestBody, Menu::class, 'json', [
            'groups' => 'post'
        ]);

        $errors = $this->validator->validate($menu);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }
        $this->persistAndFlush($menu);

        return new JsonResponse($this->serializer->serialize($menu, "json", ['groups' => 'get']), Response::HTTP_CREATED, [], true);
    }
}
