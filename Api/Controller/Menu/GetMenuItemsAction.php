<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Menu;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use SG\CmsBundle\Api\Controller\AbstractCollectionAction;
use SG\CmsBundle\Api\Doctrine\Common\Filter\SearchFilterInterface;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\ExistsFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\OrderFilter;
use SG\CmsBundle\Api\Doctrine\Orm\Filter\SearchFilter;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Entity\MenuItem;
use RuntimeException;
use SG\CmsBundle\Api\Util\ApiHelper;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/menus/{id}/items", methods={"GET"})
 */
class GetMenuItemsAction extends AbstractCollectionAction
{
    /**
     * Retrieves the MenuItems collection of a Menu resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Menu resource response.",
     *     @Model(type=MenuItem::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Menu resource id or slug"
     * )
     *
     * @OA\Parameter(
     *     name="parent.slug",
     *     in="query",
     *     description="The content parent's slug"
     * )
     *
     * @OA\Parameter(
     *     name="exists[parent]",
     *     in="query",
     *     @OA\Schema(type="boolean"),
     *     description="If item has a parent"
     * )
     *
     * @OA\Parameter(
     *     name="serialization_groups[]",
     *     in="query",
     *     @OA\Schema(type="array", @OA\Items(type="string")),
     *     description="Array of specific serialization groups"
     * )
     *
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $id
     * @param EntityManagerInterface $manager
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id, EntityManagerInterface $manager): Response
    {
        $this->addFilter(SearchFilter::class, ['parent.slug' => SearchFilterInterface::STRATEGY_EXACT]);
        $this->addFilter(ExistsFilter::class, ['parent']);
        $this->addFilter(OrderFilter::class, [
            'position' => 'asc'
        ]);

        $menu = $manager->getRepository(Menu::class)->findOneBySlugOrId($id);
        if (!$menu) {
            throw new ResourceNotFoundException("No menu found for id or slug '".$id."'");
        }

        $menuItemRepository = $manager->getRepository(MenuItem::class);

        if (!method_exists($menuItemRepository, 'createQueryBuilder')) {
            throw new RuntimeException('The repository class must have a "createQueryBuilder" method.');
        }

        $qb = $menuItemRepository->createQueryBuilder('mi')
            ->innerJoin('mi.menu', 'm')
            ->andWhere('m = :menu')
            ->setParameter('menu', $menu)
        ;

        if ($request->get('exists[parent]')) {
            $qb->andWhere('mi.parent is NOT NULL');
        }

        return $this->dataProvider->getResponse(
            MenuItem::class,
            $qb,
            ApiHelper::getSerializeGroupsFromRequest($request)
        );
    }
}
