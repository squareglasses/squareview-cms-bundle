<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\Menu;

use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a Menu resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Menu resource response.",
     *     @Model(type=Menu::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The Menu resource id"
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $resource = $this->getRepository(Menu::class)->findOneBySlugOrId($id);
        if (!$resource) {
            throw new ResourceNotFoundException("No menu found for id or slug '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($resource, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
