<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MenuItem;

use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Doctrine\Common\CacheResourcesHolder;
use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class OrderCollectionAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/menus/items/order", methods={"PUT"})
 */
class OrderCollectionAction extends AbstractItemAction
{
    /**
     * Updates positions for the given MenuItem's ids array.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Returns 200 status code if positions are updated"
     * )
     * @OA\RequestBody (
     *     description="The array of content's id, in the desired order.",
     *     @OA\Schema(
     *         type="array",
     *         @OA\Items(type="string"))
     *     )
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request              $request
     * @param CacheResourcesHolder $cacheResourcesHolder
     *
     * @return Response
     * @throws JsonException
     */
    public function __invoke(Request $request, CacheResourcesHolder $cacheResourcesHolder): Response
    {
        $ids = json_decode($request->getContent(), true, 512, JSON_THROW_ON_ERROR);
        foreach ($ids as $position => $id) {
            $menuItem = $this->entityManager
                ->getRepository(MenuItem::class)
                ->findOneBySlugOrId($id);
            if (null === $menuItem) {
                throw new ResourceNotFoundException("No MenuItem found for id or slug '".$id."'");
            }
            $menuItem->setPosition($position);
            $this->entityManager->persist($menuItem);
            $cacheResourcesHolder->addResource($menuItem);
        }
        $this->entityManager->flush();

        return new JsonResponse('', Response::HTTP_OK);
    }
}
