<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MenuItem;

use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class GetItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GetItemAction extends AbstractItemAction
{
    /**
     * Retrieves a MenuItem resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="MenuItem resource response.",
     *     @Model(type=MenuItem::class, groups={"get", "parents"})
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The MenuItem resource id"
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     */
    public function __invoke(Request $request, string $id): Response
    {
        $item = $this->getRepository(MenuItem::class)->findOneBySlugOrId($id);
        if (!$item) {
            throw new ResourceNotFoundException("No MenuItem found for id '".$id."'");
        }
        return new JsonResponse($this->serializer->serialize($item, "json", ['groups' => ['get', 'parents', 'targetResource']]), Response::HTTP_OK, [], true);
    }
}
