<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MenuItem;

use Exception;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Common\Exception\ResourceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;

/**
 * Class DeleteItemAction
 *
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 *
 * @Route("/menus/items/{id}", methods={"DELETE"})
 */
class DeleteItemAction extends AbstractItemAction
{
    /**
     * Deletes a MenuItem by id.
     *
     * @OA\Response(
     *     response=Response::HTTP_NO_CONTENT,
     *     description="MenuItem has been deleted"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The MenuItem id"
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return JsonResponse
     */
    public function __invoke(Request $request, string $id): JsonResponse
    {
        $item = $this->getRepository(MenuItem::class)->find($id);
        if (!$item) {
            throw new ResourceNotFoundException("No MenuItem found for id '".$id."'");
        }

        try {
            $this->entityManager->remove($item);
            $this->entityManager->flush();

            return new JsonResponse(null, Response::HTTP_NO_CONTENT);
        } catch (Exception $e) {
            return new JsonResponse(['error' => $e->getMessage()], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}
