<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MenuItem;

use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;
use SG\CmsBundle\Api\Entity\MenuItem;

/**
 * Class PostItemAction
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 *
 * @Route("/menus/items", methods={"POST"})
 */
class PostItemAction extends AbstractItemAction
{
    /**
     * Creates a MenuItem.
     *
     * @OA\Response(
     *     response=Response::HTTP_CREATED,
     *     description="The created MenuItem resource",
     *     @Model(type=MenuItem::class, groups={"get"})
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The locale of the item"
     * )
     * @OA\RequestBody (
     *     description="The MenuItem json",
     *     @Model(type=MenuItem::class, groups={"post"})
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws JsonException
     * @throws TranslationClientException
     */
    public function __invoke(Request $request): JsonResponse
    {
        $requestBody = $request->getContent();

        /** @var MenuItem $menuItem */
        $menuItem = $this->serializer->deserialize($requestBody, MenuItem::class, 'json', [
            'groups' => 'post'
        ]);

        $errors = $this->validator->validate($menuItem);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        $this->persistAndFlush($menuItem);

        // Translate properties if necessary
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());

        $this->translationsManager->updateTranslatablePropertiesTranslations(
            $menuItem,
            json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR),
            $locale
        );
        $this->persistAndFlush($menuItem);

        return new JsonResponse($this->serializer->serialize($menuItem, "json", ['groups' => ['get', 'parents']]), Response::HTTP_CREATED, [], true);
    }
}
