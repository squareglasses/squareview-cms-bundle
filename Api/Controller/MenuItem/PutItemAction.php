<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Controller\MenuItem;

use JsonException;
use SG\CmsBundle\Api\Controller\AbstractItemAction;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Util\ConstraintViolationResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;
use OpenApi\Annotations as OA;
use SG\CmsBundle\Api\Entity\MenuItem;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Class PutItemAction
 * @package SG\CmsBundle\Api\Controller\MenuItem
 * @author Florent Chaboud <florent@squareglasses.com>
 * @Route("/menus/items/{id}", methods={"PUT"})
 */
class PutItemAction extends AbstractItemAction
{
    /**
     * Updates a MenuItem resource.
     *
     * @OA\Response(
     *     response=Response::HTTP_OK,
     *     description="Updated MenuItem"
     * )
     * @OA\Parameter(
     *     name="id",
     *     in="path",
     *     description="The MenuItem resource id"
     * )
     * @OA\Parameter(
     *     name="locale",
     *     in="query",
     *     description="The request locale"
     * )
     * @OA\RequestBody (
     *     description="The MenuItem json",
     *     @Model(type=MenuItem::class, groups={"put"})
     * )
     * @OA\Tag(name="Menus")
     *
     * @param Request $request
     * @param string  $id
     *
     * @return Response
     * @throws JsonException
     * @throws TranslationClientException
     */
    public function __invoke(Request $request, string $id): Response
    {
        $menuItem = $this->entityManager
            ->getRepository(MenuItem::class)
            ->findOneBySlugOrId($id);
        if (!is_a($menuItem, MenuItem::class) || null === $menuItem) {
            throw new AccessDeniedHttpException('No MenuItem found for id or slug "'.$id.'".');
        }

        $requestBody = $request->getContent();

        $data = json_decode($requestBody, true, 512, JSON_THROW_ON_ERROR);

        $this->serializer->deserialize(
            $requestBody,
            MenuItem::class,
            'json',
            [
                AbstractNormalizer::OBJECT_TO_POPULATE => $menuItem,
                AbstractObjectNormalizer::DEEP_OBJECT_TO_POPULATE => true,
                'groups' => ['put']
            ]
        );

        $errors = $this->validator->validate($menuItem);
        if (count($errors) > 0) {
            return new ConstraintViolationResponse($errors);
        }

        // Translate properties if necessary
        $locale = $request->get("locale", $this->configurationBag->getDefaultLocale());

        $this->translationsManager->updateTranslatablePropertiesTranslations(
            $menuItem,
            $data,
            $locale
        );

        $this->persistAndFlush($menuItem);

        return new JsonResponse($this->serializer->serialize($menuItem, "json", ['groups' => 'get']), Response::HTTP_OK, [], true);
    }
}
