<?php

declare(strict_types=1);

namespace SG\CmsBundle\Api\Security;

use App\Entity\User;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAccountStatusException;
use Symfony\Component\Security\Core\User\UserCheckerInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class UserChecker
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class UserChecker implements UserCheckerInterface
{
    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPreAuth(UserInterface $user): void
    {
        if (!is_a($user, $this->getUserClass())) {
            return;
        }

        if (!$user->isEnabled()) {
            $ex = new CustomUserMessageAccountStatusException('User account is disabled.');
            $ex->setUser($user);
            throw $ex;
        }

        if ($user->isLocked()) {
            $ex = new CustomUserMessageAccountStatusException('User account is locked.');
            $ex->setUser($user);
            throw $ex;
        }
    }

    /**
     * @param UserInterface $user
     *
     * @return void
     */
    public function checkPostAuth(UserInterface $user): void
    {
        if (!is_a($user, $this->getUserClass())) {
            return;
        }
    }

    /**
     * @return string
     */
    private function getUserClass(): string
    {
        return User::class;
    }
}
