<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataFixtures;

use Doctrine\Persistence\ObjectManager;
use Exception;
use JetBrains\PhpStorm\ArrayShape;
use JsonException;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use ReflectionException;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\DataFixtures\Model\CmsResourceFixtureInterface;
use SG\CmsBundle\Api\Manager\GalleryManager;
use SG\CmsBundle\Api\Manager\ModuleManager;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Serializer\MenuSerializer;
use SG\CmsBundle\Api\Translation\Exception\InvalidCatalogTypeException;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Bag\ConfigurationBag;
use SG\CmsBundle\Common\Cache\CacheManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Doctrine\Bundle\FixturesBundle\Fixture;
use SG\CmsBundle\Api\Manager\HasMediaManager;
use SG\CmsBundle\Api\Manager\MediaManager;
use SG\CmsBundle\Api\Manager\MetaTagManager;
use Symfony\Component\Console\ConsoleEvents;
use Symfony\Component\Console\Event\ConsoleCommandEvent;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class AbstractCmsFixtures
 * @package SG\CmsBundle\Api\DataFixtures
 * @author Florent Chaboud <florent@squareglasses.com>
 */
abstract class AbstractCmsFixtures extends Fixture implements EventSubscriberInterface, CmsResourceFixtureInterface
{
    use CmsFixturesTrait;

    protected ?CmsResourceInterface $homepage = null;

    protected InputInterface $input;
    protected OutputInterface $output;

    /**
     * AbstractCmsFixtures constructor
     *
     * @param MediaManager $mediaManager
     * @param HasMediaManager $hasMediaManager
     * @param GalleryManager $galleryManager
     * @param FilesystemOperator $localStorage
     * @param ResourceGuideProvider $guideProvider
     * @param TranslationManager $translationManager
     * @param MetaTagManager $metaTagManager
     * @param ModuleManager $moduleManager
     * @param ConfigurationBag $configurationBag
     * @param SerializerInterface $serializer
     * @param MenuSerializer $menuSerializer
     * @param PublicationManager $publicationManager
     */
    public function __construct(
        protected MediaManager $mediaManager,
        protected HasMediaManager $hasMediaManager,
        protected GalleryManager $galleryManager,
        protected FilesystemOperator $localStorage,
        protected ResourceGuideProvider $guideProvider,
        protected TranslationManager $translationManager,
        protected MetaTagManager $metaTagManager,
        protected ModuleManager $moduleManager,
        protected ConfigurationBag $configurationBag,
        protected SerializerInterface $serializer,
        protected MenuSerializer $menuSerializer,
        protected PublicationManager $publicationManager
    ) {
        $this->filesystem           = $localStorage;
        $this->classes             = $configurationBag->getClasses();
        $this->contentClass        = $this->classes['content'] ?? null;
    }

    /**
     * @param ObjectManager $manager
     *
     * @return void
     * @throws ClientExceptionInterface
     * @throws FilesystemException
     * @throws InvalidCatalogTypeException
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ReflectionException
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     * @throws Exception
     */
    public function process(ObjectManager $manager): void
    {
        // Manage homepage
        if (null !== $homepage = $this->getHomepage()) {
            $homepage->setDiscriminator("homepage");
            $this->resources[] = $homepage; // Adding homepage to resources because it's a specific case
        }

        // Manage all resources pages
        $this->generateResources();
        $this->generateMetatags();
        $this->generateRoutes();
        $this->generateMedias();
        $this->generateGalleries();
        $this->prepareResourcesTranslations();
        $manager->flush();

        $this->generateModules($this->resources);
        $manager->flush();

        $this->translationManager->multipleTranslate($this->translations);
        $this->publishVersionableResources();
        $this->publishLayoutGuideTranslations();
        // Generate fixtures references to transversal uses of fixtures
        $this->generateReferences();
        $manager->flush();
    }

    /**
     * @return array|string[]
     */
    #[ArrayShape([ConsoleEvents::COMMAND => "string"])]
    public static function getSubscribedEvents(): array
    {
        return [
            ConsoleEvents::COMMAND => 'init',
        ];
    }

    /**
     * @param ConsoleCommandEvent $event
     */
    public function init(ConsoleCommandEvent $event): void
    {
        $this->output = $event->getOutput();
        $this->input = $event->getInput();
    }

    /**
     * @return void
     */
    private function generateReferences(): void
    {
        foreach ($this->resources as $resource) {
            if (null !== $resource->getReferenceName()) {
                $flushedResource = $this->getFlushedResource($resource);
                $this->addReference($resource->getReferenceName(), $flushedResource);
            }
        }
    }

    /**
     * @return CmsResourceInterface|null
     */
    public function getHomepage(): ?CmsResourceInterface
    {
        return $this->homepage;
    }

    /**
     * @param CmsResourceInterface|null $homepage
     *
     * @return void
     */
    public function setHomepage(?CmsResourceInterface $homepage): void
    {
        $this->homepage = $homepage;
    }
}
