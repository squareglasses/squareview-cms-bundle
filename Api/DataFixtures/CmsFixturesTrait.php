<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataFixtures;

use DateTime;
use Doctrine\Persistence\ObjectManager;
use Exception;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Translatable;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use ReflectionException;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\LanguageEnableable;
use SG\CmsBundle\Api\Contracts\LanguageInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\DataMigrations\Model\AbstractCmsMigration;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Gallery;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Entity\Menu;
use SG\CmsBundle\Api\Entity\MenuItem;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\Exception\InvalidCatalogTypeException;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\String\UnicodeString;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Trait CmsFixturesTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait CmsFixturesTrait
{
    use CmsFixtureTranslationsTrait;

    protected ObjectManager $manager;
    public FilesystemOperator $filesystem;

    /** @var array<string,string> $classes */
    protected array $classes;
    protected ?string $contentClass = null;

    private array $resources = [];
    private array $flushedResources = [];
    private array $menus = [];

    /**
     * @param ObjectManager $manager
     *
     * @return void
     */
    public function load(ObjectManager $manager): void
    {
        $this->manager = $manager;
    }

    /**
     * @param CmsResourceInterface $resourceFixture
     * @return $this
     */
    public function addResource(CmsResourceInterface $resourceFixture): self
    {
        $this->resources[] = $resourceFixture;
        return $this;
    }

    /**
     * @param Menu $menu
     *
     * @return void
     */
    public function addMenu(Menu $menu): void
    {
        $this->menus[] = $menu;
    }

    /**
     * @param CmsResourceInterface   $resource
     * @param string               $defaultClass
     *
     * @return CmsResourceInterface
     */
    private function createEntity(CmsResourceInterface $resource, string $defaultClass): CmsResourceInterface
    {
        $className = $resource->getClass() ?? $defaultClass;
        return new $className();
    }

    /**
     * @param CmsResourceInterface   $resource
     * @param CmsResourceInterface          $entity
     *
     * @return void
     */
    private function configureEntity(CmsResourceInterface $resource, CmsResourceInterface $entity): void
    {
        $slug = $this->getSlug($resource->getName());
        $resource->setSlug($slug);

        if (method_exists($entity, 'setSlug')) {
            $entity->setSlug($slug);
        }
        if (method_exists($entity, 'setDiscriminator')) {
            $entity->setDiscriminator($resource->getDiscriminator());
        }
        if (method_exists($entity, 'setDefaultName')) {
            $entity->setDefaultName($resource->getName());
        }
        if (method_exists($entity, 'setName')) {
            $entity->setName($resource->getName());
        }
        if (method_exists($entity, 'setPosition') && null !== $resource->getPosition()) {
            $entity->setPosition($resource->getPosition());
        }
        if ($entity instanceof Guidable && null !== $resource->getGuide()) {
            $entity->setGuide($resource->getGuide());
        }

        if (null !== $resource->getParent()) {
            $entity->setParent($this->getFlushedResource($resource->getParent()));
        }
        if (null !== $resource->getParentEntity()) {
            $entity->setParent($resource->getParentEntity());
        }

        foreach ($resource->getProperties() as $property => $value) {
            $this->setPropertyValue($property, $value, $entity);
            if ($property === 'menu' && $resource->getClass() === MenuItem::class) {
                $value->addItem($entity);
            }
        }
    }

    /**
     * @param CmsResourceInterface   $resource
     * @param CmsResourceInterface $entity
     *
     * @return Version|null
     * @throws Exception
     */
    private function createAndConfigureDraft(CmsResourceInterface $resource, CmsResourceInterface $entity): ?Version
    {
        $draft = null;
        if ($entity instanceof Versionable) {
            $versionClass = $this->getPublicationManager()->getVersionClass($entity);
            /** @var Version $draft */
            $draft = new $versionClass();
            $draft->setCurrentDraft(true);
            $entity->addVersion($draft);

            if ($draft instanceof Guidable && null !== $resource->getGuide()) {
                $draft->setGuide($resource->getGuide());
            }

            foreach ($resource->getProperties() as $property => $value) {
                $this->setPropertyValue($property, $value, $draft);
            }
        }
        return $draft;
    }

    /**
     * @param CmsResourceInterface $entity
     * @param Version|null $draft
     * @throws Exception
     */
    private function processHashes(CmsResourceInterface $entity, ?Version $draft): void
    {
        $draft?->setDraftHashes($this->generateFixtureDraftHashes($entity));
    }

    /**
     * @param Versionable $entity
     * @return array
     * @throws Exception
     */
    private function generateFixtureDraftHashes(Versionable $entity): array
    {
        $hashes = [];
        if ($entity instanceof LanguageEnableable) {
            /** @var LanguageInterface $language */
            foreach ($entity->getLanguages() as $language) {
                $locale = $language->getId();
                $version = [
                    'date' => new DateTime(),
                    'hash' => $locale.'-fixture'
                ];
                $hashes[$locale] = $version;
            }
        }

        return $hashes;
    }

    /**
     * @param string       $property
     * @param              $value
     * @param              $entity
     * @param Version|null $draft
     *
     * @return void
     */
    private function setPropertyValue(string $property, $value, $entity, Version $draft = null): void
    {
        if (property_exists(get_class($entity), $property)) {
            $entity->{"set" . ucfirst($property)}($value);
        } elseif ($entity instanceof Versionable && null !== $draft && property_exists(get_class($draft), $property)) {
            $draft->{"set" . ucfirst($property)}($value);
        }
    }

    /**
     * @param string $message
     * @param string $type
     *
     * @return void
     */
    public function log(string $message, string $type = 'info'): void
    {
        $io = new SymfonyStyle($this->input, $this->output);
        switch ($type) {
            case "error":
                $io->error($message);
                break;
            case "warning":
                $io->warning($message);
                break;
            case "caution":
                $io->caution($message);
                break;
            default:
                $io->info($message);
                break;
        }
    }

    /**
     * @return array
     */
    private function getFlushedResources(): array
    {
        return $this->flushedResources;
    }

    /**
     * @param CmsResourceInterface $resource
     *
     * @return CmsResourceInterface|null
     */
    private function getFlushedResource(CmsResourceInterface $resource): ?CmsResourceInterface
    {
        foreach ($this->flushedResources as $flushedResource) {
            if (!method_exists($flushedResource, 'getSlug')) {
                $slug = $this->getSlug($flushedResource->getName());
            } else {
                $slug = $flushedResource->getSlug();
            }
            if ($resource->getSlug() === $slug) {
                return $flushedResource;
            }
        }
        return null;
    }

    /**
     * @return PublicationManager
     */
    private function getPublicationManager(): PublicationManager
    {
        return $this->publicationManager;
    }

    /**
     * @param LanguageEnableable $languageEnableable
     * @param string             $locale
     *
     * @return void
     */
    protected function addLanguage(LanguageEnableable $languageEnableable, string $locale): void
    {
        $constant = 'App\DataFixtures\LanguageFixtures::LANGUAGE_' . strtoupper($locale);
        if (!defined($constant)) {
            throw new RuntimeException("No language found for locale " . $locale);
        }
        /** @var LanguageInterface $language */
        $language = $this->getReference(constant($constant), Language::class);
        $languageEnableable->addLanguage($language);
        $this->manager->persist($languageEnableable);
    }

    /**
     * @return void
     */
    private function generateRoutes(): void
    {
        foreach ($this->resources as $resource) {
            $flushedResource = $this->getFlushedResource($resource);
            if (null !== $flushedResource) {
                if ($flushedResource->getDiscriminator() === "homepage") {
                    foreach ($resource->getLanguages() as $locale) {
                        $route = new Route();
                        $route->setPath("homepage");
                        $route->setLocale($locale);
                        $route->setName($flushedResource->getSlug());
                        $route->setResourceIdentifier($flushedResource->getResourceIdentifier());
                        $route->setResourceClass($flushedResource->getResourceClass());
                        $this->manager->persist($route);
                    }
                } else {
                    foreach ($resource->getRoutes() as $locale => $routeConfig) {
                        $route = new Route();
                        if (!array_key_exists('path', $routeConfig)) {
                            throw new RuntimeException('"path" key is mandatory for route.');
                        }
                        $path = $routeConfig['path'];
                        $routeName = array_key_exists('name', $routeConfig) ?
                            $routeConfig['name'] : $flushedResource->getSlug();

                        if (array_key_exists("options", $routeConfig)) {
                            $route->setOptions($routeConfig['options']);
                        }
                        $route->setPath($path);
                        $route->setLocale($locale);
                        $route->setName($routeName);
                        $route->setResourceIdentifier($flushedResource->getResourceIdentifier());
                        $route->setResourceClass($flushedResource->getResourceClass());
                        $this->manager->persist($route);
                    }
                }
            }
        }
    }

    /**
     * @return void
     * @throws InvalidCatalogTypeException
     */
    private function generateMetatags(): void
    {
        foreach ($this->resources as $resource) {
            $flushedResource = $this->getFlushedResource($resource);
            if ($flushedResource instanceof MetaTaggable) {
                $this->generateMetatagsForResource($flushedResource, $resource);
            }
            if ($flushedResource instanceof Versionable) {
                $draft = $flushedResource->getDraftVersion();
                if ($draft instanceof MetaTaggable) {
                    $this->generateMetatagsForResource($draft, $resource);
                }
            }
        }
    }

    /**
     * @param MetaTaggable       $metaTaggable
     * @param CmsResourceInterface $resource
     *
     * @return void
     */
    private function generateMetatagsForResource(MetaTaggable $metaTaggable, CmsResourceInterface $resource): void
    {
        foreach ($resource->getMetatags() as $locale => $metatags) {
            foreach ($metatags as $type => $value) {
                $t = $type === 'title' ? 'title' : 'name';
                $this->metaTagManager->addMetaTagToResource(
                    $metaTaggable,
                    $type,
                    $value,
                    $locale,
                    $t
                );
            }
        }
    }

    /**
     * @return void
     * @throws Exception|FilesystemException
     */
    private function generateMedias(): void
    {
        foreach ($this->resources as $resource) {
            $flushedResource = $this->getFlushedResource($resource);
            if ($flushedResource instanceof HasMediasInterface) {
                $this->generateMediasForResource($flushedResource, $resource);
            }
            if ($flushedResource instanceof Versionable) {
                $draft = $flushedResource->getDraftVersion();
                if ($draft instanceof HasMediasInterface) {
                    $this->generateMediasForResource($draft, $resource);
                }
            }
        }
    }

    /**
     * @param HasMediasInterface $hasMedias
     * @param CmsResourceInterface $resource
     *
     * @return void
     * @throws FilesystemException
     */
    private function generateMediasForResource(HasMediasInterface $hasMedias, CmsResourceInterface $resource): void
    {
        foreach ($resource->getMedias() as $mediaConfig) {
            if (($mediaConfig['providerName'] !== 'image' && $mediaConfig['providerName'] !== 'file')
                || ($this->filesystem->has($mediaConfig['filename']))
            ) {
                $media = $this->mediaManager->createMedia($mediaConfig['providerName'], $mediaConfig['filename'], $mediaConfig['filename']);
                if ($media !== null) {
                    $options = array_merge([
                        'identifier' => $mediaConfig['identifier']
                    ], $mediaConfig['options']);
                    if (array_key_exists('alt', $mediaConfig)) {
                        $options['alt'] = $mediaConfig['alt'];
                    }
                    $this->hasMediaManager->addMediaToResource($media, $hasMedias, $options);
                }
            } else {
                $this->log("Media " . $mediaConfig['filename'] . " not found.", "warning");
            }
        }
    }

    /**
     * @return void
     * @throws Exception
     * @throws FilesystemException
     */
    private function generateGalleries(): void
    {
        foreach ($this->resources as $resource) {
            $flushedResource = $this->getFlushedResource($resource);
            if ($flushedResource instanceof HasGalleriesInterface) {
                $this->generateGalleriesForResource($flushedResource, $resource);
            }
            if ($flushedResource instanceof Versionable) {
                $draft = $flushedResource->getDraftVersion();
                if ($draft instanceof HasGalleriesInterface) {
                    $this->generateGalleriesForResource($draft, $resource);
                }
            }
        }
    }

    /**
     * @param HasGalleriesInterface $hasGalleries
     * @param CmsResourceInterface    $resource
     *
     * @return void
     * @throws FilesystemException
     */
    private function generateGalleriesForResource(HasGalleriesInterface $hasGalleries, CmsResourceInterface $resource): void
    {
        foreach ($resource->getGalleries() as $identifier => $galleryConfig) {
            $gallery = (new Gallery())
                ->setIdentifier($identifier)
                ->setResourceClass($hasGalleries->getResourceClass())
                ->setResourceIdentifier($hasGalleries->getResourceIdentifier())
                ->setName($galleryConfig['name'] ?? null);
            $this->manager->persist($gallery);

            if (array_key_exists('medias', $galleryConfig)) {
                foreach ($galleryConfig['medias'] as $position => $mediaConfig) {
//                    if (($mediaConfig['providerName'] !== 'image' && $mediaConfig['providerName'] !== 'file')
//                        || ($this->filesystem->has($mediaConfig['filename']))
//                    ) {
                        $position = array_key_exists('position', $mediaConfig) ? $mediaConfig['position'] : $position;
                        if (!array_key_exists('provider', $mediaConfig)) {
                            $this->log("You must pass 'provider' key into media's gallery configuration.", "warning");
                        } elseif (!array_key_exists('filename', $mediaConfig)) {
                            $this->log("You must pass 'filename' key into media's gallery configuration.", "warning");
                        } else if (($mediaConfig['provider'] !== 'image' && $mediaConfig['provider'] !== 'file')
                            || ($this->filesystem->has($mediaConfig['filename']))
                        ){
                            $media = $this->mediaManager->createMedia($mediaConfig['provider'], $mediaConfig['filename'], $mediaConfig['filename']);
                            $galleryHasMedia = (new GalleryHasMedia())
                                ->setMedia($media)
                                ->setGallery($gallery)
                                ->setIdentifier($gallery->getIdentifier())
                                ->setPosition($position);
                            if (array_key_exists('alt', $mediaConfig)) {
                                $galleryHasMedia->setAlt($mediaConfig['alt']);
                            }
                            $this->manager->persist($galleryHasMedia);
                        } else {
                            $this->log("Media " . $mediaConfig['filename'] . " not found for gallery '".$identifier."'.", "warning");
                        }
                }
            }
            // We must flush and refresh gallery to load correctly media's collection in the entity
            $this->manager->flush();
            $this->manager->refresh($gallery);
        }
    }

    /**
     * @param array $resources
     *
     * @throws Exception|FilesystemException
     */
    private function generateModules(array $resources): void
    {
        foreach ($resources as $resource) {
            $flushedResource = $this->getFlushedResource($resource);
            if ($flushedResource instanceof Modulable) {
                $this->generateModulesForResource($flushedResource, $resource);
            }
            if ($flushedResource instanceof Versionable) {
                $draft = $flushedResource->getDraftVersion();
                if ($draft instanceof Modulable) {
                    $this->generateModulesForResource($draft, $resource);
                }
            }
        }
    }

    /**
     * @param Modulable          $modulable
     * @param CmsResourceInterface $resource
     *
     * @throws InvalidCatalogTypeException
     * @throws FilesystemException
     * @throws FilesystemException
     */
    private function generateModulesForResource(Modulable $modulable, CmsResourceInterface $resource): void
    {
        foreach ($resource->getModules() as $locale => $modules) {
            foreach ($modules as $position => $moduleConfig) {
                //Make a fixed module identifier, so they stay the same at each fixture load
                $moduleIdentifier = $this->generateFixtureModuleIdentifier($modulable, $moduleConfig, $locale, $position);
                $module = $this->moduleManager->addModuleToResource($modulable, $moduleConfig['guide'], $locale, $moduleConfig['zone'], $moduleIdentifier);
                foreach ($moduleConfig['parameters'] as $identifier => $value) {
                    if (is_object($value)) {
                        $this->moduleManager->addEntityParameter($identifier, $value);
                    } elseif ($identifier === "medias") {
                        foreach ($value as $mediaConfig) {
                            if (($mediaConfig['providerName'] !== 'image' && $mediaConfig['providerName'] !== 'file')
                                || (($mediaConfig['providerName'] === 'image' || $mediaConfig['providerName'] === 'file')
                                    && $this->filesystem->has($mediaConfig['filename']))
                            ) {
                                $media = $this->mediaManager->createMedia($mediaConfig['providerName'], $mediaConfig['filename']);
                                if ($media !== null) {
                                    $options = ['identifier' => $mediaConfig['identifier']];
                                    if (array_key_exists('alt', $mediaConfig)) {
                                        $options['alt'] = $mediaConfig['alt'];
                                    }
                                    $this->hasMediaManager->addMediaToResource($media, $module, $options);
                                }
                            } else {
                                $this->log("Media " . $mediaConfig['filename'] . " not found.", "warning");
                            }
                        }
                    } elseif ($identifier === "galleries") {
                        foreach ($value as $galleryIdentifier => $galleryMedias) {
                            $gallery = $this->galleryManager->createGallery($galleryIdentifier, $module);
                            foreach ($galleryMedias as $mediaConfig) {
                                if (($mediaConfig['providerName'] !== 'image' && $mediaConfig['providerName'] !== 'file')
                                    || (($mediaConfig['providerName'] === 'image' || $mediaConfig['providerName'] === 'file')
                                        && $this->filesystem->has($mediaConfig['filename']))
                                ) {
                                    $media = $this->mediaManager->createMedia($mediaConfig['providerName'], $mediaConfig['filename']);
                                    if ($media !== null) {
                                        $options = [];
                                        if (array_key_exists('alt', $mediaConfig)) {
                                            $options['alt'] = $mediaConfig['alt'];
                                        }
                                        $this->galleryManager->addMediaToGallery($media, $gallery, $options);
                                    }
                                } else {
                                    $this->log("Media " . $mediaConfig['filename'] . " not found.", "warning");
                                }
                            }
                        }
                    } else {
                        $this->moduleManager->addParameter($identifier, $value);
                    }
                }
            }
        }
    }

    private function generateFixtureModuleIdentifier(Modulable $modulable, array $moduleConfig, string $locale, int $position): string
    {
        return sprintf(
            '%s_%s_%s_fixture%s',
            $modulable->getSlug(),
            $locale,
            $moduleConfig['zone'],
            $position+1
        );
    }

    /**
     * @return void
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ReflectionException
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    private function publishVersionableResources(): void
    {
        foreach ($this->flushedResources as $flushedResource) {
            if ($flushedResource instanceof Versionable) {
                //@TODO: Check if not LanguageEnableable
                foreach ($flushedResource->getLanguages() as $language) {
                    $locale = $language->getId();
                    $draft = $flushedResource->getDraftVersion();
                    $draftHashes = $draft->getDraftHashes();
                    $versionHash = array_key_exists($locale, $draftHashes) && array_key_exists('hash', $draftHashes[$locale])
                        ? $draftHashes[$locale]['hash']
                        : $locale."-fixture";
                    $this->publicationManager->publish($flushedResource, $locale, $versionHash);
                }
            }
        }
    }

    /**
     * @param string $name
     * @param int    $offset
     *
     * @return string
     */
    private function getSlug(string $name, int $offset = 1): string
    {
        $slugger = new AsciiSlugger();
        $slug = strtolower(UnicodeString::unwrap([$slugger->slug($name)])[0]);
        if (array_key_exists($slug, $this->flushedResources)) {
            $slug .= '-' . $offset;
            if (array_key_exists($slug, $this->flushedResources)) {
                $offset++;
                return $this->getSlug($name, $offset);
            }
        }
        return $slug;
    }

    /**
     * @return void
     * @throws Exception
     */
    private function generateResources(): void
    {
        foreach ($this->resources as $resource) {
            $entity = $this->createEntity($resource, $this->configurationBag->getContentClass());
            $this->configureEntity($resource, $entity);
            $draft = $this->createAndConfigureDraft($resource, $entity);
            $this->processPropertiesTranslations($resource, $entity, $draft);
            $this->processHashes($entity, $draft);

            $slug = $entity->getSlug();
            $this->flushedResources[$slug] = $entity;

            if (null !== $draft) {
                $this->manager->persist($draft);
            }
            $this->manager->persist($entity);
            $this->manager->flush();
        }
    }
}
