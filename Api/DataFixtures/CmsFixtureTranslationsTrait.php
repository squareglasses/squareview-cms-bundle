<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataFixtures;

use Exception;
use JsonException;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\LanguageEnableable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\DataFixtures\Model\CmsResourceFixture;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\DataMigrations\Model\AbstractCmsMigration;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Translation\Exception\InvalidCatalogTypeException;
use SG\CmsBundle\Api\Translation\Generator\TranslationKeyGenerator;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Trait CmsFixtureTranslationsTrait
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
trait CmsFixtureTranslationsTrait
{
    protected array $layoutGuideTranslations = [];
    protected array $translations = [];

    /**
     * @param PendingTranslation $translation
     * @return AbstractCmsMigration|AbstractCmsFixtures|CmsFixtureTranslationsTrait
     */
    private function addTranslation(PendingTranslation $translation): self
    {
        $this->translations[] = $translation;
        return $this;
    }

    /**
     * @param CmsResourceInterface      $resource
     * @param CmsResourceInterface      $entity
     * @param Translatable|Version|null $draft
     *
     * @return void
     */
    private function processPropertiesTranslations(
        CmsResourceInterface $resource,
        CmsResourceInterface $entity,
        Translatable|Version|null $draft = null
    ): void {
        foreach ($resource->getTranslations() as $properties) {
            foreach ($properties as $property => $value) {
                $methodName = "set" . ucfirst($property);
                $computedName = null;
                if ($entity instanceof Translatable && method_exists($entity, $methodName)) {
                    $computedName = TranslationKeyGenerator::generateKey($entity, $property);
                } elseif (null !== $draft && $entity instanceof Versionable && method_exists($draft, $methodName)) {
                    $computedName = TranslationKeyGenerator::generateKey($draft, $property);
                } elseif ($entity instanceof Versionable) {
                    $this->log("Property " . $property . " not found in class "
                        . get_class($entity) . " or in class " . get_class($draft) . " .", "warning");
                } else {
                    $this->log("Property " . $property . " not found in class "
                        . get_class($entity) . ".", "warning");
                }
                if (null !== $computedName) {
                    $this->setPropertyValue($property, $computedName, $entity, $draft);
                }
            }
        }
    }

    /**
     * @return void
     * @throws InvalidCatalogTypeException
     * @throws JsonException
     * @throws ResourceGuideNotFoundException
     */
    private function prepareResourcesTranslations(): void
    {
        /** @var CmsResourceFixture $resource */
        foreach ($this->resources as $resource) {
            $flushedResource = $this->getFlushedResource($resource);

            // Translate properties (resource translations)
            if ($flushedResource instanceof Translatable) {
                $this->translatePropertiesForTranslatable($flushedResource, $resource);
            }
            if ($flushedResource instanceof Versionable) {
                $draft = $flushedResource->getDraftVersion();
                if ($draft instanceof Translatable) {
                    $this->translatePropertiesForTranslatable($draft, $resource);
                }
            }

            // Translate resource guide
            $guide = $this->guideProvider->getGuide($resource->getGuide());
            if (null === $guide) {
                return;
            }
            // Getting resource_guide translations
            foreach ($resource->getGuideTranslations() as $locale => $zones) {
                foreach ($zones as $zoneName => $translations) {
                    try {
                        $guide->getZone($zoneName);
                    } catch (Exception) {
                        // @TODO throw a warning instead of an exception cause exception block fixtures loading, and frontend config is not updatable after
                        throw new RuntimeException("Zone '" . $zoneName . "' is not configured for guide '" . $resource->getGuide() . "'.");
                    }
                    $guidable = $flushedResource;
                    if ($flushedResource instanceof Versionable) {
                        $guidable = $flushedResource->getDraftVersion();
                    }
                    foreach ($translations as $key => $value) {
                        $this->addTranslation((new PendingTranslation())
                            ->setKey($key)
                            ->setMessage($value)
                            ->setLabel($locale === $this->configurationBag->getDefaultLocale() ? (string)$value : null)
                            ->setLocale($locale)
                            ->setGuide($guide->getName())
                            ->setZone($zoneName)
                            ->setType(TranslationKey::TYPE_RESOURCE_GUIDE)
                            ->setResourceClass($guidable->getResourceClass())
                            ->setResourceIdentifier($guidable->getResourceIdentifier()));
                    }
                }
            }
        }
    }

    /**
     * @param bool $invalidateCache
     * @return void
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     */
    private function publishLayoutGuideTranslations(bool $invalidateCache = false): void
    {
        $tags = [];
        foreach ($this->layoutGuideTranslations as $guideIdentifier => $locales) {
            $tags[] = $guideIdentifier;
            $guide = $this->guideProvider->getGuide($guideIdentifier);
            if (null === $guide) {
                throw new ResourceGuideNotFoundException($guideIdentifier);
            }

            foreach ($locales as $locale) {
                $this->publicationManager->publishLayoutGuideTranslations($guideIdentifier, $locale);
            }
        }
        $this->manager->flush();

        if ($invalidateCache === true) {
            $this->cacheManager->invalidateCacheTags($tags);
        }
    }

    /**
     * @param Translatable       $translatable
     * @param CmsResourceInterface $resource
     *
     * @return void
     * @throws InvalidCatalogTypeException
     * @throws Exception
     */
    private function translatePropertiesForTranslatable(Translatable $translatable, CmsResourceInterface $resource): void
    {
        foreach ($resource->getTranslations() as $locale => $properties) {
            foreach ($properties as $property => $value) {
                $computedName = TranslationKeyGenerator::generateKey($translatable, $property);
                $this->addTranslation((new PendingTranslation())
                    ->setKey($computedName)
                    ->setMessage($value)
                    ->setLabel($value)
                    ->setLocale($locale)
                    ->setGuide($translatable instanceof Guidable ? $translatable->getGuide() : null)
                    ->setZone("properties")
                    ->setType(TranslationKey::TYPE_RESOURCE)
                    ->setResourceClass($translatable->getResourceClass())
                    ->setResourceIdentifier($translatable->getResourceIdentifier()));
            }

            if ($translatable instanceof LanguageEnableable) {
                $this->addLanguage($translatable, $locale);
            }
            if ($translatable instanceof Version && $translatable->getVersionable() instanceof LanguageEnableable) {
                $this->addLanguage($translatable->getVersionable(), $locale);
            }
        }
    }

    /**
     * @param string      $guideIdentifier
     * @param string      $locale
     * @param string|null $zone
     * @param array       $translations
     *
     * @return void
     * @throws ResourceGuideNotFoundException
     * @throws JsonException
     */
    protected function saveLayoutGuideTranslations(string $guideIdentifier, string $locale, ?string $zone = null, array $translations = []): void
    {
        if (null === $zone) {
            $zone = "default";
        }

        $guide = $this->guideProvider->getGuide($guideIdentifier);
        if (null === $guide) {
            throw new ResourceGuideNotFoundException($guideIdentifier);
        }
        try {
            $guide->getZone($zone);
        } catch (Exception) {
            throw new RuntimeException("Zone '" . $zone . "' is not configured for guide '" . $guideIdentifier . "'.");
        }

        if (!array_key_exists($guideIdentifier, $this->layoutGuideTranslations)) {
            $this->layoutGuideTranslations[$guideIdentifier] = [];
        }

        foreach ($translations as $key => $message) {
            $label = null;
            if (is_array($message)) {
                if (!array_key_exists("message", $message)) {
                    throw new RuntimeException("No message key in translation array.");
                }
                if (array_key_exists("label", $message)) {
                    $label = $message['label'];
                }
                $message = $message['message'];
            }
            if (null === $label && $locale === $this->configurationBag->getDefaultLocale()) {
                $label = $message;
            }

            if (!in_array($locale, $this->layoutGuideTranslations[$guideIdentifier])) {
                $this->layoutGuideTranslations[$guideIdentifier][] = $locale;
            }

            $this->addTranslation((new PendingTranslation())
                ->setKey($key)
                ->setMessage($message)
                ->setLabel($label)
                ->setLocale($locale)
                ->setGuide($guideIdentifier)
                ->setZone($zone)
                ->setType(TranslationKey::TYPE_LAYOUT_GUIDE)
                ->setResourceClass(null)
                ->setResourceIdentifier(null));
        }

    }
}
