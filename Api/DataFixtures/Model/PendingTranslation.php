<?php
/*
* This file is part of the SquareView package.
*
* (c) Square GLasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataFixtures\Model;

/**
 * Class PendingTranslation
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PendingTranslation
{
    private string $locale;
    private string $key;
    private string $message;
    private ?string $label = null;
    private string $type;
    private string $zone;
    private ?string $resourceClass = null;
    private ?int $resourceIdentifier = null;
    private ?string $guide = null;

    /**
     * @return string
     */
    public function getLocale(): string
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     *
     * @return PendingTranslation
     */
    public function setLocale(string $locale): PendingTranslation
    {
        $this->locale = $locale;
        return $this;
    }

    /**
     * @return string
     */
    public function getZone(): string
    {
        return $this->zone;
    }

    /**
     * @param string $zone
     *
     * @return PendingTranslation
     */
    public function setZone(string $zone): PendingTranslation
    {
        $this->zone = $zone;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string
    {
        return $this->key;
    }

    /**
     * @param string $key
     *
     * @return PendingTranslation
     */
    public function setKey(string $key): PendingTranslation
    {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @param string $message
     *
     * @return PendingTranslation
     */
    public function setMessage(string $message): PendingTranslation
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLabel(): ?string
    {
        return $this->label;
    }

    /**
     * @param string|null $label
     *
     * @return PendingTranslation
     */
    public function setLabel(?string $label): PendingTranslation
    {
        if (null !== $label) {
            $label = strip_tags($label);
            if (strlen($label) > 64) {
                $label = mb_substr($label, 0, 64, 'utf-8')."...";
            }
        }
        $this->label = $label;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getResourceClass(): ?string
    {
        return $this->resourceClass;
    }

    /**
     * @param string|null $resourceClass
     *
     * @return PendingTranslation
     */
    public function setResourceClass(?string $resourceClass): PendingTranslation
    {
        $this->resourceClass = $resourceClass;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getResourceIdentifier(): ?int
    {
        return $this->resourceIdentifier;
    }

    /**
     * @param int|null $resourceIdentifier
     *
     * @return PendingTranslation
     */
    public function setResourceIdentifier(?int $resourceIdentifier): PendingTranslation
    {
        $this->resourceIdentifier = $resourceIdentifier;
        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return PendingTranslation
     */
    public function setType(string $type): PendingTranslation
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getGuide(): ?string
    {
        return $this->guide;
    }

    /**
     * @param string|null $guide
     *
     * @return PendingTranslation
     */
    public function setGuide(?string $guide): PendingTranslation
    {
        $this->guide = $guide;
        return $this;
    }
}
