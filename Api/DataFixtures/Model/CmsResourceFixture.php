<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataFixtures\Model;

use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\CmsResourceTrait;

class CmsResourceFixture implements CmsResourceInterface
{
    use CmsResourceTrait;

    private ?string $class = null;
    private string $discriminator;
    private array $properties = [];
    private string $name;
    private string $guide = "default";
    private ?CmsResourceFixture $parent = null;
    private ?object $parentEntity = null;
    private array $routes = [];
    private array $translations = [];
    private array $guideTranslations = [];
    private array $metatags = [];
    private array $languages = [];
    private array $medias = [];
    private array $modules = [];
    private ?string $referenceName = null;
    private ?string $slug = null;
    private array $galleries = [];
    private ?int $position = null;

    /**
     * @return string|null
     */
    public function getSlug(): ?string
    {
        return $this->slug;
    }

    /**
     * @param string|null $slug
     * @return $this
     */
    public function setSlug(?string $slug): self
    {
        $this->slug = $slug;
        return $this;
    }


    /**
     * @return string|null
     */
    public function getClass(): ?string
    {
        return $this->class;
    }

    /**
     * @param string|null $class
     * @return $this
     */
    public function setClass(?string $class): self
    {
        $this->class = $class;
        return $this;
    }

    /**
     * @return array
     */
    public function getProperties(): array
    {
        return $this->properties;
    }

    /**
     * @param array $properties
     * @return $this
     */
    public function setProperties(array $properties): self
    {
        $this->properties = $properties;
        return $this;
    }

    /**
     * @return string
     */
    public function getDiscriminator(): string
    {
        return $this->discriminator;
    }

    /**
     * @param string $discriminator
     * @return $this
     */
    public function setDiscriminator(string $discriminator): self
    {
        $this->discriminator = $discriminator;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getGuide(): string
    {
        return $this->guide;
    }

    /**
     * @param string $guide
     * @return $this
     */
    public function setGuide(string $guide): self
    {
        $this->guide = $guide;
        return $this;
    }

    /**
     * @return CmsResourceFixture|null
     */
    public function getParent(): ?CmsResourceFixture
    {
        return $this->parent;
    }

    /**
     * @param CmsResourceFixture|null $parent
     * @return $this
     */
    public function setParent(?CmsResourceFixture $parent): self
    {
        $this->parent = $parent;
        return $this;
    }

    /**
     * @return array
     */
    public function getRoutes(): array
    {
        return $this->routes;
    }

    /**
     * @param array $routes
     */
    public function setRoutes(array $routes): void
    {
        $this->routes = $routes;
    }

    /**
     * @param string $locale
     * @param array  $routeConfig
     *
     * @return $this
     */
    public function addRoute(string $locale, array $routeConfig): self
    {
        $this->routes[$locale] = $routeConfig;
        return $this;
    }

    /**
     * @return array
     */
    public function getTranslations(): array
    {
        return $this->translations;
    }

    /**
     * @param array $translations
     */
    public function setTranslations(array $translations): void
    {
        $this->translations = $translations;
    }

    /**
     * @param string $locale
     * @param string $property
     * @param string $value
     * @return $this
     */
    public function addResourceTranslation(string $locale, string $property, string $value): self
    {
        if (!array_key_exists($locale, $this->translations)) {
            $this->translations[$locale] = [];
        }
        $this->translations[$locale][$property] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getGuideTranslations(): array
    {
        return $this->guideTranslations;
    }

    /**
     * @param string $locale
     * @param string $zone
     * @param string $key
     * @param string $value
     * @return $this
     */
    public function addResourceGuideTranslation(string $locale, string $zone, string $key, string $value): self
    {
        if (!array_key_exists($locale, $this->guideTranslations)) {
            $this->guideTranslations[$locale] = [];
        }
        if (!array_key_exists($zone, $this->guideTranslations[$locale])) {
            $this->guideTranslations[$locale][$zone] = [];
        }
        $this->guideTranslations[$locale][$zone][$key] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getMetatags(): array
    {
        return $this->metatags;
    }

    /**
     * @param array $metatags
     */
    public function setMetatags(array $metatags): void
    {
        $this->metatags = $metatags;
    }

    /**
     * @param string $locale
     * @param string $tagName
     * @param string $value
     * @return $this
     */
    public function addMetaTag(string $locale, string $tagName, string $value): self
    {
        if (!array_key_exists($locale, $this->metatags)) {
            $this->metatags[$locale] = [];
        }
        $this->metatags[$locale][$tagName] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function getMedias(): array
    {
        return $this->medias;
    }

    /**
     * @param array $medias
     */
    public function setMedias(array $medias): void
    {
        $this->medias = $medias;
    }

    /**
     * @param string $providerName
     * @param string $filename
     * @param string $identifier
     * @param string|null $alt
     * @param array $options
     * @return $this
     */
    public function addMedia(
        string $providerName,
        string $filename,
        string $identifier,
        ?string $alt = null,
        array $options = []
    ): self {
        $this->medias[] = [
            "providerName"  => $providerName,
            "filename"      => $filename,
            "identifier"    => $identifier,
            "alt"           => $alt,
            "options"       => $options
        ];
        return $this;
    }

    /**
     * @return array
     */
    public function getModules(): array
    {
        return $this->modules;
    }

    /**
     * @param array $modules
     */
    public function setModules(array $modules): void
    {
        $this->modules = $modules;
    }

    /**
     * @param string $locale
     * @param string $guideName
     * @param string $zone
     * @param array $parameters
     * @return $this
     */
    public function addModule(string $locale, string $guideName, string $zone, array $parameters = []): self
    {
        if (!array_key_exists($locale, $this->modules)) {
            $this->modules[$locale] = [];
        }
        $this->modules[$locale][] = [
            "guide"         => $guideName,
            "zone"          => $zone,
            "parameters"    => $parameters
        ];
        return $this;
    }

    /**
     * @return string|null
     */
    public function getReferenceName(): ?string
    {
        return $this->referenceName;
    }

    /**
     * @param string|null $referenceName
     * @return $this
     */
    public function setReferenceName(?string $referenceName): self
    {
        $this->referenceName = $referenceName;
        return $this;
    }

    /**
     * @return array
     */
    public function getLanguages(): array
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     * @return $this
     */
    public function setLanguages(array $languages): self
    {
        $this->languages = $languages;
        return $this;
    }

    public function addLanguage(string $locale): self
    {
        $this->languages[] = $locale;
        return $this;
    }

    /**
     * @return array
     */
    public function getGalleries(): array
    {
        return $this->galleries;
    }

    /**
     * @param array $galleries
     * @return $this
     */
    public function setGalleries(array $galleries): self
    {
        $this->galleries = $galleries;
        return $this;
    }

    /**
     * @param string $identifier
     * @param array $config
     * @return $this
     */
    public function addGallery(string $identifier, array $config): self
    {
        $this->galleries[$identifier] = $config;
        return $this;
    }

    /**
     * @return object|null
     */
    public function getParentEntity(): ?object
    {
        return $this->parentEntity;
    }

    /**
     * @param object|null $parentEntity
     * @return $this
     */
    public function setParentEntity(?object $parentEntity): self
    {
        $this->parentEntity = $parentEntity;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPosition(): ?int
    {
        return $this->position;
    }

    /**
     * @param int|null $position
     * @return $this
     */
    public function setPosition(?int $position): self
    {
        $this->position = $position;
        return $this;
    }
}
