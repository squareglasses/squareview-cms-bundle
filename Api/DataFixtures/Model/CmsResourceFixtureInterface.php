<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\DataFixtures\Model;

use Doctrine\Persistence\ObjectManager;

/**
 * Interface CmsResourceFixtureInterface
 * @package SG\CmsBundle\Api\DataFixtures
 * @author Florent Chaboud <florent@squareglasses.com>
 */
interface CmsResourceFixtureInterface
{
    /**
     * @param ObjectManager $manager
     * @return void
     */
    public function process(ObjectManager $manager): void;
}
