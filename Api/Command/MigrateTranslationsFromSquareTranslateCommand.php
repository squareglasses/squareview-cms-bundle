<?php

namespace SG\CmsBundle\Api\Command;

use App\Entity\ContentVersion;
use App\Entity\Content;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use JsonException;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\DataFixtures\Model\PendingTranslation;
use SG\CmsBundle\Api\Doctrine\Annotation\EntityTranslationPropertiesExtractor;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Entity\Language;
use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Entity\ModuleParameter;
use SG\CmsBundle\Api\Entity\TranslationKey;
use SG\CmsBundle\Api\Manager\PublicationManager;
use SG\CmsBundle\Api\Translation\Exception\ResourceNotFoundException;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationClient;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\Cache\CacheManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Output\ConsoleSectionOutput;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class MigrateTranslationsFromSquareTranslateCommand
 * @package SG\CmsBundle\Api\Command
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
class MigrateTranslationsFromSquareTranslateCommand extends Command
{
    protected static $defaultDescription = 'Migrate translations from Square Translate to project database';

    private int $timer;
    protected ?SymfonyStyle $io = null;
    private ConsoleSectionOutput $section;
    private ?ProgressBar $progress = null;
    private OutputInterface $output;
    private InputInterface $input;

    protected string $memoryLimit = "8192M";
    protected string $maxExecutionTime = "180000";

    /**
     * @param EntityManagerInterface $em
     * @param TranslationManager $translationManager
     * @param TranslationClient $translationClient
     * @param ResourceGuideProvider $resourceGuideProvider
     * @param PublicationManager $publicationManager
     * @param CacheManager $cacheManager
     * @param EntityTranslationPropertiesExtractor $propertiesExtractor
     */
    public function __construct(
        private EntityManagerInterface $em,
        private TranslationManager $translationManager,
        private TranslationClient $translationClient,
        private ResourceGuideProvider $resourceGuideProvider,
        private PublicationManager $publicationManager,
        private CacheManager $cacheManager,
        private readonly EntityTranslationPropertiesExtractor $propertiesExtractor,
    )
    {
        parent::__construct('translations:migrate');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface
     * @throws NonUniqueResultException
     * @throws TranslationClientException
     * @throws ResourceGuideNotFoundException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output       = $output;
        $this->input        = $input;
        $this->io           = new SymfonyStyle($input, $output);

        $this->io->title("Synchronizing layout guide translations..." );

        $guides = $this->resourceGuideProvider->getAvailableGuides();
        $tags = [];
        foreach (array_keys($guides) as $guide) {
            $sqTranslateLayoutGuideTranslations = $this->translationClient->searchTranslations(null, $guide,null,TranslationKey::TYPE_LAYOUT_GUIDE, $guide);

            if (count($sqTranslateLayoutGuideTranslations) > 0) {
                $this->start(" // Synchronize translations for " . $guide . "...", count($sqTranslateLayoutGuideTranslations));
                $translations = [];
                $locales = [];
                $tags[] = $guide;

                foreach ($sqTranslateLayoutGuideTranslations as $translation) {
                    $translations[] = (new PendingTranslation())
                        ->setKey($translation["key"])
                        ->setMessage($translation["message"])
                        ->setLabel($translation["label"])
                        ->setLocale($translation["locale"])
                        ->setGuide($translation["domain"])
                        ->setZone($translation["zone"])
                        ->setType(TranslationKey::TYPE_LAYOUT_GUIDE)
                        ->setResourceClass(null)
                        ->setResourceIdentifier(null);
                    $this->progress->advance();

                    if (!in_array($translation["locale"], $locales)) {
                        $locales[] = $translation["locale"];
                    }
                }
                $this->translationManager->multipleTranslate($translations);

                foreach ($locales as $locale) {
                    $this->publicationManager->publishLayoutGuideTranslations($guide, $locale, true);
                }
                $this->end(" // Layout guide translations for " . $guide . "... OK");
            }
        }

        $this->io->success("Layout guide translations synchronization done !");

        $this->io->title("Synchronize resource guide translations..." );
        $classNames = $this->em->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        foreach ($classNames as $className) {

            if (in_array(Guidable::class, class_implements($className))) {
                $resources = $this->getAllResources($className);

                /** @var Guidable $resource */
                foreach ($resources as $resource) {
                    $sqTranslateResourceGuideTranslations = $this->translationClient->searchTranslations(null, $resource->getSlug(), null, TranslationKey::TYPE_RESOURCE_GUIDE, $resource->getDiscriminator());

                    if (count($sqTranslateResourceGuideTranslations) > 0) {
                        $this->start(" // Synchronize translations for " . $resource->getSlug() . "...", count($sqTranslateResourceGuideTranslations));
                        $translations = [];
                        foreach ($sqTranslateResourceGuideTranslations as $translation) {
                            $translations[] = (new PendingTranslation())
                                ->setKey($translation["key"])
                                ->setMessage($translation["message"])
                                ->setLabel($translation["label"])
                                ->setLocale($translation["locale"])
                                ->setGuide($resource->getGuide())
                                ->setZone($translation["zone"])
                                ->setType(TranslationKey::TYPE_RESOURCE_GUIDE)
                                ->setResourceClass($resource->getResourceClass())
                                ->setResourceIdentifier($resource->getResourceIdentifier());
                            $this->progress->advance();
                        }
                        if (!in_array($resource->getGuide(), $tags)) {
                            $tags[] = $resource->getGuide();
                        }

                        $this->translationManager->multipleTranslate($translations);

                        $this->end(" // Resource guide translations for " . $resource->getSlug() . "... OK");

                    }
                }
            }
        }

        $this->io->section("Fixing translations not being imported");
        $contents = $this->em->getRepository(Content::class)->findAll();

        $this->start(" // Fixing translations for Contents...", count($contents));
        /** @var Content $content */
        foreach ($contents as $content) {
            $draft = $content->getDraftVersion();
            $guideTranslations = [];
            $resourceGuideTranslations = $this->translationManager->getResourceGuideTranslations($draft);
            foreach ($resourceGuideTranslations as $guideTranslation) {
                if (!array_key_exists($guideTranslation['locale'], $guideTranslations)) {
                    $guideTranslations[$guideTranslation['locale']] = [];
                }
                if (!array_key_exists($guideTranslation['zone'], $guideTranslations[$guideTranslation['locale']])) {
                    $guideTranslations[$guideTranslation['locale']][$guideTranslation['zone']] = [];
                }
                $guideTranslations[$guideTranslation['locale']][$guideTranslation['zone']][$guideTranslation['key']] = $guideTranslation['message'];
            }
            $translations = [];
            foreach ($guideTranslations as $loc => $guideTranslation) {
                $translations[$loc]['guide'] = $guideTranslation;
            }

            $languages = $this->em->getRepository(Language::class)->findAll();
            /** @var Language $language */
            foreach ($languages as $language) {
                $this->fixModuleParameterTranslations($content, $draft, $language->getId());
                $this->fixMetatagTranslations($content, $draft, $language->getId());

                $currentVersion = $content->getCurrentVersion($language->getId());
                if ($currentVersion) {
                    $this->fixModuleParameterTranslations($content, $currentVersion, $language->getId());
                    $this->fixMetatagTranslations($content, $currentVersion, $language->getId());

                    if ($translations) {
                        $currentVersion->setTranslations($translations);
                        $this->em->persist($currentVersion);
                        $this->em->flush();
                    }
                }
            }
            $this->progress->advance();
        }
        $this->end(" // Fixing translations for Contents... OK");

        $this->io->success("Resource guide translations synchronization done !");

        $this->io->title("Synchronize resource translations..." );

        $discriminators = [];
        foreach ($classNames as $className) {
            if (in_array(Translatable::class, class_implements($className))) {
                $properties = $this->propertiesExtractor->extractTranslationProperties($className);
                if ($properties) {
                    $resources = $this->getAllResources($className);
                    $this->start(" // Synchronize translations for " . $className . "...", count($resources));

                    foreach ($resources as $resource) {
                        $discriminators[$resource->getDiscriminator()] = $className;
                        $results = $this->translationClient->searchTranslations(null, $resource->getSlug(), null, TranslationKey::TYPE_RESOURCE, $resource->getDiscriminator());

                        // Don't import modules and metatags as translations because they are versionable
                        $sqTranslateResourceTranslations = array_filter($results, function ($t) {
                            return $t["zone"] !== "modules" && $t["zone"] !== "metatags";
                        });

                        if (count($sqTranslateResourceTranslations) > 0) {
                            $translations = [];

                            foreach ($sqTranslateResourceTranslations as $translation) {
                                $discriminator = explode(".", $translation["key"])[0];
                                $slug = explode(".", $translation["key"])[1];

                                $data = (new PendingTranslation())
                                    ->setKey($translation["key"])
                                    ->setLabel($translation["label"])
                                    ->setLocale($translation["locale"])
                                    ->setZone($translation["zone"])
                                    ->setType(TranslationKey::TYPE_RESOURCE);

                                // Handle Bucol bug where product_color translations got product discriminator
                                if ($resource->getDiscriminator() === $discriminator || $translation["zone"] !== "properties") {
                                    $data
                                        ->setResourceClass($resource->getResourceClass())
                                        ->setResourceIdentifier($resource->getResourceIdentifier())
                                    ;
                                } else {
                                    $res = $this->em->getRepository($discriminators[$discriminator])->findOneBySlug($slug);
                                    if(!$res) {
                                        throw new ResourceNotFoundException();
                                    }
                                    $data
                                        ->setResourceClass($res->getResourceClass())
                                        ->setResourceIdentifier($res->getResourceIdentifier())
                                    ;
                                }

                                if ($translation["message"] !== "NULL") {
                                    $data->setMessage($translation["message"]);
                                } else {
                                    $data->setMessage("");
                                }
                                if ($resource instanceof Guidable) {
                                    $data->setGuide($resource->getGuide());
                                    if (!in_array($resource->getGuide(), $tags)) {
                                        $tags[] = $resource->getGuide();
                                    }
                                }
                                $translations[] = $data;
                            }

                            $this->translationManager->multipleTranslate($translations);

                        }
                        $this->progress->advance();
                    }
                    $this->end(" // Resource translations for " . $className . "... OK");
                }
            }
        }
        $this->io->success("Resource translations synchronization done !");

        $this->io->text("Removing cache…");
        $this->cacheManager->invalidateCacheTags($tags);
        $this->io->text("Removing cache OK");

        return Command::SUCCESS;
    }

    private function getAllResources(string $className): array
    {
        if (in_array(Version::class, class_implements($className))) {
            $resources = $this->em->getRepository($className)->findBy([
                'currentDraft' => true,
            ]);
        } else {
            $resources = $this->em->getRepository($className)->findAll();
        }
        return $resources;
    }

    /**
     * @param string $message
     * @param int|null $count
     */
    private function start(string $message, ?int $count = null): void
    {
        $this->startTimer();
        $this->section = $this->output->section();
        $this->section->writeln($message);
        if (null !== $count && !$this->isSilent()) {
            $sectionProgress = $this->output->section();
            $this->progress = new ProgressBar($sectionProgress);
            $this->progress->start($count);
        }
    }

    /**
     * @param string $message
     */
    private function end(string $message): void
    {
        if (!$this->isSilent() && null !== $this->progress)  $this->progress->finish();
        $this->section->overwrite($message." (".$this->getTimerValue()."s)");
        $this->progress = null;
    }

    /**
     * @return bool
     */
    private function isSilent(): bool
    {
        return $this->input->hasOption('silent') && $this->input->getOption('silent') !== false;
    }

    /**
     * Configure php custom limitations.
     */
    protected function initPhp()
    {
        ini_set('memory_limit', $this->memoryLimit);
        ini_set('max_execution_time', $this->maxExecutionTime);
    }

    private function startTimer(): void
    {
        $this->timer = hrtime(true);
    }

    /**
     * @return float
     */
    private function getTimerValue(): float
    {
        $timeEnd = hrtime(true);
        return round(($timeEnd - $this->timer)/1000000000, 2);
    }

    private function fixModuleParameterTranslations(Content $content, ContentVersion $version, string $locale)
    {
        $modules = $this->em->getRepository(Module::class)->findBy([
            "modulableIdentifier" => $version->getId(),
            "modulableClass" => ContentVersion::class
        ]);
        /** @var Module $module */
        foreach ($modules as $module) {
            $moduleTranslations = $this->translationClient->searchTranslations(
                $locale,
                $content->getSlug(),
                'modules', // All modules were set as 'modules' zone in SquareTranslate
                TranslationKey::TYPE_RESOURCE,
                $content->getDiscriminator()
            );

            $parameters = $module->getParameters();
            /** @var ModuleParameter $parameter */
            foreach ($parameters as $parameter) {
                $key = array_search($parameter->getValue(), array_column($moduleTranslations, 'key'));
                if ($key !== false) {
                    $parameter->setValue($moduleTranslations[$key]["message"]);
                    $this->em->persist($parameter);
                    $this->em->flush();
                }
            }
        }
    }

    private function fixMetatagTranslations(Content $content, ContentVersion $version, string $locale)
    {
        $metatags = $this->em->getRepository(MetaTag::class)->findBy([
            "resourceIdentifier" => $version->getId(),
            "resourceClass" => ContentVersion::class
        ]);
        /** @var MetaTag $metatag */
        foreach ($metatags as $metatag) {
            $metatagTranslations = $this->translationClient->searchTranslations(
                $locale,
                $content->getSlug(),
                'metatags', // All modules were set as 'modules' zone in SquareTranslate
                TranslationKey::TYPE_RESOURCE,
                $content->getDiscriminator()
            );

            $key = array_search($metatag->getContent(), array_column($metatagTranslations, 'key'));
            if ($key !== false) {
                $metatag->setContent($metatagTranslations[$key]["message"]);
                $this->em->persist($metatag);
                $this->em->flush();
            }
        }
    }
}
