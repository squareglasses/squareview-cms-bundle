<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use SG\CmsBundle\Api\Entity\MetaTag;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use SG\CmsBundle\Api\Contracts\MetaTaggable;

/**
 * Class MetaTagManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MetaTagManager
{
    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @param MetaTaggable $resource
     * @param string       $name
     * @param string       $content
     * @param string       $locale
     * @param string       $type
     *
     * @return void
     */
    public function addMetaTagToResource(
        MetaTaggable $resource,
        string $name,
        string $content,
        string $locale,
        string $type = "name"
    ): void {
        $metaTag = new MetaTag();
        $metaTag->setResourceIdentifier($resource->getResourceIdentifier());
        $metaTag->setResourceClass($resource->getResourceClass());
        $metaTag->setName($name);
        $metaTag->setLocale($locale);
        $metaTag->setType($type);
        $metaTag->setContent($content);

        $this->em->persist($metaTag);
        $this->em->flush();
    }

    /**
     * @param MetaTaggable $resource
     * @param string       $locale
     * @param bool         $arrayResults
     *
     * @return array
     */
    public function findByResource(MetaTaggable $resource, string $locale = 'fr', bool $arrayResults = false): array
    {
        /** @var EntityRepository $repo */
        $repo = $this->em->getRepository(MetaTag::class);
        $qb = $repo
            ->createQueryBuilder("m")
            ->where("m.resourceIdentifier = :resourceIdentifier")
            ->andWhere("m.resourceClass = :resourceClass")
            ->andWhere("m.locale = :locale")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->setParameter("locale", $locale);
        if ($arrayResults) {
            return $qb->getQuery()->getArrayResult();
        }

        return $qb->getQuery()->getResult();
    }

    /**
     * @param MetaTag $metaTag
     *
     * @return MetaTaggable|null
     */
    public function findResource(MetaTag $metaTag): ?MetaTaggable
    {
        return $this->em->getRepository($metaTag->getResourceClass())
            ->find($metaTag->getResourceIdentifier());
    }

    /**
     * @param MetaTaggable $resource
     * @param string       $locale
     *
     * @return void
     */
    public function loadMetaTags(MetaTaggable $resource, string $locale): void
    {
        $metatags = $this->findByResource($resource, $locale);
        $resource->setMetatags($metatags);
    }

    /**
     * @param MetaTag $metaTag
     *
     * @return void
     */
    public function deleteMetaTag(MetaTag $metaTag): void
    {
        $this->em->remove($metaTag);
        $this->em->flush();
    }

    /**
     * Deletes all metatag records for the given MetaTaggable resource.
     *
     * @param MetaTaggable $resource
     *
     * @return void
     */
    public function deleteAllMetaTagsForResource(MetaTaggable $resource): void
    {
        $metaTags = $this->em->createQueryBuilder()
            ->select('mt')
            ->from(MetaTag::class, 'mt')
            ->where('mt.resourceClass = :class')
            ->setParameter('class', $resource->getResourceClass())
            ->andWhere('mt.resourceIdentifier = :identifier')
            ->setParameter('identifier', $resource->getResourceIdentifier())
            ->getQuery()
            ->getResult();

        foreach ($metaTags as $metaTag) {
            $this->em->remove($metaTag);
        }
        $this->em->flush();
    }
}
