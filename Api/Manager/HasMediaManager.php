<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Entity\ResourceHasMedia;
use SG\CmsBundle\Api\Repository\ResourceHasMediaRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\Query\Expr\Join;


/**
 * Class HasMediaManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class HasMediaManager
{
    /**
     * @param EntityManagerInterface     $manager
     * @param ResourceHasMediaRepository $repository
     */
    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly ResourceHasMediaRepository $repository
    ) {
    }

    /**
     * @param Media              $media
     * @param HasMediasInterface $resource
     * @param array              $options
     * @param bool               $flush
     *
     * @return ResourceHasMedia
     */
    public function addMediaToResource(Media $media, HasMediasInterface $resource, array $options = [], bool $flush = true): ResourceHasMedia
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $options = $resolver->resolve($options);

        $resourceHasMedia = new ResourceHasMedia();
        $resourceHasMedia->setResourceClass($resource->getResourceClass());
        $resourceHasMedia->setResourceIdentifier($resource->getResourceIdentifier());
        $resourceHasMedia->setMedia($media);
        $resourceHasMedia->setIdentifier($options['identifier']);
        $resourceHasMedia->setName($options['display_name'] ?? $media->getProviderReference());
        $resourceHasMedia->setAlt($options['alt']);
        $this->manager->persist($resourceHasMedia);
        if ($flush) {
            $this->manager->flush();
        }

        return $resourceHasMedia;
    }

    /**
     * @param HasMediasInterface $resource
     *
     * @return array
     */
    public function getResourceMedias(HasMediasInterface $resource): array
    {
        return $this->repository->findByResource($resource);
    }

    /**
     * @param HasMediasInterface $resource
     *
     * @return void
     */
    public function removeResourceMedias(HasMediasInterface $resource): void
    {
        $this->repository->removeByResource($resource);
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     *
     * @return ResourceHasMedia|null
     * @throws NonUniqueResultException
     */
    public function getResourceMediaByIdentifier(HasMediasInterface $resource, string $identifier): ?ResourceHasMedia
    {
        return $this->repository->findByResourceAndIdentifier($resource, $identifier);
    }

    /**
     * @param HasMediasInterface $resource
     * @param string             $identifier
     *
     * @return bool
     * @throws NonUniqueResultException
     */
    public function resourceHasMediaByIdentifier(HasMediasInterface $resource, string $identifier): bool
    {
        return $this->repository->resourceHasMediaByIdentifier($resource, $identifier);
    }

    /**
     * Load contextual medias for the given HasMediasInterface resource
     *
     * @param HasMediasInterface $resource
     *
     * @return void
     */
    public function loadMedias(HasMediasInterface $resource): void
    {
        $resource->setMedias($this->getResourceMedias($resource));
    }

    /**
     * Deletes all resourceHasMedia records for the given HasMediaInterface resource.
     *
     * @param HasMediasInterface $resource
     *
     * @return void
     */
    public function deleteAllMediasForResource(HasMediasInterface $resource): void
    {
        $mediasList = $this->manager->createQueryBuilder()
            ->select('rhm')
            ->from(ResourceHasMedia::class, 'rhm')
            ->where('rhm.resourceClass = :class')
            ->setParameter('class', $resource->getResourceClass())
            ->andWhere('rhm.resourceIdentifier = :identifier')
            ->setParameter('identifier', $resource->getResourceIdentifier())
            ->getQuery()
            ->getResult();

        foreach ($mediasList as $rhm) {
            $this->manager->remove($rhm);
        }
        $this->manager->flush();
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setRequired("identifier");
        $resolver->setDefaults([
            'display_name'  => null,
            'alt'           => null
        ]);

        $resolver->setAllowedTypes('identifier', 'string');
        $resolver->setAllowedTypes('display_name', ['string', 'null']);
        $resolver->setAllowedTypes('alt', ['string', 'null']);
    }

    /**
     * @param ResourceHasMedia $resourceHasMedia
     *
     * @return HasMediasInterface|null
     */
    public function findResource(ResourceHasMedia $resourceHasMedia): ?HasMediasInterface
    {
        return $this->manager->getRepository($resourceHasMedia->getResourceClass())->findOneBy(['id' => $resourceHasMedia->getResourceIdentifier()]);
    }

    /**
     * @param string $format
     * @param string $resourceClass
     * @param array  $resourceIdentifiers
     *
     * @return Media|null
     * @throws NonUniqueResultException
     */
    public function getMediaByFormatAndResource(string $format, string $resourceClass, array $resourceIdentifiers = []): ?Media
    {
        $qb = $this->manager->createQueryBuilder()
            ->select('rhm')
            ->from(ResourceHasMedia::class, 'rhm')
            ->innerJoin("rhm.media", "m")
            ->where('rhm.identifier = :format')
            ->setParameter('format', $format)
            ->innerJoin($resourceClass, 'r', Join::WITH, 'rhm.resourceIdentifier = r.id');

        foreach ($resourceIdentifiers as $field => $value) {
            $qb->andWhere("r.".$field." = :value".$field)
                ->setParameter("value".$field, $value);
        }
        $rhm = $qb->getQuery()->getOneOrNullResult();
        if (null !== $rhm) {
            return $rhm->getMedia();
        }
        return null;
    }
}
