<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Exception;
use FFMpeg\Coordinate\TimeCode;
use FFMpeg\FFMpeg;
use League\Flysystem\FilesystemException;
use League\Flysystem\FilesystemOperator;
use RuntimeException;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Common\Media\Provider\Pool;
use SG\CmsBundle\Common\Media\Provider\ProviderInterface;
use SG\CmsBundle\Api\Repository\MediaRepository;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Process\Process;

/**
 * Class MediaManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class MediaManager
{
    /**
     * @param MediaRepository $repository
     * @param EntityManagerInterface $manager
     * @param Pool $pool
     * @param FilesystemOperator $localStorage
     * @param string $projectDir
     * @param string $localStorageDirectory
     */
    public function __construct(
        private readonly MediaRepository $repository,
        private readonly EntityManagerInterface $manager,
        private readonly Pool $pool,
        private readonly FilesystemOperator $localStorage,
        private string $projectDir,
        private string $localStorageDirectory,
    ) {
    }

    /**
     * @param array $criteria
     *
     * @return Media|null
     */
    public function findOneBy(array $criteria): ?Media
    {
        return $this->repository->findOneBy($criteria);
    }

    /**
     * @param string $providerName
     * @param string $filename
     * @param string|null $originalName
     *
     * @return Media
     * @throws FilesystemException
     */
    public function createMedia(string $providerName, string $filename, ?string $originalName = null): Media
    {
        $media = $this->findOneBy(array('providerReference' => $filename));
        if (!$media) {
            $media = new Media();
            $media->setProviderName($providerName);
            $media->setProviderReference($filename);
            $media->setOriginalName($originalName);

            /** @var ProviderInterface $provider */
            $provider = $this->pool->getProvider($providerName);
            $media = $provider->setMetadata($media, $filename);

            $this->manager->persist($media);
            $this->manager->flush();
        }

        if ($providerName === "video") {
            $this->createVideoThumbnail($media);
        }
        return $media;
    }

    /**
     * @param Media $media
     * @return void
     * @throws FilesystemException
     */
    public function createVideoThumbnail(Media $media):void
    {
        $directoryPath = $this->projectDir . $this->localStorageDirectory .'/';

        if (!$this->localStorage->directoryExists('video_thumbnails')) {
            $this->localStorage->createDirectory('video_thumbnails');
        }

        $thumbnailFileName = pathinfo($media->getProviderReference(), PATHINFO_FILENAME).'.jpg';
        $thumbnailPath = 'video_thumbnails/'. $thumbnailFileName;
        $ffmpeg = FFMpeg::create();
        $video = $ffmpeg->open($directoryPath . $media->getProviderReference());
        $video->frame(TimeCode::fromSeconds(0))
            ->save($directoryPath. $thumbnailPath);

        if ($this->localStorage->has($thumbnailPath)) {
            $metadatas = $media->getProviderMetadata();
            $metadatas["thumbnail"] = $thumbnailFileName;
            $media->setProviderMetadata($metadatas);
            $this->manager->persist($media);
            $this->manager->flush();
        }
    }

    /**
     * Add a unique string before the filename extension to make it unique
     *
     * @param string $originalFilename
     *
     * @return string
     */
    public static function getUniqueFilename(string $originalFilename): string
    {
        $fileArray = pathinfo($originalFilename);
        return $fileArray['filename'].'_'.uniqid('', false).'.'.$fileArray['extension'];
    }

    /**
     * @param Media $media
     *
     * @return void
     * @throws FilesystemException
     */
    public function deleteMedia(Media $media): void
    {
        try {
            $fileExists = $this->localStorage->has($media->getProviderReference());
            if (!$fileExists) {
                throw new RuntimeException(sprintf("Failed to find media %s in storage.", $media->getProviderReference()), Response::HTTP_BAD_REQUEST);
            }

            $this->localStorage->delete($media->getProviderReference());
            $this->manager->remove($media);
            $this->manager->flush();
        } catch (Exception $e) {
            throw new RuntimeException($e->getMessage(), $e->getCode());
        }
    }
}
