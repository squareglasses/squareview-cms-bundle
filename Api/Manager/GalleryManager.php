<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Contracts\GalleryInterface;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Entity\Gallery;
use SG\CmsBundle\Api\Entity\GalleryHasMedia;
use SG\CmsBundle\Api\Entity\Media;
use SG\CmsBundle\Api\Repository\GalleryRepository;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GalleryManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class GalleryManager
{
    /**
     * @param GalleryRepository      $repository
     * @param EntityManagerInterface $manager
     */
    public function __construct(private readonly GalleryRepository $repository, private readonly EntityManagerInterface $manager)
    {
    }

    /**
     * @param HasGalleriesInterface $resource
     *
     * @return array
     */
    public function getResourceGalleries(HasGalleriesInterface $resource): array
    {
        return $this->repository->findByResource($resource);
    }

    /**
     * @param HasGalleriesInterface $resource
     *
     * @return void
     */
    public function loadGalleries(HasGalleriesInterface $resource): void
    {
        $resource->setGalleries($this->getResourceGalleries($resource));
    }

    /**
     * @param Media            $media
     * @param GalleryInterface $gallery
     * @param array            $options
     *
     * @return GalleryHasMedia
     */
    public function addMediaToGallery(Media $media, GalleryInterface $gallery, array $options = []): GalleryHasMedia
    {
        $resolver = new OptionsResolver();
        $this->configureOptions($resolver);
        $options = $resolver->resolve($options);

        $galleryHasMedia = new GalleryHasMedia();
        $galleryHasMedia->setMedia($media);
        $galleryHasMedia->setName($options['display_name'] ?? $media->getProviderReference());
        $galleryHasMedia->setAlt($options['alt']);
        $galleryHasMedia->setGallery($gallery);
        $galleryHasMedia->setIdentifier($gallery->getIdentifier());
        $this->manager->persist($galleryHasMedia);

        return $galleryHasMedia;
    }

    /**
     * @param OptionsResolver $resolver
     *
     * @return void
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'display_name'  => null,
            'alt'           => null
        ]);

        $resolver->setAllowedTypes('display_name', ['string', 'null']);
        $resolver->setAllowedTypes('alt', ['string', 'null']);
    }

    /**
     * Deletes all gallery records for the given HasGalleriesInterface resource.
     *
     * @param HasGalleriesInterface $resource
     *
     * @return void
     */
    public function deleteAllGalleriesForResource(HasGalleriesInterface $resource): void
    {
        $galleries = $this->manager->createQueryBuilder()
            ->select('rhm')
            ->from(Gallery::class, 'g')
            ->where('g.resourceClass = :class')
            ->setParameter('class', $resource->getResourceClass())
            ->andWhere('g.resourceIdentifier = :identifier')
            ->setParameter('identifier', $resource->getResourceIdentifier())
            ->getQuery()
            ->getResult();

        foreach ($galleries as $gallery) {
            $this->manager->remove($gallery);
        }
        $this->manager->flush();
    }

    /**
     * @param string                $identifier
     * @param HasGalleriesInterface $resource
     * @param string|null           $name
     * @param bool                  $flush
     *
     * @return Gallery
     */
    public function createGallery(string $identifier, HasGalleriesInterface $resource, ?string $name = null, bool $flush = true): Gallery
    {
        $gallery = (new Gallery())
            ->setIdentifier($identifier)
            ->setName($name)
            ->setResourceIdentifier($resource->getResourceIdentifier())
            ->setResourceClass($resource->getResourceClass())
        ;
        if ($flush) {
            $this->manager->persist($gallery);
            $this->manager->flush();
        }
        return $gallery;
    }

    /**
     * @param GalleryInterface $gallery
     *
     * @return HasGalleriesInterface|null
     */
    public function getGalleryResource(GalleryInterface $gallery): ?HasGalleriesInterface
    {
        return $this->manager->getRepository($gallery->getResourceClass())
            ->findOneBy(['id' => $gallery->getResourceIdentifier()]);
    }
}
