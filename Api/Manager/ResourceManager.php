<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;

/**
 * Class ResourceManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ResourceManager
{
    /**
     * ResourceManager constructor.
     * @param EntityManagerInterface $manager
     * @param HasMediaManager $hasMediaManager
     * @param MetaTagManager $metaTagManager
     * @param ModuleManager $moduleManager
     * @param GalleryManager $galleryManager
     */
    public function __construct(
        private readonly EntityManagerInterface $manager,
        private readonly HasMediaManager $hasMediaManager,
        private readonly MetaTagManager $metaTagManager,
        private readonly ModuleManager $moduleManager,
        private readonly GalleryManager $galleryManager
    ) {
    }

    /**
     * @param $resource
     *
     * @return void
     */
    public function deleteResource($resource): void
    {
//        if ($resource instanceof Translatable) {
//            //@TODO: delete property translations in squaretranslate
//        }

        if ($resource instanceof HasMediasInterface) {
            $this->hasMediaManager->deleteAllMediasForResource($resource);
        }

        if ($resource instanceof MetaTaggable) {
            $this->metaTagManager->deleteAllMetaTagsForResource($resource);
        }

        if ($resource instanceof Modulable) {
            $this->moduleManager->deleteAllModulesForResource($resource);
        }

        if ($resource instanceof HasGalleriesInterface) {
            $this->galleryManager->deleteAllGalleriesForResource($resource);
        }

        if ($resource instanceof Versionable) {
            $versions = $resource->getVersions();

            /** @var Version $version */
            foreach ($versions as $version) {
                $this->manager->remove($version);
            }

            $this->manager->flush();
        }

        $this->manager->remove($resource);
        $this->manager->flush();
    }
}
