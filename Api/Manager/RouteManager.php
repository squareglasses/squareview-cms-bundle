<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\Mapping\MappingException;
use ReflectionException;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Repository\RouteRepository;
use SG\CmsBundle\Api\Entity\Route;

/**
 * Class RouteManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class RouteManager
{
    /**
     * @param RouteRepository $repository
     */
    public function __construct(private readonly RouteRepository $repository)
    {
    }

    /**
     * @param Routable $resource
     *
     * @return array
     * @throws MappingException
     * @throws ReflectionException
     */
    public function getRoutesByResource(Routable $resource): array
    {
        return $this->repository->findByResource($resource);
    }

    /**
     * @param Routable $resource
     * @param string   $locale
     *
     * @return Route|null
     * @throws MappingException
     * @throws NonUniqueResultException
     * @throws ReflectionException
     */
    public function getRouteByResource(Routable $resource, string $locale): ?Route
    {
        return $this->repository->findOneByResource($resource, $locale);
    }
}
