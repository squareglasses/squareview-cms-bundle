<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Contracts\Routable;
use SG\CmsBundle\Api\Entity\MenuItem;

/**
 * Class MenuItemManager
 *
 * @author Marie-Lou Perreau <marielou@squareglasses.com>
 */
class MenuItemManager
{

    /**
     * ModuleManager constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(private readonly EntityManagerInterface $manager)
    {
    }

    /**
     * @param MenuItem $menuItem
     *
     * @return Routable|null
     */
    public function findResource(MenuItem $menuItem): ?Routable
    {
        return $this->manager->getRepository($menuItem->getTargetResourceClass())->find($menuItem->getTargetResourceId());
    }
}
