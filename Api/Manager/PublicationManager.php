<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use DateTime;
use Doctrine\Common\Annotations\AnnotationReader;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\NonUniqueResultException;
use Exception;
use JsonException;
use ReflectionClass;
use ReflectionException;
use RuntimeException;
use SG\CmsBundle\Api\Contracts\CmsResourceInterface;
use SG\CmsBundle\Api\Contracts\Guidable;
use SG\CmsBundle\Api\Contracts\HasGalleriesInterface;
use SG\CmsBundle\Api\Contracts\HasMediasInterface;
use SG\CmsBundle\Api\Contracts\MetaTaggable;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\Translatable;
use SG\CmsBundle\Api\Doctrine\Annotation\ExcludeFromPublication;
use SG\CmsBundle\Api\Doctrine\Contracts\Version;
use SG\CmsBundle\Api\Doctrine\Contracts\Versionable;
use SG\CmsBundle\Api\Entity\Gallery;
use SG\CmsBundle\Api\Entity\TranslationsPublication;
use SG\CmsBundle\Api\Entity\MetaTag;
use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Entity\ModuleParameter;
use SG\CmsBundle\Api\Repository\TranslationsPublicationRepository;
use SG\CmsBundle\Api\Translation\Exception\TranslationClientException;
use SG\CmsBundle\Api\Translation\TranslationManager;
use SG\CmsBundle\Common\ResourceGuide\Exception\ResourceGuideNotFoundException;
use SG\CmsBundle\Common\ResourceGuide\Model\ResourceGuide;
use SG\CmsBundle\Common\ResourceGuide\ResourceGuideProvider;
use Symfony\Contracts\HttpClient\Exception\ClientExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\RedirectionExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\ServerExceptionInterface;
use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;

/**
 * Class PublicationManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class PublicationManager
{
    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly HasMediaManager $hasMediaManager,
        private readonly GalleryManager $galleryManager,
        private readonly MetaTagManager $metaTagManager,
        private readonly ModuleManager $moduleManager,
        private readonly TranslationManager $translationManager,
        private readonly ResourceGuideProvider $resourceGuideProvider
    ) {
    }

    /**
     * @param Versionable $versionable
     * @param string      $locale
     * @param string|null $hash
     *
     * @return Version
     * @throws ClientExceptionInterface
     * @throws JsonException
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     */
    public function publish(
        Versionable $versionable,
        string $locale,
        ?string $hash = null
    ): Version {
        if (method_exists($versionable, 'setEnabled') && !$versionable->getEnabled()) {
            $versionable->setEnabled(true);
            $this->em->persist($versionable);
        }

        $draftVersion = $versionable->getDraftVersion();

        $currentVersion = $versionable->getCurrentVersion($locale);
        if (null !== $currentVersion) {
            $currentVersion->setCurrentVersion(false);
            $this->em->persist($currentVersion);
        }

        $versionClass = $this->getVersionClass($versionable);
        /** @var Version $version */
        $publishedVersion = new $versionClass();
        $publishedVersion->setVersionable($versionable);
        $publishedVersion->setCurrentVersion(true);
        $publishedVersion->setLocale($locale);
        $publishedVersion->setHash($hash);

        foreach ($this->getCloneableFields($draftVersion) as $fieldName) {
            $cloneMethod = 'clone'.ucfirst($fieldName);
            if (method_exists($draftVersion, $cloneMethod)) {
                $draftVersion->$cloneMethod($publishedVersion);
            } else {
                $getterMethod = 'get'.ucfirst($fieldName);
                $setterMethod = 'set'.ucfirst($fieldName);
                if (method_exists($publishedVersion, $setterMethod)) {
                    $publishedVersion->$setterMethod($draftVersion->$getterMethod());
                }
            }
        }
        $this->em->persist($publishedVersion);
        $this->em->flush();

        // Update medias
        if ($draftVersion instanceof HasMediasInterface) {
            $this->cloneMedias($draftVersion, $publishedVersion);
        }

        // Update galleries
        if ($draftVersion instanceof HasGalleriesInterface) {
            $this->cloneGalleries($draftVersion, $publishedVersion);
        }

        // Update metatags
        if ($draftVersion instanceof MetaTaggable) {
            $this->cloneMetatags($draftVersion, $publishedVersion, $locale);
        }

        // Update modules
        if ($draftVersion instanceof Modulable) {
            $this->cloneModules($draftVersion, $publishedVersion, $locale);
        }

        // Serialize translations
        $translations = [];
        if ($draftVersion instanceof Translatable) {
            $this->translationManager->loadTranslatablePropertiesTranslations(
                $publishedVersion,
                $locale,
                ["get", "medias", "metatags", "modules", "galleries"]
            );
        }

        // Publish resource guide translations
        $guideTranslations = $this->publishResourceGuideTranslations($draftVersion);
        foreach ($guideTranslations as $loc => $guideTranslation) {
            $translations[$loc]['guide'] = $guideTranslation;
        }
        $publishedVersion->setTranslations($translations);
        $versionable->addVersion($publishedVersion);
        $versionable->setUpdatedAt(new DateTime());
        $this->em->persist($versionable);
        $this->em->persist($publishedVersion);
        $this->em->flush();

        return $publishedVersion;
    }

    /**
     * @param Versionable $versionable
     *
     * @return string
     * @throws Exception
     */
    public function getVersionClass(Versionable $versionable): string
    {
        $metadata = $this->getClassMetadata(get_class($versionable));
        foreach ($metadata as $property => $propertyMetadata) {
            if ($property === 'versions') {
                return $propertyMetadata['targetEntity'];
            }
        }
        throw new RuntimeException("Version class not found");
    }

    /**
     * @param Version $draftVersion
     *
     * @return array
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TranslationClientException
     * @throws TransportExceptionInterface
     * @throws JsonException
     */
    private function publishResourceGuideTranslations(Version $draftVersion): array
    {
        $guideTranslations = [];
        if ($draftVersion instanceof Guidable) {
            /** @var ResourceGuide $guide */
            $this->resourceGuideProvider->getGuide($draftVersion->getGuide());
            $draftGuideTranslations = $this->translationManager->getResourceGuideTranslations($draftVersion);
            foreach ($draftGuideTranslations as $guideTranslation) {
                if (!array_key_exists($guideTranslation['locale'], $guideTranslations)) {
                    $guideTranslations[$guideTranslation['locale']] = [];
                }
                if (!array_key_exists($guideTranslation['zone'], $guideTranslations[$guideTranslation['locale']])) {
                    $guideTranslations[$guideTranslation['locale']][$guideTranslation['zone']] = [];
                }
                $guideTranslations[$guideTranslation['locale']][$guideTranslation['zone']][$guideTranslation['key']] = $guideTranslation['message'];
            }
        }
        return $guideTranslations;
    }

    /**
     * @param CmsResourceInterface $resource
     * @param string               $locale
     *
     * @return array
     * @throws NonUniqueResultException
     * @throws Exception
     */
    public function getGuideTranslationsForResource(CmsResourceInterface $resource, string $locale): array
    {
        /** @var TranslationsPublicationRepository  $repository */
        $repository = $this->em->getRepository(TranslationsPublication::class);
        $translations = [];

        // Getting the resource guide translations
        $guideName = null;
        if ($resource instanceof Guidable) {
            $guideName = $resource->getGuide();
        }
        if ($resource instanceof Versionable) {
            $currentVersion = $resource->getCurrentVersion($locale);
            if ($currentVersion instanceof Guidable) {
                $guideName = $currentVersion->getGuide();
            }
        }
        if (null !== $guideName) {
            $translationsPublication = $repository->findActive($locale, $guideName);
            if ($translationsPublication) {
                $translations[$guideName] = $translationsPublication->getTranslations();
            }
        }

        return $translations;
    }

    /**
     * @param CmsResourceInterface $resource
     * @param string               $locale
     *
     * @return array
     * @throws NonUniqueResultException
     */
    public function getLayoutTranslations(CmsResourceInterface $resource, string $locale): array
    {
        /** @var TranslationsPublicationRepository  $repository */
        $repository = $this->em->getRepository(TranslationsPublication::class);
        $translations = [];

        // Getting the layout guide, added on every resource to manage layout shared translations
        $translationsPublication = $repository->findActive($locale, "layout");
        if ($translationsPublication) {
            $translations['layout'] = $translationsPublication->getTranslations();
        }

        return $translations;
    }

    /**
     * @param string $discriminator
     * @param string $locale
     * @param bool   $flush
     *
     * @return TranslationsPublication
     * @throws ClientExceptionInterface
     * @throws RedirectionExceptionInterface
     * @throws ResourceGuideNotFoundException
     * @throws ServerExceptionInterface
     * @throws TransportExceptionInterface|JsonException
     */
    public function publishLayoutGuideTranslations(string $discriminator, string $locale, bool $flush = false): TranslationsPublication
    {
        $guide = $this->resourceGuideProvider->getGuide($discriminator);
        if (null === $guide) {
            throw new RuntimeException("Guide not found");
        }
        try {
            $guideTranslations = $this->translationManager->getLayoutGuideTranslations($guide);
        } catch (Exception $e) {
            throw new RuntimeException("Error getting translations from SquareTranslate : ".$e->getMessage());
        }
        $translations = $this->translationManager->formatTranslationsAsArray($guideTranslations, $locale);

        /** @var TranslationsPublication $previousPublication */
        $previousPublication = $this->em->getRepository(TranslationsPublication::class)->findActive($locale, $discriminator);

        if ($previousPublication) {
            $version = $previousPublication->getVersion() + 1;
        } else {
            $version = 1;
        }

        $publication = new TranslationsPublication();
        $publication->setGuideIdentifier($discriminator);
        $publication->setLocale($locale);
        $publication->setTranslations($translations);
        $publication->setVersion($version);
        $this->em->persist($publication);

        if ($flush) {
            $this->em->flush();
        }

        return $publication;
    }

    /**
     * @param Modulable $draftVersion
     * @param Modulable $publishedVersion
     * @param string    $locale
     *
     * @return void
     */
    private function cloneModules(Modulable $draftVersion, Modulable $publishedVersion, string $locale): void
    {
        $publishedModules = [];
        /** @var Module $draftModule */
        foreach ($this->moduleManager->findByResource($draftVersion, $locale) as $draftModule) {
            $publishedModule = new Module();
            $publishedModule->setIdentifier($draftModule->getIdentifier());
            $publishedModule->setGuideIdentifier($draftModule->getGuideIdentifier());
            $publishedModule->setModulableClass($publishedVersion->getResourceClass());
            $publishedModule->setModulableIdentifier($publishedVersion->getResourceIdentifier());
            $publishedModule->setLocale($draftModule->getLocale());
            $publishedModule->setZone($draftModule->getZone());
            $publishedModule->setPosition($draftModule->getPosition());

            foreach ($draftModule->getParameters() as $draftParameter) {
                $parameter = new ModuleParameter();
                $parameter->setIdentifier($draftParameter->getIdentifier());
                $parameter->setValue($draftParameter->getValue());
                $parameter->setResourceClass($draftParameter->getResourceClass());
                $parameter->setResourceIdentifier($draftParameter->getResourceIdentifier());
                $publishedModule->addParameter($parameter);
            }

            $this->em->persist($publishedModule);
            $this->em->flush();
            $this->cloneMedias($draftModule, $publishedModule);
            $this->cloneGalleries($draftModule, $publishedModule);
            $publishedModules[] = $publishedModule;
        }
        $publishedVersion->setModules($publishedModules);
    }

    /**
     * @param HasMediasInterface $draftVersion
     * @param HasMediasInterface $publishedVersion
     *
     * @return void
     */
    private function cloneMedias(HasMediasInterface $draftVersion, HasMediasInterface $publishedVersion): void
    {
        foreach ($this->hasMediaManager->getResourceMedias($draftVersion) as $resourceHasMedia) {
            $this->hasMediaManager->addMediaToResource($resourceHasMedia->getMedia(), $publishedVersion, [
                'identifier' => $resourceHasMedia->getIdentifier(),
                'display_name' => $resourceHasMedia->getName(),
                'alt' => $resourceHasMedia->getAlt()
            ], false);
        }
    }

    /**
     * @param HasGalleriesInterface $draftVersion
     * @param HasGalleriesInterface $publishedVersion
     *
     * @return void
     */
    private function cloneGalleries(HasGalleriesInterface $draftVersion, HasGalleriesInterface $publishedVersion): void
    {
        /** @var Gallery $gallery */
        foreach ($this->galleryManager->getResourceGalleries($draftVersion) as $gallery) {
            $this->em->refresh($gallery); // Refresh needed to get gallery medias
            $newGallery = new Gallery();
            $newGallery->setIdentifier($gallery->getIdentifier());
            $newGallery->setName($gallery->getName());
            $newGallery->setResourceIdentifier($publishedVersion->getResourceIdentifier());
            $newGallery->setResourceClass($publishedVersion->getResourceClass());
            $this->em->persist($newGallery);
            $this->em->flush();
            foreach ($gallery->getMedias() as $galleryMedia) {
                $this->galleryManager->addMediaToGallery($galleryMedia->getMedia(), $newGallery, [
                    'display_name' => $galleryMedia->getName(),
                    'alt' => $galleryMedia->getAlt()
                ]);
            }
            $this->em->persist($newGallery);
        }
    }

    /**
     * @param MetaTaggable $draftVersion
     * @param MetaTaggable $publishedVersion
     * @param string       $locale
     *
     * @return void
     */
    private function cloneMetatags(MetaTaggable $draftVersion, MetaTaggable $publishedVersion, string $locale): void
    {
        $publishedMetaTags = [];

        /** @var MetaTag $tag */
        foreach ($this->metaTagManager->findByResource($draftVersion, $locale) as $tag) {
            $tempTag = clone $tag;

            $metaTag = new MetaTag();
            $metaTag->setResourceIdentifier($publishedVersion->getResourceIdentifier());
            $metaTag->setResourceClass($publishedVersion->getResourceClass());
            $metaTag->setName($tempTag->getName());
            $metaTag->setLocale($locale);
            $metaTag->setType($tempTag->getType());
            $metaTag->setContent($tempTag->getContent());
            $this->em->persist($metaTag);
            $publishedMetaTags[] = $metaTag;
        }
        $publishedVersion->setMetatags($publishedMetaTags);
    }

    /**
     * @param Version $version
     *
     * @return array
     */
    private function getCloneableFields(Version $version): array
    {
        $fields = array();
        $reader = new AnnotationReader();
        $reflectionClass = new ReflectionClass(get_class($version));

        foreach ($reflectionClass->getProperties() as $reflectionProperty) {
            if (!$reader->getPropertyAnnotation($reflectionProperty, ExcludeFromPublication::class)) {
                $fields[] = $reflectionProperty->getName();
            }
        }

        return $fields;
    }

    /**
     * @param string $className
     *
     * @return array
     */
    private function getClassMetadata(string $className): array
    {
        $metadata = $this->em->getClassMetadata($className);
        return array_merge($metadata->fieldMappings, $metadata->associationMappings);
    }
}
