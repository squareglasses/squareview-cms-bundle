<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use SG\CmsBundle\Api\Entity\Module;
use SG\CmsBundle\Api\Entity\ModuleParameter;
use SG\CmsBundle\Api\Contracts\Modulable;
use SG\CmsBundle\Api\Contracts\ModuleInterface;

/**
 * Class ModuleManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class ModuleManager
{
    private ?ModuleInterface $module = null;
    private int $position = 1;

    /**
     * ModuleManager constructor.
     * @param EntityManagerInterface $manager
     */
    public function __construct(private readonly EntityManagerInterface $manager)
    {
    }

    /**
     * @param Modulable   $resource
     * @param string      $guide
     * @param string|null $locale
     * @param string      $zone
     * @param string|null $identifier
     * @param int|null    $position
     * @param bool        $flush
     *
     * @return ModuleInterface
     */
    public function addModuleToResource(
        Modulable $resource,
        string $guide,
        ?string $locale = null,
        string $zone = 'default',
        ?string $identifier = null,
        ?int $position = null,
        bool $flush = false
    ): ModuleInterface {
        $this->module = new Module($identifier);
        $this->module->setGuideIdentifier($guide);
        $this->module->setModulableClass($resource->getResourceClass());
        $this->module->setModulableIdentifier($resource->getResourceIdentifier());
        $this->module->setLocale($locale);
        $this->module->setZone($zone);
        $this->module->setPosition($position ?? $this->position);
        $this->manager->persist($this->module);

        if ($flush) {
            $this->manager->flush();
        }
        $this->position = $position !== null ? $position+1 : $this->position+1;

        return $this->module;
    }

    /**
     * @param string $identifier
     * @param string $value
     * @param string|null $resourceClass
     * @param string|null $resourceIdentifier
     * @return void
     */
    public function addParameter(string $identifier, string $value, ?string $resourceClass = null, ?string $resourceIdentifier = null): void
    {
        $parameter = new ModuleParameter();
        $parameter->setIdentifier($identifier);
        $parameter->setValue($value);
        $parameter->setResourceClass($resourceClass);
        $parameter->setResourceIdentifier($resourceIdentifier);
        $this->module->addParameter($parameter);
        $this->manager->persist($this->module);
        $this->manager->flush();
    }

    /**
     * @param string $identifier
     * @param object $entity
     *
     * @return void
     */
    public function addEntityParameter(string $identifier, object $entity): void
    {
        $parameter = (new ModuleParameter())
            ->setIdentifier($identifier)
            ->setResourceClass($entity->getResourceClass())
            ->setResourceIdentifier((string)$entity->getResourceIdentifier())
        ;
        $this->module->addParameter($parameter);
        $this->manager->persist($this->module);
    }

    /**
     * @param Modulable $resource
     * @param string    $locale
     * @param bool      $arrayResults
     *
     * @return array
     */
    public function findByResource(Modulable $resource, string $locale = 'fr', bool $arrayResults = false): array
    {
        /** @var EntityRepository $repo */
        $repo = $this->manager->getRepository(Module::class);
        $qb = $repo
            ->createQueryBuilder("m")
            ->where("m.modulableIdentifier = :resourceIdentifier")
            ->andWhere("m.modulableClass = :resourceClass")
            ->andWhere("m.locale = :locale")
            ->setParameter("resourceIdentifier", $resource->getResourceIdentifier())
            ->setParameter("resourceClass", $resource->getResourceClass())
            ->setParameter("locale", $locale)
            ->orderBy("m.position", "ASC")
        ;

        if ($arrayResults) {
            return $qb->getQuery()->getArrayResult();
        }

        $modules = $qb->getQuery()->getResult();

        if (count($resource->getModules()) === 0) {
            foreach ($modules as $module) {
                $parameters = $module->getParameters();
                foreach ($parameters as $parameter) {
                    if ($parameter->getResourceClass() && $parameter->getResourceIdentifier()) {
                        $resource = $this->manager->getRepository($parameter->getResourceClass())->find($parameter->getResourceIdentifier());
                        if (!$resource) {
                            $parameter->setResourceClass(null);
                            $parameter->setResourceIdentifier(null);
                        } else {
                            $parameter->setResource($resource);
                        }
                    }
                }
            }
        }
        return $modules;
    }

    /**
     * @param Modulable $resource
     * @param string    $locale
     *
     * @return void
     */
    public function loadModules(Modulable $resource, string $locale): void
    {
        $resource->setModules($this->findByResource($resource, $locale));
    }

    /**
     * @param ModuleInterface $module
     *
     * @return Modulable|null
     */
    public function findResource(ModuleInterface $module): ?Modulable
    {
        return $this->manager->getRepository($module->getModulableClass())
            ->find($module->getModulableIdentifier());
    }

    /**
     * @param Module $module
     *
     * @return void
     */
    public function deleteModule(Module $module): void
    {
        $this->manager->remove($module);
        $this->manager->flush();
    }

    /**
     * @param Modulable $resource
     *
     * @return void
     */
    public function deleteAllModulesForResource(Modulable $resource): void
    {
        $modules = $this->manager->createQueryBuilder()
            ->select('m')
            ->from(Module::class, 'm')
            ->where('m.modulableClass = :class')
            ->setParameter('class', $resource->getResourceClass())
            ->andWhere('m.modulableIdentifier = :identifier')
            ->setParameter('identifier', $resource->getResourceIdentifier())
            ->getQuery()
            ->getResult();

        foreach ($modules as $module) {
            $this->manager->remove($module);
            //ModuleParameters are cascade deleted
        }
        $this->manager->flush();
    }
}
