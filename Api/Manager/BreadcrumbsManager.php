<?php
/*
* This file is part of the SquareView package.
*
* (c) Square Glasses <dev@squareglasses.com>
*
* For the full copyright and license information, please view the LICENSE
* file that was distributed with this source code.
*/

declare(strict_types=1);

namespace SG\CmsBundle\Api\Manager;

use Doctrine\ORM\NonUniqueResultException;
use Doctrine\Persistence\Mapping\MappingException;
use ReflectionException;
use SG\CmsBundle\Api\Contracts\Routable;
use Doctrine\ORM\EntityManagerInterface;
use SG\CmsBundle\Api\Entity\Route;
use SG\CmsBundle\Api\Repository\RouteRepository;

/**
 * Class BreadcrumbsManager
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class BreadcrumbsManager
{
    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(private readonly EntityManagerInterface $em)
    {
    }

    /**
     * @param Routable $routable
     * @param string   $locale
     *
     * @return string|null
     * @throws MappingException
     * @throws NonUniqueResultException
     * @throws ReflectionException
     */
    public function getRouteForResource(Routable $routable, string $locale): ?string
    {
        /** @var RouteRepository $repo */
        $repo = $this->em->getRepository(Route::class);
        return $repo->findOneByResource($routable, $locale)?->getName();
    }

    /**
     * @param array $serializedEntity
     *
     * @return string
     */
    public static function getLabelFromSerializedEntity(array $serializedEntity): string
    {
        if (array_key_exists("breadcrumbsLabel", $serializedEntity) && null !== $serializedEntity['breadcrumbsLabel']) {
            return $serializedEntity['breadcrumbsLabel'];
        }

        if (array_key_exists("title", $serializedEntity) && null !== $serializedEntity['title']) {
            return $serializedEntity['title'];
        }
        return array_key_exists("name", $serializedEntity) && $serializedEntity['name'] ?
            $serializedEntity['name'] : "Name not found";
    }
}
