# ================================================================================
# FUNCTIONS
# ================================================================================
$(call check_defined, PROJECT_DIR)


# ================================================================================
# DOCKER MANAGEMENT
# ================================================================================
docker-install:
	@make docker-compose-prepare
	@make docker-load
	@echo "Try docker-install-debug if things don't seem to work"

docker-install-debug:
	@make docker-compose-prepare
	@make docker-compose-foreground

docker-compose-prepare:
ifeq (,$(wildcard $(PROJECT_DIR)/docker-compose.overwrite.yml))
	@echo "Creating docker-compose.overwrite.yml based on .dist file, please edit it to meet your local requirements"
	@cp $(PROJECT_DIR)/docker-compose.overwrite.yml.dist $(PROJECT_DIR)/docker-compose.overwrite.yml
	@echo "Automatically naming database name to project slug '$(PROJECT_IDENTIFIER)'"
ifeq ($(OS),Darwin)
	@sed -i '' "s/\[DATABASE_NAME\]/$(PROJECT_IDENTIFIER)/" '$(PROJECT_DIR)/docker-compose.overwrite.yml'
	@sed -i '' "s/\[DATABASE_NAME\]/$(PROJECT_IDENTIFIER)/" '$(PROJECT_DIR)/.localconfig'
	@sed -i '' "s/\[DATABASE_PORT\]/$(DATABASE_PORT)/" '$(PROJECT_DIR)/docker-compose.overwrite.yml'
	@sed -i '' "s/\[PROJECT_ID\]/$(PROJECT_ID)/" '$(PROJECT_DIR)/docker-compose.overwrite.yml'
else
	@sed -i "s/\[DATABASE_NAME\]/$(PROJECT_IDENTIFIER)/" '$(PROJECT_DIR)/docker-compose.overwrite.yml'
	@sed -i "s/\[DATABASE_NAME\]/$(PROJECT_IDENTIFIER)/" '$(PROJECT_DIR)/.localconfig'
	@sed -i "s/\[DATABASE_PORT\]/$(DATABASE_PORT)/" '$(PROJECT_DIR)/docker-compose.overwrite.yml'
	@sed -i "s/\[PROJECT_ID\]/$(PROJECT_ID)/" '$(PROJECT_DIR)/docker-compose.overwrite.yml'
endif
else
	@echo "Skipping docker-compose-prepare"
endif

docker-load:
	@echo "Removing docker project"
	@cd $(PROJECT_DIR) \
	&& docker compose kill \
	&& docker compose rm -f \
	&& symfony server:stop \
	&& docker compose build
	@echo "Loading docker project in background..."
	@make docker-compose-background
	@echo "🚀 Docker project running in background"

docker-compose-foreground:
	@cd $(PROJECT_DIR) && symfony run docker compose -f docker-compose.yml -f docker-compose.overwrite.yml up --remove-orphans

docker-compose-background:
	@cd $(PROJECT_DIR) && symfony run -d docker compose -f docker-compose.yml -f docker-compose.overwrite.yml up --remove-orphans
