# ================================================================================
# CHECKING VARS
# ================================================================================
$(call check_defined, PROJECT_DIR)

# ================================================================================
# SYMFONY LOCAL SERVER MANAGEMENT
# ================================================================================
stop:
	@echo "Stopping Symfony server..."
	@cd $(PROJECT_DIR)/$(for) && symfony server:stop
	@cd $(PROJECT_DIR)/$(for) && symfony proxy:stop

restart:
	# killing remaining symfony server
	@make stop

	# running symfony server
	@echo "Starting Symfony proxy..."
	@cd $(PROJECT_DIR)/$(for) && symfony proxy:start
	@echo "Starting Symfony server..."
ifeq ($(TLS),true)
	@cd $(PROJECT_DIR)/$(for) && symfony server:start -d
else
	@cd $(PROJECT_DIR)/$(for) && symfony server:start -d --no-tls
endif
	@echo "Attaching Symfony proxy..."
ifeq ($(for),api)
	@cd $(PROJECT_DIR)/$(for) && symfony proxy:domain:attach $(API_HOST)
else ifeq ($(for),frontend)
	@cd $(PROJECT_DIR)/$(for) && symfony proxy:domain:attach $(FRONTEND_HOST)
else ifeq ($(for),backend)
	@cd $(PROJECT_DIR)/$(for) && symfony proxy:domain:attach $(BACKEND_HOST)
endif
	@echo "Done restarting."

log:
    @cd $(PROJECT_DIR)/$(for) && symfony server:log
