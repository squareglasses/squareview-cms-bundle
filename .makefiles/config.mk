# ================================================================================
# FUNCTIONS
# ================================================================================
check_defined = \
    $(foreach 1,$1,$(__check_defined))
__check_defined = \
    $(if $(value $1),, \
      $(error Undefined $1$(if $(value 2), ($(strip $2)))))

# ================================================================================
# CHECKING VARS
# ================================================================================
$(call check_defined, for)
$(call check_defined, PROJECT_DIR)

# ================================================================================
# COMMANDS
# ================================================================================
env-local:
	@echo "Creating .env.local for $(for)"
	@touch $(PROJECT_DIR)/$(for)/.env.local
	@echo "SLACK_TOKEN=$(SLACK_TOKEN)" > $(PROJECT_DIR)/$(for)/.env.local
	@echo "MAILER_DSN=smtp://localhost:25$(PROJECT_ID)" >> $(PROJECT_DIR)/$(for)/.env.local
ifeq ($(for),api)
	@echo "SELF_URL=http://$(API_HOST).wip/" >> $(PROJECT_DIR)/$(for)/.env.local
	@echo "DATABASE_URL=mysql://root:root@127.0.0.1:$(DATABASE_PORT)/$(DATABASE_NAME)" >> $(PROJECT_DIR)/$(for)/.env.local
	@echo "FRONTEND_URL=http://$(FRONTEND_HOST).wip/" >> $(PROJECT_DIR)/$(for)/.env.local
else ifeq ($(for),backend)
	@echo "API_URL=http://$(API_HOST).wip/" >> $(PROJECT_DIR)/$(for)/.env.local
	@echo "FRONTEND_URL=http://$(FRONTEND_HOST).wip/" >> $(PROJECT_DIR)/$(for)/.env.local
else ifeq ($(for),frontend)
	@echo "API_URL=http://$(API_HOST).wip/" >> $(PROJECT_DIR)/$(for)/.env.local
endif

local-vars:
	@echo "Checking local configuration..."
ifeq (,$(wildcard $(PROJECT_DIR)/.localconfig))
	@cp ./.makefiles/.localconfig.dist $(PROJECT_DIR)/.localconfig
endif
	@.makefiles/commands.sh check-project-id  $(PROJECT_DIR)/.localconfig
	@.makefiles/commands.sh check-project-name $(PROJECT_DIR)/.localconfig $(PROJECT_DIR)/frontend/config/squareview/cms.yaml
	@.makefiles/commands.sh check-project-identifier $(PROJECT_DIR)/.localconfig $(PROJECT_DIR)/frontend/config/squareview/cms.yaml
	@echo "Project use https locally? [true or false]): $(TLS)"
	@.makefiles/commands.sh check-localconfig-item $(PROJECT_DIR)/.localconfig "TLS"
	@echo "Enter a Slack Token for error reports if you have one [check https://api.slack.com/apps/A028JN1FK1B/install-on-team]: $(SLACK_TOKEN)"
	@.makefiles/commands.sh check-localconfig-item $(PROJECT_DIR)/.localconfig "SLACK_TOKEN"
	@echo "✅ Local configuration"
