#!/usr/bin/env bash
. $(dirname "$0")/style.sh #import style + color to format message

# ================================================================================
# COMMON FUNCTIONS
# ================================================================================

# Shortcut to symfony console
function console() {
    php bin/console $*
}

function sync-frontend-config() {
	console sg:config:sync-frontend
}

function command_exists() {
	type "$1" &>/dev/null
}

# ================================================================================
# API
# ================================================================================

# enable pre-commit hooks with php validations
function install_hooks() {
    ln -s $(pwd)/hooks/pre-commit .git/hooks/pre-commit
}

# -----------------
# SCHEMA AND DATABASE MANAGEMENT
# -----------------
function database-drop() {
	cd ./api
	display_title "Dropping database..."
	console doctrine:database:drop --force --if-exists
	display_success "Database successfully dropped!"
}

function database-create() {
	cd ./api
	display_title "Creating database..."
	console doctrine:database:create --if-not-exists
	display_success "Database successfully created!"
}

function database-reset() {
	cd ./api
	display_title "Reset the database"
	console doctrine:database:drop  --if-exists --force
	console doctrine:database:create --if-not-exists
	display_success "Database has been successfully reset!"
}

function schema-update() {
	cd ./api
	display_title "Updating database schema..."
	console doctrine:schema:update --force --complete
}

function migration-diff() {
	cd ./api
	display_title "Apply and create migrations from current database"
	display_subtitle "Applying applications migrations..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_subtitle "Generate new migrations if necessary..."
	console doctrine:migrations:diff --allow-empty-diff --quiet
	display_subtitle "Applying new migrations if neccessary..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_success "Migrations successfully created and applied!"
}

function migration() {
	cd ./api
	display_title "Apply and create migrations from cleared database"
	display_subtitle "Reset database..."
	console doctrine:database:drop --force  --if-exists --quiet
	console doctrine:database:create --if-not-exists --quiet
	display_subtitle "Applying applications migrations..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_subtitle "Generate new migrations if necessary..."
	console doctrine:migrations:diff --allow-empty-diff --quiet
	display_subtitle "Applying new migrations if neccessary..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_success "Migrations successfully created and applied!"
}

function migrate() {
	cd ./api
	display_title "Apply migrations"
	console doctrine:migrations:migrate --no-interaction
	display_success "Migrations successfully applied!"
}

# -----------------
# DATA FIXTURES MANAGEMENT
# -----------------
function load-data() {
	cd ./api
	display_title "Load data fixtures into API's database"
	display_subtitle "Synchronize frontend configuration..."
	sync-frontend-config
	display_subtitle "Clearing cache for API..."
	rm -rf var/cache/*
	display_subtitle "Loading data fixtures..."
	console doctrine:fixtures:load --no-interaction
	display_success "Data fixtures successfully loaded!"
}

function reload-data() {
	cd ./api
	display_title "Load data fixtures into API's database from empty database"
	display_subtitle "Synchronize frontend configuration..."
	sync-frontend-config
	display_subtitle "Clearing cache for API..."
	console cache:clear --quiet
	display_subtitle "Reset database..."
	console doctrine:database:drop --force  --if-exists --quiet
	console doctrine:database:create --if-not-exists --quiet
	display_subtitle "Applying applications migrations..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_subtitle "Loading data fixtures..."
	console doctrine:fixtures:load --no-interaction
	display_subtitle "Executing data migrations..."
    console data:migrations:migrate --no-interaction
	rm -rf var/cache/*
	display_success "Data fixtures successfully reloaded!"
}

function rebuild-data() {
	cd ./api
	display_title "Rebuild database and load data fixtures into API's database"
	display_subtitle "Synchronize frontend configuration..."
	sync-frontend-config
	display_subtitle "Clearing cache for API..."
	console cache:clear --quiet
	display_subtitle "Reset database..."
	console doctrine:database:drop --force  --if-exists --quiet
	console doctrine:database:create --if-not-exists --quiet
	display_subtitle "Applying applications migrations..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_subtitle "Generate new migrations if necessary..."
	console doctrine:migrations:diff --allow-empty-diff --quiet
	display_subtitle "Applying new migrations if neccessary..."
	console doctrine:migrations:migrate --no-interaction --quiet
	display_subtitle "Loading data fixtures..."
	console doctrine:fixtures:load --no-interaction --quiet
	rm -rf var/cache/*
	display_success "Data fixtures successfully rebuilt!"
}

function drop-data() {
	cd ./api
	display_title "Reset database"
	display_subtitle "Clearing cache for API..."
	console cache:clear --quiet
	display_subtitle "Reset database..."
	database-drop "quiet"
	display_success "Database successfully reset!"
}


# -----------------
# SECURITY
# -----------------
function generate-pk() {
	cd ${1}
	sh -c 'set -e
		mkdir -p config/jwt
		jwt_passhrase=$(grep ''^JWT_PASSPHRASE='' .env | cut -f 2 -d ''='')
		echo "$jwt_passhrase" | openssl genpkey -out config/jwt/private.pem -pass stdin -aes256 -algorithm rsa -pkeyopt rsa_keygen_bits:4096
		echo "$jwt_passhrase" | openssl pkey -in config/jwt/private.pem -passin stdin -out config/jwt/public.pem -pubout
	'
	if command_exists setfacl; then
		sh -c 'setfacl -R -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
			setfacl -dR -m u:www-data:rX -m u:"$(whoami)":rwX config/jwt
		'
	else
		echo no setfacl - using chmod
		sh -c 'chmod -R 750 config/jwt'
	fi
}

function get-backend-jwt-token() {
	printf "\nGenerating new backend JWT token for development (if nothing prints below, make sure the server is running):\n"
	if [ "$2" = true ]; then
		https_proxy=http://127.0.0.1:7080 curl -X POST -H "Content-Type: application/json" https://${1}.wip/authentication_token -d '{"username":"admin","password":"admin"}'
	else
		http_proxy=http://127.0.0.1:7080 curl -s -X POST -H "Content-Type: application/json" http://${1}.wip/authentication_token -d '{"username":"admin","password":"admin"}'
	fi
}

# -----------------
# LOCALCONFIG MAKER
# -----------------
function check-project-id() {
	if grep -q "\[PROJECT_ID\]" "$1"; then
		create-project-id "$1"
	fi
}

function create-project-id() {
	read -p "Enter a project id with 3 digits (eg: 001): " projectId
	echo "🔍 Checking if docker ports are available with project id $projectId..."

	DATABASE_PORT="33"$projectId
	FRONTEND_PORT="40"$projectId
	BACKEND_PORT="41"$projectId
	CHECK_DB_PORT=$(docker ps -a --format "{{.Ports}}" | grep -w "$DATABASE_PORT")
	CHECK_ML_PORT=$(docker ps -a --format "{{.Ports}}" | grep -w "25"$projectId)

	if [ -z "$CHECK_DB_PORT" ]; then
		if [ -z "$CHECK_ML_PORT" ]; then
			if [[ "$OSTYPE" == "darwin"* ]]; then
				sed -i '' "s/\[PROJECT_ID\]/$projectId/" "$1"
				sed -i '' "s/\[DATABASE_PORT\]/$DATABASE_PORT/" "$1"
				sed -i '' "s/\[FRONTEND_PORT\]/$FRONTEND_PORT/" "$1"
				sed -i '' "s/\[BACKEND_PORT\]/$BACKEND_PORT/" "$1"
			else
				sed -i "s/\[PROJECT_ID\]/$projectId/" "$1"
				sed -i "s/\[DATABASE_PORT\]/$DATABASE_PORT/" "$1"
				sed -i "s/\[FRONTEND_PORT\]/$FRONTEND_PORT/" "$1"
				sed -i "s/\[BACKEND_PORT\]/$BACKEND_PORT/" "$1"
			fi
		else
			display_error "MAILCATCHER_PORT $MAILCATCHER_PORT already used change project id."
			create-project-id "$1"
		fi
		echo "✅ Ports available"

	else
		display_error "DATABASE_PORT $DATABASE_PORT already used change project id."
		create-project-id "$1"
	fi
}

#check-localconfig-item file key prompt
function check-localconfig-item() {
	if grep -q "\[$2\]" "$1"; then
		read -p "" value
		if [[ "$OSTYPE" == "darwin"* ]]; then
			sed -i '' "s/\[$2\]/$value/" "$1"
		else
			sed -i "s/\[$2\]/$value/" "$1"
		fi
	fi
}

function check-project-name() {
	if grep -q "\[PROJECT_NAME\]" "$1"; then
		read -p "Enter website full name: " websiteName
		if [[ "$OSTYPE" == "darwin"* ]]; then
			sed -i '' "s/\[PROJECT_NAME\]/$websiteName/" "$1"
		else
			sed -i "s/\[PROJECT_NAME\]/$websiteName/" "$1"
		fi
	fi
    if grep -q "website_name: \'SquareView Skeleton\'" "$2"; then
    	echo "Updating website_name for cms frontend config $websiteName"
		if [[ "$OSTYPE" == "darwin"* ]]; then
			sed -i '' "s/website_name: \'SquareView Skeleton\'/website_name: \'$websiteName\'/" "$2"
		else
			sed -i "s/website_name: \'SquareView Skeleton\'/website_name: \'$websiteName\'/" "$2"
		fi
	fi
}

function check-project-identifier() {
	if grep -q "\[PROJECT_IDENTIFIER\]" "$1"; then
		read -p "Enter website identifier (slug format, will be used in .wip urls): " websiteIdentifier
		if [[ "$OSTYPE" == "darwin"* ]]; then
			sed -i '' "s/\[PROJECT_IDENTIFIER\]/$websiteIdentifier/" "$1"
		else
			sed -i "s/\[PROJECT_IDENTIFIER\]/$websiteIdentifier/" "$1"
		fi
	fi
    if grep -q "website_identifier: squareview-skeleton" "$2"; then
    	echo "Updating website_identifier for cms frontend config $websiteIdentifier"
		if [[ "$OSTYPE" == "darwin"* ]]; then
			sed -i '' "s/website_identifier: squareview-skeleton/website_identifier: $websiteIdentifier/" "$2"
		else
			sed -i "s/website_identifier: squareview-skeleton/website_identifier: $websiteIdentifier/" "$2"
		fi
	fi
}

$*
