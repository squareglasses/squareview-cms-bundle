# ================================================================================
# BUNDLE COMMANDS
# Commands below are meant to be used in bundle directory only
# @TODO: Check if still needed ?
# ================================================================================
.PHONY: help
help:
	@echo "";
	@echo "  Usage: make command-name";
	@echo "";
	@echo "  ************************";
	@echo "";
	@echo "  --- COMMANDS ---";
	@echo "  check 					check code and security for the bundle";
	@echo "  phpunit-tests 					launch phpunit tests";
	@echo "  clean 					launch phpunit tests";
	@echo "";

# Launch phpunit tests
.PHONY: test
test: phpunit-test
phpunit-test:
	echo "PHP Unit tests..."
	composer install
	php ./vendor/bin/simple-phpunit --coverage-text --coverage-html=../../../public/coverage && \
    echo "PHP unit tests checked."

clean:
	rm -rf vendor
	rm -rf config
	rm -rf src
	rm -rf Tests/Functional/var

# ------------------------
# SECURITY & CODE CHECKER
# ------------------------
# @TODO: Move to pre-commit hook
# @TODO: Prevent exit when error
check:
	@make phpmd
	@make phpcs
	@make phpstan

.PHONY: phpmd
phpmd:
	@echo "PHP Mess Detector..."
	@php ./vendor/bin/phpmd ./ text ruleset.xml --exclude assets,bin,node_modules,public,translations,var,vendor \
	&& echo "PHP checked."

.PHONY: phpcs
phpcs:
	@echo "Checking PSR-2 Syntax..."
	@php ./vendor/bin/phpcs ./ --ignore=assets,bin,node_modules,public,src/Migrations,translations,var,vendor \
	&& echo "PSR-2 Syntax checked."

.PHONY: phpstan
phpstan:
	@echo "PHP Stan Analysis..."
	@./vendor/bin/phpstan analyse -c ./phpstan.neon  && \
	echo "PHP checked."


# -----------------
# @TODO: TEST to delete
# -----------------
colors:
	@.makefiles/commands.sh display_colors

titles:
	@.makefiles/commands.sh display_title display_title
	@echo "."
	@.makefiles/commands.sh display_subtitle display_subtitle
	@echo "."
	@.makefiles/commands.sh display_success display_success
	@echo "."
	@.makefiles/commands.sh display_warning display_warning
	@echo "."
	@.makefiles/commands.sh display_info display_info
	@echo "."
	@.makefiles/commands.sh display_error display_error
	@echo "."
	@.makefiles/commands.sh display_command_title display_command_title
