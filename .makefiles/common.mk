# This file is imported in project Makefile

# ================================================================================
# VARIABLES
# ================================================================================
for = api # Default server to apply commands to

# ================================================================================
# COMMANDS
# ================================================================================
#@TODO: Improve style for help
.PHONY: help
help:
	@echo "";
	@echo "  Usage: make command-name for=[server]";
	@echo "  Default server = "$(for);
	@echo "";
	@echo "  ************************";
	@echo "";
	@echo "  --- MAIN COMMANDS ---";
	@echo "  install					full project installation for all servers";
	@echo "  start						start project if it has already been installed";
	@echo "  stop						stop project (symfony-cli/docker)";
	@echo "  serve-front 				        start the frontend project (assets/watch)";
	@echo "  serve-back 				        start the backend project (assets/watch)";
	@echo "";
	@echo "  --- OTHER ---";
	@echo "  app-infos					show information about the sub applications";
	@echo "  clear-cache-all			        clear cache for the sub applications";
	@echo "  log for=[server]			        show symfony local web server logs";
	@echo "  check-all or check-app for=[server]		check code and security for all apps or one app";
	@echo "  security-all					prepare JWT keys and print a JWT token for the backend user";
	@echo "  sync-all					synchronize frontend cms config for api and backend";
	@echo "  reset-project					reset project to initial config state";
	@echo "--- DATA FIXTURES MANAGMENT ---";
	@echo "  load-data					Load data fixtures into database";
	@echo "  reload-data				        Reset database and load data fixtures into database";
	@echo "  rebuild-data				        Reset database, apply and generate migrations, load data fixtures into database";
	@echo "--- SCHEMA AND DATABASE MANAGEMENT ---";
	@echo "  db-drop				        Drop the database";
	@echo "  db-create					Create the database";
	@echo "  db-reset					Reset the database";
	@echo "  schema-update					Update the database schema with local changes";
	@echo "  migration					Apply and create migrations from cleared database";
	@echo "  migration-diff				Apply and create migrations from current database";
	@echo "  migrate					Apply migrations";
	@echo "";

# -----------------
# SERVE APP
# -----------------
back: serve-back
serve-back:
	@cd ./backend \
	&& ../$(SV4_CMD_FILE) sync-frontend-config \
	&& yarn install \
	&& bin/console fos:js-routing:dump --format=json --target=public/fos_js_routes.json \
	&& yarn encore dev-server --client-logging warn --port $(BACKEND_PORT) #watch

front: serve-front
serve-front:
	@cd ./frontend \
	&& yarn install \
	&& bin/console fos:js-routing:dump --format=json --target=public/fos_js_routes.json \
	&& yarn encore dev-server --client-logging warn --port $(FRONTEND_PORT)

start:
	@cd $(SV4_DIR) \
	&& make docker-load \
	&& make restart for=api \
	&& make restart for=backend \
	&& make restart for=frontend
	@make app-infos

stop:
	@cd $(SV4_DIR) \
	&& make stop for=api \
	&& make stop for=backend \
	&& make stop for=frontend

# -----------------
# CACHE
# -----------------
cc: clear-cache-all
clear-cache-all:
	@make clear-cache-app for=api
	@make clear-cache-app for=backend
	@make clear-cache-app for=frontend

clear-cache-app:
	@echo "🧹 Clearing cache for $(for)..."
	@cd $(for) && rm -rf var/cache/*
	@echo "Cache successfully cleared"
#alias
cc-a:
	@make clear-cache-app for=api
cc-b:
	@make clear-cache-app for=backend
cc-f:
	@make clear-cache-app for=frontend

# -----------------
# DATA MANAGEMENT
# -----------------
load-data:
	@$(SV4_CMD_FILE) load-data
reload-data:
	$(SV4_CMD_FILE) reload-data
rebuild-data:
	$(SV4_CMD_FILE) rebuild-data
drop-data:
	@$(SV4_CMD_FILE) drop-data

dbd: db-drop
db-drop:
	@$(SV4_CMD_FILE) database-drop

dbc: db-create
db-create:
	@$(SV4_CMD_FILE) database-create

dbr: db-reset
db-reset:
	@$(SV4_CMD_FILE) database-reset

scu: schema-update
schema-update:
	@$(SV4_CMD_FILE) schema-update

mi: migration
migration:
	@$(SV4_CMD_FILE) migration

migration-diff:
	@$(SV4_CMD_FILE) migration-diff

migrate:
	@$(SV4_CMD_FILE) migrate

# -----------------
# SQUAREVIEW UPDATE
# -----------------
sv-update: squareview-update-all
squareview-update-all:
	@make clear-cache-app for=api
	@make squareview-update-app for=api
	@make clear-cache-app for=backend
	@make squareview-update-app for=backend
	@make clear-cache-app for=frontend
	@make squareview-update-app for=frontend

squareview-update-app:
	@cd ./$(for) && symfony composer update "squareglasses/*"
#alias
sv-update-a:
	@make clear-cache-app for=api
	@make squareview-update-app for=api
sv-update-b:
	@make clear-cache-app for=backend
	@make squareview-update-app for=backend
sv-update-f:
	@make clear-cache-app for=frontend
	@make squareview-update-app for=frontend

# -----------------
# INSTALL VENDORS
# -----------------
post-pull: composer-install-all
composer-install-all:
	@make clear-cache-app for=api
	@make composer-install-app for=api
	@make clear-cache-app for=backend
	@make composer-install-app for=backend
	@make clear-cache-app for=frontend
	@make composer-install-app for=frontend

composer-install-app:
	@echo "Installing dependencies for $(for)..."
	@cd ./$(for) && symfony composer install
	@echo "All dependencies installed"
#alias
ci-a:
	@make clear-cache-app for=api
	@make composer-install-app for=api
ci-b:
	@make clear-cache-app for=backend
	@make composer-install-app for=backend
ci-f:
	@make clear-cache-app for=frontend
	@make composer-install-app for=frontend

# ------------------------
# SECURITY & CODE CHECKER
# ------------------------
# Prepare JWT keys and print a JWT token for the backend user
# @TODO: Check if needed here cause security is made during project installation
security: security-all
security-all:
	@cd $(SV4_DIR) \
 	&& make security-app for=api \
 	&& make security-app for=frontend
#alias
security-a:
	@cd $(SV4_DIR) && make security-app for=api
security-f:
	@cd $(SV4_DIR) && make security-app for=frontend

# Check code and security
# @TODO: Move to pre-commit hook
check-all:
	@make check-app for=api
	@make check-app for=backend
	@make check-app for=frontend

# @TODO: Prevent exit when error
check-app:
	@make phpmd
	@make phpcs
	@make phpstan

.PHONY: phpmd
phpmd:
	@echo "PHP Mess Detector for $(for):"
	@php ./$(for)/vendor/bin/phpmd ./$(for) text ruleset.xml --exclude assets,bin,node_modules,public,translations,var,vendor \
	&& echo "PHP checked."

.PHONY: phpcs
phpcs:
	@echo "Checking PSR-2 Syntax..."
	@php ./$(for)/vendor/bin/phpcs ./$(for) --ignore=assets,bin,node_modules,public,src/Migrations,translations,var,vendor \
	&& echo "PSR-2 Syntax checked."

.PHONY: phpstan
phpstan:
	@echo "PHP Stan Analysis..."
	@./$(for)/vendor/bin/phpstan analyse -c ./$(for)/phpstan.neon  && \
	echo "PHP checked."

# -----------------
# UTILITIES
# -----------------
# Display app informations
app-infos:
	@$(SV4_CMD_FILE) display_app_infos $(API_HOST) $(FRONTEND_HOST) $(BACKEND_HOST) $(TLS)

# Watch for logs
log:
	@cd ./$(for) && symfony server:log
#alias
log-a:
	@make log for=api
log-b:
	@make log for=backend
log-f:
	@make log for=frontend

#Reset project installation
reset-project:
	@make stop
	@make clear-cache-all
	@make drop-data
	@docker compose kill && docker compose rm -f && symfony server:stop
	@rm -rf ./.localconfig ./docker-compose.overwrite.yml ./api/.env.local ./backend/.env.local ./frontend/.env.local
	@echo "Project reset done. Run make install to reconfigure project"

# Update config when changes are made to cms.yaml files
sync-all:
	@make sync-app for=api
	@make sync-app for=backend
sync-app:
	@cd $(for) && php bin/console sg:config:sync-frontend
#alias
sync-a:
	@make sync-app for=api
sync-b:
	@make sync-app for=backend
