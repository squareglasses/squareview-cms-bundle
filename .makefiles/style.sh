# ================================================================================
# COLORS
# ================================================================================
# ANSI Start Codes
# https://gist.github.com/mxmerz/92e97cd27857a9ba787b

# Styles.
Normal="\x1b[0m"
Bold="\x1b[1m"
Faint="\x1b[2m"
Italic="\x1b[3m"
Underline="\x1b[4m"
Blink_Slow="\x1b[5m"
Blink_Rapid="\x1b[6m"
Inverse="\x1b[7m"
Conceal="\x1b[8m"
Crossed_Out="\x1b[9m"
# Text colors.
Black="\x1b[30m"
Red="\x1b[31m"
Green="\x1b[32m"
Yellow="\x1b[33m"
Blue="\x1b[34m"
Magenta="\x1b[35m"
Cyan="\x1b[36m"
White="\x1b[37m"
# Background colors.
Bg_Black="\x1b[40m"
Bg_Red="\x1b[41m"
Bg_Green="\x1b[42m"
Bg_Yellow="\x1b[43m"
Bg_Blue="\x1b[44m"
Bg_Magenta="\x1b[45m"
Bg_Cyan="\x1b[46m"
Bg_White="\x1b[47m"
# Resets
NoStyle="\x1b[0m"
NoUnderline="\x1b[24m"
NoInverse="\x1b[27m"
NoColor="\x1b[39m"

Colors1="None $Black""Black""$NoColor $Red""Red""$NoColor $Green""Green""$NoColor $Yellow""Yellow""$NoColor"
Colors2="$Blue""Blue""$NoColor $Magenta""Magenta""$NoColor $Cyan""Cyan""$NoColor $White""White""$NoColor"
AllColors="$Colors1 $Colors2 $NoStyle"

Bg_Black_All="$Bg_Black$AllColors"
Bg_Red_All="$Bg_Red$AllColors"
Bg_Green_All="$Bg_Green$AllColors"
Bg_Yellow_All="$Bg_Yellow$AllColors"
Bg_Blue_All="$Bg_Blue$AllColors"
Bg_Magenta_All="$Bg_Magenta$AllColors"
Bg_Cyan_All="$Bg_Cyan$AllColors"
Bg_White_All="$Bg_White$AllColors"

# Test Table
display_colors() {
	echo -e "Background: | Style:      | Text Colors:"
	echo -e "------------|-------------|----------------------------------------------------"
	echo -e "            | Normal      | "$Normal$AllColors
	echo -e "            | Bold        | "$Bold$AllColors
	echo -e "            | Faint       | "$Faint$AllColors
	echo -e "            | Italic      | "$Italic$AllColors
	echo -e "            | Underline   | "$Underline$AllColors
	echo -e "            | Blink_Slow  | "$Blink_Slow$AllColors
	echo -e "            | Blink_Rapid | "$Blink_Rapid$AllColors
	echo -e "            | Inverse     | "$Inverse$AllColors
	echo -e "            | Conceal     | "$Conceal$AllColors
	echo -e "            | Crossed_Out | "$Crossed_Out$AllColors
	echo -e "BG Black    | Normal      | "$Normal$Bg_Black_All
	echo -e "BG Red      | Normal      | "$Normal$Bg_Red_All
	echo -e "BG Green    | Normal      | "$Normal$Bg_Green_All
	echo -e "BG Yellow   | Normal      | "$Normal$Bg_Yellow_All
	echo -e "BG Blue     | Normal      | "$Normal$Bg_Blue_All
	echo -e "BG Magenta  | Normal      | "$Normal$Bg_Magenta_All
	echo -e "BG Cyan     | Normal      | "$Normal$Bg_Cyan_All
	echo -e "BG White    | Normal      | "$Normal$Bg_White_All
}


# ================================================================================
# STYLE
# ================================================================================

# colored display for success
display_success()
{
    echo -e "$NoStyle$Bg_Green$Black\n\n [OK] $* \n$NoStyle";
}

# colored display for info
display_info() {
    MESSAGE=${@:-"${RESET}Error: No message passed"}
	echo -e "$Bg_Blue$Black\n\n ${MESSAGE}\n\x1b[K$NoStyle";
}

# colored display for error
display_error() {
    MESSAGE=${@:-"${RESET}Error: No message passed"}
	echo -e "$Bg_Red$White\n\n ${MESSAGE}\n\x1b[K$NoStyle";
}

# colored display for warning
display_warning() {
    MESSAGE=${@:-"${RESET}Error: No message passed"}
	echo -e "$Bg_Yellow$Black\n\n ${MESSAGE}\n\x1b[K$NoStyle";
}

# Display a 3 lines bold title with blue background
display_command_title() {
	MESSAGE=${@:-"${RESET}Error: No message passed"}
	echo -e "$Bg_Cyan$Black\n\n ${MESSAGE}\n\x1b[K$NoStyle";
}

# Display a title text underlined with = (same as symfony's one)
display_title() {
	#blue=$(tput setaf 4)
    #normal=$(tput sgr0)
	UNDERTEXT="";
	for (( i=0; i<${#1}; i++ )); do
		UNDERTEXT="${UNDERTEXT}=";
	done
	echo -e "$NoStyle$Green${UNDERTEXT}";
	echo -e "$1";
	echo -e "${UNDERTEXT}$NoStyle";
}

# Display a subtitle text underlined with =
display_subtitle() {
	echo -e "$NoStyle$Bold$Green$Underline$1 $NoStyle";
}

display_app_infos() {
	if test ${4} = true
	then
	   PROTOCOL="https://"
	else
	   PROTOCOL="http://"
	fi
	echo -e "$Bold$Underline\n Applications are now installed :$NoStyle";
	echo -e "$Bg_Cyan$Black\n\n - API address    : ${PROTOCOL}${1}.wip";
	echo -e " - Frontend address : ${PROTOCOL}${2}.wip";
	echo -e " - Backend address  : ${PROTOCOL}${3}.wip\n$NoStyle";

	display_info " Almost done ! 🥳\n - Run $Underline'make serve-back'$NoUnderline to serve backend admin interface\n - Run $Underline'make serve-front'$NoUnderline to serve frontend app\n\n $Italic Remember to clear bad proxies for Chrome user : chrome://net-internals/#proxy\n$NoStyle\n"
}
