# ================================================================================
# VARIABLES
# ================================================================================
OS := $(shell uname)
PROJECT_DIR = ../../../..
for = api # Default server to apply commands to
-include $(PROJECT_DIR)/.localconfig

# ================================================================================
# COMMANDS
# ================================================================================
# -----------------
# COMMANDS FOR BUNDLE
# commands to call from bundle directory
# -----------------
-include ./.makefiles/bundle.mk

# -----------------
# INSTALL PROJECT
# commands below must only be called by Makefile in project directory
# -----------------
-include ./.makefiles/config.mk
-include ./.makefiles/docker.mk
-include ./.makefiles/symfony.mk

install-project:
	@make local-vars
	@make docker-install
	@make app-install for=api
	@make app-install for=frontend
	@make app-install for=backend
	@#TODO: automatically add project and environment in SquareTranslate
	@#TODO: make reload-data when SquareTranslate config is automatic
	@cd $(PROJECT_DIR) \
	&& make sync-all \
	&& make reload-data \
	&& make app-infos
	@echo "🚀 Installation done !"

app-install: app-install-$(for)

app-install-api:
	@make env-local
	@make restart
	@make security-app

app-install-frontend:
	@make env-local
	@make restart
	@cd $(PROJECT_DIR)/$(for) && symfony composer install
	@make security-app

app-install-backend:
	@make env-local
	@make restart
	@cd $(PROJECT_DIR)/$(for) && symfony composer install

security-app:
	@echo "Generating private keys..."
	.makefiles/commands.sh generate-pk '$(PROJECT_DIR)/$(for)'

