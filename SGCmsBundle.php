<?php
/*
 * This file is part of the SquareView package.
 *
 * (c) Square Glasses <dev@squareglasses.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

declare(strict_types=1);

namespace SG\CmsBundle;

use SG\CmsBundle\Api\DependencyInjection\Compiler\DriverChainCompilerPass;
use SG\CmsBundle\Api\DependencyInjection\Compiler\TranslationCompilerPass;
use SG\CmsBundle\Common\DependencyInjection\Compiler\HttpClientCacheCompilerPass;
use SG\CmsBundle\Common\DependencyInjection\Compiler\MediaProviderCompilerPass;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\LocaleGuesserCompilerPass;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\ModuleGuideProviderCompilerPass;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\OverrideRoutingCompilerPass;
use SG\CmsBundle\DependencyInjection\Compiler\ResourceGuideProviderCompilerPass;
use SG\CmsBundle\Frontend\DependencyInjection\Compiler\SitemapCompilerPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SGCmsBundle
 *
 * @author Florent Chaboud <florent@squareglasses.com>
 */
class SGCmsBundle extends Bundle
{
    /**
     * @param ContainerBuilder $container
     */
    public function build(ContainerBuilder $container): void
    {
        $container->addCompilerPass(new OverrideRoutingCompilerPass());
        $container->addCompilerPass(new ResourceGuideProviderCompilerPass());
        $container->addCompilerPass(new ModuleGuideProviderCompilerPass());
        $container->addCompilerPass(new DriverChainCompilerPass());
        $container->addCompilerPass(new TranslationCompilerPass());
        $container->addCompilerPass(new MediaProviderCompilerPass());
        $container->addCompilerPass(new HttpClientCacheCompilerPass());
        $container->addCompilerPass(new SitemapCompilerPass());
        $container->addCompilerPass(new LocaleGuesserCompilerPass());


        parent::build($container);
    }
}
